import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import CreateGroup from './containers/communication/create-group/create-group';
import ViewGroup from './containers/communication/view-group/view-group';
import MessageCredit from './containers/communication/message-credit/message-credit';
import SendMessage from './containers/communication/send-message/send-message';
import MessageLog from './containers/communication/message-log/message-log';
import StudentHomework from './containers/homework/student-homework/student-homework';
import AssignRole from './containers/communication/assign-role/assign-role';
import RoleManagement from './containers/role-management';
import store from './redux/store';
import AlertNotificationProvider from './context-api/alert-context/alert-state';
import './assets/styles/styles.scss';
import UserManagement from './containers/user-management';
import ViewUsers from './containers//view-users';
import Login from './containers/login';
import Dashboard from './containers/dashboard';
import { listSubjects } from './redux/actions/academic-mapping-actions';
import OnlineclassViewProvider from './containers/online-class/online-class-context/online-class-state';
import CreateClass from './containers/online-class/create-class';
import ViewClassManagement from './containers/online-class/view-class/view-class-management/view-class-management';
import AttendeeList from './containers/online-class/view-class/view-class-management/attendee-list/attendee-list';
import ViewClassStudentCollection from './containers/online-class/view-class/view-class-student/view-class-student-collection';
import SubjectTable from './containers/master-management/subject/subject-table';
import SectionTable from './containers/master-management/section/section-table';
import GradeTable from './containers/master-management/grade/grade-table';
import AcademicYearTable from './containers/master-management/academic-year/academic-year-table';
import MessageTypeTable from './containers/master-management/message-type/message-type-table';
// import OnlineClassResource from './containers/online-class/online-class-resources/online-class-resource';
import HomeworkCard from './containers/homework/homework-card';
import Profile from './containers/profile/profile';
import { fetchLoggedInUserDetails } from './redux/actions';
import TeacherHomework from './containers/homework/teacher-homework';
import HomeworkAdmin from './containers/homework/homework-admin';
import AddHomework from './containers/homework/teacher-homework/add-homework';
import BulkUpload from './containers/user-management/bulk-upload/bulk-upload';
import CoordinatorHomework from './containers/homework/coordinator-homework';
import AddHomeworkCoord from './containers/homework/coordinator-homework/add-homework';
import LessonReport from './containers/lesson-plan/lesson-plan-report';
import LessonPlan from './containers/lesson-plan/lesson-plan-view';
import endpoints from './config/endpoints';
import ChapterTypeTable from './containers/master-management/chapter-type/chapter-type-table';
// import ChapterBook from 'containers/intelligent-textbook/chapterpage/ChapterBook';
import ChapterBook from 'containers/intelligent-textbook/chapterpage/ChapterBook';
import {
  ViewAssessments,
  AssessmentAttemption,
  AssessmentAnalysis,
  AssessmentComparisionUI,
} from './containers/assessment';

import {
  TeacherBlog,
  ContentView,
  ContentViewAdmin,
  ContentViewPrincipal,
  WriteBlog,
  EditBlog,
  PreviewBlog,
  PreviewEditBlog,
  CreateWordCountConfig,
  StudentDashboard,
  TeacherPublishBlogView,
  BlogView,
  CreateGenre,
  ViewGenre,
  ContentViewPublish,
  ContentViewPublishStudent,
  AdminBlog,
  PrincipalBlog,
  PrincipalPublishBlogView,
  StudentPublishBlogView,
  AdminPublishBlogView,
  ContentViewPublishAdmin,
  ContentViewPublishPrincipal,
  EditWordCountConfig,
} from './containers/blog';
import LessonPlanGraphReport from './containers/lesson-plan/lesson-plan-graph-report';
import Discussionforum from './containers/discussionForum/index';
import DiscussionPost from './containers/discussionForum/discussion/DiscussionPost';
import CreateCategory from './containers/discussionForum/createCategory';
import CreateDiscussionForum from './containers/discussionForum/createDiscussionForum';
import CircularList from './containers/circular';
import CreateCircular from './containers/circular/create-circular';
import CircularStore from './containers/circular/context/CircularStore';
import GeneralDairyStore from './containers/general-dairy/context/context';
import Subjectgrade from './containers/subjectGradeMapping';
import ListandFilter from './containers/subjectGradeMapping/listAndFilter';
import GeneralDairyList from './containers/general-dairy';
import GeneralDairyStudentView from './containers/general-dairy/generalDairyStudentView';
import GeneralDairyStudentList from './containers/general-dairy/generalDairyStudnet';
import CreateGeneralDairy from './containers/general-dairy/create-dairy';
import CreateDailyDairy from './containers/daily-dairy/create-daily-dairy';
import DailyDairyList from './containers/daily-dairy/list-daily-dairy';
import AOLClassView from './containers/online-class/aol-view/index';
import ResourceView from './containers/online-class/online-class-resources/index';
import CoursePrice from './containers/master-management/course/course-price';
import CreateCourse from './containers/master-management/course/create-course';
import ImportCourse from './containers/master-management/course/import-course';
import CourseView from './containers/master-management/course/view-course';
import ViewCourseCard from './containers/master-management/course/view-course/view-more-card/ViewCourseCard';
import ViewStore from './containers/master-management/course/view-course/context/ViewStore';
import DailyDairyStore from './containers/daily-dairy/context/context';
import AttendeeListRemake from './containers/attendance';
import Reshuffle from './containers/online-class/aol-view/Reshuffle';
import StudentStrength from './containers/student-strength';
import StudentIdCard from './containers/student-Id-Card';
import SignatureUpload from './containers/signature-upload';
import TeacherBatchView from './containers/teacherBatchView';
import ErpAdminViewClass from './containers/online-class/erp-view-class/admin';
import AolLogin from './containers/aol-login';
import OnlineClassResource from './containers/online-class/online-class-resources/online-class-resource';
import AttachmentPreviewer from './components/attachment-previewer';
import SalesModule from './containers/salesmodule';
import CreateCustomeBatch from './containers/aol-custom-batch';
import CouponCodeGenerator from './containers/aol-coupon-code-generator';
import AddEditCoupon from './containers/aol-coupon-code-generator/add-edit-coupon';
import Forgot from 'containers/Forgot-Password/Forgot';
import Testimonial from './containers/testimonial';
import TestimonialUpload from './containers/testimonial/upload-new-video';
import CreateUpdateAolBlog from './containers/aol-blogs/create-update-aol-blog';
import AolBlogs from './containers/aol-blogs';
import StudentBatchView from './containers/student-batch';
import AolBatchReference from './containers/aol-batch-reference';
import AolCreateBatch from './containers/aol-create-batch';
import StudentBatchTimeslot from './containers/student-batch-timeslot-status';
import AolBundleCourseCreation from './containers/aol-bundle-course-creation';
import ViewBundleCourses from './containers/aol-bundle-course-view';
import AolBundleDisconnect from './containers/aol-bundle-discount';
import StudentBatchEnroll from './containers/aol-student-batch-enroll';
import StudentEnroll from './containers/aol-student-enroll';
import StudentAssignBatch from './containers/aol-create-batch/student-assign-batch';
import AolUnAssignedBatch from './containers/aol-un-assingned-batch';
import StudentBatchChange from './containers/aol-student-batch-enroll/aol-student-batch-change';
import AolTeacherList from './containers/aol-teacher-list';
import TeacherAllBatchesList from './containers/aol-teacher-list/teacher-all-batch-list';
import AOLSalesPaymentList from './containers/aol-sales-payment-list';
import AolSalesPendingBatches from './containers/aol-sales-pending-batches';
import StudentAttendence from './containers/aol-student-attendence';
import AddEditZoomId from './containers/zoom/add-edit-zoom';
import ZoomList from './containers/zoom';
import Certificate from './containers/ViewCertificate/Certificate';
import StudentDesk from './containers/aol-student-desk';
import ExampleStudents from './containers/aol-student-desk/exampleStudents';
import StudentDeskExample from './containers/aol-student-desk/studentdeskExapmle';
import AolViewBatch from './containers/batch/aolBatchView';
import ViewBatchDetails from './containers/batch/viewBatchDetails';
import ViewBatchZoomDetails from 'containers/batch/viewBatchZoomDetails';
import ViewStudentBatchDetails from './containers/batch/ViewStudentBatchDetails';
import ClassWork from './containers/Classwork/index';
import BatchPauseHistory from './containers/aol-student-batch-pause-history';
import BatchPauseAssignNewBatch from './containers/aol-student-batch-pause-history/student-assign-batch';
import ConnectionPod from './containers/connectionPod';
import AllBooksPage from './containers/intelligent-textbook/bookpage/AllBooksPage';
import AcademicYearTableII from './containers/master-management/academic-year-II/academic-year-table';
import TabularField from './containers/new-model/table';
import ClassWorkTeacherReport from './containers/Classwork/classwork-report/classwork-report-teacher/ClassWorkTeacherReport';
import StudentClassWorkReport from './containers/Classwork/StudentClassWork';
import WeeklyReport from './containers/weekly-report';
import BatchExtension from './containers/batchExtension';
import OccupancyReport from './containers/occupancy-report';
import CrudTabel from './containers/new-model/CrudTabel';
import displayName from './config/displayName';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#ff6b6b',
    },
    secondary: {
      main: '#014b7e',
    },
    text: {
      default: '#014b7e',
    },
    background: {
      primary: '#ffffff',
      secondary: '#f9f9f9',
    },
  },
  typography: {
    fontSize: 16,
    color: '#014b7e',
  },

  overrides: {
    MuiButton: {
      // Name of the rule
      root: {
        // Some CSS
        color: '#ffffff',
        backgroundColor: ' #ff6b6b',
      },
    },
  },
});

function App() {
  React.useEffect(() => {
    const {
      repoName = 'Revamp',
      first_name: firstName,
      user_id: userId,
      is_superuser: isSuperuser,
    } = JSON.parse(localStorage.getItem('userDetails') || '{}') || {};
    if (window.location.hostname.includes('localhost')) {
      document.title = [repoName, firstName, userId, isSuperuser ? 'Spr' : 'Nrml'].join(
        ' - '
      );
    }
  }, []);
  return (
    <div className='App'>
      <Router>
        <AlertNotificationProvider>
          <OnlineclassViewProvider>
            <ThemeProvider theme={theme}>
              <AttachmentPreviewer>
                <CircularStore>
                  <GeneralDairyStore>
                    <ViewStore>
                      <DailyDairyStore>
                        <Switch>
                          <Route path='/profile'>
                            {({ match }) => <Profile match={match} />}
                          </Route>
                          <Route path='/role-management'>
                            {({ match }) => <RoleManagement match={match} />}
                          </Route>
                          <Route path='/user-management'>
                            {({ match }) => <UserManagement match={match} />}
                          </Route>
                          {/*
                        <Route exact path='/view-users'>
                            {({ match }) => <ViewUsers match={match} />}
                        </Route>
                        */}
                          <Route path='/communication/messagelog'>
                            {({ match }) => <MessageLog match={match} />}
                          </Route>
                          <Route path='/dashboard'>
                            {({ match }) => <Dashboard match={match} />}
                          </Route>
                          <Route exact path='/'>
                            {({ match, history }) => (
                              <Login match={match} history={history} />
                            )}
                          </Route>
                          <Route exact path='/forgot'>
                            {({ match, history }) => (
                              <Forgot match={match} history={history} />
                            )}
                          </Route>
                          <Route exact path='/aol_login'>
                            {({ match, history }) => (
                              <AolLogin match={match} history={history} />
                            )}
                          </Route>
                          {/*
                        <Route exact path='/assignrole'>
                          {({ match }) => <AssignRole match={match} />}
                        </Route>
                        */}
                          <Route exact path='/blog/genre'>
                            {({ match }) => <CreateGenre match={match} />}
                          </Route>
                          {/* <Route exact path='/blog/genre/edit'>
                          {({ match }) => <EditGenre match={match} />}
                        </Route> */}
                          <Route exact path='/blog/wordcount-config'>
                            {({ match }) => <CreateWordCountConfig match={match} />}
                          </Route>
                          <Route exact path='/blog/wordcount-config/edit'>
                            {({ match }) => <EditWordCountConfig match={match} />}
                          </Route>
                          <Route exact path='/blog/teacher'>
                            {({ match }) => <TeacherBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/admin'>
                            {({ match }) => <AdminBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/principal'>
                            {({ match }) => <PrincipalBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/teacher/contentView'>
                            {({ match }) => <ContentView match={match} />}
                          </Route>
                          <Route exact path='/blog/principal/contentView'>
                            {({ match }) => <ContentViewPrincipal match={match} />}
                          </Route>
                          <Route exact path='/blog/admin/contentView'>
                            {({ match }) => <ContentViewAdmin match={match} />}
                          </Route>
                          <Route exact path='/blog/teacher/contentViewPublish'>
                            {({ match }) => <ContentViewPublish match={match} />}
                          </Route>
                          <Route exact path='/blog/student/contentViewPublishStudent'>
                            {({ match }) => <ContentViewPublishStudent match={match} />}
                          </Route>
                          <Route exact path='/blog/principal/contentViewPublishPrincipal'>
                            {({ match }) => <ContentViewPublishPrincipal match={match} />}
                          </Route>
                          <Route exact path='/blog/admin/contentViewPublishAdmin'>
                            {({ match }) => <ContentViewPublishAdmin match={match} />}
                          </Route>

                          <Route exact path='/blog/teacher/publish/view'>
                            {({ match }) => <TeacherPublishBlogView match={match} />}
                          </Route>
                          <Route exact path='/blog/admin/publish/view'>
                            {({ match }) => <AdminPublishBlogView match={match} />}
                          </Route>
                          <Route exact path='/blog/student/publish/view'>
                            {({ match }) => <StudentPublishBlogView match={match} />}
                          </Route>
                          <Route exact path='/blog/principal/publish/view'>
                            {({ match }) => <PrincipalPublishBlogView match={match} />}
                          </Route>
                          <Route exact path='/blog/student/dashboard'>
                            {({ match }) => <StudentDashboard match={match} />}
                          </Route>
                          <Route exact path='/blog/student/write-blog'>
                            {({ match }) => <WriteBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/student/edit-blog'>
                            {({ match }) => <EditBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/student/preview-blog'>
                            {({ match }) => <PreviewBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/student/preview-edit-blog'>
                            {({ match }) => <PreviewEditBlog match={match} />}
                          </Route>
                          <Route exact path='/blog/student/view-blog'>
                            {({ match }) => <BlogView match={match} />}
                          </Route>
                          <Route exact path='/communication/addgroup'>
                            {({ match }) => <CreateGroup match={match} />}
                          </Route>
                          <Route exact path='/communication/smscredit'>
                            {({ match }) => <MessageCredit match={match} />}
                          </Route>
                          <Route exact path='/communication/viewgroup'>
                            {({ match }) => <ViewGroup match={match} />}
                          </Route>
                          <Route exact path='/communication/sendmessage'>
                            {({ match }) => <SendMessage match={match} />}
                          </Route>
                          <Route exact path='/online-class/create-class'>
                            {({ match }) => <CreateClass match={match} />}
                          </Route>
                          {/* <Route exact path='/online-class/view-class'>
                      {({ match }) => <ViewClassManagement match={match} />}
                    </Route> */}
                          {/* <Route exact path='/online-class/resource'>
                      {({ match }) => <OnlineClassResource match={match} />}
                    </Route> */}
                          <Route exact path='/online-class/attendee-list/:id'>
                            {({ match }) => <AttendeeList match={match} />}
                          </Route>
                          {/* <Route exact path='/online-class/attend-class'>
                          {({ match }) => <AOLClassView match={match} />}
                        </Route> */}
                          {/* {({ match }) => <ViewClassStudentCollection match={match} />} */}
                          <Route exact path='/online-class/resource'>
                            {({ match }) => <ResourceView match={match} />}
                          </Route>
                          <Route exact path='/online-class/view-class'>
                            {({ match }) => <AOLClassView match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/subject-table'>
                            {({ match }) => <SubjectTable match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/section-table'>
                            {({ match }) => <SectionTable match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/grade-table'>
                            {({ match }) => <GradeTable match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/academic-year-table'>
                            {({ match }) => <AcademicYearTable match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/academic-year-table-II'>
                            {({ match }) => <AcademicYearTableII match={match} />}
                          </Route>
                          <Route exact path='/dynamic/academic-year'>
                            {({ match }) => <TabularField match={match} />}
                          </Route>
                          <Route exact path='/dynamic/academic-yearCrud'>
                            {({ match }) => <CrudTabel match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/message-type-table'>
                            {({ match }) => <MessageTypeTable match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/subject/grade/mapping'>
                            {({ match }) => <Subjectgrade match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/chapter-type-table'>
                            {({ match }) => <ChapterTypeTable match={match} />}
                          </Route>
                          <Route exact path='/subject/grade'>
                            {({ match }) => <ListandFilter match={match} />}
                          </Route>
                          <Route exact path='/homework/homework-card'>
                            {({ match }) => <HomeworkCard match={match} />}
                          </Route>
                          <Route exact path='/homework/student'>
                            {({ match }) => <StudentHomework match={match} />}
                          </Route>
                          <Route exact path='/homework/teacher'>
                            {({ match }) => <TeacherHomework match={match} />}
                          </Route>
                          <Route
                            exact
                            path='/homework/add/:year/:branch/:grade/:date/:subject/:id/:section_mapping'
                          >
                            {({ match }) => <AddHomework match={match} />}
                          </Route>
                          <Route exact path='/homework/admin'>
                            {({ match }) => <HomeworkAdmin match={match} />}
                          </Route>
                          <Route exact path='/homework/coordinator'>
                            {({ match }) => <CoordinatorHomework match={match} />}
                          </Route>
                          <Route path='/erp-online-class/class-work/:param1/:param2/:param3'>
                            {({ match, location }) => (
                              <ClassWork match={match} location={location} />
                            )}
                          </Route>
                          <Route exact path='/classwork-report-teacher-view'>
                            {({ match }) => <ClassWorkTeacherReport match={match} />}
                          </Route>
                          <Route exact path='/classwork/student-report'>
                            {({ match }) => <StudentClassWorkReport match={match} />}
                          </Route>
                          <Route
                            exact
                            path='/homework/cadd/:year/:branch/:grade/:date/:subject/:id/:coord_selected_teacher_id/:section_mapping'
                          >
                            {({ match }) => <AddHomeworkCoord match={match} />}
                          </Route>
                          <Route exact path='/lesson-plan/teacher-view'>
                            {({ match }) => <LessonPlan match={match} />}
                          </Route>
                          <Route exact path='/lesson-plan/student-view'>
                            {({ match }) => <LessonPlan match={match} />}
                          </Route>
                          <Route exact path='/lesson-plan/report'>
                            {({ match }) => <LessonReport match={match} />}
                          </Route>
                          <Route exact path='/lesson-plan/graph-report'>
                            {({ match }) => <LessonPlanGraphReport match={match} />}
                          </Route>
                          <Route exact path='/discussion-forum'>
                            {({ match }) => <Discussionforum match={match} />}
                          </Route>
                          <Route exact path='/discussion-forum/post/:id'>
                            {({ match }) => <DiscussionPost match={match} />}
                          </Route>
                          <Route exact path='/category/create'>
                            {({ match }) => <CreateCategory match={match} />}
                          </Route>
                          <Route exact path='/discussion-forum/create'>
                            {({ match }) => <CreateDiscussionForum match={match} />}
                          </Route>
                          <Route exact path='/circular'>
                            {({ match }) => <CircularList match={match} />}
                          </Route>
                          <Route exact path='/create-circular'>
                            {({ match }) => <CreateCircular match={match} />}
                          </Route>
                          <Route exact path='/general-dairy'>
                            {({ match }) => <GeneralDairyList match={match} />}
                          </Route>
                          <Route exact path='/diary/student'>
                            {({ match }) => <GeneralDairyList match={match} />}
                          </Route>
                          <Route exact path='/diary/teacher'>
                            {({ match }) => <GeneralDairyList match={match} />}
                          </Route>
                          <Route exact path='/general-dairy/student-view'>
                            {({ match }) => <GeneralDairyStudentList match={match} />}
                          </Route>
                          <Route exact path='/create/general-diary'>
                            {({ match }) => <CreateGeneralDairy match={match} />}
                          </Route>
                          <Route exact path='/daily-dairy'>
                            {({ match }) => <DailyDairyList match={match} />}
                          </Route>
                          <Route exact path='/create/daily-diary'>
                            {({ match }) => <CreateDailyDairy match={match} />}
                          </Route>
                          <Route
                            exact
                            path='/course-price/:courseKey?/:gradeKey?'
                            component={CoursePrice}
                          />
                          <Route exact path='/create/course/:courseKey?/:gradeKey?'>
                            {({ match }) => <CreateCourse match={match} />}
                          </Route>
                          <Route exact path='/import/course/'>
                            {({ match }) => <ImportCourse match={match} />}
                          </Route>
                          <Route exact path='/course-list/:gradeKey?'>
                            {({ match }) => <CourseView match={match} />}
                          </Route>
                          <Route exact path='/view-period/:id?'>
                            {({ match }) => <ViewCourseCard match={match} />}
                          </Route>
                          <Route exact path='/assessment/comparision'>
                            {({ match }) => <AssessmentComparisionUI match={match} />}
                          </Route>
                          <Route exact path='/assessment/:assessmentId/analysis'>
                            {({ match }) => <AssessmentAnalysis match={match} />}
                          </Route>
                          <Route exact path='/aol-attendance-list/:id?'>
                            {({ match }) => <AttendeeListRemake match={match} />}
                          </Route>
                          <Route exact path='/assessment/'>
                            {({ match }) => <ViewAssessments match={match} />}
                          </Route>
                          <Route exact path='/assessment/:assessmentId/attempt'>
                            {({ match }) => <AssessmentAttemption match={match} />}
                          </Route>
                          <Route exact path='/student-strength'>
                            {({ match }) => <StudentStrength match={match} />}
                          </Route>
                          <Route exact path='/student-id-card'>
                            {({ match }) => <StudentIdCard match={match} />}
                          </Route>
                          <Route exact path='/master-mgmt/signature-upload'>
                            {({ match }) => <SignatureUpload match={match} />}
                          </Route>
                          <Route exact path='/online-class/attend-class'>
                            {({ match }) => <TeacherBatchView match={match} />}
                          </Route>
                          <Route exact path='/online-class/teacher-view-class'>
                            {({ match }) => <TeacherBatchView match={match} />}
                          </Route>
                          <Route exact path='/aol-reshuffle/:id?'>
                            {({ match }) => <Reshuffle match={match} />}
                          </Route>
                          <Route exact path='/erp-online-class'>
                            {({ match }) => <ErpAdminViewClass match={match} />}
                          </Route>
                          <Route exact path='/erp-online-class-teacher-view'>
                            {({ match }) => <ErpAdminViewClass match={match} />}
                          </Route>
                          <Route exact path='/erp-online-class-student-view'>
                            {({ match }) => <ErpAdminViewClass match={match} />}
                          </Route>
                          <Route exact path='/erp-online-resources'>
                            {({ match }) => <OnlineClassResource match={match} />}
                          </Route>
                          <Route exact path='/mysparklebox-sales'>
                            {({ match }) => <SalesModule match={match} />}
                          </Route>
                          <Route exact path='/sparklebox.school-sales'>
                            {({ match }) => <SalesModule match={match} />}
                          </Route>
                          <Route exact path='/plufo-sales'>
                            {({ match }) => <SalesModule match={match} />}
                          </Route>
                          <Route exact path='/aol-custom-batch/:user_id/:student_id'>
                            {({ match }) => <CreateCustomeBatch match={match} />}
                          </Route>
                          <Route exact path='/aol-view-coupons'>
                            {({ match }) => <CouponCodeGenerator match={match} />}
                          </Route>
                          <Route exact path='/aol-add-edit-coupon-code'>
                            {({ match }) => <AddEditCoupon match={match} />}
                          </Route>
                          <Route exact path='/testimonial'>
                            {({ match }) => <Testimonial match={match} />}
                          </Route>
                          <Route exact path='/testimonial-upload'>
                            {({ match }) => <TestimonialUpload match={match} />}
                          </Route>

                          <Route exact path='/aol-blogs/view'>
                            {({ match }) => <AolBlogs match={match} />}
                          </Route>
                          <Route exact path='/aol-blogs/view/:status'>
                            {({ match }) => <AolBlogs match={match} />}
                          </Route>
                          <Route exact path='/aol-blog/creation'>
                            {({ match }) => <CreateUpdateAolBlog match={match} />}
                          </Route>
                          <Route exact path='/aol-blog/update'>
                            {({ match }) => <CreateUpdateAolBlog match={match} />}
                          </Route>
                          <Route exact path='/aol-student/batch/:userId'>
                            {({ match }) => <StudentBatchView match={match} />}
                          </Route>
                          <Route exact path='/aol-batch-reference'>
                            {({ match }) => <AolBatchReference match={match} />}
                          </Route>
                          <Route exact path='/aol-batch-create'>
                            {({ match }) => <AolCreateBatch match={match} />}
                          </Route>
                          <Route exact path='/student-batch-timeslot'>
                            {({ match }) => <StudentBatchTimeslot match={match} />}
                          </Route>
                          <Route exact path='/aol-batch-assign'>
                            {({ match }) => <StudentAssignBatch match={match} />}
                          </Route>
                          <Route exact path='/aol-batch-pause-new-batch-assign'>
                            {({ match }) => <BatchPauseAssignNewBatch match={match} />}
                          </Route>
                          <Route exact path='/aol-bundle-course-create'>
                            {({ match }) => <AolBundleCourseCreation match={match} />}
                          </Route>
                          <Route exact path='/aol-bundle-course-view'>
                            {({ match }) => <ViewBundleCourses match={match} />}
                          </Route>
                          <Route exact path='/aol-bundle-discount-reference'>
                            {({ match }) => <AolBundleDisconnect match={match} />}
                          </Route>
                          <Route exact path='/aol-enroll-student'>
                            {({ match }) => <StudentEnroll match={match} />}
                          </Route>
                          <Route exact path='/aol-student-desk'>
                            {({ match }) => <StudentDesk match={match} />}
                          </Route>
                          <Route exact path='/exampaol-student-desk'>
                            {({ match }) => <ExampleStudents match={match} />}
                          </Route>
                          <Route exact path='/exampleaol-student-desk'>
                            {({ match }) => <StudentDeskExample match={match} />}
                          </Route>
                          <Route exact path='/aol-student-batch-pause-history'>
                            {({ match }) => <BatchPauseHistory match={match} />}
                          </Route>
                          <Route exact path='/aol-student-enroll-batch'>
                            {({ match }) => <StudentBatchEnroll match={match} />}
                          </Route>
                          <Route exact path='/aol-student-enroll-batch-change'>
                            {({ match }) => <StudentBatchChange match={match} />}
                          </Route>
                          <Route exact path='/aol-un-assigned-batches'>
                            {({ match }) => <AolUnAssignedBatch match={match} />}
                          </Route>
                          <Route exact path='/aol-teachers'>
                            {({ match }) => <AolTeacherList match={match} />}
                          </Route>
                          <Route exact path='/aol-teachers/:id'>
                            {({ match }) => <TeacherAllBatchesList match={match} />}
                          </Route>
                          <Route exact path='/mysparklebox-sales-payment-list'>
                            {({ match }) => <AOLSalesPaymentList match={match} />}
                          </Route>
                          <Route exact path='/mysparklebox-sales-pending-batches'>
                            {({ match }) => <AolSalesPendingBatches match={match} />}
                          </Route>
                          <Route exact path='/sparklebox.school-sales-payment-list'>
                            {({ match }) => <AOLSalesPaymentList match={match} />}
                          </Route>
                          <Route exact path='/sparklebox.school-sales-pending-batches'>
                            {({ match }) => <AolSalesPendingBatches match={match} />}
                          </Route>
                          <Route exact path='/plufo-sales-payment-list'>
                            {({ match }) => <AOLSalesPaymentList match={match} />}
                          </Route>
                          <Route exact path='/plufo-sales-pending-batches'>
                            {({ match }) => <AolSalesPendingBatches match={match} />}
                          </Route>
                          <Route exact path='/aol-student-attendence'>
                            {({ match }) => <StudentAttendence match={match} />}
                          </Route>
                          <Route exact path='/aol-view-zoom'>
                            {({ match }) => <ZoomList match={match} />}
                          </Route>
                          <Route exact path='/certificate'>
                            {({ match }) => <Certificate match={match} />}
                          </Route>
                          <Route exact path='/aol-add-edit-zoom-id'>
                            {({ match }) => <AddEditZoomId match={match} />}
                          </Route>
                          <Route exact path='/aol-view-batch'>
                            {({ match }) => <AolViewBatch match={match} />}
                          </Route>
                          <Route exact path='/aol-view-batch-details/:id'>
                            {({ match }) => <ViewBatchDetails match={match} />}
                          </Route>
                          <Route exact path='/aol-view-batch-zoom-details/:id'>
                            {({ match }) => <ViewBatchZoomDetails match={match} />}
                          </Route>
                          <Route exact path='/aol-view-student-batch-zoom-details/:id'>
                            {({ match }) => <ViewStudentBatchDetails match={match} />}
                          </Route>
                          <Route exact path='/connection-pod'>
                            {({ match }) => <ConnectionPod match={match} />}
                          </Route>
                          <Route exact path='/batch-extension'>
                            {({ match }) => <BatchExtension match={match} />}
                          </Route>
                          <Route exact path='/intelligent-textbook/view'>
                            {({ match }) => <AllBooksPage match={match} />}
                          </Route>
                          <Route
                            exact
                            path='/intelligent-book/:bookId/:bookUid/:localStorageName/:environment/:type'
                          >
                            {({ match }) => <ChapterBook match={match} />}
                          </Route>
                          <Route exact path='/weekly-report'>
                            {({ match }) => <WeeklyReport match={match} />}
                          </Route>
                          <Route exact path='/occupancy-report'>
                            {({ match }) => <OccupancyReport match={match} />}
                          </Route>
                        </Switch>
                      </DailyDairyStore>
                    </ViewStore>
                  </GeneralDairyStore>
                </CircularStore>
              </AttachmentPreviewer>
            </ThemeProvider>
          </OnlineclassViewProvider>
        </AlertNotificationProvider>
      </Router>
    </div>
  );
}

export default App;
