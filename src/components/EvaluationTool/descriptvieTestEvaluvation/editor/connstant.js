
export const marginStyle = {
  '1.5': '15% 0% 0% 25%',
  '3': '48% 0% 0% 100%',
  '4.5': '82% 0% 0% 176%',
  '6': '115% 0% 0% 250%'
}

export const marginStyleAngle = {
  '1.5': '10% 0% 0% 29%',
  '3': '55% 0% 0% 86%',
  '4.5': '95% 0% 0% 144%'
}

export const marginStyleAngleReverse = {
  '0.5': '0% 0% 0% -17%',
  '0.4': '0% 0% 0% -17%',
  '0.3': '0% 0% 0% -17%',
  '0.2': '0% 0% 0% -17%',
  '0.1': '0% 0% 0% -17%'
}

export const marginPDFStyle = {
  '1.5': '20% auto auto auto',
  '3': '66% auto auto 40%',
  '4.5': '133% 0% 0% 49%'
}

export const marginStyleBelowWindowSize = {
  '1.5': '25% auto auto auto',
  '3': '81% auto auto 40%',
  '4.5': '140% 0% 0% 100%',
  '6': '195% 0% 0% 143%'
}
