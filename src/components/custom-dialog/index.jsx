import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import { DialogContent, Divider } from '@material-ui/core';

const CustomDialog = ({
  handleClose,
  open,
  dialogTitle,
  maxWidth,
  children,
  fullScreen,
  stylesProps = {},
}) => {
  const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
      color: theme.palette.secondary.main,
    },
    title: {
      textAlign: 'left',
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant='h6' className={classes.title}>
          {children}
        </Typography>
        {onClose ? (
          <IconButton
            aria-label='close'
            className={classes.closeButton}
            onClick={onClose}
          >
            <CloseIcon color='primary' />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });
  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby='customized-dialog-title'
      open={open}
      maxWidth={maxWidth || 'sm'}
      fullWidth
      style={{ zIndex: '10000', ...stylesProps }}
      fullScreen={fullScreen}
    >
      <DialogTitle id='customized-dialog-title' onClose={handleClose}>
        {dialogTitle}
      </DialogTitle>
      <Divider />
      <DialogContent>{children}</DialogContent>
    </Dialog>
  );
};

export default CustomDialog;
