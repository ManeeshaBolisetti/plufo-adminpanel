import React from 'react';
import { Grid, Typography, SvgIcon } from '@material-ui/core';
import unfiltered from '../../assets/images/unfiltered.svg';

const CustomFiterImage = ({ label }) => {
  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12} style={{ textAlign: 'center', marginTop: '50px' }}>
        <SvgIcon
          component={() => (
            <img
              alt='crash'
              loading='lazy'
              style={{
                height: '160px',
                width: '290px',
                imageRendering: 'pixelated',
                objectFit: 'cover',
              }}
              src={unfiltered}
            />
          )}
        />
      </Grid>
      <Grid item md={12} xs={12}>
        <Typography style={{ textAlign: 'center' }}>{label}</Typography>
      </Grid>
    </Grid>
  );
};

export default CustomFiterImage;
