import React, { useContext, useState, useRef, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import MicNoneIcon from '@material-ui/icons/MicNone';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    borderRadius: '50px',
    border: '1px solid lightgray',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    height: 28,
    margin: 4,
  },
}));

const CustomSearchBar = ({ value, onChange, label, placeholder, setValue }) => {
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const [recording, setRecording] = useState(false);
  const inputRef = useRef();
  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const handleRecord = () => {
    if (navigator.userAgent.indexOf('Chrome') !== -1) {
      const webkitSpeechRecognition =
        window.SpeechRecognition || window.webkitSpeechRecognition;
      const recognition = new webkitSpeechRecognition();
      recognition.lang = 'en-GB';
      recognition.onresult = function (event) {
        setValue(event.results[0][0].transcript);
      };
      recognition.start();
      recognition.onstart = function () {
        setRecording(true);
      };
      recognition.onspeechend = function () {
        setRecording(false);
      };
    } else {
      setAlert('warning', 'Voice search will support only on Chrome');
    }
  };

  return (
    <Paper component='form' className={classes.root}>
      <IconButton className={classes.iconButton} aria-label='search'>
        <SearchIcon />
      </IconButton>
      <InputBase
        className={classes.input}
        onChange={onChange}
        value={value || ''}
        label={label || ''}
        type='search'
        ref={inputRef}
        placeholder={placeholder}
        onKeyDown={(e) => (e.keyCode === 13 ? e.preventDefault() : '')}
        inputProps={{ 'aria-label': 'search google maps' }}
      />
      <Divider className={classes.divider} orientation='vertical' />
      <IconButton
        onClick={handleRecord}
        color='primary'
        className={classes.iconButton}
        aria-label='directions'
      >
        {recording ? (
          <RecordVoiceOverIcon color='primary' />
        ) : (
          <MicNoneIcon color='secondary' />
        )}
      </IconButton>
    </Paper>
  );
};

export default CustomSearchBar;
