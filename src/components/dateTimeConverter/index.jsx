export function ConverTime(time) {
  let hour = time?.split(':')[0];
  let min = time?.split(':')[1];
  const part = hour >= 12 ? 'PM' : 'AM';
  min = `${min}`.length === 1 ? `0${min}` : min;
  hour = hour > 12 ? hour - 12 : hour;
  hour = `${hour}`.length === 1 ? `0${hour}` : hour;
  return `${hour}:${min} ${part}`;
}

export function DateTimeConverter(DT) {
  const dateTime = DT?.split('T');
  const data = dateTime?.[0]
    ? new Date(dateTime?.[0]).toString().split('G')[0].substring(0, 16)
    : '';
  return `${data} ${dateTime?.[1] ? ConverTime(dateTime?.[1]) : ''}`;
}

export function addCommas(data) {
  const amount = JSON.stringify(data);
  const x = amount.split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? `.${x[1]}` : '';
  const rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    // eslint-disable-next-line no-useless-concat
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}
