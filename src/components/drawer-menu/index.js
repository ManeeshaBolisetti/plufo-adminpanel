import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import MenuItem from './menu-item';
import SuperUserMenu from './super-user-menu';
import './styles.scss';
import endpoints from 'config/endpoints';
import axiosInstance from 'config/axios';
import moment from 'moment';
// import { set } from 'lodash';

const resolveMenu = (url) => {
  if (url.includes('user-management')) return 'User Management';
  if (url.includes('lesson-plan')) return 'Lesson Plan';
  if (url.includes('master-mgmt')) return 'Master Management';
  if (url.includes('online-class')) return 'Online Class';
  if (url.includes('communication')) return 'Communication';
  if (url.includes('homework')) return 'Homework';
  if (url.includes('blog')) return 'Blogs';
  if (url.includes('diary')) return 'Diary';
  if (url.includes('plufo-sales')) return 'Plufo Sales';
  if (url.includes('aol-blogs')) return 'Plufo Blogs';
  if (url.includes('aol-view-coupons')) return 'Coupon';
  if (url.includes('aol-view-zoom')) return 'Zoom';
  if (url.includes('classwork')) return 'Online Class';
  if (url.includes('homework')) return 'Homework';
  if (url.includes('sales')) return 'Sales';
  if (url.includes('mysparklebox-blogs')) return 'MySparkleBox Blogs';
  return null;
};

const DrawerMenu = ({ navigationItems, superUser, onClick, roleDetails }) => {
  const [openMenu, setOpenMenu] = useState(null);
  const [unAssignedStudentCount, setUnAssignedStudentCount] = useState('')
  const [unAssignedBatchCount, setUnAssignedBatchCount] = useState('')
  const [reshuffleBatchCount, setReshuffleBatchCount] = useState('')
  const { location } = useHistory();
  useEffect(() => {
    setOpenMenu(resolveMenu(location.pathname));
  }, []);
  const { role_details } = JSON.parse(localStorage.getItem('userDetails')) || {};

  useEffect(() => {
      getUnassignedStudentCount()
      getUnassignedBatchCount()
      getReshuffleBatchCount()
    // setInterval(() => {
    //   getUnassignedStudentCount()
    //   getUnassignedBatchCount()
    //   getReshuffleBatchCount()
    // }, 60000)
  },[])

  const getUnassignedStudentCount = async () => {
    try {
      const { data } = await axiosInstance.get(
        `${
          endpoints.aolBatch.getUnAssighedStudentCount
        }`
      );
      if (data?.unassigned_students_count) {
        setUnAssignedStudentCount(data.unassigned_students_count);
      } else {
        console.log('error')
      }
    } catch (error) {
      console.log(error)
    }
  }

  const getUnassignedBatchCount = async () => {
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.aolBatch.getUnAssighedBatchCount
        }`
      );
      if (data?.unassigned_batches_count) {
        setUnAssignedBatchCount(data.unassigned_batches_count);
      } else {
        console.log('error')
      }
    } catch (error) {
      console.log(error)
    }
  }

  const getReshuffleBatchCount = async () => {
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.aolBatch.getReshuffleBatchCount
        }?start_date=${moment(new Date().setDate(new Date().getDate() - 1)).format('yyyy-MM-DD')}&end_date=${moment(new Date()).format('yyyy-MM-DD')}`
      );
      if (data?.reshuffle_batch_count) {
        setReshuffleBatchCount(data.reshuffle_batch_count);
      } else {
        console.log('error')
      }
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <>
      {superUser && (
        <SuperUserMenu
          onClickMenuItem={onClick}
          openMenu={openMenu}
          onChangeMenuState={(menu) => {
            if (menu === openMenu) {
              setOpenMenu(null);
            } else {
              setOpenMenu(menu);
            }
          }}
        />
      )}

      {/* {superUser === true ? (
        <SuperUserMenu
          onClickMenuItem={onClick}
          openMenu={openMenu}
          onChangeMenuState={(menu) => {
            if (menu === openMenu) {
              setOpenMenu(null);
            } else {
              setOpenMenu(menu);
            }
          }}
        />
        ) : roleDetails?.role_id !== 1 && roleDetails?.role_id !== 2 ? (
          <SuperUserMenu
            onClickMenuItem={onClick}
            openMenu={openMenu}
            onChangeMenuState={(menu) => {
              if (menu === openMenu) {
                setOpenMenu(null);
              } else {
                setOpenMenu(menu);
              }
            }}
          />
          ) : ""} */}

      {navigationItems &&
        navigationItems
          .filter((item) => item.child_module && item.child_module.length > 0)
          .map((item) => (
            <MenuItem
              item={item}
              menuOpen={item.parent_modules === openMenu}
              onChangeMenuState={() => {
                if (item.parent_modules === openMenu) {
                  setOpenMenu(null);
                } else {
                  setOpenMenu(item.parent_modules);
                }
              }}
              onClick={onClick}
              unAssignedStudentCount={unAssignedStudentCount}
              unAssignedBatchCount={unAssignedBatchCount}
              reshuffleBatchCount={reshuffleBatchCount}
            />
          ))}
    </>
  );
};

export default DrawerMenu;
