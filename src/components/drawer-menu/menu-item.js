/* eslint-disable no-debugger */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import useStyles from './useStyles';
import menuIcon from './menu-icon';
import { Badge, withStyles } from '@material-ui/core';

const StyledBadge = withStyles((theme) => ({
  badge: {
    right: 20,
    top: 0,
    backgroundColor: 'black',
    color: 'white',
    padding: '0 4px',
  },
}))(Badge);

const MenuItem = withRouter(({ history, ...props }) => {
  const { item, onClick, onChangeMenuState, menuOpen } = props || {};
  const [selectedIndex, setSelectedIndex] = useState(null);
  const menuSelectionArray = [
    { name: 'Take Class', Path: '/take-class' },
    // { name: 'View Class', Path: '/online-class/view-class' },
    // { name: 'Attend Online Class', Path: '/online-class/attend-class' },
    // { name: 'Teacher View Class', Path: '/online-class/teacher-view-class' },
    { name: 'Role Management', Path: '/role-management' },
    { name: 'DashBoard', Path: '/dashboard' },
    { name: 'View Role', Path: '/role-management' },
    { name: 'Master Management', Path: '/master-mgmt' },
    { name: 'Subject', Path: '/master-mgmt/subject-table' },
    { name: 'Section', Path: '/master-mgmt/section-table' },
    { name: 'Grade', Path: '/master-mgmt/grade-table' },
    { name: 'Academic Year', Path: '/master-mgmt/academic-year-table' },
    { name: 'Chapter Creation', Path: '/master-mgmt/chapter-type-table' },
    { name: 'Message Type', Path: '/master-mgmt/message-type-table' },
    { name: 'Signature Upload', Path: '/master-mgmt/signature-upload' },
    { name: 'Course', Path: '/course-list' },
    { name: 'Course Price', Path: '/course-price' },
    { name: 'Bundle Discount', Path: '/aol-bundle-discount-reference' },
    { name: 'Bundle Course Creation', Path: '/aol-bundle-course-create' },
    { name: 'Bundle Course', Path: '/aol-bundle-course-view' },
    { name: 'Unassigned Students', Path: '/aol-batch-create' },
    { name: 'Batch Time Slot', Path: '/aol-batch-reference' },
    { name: 'All Students', path: '/aol-enroll-student' },
    { name: 'Reshuffle Requests', path: '/aol-student-enroll-batch' },
    { name: 'Unassigned Batches', path: '/aol-un-assigned-batches' },
    { name: 'Teacher List', path: '/aol-teachers' },
    { name: 'Payment List', path: '/plufo-sales-payment-list' },
    { name: 'Student Pending Details', Path: '/plufo-sales-pending-batches' },
    { name: 'Payment List', path: '/mysparklebox-sales-payment-list' },
    { name: 'Student Pending Details', Path: '/mysparklebox-sales-pending-batches' },
    { name: 'Payment List', path: '/sparklebox.school-sales-payment-list' },
    { name: 'Student Pending Details', Path: '/sparklebox.school-sales-pending-batches' },
    { name: 'Attendance', Path: '/aol-student-attendence' },
    { name: 'Lesson Plan Mapping', Path: '/subject/grade' },
    { name: 'Create User', Path: '/user-management/create-user' },
    { name: 'View User', Path: '/user-management/view-users' },
    { name: 'Bulk Upload Status', Path: '/user-management/bulk-upload' },
    { name: 'Assign Role', Path: '/user-management/assign-role' },
    { name: 'View Class', Path: '/erp-online-class' },
    { name: 'Attend Online Class', Path: '/online-class/attend-class' },
    { name: 'Teacher View Class', Path: '/online-class/teacher-view-class' },
    { name: 'Create Class', Path: '/online-class/create-class' },
    { name: 'Online Class', Path: '/online-class/attend-class' },
    { name: 'Management View', Path: '/homework/coordinator' },
    { name: 'Configuration', Path: '/homework/admin' },
    { name: 'Student Homework', Path: '/homework/student' },
    { name: 'Teacher Homework', Path: '/homework/teacher' },
    { name: 'Teacher Classwork Report', Path: '/classwork-report-teacher-view' },
    { name: 'Student Classwork Report', Path: '/classwork/student-report' },
    {
      name: 'Teacher Classwork Report',
      Path: '/classwork-report-teacher-view',
    },
    { name: 'Assessment', Path: '/assessment' },
    { name: 'Communication', Path: '/communication' },
    { name: 'Add Group', Path: '/communication/addgroup' },
    { name: 'View & Edit Group', Path: '/communication/viewgroup' },
    { name: 'Send Message', Path: '/communication/sendmessage' },
    { name: 'Add SMS Credit', Path: '/communication/smscredit' },
    { name: 'SMS & Email Log', Path: '/communication/messageLog' },
    { name: 'Teacher View', Path: '/lesson-plan/teacher-view' },
    { name: 'Student View', Path: '/lesson-plan/student-view' },
    { name: 'Management Report', Path: '/lesson-plan/report' },
    { name: 'Graphical Report', Path: '/lesson-plan/graph-report' },
    { name: 'Student Blogs', Path: '/blog/student/dashboard' },
    { name: 'Teacher Blogs', Path: '/blog/teacher' },
    { name: 'Management Blogs', Path: '/blog/admin' },
    { name: 'Principal Blogs', Path: '/blog/principal' },
    { name: 'Genre', Path: '/blog/genre' },
    { name: 'Word Count Cofiguration', Path: '/blog/wordcount-config' },
    { name: 'ID Cards', Path: '/student-id-card' },
    { name: 'Student Strength', Path: '/student-strength' },
    { name: 'Teacher Circular', Path: '/circular' },
    { name: 'Student Circular', Path: '/circular' },
    { name: 'Enroll User', Path: '/plufo-sales' },
    { name: 'Enroll User', Path: '/mysparklebox-sales' },
    { name: 'Enroll User', Path: '/sparklebox.school-sales' },
    { name: 'Create Coupon', Path: '/aol-view-coupons' },
    { name: 'View Blogs', Path: '/aol-blogs/view/1' },
    { name: 'Testimonial Videos', Path: '/testimonial' },
    { name: 'Zoom', Path: '/aol-view-zoom' },
    { name: 'Student Desk', Path: '/aol-student-desk' },
    { name: 'Batch Pause History', Path: '/aol-student-batch-pause-history' },
    { name: 'IBook', Path: '/intelligent-textbook' },
    // { name: 'Certificate', Path: '/certificate'},
    { name: 'Course Completion Certificate', Path: '/certificate' },
    { name: 'Occupancy Report', Path: '/occupancy-report' },
    { name: 'Academic Year', Path: '/dynamic/academic-year' },

  ];
  // const [menuOpen, setMenuOpen] = useState(false);
  useEffect(() => {
    menuSelectionArray.forEach((items, index) => {
      if (items.Path === history.location.pathname) {
        setSelectedIndex(index);
      }
    });
  }, [history.location.pathname]);

  const classes = useStyles();
  return (
    <>
      <ListItem
        button
        onClick={() => {
          if (item.child_module.length > 0) {
            onChangeMenuState();
          } else {
            onClick(item.parent_modules);
          }
        }}
      >
        <ListItemIcon className={classes.menuItemIcon}>
          {/* <MenuIcon name={item.parent_modules} /> */}
          {menuIcon(item.parent_modules)}
        </ListItemIcon>
        <ListItemText primary={item.parent_modules} className='menu-item-text' />
        {item.child_module && item.child_module.length > 0 ? (
          menuOpen ? (
            <ExpandLess className={classes.expandIcons} />
          ) : (
            <ExpandMore className={classes.expandIcons} />
          )
        ) : (
          ''
        )}
      </ListItem>
      {item.child_module && item.child_module.length > 0 && (
        <Collapse in={menuOpen}>
          <Divider />
          <List>
            {item.child_module.map((child) => (
              <ListItem
                button
                className={
                  selectedIndex &&
                    child.child_name === menuSelectionArray[selectedIndex].name
                    ? 'menu_selection'
                    : null
                }
                onClick={() => {
                  onClick(child.child_name);
                }}
              >
                <ListItemIcon className={classes.menuItemIcon}>
                  {/* <MenuIcon name={child.child_name} /> */}
                  {/* {menuIcon(child.child_name)} */}
                </ListItemIcon>
                <StyledBadge
                  badgeContent={
                    child.child_name === 'Unassigned Students' &&
                      props.unAssignedStudentCount
                      ? props.unAssignedStudentCount
                      : child.child_name === 'Unassigned Batches' &&
                        props.unAssignedBatchCount
                        ? props.unAssignedBatchCount
                        : child.child_name === 'Reshuffle Requests' &&
                          props.reshuffleBatchCount
                          ? props.reshuffleBatchCount
                          : 0
                  }
                  max={999}
                >
                  <h6>&nbsp;</h6>
                </StyledBadge>
                <ListItemText primary={child.child_name} className='menu-item-text' />
              </ListItem>
            ))}
          </List>
        </Collapse>
      )}
    </>
  );
});

export default MenuItem;
