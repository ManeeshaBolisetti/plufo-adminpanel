import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  menuItemIcon: {
    color: '#ffffff',
  },
  expandIcons: {
    marginLeft: '2rem',
    color: '#ffffff',
  },
}));

export default useStyles;
