import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

const ArrowForwardIcon = () => {
  return (
    <SvgIcon viewBox='0 0 9.049 15.716'>
      <g transform='translate(-7.932 22.141) rotate(-90)' fill='#ff6b6b'>
        <g transform='translate(6.425 7.932)'>
          <rect
            className='a'
            width='1.828'
            height='10.969'
            rx='0.914'
            transform='translate(0 1.293) rotate(-45)'
          />
          <rect
            className='a'
            width='1.828'
            height='10.969'
            rx='0.914'
            transform='translate(7.959 9.049) rotate(-135)'
          />
          />
        </g>
      </g>
    </SvgIcon>
  );
};

export default ArrowForwardIcon;
