import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  imageUploadBtn: {
    backgroundColor: '#ff6b6b',
    padding: '15px',
    fontSize: '16px',
    color: '#ffffff',
    borderRadius: '5px',
    cursor: 'pointer',
  },
}));

export default useStyles;
