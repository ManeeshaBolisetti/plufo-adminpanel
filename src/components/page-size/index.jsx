import React, { useState } from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

const PageSize = () => {
  const [pageCount, setPageCount] = useState('');

  const handleChange = (event) => {
    setPageCount(event.target.value);
  };
  return (
    <div>
      <FormControl fullWidth margin='dense' variant='outlined'>
        <InputLabel>Page Size</InputLabel>
        <Select
          value={pageCount}
          onChange={handleChange}
          // value={filterInput.meetingTypeFilter || ''}
          label='Meeting Type'
          name='meetingTypeFilter'
          // onChange={(e) => {
          //   setFilterValue(e);
          // }}
        >
          <MenuItem value={'ten'}>10</MenuItem>
          <MenuItem value={'twentyFive'}>25</MenuItem>
          <MenuItem value={'fifty'}>50</MenuItem>
          <MenuItem value={'hundred'}>100</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
};

export default PageSize;
