import React from 'react';

let displayName = '';

if (window.location.hostname === 'dev.erp.mysparklebox.com' || window.location.hostname === 'erp.mysparklebox.com') {
    displayName = 'My SparkleBox';
}
else if (window.location.hostname === 'dev.erp.sparklebox.school' || window.location.hostname === 'dev.erp.sparklebox.school') {
    displayName = 'SparkleBox School';
}
else if (window.location.hostname === 'localhost') {
    displayName = 'My SparkleBox/SparkleBox School/Plufo'
}
else {
    displayName = 'Plufo'
}


export default displayName;