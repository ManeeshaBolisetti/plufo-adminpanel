import React, { useState, useContext, useEffect } from 'react';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button, Grid, TableFooter, TablePagination } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import Filteration from './filteration';
import { Pagination } from '@material-ui/lab';
import GenerateModal from './GenerateModal';
import CustomFiterImage from '../../components/custom-filter-image';
import moment from 'moment';

const useStyles = makeStyles({
  table: {
    minWidth: '100%',
  },
});

const Certificate = () => {
  const classes = useStyles();

  const [loading, setLoading] = useState(false);
  const [open, setOpen] = React.useState(false);

  const [name, setName] = useState([]);
  const [nameApi, setNameApi] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [index, setIndex] = useState(1);
  const [studentId, setStudentId] = useState('');
  const [batchId, setBatchId] = useState('');
  const [filterState, setFilterState] = useState({
    search: '',
  });
  const [page, setPage] = useState(1);

  useEffect(() => {
    setPage(1);
    getStudentsList(
      1,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }, []);
  async function getStudentsList(pageNo, searchVal, grad, sub, startDate, endDate) {
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = grad?.length ? `&grades=${grad}` : '';
    const selectedSubject = sub ? `&subject_id=${sub}` : '';
    const start = startDate ? `&start_date=${startDate}` : '';
    const end = endDate ? `&end_date=${endDate}` : '';
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${
          endpoints.aolBatchAssign.getStudentList
        }?page=${pageNo}&page_size=${10}${searchValue}${selectedGrade}${selectedSubject}${start}${end}`
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setStudentBatchList(data);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  function handlePagination(event, page) {
    event.preventDefault();
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * 10);
    }
    getStudentsList(
      page,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }

  const generateDisable = (pause_date, end_date, certificate) => {
    if (certificate === null) {
      if (pause_date !== null) {
        if (
          moment(moment(pause_date).toArray()).diff(
            moment(moment(end_date).toArray()),
            'days'
          ) >= 0
        ) {
          return false;
        } else return true;
      } else return true;
    } else return true;
  };

  return (
    <>
      {loading ? <Loading /> : ''}
      <Layout>
        <div style={{ width: '95%', margin: '20px 20px 20px 20px' }}>
          <CommonBreadcrumbs componentName='Generate Certificate' />
        </div>
        <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
          <Grid item md={12} xs={12}>
            <Filteration
              setPage={setPage}
              getStudentsList={getStudentsList}
              filterState={filterState}
              setFilterState={setFilterState}
              setLoading={setLoading}
            />
          </Grid>
        </Grid>

        <>
          <TableContainer component={Paper} style={{ margin: '20px', width: '97%' }}>
            <Table className={classes.table} aria-label='simple table'>
              <TableHead>
                <TableRow>
                  {/* <TableCell align='left'>S.No</TableCell> */}
                  <TableCell align='right'>Name</TableCell>

                  <TableCell align='right'>Batch&nbsp;Name</TableCell>
                  <TableCell align='right'>Course&nbsp;Name</TableCell>
                  <TableCell align='right'>Attendance</TableCell>
                  <TableCell align='right'>Start&nbsp;Date</TableCell>
                  <TableCell align='right'>End&nbsp;Date</TableCell>
                  <TableCell align='right'>Pause&nbsp;Date</TableCell>
                  <TableCell align='right'>Certificate</TableCell>
                  <TableCell align='right'>Download</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {studentBatchList?.result?.length ? (
                  <>
                    {studentBatchList?.result.map((row, index) => (
                      <TableRow key={index}>
                        {/* {new Date(row.end_date).getTime() <= value.getTime() && ( */}
                        {row.name &&
                          row.batch_name &&
                          row.course_name &&
                          row.start_date &&
                          row.end_date && (
                            <>
                              {/* {row.id && (
                            <TableCell component='th' scope='row'>
                              {index}
                            </TableCell>
                          )} */}
                              <TableCell align='right'>{row.name}</TableCell>

                              <TableCell align='right'>{row.batch_name}</TableCell>
                              <TableCell align='right'>{row.course_name}</TableCell>
                              <TableCell align='right'>{row.attendance}</TableCell>

                              <TableCell align='right'>{row.start_date}</TableCell>
                              <TableCell align='right'>{row.end_date}</TableCell>
                              <TableCell align='right'>
                                {row?.pause_date ? row.pause_date : 'Null'}
                              </TableCell>
                              <TableCell align='right'>
                                <Button
                                  size='small'
                                  color='primary'
                                  variant='contained'
                                  disabled={generateDisable(
                                    row.pause_date,
                                    row.end_date,
                                    row.certificate
                                  )}
                                  onClick={() => {
                                    setOpen(true);
                                    setBatchId(row.aol_batch);
                                    setStudentId(row.student_id);
                                    console.log(
                                      moment(row.pause_date).toArray(),
                                      moment(row.end_date).toArray(),
                                      moment(moment(row.pause_date).toArray()).diff(
                                        moment(moment(row.end_date).toArray()),
                                        'days'
                                      )
                                    );
                                  }}
                                >
                                  Generate
                                </Button>
                              </TableCell>

                              <TableCell align='right'>
                                <form action={row.certificate}>
                                  <Button
                                    size='small'
                                    color='primary'
                                    variant='contained'
                                    disabled={row.certificate ? false : true}
                                    type='submit'
                                  >
                                    Download
                                  </Button>
                                </form>
                              </TableCell>
                            </>
                          )}
                        {/* )} */}
                      </TableRow>
                    ))}
                  </>
                ) : (
                  <TableRow>
                    <TableCell colSpan='9'>
                      <CustomFiterImage label='Batches Not Found' />
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableCell colSpan='9'>
                    <Pagination
                      style={{ textAlign: 'center', display: 'inline-flex' }}
                      onChange={handlePagination}
                      count={studentBatchList?.total_pages}
                      color='primary'
                      page={page}
                    />
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </>

        {open ? (
          <GenerateModal
            content={[studentId, batchId]}
            open={open}
            close={() => {
              setOpen(false);
              setBatchId('');
              setPage(page);
              getStudentsList(
                page,
                filterState?.search || '',
                filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                filterState?.subjectList?.id || '',
                filterState?.startDate || '',
                filterState?.endDate || ''
              );
              setStudentId('');
            }}
          />
        ) : (
          ''
        )}
      </Layout>
    </>
  );
};

export default Certificate;
