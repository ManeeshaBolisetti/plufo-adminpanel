import React, { useState, useContext } from 'react';
import { Grid, TextField, Button, Divider, Dialog } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import CustomDialog from '../../components/custom-dialog';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { KeyboardDatePicker } from '@material-ui/pickers';
import moment from 'moment';
import MomentUtils from '@date-io/moment';
import Loader from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';

const GenerateModal = ({ content, open, close }) => {
  const [loading, setLoading] = useState(false);
  const [selectedMenu, setSelectedMenu] = useState('');
  const [issueDate, setIssueDate] = useState(moment().format('YYYY-MM-DD'));
  const [score, setScore] = useState(null);
  const [studentId, setStudentId] = useState(content[0]);
  const [batchId, setBatchId] = useState(content[1]);

  const { setAlert } = useContext(AlertNotificationContext);
  const [open1, setOpen1] = React.useState(false);
  console.log('the content', content);
  const handleIssueDateChange = (event, value) => {
    // console.log(value);
    if (value) {
      setIssueDate(value);
      console.log(typeof value);
    } else {
      setIssueDate('');
    }
  };
  const handleScoreChange = (event) => {
    // console.log(studentId);
    // console.log(event.target.value);
    if (event) {
      setScore(event.target.value);
      console.log(typeof event.target.value);
    } else {
      setScore('');
    }
  };

  const handlecl = () => {
    setOpen1(close);
    setLoading(false);
  };
  const handleGenerate = () => {
    setLoading(true);
    const payload = {
      issue_date: `${issueDate}`,
      score: `${score}`,
      erp_id: `${studentId}`,
      batch_id: `${batchId}`,
    };
    if (score <= 100 && score >= 0) {
      axiosInstance
        .post(`${endpoints.certificate.certificateGenerate}`, payload)
        .then((result) => {
          if (result.status === 200) {
            setLoading(false);
            handlecl();
            setAlert('success', 'Successfully Certificate Generated');
          } else {
            setLoading(false);
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    } else {
      setLoading(false);
      setAlert('error', 'Enter correct Score Range (0-100)');
    }
  };

  return (
    <>
      {loading ? <Loader /> : ''}
      <Grid container spacing={2}>
        <Grid item md={12} xs={12}>
          <CustomDialog
            handleClose={close}
            open={open}
            dialogTitle='Generate Certificate'
            maxWidth='md'
            fullScreen={false}
            stylesProps={{ zIndex: '1', marginTop: 50 }}
          >
            <Grid container spacing={2}>
              <Grid item md={4}>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                  <KeyboardDatePicker
                    size='small'
                    variant='outlined'
                    format='YYYY-MM-DD'
                    margin='none'
                    id='date-picker'
                    label='Issue Date'
                    minDate={new Date()}
                    name='issue_date'
                    inputVariant='outlined'
                    value={issueDate}
                    onChange={handleIssueDateChange}
                    className='dropdownIcon'
                    KeyboardButtonProps={{
                      'aria-label': 'change date',
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item md={4}>
                <TextField
                  required
                  size='small'
                  variant='outlined'
                  label='Score'
                  type='number'
                  error={(score <= 100) & (score >= 0) ? false : true}
                  helperText='Enter Score 0 to 100.'
                  fullWidth
                  value={score}
                  onChange={handleScoreChange}
                  InputProps={{ inputProps: { min: 0, max: 100 } }}
                />
              </Grid>
              <Grid item md={12}></Grid>
            </Grid>

            <Grid>
              {' '}
              <Button
                onClick={() => {
                  handleGenerate();
                }}
                color='primary'
                autoFocus
              >
                Submit
              </Button>
            </Grid>
          </CustomDialog>
        </Grid>
      </Grid>
    </>
  );
};

export default GenerateModal;
