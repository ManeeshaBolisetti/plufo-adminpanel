import React,{ useState } from 'react';
import './style.scss';

const BatchSelect = (props) => {
    const { 
        setSelectedLimit,
     } = props;

    const [joinLimits, setJoinLimits] = useState([
        // { limit: '1:1', isSelected: true , index: 0 },
        { limit: '1:5', isSelected: true , index: 1 },
        { limit: '1:10', isSelected: false, index: 2 },
        // { limit: '1:20', isSelected: false , index: 3 },
        // { limit: '1:30', isSelected: false , index: 4 },
    ]);
    const handleClickJoinLimit = (index) => {
        console.log("index:",index);
        const list = [...joinLimits].map(value => value.isSelected ? { ...value, isSelected: false } : value)
        list[index]['isSelected'] = true;
        setJoinLimits(list);
        setSelectedLimit(list[index]['index']);
    };

    return (
        <div className="joinLimitWrapper">
            <div className="joinLimitTag">Join Limit</div>
            <div className="joinLimitContainer">
                {joinLimits.map((value, index) => (
                    <div className={value.isSelected ? "singleJoinLimitSelected" : "singleJoinLimit"} onClick={() => handleClickJoinLimit(index)}>{value.limit}</div>
                ))}
            </div>
        </div>
    )
}

export default BatchSelect;