import React, { useState, useEffect, useContext } from 'react';
import './style.scss';
import { Autocomplete } from '@material-ui/lab';
import {
  Grid,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableHead,
  SvgIcon,
  TextField,
  Typography,
} from '@material-ui/core';
import unfiltered from '../../assets/images/unfiltered.svg';
import TimeSlotList from './timeSlotList';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';

const CreateBatchTime = ({ records, refresh }) => {
  const [loading, setLoading] = useState(false);
  const [dataRecords, setDataRecords] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);

  function handleCallBack(list) {
    const newList = [];
    list.map((item) => newList.push(...TimeSlotList?.filter((i) => i.send === item)));
    return newList;
  }
  useEffect(() => {
    if (records?.length !== 0) {
      const newData = JSON.parse(JSON.stringify(records));
      for (let i = 0; i < newData.length; i += 1) {
        newData[i].time_slot_available = handleCallBack(newData[i].time_slot_available);
      }
      setDataRecords(newData);
    } else {
      setDataRecords([]);
    }
  }, [records]);

  async function handleUpdate(payload) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.put(
        endpoints.coursePrice.updateCourseDetailsApi,
        {
          ...payload,
        }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        const message = 'Course Time Slot added Successfully';
        setAlert('success', message);
        refresh();
      } else {
        setLoading(false);
        setAlert('error', data.description);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.description);
    }
  }

  function handleOnChange(value, key, index, id) {
    setDataRecords((data) => {
      const newData = [...data];
      switch (key) {
        case key:
          handleUpdate({
            course_detail_id: id,
            [key]: value?.map((item) => item.send) || [],
          });
          newData[index][key] = value;
          return newData;
        default:
          return null;
      }
    });
  }

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12} style={{ marginBottom: '15px' }}>
        <div className='daysTag'>View All Time Slots</div>
      </Grid>
      <Grid
        item
        md={12}
        xs={12}
        className='priceLimitContainer'
        style={{ marginBottom: '20px' }}
      >
        <Grid container spacing={2} style={{ textAlign: 'center', padding: '20px 0px' }}>
          {dataRecords?.length ? (
            <Table style={{ padding: '0px', margin: '0px' }}>
              <TableHead style={{ padding: '0px', margin: '0px' }}>
                <TableRow style={{ padding: '0px', margin: '0px', width: '100%' }}>
                  <TableCell
                    style={{ padding: '10px 0px', margin: '0px', width: '33.33%' }}
                  >
                    Days
                  </TableCell>
                  <TableCell
                    style={{ padding: '10px 0px', margin: '0px', width: '33.33%' }}
                  >
                    No. of Weeks
                  </TableCell>
                  <TableCell
                    style={{ padding: '10px 0px', margin: '0px', width: '33.33%' }}
                  >
                    TimeSlot
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody style={{ padding: '0px', margin: '0px' }}>
                {dataRecords?.length &&
                  dataRecords.map((item, index) => (
                    <TableRow
                      key={item.id}
                      style={{ padding: '0px', margin: '0px', width: '100%' }}
                    >
                      <TableCell
                        style={{ padding: '10px 0px', margin: '0px', width: '33.33%' }}
                      >
                        {item?.days?.[0]}
                      </TableCell>
                      <TableCell
                        style={{ padding: '10px 0px', margin: '0px', width: '33.33%' }}
                      >
                        {`${item?.no_of_week} Weeks`}
                      </TableCell>
                      <TableCell
                        style={{ padding: '10px 0px', margin: '0px', width: '33.33%' }}
                      >
                        <Autocomplete
                          size='small'
                          multiple
                          id='daysCombination'
                          className='dropdownIcon'
                          style={{ width: '95%' }}
                          options={TimeSlotList}
                          getOptionLabel={(option) => option?.value || ''}
                          filterSelectedOptions
                          value={item?.time_slot_available || []}
                          onChange={(event, value) => {
                            handleOnChange(value, 'time_slot_available', index, item?.id);
                          }}
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              variant='outlined'
                              label='Select Time Slots' 
                              placeholder='Select Time Slots'
                            />
                          )}
                        />
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          ) : (
            <>
              <Grid item md={12} xs={12}>
                <SvgIcon
                  component={() => (
                    <img
                      alt='Crash'
                      style={{
                        height: '160px',
                        width: '290px',
                        imageRendering: 'pixelated',
                      }}
                      src={unfiltered}
                    />
                  )}
                />
              </Grid>
              <Grid item md={12} xs={12}>
                <Typography style={{ textAlign: 'center' }}>
                  Time slots are not added
                </Typography>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Grid>
  );
};

export default CreateBatchTime;
