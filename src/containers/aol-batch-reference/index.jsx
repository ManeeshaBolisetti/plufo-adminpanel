import React, { useState, useContext, useEffect } from 'react';
import Layout from '../Layout';
import Loader from '../../components/loader/loader';
import Autocomplete from '@material-ui/lab/Autocomplete';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import { Grid, TextField ,Paper } from '@material-ui/core';
import BatchSelect from './batchSelect';
import CreateBatchTime from './createTimeSlot';

const AolBatchReference = () => {
    const [loading, setLoading] = useState(false);
    const { setAlert } = useContext(AlertNotificationContext);
    const [subjectList, setSubjectList] = useState([]);
    const [selectedBatch, setSelectedBatch] = useState(1);
    const [gradeList, setGradeList] = useState([]);
    const [courseList, setCourseList] = useState([]);
    const [selectedSubject, setSelectedSubject] = useState('');
    const [selectedGrade, setSelectedGrade] = useState([]);
    const [selectedCourse, setSelectedCourse] = useState('');
    const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
    const [moduleId, setModuleId] = useState('');
    const [timeList, setTimeList] = useState('');
    const [selectDropDown,setSelectDropDown] =useState([])
    const [branch,setBranch] = useState([]);
    const [branchId, setBranchId] = useState(null)
    const [gradeId,setGradeId] = useState(null);

    async function getDetailsApi(id) {
        setLoading(true);
        try {
          const { data } = await axiosInstance.get(`${endpoints.coursePrice.getCoursePriceApi}?course=${id}`)
          if(data?.status_code === 200) {
            setLoading(false);
            const Batch1 = [];
            const Batch5 = [];
            const Batch10 = [];
            const Batch20 = [];
            const Batch30 = [];
            if(data?.result?.length !== 0) {
              for(let i = 0; i < data?.result.length; i++) {
                const item = data?.result[i];
                if(item.batch_size === 1) {
                  Batch1.push(item);
                } else if(item.batch_size === 5) {
                  Batch5.push(item);
                } else if(item.batch_size === 10) {
                  Batch10.push(item);
                } else if(item.batch_size === 20) {
                  Batch20.push(item);
                } else if(item.batch_size === 30) {
                  Batch30.push(item);
                }
              }
            }
            setTimeList([[...Batch1],[...Batch5],[...Batch10],[...Batch20],[...Batch30]]);
          }
        } catch (error) {
          setLoading(false);
          setAlert('error', error.message);
        }
      }

    useEffect(()=> {
      if(selectedCourse) {
        getDetailsApi(selectedCourse?.id);
      }
    },[selectedCourse]);
  
    useEffect(() => {
      if (NavData && NavData.length) {
        NavData.forEach((item) => {
          if (
            item.parent_modules === 'Master Management' &&
            item.child_module &&
            item.child_module.length > 0
          ) {
            item.child_module.forEach((item) => {
              if (item.child_name === 'Course Price') {
                setModuleId(item.child_id);
              }
            });
          }
        });
      }
    }, []);
  
   async function ApiCall(url, type) {
     setLoading(true);
      try {
        const { data } = await axiosInstance.get(url);
        if(data?.status_code === 200 || data?.status_code === 201) {
          if(type === 'branch') {
            setSelectDropDown(data?.data)
          }
          else if(type === 'grade') {
            setGradeList(data?.data);
          } else if(type === 'subject'){
            setSubjectList(data?.result);
          }
          setLoading(false);
        } else {
          setLoading(false);
          setAlert('error', data.message);
        }
      } catch (error) {
        setAlert('error',error.message);
        setLoading(false);
      }
   }
  useEffect(() => {
    ApiCall(`${endpoints.studentDesk.branchSelect}`,'branch')
  }, [])
  
    useEffect(() => {
      if(moduleId, branchId) {
        // ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}&module_id=${moduleId}`, 'subject');
        ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}&module_id=${moduleId}`, 'grade');
      }
    }, [moduleId,branchId]);

    useEffect(() => {
      if(branchId && gradeId){
        ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}&module_id=${moduleId}&grades=${gradeId}`, 'subject');
      }
    }, [gradeId])
  
    const handleGrade = (event, value = []) => {
      setSelectedGrade([]);
      setCourseList([]);
      setSelectedCourse('');
      if (value && value?.length) {
        setSelectedGrade(value);
        setGradeId(value.map((item) => item.grade_id));
        // getCourseList(value.map(item => item.grade_id));
      }
    };
  
    const handleCourse = (event, value = '') => {
      setSelectedCourse('');
      if (value) {
        setSelectedCourse(value);
      }
    };
    useEffect(() => {
      if(selectedSubject?.id && gradeId)
     getCourseList()
    }, [selectedSubject?.id])
    const getCourseList = () => {
      setLoading(true);
      axiosInstance
        .get(`${endpoints.academics.courses}?grade=${gradeId}&branch=${branchId}&subject=${selectedSubject.id}&module_id=${moduleId}`)
        .then((result) => {
          setLoading(false);
          if (result.data.status_code === 200) {
            setCourseList(result.data?.result);
          } else {
            setCourseList([]);
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setCourseList([]);
          setLoading(false);
          setAlert('error', error.message);
        });
    };
    
    return (
        <Layout>
          <div style={{ width: '95%', margin: '20px auto' }}>
            <CommonBreadcrumbs componentName="Master Management" childComponentName='Batch Time Slot' />
          </div>
          <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
          <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
              onChange={(event, value) => {
                if(value){
                  setBranchId(value?.id)
                  setBranch(value)
                }else{
                  setSelectedGrade([])
                  setCourseList([]);
                  setSelectedCourse('');
                }
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>
          <Grid item xs={12} sm={3}>
            <Autocomplete
            size='small'
            id='grades'
            multiple
            className='dropdownIcon'
            options={gradeList || []}
            getOptionLabel={(option) => option?.grade__grade_name || ''}
            filterSelectedOptions
            value={selectedGrade || ''}
            onChange={handleGrade}
            renderInput={(params) => (
                <TextField
                {...params}
                size='small'
                variant='outlined'
                label='Grade'
                placeholder='Grade'
                />
            )}
            />
        </Grid>
        {selectedGrade?.length ?(
          <Grid item xs={12} sm={3}>
            <Autocomplete
            size='small'
            id='sbuject'
            className='dropdownIcon'
            options={subjectList || []}
            getOptionLabel={(option) => option?.subject__subject_name || ''}
            filterSelectedOptions
            value={selectedSubject || ''}
            onChange={(event, value)=> {
              setSelectedSubject(value);
              // handleGrade();
              handleCourse();
            }}
            renderInput={(params) => (
                <TextField
                {...params}
                size='small'
                variant='outlined'
                label='Subject'
                placeholder='Subject'
                />
            )}
            />
        </Grid>

        ): '' }
        {selectedSubject && selectedGrade?.length ? (
            <Grid item xs={12} sm={6}>
            <Autocomplete
                size='small'
                id='courseName'
                className='dropdownIcon'
                options={courseList || []}
                getOptionLabel={(option) => option ? `${option?.course_name} , ${option?.is_fixed ? 'Full Year Course - 40 weeks' : 'Fixed Course' + ' - ' + option?.no_of_periods + ' session'}` : ''}
                filterSelectedOptions
                value={selectedCourse || ''}
                onChange={handleCourse}
                renderInput={(params) => (
                <TextField
                    {...params}
                    size='small'
                    variant='outlined'
                    label='Course'
                    placeholder='Course'
                />
                )}
            />
            </Grid>
          ) : ''}
          </Grid>
          <Paper style={{ width: '100%' , display: selectedCourse? '' : 'none' }}>
            <Grid container spacing={3} style={{ width: '95%', margin: '20px auto' }}>
              {selectedCourse ? (
               <Grid item xs={12} sm={2}>
                 <BatchSelect setSelectedLimit={setSelectedBatch} />
               </Grid>
              ) : ''}
            <Grid item xs={12} sm={10}>
              <CreateBatchTime records={timeList?.[selectedBatch] || []} refresh={()=>  getDetailsApi(selectedCourse?.id)} />
            </Grid>
            </Grid>
          </Paper>
          {loading && <Loader />}
        </Layout>
    )
}

export default AolBatchReference;