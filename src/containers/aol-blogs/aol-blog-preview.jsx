/* eslint-disable react/no-array-index-key */
import React from 'react';
import reactHtmlParser from 'react-html-parser';
import { Grid, Divider } from '@material-ui/core';
import CustomDialog from '../../components/custom-dialog';
import DummyImage from '../../assets/images/Cancel-icon.svg';
import displayName from '../../config/displayName';

const AolBlogPreview = ({ data, open, close }) => {
  const DummyBlogs = () => {
    return (
      <Grid container spacing={2} direction='row' justify='center' alignItems='center'>
        <Grid item md={4} xs={4}>
          <img
            src={DummyImage}
            alt='crash'
            width='100%'
            height='50px'
            loading='lazy'
            style={{
              imageRendering: 'pixelated',
              objectFit: 'cover',
              borderRadius: '5px',
            }}
          />
        </Grid>
        <Grid item md={8} xs={8}>
          <span style={{ color: '#555555', fontSize: '15px', fontFamily: 'Poppins' }}>
            xxxxxx xx xxxxxxx xx xxxxx xxxxxx xxx xxxx xxxxxx
          </span>
        </Grid>
      </Grid>
    );
  };
  return (
    <CustomDialog
      handleClose={close}
      open={open}
      dialogTitle={`Blog Preview ${data?.title}`}
      maxWidth='ls'
      fullScreen
    >
      <Grid container spacing={2}>
        <Grid item md={12} x={12}>
          <Grid container spacing={2}>
            <Grid item md={12} x={12}>
              <span
                style={{
                  color: '#22253A !important',
                  fontFamily: 'Poppins',
                  margin: '0px !important',
                  fontSize: '20px',
                }}
              >
                {displayName?displayName:''} BLOGS
              </span>
              <br />
              <span
                style={{
                  color: '#22253A !important',
                  fontFamily: 'Poppins',
                  margin: '0px !important',
                  fontSize: '20px',
                }}
              >
                <b>{data?.title}</b>
                &nbsp;-&nbsp;
                {data?.sub_title}
              </span>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={8} xs={12}>
          <Grid container spacing={2} style={{ paddingLeft: '10px' }}>
            <Grid item md={12} x={12}>
              <img
                src={data?.image}
                alt='crash'
                width='100%'
                height='400px'
                style={{ borderRadius: '5px' }}
              />
            </Grid>
            <Grid item md={12} x={12}>
              <span
                style={{
                  color: '#013E56',
                  fontFamily: 'Poppins',
                  margin: '0px !important',
                  fontSize: '15px',
                }}
              >
                {data?.description ? reactHtmlParser(data?.description) : ''}
              </span>
            </Grid>
            <Grid item md={12} x={12}>
              {data?.blog_content?.length !== 0 &&
                data?.blog_content.map((item, index) => (
                  <Grid constructor spacing={2} key={index} style={{ margin: '5px 0px' }}>
                    <Grid item md={12} x={12} style={{ margin: '2px 0px' }}>
                      <span
                        style={{
                          color: '#013E56',
                          fontFamily: 'Poppins',
                          margin: '0px !important',
                          fontSize: '20px',
                          fontWeight: 'bold',
                        }}
                      >
                        {item?.title}
                      </span>
                    </Grid>
                    {item?.type !== 'Text' ? (
                      <Grid item md={12} x={12} style={{ margin: '2px 0px' }}>
                        {item?.type === 'Image' ? (
                          <>
                            <img
                              alt='crash'
                              style={{
                                width: '100%',
                                height: '400px',
                                borderRadius: '5px',
                              }}
                              src={item?.file || ''}
                            />
                          </>
                        ) : (
                          <video
                            style={{
                              width: '100%',
                              height: '400px',
                              borderRadius: '5px',
                              backgroundColor: 'black',
                            }}
                            controls
                            controlsList='nodownload'
                            src={item?.file || ''}
                          >
                            <source src={item?.file || ''} type='video/mp4' />
                            <source src={item?.file || ''} type='video/ogg' />
                            <source src={item?.file || ''} type='video/webm' />
                            <track
                              src={item?.file || ''}
                              kind='captions'
                              srcLang='en'
                              label='english_captions'
                            />
                          </video>
                        )}
                      </Grid>
                    ) : (
                      ''
                    )}
                    <Grid item md={12} x={12} style={{ margin: '2px 0px' }}>
                      <span
                        style={{
                          color: '#013E56',
                          fontFamily: 'Poppins',
                          margin: '0px !important',
                          fontSize: '14px',
                        }}
                      >
                        {item?.description ? reactHtmlParser(item?.description) : ''}
                      </span>
                    </Grid>
                  </Grid>
                ))}
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={4} xs={12} style={{ padding: '0px 50px' }}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <span style={{ color: '#555555', fontFamily: 'Poppins', fontSize: '18px' }}>
                Most Viewed blogs
              </span>
              <Divider style={{ margin: '10px 0px', color: '#555555' }} />
              {DummyBlogs()}
              {DummyBlogs()}
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <span style={{ color: '#555555', fontFamily: 'Poppins', fontSize: '18px' }}>
                How To&apos;s
              </span>
              <Divider style={{ margin: '10px 0px', color: '#555555' }} />
              {DummyBlogs()}
              {DummyBlogs()}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </CustomDialog>
  );
};

export default AolBlogPreview;
