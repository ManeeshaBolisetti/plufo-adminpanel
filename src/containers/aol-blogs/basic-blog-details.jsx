import React, { useContext, useState } from 'react';
import { Paper, Grid, TextField, Typography, Button, Card } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import MyTinyMcEditor from '../../components/tinyMc-editor';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import FileUpload from './file-upload';
import CircularProgressWithLabel from './circularProcessWithLabel';

const BasicBlogDetail = ({ blogDetails, setBlogDetails, edit }) => {
  const [imageUploadLoading, setImageUploadLoading] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);
  function functionToHandleImage(data) {
    if ((data && data.type === 'image/jpeg') || (data && data.type === 'image/png')) {
      FileUpload(
        data,
        setAlert,
        'basicDetails',
        blogDetails,
        setBlogDetails,
        setImageUploadLoading
      );
    } else {
      setAlert('warning', 'Upload Image in JPEG && PNG format Only');
    }
  }

  return (
    <Paper style={{ padding: '10px' }}>
      <Grid container spacing={2}>
        <Grid item md={6} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            label='Title'
            margin='dense'
            inputProps={{ maxlength: 150 }}
            value={blogDetails?.title}
            required
            placeholder='Blog Title'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                title: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            label='Sub Title'
            margin='dense'
            inputProps={{ maxlength: 100 }}
            required
            value={blogDetails?.sub_title}
            placeholder='Blog Sub Title'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                sub_title: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            label='SEO Title'
            inputProps={{ maxlength: 200 }}
            required
            margin='dense'
            value={blogDetails?.seo_title}
            placeholder='Blog SEO Title'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                seo_title: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={8} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            required
            label='SEO Description'
            inputProps={{ maxlength: 500 }}
            multiline
            margin='dense'
            value={blogDetails?.seo_desc}
            placeholder='Blog SEO Description'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                seo_desc: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            required
            label='SEO Tags/Keywords'
            margin='dense'
            inputProps={{ maxlength: 500 }}
            value={blogDetails?.seo_tag}
            placeholder='SEO Tags/Keywords'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                seo_tag: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            required
            label='Meta Link'
            margin='dense'
            inputProps={{ maxlength: 100 }}
            value={blogDetails?.meta_title}
            placeholder='Blog Meta Link'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                meta_title: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={4} xs={12}>
          <TextField
            variant='outlined'
            fullWidth
            label='Author'
            margin='dense'
            required
            inputProps={{ maxlength: 100 }}
            value={blogDetails?.author}
            placeholder='Author Name'
            onChange={(e) =>
              setBlogDetails({
                ...blogDetails,
                author: e.target.value.trimLeft(),
              })}
          />
        </Grid>
        <Grid item md={8} xs={12}>
          <Typography>Blog Description&nbsp;*</Typography>
          {edit ? (
            <MyTinyMcEditor
              id='EditBlogDescription'
              initialValue={blogDetails?.description || ''}
              value={blogDetails?.description || ''}
              onChange={(content) =>
                setBlogDetails({
                  ...blogDetails,
                  description: content,
                })
              }
            />
          ) : (
            ''
          )}
          {!edit ? (
            <MyTinyMcEditor
              id='CreateBlogDescription'
              initialValue={blogDetails?.description || ''}
              value={blogDetails?.description || ''}
              onChange={(content) =>
                setBlogDetails({
                  ...blogDetails,
                  description: content,
                })
              }
            />
          ) : (
            ''
          )}
        </Grid>
        <Grid item md={4} xs={12}>
          <Typography>Upload Landing Blog Image&nbsp;*</Typography>
          {blogDetails?.image || imageUploadLoading ? (
            <Card style={{ width: '100%', height: '200px', marginBottom: '10px' }}>
              {imageUploadLoading ? (
                <Grid container spacing={2}>
                  <Grid
                    item
                    md={12}
                    xs={12}
                    style={{ textAlign: 'center', marginTop: '60px' }}
                  >
                    <CircularProgressWithLabel value={imageUploadLoading} />
                    <Typography>Uploading...</Typography>
                  </Grid>
                </Grid>
              ) : (
                <img
                  alt='crash'
                  style={{ width: '100%', height: '100%' }}
                  src={blogDetails?.image || ''}
                />
              )}
            </Card>
          ) : (
            ''
          )}
          <input
            style={{ display: 'none' }}
            id='aol-blog-image'
            type='file'
            disabled={imageUploadLoading}
            accept='image/x-png,image/gif,image/jpeg'
            onChange={(e) => functionToHandleImage(e.target.files[0])}
          />
          <label htmlFor='aol-blog-image'>
            <Button
              variant='contained'
              color='primary'
              component='span'
              size='small'
              disabled={imageUploadLoading}
              startIcon={<CloudUploadIcon />}
            >
              {blogDetails?.image ? 'update Image' : 'Browse'}
            </Button>
          </label>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default BasicBlogDetail;
