/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-array-index-key */
import React, { useContext, useState, useEffect } from 'react';
import AddIcon from '@material-ui/icons/Add';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useHistory } from 'react-router-dom';
import {
  Paper,
  Grid,
  TextField,
  Typography,
  Button,
  Card,
  IconButton,
  Divider,
} from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import ConfirmDialog from '../../components/confirm-dialog';
import MyTinyMcEditor from '../../components/tinyMc-editor';
import FileUpload from './file-upload';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loader from '../../components/loader/loader';
import CircularProgressWithLabel from './circularProcessWithLabel';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import AolBlogPreview from './aol-blog-preview';
import handleUpdateBlog from './handle-update';

const BlogContent = ({ blogDetails, setBlogDetails, setStep, edit }) => {
  const history = useHistory();
  const [uploadLoading, setUploadLoading] = useState('');
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const contentTypeList = ['Video', 'Image', 'Text'];
  const [blogContent, setBlogContent] = useState([]);
  const [openConfirm, setOpenConfirm] = useState(false);
  const [deleteIndex, setDeleteIndex] = useState('');
  const [openPreview, setOpenPreview] = useState(false);

  useEffect(() => {
    setBlogContent(
      blogDetails?.blog_content || [{ title: '', type: '', description: '', file: '' }]
    );
  }, [blogDetails]);

  function handleOnChange(i, key, e) {
    setBlogContent((data) => {
      const newData = [...data];
      switch (key) {
        case key:
          newData[i][key] = e;
          return newData;
        default:
          return null;
      }
    });
  }

  function handleDisable() {
    const blogSelected = blogContent[blogContent?.length - 1];
    if (
      !blogSelected?.title ||
      !blogSelected?.type ||
      (blogSelected?.type !== 'Text' && !blogSelected?.file) ||
      (blogSelected?.type === 'Text' && !blogSelected?.description)
    ) {
      return true;
    }
    return false;
  }

  function addContent() {
    setBlogContent((previousData) => {
      previousData.push({ title: '', type: '', description: '', file: '' });
      return [...previousData];
    });
  }

  function removeContent(index) {
    setBlogContent((previousData) => {
      previousData.splice(index, 1);
      return [...previousData];
    });
    setDeleteIndex('');
    setOpenConfirm(false);
  }

  function handleFileChange(index, data, type) {
    setDeleteIndex(index);
    if (type === 'Image') {
      if ((data && data.type === 'image/jpeg') || (data && data.type === 'image/png')) {
        FileUpload(
          data,
          setAlert,
          'contentDetails',
          index,
          handleOnChange,
          setUploadLoading,
          setDeleteIndex
        );
      } else {
        setAlert('warning', 'Upload Image in JPEG && PNG format Only');
      }
    } else if (type === 'Video') {
      if (data && data.type === 'video/mp4') {
        FileUpload(
          data,
          setAlert,
          'contentDetails',
          index,
          handleOnChange,
          setUploadLoading,
          setDeleteIndex
        );
      } else {
        setAlert('warning', 'Upload Video in MP4 Format');
      }
    }
  }

  async function handleSubmit() {
    setLoading(true);
    const payload = { ...blogDetails };
    payload.blog_content = blogContent;
    try {
      const { data } = await axiosInstance.post(
        endpoints.aolBlogs.createBlogApi,
        payload
      );
      setLoading(false);
      if (data.status_code === 200) {
        setAlert('success', data.message);
        history.push('/aol-blogs/view/1');
      } else {
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  function handleUpdate() {
    const payload = { ...blogDetails };
    payload.blog_content = blogContent;
    handleUpdateBlog(payload, payload?.id, setLoading, setAlert, () => {
      history.push('/aol-blogs/view/2');
    });
  }

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12}>
        <Grid container spacing={2}>
          {blogContent?.map((item, index) => (
            <Grid item md={12} xs={12} key={index}>
              <Paper
                style={{
                  padding: '10px',
                  borderRadius: '5px',
                  border: '1px solid lightgray',
                }}
              >
                <Grid container spacing={1}>
                  <Grid item md={11} xs={6} style={{ textAlign: 'left' }}>
                    <Typography variant='h6' color='secondary'>
                      {`Content ${index + 1}`}
                    </Typography>
                  </Grid>
                  <Grid item md={1} xs={6} style={{ textAlign: 'right' }}>
                    <IconButton
                      size='small'
                      variant='container'
                      color='primary'
                      disabled={index === 0}
                      onClick={() => {
                        setOpenConfirm(true);
                        setDeleteIndex(index);
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Grid>
                  <Grid item md={12} xs={12}>
                    <Grid container spacing={2}>
                      <Grid item md={8} xs={12}>
                        <TextField
                          variant='outlined'
                          fullWidth
                          label='Title'
                          margin='dense'
                          inputProps={{ maxlength: 150 }}
                          required
                          value={item?.title}
                          placeholder='Blog Title'
                          onChange={(e) =>
                            handleOnChange(index, 'title', e.target.value.trimLeft())}
                        />
                      </Grid>
                      <Grid item md={4} xs={12} style={{ marginTop: '8px' }}>
                        <Autocomplete
                          style={{ width: '100%' }}
                          size='small'
                          onChange={(event, value) => {
                            handleOnChange(index, 'type', value);
                            handleOnChange(index, 'file', '');
                          }}
                          disabled={deleteIndex === index && uploadLoading}
                          id='content'
                          className='dropdownIcon'
                          value={item?.type}
                          options={contentTypeList}
                          getOptionLabel={(option) => option}
                          filterSelectedOptions
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              variant='outlined'
                              label='Select Content Type'
                              placeholder='Select Content Type'
                            />
                          )}
                        />
                      </Grid>
                      {item?.type ? (
                        <>
                          <Grid item md={item?.type === 'Text' ? 12 : 8} xs={12}>
                            <Typography>Description&nbsp;*</Typography>
                            <MyTinyMcEditor
                              id={`blog-content-${index}`}
                              initialValue={item?.description || ''}
                              value={item?.description || ''}
                              heightInchs='250'
                              onChange={(content) =>
                                handleOnChange(index, 'description', content)}
                            />
                          </Grid>
                          <Grid
                            item
                            md={item?.type !== 'Text' ? 4 : 0}
                            xs={item?.type !== 'Text' ? 12 : 0}
                          >
                            <Typography
                              style={{ display: item?.type === 'Text' ? 'none' : '' }}
                            >
                              Upload&nbsp;
                              {item?.type === 'Image' ? 'Image' : 'Video'}
                              &nbsp;*
                            </Typography>
                            {item?.file || uploadLoading ? (
                              <Card
                                style={{
                                  width: '100%',
                                  height: '200px',
                                  marginBottom: '10px',
                                }}
                              >
                                {deleteIndex === index && uploadLoading ? (
                                  <Grid container spacing={2}>
                                    <Grid
                                      item
                                      md={12}
                                      xs={12}
                                      style={{ textAlign: 'center', marginTop: '60px' }}
                                    >
                                      <CircularProgressWithLabel value={uploadLoading} />
                                      <Typography>Uploading...</Typography>
                                    </Grid>
                                  </Grid>
                                ) : item?.type === 'Image' ? (
                                  <>
                                    <img
                                      alt='crash'
                                      loading='lazy'
                                      style={{
                                        imageRendering: 'pixelated',
                                        objectFit: 'cover',
                                        width: '100%',
                                        height: '100%',
                                      }}
                                      src={item?.file || ''}
                                    />
                                  </>
                                ) : (
                                  <video
                                    style={{ width: '100%', height: '100%' }}
                                    controls
                                    controlsList='nodownload'
                                    src={item?.file || ''}
                                  >
                                    <source src={item?.file || ''} type='video/mp4' />
                                    <source src={item?.file || ''} type='video/ogg' />
                                    <source src={item?.file || ''} type='video/webm' />
                                    <track
                                      src={item?.file || ''}
                                      kind='captions'
                                      srcLang='en'
                                      label='english_captions'
                                    />
                                  </video>
                                )}
                              </Card>
                            ) : (
                              ''
                            )}
                            <input
                              style={{ display: 'none' }}
                              disabled={deleteIndex === index && uploadLoading}
                              id={`aol-blog-content-image${index}`}
                              type='file'
                              accept={
                                item?.type === 'Image'
                                  ? '/x-png,image/gif,image/jpeg'
                                  : 'video/mp4,video/x-m4v,video/*'
                              }
                              // handleOnChange(index, 'file', e.target.files[0])
                              onChange={(e) =>
                                handleFileChange(index, e.target.files[0], item?.type)}
                            />
                            <label htmlFor={`aol-blog-content-image${index}`}>
                              <Button
                                variant='contained'
                                color='primary'
                                component='span'
                                size='small'
                                disabled={deleteIndex === index && uploadLoading}
                                style={{ display: item?.type === 'Text' ? 'none' : '' }}
                                startIcon={<CloudUploadIcon />}
                              >
                                {item?.file
                                  ? item?.type === 'Image'
                                    ? 'Update Image'
                                    : 'Update video'
                                  : 'Browse'}
                              </Button>
                            </label>
                          </Grid>
                        </>
                      ) : (
                        ''
                      )}
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
        <Button
          variant='contained'
          color='primary'
          size='small'
          disabled={handleDisable()}
          onClick={addContent}
        >
          <AddIcon />
          &nbsp;Add Content
        </Button>
        &nbsp;&nbsp;
        <Button
          variant='contained'
          color='primary'
          size='small'
          onClick={() => setOpenPreview(true)}
        >
          <VisibilityIcon />
          &nbsp;Blog Preview
        </Button>
      </Grid>
      <Grid item md={12} xs={12}>
        <Divider />
      </Grid>
      <Grid item md={12} xs={12}>
        <Grid content spacing={2} justify='space-between' style={{ display: 'flex' }}>
          <Button
            onClick={() => {
              setBlogDetails({
                ...blogDetails,
                blog_content: blogContent,
              });
              setStep(1);
            }}
            size='small'
            color='secondary'
            variant='contained'
          >
            Back
          </Button>
          <Button
            size='small'
            color='primary'
            disabled={handleDisable()}
            variant='contained'
            onClick={edit ? handleUpdate : handleSubmit}
          >
            {edit ? 'Update Blog' : 'Create Blog'}
          </Button>
        </Grid>
      </Grid>
      {openConfirm ? (
        <ConfirmDialog
          open={openConfirm}
          cancel={() => {
            setOpenConfirm(false);
            setDeleteIndex('');
          }}
          confirm={() => removeContent(deleteIndex)}
          title='Are you sure you want to delete ?'
        />
      ) : (
        ''
      )}
      {loading && <Loader />}
      {openPreview && (
        <AolBlogPreview
          data={{ ...blogDetails, blog_content: blogContent }}
          open={openPreview}
          close={() => setOpenPreview(false)}
        />
      )}
    </Grid>
  );
};

export default BlogContent;
