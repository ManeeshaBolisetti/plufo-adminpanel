import React from 'react';
import VisibilityIcon from '@material-ui/icons/Visibility';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CancelIcon from '@material-ui/icons/Cancel';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Grid, IconButton, Popover, Paper, MenuItem, MenuList } from '@material-ui/core';
import reactHtmlParser from 'react-html-parser';
import ConfirmDialog from '../../components/confirm-dialog';
import './style.scss';

const BlogViewCard = ({
  data,
  statusUpdate,
  viewPreview,
  handleEdit,
  handleDelete,
  status,
}) => {
  const useStyles = makeStyles({
    root: {
      maxWidth: '100%',
      borderRadius: '5px',
      backgroundColor: '#fffbe4',
      color: '#b9ac92',
    },
    media: {
      height: '200px',
      width: '100%',
      textAlign: 'end',
      padding: '5px',
    },
  });
  const classes = useStyles();
  const [openConfirm, setOpenConfirm] = React.useState(false);
  const [openPopOver, setOpenPopOver] = React.useState(false);
  const [openDeleteConfirm, setOpenDeleteConfirm] = React.useState(false);
  function handleClosePopOver() {
    setOpenPopOver(null);
  }

  function handleButton(Icon, name) {
    return (
      <Button
        size='small'
        fullWidth
        style={{
          backgroundColor: '#F9F9F9',
          color: '#ff6b6b',
          border: '1px solid #ff6b6b',
          borderRadius: '10px',
        }}
      >
        {Icon}
        &nbsp;
        {name}
      </Button>
    );
  }
  const handleMenuOpen = (event) => {
    setOpenPopOver(event.currentTarget);
  };
  const opnePop = Boolean(openPopOver);
  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpenPopOver(false);
    }
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={12} xs={12}>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image={data?.image}
                title={data?.title}
              >
                <Grid
                  item
                  md={12}
                  xs={12}
                  style={{
                    padding: '0px 10px',
                    margin: '0px',
                    textAlign: 'right',
                  }}
                >
                  <IconButton
                    size='small'
                    type='text'
                    onClick={(event) => handleMenuOpen(event)}
                  >
                    <MoreHorizIcon
                      style={{
                        fontSize: '25px',
                        color: '#FF6B6B',
                        fontWeight: 'bold',
                      }}
                    />
                  </IconButton>
                  <Popover
                    id=''
                    open={opnePop}
                    anchorEl={openPopOver}
                    onClose={handleClosePopOver}
                    anchorOrigin={{
                      vertical: 'center',
                      horizontal: 'center',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                  >
                    <Paper>
                      <MenuList
                        autoFocusItem={openPopOver}
                        id={`menu-list-grow+${data?.id}`}
                        onKeyDown={handleListKeyDown}
                      >
                        {status === 1 ? (
                          <MenuItem onClick={() => setOpenConfirm(true)}>
                            {handleButton(
                              <CancelIcon style={{ color: 'red' }} />,
                              'In Active'
                            )}
                          </MenuItem>
                        ) : (
                          <>
                            <MenuItem onClick={() => handleEdit(data, data?.id)}>
                              {handleButton(<EditIcon />, 'Edit Blog')}
                            </MenuItem>
                            <MenuItem onClick={() => setOpenDeleteConfirm(true)}>
                              {handleButton(<DeleteIcon />, 'Delete Blog')}
                            </MenuItem>
                            <MenuItem onClick={() => setOpenConfirm(true)}>
                              {handleButton(
                                <CheckBoxIcon style={{ color: 'green' }} />,
                                'Activate'
                              )}
                            </MenuItem>
                          </>
                        )}
                        <MenuItem onClick={() => viewPreview(data)}>
                          {handleButton(<VisibilityIcon />, 'Blog Preview')}
                        </MenuItem>
                      </MenuList>
                    </Paper>
                  </Popover>
                </Grid>
              </CardMedia>
              <CardContent>
                <Typography className='blog-title'>
                  {`${data?.title} ${data?.sub_title}`}
                </Typography>
                <Typography className='blog_description'>
                  {reactHtmlParser(data?.description)}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Grid>
      {openConfirm ? (
        <ConfirmDialog
          open={openConfirm}
          cancel={() => setOpenConfirm(false)}
          confirm={() => {
            setOpenConfirm(false);
            statusUpdate({ is_active: status === 1 ? 'False' : 'True' }, data.id);
          }}
          title={`Are You Sure To  ${
            status === 1 ? 'In Activate This Blog' : 'Activate This Blog'
          }?`}
        />
      ) : (
        ''
      )}
      {openDeleteConfirm ? (
        <ConfirmDialog
          open={openDeleteConfirm}
          cancel={() => setOpenDeleteConfirm(false)}
          confirm={() => {
            setOpenDeleteConfirm(false);
            handleDelete(data.id);
          }}
          title='Are You Sure To Delete Blog?'
        />
      ) : (
        ''
      )}
    </>
  );
};

export default BlogViewCard;
