import React, { useState, useContext, useEffect } from 'react';
import { Grid, Button } from '@material-ui/core';
import { useLocation, useHistory } from 'react-router-dom';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Layout from '../Layout';
import BasicBlogDetail from './basic-blog-details';
import BlogContent from './blog-content';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import displayName from '../../config/displayName';

const CreateUpdateAolBlog = () => {
  const history = useHistory();
  const location = useLocation();
  const { setAlert } = useContext(AlertNotificationContext);
  const [step, setStep] = useState(1);
  const [edit, setEdit] = useState(false);

  const [blogDetails, setBlogDetails] = useState({
    title: '',
    sub_title: '',
    description: '',
    author: '',
    image: '',
    meta_title: '',
    seo_title: '',
    seo_desc: '',
    seo_tag: '',
    blog_content: [{ title: '', type: '', description: '', file: '' }],
  });

  useEffect(() => {
    if (location.state && location.state.id) {
      setBlogDetails(location.state);
      setEdit(true);
    }
  }, [location.state]);

  function handleNext() {
    if (!blogDetails?.title) {
      setAlert('warning', 'Please enter title');
      return;
    }
    if (!blogDetails?.sub_title) {
      setAlert('warning', 'Please enter sub title');
      return;
    }
    if (!blogDetails?.description) {
      setAlert('warning', 'Please enter description');
      return;
    }
    if (!blogDetails?.author) {
      setAlert('warning', 'Please enter author name');
      return;
    }
    if (!blogDetails?.image) {
      setAlert('warning', 'Please upload image');
      return;
    }
    if (!blogDetails?.meta_title) {
      setAlert('warning', 'Please enter meta link');
      return;
    }
    if (!blogDetails?.seo_title) {
      setAlert('warning', 'Please enter SEO title');
      return;
    }
    if (!blogDetails?.seo_desc) {
      setAlert('warning', 'Please enter SEO description');
      return;
    }
    if (!blogDetails?.seo_tag) {
      setAlert('warning', 'Please enter SEO tags');
      return;
    }
    setStep(2);
  }

  return (
    <Layout>
      <Grid container spacing={2} style={{ width: '100%', overflow: 'hidden' }}>
        <Grid item md={12} xs={12}>
          <div className='breadcrumb-container-create' style={{ marginLeft: '15px' }}>
            <CommonBreadcrumbs
              componentName={displayName?displayName-'Blog':''}
              childComponentName={edit ? 'Update Blog' : 'Create Blog'}
            />
          </div>
        </Grid>
        <Grid item md={12} xs={12} style={{ margin: '0px 0px 0px 10px' }}>
          <Grid container spacing={1}>
            <Grid item md={12} xs={12}>
              <Stepper activeStep={step - 1} alternativeLabel>
                {['Blog Basic Details', 'Blog Content'].map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
            </Grid>
            {step === 1 ? (
              <>
                <Grid item md={12} xs={12}>
                  <BasicBlogDetail
                    blogDetails={blogDetails}
                    setBlogDetails={setBlogDetails}
                    edit={edit}
                  />
                </Grid>
                <Grid item md={12} xs={12} style={{ margin: '20px 10px' }}>
                  <Grid
                    container
                    spacing={2}
                    direction='row'
                    justify='space-between'
                    alignItems='center'
                  >
                    <Button
                      style={{ width: '100px' }}
                      color='secondary'
                      onClick={() => history.goBack()}
                      variant='contained'
                    >
                      Back
                    </Button>
                    <Button
                      style={{ width: '200px' }}
                      onClick={handleNext}
                      color='primary'
                      variant='contained'
                    >
                      Next
                    </Button>
                  </Grid>
                </Grid>
              </>
            ) : (
              <>
                <Grid item md={12} xs={12}>
                  <BlogContent
                    blogDetails={blogDetails}
                    setBlogDetails={setBlogDetails}
                    setStep={setStep}
                    edit={edit}
                  />
                </Grid>
              </>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default CreateUpdateAolBlog;
