import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';

async function FileUpload(
  file,
  setAlert,
  type,
  blogDetails,
  setBlogDetails,
  setLoading,
  setIndex
) {
  const options = {
    onUploadProgress: (progressEvent) => {
      const { loaded, total } = progressEvent;
      const percent = Math.floor((loaded * 100) / total);
      setLoading(percent);
    },
  };
  const formData = new FormData();
  formData.append('file', file);
  axiosInstance
    .post(`${endpoints.aolBlogs.fileUpload}`, formData, options, {
      headers: {
        'content-type': 'multipart/form-data',
      },
    })
    .then((res) => {
      if (res && res.data && res.data.status_code === 201) {
        setLoading('');
        if (type === 'basicDetails') {
          setBlogDetails({
            ...blogDetails,
            image: res?.data?.result,
          });
        } else {
          setBlogDetails(blogDetails, 'file', res?.data?.result);
        }
      } else if (type === 'basicDetails') {
        setBlogDetails({
          ...blogDetails,
          image: '',
        });
      } else {
        setBlogDetails(blogDetails, 'file', '');
        setIndex('');
      }
    })
    .catch((error) => {
      setAlert('error', error?.response?.data?.message);
    });
}
export default FileUpload;
