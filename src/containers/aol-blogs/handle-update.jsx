import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';

async function handleUpdateBlog(payload, id, setLoading, setAlert, refresh, status) {
  if (status) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.patch(
        `${endpoints.aolBlogs.updateBlogApi}${id}/update-blog/`,
        {
          ...payload,
        }
      );
      if (data.status_code === 200) {
        setLoading(false);
        setAlert('success', data.message);
        refresh();
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  } else {
    setLoading(true);
    try {
      const { data } = await axiosInstance.put(
        `${endpoints.aolBlogs.updateBlogApi}${id}/update-blog/`,
        {
          ...payload,
        }
      );
      if (data.status_code === 200) {
        setLoading(false);
        setAlert('success', data.message);
        refresh();
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }
}

export default handleUpdateBlog;
