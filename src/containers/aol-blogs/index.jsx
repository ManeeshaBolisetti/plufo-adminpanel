/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Pagination } from '@material-ui/lab';
import { Grid, Button, Divider, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Layout from '../Layout';
import BlogViewCard from './blog-view-card';
import notFound from '../../assets/images/unfiltered.svg';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loader from '../../components/loader/loader';
import AolBlogPreview from './aol-blog-preview';
import handleUpdateBlog from './handle-update';

const AolBlogs = (props) => {
  const { match } = props;
  const history = useHistory();
  const [value, setValue] = useState(1);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [blogsData, setBlogsData] = useState([]);
  const { setAlert } = useContext(AlertNotificationContext);
  const [preview, setPreview] = useState('');
  const [open, setOpen] = useState(false);

  function handleCreateBlog() {
    history.push('/aol-blog/creation');
  }

  async function getBlogRecords(pageNo, val = value) {
    setPage(pageNo);
    const url = `${
      endpoints.aolBlogs.getBlogsApi
    }?page=${pageNo}&page_size=${9}&is_active=${val === 1 ? 'True' : 'False'}`;
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      setLoading(false);
      if (data.status_code === 200) {
        setBlogsData(data.data);
      } else {
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  const handleChange = (event, newValue) => {
    setValue(newValue);
    history.push(`/aol-blogs/view/${newValue}`);
    getBlogRecords(1, newValue);
  };

  const handlePagination = (event, page) => {
    history.push(`/aol-blogs/view/${value}/?page=${page}`);
    getBlogRecords(page);
  };

  useEffect(() => {
    if (match.params.status) {
      const paymentDetails = new URLSearchParams(window.location.search);
      const pageId = parseInt(paymentDetails.get('page') || 1, 10);
      setPage(pageId);
      setValue(parseInt(match.params.status, 10));
      const val = parseInt(match.params.status, 10);
      getBlogRecords(pageId, val);
    } else {
      getBlogRecords(1);
    }
  }, []);

  const handleStatus = (data, id) => {
    handleUpdateBlog(
      data,
      id,
      setLoading,
      setAlert,
      () => {
        getBlogRecords(1);
      },
      true
    );
  };
  const handleEdit = (payload) => {
    history.push({
      pathname: '/aol-blog/update',
      state: { ...payload },
    });
  };
  const handleDelete = async (id) => {
    setLoading(true);
    try {
      const { data } = await axiosInstance.delete(
        `${endpoints.aolBlogs.deleteBlogApi}${id}/update-blog/`
      );
      if (data.status_code === 200) {
        setLoading(false);
        getBlogRecords(1);
        setAlert('success', data.message);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  };

  return (
    <Layout>
      <Grid container spacing={2} style={{ width: '100%', overflow: 'hidden' }}>
        <Grid item md={12} xs={12}>
          <div className='breadcrumb-container-create' style={{ marginLeft: '15px' }}>
            <CommonBreadcrumbs componentName='PLUFO Blog' />
          </div>
        </Grid>
        <Grid item md={12} xs={12} style={{ margin: '0px 0px 0px 10px' }}>
          <Button
            onClick={handleCreateBlog}
            variant='contained'
            size='small'
            color='primary'
          >
            <AddIcon />
            &nbsp;Create New Blog
          </Button>
        </Grid>
        <Grid item md={12} xs={12} style={{ margin: '0px 0px 0px 10px' }}>
          <Divider />
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid
            container
            spacing={2}
            style={{ padding: '10px 20px 20px 0px', margin: '0px 0px 0px 10px' }}
          >
            <Grid item md={12} xs={12}>
              <AppBar position='static' color='default'>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  indicatorColor='primary'
                  textColor='primary'
                  variant='scrollable'
                  scrollButtons='auto'
                  aria-label='scrollable auto tabs example'
                >
                  <Tab label='Active' value={1} />
                  <Tab label='In Active' value={2} />
                </Tabs>
              </AppBar>
            </Grid>
            <Grid item md={12} xs={12} style={{ margin: '0px 0px 0px 10px' }}>
              <Grid container spacing={2}>
                {blogsData?.results?.length !== 0 ? (
                  <>
                    {blogsData?.results?.map((item) => (
                      <Grid item md={4} xs={12} key={item.id}>
                        <BlogViewCard
                          data={item}
                          status={value}
                          statusUpdate={handleStatus}
                          viewPreview={(data) => {
                            setPreview(data);
                            setOpen(true);
                          }}
                          handleEdit={handleEdit}
                          handleDelete={handleDelete}
                        />
                      </Grid>
                    ))}
                    <Grid
                      item
                      md={12}
                      xs={12}
                      style={{
                        display: blogsData?.results?.length !== 0 ? '' : 'none',
                        textAlign: 'center',
                      }}
                    >
                      <Pagination
                        onChange={handlePagination}
                        style={{ marginTop: '25px', display: 'inline-table' }}
                        count={blogsData?.total_pages}
                        color='primary'
                        page={page}
                      />
                    </Grid>
                  </>
                ) : (
                  <Grid
                    item
                    md={12}
                    xs={12}
                    style={{ textAlign: 'center', marginTop: '40px' }}
                  >
                    <img
                      loading='lazy'
                      style={{
                        imageRendering: 'pixelated',
                        objectFit: 'cover',
                      }}
                      src={notFound}
                      alt='crash'
                    />
                    <Typography variant='h6' color='secondary'>
                      Records Not Found
                    </Typography>
                  </Grid>
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
      {open && (
        <AolBlogPreview
          data={preview || {}}
          open={open}
          close={() => {
            setOpen(false);
            setPreview('');
          }}
        />
      )}
    </Layout>
  );
};

export default AolBlogs;
