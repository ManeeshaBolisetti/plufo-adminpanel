import React, { useState } from 'react';
import { Grid, Button, Typography, Card } from '@material-ui/core';
import endpoints from '../../config/endpoints';
import ConfirmDialog from '../../components/confirm-dialog';
import './style.scss';

const CourseCard = ({ item, btnLabel, handlefunction }) => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Card
        style={{
          borderRadius: '5px',
          border: '1px solid #E2E2E2',
          background: '#FFD9D9',
        }}
      >
        <Grid container spacing={2} style={{ padding: '10px' }}>
          <Grid item md={6} xs={12}>
            <img
              src={`${endpoints.s3}/dev/aol_file/course/${item?.thumbnail[0]}` || ''}
              alt='crash'
              height='150px'
              width='100%'
              style={{ borderRadius: '5px', imageRendering: 'pixelated' }}
            />
          </Grid>
          <Grid item md={6} xs={12} style={{ display: 'flex' }}>
            <Grid container spacing={2} style={{ padding: '0px !important' }}>
              <Grid item md={12} xs={12}>
                <Typography className='bundle-course-title' title={item?.course_name}>
                  {item?.course_name}
                </Typography>
              </Grid>
              <Grid item md={12} xs={12}>
                <Grid container spacing={2}>
                  {item?.is_fixed ? (
                    <Grid item md={12} xs={12}>
                      <Typography>
                        {item?.no_of_week}
                        &nbsp;Weeks
                      </Typography>
                    </Grid>
                  ) : (
                    <Grid item md={12} xs={12}>
                      <Typography>
                        {item?.no_of_periods}
                        &nbsp;Sessions
                      </Typography>
                    </Grid>
                  )}
                </Grid>
              </Grid>
              <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
                <Button
                  variant='contained'
                  color='primary'
                  size='small'
                  onClick={btnLabel === 'Remove' ? () => setOpen(true) : handlefunction}
                >
                  {btnLabel}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Card>
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => setOpen(false)}
          confirm={() => {
            setOpen(false);
            handlefunction();
          }}
          title='Are you sure to remove this course?'
        />
      ) : (
        ''
      )}
    </>
  );
};

export default CourseCard;
