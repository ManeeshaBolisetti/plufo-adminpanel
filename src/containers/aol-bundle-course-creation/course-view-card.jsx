import React, { useState } from 'react';
import { Grid, Typography, SvgIcon } from '@material-ui/core';
import CourseCard from './course-card';
import unfiltered from '../../assets/images/unfiltered.svg';
import CustomSearchBar from '../../components/custom-seearch-bar';

const CourseViewCard = ({ courses, handleAdd }) => {
  const [searchValue, setSearchValue] = useState('');

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12}>
        <Grid container spacing={2} direction='row' align='center' justify='center'>
          <Grid item md={5} xs={12}>
            <CustomSearchBar
              label=''
              setValue={setSearchValue}
              value={searchValue}
              placeholder='Search based on course name...'
              onChange={(e) => setSearchValue(e.target.value.trimLeft())}
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item md={12} xs={12}>
        <Grid container spacing={2}>
          {courses?.length &&
          courses.filter(
            (val) =>
              val?.course_name &&
              val?.course_name?.toLowerCase()?.includes(searchValue?.toLowerCase())
          )?.length ? (
            courses
              .filter(
                (val) =>
                  val?.course_name &&
                  val?.course_name?.toLowerCase()?.includes(searchValue?.toLowerCase())
              )
              .map((item) => (
                <Grid item md={4} xs={12} key={item.id}>
                  <CourseCard
                    item={item}
                    btnLabel='Add'
                    handlefunction={() => handleAdd(item)}
                  />
                </Grid>
              ))
          ) : (
            <>
              <Grid
                item
                md={12}
                xs={12}
                style={{ textAlign: 'center', marginTop: '50px' }}
              >
                <SvgIcon
                  component={() => (
                    <img
                      alt='crash'
                      style={{
                        height: '160px',
                        width: '290px',
                        imageRendering: 'pixelated',
                      }}
                      src={unfiltered}
                    />
                  )}
                />
              </Grid>
              <Grid item md={12} xs={12}>
                <Typography style={{ textAlign: 'center' }}>
                  Courses are not available
                </Typography>
              </Grid>
            </>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default CourseViewCard;
