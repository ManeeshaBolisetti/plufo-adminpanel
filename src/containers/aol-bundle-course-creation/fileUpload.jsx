import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';

async function FileUpload(file, setAlert, setLoading, setThumbnail) {
  const options = {
    onUploadProgress: (progressEvent) => {
      const { loaded, total } = progressEvent;
      const percent = Math.floor((loaded * 100) / total);
      setLoading(percent);
    },
  };
  const formData = new FormData();
  formData.append('file', file);
  axiosInstance
    .post(`${endpoints.onlineCourses.fileUpload}`, formData, options, {
      headers: {
        'content-type': 'multipart/form-data',
      },
    })
    .then((res) => {
      if (res && res.data && res.data.status_code === 200) {
        setLoading('');
        setThumbnail(res?.data?.result?.get_file_path);
      } else {
        setAlert('error', res?.data?.message);
      }
    })
    .catch((error) => {
      setAlert('error', error?.response?.data?.message);
    });
}
export default FileUpload;
