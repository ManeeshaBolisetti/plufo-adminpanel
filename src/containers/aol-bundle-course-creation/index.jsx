import React, { useState, useEffect, useContext, useRef } from 'react';
import debounce from 'lodash.debounce';
import { Grid, TextField, SvgIcon, Typography, Card } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import CourseViewCard from './course-view-card';
import unfiltered from '../../assets/images/unfiltered.svg';
import SelectedCourseList from './selectedCourseList';
import CourseCard from './course-card';

const AolBundleCourseCreation = () => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [gradeList, setGradeList] = useState([]);
  const [selectedGrade, setSelectedGrade] = useState('');
  const [selectedCourseType, setSelectedCourseType] = useState('');
  const [courseList, setCourseList] = useState('');
  const [noOfSessions, setNoOfSessions] = useState('');
  const [moduleId, setModuleId] = useState('');
  const [loading, setLoading] = useState(false);
  const [selectedCourse, setSelectedCourse] = useState([]);
  const [courseView, setCourseView] = useState(true);
  const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
  const courseTypeList = [
    { id: 1, name: 'Full Year Course' },
    { id: 2, name: 'Fixed Course' },
  ];

  const [branchs, setBranches] = useState('');
  const [selectedBranch, setSelectedBranch] = useState('');

  useEffect(() => {
    // localStorage.setItem("courseView",true);
    // courseView = JSON.parse(localStorage.getItem("courseView"));
    if (NavData && NavData.length) {
      NavData.forEach((item) => {
        if (
          item.parent_modules === 'Master Management' &&
          item.child_module &&
          item.child_module.length > 0
        ) {
          item.child_module.forEach((item) => {
            if (item.child_name === 'Course Price') {
              setModuleId(item.child_id);
            }
          });
        }
      });
    }
  }, []);

  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);

      if (data?.status_code === 200) {
        if (type === 'grade') {
          setGradeList(data?.data);
        } else {
          setCourseList(data?.result || []);
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  function getData(value, grade, course) {

    if (selectedBranch && selectedGrade && selectedCourseType) {
      ApiCall(
        `${endpoints.bundleCourse.getCourseList}?branch=${selectedBranch?.id}&grade=${grade}&course_type=${course === 1 ? 'True' : 'False'
        }&no_session=${parseInt(value, 10)}`,
        'courseList'
      );
    } else {
      setCourseList([]);
    }
  }
  const delayedQuery = useRef(debounce((q, p, r) => getData(q, p, r), 1000)).current;

  useEffect(() => {
    getData(selectedBranch?.id, selectedGrade?.grade_id, selectedCourseType?.id);
  }, [selectedBranch, selectedGrade, selectedCourseType])
  useEffect(() => {
    if (moduleId && selectedBranch) {
      ApiCall(
        `${endpoints.communication.grades}?branch_id=${selectedBranch?.id}&module_id=${moduleId}`,
        'grade'
      );
    } else {
      setSelectedGrade('');
    }
  }, [moduleId, selectedBranch]);

  function handelRemove(info) {
    setSelectedCourse((prev) => [...prev.filter((item) => item.id !== info.id)]);
  }

  async function handleCreateBundle(info) {
    if (selectedCourseType?.id === 2 && !noOfSessions) {
      setAlert('warning', 'Please Enter No of Sessions');
      return;
    }
    if (!info?.meta_title) {
      setAlert('warning', 'Please Course URL');
      return;
    }
    if (info?.thumbnail?.length === 0) {
      setAlert('warning', 'Please Upload thumbnail');
      return;
    }
    setLoading(true);
    const paylad = {
      Name: 'a',
      courses: selectedCourse?.map((item) => item.id),
      is_fixed: selectedCourseType?.id === 1,
      session: noOfSessions || 40,
      grade: selectedGrade?.grade_id || '',
      ...info,
    };
    try {
      const { data } = await axiosInstance.post(
        `${endpoints.bundleCourse.createBundleCourse}`,
        {
          ...paylad,
        }
      );
      if (data.status_code === 200) {
        setLoading(false);
        setSelectedCourse([]);
        setSelectedGrade('');
        setSelectedCourseType('');
        setCourseList('');
        setNoOfSessions('');
        setAlert('success', data.message);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  const getBranchApi = async () => {
    try {
      const result = await axiosInstance.get(`${endpoints.communication.branches}?module_id=${moduleId}`);
      if (result.status === 200) {
        setBranches(result.data.data);
      } else {
        setAlert('error', result.data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
    }
  };
  useEffect(() => {
    getBranchApi()
  }, []);


  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Bundle Course'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={3} xs={12} style={{ marginTop: '8px' }}>
              <Autocomplete
                style={{ width: '100%' }}
                size='small'
                className='dropdownIcon'
                onChange={(event, value) => {
                  setSelectedBranch(value);
                }}
                id='branch_id'
                options={branchs}
                getOptionLabel={(option) => option?.branch_name}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='Branch'
                    placeholder='Branch'
                  />
                )}
              />
            </Grid>
            {selectedBranch ? (
              <Grid item md={3} xs={12} style={{ marginTop: '8px' }}>
                <Autocomplete
                  size='small'
                  id='grades'
                  className='dropdownIcon'
                  options={gradeList || []}
                  getOptionLabel={(option) => option?.grade__grade_name || ''}
                  filterSelectedOptions
                  value={selectedGrade || ''}
                  onChange={(event, value) => {
                    setSelectedGrade(value);
                    setSelectedCourseType('');
                    setCourseList('');
                    setSelectedCourse([]);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      size='small'
                      variant='outlined'
                      label='Grade'
                      placeholder='Grade'
                    />
                  )}
                />
              </Grid>
            ) : (
              ''
            )}
            {selectedBranch && selectedGrade ? (
              <Grid item md={3} xs={12} style={{ marginTop: '8px' }}>
                <Autocomplete
                  size='small'
                  id='courseType'
                  className='dropdownIcon'
                  options={courseTypeList || []}
                  getOptionLabel={(option) => option?.name}
                  filterSelectedOptions
                  value={selectedCourseType || ''}
                  onChange={(event, value) => {
                    setSelectedCourseType(value);
                    setNoOfSessions('');
                    setCourseList('');
                    setSelectedCourse([]);
                    if (value && value.id === 1) {
                      ApiCall(
                        `${endpoints.bundleCourse.getCourseList}?branch=${selectedBranch?.id}&grade=${selectedGrade?.grade_id
                        }&course_type=${value.id === 1 ? 'True' : 'False'}`,
                        'courseList'
                      );
                    }
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      size='small'
                      variant='outlined'
                      label='Course Type'
                      placeholder='Course Type'
                    />
                  )}
                />
              </Grid>
            ) : (
              ''
            )}
            {selectedGrade && selectedCourseType?.id === 2 ? (
              <Grid item md={3} xs={12}>
                <TextField
                  className='inputFiled'
                  style={{ borderRadius: '10px' }}
                  margin='dense'
                  variant='outlined'
                  label='No of Sessions'
                  placeholder='No of Sessions'
                  value={noOfSessions}
                  type='number'
                  fullWidth
                  onChange={(e) => {
                    if (e.target.value > -1 && e.target.value?.length < 10) {
                      setNoOfSessions(e.target.value.trimLeft());
                    }
                    if (e.target.value && selectedCourseType?.id === 2) {
                      delayedQuery(
                        e.target.value.trimLeft(),
                        selectedGrade?.grade_id,
                        selectedCourseType?.id
                      );
                    }
                  }}
                />
              </Grid>
            ) : (
              ''
            )}
          </Grid>
          {selectedCourse?.length ? (
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ marginTop: '20px' }}>
                <Card style={{ width: '100%', padding: '10px', borderRadius: '10px' }}>
                  <SelectedCourseList
                    selectedCourse={selectedCourse}
                    handelRemove={handelRemove}
                    handleCreateBundle={handleCreateBundle}
                    setAlert={setAlert}
                    setCourseView={setCourseView}
                    courseView={courseView}
                  />
                </Card>
              </Grid>
            </Grid>
          ) : (
            ''
          )}
          <Grid container spacing={2}>
            {courseView && courseList && courseList?.length ? (
              <Grid item md={12} xs={12} style={{ marginTop: '20px' }}>
                <Card style={{ width: '100%', padding: '10px', borderRadius: '10px' }}>
                  <Grid container spacing={2}>
                    <Grid item md={12} xs={12}>
                      <Typography
                        style={{ textAlign: 'center' }}
                        variant='h6'
                        color='secondary'
                      >
                        View All Courses
                      </Typography>
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <CourseViewCard
                        setAlert={setAlert}
                        courses={
                          courseList?.length
                            ? courseList?.filter(
                              (info) => !selectedCourse?.includes(info)
                            )
                            : []
                        }
                        handleAdd={(item) =>
                          selectedCourse?.length < 6
                            ? setSelectedCourse((prev) => [...prev, item])
                            : setAlert(
                              'warning',
                              'You cannot add more than 6 course in a bundle'
                            )
                        }
                      />
                    </Grid>
                  </Grid>
                </Card>
              </Grid>
            ) : (
              ''
            )}
          </Grid>
          <Grid container spacing={2}>
            {!courseView && selectedCourse && selectedCourse?.length ? (
              <Grid item md={12} xs={12} style={{ marginTop: '20px' }}>
                <Card style={{ width: '100%', padding: '10px', borderRadius: '10px' }}>
                  <Grid container spacing={2}>
                    <Grid item md={12} xs={12}>
                      <Typography
                        style={{ textAlign: 'center' }}
                        variant='h6'
                        color='secondary'
                      >
                        Selected Courses
                      </Typography>
                    </Grid>
                    <Grid container spacing={2}>
                      {selectedCourse?.map((item) => (
                        <Grid item md={4} xs={12}>
                          <CourseCard
                            item={item}
                            // btnLabel='Remove'
                            handlefunction={() => handelRemove(item)}
                          />
                        </Grid>
                      ))}
                    </Grid>
                  </Grid>
                </Card>
              </Grid>
            ) : (
              <>
                <Grid
                  item
                  md={12}
                  xs={12}
                  style={{ textAlign: 'center', marginTop: '50px' }}
                >
                  <SvgIcon
                    component={() => (
                      <img
                        alt='crash'
                        style={{ height: '160px', width: '290px' }}
                        src={unfiltered}
                      />
                    )}
                  />
                </Grid>
                <Grid item md={12} xs={12}>
                  <Typography style={{ textAlign: 'center' }}>
                    {courseList && courseList?.length === 0
                      ? 'Courses are not available on this filter'
                      : 'Filter to get courses'}
                  </Typography>
                </Grid>
              </>
            )}
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default AolBundleCourseCreation;
