import React, { useState, useEffect } from 'react';
import {
  Grid,
  Switch,
  Typography,
  TextField,
  Divider,
  IconButton,
} from '@material-ui/core';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import DeleteIcon from '@material-ui/icons/Delete';
import CourseCard from './course-card';
import FileUpload from './fileUpload';
import ConfirmDialog from '../../components/confirm-dialog';
import LinearProgressBar from '../../components/progress-bar';
import displayName from '../../config/displayName';

const SelectedCourseList = ({
  selectedCourse,
  handelRemove,
  handleCreateBundle,
  setAlert,
  setCourseView,
  courseView,
}) => {
  const [step, setStep] = useState(0);
  const [specialDiscount, setSpecialDiscount] = useState('');
  const [isShow, setIsShow] = useState(false);
  const [metaTitle, setMetaTitle] = useState('');
  const [metaDescription, setMetaDescription] = useState('');
  const [courseUrl, setCourseUrl] = useState('');
  const [thumbnail, setThumbnail] = useState('');
  const [loading, setLoading] = useState('');
  const [open, setOpen] = useState(false);

  function handleFileChange(data) {
    if ((data && data.type === 'image/jpeg') || (data && data.type === 'image/png')) {
      FileUpload(data, setAlert, setLoading, setThumbnail);
    } else {
      setAlert('warning', 'Upload Image in JPEG && PNG format Only');
    }
  }

  useEffect(() => {
    console.log('step:',step);
    if(step === 0){
      localStorage.setItem("courseView",true);
    }else{
      localStorage.setItem("courseView",false);

    }

  },[step]);

  return (
    <>
      <Grid container spacing={2} style={{ padding: '10px' }}>
        <Grid item md={12} xs={12}>
          <Stepper activeStep={step} alternativeLabel>
            {['Selected Courses', 'Course Info'].map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
        </Grid>
        {step === 0 ? (
          <Grid item md={12} xs={12}>
            <Grid container spacing={2}>
              {selectedCourse?.map((item) => (
                <Grid item md={4} xs={12}>
                  <CourseCard
                    item={item}
                    btnLabel='Remove'
                    handlefunction={() => handelRemove(item)}
                  />
                </Grid>
              ))}
            </Grid>
          </Grid>
        ) : (
          <Grid container spacing={2}>
            <Grid item md={4} xs={12}>
              <TextField
                label='Course URL'
                value={courseUrl}
                fullWidth
                variant='outlined'
                margin='dense'
                required
                onChange={(e) => setCourseUrl(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                label='Meta Title'
                fullWidth
                value={metaTitle}
                variant='outlined'
                margin='dense'
                onChange={(e) => setMetaTitle(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                label='Meta Description'
                fullWidth
                value={metaDescription}
                variant='outlined'
                multiline
                margin='dense'
                onChange={(e) => setMetaDescription(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                value={specialDiscount}
                label='Special Discount (%)'
                variant='outlined'
                margin='dense'
                fullWidth
                type='number'
                className='inputFiled'
                onChange={(e) =>
                  e.target.value > -1 &&
                  e.target.value < 101 &&
                  e.target.value?.length < 4 &&
                  setSpecialDiscount(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12} style={{ textAlign: 'center' }}>
              <Typography>Show course in {displayName?displayName:''} Dashboard</Typography>
              NO&nbsp;
              <Switch
                checked={isShow}
                color='secondary'
                onChange={(e) => setIsShow(e.target.checked)}
              />
              &nbsp;Yes
            </Grid>
            <Grid item md={4} xs={12}>
              <Typography>
                Course Thumbnail&nbsp;
                <span style={{ color: 'red' }}>*</span>
              </Typography>
              {!thumbnail ? (
                <>
                  <input
                    required
                    style={{ display: 'none' }}
                    id='bundle-course-upload-image'
                    type='file'
                    accept='/x-png,image/gif,image/jpeg'
                    onChange={(e) => handleFileChange(e.target.files[0])}
                  />
                  <label htmlFor='bundle-course-upload-image'>
                    <Button
                      variant='contained'
                      color='primary'
                      component='span'
                      size='small'
                      startIcon={<CloudUploadIcon />}
                    >
                      Upload Thumbnail
                    </Button>
                  </label>
                </>
              ) : (
                <>
                  <Grid
                    container
                    spacing={2}
                    style={{
                      padding: '10px',
                      borderRadius: '10px',
                      border: '1px solid #FF6B6B',
                      width: '100%',
                      margin: '5px 0px',
                    }}
                    justify='space-between'
                    alignItems='center'
                    direction='row'
                  >
                    <span style={{ color: '#fe6b6b', fontSize: '13px' }}>
                      {thumbnail}
                    </span>
                    &nbsp; &nbsp;
                    <IconButton
                      onClick={() => setOpen(true)}
                      size='small'
                      color='primary'
                      variant='contained'
                    >
                      <DeleteIcon />
                    </IconButton>
                  </Grid>
                </>
              )}
              {loading && !thumbnail ? (
                <LinearProgressBar value={loading} color='secondary' />
              ) : (
                ''
              )}
            </Grid>
          </Grid>
        )}
        <Grid item md={12} xs={12}>
          <Grid container spacing={2} justify={step === 0 ? 'flex-end' : 'space-between'}>
            <Grid item md={12} xs={12}>
              <Divider />
            </Grid>
            <Button
              variant='contained'
              color='secondary'
              size='small'
              onClick={() => (setStep(0),setCourseView(true))}
              style={step === 0 ? { display: 'none' } : {}}
            >
              Back
            </Button>
            <Button
              variant='contained'
              size='small'
              color='primary'
              onClick={() => {
                if (step === 0) {
                  if (selectedCourse?.length < 2) {
                    setAlert(
                      'warning',
                      'To Create Bundle Course minimum 2 course are required'
                    );
                    return;
                  }
                  setStep(1);
                  setCourseView(false);
                }else if(!thumbnail){
                  setAlert(
                    'warning',
                    'Thumbnail is required to create Bundle Course'
                  );
                  return;
                }else {
                  handleCreateBundle({
                    is_show: isShow,
                    special_discount: specialDiscount || 0,
                    thumbnail: [thumbnail],
                    meta_title: courseUrl,
                    course_meta_title: metaTitle,
                    meta_description: metaDescription,
                  });
                }
              }}
            >
              {step === 0 && courseView ?'Next' :'Create Bundle Course'}
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => setOpen(false)}
          confirm={() => {
            setOpen(false);
            setThumbnail('');
          }}
          title='Are You Sure to Delete ?'
        />
      ) : (
        ''
      )}
    </>
  );
};

export default SelectedCourseList;
