import React from 'react';
import { Grid, Typography, Card } from '@material-ui/core';

const BundleCourseCard = ({ courseDetails, courseType, noOfSessions }) => {
  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12}>
        <Card
          style={{ border: '1px solid lightgray', padding: '10px', borderRadius: '10px' }}
        >
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Typography>
                Course Name :&nbsp;
                {courseDetails?.course_name}
              </Typography>
              <Typography>
                Subject Name :&nbsp;
                {courseDetails?.subject}
              </Typography>
              <Typography>{!courseType ? `${noOfSessions} sessions` : ''}</Typography>
            </Grid>
          </Grid>
        </Card>
      </Grid>
    </Grid>
  );
};
export default BundleCourseCard;
