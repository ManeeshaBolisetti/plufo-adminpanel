import React, { useState, useEffect } from 'react';
import {
  Grid,
  Typography,
  IconButton,
  TextField,
  Button,
  Switch,
  Divider,
} from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import CancelIcon from '@material-ui/icons/Cancel';
import endpoints from '../../config/endpoints';
import FileUpload from '../aol-bundle-course-creation/fileUpload';
import ConfirmDialog from '../../components/confirm-dialog';
import axiosInstance from '../../config/axios';
import LinearProgressBar from '../../components/progress-bar';
import displayName from '../../config/displayName';

const BundleCourseDetails = ({ selectedCourse, setAlert, setLoader, handleRefresh }) => {
  const [edit, setEdit] = useState(false);
  const [specialDiscount, setSpecialDiscount] = useState('');
  const [isShow, setIsShow] = useState(false);
  const [metaTitle, setMetaTitle] = useState('');
  const [metaDescription, setMetaDescription] = useState('');
  const [courseUrl, setCourseUrl] = useState('');
  const [thumbnail, setThumbnail] = useState('');
  const [loading, setLoading] = useState('');
  const [open, setOpen] = useState(false);
  const [cancel, setCancel] = useState(false);

  useEffect(() => {
    if (selectedCourse) {
      setSpecialDiscount(selectedCourse?.special_discount || '');
      setIsShow(selectedCourse?.is_show);
      setMetaTitle(selectedCourse?.course_meta_title || '');
      setMetaDescription(selectedCourse?.meta_description || '');
      setCourseUrl(selectedCourse?.meta_title || '');
      setThumbnail(selectedCourse?.thumbnail?.[0] || '');
    }
  }, [selectedCourse]);

  function handleFileChange(data) {
    if ((data && data.type === 'image/jpeg') || (data && data.type === 'image/png')) {
      FileUpload(data, setAlert, setLoading, setThumbnail);
    } else {
      setAlert('warning', 'Upload Image in JPEG && PNG format Only');
    }
  }

  async function handleSave() {
    if (!courseUrl) {
      setAlert('warning', 'Please Enter Course URL');
      return;
    }
    if (!thumbnail) {
      setAlert('warning', 'Please Upload Thumbnail');
      return;
    }
    const payload = {
      id: selectedCourse?.id,
      is_show: isShow,
      special_discount: specialDiscount || 0,
      thumbnail: [thumbnail || ''],
      meta_title: courseUrl,
      course_meta_title: metaTitle,
      meta_description: metaDescription,
    };
    setLoader(true);
    try {
      const { data } = await axiosInstance.put(
        endpoints.bundleCourse.updateBundleCourse,
        { ...payload }
      );
      if (data?.status_code === 200) {
        setLoader(false);
        setAlert('success', data?.message);
        handleRefresh();
        setEdit(false);
      } else {
        setAlert('error', data?.message);
        setLoader(false);
      }
    } catch (error) {
      setLoader(false);
      setAlert('error', error?.message);
    }
  }

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12}>
        <Grid container spacing={2} justify='space-around' alignItems='center'>
          <Grid item md={6} xs={6} style={{ textAlign: 'left' }}>
            <Typography>Bundle Course Details</Typography>
          </Grid>
          <Grid item md={6} xs={6} style={{ textAlign: 'right' }}>
            <IconButton
              onClick={() => {
                edit ? setCancel(true) : setEdit(true);
              }}
              variant='contained'
              color='primary'
              size='small'
            >
              {edit ? <CancelIcon /> : <EditIcon />}
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
      <Grid item md={12} xs={12}>
        <Divider />
      </Grid>
      <Grid item md={12} xs={12}>
        <Grid container spacing={2}>
          <Grid item md={4} xs={12}>
            {edit ? (
              <>
                <Typography>
                  Course Thumbnail&nbsp;
                  <span style={{ color: 'red' }}>*</span>
                </Typography>
                {!thumbnail ? (
                  <>
                    <input
                      style={{ display: 'none' }}
                      id='bundle-course-upload-image'
                      type='file'
                      accept='/x-png,image/gif,image/jpeg'
                      onChange={(e) => handleFileChange(e.target.files[0])}
                    />
                    <label htmlFor='bundle-course-upload-image'>
                      <Button
                        variant='contained'
                        color='primary'
                        component='span'
                        size='small'
                        startIcon={<CloudUploadIcon />}
                      >
                        Upload Thumbnail
                      </Button>
                    </label>
                  </>
                ) : (
                  <>
                    <Grid
                      container
                      spacing={2}
                      style={{
                        padding: '10px',
                        borderRadius: '10px',
                        border: '1px solid #FF6B6B',
                        width: '100%',
                        margin: '5px 0px',
                      }}
                      justify='space-between'
                      alignItems='center'
                      direction='row'
                    >
                      <span style={{ color: '#fe6b6b', fontSize: '13px' }}>
                        {thumbnail}
                      </span>
                      &nbsp; &nbsp;
                      <IconButton
                        onClick={() => setOpen(true)}
                        size='small'
                        color='primary'
                        variant='contained'
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Grid>
                  </>
                )}
                {loading && !thumbnail ? (
                  <LinearProgressBar value={loading} color='secondary' />
                ) : (
                  ''
                )}
              </>
            ) : (
              <img
                src={`${endpoints.bundleCourse.imageS3List}${selectedCourse?.thumbnail?.[0]}`}
                alt='crash'
                height='200px'
                width='100%'
                loading='lazy'
                style={{ imageRendering: 'pixelated', objectFit: 'cover' }}
              />
            )}
          </Grid>
          <Grid item md={8} xs={12}>
            <Grid container spacing={2}>
              <Grid item md={edit ? 4 : 6} xs={12}>
                {edit ? (
                  <TextField
                    label='Course URL'
                    value={courseUrl}
                    fullWidth
                    variant='outlined'
                    margin='dense'
                    required
                    onChange={(e) => setCourseUrl(e.target.value.trimLeft())}
                  />
                ) : (
                  <Typography>
                    Course URL :&nbsp;
                    {selectedCourse?.meta_title || 'Not added'}
                  </Typography>
                )}
              </Grid>
              <Grid item md={edit ? 4 : 6} xs={12}>
                {edit ? (
                  <TextField
                    label='Meta Title'
                    fullWidth
                    value={metaTitle}
                    variant='outlined'
                    margin='dense'
                    onChange={(e) => setMetaTitle(e.target.value.trimLeft())}
                  />
                ) : (
                  <Typography>
                    Meta Title :&nbsp;
                    {selectedCourse?.course_meta_title || 'Not Added'}
                  </Typography>
                )}
              </Grid>
              <Grid item md={edit ? 4 : 6} xs={12}>
                {edit ? (
                  <TextField
                    label='Meta Description'
                    fullWidth
                    value={metaDescription}
                    variant='outlined'
                    multiline
                    margin='dense'
                    onChange={(e) => setMetaDescription(e.target.value.trimLeft())}
                  />
                ) : (
                  <Typography>
                    Meta Description :&nbsp;
                    {selectedCourse?.meta_description || 'Not added'}
                  </Typography>
                )}
              </Grid>
              <Grid item md={edit ? 4 : 6} xs={12}>
                {edit ? (
                  <TextField
                    value={specialDiscount}
                    label='Special Discount (%)'
                    variant='outlined'
                    margin='dense'
                    fullWidth
                    type='number'
                    className='inputFiled'
                    onChange={(e) =>
                      e.target.value > -1 &&
                      e.target.value < 101 &&
                      e.target.value?.length < 4 &&
                      setSpecialDiscount(e.target.value.trimLeft())
                    }
                  />
                ) : (
                  <Typography>
                    Special Discount :&nbsp;
                    {selectedCourse?.special_discount
                      ? `${selectedCourse?.special_discount} %`
                      : 'Not Added'}
                  </Typography>
                )}
              </Grid>
              <Grid item md={edit ? 8 : 6} xs={12}>
                {edit ? (
                  <>
                    <Typography>Show course in PLUFO Dashboard</Typography>
                    NO&nbsp;
                    <Switch
                      checked={isShow}
                      color='secondary'
                      onChange={(e) => setIsShow(e.target.checked)}
                    />
                    &nbsp;Yes
                  </>
                ) : (
                  <Typography>
                    Show Course In {displayName ? displayName : ''} Dashboard :&nbsp;
                    {selectedCourse?.is_show ? 'Yes' : 'NO'}
                  </Typography>
                )}
              </Grid>
              {edit ? (
                <Grid
                  item
                  md={12}
                  xs={12}
                  style={{ marginTop: '10px', textAlign: 'right' }}
                >
                  <Button
                    variant='contained'
                    color='primary'
                    size='small'
                    onClick={() => handleSave()}
                  >
                    Save
                  </Button>
                </Grid>
              ) : (
                ''
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => setOpen(false)}
          confirm={() => {
            setOpen(false);
            setThumbnail('');
          }}
          title='Are You Sure to Delete ?'
        />
      ) : (
        ''
      )}
      {cancel ? (
        <ConfirmDialog
          open={cancel}
          cancel={() => {
            setCancel(false);
          }}
          confirm={() => {
            setEdit(false);
            setCancel(false);
            setSpecialDiscount(selectedCourse?.special_discount || '');
            setIsShow(selectedCourse?.is_show);
            setMetaTitle(selectedCourse?.course_meta_title || '');
            setMetaDescription(selectedCourse?.meta_description || '');
            setCourseUrl(selectedCourse?.meta_title || '');
            setThumbnail(selectedCourse?.thumbnail?.[0] || '');
          }}
          title='Are You Sure to Cancel ?'
        />
      ) : (
        ''
      )}
    </Grid>
  );
};

export default BundleCourseDetails;
