import React, { useState, useEffect, useContext } from 'react';
import { Grid, TextField, Divider, Card } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import BundleCourseCard from './bundle-course-cards';
import BundleCourseDetails from './bundleCourseDetails';

const ViewBundleCourses = () => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [gradeList, setGradeList] = useState([]);
  const [selectedGrade, setSelectedGrade] = useState('');
  const [courseList, setCourseList] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState('');
  const [loading, setLoading] = useState(false);
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null);
  const [selectDropDown,setSelectDropDown] =useState([]);


  async function ApiCall(url, type, filt) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200) {
        if (type === 'grade') {
          setGradeList(data?.data);
        } else {
          setCourseList(data?.result || []);
          if (filt === 'filter') {
            setSelectedCourse(
              (prev) => data?.result?.filter((item) => item?.id === prev.id)?.[0]
            );
          }
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }
  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      setSelectDropDown(res.data.data)

    })
  }, [])

  useEffect(() => {
    if(branchId){
      ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}`, 'grade');
    }
  }, [branchId]);

  function handleRefresh() {
    ApiCall(
      `${endpoints.academics.bundleCourse}?branch=${branchId}&grade=${selectedGrade?.grade_id}`,
      'courses',
      'filter'
    );
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Bundle Course'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
      <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
              onChange={(event, value) => {
                // console.log(value,'ml999999')
                setBranchId(value?.id)
                setBranch(value)
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>
        <Grid item md={3} xs={12}>
          <Autocomplete
            size='small'
            id='grades'
            className='dropdownIcon'
            options={gradeList || []}
            getOptionLabel={(option) => option?.grade__grade_name || ''}
            filterSelectedOptions
            value={selectedGrade || ''}
            onChange={(event, value) => {
              setSelectedGrade(value);
              setCourseList([]);
              setSelectedCourse('');
              if (value) {
                ApiCall(
                  `${endpoints.academics.bundleCourse}?branch=${branchId}&grade=${value?.grade_id}`,
                  'courses'
                );
              }
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                size='small'
                variant='outlined'
                label='Grade'
                placeholder='Grade'
              />
            )}
          />
        </Grid>
        <Grid item md={6} xs={12}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={(event, value) => {
              setSelectedCourse(JSON.parse(JSON.stringify(value)));
            }}
            id='course-id'
            className='dropdownIcon'
            value={selectedCourse}
            options={courseList}
            getOptionLabel={(option) => option?.name}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Select Bundle Course'
                placeholder='Select Bundle Course'
              />
            )}
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <Divider />
        </Grid>
        {selectedCourse ? (
          <Grid item md={12} xs={12}>
            <Card
              style={{
                borderRadius: '10px',
                padding: '10px',
                border: '1px solid lightgray',
              }}
            >
              <BundleCourseDetails
                selectedCourse={selectedCourse}
                setAlert={setAlert}
                setLoader={setLoading}
                handleRefresh={handleRefresh}
              />
            </Card>
          </Grid>
        ) : (
          ''
        )}
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            {selectedCourse?.course?.map((item) => (
              <Grid item md={4} xs={12} key={item.id}>
                <BundleCourseCard
                  courseDetails={item}
                  courseType={selectedCourse?.is_fixed}
                  noOfSessions={selectedCourse?.session}
                />
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default ViewBundleCourses;
