import React, { useState, useEffect, useContext, useRef } from 'react';
import {
  Grid,
  Card,
  TextField,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Modal,
  makeStyles,
  IconButton,
  FormControl,
  NativeSelect,
  TableFooter,
  Typography,
  TablePagination,
} from '@material-ui/core';
import debounce from 'lodash.debounce';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import './style.scss';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';
import ModalContent from './modal-content';
import { ChevronLeft, ChevronRight, FirstPage, KeyboardArrowLeft, KeyboardArrowRight, LastPage } from '@material-ui/icons';
import ConfirmDialog from 'components/confirm-dialog';

const AolBundleDisconnect = () => {
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [discountList, setDiscountList] = useState([]);
  const delayedQuery = useRef(debounce((q, p) => handleCreateDiscount(q, p), 1000))
    .current;

  const [open, setOpen] = useState(false);
  const [confirmModalOpen, setConfirmModalOpen] = useState(false);
  const [indexNo, setIndexNo] = useState('')
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPages, setTotalPage] = useState(1)
  const [totalCount, setTotalCount] = useState('')
  const [rowsPerPage, setRowsPerPages] = useState(10)
  const [bundleSize, setBundleSize] = useState(2)
  const [discount, setDiscount] = useState('')
  const [fromDate, setFromDate] = useState(new Date())
  const [toDate, setToDate] = useState(new Date().setDate(new Date().getDate() + 1))
  const [ width, setWidth] = useState(window.innerWidth)

  const handleBundleChange = (e) => {
    setBundleSize(e.target.value)
  }

  const handleDiscountChange = (e) => {
    setDiscount(e.target.value)
  }

  const handleFromDateChange = (date) => {
    setFromDate(date)
    console.log(new Date().getDate() >= new Date(toDate).getDate() && new Date().getMonth() >= new Date(toDate).getMonth() && new Date().getFullYear() >= new Date(toDate).getFullYear())
    if (new Date().getDate() >= new Date(toDate).getDate() || new Date().getMonth() >= new Date(toDate).getMonth() || new Date().getFullYear() >= new Date(toDate).getFullYear()) {
      setToDate(new Date(date).setDate(new Date(date).getDate() + 1))
    }
  }

  const handleToDateChange = (date) => {
    setToDate(date)
  }

  const handleAdd = async () => {
    setLoading(true);
    const body = {
      bundle_size: bundleSize,
      discount: discount,
      start_date: moment(fromDate).format('YYYY-MM-DD'),
      end_date: moment(toDate).format('YYYY-MM-DD'),
    }
    try {
      const { data } = await axiosInstance.post(endpoints.bundleCourse.addDiscount, body);
      if (data?.status_code === 200) {
        setLoading(false);
        getDiscountList()
        handleClose()
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  const handleClose = () => {
    setOpen(false)
    setFromDate(new Date())
    setToDate(new Date().setDate(new Date().getDate() + 1))
    setDiscount('')
    setBundleSize(2)
  }

  async function getDiscountList() {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(`${endpoints.bundleCourse.getDiscountList}?page=${currentPage}&page_size=${rowsPerPage}`);
      if (data?.status_code === 200) {
        setLoading(false);
        setDiscountList(data?.result || []);
        setTotalCount(data?.count)
        setTotalPage(data?.total_pages)
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  async function handleCreateDiscount(discount, size) {
    const payload = {
      bundle_size: size,
      discount: parseInt(discount, 10),
    };
    setLoading(true);
    try {
      const { data } = await axiosInstance.post(endpoints.bundleCourse.getDiscountList, {
        ...payload,
      });
      if (data?.status_code === 200) {
        setLoading(false);
        getDiscountList();
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  useEffect(() => {
    getDiscountList();
    window.addEventListener("resize", updateWidth);
    return () => window.removeEventListener("resize", updateWidth);
  }, []);

  const updateWidth = () => {
    setWidth(window.innerWidth)
    console.log(window.innerWidth)
  }

  function handleOnChange(value, size, index, key = 'discount') {
    delayedQuery(value, size);
    setDiscountList((previous) => {
      const newList = [...previous];
      switch (key) {
        case key:
          newList[index][key] = value;
          return newList;
        default:
          return null;
      }
    });
  }

  const handleChangeStatus = async (item) => {
    setConfirmModalOpen(false)
    setIndexNo('')
    setLoading(true);
    try {
      const { data } = await axiosInstance.delete(`${endpoints.bundleCourse.deleteDiscount}/${item}`);
      if (data?.status_code === 200) {
        setLoading(false);
        getDiscountList();
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  const handleDelete = (item) => {
    setConfirmModalOpen(true)
    setIndexNo(item.id)
  }

  useEffect(() => {
    getDiscountList()
  }, [currentPage, rowsPerPage])

  const handleRowsChange = (event) => {
    setRowsPerPages(parseInt(event.target.value))
    setCurrentPage(1)
  }

  const handleFirstPage = () => {
    setCurrentPage(1)
  }

  const handleLastPage = () => {
    setCurrentPage(totalPages)
  }

  const handleNextPage = () => {
    setCurrentPage(currentPage + 1)
  }

  const handlePreviousPage = () => {
    setCurrentPage(currentPage - 1)
  }

  const TablePaginationAction = () => {
    return (
      <div style={{ flexShrink: 0, marginLeft: 10 }}>
        <IconButton
          onClick={handleFirstPage}
          disabled={currentPage === 1}
          aria-label="first page"
        >
          <FirstPage />
        </IconButton>
        <IconButton onClick={handlePreviousPage} disabled={currentPage === 1} aria-label="previous page">
          <KeyboardArrowLeft />
        </IconButton>
        <IconButton
          onClick={handleNextPage}
          disabled={currentPage >= Math.ceil(totalCount / rowsPerPage)}
          aria-label="next page"
        >
          <KeyboardArrowRight />
        </IconButton>
        <IconButton
          onClick={handleLastPage}
          disabled={currentPage >= Math.ceil(totalCount / rowsPerPage)}
          aria-label="last page"
        >
          <LastPage />
        </IconButton>
      </div>)
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Bundle Standard Discount'
        />
      </div>
      {/* <MuiPickersUtilsProvider utils = {DateFnsUtils}> */}
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <div>
            <Button
              variant='contained'
              color='primary'
              size='small'
              style={{ fontSize: '12px', marginBottom: 15 }}
              onClick={() => setOpen(true)}
            >
              Add Bundle Discount
            </Button>
          </div>
          </Grid>
          <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Table>
              <TableHead style={{ padding: '0px 0px' }}>
                <TableRow style={{ padding: '0px 0px' }}>
                  <TableCell style={{ padding: '5px 0px' }}>Bundle Size</TableCell>
                  <TableCell style={{ padding: '5px 0px' }}>Discount (%) </TableCell>
                  <TableCell style={{ padding: '5px 0px' }}>Effective From Date </TableCell>
                  <TableCell style={{ padding: '5px 0px' }}>Effective To Date </TableCell>
                  <TableCell style={{ padding: '5px 0px' }}>Delete </TableCell>
                </TableRow>
              </TableHead>
              <TableBody style={{ padding: '0px 0px' }}>
                {discountList &&
                  discountList?.map((item, index) => (
                    <TableRow key={item} style={{ padding: '0px 0px' }}>
                      <TableCell style={{ padding: '5px 0px' }}>
                        {item?.bundle_size || ''}
                      </TableCell>
                      <TableCell style={{ padding: '5px 0px' }}>
                        {/* <TextField
                          className='inputFiled'
                          type='number'
                          margin='dense'
                          value={item?.discount || ''}
                          onChange={(e) =>
                            e.target.value > -1 &&
                            e.target.value < 101 &&
                            handleOnChange(
                              e.target.value.trimLeft(),
                              item.bundle_size,
                              index
                            )}
                        /> */}
                        {item?.discount || ''}
                      </TableCell>
                      <TableCell style={{ padding: '5px 0px' }}>
                        {/* <KeyboardDatePicker
                            margin="dense"
                            style = {{width : 150}}
                            id="date-picker-dialog"
                            format="MM/dd/yyyy"
                            value={new Date()}
                            onChange={(value => console.log(value))}
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                          /> */}
                        {/* {moment(new Date()).format('DD-MM-YYYY')} */}
                        {item?.start_date ? moment(item?.start_date).format('DD-MM-YYYY') : ''}
                      </TableCell>
                      <TableCell style={{ padding: '5px 0px' }}>
                        {/* <KeyboardDatePicker
                            margin="dense"
                            style = {{width : 150}}
                            id="date-picker-dialog"
                            format="MM/dd/yyyy"
                            value={new Date()}
                            onChange={(value => console.log(value))}
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                          /> */}
                        {/* {moment(new Date()).format('DD-MM-YYYY')} */}
                        {item?.end_date ? moment(item?.end_date).format('DD-MM-YYYY') : ''}
                      </TableCell>
                      <TableCell style={{ padding: '5px 0px' }}>
                        {item?.is_active &&
                        <Button
                          variant='contained'
                          color='primary'
                          size='small'
                          style={{ fontSize: '12px', width: '40%', marginTop: 10 }}
                          disabled={item?.is_active ? false : true}
                          onClick={() => handleDelete(item)}
                        >
                          Delete
                        </Button>}
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>


              <TableFooter>
                {discountList && discountList.length !== 0 &&
                    <TablePagination
                      style = {{paddingRight : width/2.5 - width/24}}
                      rowsPerPageOptions=''
                      colSpan={12}
                      count={totalCount}
                      rowsPerPage={rowsPerPage}
                      page={currentPage - 1}
                      SelectProps={{
                        inputProps: { 'aria-label': 'rows per page' },
                        native: true,
                      }}
                      onChangePage={handleFirstPage}
                      onChangeRowsPerPage={handleRowsChange}
                      ActionsComponent={TablePaginationAction}
                    />
                  }
              </TableFooter>
            </Table>
          </Card>
        </Grid>
      </Grid>
      {/* </MuiPickersUtilsProvider> */}
      <Modal
        open={open}
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
        onClose={handleClose}>
        <ModalContent
          bundleSize={bundleSize}
          discount={discount}
          fromDate={fromDate}
          toDate={toDate}
          handleBundleChange={handleBundleChange}
          handleDiscountChange={handleDiscountChange}
          handleFromDateChange={handleFromDateChange}
          handleToDateChange={handleToDateChange}
          handleClose={handleClose}
          handleAdd={handleAdd}
          loader = {loading}
        />
      </Modal>
      {loading && <Loader />}
      <ConfirmDialog open={confirmModalOpen} cancel={()=> { setConfirmModalOpen(false); setIndexNo('') }} confirm={()=> handleChangeStatus(indexNo)} title="Are You Sure To Delete ?" /> 
    </Layout>
  );
};

export default AolBundleDisconnect;
