import DateFnsUtils from '@date-io/date-fns'
import { Button, Card, FormControl, Grid, IconButton, InputLabel, MenuItem, Select, TextField, Typography } from '@material-ui/core'
import { CloseIcon } from '@material-ui/data-grid'
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import React from 'react'
import './style.scss'

const ModalContent = (props) => {
    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Card style={{ width: 400, display: 'flex', flexDirection: 'column', alignItems: 'center', padding: 20 }}>

                <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%', alignItems: 'center' }}>
                    <CloseIcon style={{ color: 'white' }} />
                    <Typography variant='h6' color='primary' style={{ fontSize: 20, fontWeight: 'bold', marginBottom: 20 }}>Add Bundle Discount</Typography>
                    <IconButton onClick={() => props.handleClose()} size='small' style={{ marginTop: -20 }}>
                        <CloseIcon />
                    </IconButton>
                </div>


                <FormControl color='primary' variant="outlined" size='small' style={{ width: '75%', marginBottom: 10 }}>
                    <InputLabel>Bundle Size</InputLabel>
                    <Select
                        value={props.bundleSize}
                        onChange={(e) => props.handleBundleChange(e)}
                        label="Bundle Size"
                    >
                        <MenuItem value={2}>2</MenuItem>
                        <MenuItem value={3}>3</MenuItem>
                        <MenuItem value={4}>4</MenuItem>
                        <MenuItem value={5}>5</MenuItem>
                        <MenuItem value={6}>6</MenuItem>
                    </Select>
                </FormControl>

                <TextField
                    label="Discount(%)"
                    variant="outlined"
                    size='small'
                    type='number'
                    color='primary'
                    value={props.discount}
                    className='textfield-number'
                    style={{ width: '75%', marginBottom: 10 }}
                    onChange={(e) => {
                        if ((e.target.value > 0 && e.target.value <= 50 && new RegExp(/^\d+(\.\d{0,2})?$/).test(e.target.value))|| e.target.value == '') {
                            props.handleDiscountChange(e)
                        }
                    }} />

                <KeyboardDatePicker
                    margin="dense"
                    label='Effective From Date'
                    format="dd/MM/yyyy"
                    inputVariant='outlined'
                    size='small'
                    value={props.fromDate}
                    style={{ width: '75%', marginBottom: 10 }}
                    minDate = {new Date()}
                    onChange={(value => props.handleFromDateChange(value) )}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />

                <KeyboardDatePicker
                    margin="dense"
                    label='Effective To Date'
                    format="dd/MM/yyyy"
                    inputVariant='outlined'
                    size='small'
                    value={props.toDate}
                    minDate={new Date(props.fromDate).setDate(new Date(props.fromDate).getDate() + 1)}
                    onChange={(value => props.handleToDateChange(value) )}
                    style={{ width: '75%', marginBottom: 10 }}
                    KeyboardButtonProps={{
                        'aria-label': 'change date',
                    }}
                />

                <Button
                    variant='contained'
                    color='primary'
                    size='small'
                    style={{ fontSize: '12px', width: '40%', marginTop: 10 }}
                    disabled = {(props.fromDate && props.toDate && props.bundleSize && props.discount) ? props.loader ? true : false : true}
                    onClick = {() => props.handleAdd()}
                >
                    Add
                </Button>
                
            </Card>
        </MuiPickersUtilsProvider>
    )
}

export default ModalContent