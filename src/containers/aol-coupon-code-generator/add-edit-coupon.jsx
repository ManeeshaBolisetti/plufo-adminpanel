/* eslint-disable no-nested-ternary */
import React, { useState, useContext, useEffect } from 'react';
import {
  Grid,
  Card,
  TextField,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
  Switch,
  Button,
} from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import './style.scss';
import Loader from '../../components/loader/loader';
import Layout from '../Layout';

const AddEditCoupon = () => {
  const location = useLocation();
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [edit, setEdit] = useState(false);
  const [loading, setLoading] = useState(false);
  const [couponDetais, setCouponDetails] = useState({
    code_name: '',
    code: '',
    coupon_details: '',
    discount: '',
    valid: '1',
    limit: '',
    min_amount: '',
    message: '',
    valid_from: '',
    valid_to: '',
    days: '',
    is_limit_applicable: false,
  });

  useEffect(() => {
    if (location.state && location.state.edit) {
      setEdit(true);
      setCouponDetails(location.state);
    }
  }, [location.state]);

  function handleOnchnage(value, key) {
    setCouponDetails((prev) => {
      const data = { ...prev };
      switch (key) {
        case key:
          data[key] = value;
          return data;
        default:
          return data;
      }
    });
  }

  function handleOnChangeData(e, key) {
    const selectedDateTo = new Date(e);
    const todayTo = new Date();
    todayTo.setDate(todayTo.getDate());
    if (
      new Date(selectedDateTo) <
      new Date(`${todayTo.getFullYear()}-${todayTo.getMonth() + 1}-${todayTo.getDate()}`)
    ) {
      setAlert('warning', 'Coupon cant be created for the past date');
      return;
    }
    if (key === 'valid_to') {
      if (!couponDetais.valid_from) {
        setAlert('warning', 'Enter Start Date');
      } else if (couponDetais.valid_from > e) {
        setAlert('warning', 'start date should not be greater than end date');
      } else {
        handleOnchnage(e, key);
      }
    } else {
      handleOnchnage(e, key);
    }
  }

  function handleTextField(key, name, type) {
    return (
      <TextField
        className='add-edit-text-field'
        value={couponDetais[key] || ''}
        disabled={edit && key === 'discount'}
        onChange={(e) => {
          if (type === 'number' && e.target.value > -1) {
            handleOnchnage(e.target.value.trimLeft(), key);
          } else if (type === 'date') {
            handleOnChangeData(e.target.value, key);
          } else if (key === 'code') {
            handleOnchnage(e.target.value.trimLeft().toUpperCase(), key);
          } else {
            handleOnchnage(e.target.value.trimLeft(), key);
          }
        }}
        margin='dense'
        variant='outlined'
        label={type === 'date' ? '' : name}
        helperText={type === 'date' ? name : ''}
        fullWidth
        type={type}
        placeholder={`Enter ${name}`}
      />
    );
  }

  function dateFormate(dateInd) {
    const dataNu =
      dateInd.getDate() && dateInd.getDate() > 9
        ? dateInd.getDate()
        : `0${dateInd.getDate()}`;
    const MounthNu =
      dateInd.getMonth() + 1 && dateInd.getMonth() + 1 > 9
        ? dateInd.getMonth() + 1
        : `0${dateInd.getMonth() + 1}`;
    const YearNu = dateInd.getDate() && dateInd.getFullYear();
    return `${YearNu}-${MounthNu}-${dataNu}`;
  }

  function convertToDate(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + parseInt(days, 10));
    return dateFormate(result);
  }

  function handleSubmit() {
    if (!couponDetais.code_name) {
      setAlert('warning', 'Enter Coupon name');
      return;
    }
    if (!couponDetais.code) {
      setAlert('warning', 'Enter Coupon code');
      return;
    }
    if (!couponDetais.coupon_details) {
      setAlert('warning', 'Enter Coupon details');
      return;
    }
    if (!couponDetais.discount) {
      setAlert('warning', 'Enter discount');
      return;
    }
    // if (!couponDetais.min_amount) {
    //   setAlert('warning', 'Enter minimum amount');
    //   return;
    // }
    if (!couponDetais.message) {
      setAlert('warning', 'Enter message');
      return;
    }
    if (!couponDetais.valid_from) {
      setAlert('warning', 'Enter start date');
      return;
    }
    if (couponDetais.valid === '1') {
      if (!couponDetais.valid_to) {
        setAlert('warning', 'Enter end date');
        return;
      }
    }
    if (couponDetais.valid === '2') {
      if (!couponDetais.days) {
        setAlert('warning', 'Enter no of days');
        return;
      }
    }
    if (couponDetais.is_limit_applicable) {
      if (!couponDetais.limit) {
        setAlert('warning', 'Enter no of applicable times');
        return;
      }
    }
    const data = { ...couponDetais };
    if (couponDetais.valid === '2') {
      data.valid_to = convertToDate(couponDetais.valid_from, couponDetais.days);
    }
    if (!couponDetais.is_limit_applicable) {
      data.limit = 0;
    }
    if (edit) {
      data.id = location.state.id;
      setLoading(true);
      axiosInstance
        .put(`${endpoints.couponGenerateApis.updateCouponApi}`, { ...data })
        .then((response) => {
          setLoading(false);
          if (response.data.status_code === 200) {
            history.push('/aol-view-coupons');
            setAlert('success', response.data.message);
          } else {
            setAlert('error', response.data.description);
          }
        })
        .catch((err) => {
          setAlert('success', err.response.data.description);
          setLoading(false);
        });
    } else {
      setLoading(true);
      axiosInstance
        .post(endpoints.couponGenerateApis.addCouponApi, { ...data })
        .then((response) => {
          setLoading(false);
          if (response.data.status_code === 200) {
            history.push('/aol-view-coupons');
            setAlert('success', response.data.message);
          } else {
            setAlert('error', response.data.description);
          }
        })
        .catch((err) => {
          setAlert('success', err.response.data.description);
          setLoading(false);
        });
    }
  }

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <Layout>
          <Grid container spacing={2} className='add-edit-coupon-body'>
            <Grid item md={12} xs={12} className='add-edit-coupon-top-head'>
              <CommonBreadcrumbs
                componentName='Coupon'
                childComponentName={edit ? 'Update Coupon' : 'Add Coupon'}
              />
            </Grid>
            <Grid item md={12} xs={12} className='add-edit-coupon-form-div'>
              <Card className='add-edit-coupon-form-card'>
                <Grid container spacing={2}>
                  <Grid item md={4} xs={12}>
                    {handleTextField('code_name', 'Coupon Name', 'text')}
                  </Grid>
                  <Grid item md={4} xs={12}>
                    {handleTextField('code', 'Coupon Code', 'text')}
                  </Grid>
                  <Grid item md={4} xs={12}>
                    {handleTextField('coupon_details', 'Coupon Details', 'text')}
                  </Grid>
                  <Grid item md={4} xs={12}>
                    {handleTextField('discount', 'Discount amount', 'number')}
                  </Grid>
                  <Grid item md={8} xs={12}>
                    {handleTextField('message', 'Message', 'text')}
                  </Grid>
                  {/* <Grid item md={4} xs={12}> */}
                  {/* {handleTextField('min_amount', 'Minimum Amount', 'number')} */}
                  {/* </Grid> */}
                  <Grid item md={3} xs={12}>
                    <Typography>Valid Thru</Typography>
                    <RadioGroup
                      value={couponDetais.valid || ''}
                      onChange={(e) => {
                        handleOnchnage(e.target.value, 'valid');
                        handleOnchnage('', 'valid_to');
                        handleOnchnage('', 'days');
                      }}
                    >
                      <Grid container spacing={2}>
                        <Grid item md={6} xs={6}>
                          <FormControlLabel value='1' control={<Radio />} label='Dates' />
                        </Grid>
                        <Grid item md={6} xs={6}>
                          <FormControlLabel value='2' control={<Radio />} label='Days' />
                        </Grid>
                      </Grid>
                    </RadioGroup>
                  </Grid>
                  <Grid item md={2} xs={12} style={{ marginTop: '20px' }}>
                    {handleTextField('valid_from', 'Start Date', 'date')}
                  </Grid>
                  <Grid item md={2} xs={12} style={{ marginTop: '20px' }}>
                    {couponDetais.valid === '1'
                      ? handleTextField('valid_to', 'End Date', 'date')
                      : handleTextField('days', 'No Of Days', 'number')}
                  </Grid>
                  <Grid item md={2} xs={12}>
                    <Typography>Applicable Times</Typography>
                    <span style={{ fontSize: '15px' }}>No Limit</span>
                    <Switch
                      checked={couponDetais.is_limit_applicable}
                      onChange={(e) => {
                        handleOnchnage(e.target.checked, 'is_limit_applicable');
                        handleOnchnage('', 'limit');
                      }}
                    />
                    <span style={{ fontSize: '15px' }}>Limited</span>
                  </Grid>
                  {couponDetais.is_limit_applicable ? (
                    <Grid item md={3} xs={12} style={{ marginTop: '20px' }}>
                      {handleTextField('limit', 'Applicable Times', 'number')}
                    </Grid>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
                  <Grid
                    container
                    spacing={2}
                    justify='space-between'
                    style={{ padding: '20px 10px' }}
                  >
                    <Button
                      onClick={() => {
                        window.history.back();
                      }}
                      style={{
                        border: '1px solid #FF6B6B',
                        color: '#014B7E',
                        backgroundColor: 'white',
                        borderRadius: '10px',
                      }}
                      size='small'
                    >
                      Back
                    </Button>
                    <Button
                      size='small'
                      variant='contained'
                      color='primary'
                      onClick={handleSubmit}
                    >
                      Submit
                    </Button>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
          </Grid>
        </Layout>
      )}
    </>
  );
};

export default AddEditCoupon;
