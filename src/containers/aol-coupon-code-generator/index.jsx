import React, { useState, useEffect, useContext } from 'react';
import {
  Grid,
  Button,
  TableHead,
  TableCell,
  Card,
  TableRow,
  Table,
  TableBody,
  IconButton,
  TablePagination,
  Typography,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import BlockIcon from '@material-ui/icons/Block';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
import './style.scss';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import ConfirmDialog from '../../components/confirm-dialog';
import { Pagination } from '@material-ui/lab';

const selectedCoupon = {
  code_name: '',
  code: '',
  coupon_details: '',
  discount: '',
  valid: '1',
  limit: '',
  min_amount: '',
  message: '',
  valid_from: '',
  valid_to: '',
  days: '',
  is_limit_applicable: false,
  edit: false,
};

const CouponCodeGenerator = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [couponList, setCourseList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState('');

  function getCouponList(pageNo) {
    setLoading(true);
    axiosInstance
      .get(`${endpoints.couponGenerateApis.getCouponApi}?page=${pageNo}&page_size=10`)
      .then((result) => {
        setLoading(false);
        if (result.status === 200) {
          setCourseList(result.data);
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }
  useEffect(() => {
    setPageNumber(1);
    getCouponList(pageNumber);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleUpdate() {
    setLoading(true);
    axiosInstance
      .put(`${endpoints.couponGenerateApis.updateCouponApi}`, { ...content })
      .then((response) => {
        setLoading(false);
        if (response.data.status_code === 200) {
          setOpen(false);
          setAlert('success', `Coupon Successfully ${content.key}`);
          getCouponList(pageNumber);
          setContent('');
        } else {
          setAlert('success', response.data.description);
        }
      })
      .catch((err) => {
        setAlert('success', err.response.data.description);
        setLoading(false);
      });
  }

  function addCoupon() {
    history.push({
      pathname: '/aol-add-edit-coupon-code',
      state: {
        ...selectedCoupon,
      },
    });
  }
  function handleEdit(item) {
    const data = { ...item };
    data.valid = '1';
    data.valid_from = item.valid_from.split('T')[0] || '';
    data.valid_to = item.valid_to.split('T')[0] || '';
    data.edit = true;
    history.push({
      pathname: '/aol-add-edit-coupon-code',
      state: {
        ...data,
      },
    });
  }
  function handlePagination(event, page) {
    event.preventDefault();
    setPageNumber(page);
    getCouponList(page);
  }

  return (
    <Layout>
      <Grid container spacing={2} className='aol-coupon-code-generator-body'>
        <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head'>
          <CommonBreadcrumbs componentName='Coupon' childComponentName='Apply Coupons' />
        </Grid>
        <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head1'>
          <Button
            variant='contained'
            color='primary'
            size='small'
            onClick={() => addCoupon()}
          >
            <AddCircleOutlineIcon />
            &nbsp;Add Coupon
          </Button>
        </Grid>
        <Grid item md={12} xs={12} className='aol-coupon-code-generator-table-div'>
          <Card className='aol-coupon-code-generator-table-card'>
            {couponList && couponList.result && couponList.result.length === 0 && (
              <Typography
                variant='h6'
                color='primary'
                style={{ textAlign: 'center', margin: '20px 0px' }}
              >
                Coupons Not Created
              </Typography>
            )}
            {couponList && couponList.result && couponList.result.length !== 0 && (
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell float='left'>S.No</TableCell>
                    <TableCell float='left'>Coupon Name</TableCell>
                    <TableCell float='left'>Coupon Code</TableCell>
                    <TableCell float='left'>Validity</TableCell>
                    <TableCell float='left'>Discount</TableCell>
                    <TableCell float='left'>Status</TableCell>
                    <TableCell float='left'>Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {couponList &&
                    couponList.result &&
                    couponList.result.length !== 0 &&
                    couponList.result.map((item, index) => (
                      <TableRow key={item.id}>
                        <TableCell float='left'>{index + 1}</TableCell>
                        <TableCell float='left'>{item.code_name}</TableCell>
                        <TableCell float='left'>{item.code}</TableCell>
                        <TableCell float='left'>
                          {new Date(item.valid_from).toLocaleDateString()}
                          &nbsp; to &nbsp;
                          {new Date(item.valid_to).toLocaleDateString()}
                        </TableCell>
                        <TableCell float='left'>{`${item.discount}`}</TableCell>
                        <TableCell float='left'>
                          {item.is_active ? (
                            <span style={{ color: 'green', fontSize: '15px' }}>
                              Active
                            </span>
                          ) : (
                            <span style={{ color: 'red', fontSize: '15px' }}>
                              In Active
                            </span>
                          )}
                        </TableCell>
                        <TableCell float='left'>
                          <IconButton
                            size='small'
                            title={item.is_active ? 'Dectivate' : 'Activate'}
                            onClick={
                              () => {
                                setOpen(true);
                                setContent({
                                  is_active: !item.is_active,
                                  id: item.id,
                                  key: item.is_active ? 'Dectivate' : 'Activate',
                                });
                              }
                              // handleUpdate({ is_active: !item.is_active, id: item.id })
                            }
                          >
                            {item.is_active ? (
                              <BlockIcon color='primary' />
                            ) : (
                              <RemoveCircleIcon color='primary' />
                            )}
                          </IconButton>
                          <IconButton
                            size='small'
                            onClick={
                              () => {
                                setOpen(true);
                                setContent({
                                  is_delete: !item.is_delete,
                                  id: item.id,
                                  key: 'Deleted',
                                });
                              }
                              // handleUpdate({ is_delete: !item.is_delete, id: item.id })
                            }
                          >
                            {/* <DeleteIcon color='primary' /> */}
                          </IconButton>
                          <IconButton size='small' onClick={() => handleEdit(item)}>
                            <EditIcon color='primary' />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                  <TableRow>
                    <TableCell colSpan='8'>
                      {couponList &&
                        couponList.result &&
                        couponList.result.length !== 0 && (
                          <Pagination
                            style={{ textAlign: 'center', display: 'inline-flex' }}
                            count={couponList && couponList.total_pages}
                            color='primary'
                            page={pageNumber}
                            onChange={handlePagination}
                          />
                        )}
                    </TableCell>
                  </TableRow>
                </TableBody>
                {/* {couponList && couponList.result && couponList.result.length !== 0 && (
                  <TablePagination
                    component='div'
                    count={couponList && couponList.total_pages}
                    rowsPerPage={10}
                    page={Number(pageNumber) - 1}
                    onChangePage={(e, pageNo) => {
                      getCouponList(pageNo + 1);
                      setPageNumber(pageNo + 1);
                    }}
                    rowsPerPageOptions={false}
                    style={{ textAlign: 'right' }}
                    className='table-pagination-view-group'
                  />
                )} */}
              </Table>
            )}
          </Card>
        </Grid>
      </Grid>
      {loading && <Loading />}
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => setOpen(false)}
          confirm={handleUpdate}
          title={`Are you sure to ${content && content.key}`}
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default CouponCodeGenerator;
