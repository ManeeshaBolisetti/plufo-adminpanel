import React from 'react';
import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Card,
  Typography
} from '@material-ui/core';
import CustomFiterImage from '../../components/custom-filter-image';
import { DateTimeConverter, addCommas } from '../../components/dateTimeConverter';

const AolViewAllBatches = ({ studentData, assign, page }) => {
  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={12} xs={12}>
          <Card>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Student Name</TableCell>
                      <TableCell align='left'>Date of Enrollment</TableCell>
                      <TableCell align='left'>Grade</TableCell>
                      <TableCell align='left'>Subject</TableCell>
                      <TableCell align='left'>Course</TableCell>
                      <TableCell align='left'>No. of Sessions</TableCell>
                      <TableCell align="left">Enrollment Number </TableCell>
                      <TableCell align='left'>Amount Paid</TableCell>
                      <TableCell align='left'>Batches</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {studentData?.length !== 0 ? (
                      studentData.map((item, index) => (
                        <TableRow>
                          <TableCell align='left'>
                            {(page - 1) * 10 + index + 1}
                          </TableCell>
                          <TableCell align='left'>{item?.name || ''}</TableCell>
                          <TableCell align='left'>
                            {item?.enrollment_date
                              ? DateTimeConverter(item?.enrollment_date)
                              : ''}
                          </TableCell>
                          <TableCell align='left'>{item?.grade_name || ''}</TableCell>
                          <TableCell align='left'>{item?.subject_name || ''}</TableCell>
                          <TableCell align='left'>
                            {item?.is_fixed ? 'Full Year' : 'Fixed' || ''}
                          </TableCell>
                          <TableCell align='left'>{item?.no_of_session || '0'}</TableCell>
                          <TableCell align="left">{item?.enrollment_no ||(
                             <Typography style={{color: 'red', fontFamily:'serif', fontWeight:600}}>NA</Typography>
                          )}</TableCell>
                          <TableCell align='left'>
                            {`${addCommas(item?.amount_paid, '')} ₹` || ''}
                          </TableCell>
                          <TableCell align='left'>
                            <Button
                              variant='contained'
                              color='primary'
                              size='small'
                              disabled={item?.batch_time_slot == null ? true : false}
                              style={{ fontSize: '11px' }}
                              onClick={() => assign(item)}
                            >
                              Assign Now
                            </Button>
                            {item?.batch_time_slot == null ? (
                              <p>{item?.contact}</p>
                            ) : null}
                          </TableCell>
                        </TableRow>
                      ))
                    ) : (
                      <TableRow>
                        <TableCell colspan='9'>
                          <CustomFiterImage label='Batches are not found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </>
  );
};

export default AolViewAllBatches;
