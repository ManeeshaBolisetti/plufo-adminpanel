import React, { useRef, useContext, useEffect } from 'react';
import { Grid, TextField, Button } from '@material-ui/core';
import debounce from 'lodash.debounce';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import { LocalizationProvider, DateRangePicker } from '@material-ui/pickers-4.2';
import { Autocomplete } from '@material-ui/lab';
import moment from 'moment';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';

const Filteration = ({
  setPage,
  getStudentsList,
  filterState,
  setFilterState,
  setLoading,
}) => {
  const { setAlert } = useContext(AlertNotificationContext);
  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200 || data?.status_code === 201 || data) {
        if (type === 'grade') {
          setFilterState((prev) => ({ ...prev, gradeList: data?.data }));
        } else if (type === 'subject') {
          setFilterState((prev) => ({ ...prev, subjectList: data?.result }));
        }
        else if (type === 'teacher') {
            console.log("teacher:",data);
            setFilterState((prev) => ({ ...prev, teacherList: data?.data }));
        }
        // else if (type === 'courses') {
        //     console.log("course:",data);
        //     setFilterState((prev) => ({ ...prev, courseList: data }));
        // }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  useEffect(() => {
    ApiCall(`${endpoints.communication.subjectList}?branch_id=1`, 'subject');
    ApiCall(`${endpoints.communication.grades}?branch_id=1`, 'grade');
    ApiCall(`${endpoints.aolBatchAssign.getTeacherList}`, 'teacher');
    // ApiCall(`${endpoints.communication.courses}`, 'courses');
  }, []);

  const delayedQuery = useRef(
    debounce((q, state) => {
      setPage(1);
      getStudentsList(
        1,
        q,
        state?.selectedGrade?.map((grade) => grade?.grade_id) || [],
        state?.selectedSubject?.id || '',
        state?.selectedTeacher?.tutor_id || '',
        // state?.selectedCourse?.id || '',
        // state?.startDate || '',
        // state?.endDate || ''
      );
    }, 1000)
  ).current;

  return (
    <Grid container spacing={2}>
      <Grid item md={4} xs={12}>
        <Autocomplete
          size='small'
          id='grades'
          multiple
          className='dropdownIcon'
          options={filterState?.gradeList || []}
          getOptionLabel={(option) => option?.grade__grade_name || ''}
          filterSelectedOptions
          value={filterState?.selectedGrade || ''}
          onChange={(event, value) => {
            setFilterState({ ...filterState, selectedGrade: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              value?.map((grade) => grade?.grade_id) || [],
              filterState?.selectedSubject?.id || '',
              filterState?.selectedTeacher?.tutor_id || '',
              // filterState?.selectedCourse?.id || '',
            //   filterState?.startDate || '',
            //   filterState?.endDate || ''
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Grade'
              placeholder='Grade'
            />
          )}
        />
      </Grid>
      <Grid item md={4} xs={12}>
        <Autocomplete
          size='small'
          id='subject'
          className='dropdownIcon'
          options={filterState?.subjectList || []}
          getOptionLabel={(option) => option?.subject__subject_name || ''}
          filterSelectedOptions
          value={filterState?.selectedSubject || ''}
          onChange={(event, value) => {
            setFilterState({ ...filterState, selectedSubject: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
              value?.id || '',
              filterState?.selectedTeacher?.tutor_id || '',
              // filterState?.selectedCourse?.id || '',
            //   filterState?.startDate || '',
            //   filterState?.endDate || ''
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Subject'
              placeholder='Subject'
            />
          )}
        />
      </Grid>
            <Grid item md={4} xs={12}>
                <Autocomplete
                size='small'
                id='teacher'
                className='dropdownIcon'
                options={filterState?.teacherList || []}
                getOptionLabel={(option) => option?.name || ''}
                filterSelectedOptions
                value={filterState?.selectedTeacher || ''}
                onChange={(event, value) => {
                    setFilterState({ ...filterState, selectedTeacher: value });
                    setPage(1);
                    getStudentsList(
                    1,
                    filterState?.search || '',
                    filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                    filterState?.selectedSubject?.id || '',
                    value?.tutor_id || '',
                    // filterState?.selectedCourse?.id || '',
                    //   filterState?.startDate || '',
                    //   filterState?.endDate || ''
                    );
                }}
                renderInput={(params) => (
                    <TextField
                    {...params}
                    size='small'
                    variant='outlined'
                    label='Teacher'
                    placeholder='Teacher'
                    />
                )}
                />
            </Grid>
      {/* <Grid item md={4} xs={12}>
        <Autocomplete
          size='small'
          id='course'
          className='dropdownIcon'
          options={filterState?.courseList || []}
          getOptionLabel={(option) => option?.course_name || ''}
          filterSelectedOptions
          value={filterState?.selectedCourse || ''}
          onChange={(event, value) => {
            setFilterState({ ...filterState, selectedCourse: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
              filterState?.selectedSubject?.id || '',
              filterState?.selectedTeacher?.tutor_id || '',
              value?.id || '',
            //   filterState?.startDate || '',
            //   filterState?.endDate || ''
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Course'
              placeholder='Course'
            />
          )}
        />
      </Grid> */}
      {/* <Grid item md={4} xs={12}>
        <LocalizationProvider dateAdapter={MomentUtils}>
          <DateRangePicker
            startText='Select-date-range'
            value={filterState?.fullDate}
            onChange={(newValue) => {
              setFilterState((prev) => ({ ...prev, fullDate: newValue }));
              setFilterState((prev) => ({
                ...prev,
                startDate: moment(newValue?.[0])?.format()?.split('T')?.[0],
              }));
              setFilterState((prev) => ({
                ...prev,
                endDate: moment(newValue?.[1])?.format()?.split('T')?.[0],
              }));
              setPage(1);
              getStudentsList(
                1,
                filterState?.search || '',
                filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                filterState?.selectedSubject?.id || '',
                moment(newValue?.[0])?.format()?.split('T')?.[0] || '',
                moment(newValue?.[1])?.format()?.split('T')?.[0] || ''
              );
            }}
            renderInput={({ inputProps, ...startProps }, endProps) => {
              return (
                <>
                  <TextField
                    {...startProps}
                    inputProps={{
                      ...inputProps,
                      value: `${inputProps.value} - ${endProps.inputProps.value}`,
                      readOnly: true,
                    }}
                    size='small'
                    style={{ minWidth: '100%' }}
                    helperText=''
                  />
                </>
              );
            }}
          />
        </LocalizationProvider>
      </Grid> */}
      <Grid item md={6} xs={12}>
        <CustomSearchBar
          value={filterState?.search}
          setValue={(value) => {
            setFilterState({ ...filterState, search: value });
            delayedQuery(value, filterState);
          }}
          onChange={(e) => {
            setFilterState({ ...filterState, search: e.target.value.trimLeft() });
            delayedQuery(e.target.value.trimLeft(), filterState);
          }}
          label=''
          placeholder='Search Batches'
        />
      </Grid>
      <Grid item md={12} xs={12}>
      <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
        <Button
          style={{ borderRadius: '10px' }}
          size='small'
          fullWidth
          variant='contained'
          color='primary'
          onClick={() => {
            setPage(1);
            getStudentsList(1);
            setFilterState((prev) => ({
              ...prev,
              selectedGrade: [],
              selectedSubject: [],
              selectedTeacher: [],
              // selectedCourse: [],
            //   startDate: '',
            //   endDate: '',
              // fullDate: [],
              search: '',
            }));
          }}
        >
          Clear Filter
        </Button>
      </Grid>

      </Grid>
    </Grid>
  );
};

export default Filteration;
