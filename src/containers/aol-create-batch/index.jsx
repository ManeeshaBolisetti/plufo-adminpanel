/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Grid, Paper, TextField } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import Autocomplete from '@material-ui/lab/Autocomplete';
import debounce from 'lodash.debounce';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import AolViewAllBatches from './aol-view-all-batches';

const AolCreateBatch = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentList, setStudentList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [page, setPage] = useState(1);
  const [selectDropDown,setSelectDropDown] =useState([])
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)

  async function getStudentList(pageNo = page, searchVal = searchValue) {
    if(branchId ){
      setLoading(true);
      const searchParm = searchVal ? `&term=${searchVal}` : '';
      try {
        const { data } = await axiosInstance.get(
          `${
            endpoints.aolBatch.getUnAssighedList
          }?page=${pageNo}&page_size=${10}${searchParm}&branch=${branchId}`
        );
        if (data?.status_code === 200) {
          setStudentList(data);
          setLoading(false);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    }
  }

  useEffect(() => {
    if(branchId || searchValue){
      getStudentList()
    }
  }, [branchId, searchValue])

  useEffect(() => {
    const quiryDetails = new URLSearchParams(window.location.search);
    setPage(parseInt(quiryDetails.get('page'), 10) || 1);
    const searchVal = quiryDetails.get('term') || '';
    setSearchValue(searchVal);
    getStudentList(parseInt(quiryDetails.get('page'), 10) || 1, searchVal);
  }, []);

  const handlePagination = (event, page) => {
    setPage(page);
    const searchParm = searchValue ? `&term=${searchValue}` : '';
    history.push(`/aol-batch-create/?page=${page}${searchParm}`);
    getStudentList(page, searchValue);
  };

  function handleAssign(data) {
    // console.log(data, 'red ====')
    history.push({
      pathname: `/aol-batch-assign`,
      state: {
        ...data,
      },
    });
  }

  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      setSelectDropDown(res.data.data)

    })
  }, [])

  const debounceFunc = useRef(
    debounce((q) => {
      setPage(1);
      const searchParm = q ? `&term=${q}` : '';
      history.push(`/aol-batch-create/?page=${1}${searchParm}`);
      getStudentList(1, q);
    }, 1000)
  ).current;

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Unassigned Students'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto', }}>
      <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
              onChange={(event, value) => {
                // console.log(value,'ml999999')
                setBranchId(value?.id)
                setBranch(value)
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>

        <Grid item md={12} xs={12}>
          <Grid container direction='row' >
            <Grid item md={6} xs={12}>
              <CustomSearchBar
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value.trimLeft());
                  debounceFunc(e.target.value.trimLeft());
                }}
                label=''
                placeholder='Search Student Name'
                setValue={(value) => {
                  setSearchValue(value);
                  debounceFunc(value);
                }}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <AolViewAllBatches
            studentData={studentList?.result || []}
            assign={handleAssign}
            page={page}
          />
          {studentList?.result?.length ? (
            <Paper style={{ textAlign: 'center', padding: '10px' }}>
              <Pagination
                style={{ textAlign: 'center', display: 'inline-flex' }}
                onChange={handlePagination}
                count={studentList?.total_pages}
                color='primary'
                page={page}
              />
            </Paper>
          ) : (
            ''
          )}
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default AolCreateBatch;
