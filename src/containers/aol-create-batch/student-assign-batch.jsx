import React, { useContext, useState, useEffect } from 'react';
import {
  Card,
  Grid,
  Typography,
  Button,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import CustomFiterImage from '../../components/custom-filter-image';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
// import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import StudentOnGoingBatches from './student-ongoing-batches';
import CreateStudentBatchModel from './create-student-batch-model';
import ConfirmDialog from '../../components/confirm-dialog';
import StudentAssignModel from './student-assign-model';
import { DateTimeConverter, ConverTime } from '../../components/dateTimeConverter';
import './style.scss';
import TablePagination from '@material-ui/core/TablePagination';
import Filteration from './filteration';
import moment from 'moment';
import { Pagination } from '@material-ui/lab';

const StudentAssignBatch = () => {
  const location = useLocation();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [userDetails, setUserDetails] = useState([]);
  const [studentBatchDetails, setStudentBatchDetails] = useState({});
  const [open, setOpen] = useState(false);
  const [batchList, setBatchList] = useState([]);
  const { setAlert } = useContext(AlertNotificationContext);
  const [openCreate, setOpenCreate] = useState(false);
  const [openAssign, setOpenAssign] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  const [page,setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [timeSlot, setTimeSlot] = useState('');
  const [index, setIndex] = useState(1);
  const [filterState, setFilterState] = useState({
    gradeList: [],
    selectedGrade: [],
    teacherList:[],
    selectedTeacher: [],
    subjectList: [],
    selectedSubject: [],
    // startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
    // endDate: moment()?.format()?.split('T')?.[0],
    fullDate: [],
    search: '',
  });

  const limit = 10;
  // console.log( studentBatchDetails, 'hjk=================');
  

  // useEffect(() => {
  //   console.log(studentBatchDetails, 'bbgg@@')
   
  // }, [])
  const batchViewCard = (value) => {
    return (
      <Grid item md={2} xs={12}>
        <Card
          hovable
          style={{
            background: '#FFD9D9',
            padding: '8px',
            borderTop: '5px solid #CECECE',
            borderBottom: '5px solid #CECECE',
            textAlign: 'center',
          }}
        >
          <Typography style={{fontSize:'13px', fontWeight:'bold'}}>{value}</Typography>
        </Card>
      </Grid>
    );
  };

  async function apiCall(API, KEY) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(API);
      if (data?.status_code === 200) {
        setLoading(false);
        if (KEY === 'batchList') {
          setTotalCount(data.count)
          setUserDetails(data);
        }
        if (KEY === 'batches') {
          setBatchList(data);
          setOpen(true);
        }
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }
  async function getStudentsList(pageNo, searchVal,grade,subject, teacher){
    // console.log({pageNo, searchVal,teacher},'maskkkk')
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = grade?.length ? `&grades=${grade}` : '';
    const selectedSubject = subject ? `&subject_id=${subject}` : '';
    const selectedTeacher = teacher ? `&teacher_id=${teacher}` : '';
    apiCall(
      `${endpoints.aolBatchAssign.getStudentAllBatchesFilter}?page=${pageNo}&page_size=${limit}${selectedTeacher}${searchValue}${selectedGrade}${selectedSubject}`,
      'batchList'
    );
  }
  function handlePagination(event, page) {
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * 10);
    }
    getStudentsList(
      page,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.selectedSubject?.id || '',
      filterState?.selectedTeacher?.tutor_id || '',
      // filterState?.courseList?.id || ''
      // filterState?.startDate || '',
      // filterState?.endDate || ''
    );
  }

  useEffect(() => {
    setStudentBatchDetails(location.state);
    setPage(1)
    getStudentsList(
      1,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.teacherList?.id || '',
      // filterState?.courseList?.id || ''
    )
    // apiCall(
    //   `${endpoints.aolBatch.getRunningBatchList}?course=${location?.state?.course_id}&course_detail_id=${location?.state?.course_details_id}&course_order_id=${location?.state?.id}&page=${1}&page_size=${limit}`,
    //   'batchList'
    // );
    // console.log( studentBatchDetails.batch_start_time, 'hjk=================');

  }, [location?.state]);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    },450)
  },[page])

  const handleChangePage = (event, newPage) => {
    setPage(newPage+1)
  }

  const handleGoBack = () => {
    setPage(1)
  };

  const TitleCards = (label) => {
    return (
      <Card
        style={{
          width: 'fit-content',
          background: '#707070',
          padding: '8px 20px',
          borderTop: '5px solid #CECECE',
          borderBottom: '5px solid #CECECE',
          textAlign: 'center',
        }}
      >
        <Typography style={{ color: 'white' }}>{label}</Typography>
      </Card>
    );
  };

  function handleAssign(data) {
    setOpenAssign(true);
    setSelectedItem(data);
  }

  async function handleSubmitAssign(info) {
    setLoading(true);
    const payload = {
      ...info,
      batch_id: selectedItem?.id,
    };
    try {
      const { data } = await axiosInstance.put(endpoints.aolBatch.assignBatch, {
        ...payload,
      });
      if (data?.status_code === 200) {
        setLoading(false);
        setOpenAssign(false);
        setSelectedItem('');
        setAlert('success', data?.message);
        history.goBack();
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Unassigned Students'
          childComponentNameNext='Assign Batch'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Grid container spacing={2} direction='row'>
                {TitleCards(studentBatchDetails?.name || '')}
                &nbsp;&nbsp;&nbsp;
                {TitleCards(
                  studentBatchDetails?.is_fixed ? 'Yearly Course' : 'Fixed Course' || ''
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={6}>
              <Grid container spacing={2}>
                {batchViewCard('Batch Size')}
                {batchViewCard('No. of Days')}
                {batchViewCard('Time Slot')}
                {batchViewCard('No. of Session')}
                {batchViewCard('Start Date')}
              </Grid>
            </Grid>
            <Grid item md={12} xs={6}>
              <Grid container spacing={2}>
                {batchViewCard(`1 : ${studentBatchDetails?.batch_size}`)}
                {batchViewCard(studentBatchDetails?.days?.[0] || '')}
                {batchViewCard(
                  studentBatchDetails?.batch_time_slot
                    ? `${ConverTime(
                        studentBatchDetails?.batch_time_slot?.split('-')[0]
                      )} - ${ConverTime(
                        studentBatchDetails?.batch_time_slot?.split('-')[1]
                      )}`
                    : '' || ''
                )}

                {/* {console.log(studentBatchDetails?.batch_time_slot.replaceAll('-', ' ')), 'fkj66666')} */}
                {/* console.log(s.replaceAll('-', ' '));  */}
                {/* {console.log(studentBatchDetails?.batch_time_slot),'fkj66666'} */}
                {/* {console.log(`${studentBatchDetails?.batch_time_slot}`.slice(0,8),'kll999=======')} */}
                {/* parseInt(a.slice(0, 2)) > 11
        ? parseInt(a.slice(0, 2)) - 12 + `:${a.slice(3, 5)} PM`
        : a.slice(0, 5) + ' AM' */}
        {/* {console.log(parseInt(`${studentBatchDetails?.batch_time_slot}`.slice(0,8).slice(0, 2)) > 11
        ? parseInt(`${studentBatchDetails?.batch_time_slot}`.slice(0,8).slice(0, 2)) - 12 + `:${`${studentBatchDetails?.batch_time_slot}`.slice(0,8).slice(3, 5)} PM`
        : `${studentBatchDetails?.batch_time_slot}`.slice(0,8).slice(0, 5) + ' AM')} */}

                {batchViewCard(
                  studentBatchDetails.no_of_session ? studentBatchDetails.no_of_session : 'NA' 
                )}

                {batchViewCard(
                  studentBatchDetails?.reference_start_date
                    ? new Date(studentBatchDetails?.reference_start_date)
                        .toString()
                        .split('G')[0]
                        .substring(0, 16)
                    : 'NA'
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={3} xs={12} style={{ textAlign: 'center' }}>
              <Button
                style={{ borderRadius: '10px' }}
                variant='contained'
                className='action-btn'
                color='primary'
                onClick={() => {
                  apiCall(
                    `${endpoints.aolBatch.getRunningStudentBatchList}?student=${studentBatchDetails?.student_id}`,
                    'batches'
                  );
                }}
              >
                Assigned Batches
              </Button>
            </Grid>
            <Grid item md={3} xs={12} style={{ textAlign: 'center' }}>
              <Button
                style={{ borderRadius: '10px' }}
                className='action-btn'
                variant='contained'
                color='primary'
                onClick={() => setOpenCreate(true)}
              >
                Create New
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
              <Filteration
                setPage={setPage}
                getStudentsList={getStudentsList}
                filterState={filterState}
                setFilterState={setFilterState}
                setLoading={setLoading}
              />
            </Grid>

        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.NO</TableCell>
                      <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Batch Code</TableCell>
                      <TableCell align='left'>Seats Left</TableCell>
                      <TableCell align='left'>Start Date & Time</TableCell>
                      <TableCell align='left'>Days</TableCell>
                      <TableCell align='left'>No. of Sessions</TableCell>
                      <TableCell align='left'>Teacher Name</TableCell>
                      <TableCell align='left'>Assign</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userDetails?.results?.length !== 0 ? (
                      <>
                      {userDetails?.results?.map((item,i) => (
                        <>
                        {/* {console.log(i, 'IDEX ====>')} */}
                        <TableRow key={item.id}>
                          <TableCell align="left">{i+index}</TableCell>
                          <TableCell align='left'>
                            {item?.course?.course_name || ''}
                          </TableCell>
                          <TableCell align='left'>{item?.batch_name || ''}</TableCell>
                          <TableCell align='left'>
                              {item?.seat_left === 0 ? 0 : item?.seat_left || ''}
                          </TableCell>
                          <TableCell align='left'>
                              {item?.start_date
                                ? DateTimeConverter(item?.start_date)
                                : ''}
                            </TableCell>
                          <TableCell align='left'>
                            {item?.batch_days?.[0] || ''}
                          </TableCell>
                          <TableCell align='left'>
                              {item?.no_of_session || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.teacher_name || (
                                <Typography style={{ color: 'red' }}>NA</Typography>
                              )}
                            </TableCell>
                          {/* <TableCell align='left'>{item?.seat_left || ''}</TableCell>
                          <TableCell align='left'>
                            {item?.start_date ? DateTimeConverter(item?.start_date) : ''}
                          </TableCell>
                          <TableCell align='left'>{item?.teacher__name || ''}</TableCell> */}
                          <TableCell align='left'>
                            <Button
                              variant='contained'
                              size='small'
                              color='primary'
                              onClick={() => handleAssign(item)}
                            >
                              Assign
                            </Button>
                          </TableCell>
                        </TableRow>
                        </>
                      ))}
                         <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={userDetails?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colspan='5'>
                          <CustomFiterImage label='Batches are not found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
                {/* <div className="paginateData">
                  <TablePagination
                  component = 'div'
                  count={totalCount}
                  rowsPerPage={limit}
                  page={page-1}
                  onChangePage = {handleChangePage}
                  rowsPerPageOptions = {false}
                  />
                </div> */}
            </Grid>

          </Card>
        </Grid>
        <Grid item md={12} xs={12}>
          <Button
            onClick={() => history.goBack()}
            variant='contained'
            color='secondary'
            size='small'
          >
            Back
          </Button>
        </Grid>
      </Grid>
      {loading ? <Loader /> : ''}
      {open ? (
        <StudentOnGoingBatches
          open={open}
          close={() => {
            setOpen(false);
            setBatchList([]);
          }}
          data={batchList?.result || []}
        />
      ) : (
        ''
      )}
      {openCreate ? (
        <CreateStudentBatchModel
          open={openCreate}
          close={(info) => {
            setOpenCreate(false);
            if (info === 'success') {
              history.goBack();
            }
          }}
          selectedData={studentBatchDetails || {}}
        />
      ) : (
        ''
      )}
      {openAssign && location?.state?.is_fixed ? (
        <StudentAssignModel
          close={() => {
            setOpenAssign(false);
            setSelectedItem('');
          }}
          open={openAssign}
          handleSubmitAssign={handleSubmitAssign}
          selectedData={studentBatchDetails || {}}
        />
      ) : (
        ''
      )}
      {openAssign && location?.state?.is_fixed === false ? (
        <ConfirmDialog
          open={openAssign}
          cancel={() => {
            setOpenAssign(false);
            setSelectedItem('');
          }}
          selectedData={studentBatchDetails || {}}
          confirm={() => {
            handleSubmitAssign({
              student_id: studentBatchDetails?.student_id,
              order_id: studentBatchDetails?.id,
              start_date: null,
            });
          }}
          title='Are you sure to assign?'
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default StudentAssignBatch;
