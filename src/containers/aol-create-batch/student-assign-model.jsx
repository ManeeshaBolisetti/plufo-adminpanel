import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Button, TextField } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import CustomDialog from '../../components/custom-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import { getWeekList } from 'utility-functions';
import moment from 'moment';
import { Autocomplete } from '@material-ui/lab';

const StudentAssignModel = ({ open, close, selectedData, handleSubmitAssign }) => {
  const [startDate, setStartDate] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);
  const [dateList, setDateList] = useState([]);
  const weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  async function handleSubmit() {
    if (!startDate) {
      setAlert('warning', 'Select Start Date');
      return;
    }
    let sel = moment(startDate, "DD/MM/YYYY");
    let newDateString = sel.format("YYYY-MM-DD");
    // console.log(moment(startDate).format("YYYY-MM-DD"),'llp22========')
    // console.log(sel, 'hj22')
    const payload = {
      student_id: selectedData?.student_id,
      order_id: selectedData?.id,
      start_date: moment(startDate).format("YYYY-MM-DD"),
    };
    handleSubmitAssign(payload);
  }

  useEffect(()=>{
    let selday = selectedData?.days?.[0].split('/')[0];
    getWeeks(weekdays.indexOf(selday))

  },[])

  const getWeeks = (dayIdx) => {
    setDateList(getWeekList(dayIdx, selectedData))
  }

  const handleDate = (value) => {
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    const date = new Date(value)
    var selectedDay = weekday[date.getDay()];
    var retrievedDays = selectedData?.days?.[0];
    var ComparedDayslist = retrievedDays.split("/");
    console.log("Day:",ComparedDayslist.includes(selectedDay));
    if(ComparedDayslist.includes(selectedDay)){
      
      setStartDate(value);
    }
    else{
      setStartDate('')
      console.log()
      const message = "Please select Date as per Course Days";
      setAlert('error',message);
    }
  };

  function disableWeekends(value) {
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    const date = new Date(value)
    var selectedDay = weekday[date.getDay()];
    var retrievedDays = selectedData?.days?.[0];
    var ComparedDayslist = retrievedDays.split("/");
    console.log("Day:",ComparedDayslist.includes(selectedDay));
    if(ComparedDayslist.includes(selectedDay)){
      return
    }
    else{
      return value.getDay() === date.getDay();
    }
  
}

// console.log(startDate,'gh44')



  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Assign'
        maxWidth='xs'
        fullScreen={false}
        stylesProps={{ zIndex: '1' }}
      >
        <Grid container spacing={3} style={{ padding: '10px' }}>
          <Grid item md={12} xs={12}>
            <Typography>{`Days: ${selectedData?.days?.[0]}`}</Typography>
          </Grid>
          {/* <Grid item md={12} xs={12}>
            <MuiPickersUtilsProvider
              variant='outlined'
              utils={DateFnsUtils}
              style={{ zIndex: '9000000000' }}
            >
              <KeyboardDatePicker
                fullWidth
                autoOk
                disablePast
                placeholder='Start Date'
                helperText='Select Start Date'
                value={startDate || ''}
                // onChange={(data, value) => setStartDate(value)}
                onChange={(data, value) => handleDate(value)}
                onError={console.log}
                variant='outlined'
                minDate={new Date('2018-01-01')}
                format='yyyy-MM-dd'
              />
            </MuiPickersUtilsProvider>
          </Grid> */}
          {/* <Grid item md={12} xs={12}>
            <Autocomplete
              size='small'
              id='daysCombination'
              className='dropdownIcon'
              style={{ width: '100%' }}
              options={dateList}
              getOptionLabel={(option) => option || ''}
              filterSelectedOptions
              value={startDate || ''}
              onChange={(event, value) => setStartDate(value)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant='outlined'
                  label='Select Start Date'
                  placeholder='Select Start Date'
                />
              )}
            />
          </Grid> */}

          <Grid item md={12} xs={12}>
            <MuiPickersUtilsProvider
              utils={DateFnsUtils}
              style={{ zIndex: '9000000000'}}
            >
              <KeyboardDatePicker
                fullWidth
                autoOk
                className='dropdownIcon'
                variant="inline"
                inputVariant="outlined"
                size='small'
                placeholder='Select Start Date'
                shouldDisableDate={disableWeekends}
                helperText='Select Start Date'
                value={startDate || ''}
                onChange={(data, value) => handleDate(value)}
                onError={console.log}
                // minDate={new Date('2018-01-01')}
                minDate={new Date()}
                format='yyyy-MM-dd'
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='Select Start Date'
                    placeholder='Select Start Date'
                  />
                )}
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Button variant='contained' color='primary' onClick={() => handleSubmit()}>
              Submit
            </Button>
          </Grid>
        </Grid>
      </CustomDialog>
    </>
  );
};

export default StudentAssignModel;
