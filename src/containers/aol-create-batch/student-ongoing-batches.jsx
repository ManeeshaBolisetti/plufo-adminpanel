import React from 'react';
import {
  Grid,
  Table,
  TableCell,
  TableRow,
  TableBody,
  TableHead,
  Card,
} from '@material-ui/core';
import CustomDialog from '../../components/custom-dialog';
import { DateTimeConverter, ConverTime } from '../../components/dateTimeConverter';
import CustomFiterImage from '../../components/custom-filter-image';

const StudentOnGoingBatches = ({ open, close, data }) => {
  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Assigned Batches'
        maxWidth='lg'
        fullScreen={false}
      >
        <Grid container spacing={2}>
          <Grid item md={12} xs={12}>
            <Card style={{ padding: '10px', borderRadius: '10px' }}>
              <Grid container spacing={2}>
                <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                  <Table style={{ width: '100%', overflow: 'auto' }}>
                    <TableHead>
                      <TableRow>
                        <TableCell align='left'>Days</TableCell>
                        <TableCell align='left'>Subject</TableCell>
                        <TableCell align='left'>Start Date</TableCell>
                        <TableCell align='left'>Time Slot</TableCell>
                        <TableCell align='left'>Course Type</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {data?.length ? (
                        data.map((item) => (
                          <TableRow key={item.id}>
                            <TableCell align='left'>{item?.days?.[0] || ''}</TableCell>
                            <TableCell align='left'>{item?.subject_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.start_date
                                ? DateTimeConverter(item?.start_date)
                                : ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.batch_time_slot
                                ? `${ConverTime(
                                    item?.batch_time_slot?.split('-')[0]
                                  )} - ${ConverTime(
                                    item?.batch_time_slot?.split('-')[1]
                                  )}`
                                : ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.is_fixed ? 'Full Year' : 'Fixed'}
                            </TableCell>
                          </TableRow>
                        ))
                      ) : (
                        <TableRow>
                          <TableCell align='center' colSpan='5'>
                            <CustomFiterImage label='Batches Not Found' />
                          </TableCell>
                        </TableRow>
                      )}
                    </TableBody>
                  </Table>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </CustomDialog>
    </>
  );
};

export default StudentOnGoingBatches;
