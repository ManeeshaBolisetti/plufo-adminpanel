/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from 'react';
import {
  Grid,
  SwipeableDrawer,
  Typography,
  IconButton,
  Divider,
  TextField,
  Button,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import './style.scss';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';

const AddEditPaymentAddress = ({ studentId, open, edit, close, addressInfo }) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [addressDetails, setAdddressDetails] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (addressInfo) {
      setAdddressDetails(addressInfo);
    } else {
      setAdddressDetails({
        name: '',
        address1: '',
        address2: '',
        zip_code: '',
        country: '',
        state: '',
        city: '',
        phone_number: '',
        student: studentId,
      });
    }
  }, [open, addressInfo]);

  function handleOnChange(e, key) {
    setAdddressDetails((data) => {
      const newData = { ...data };
      switch (key) {
        case key:
          newData[key] = e;
          return newData;
        default:
          return null;
      }
    });
  }

  function handelSaveAddress() {
    if (!addressDetails.name) {
      setAlert('warning', 'Enter name');
      return;
    }
    if (!addressDetails.address1) {
      setAlert('warning', 'Enter address 1');
      return;
    }
    if (!addressDetails.country) {
      setAlert('warning', 'Enter country name');
      return;
    }
    if (!addressDetails.state) {
      setAlert('warning', 'Enter state name');
      return;
    }
    if (!addressDetails.city) {
      setAlert('warning', 'Enter city name');
      return;
    }
    if (!addressDetails.phone_number) {
      setAlert('warning', 'Enter phone number');
      return;
    }
    if (addressDetails.phoneNumber && addressDetails.phone_number.length !== 10) {
      setAlert('warning', 'Enter Valid phone number');
      return;
    }
    if (!addressDetails.zip_code) {
      setAlert('warning', 'Enter zip Code');
      return;
    }
    if (addressDetails.zip_code.length !== 6) {
      setAlert('warning', 'Enter Valid zip Code');
      return;
    }
    const data = { ...addressDetails };
    data.student = studentId;
    if (edit) {
      data.address_id = addressDetails.id;
    }

    setLoading(true);
    if (edit) {
      axiosInstance
        .put(`${endpoints.salesAol.updateAddressApi}`, { ...data })
        .then((result) => {
          setLoading(false);
          if (result && result.data.status_code === 200) {
            setAlert('success', result.data.message);
            close('success');
          } else {
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
    if (!edit) {
      axiosInstance
        .post(`${endpoints.salesAol.addAddressApi}`, { ...data })
        .then((result) => {
          setLoading(false);
          if (result && result.data) {
            setAlert('success', 'Address Successfully Created');
            close('success');
          } else {
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
  }

  return (
    <>
      {loading ? (
        <Loading message='Loading...' />
      ) : (
        <Grid container spacing={2} style={{ width: '100%', overflow: 'auto' }}>
          <Grid item md={12} xs={12}>
            <SwipeableDrawer
              className='my__swipable-address'
              anchor='right'
              open={open}
              onClose={close}
              onOpen={close}
            >
              <Grid container spacing={2} style={{ marginTop: '58px' }}>
                <Grid
                  item
                  md={12}
                  xs={12}
                  style={{ backgroundColor: '#FCEEEE', padding: '10px 20px' }}
                >
                  <Grid
                    container
                    spacing={2}
                    justify='space-between'
                    direction='row'
                    alignItems='center'
                  >
                    <Typography
                      variant='h5'
                      style={{
                        color: '#014B7E',
                        fontSize: '20px',
                        fontFamily: 'Raleway',
                        fontWeight: 'bold',
                      }}
                    >
                      {edit ? 'Update ' : 'Create '}
                      Address
                    </Typography>
                    <IconButton onClick={close}>
                      <CloseIcon style={{ color: '#FF6B6B' }} />
                    </IconButton>
                  </Grid>
                </Grid>
                <Divider style={{ margin: '5px 0px' }} />
                <Grid item md={12} xs={12} style={{ padding: '0px 40px' }}>
                  <Grid item md={12} xs={12}>
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      label='Name'
                      required
                      placeholder='Enter Name'
                      value={addressDetails.name || ''}
                      onChange={(e) => handleOnChange(e.target.value, 'name')}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      label='Address 1'
                      required
                      placeholder='Enter Address 1'
                      value={addressDetails.address1 || ''}
                      onChange={(e) => handleOnChange(e.target.value, 'address1')}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      label='Address 2'
                      placeholder='Enter Address 2'
                      value={addressDetails.address2 || ''}
                      onChange={(e) => handleOnChange(e.target.value, 'address2')}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      required
                      label='Phone Number'
                      placeholder='Enter Phone Number'
                      type='number'
                      className='addressFormInputNumber'
                      value={addressDetails.phone_number || ''}
                      onChange={(e) =>
                        e.target.value.length > -1 && e.target.value.length < 11
                          ? handleOnChange(e.target.value, 'phone_number')
                          : ''}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      label='Country'
                      required
                      placeholder='Enter Country Name'
                      value={addressDetails.country || ''}
                      onChange={(e) => handleOnChange(e.target.value, 'country')}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      label='State'
                      required
                      placeholder='Enter State Name'
                      value={addressDetails.state || ''}
                      onChange={(e) => handleOnChange(e.target.value, 'state')}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      required
                      label='City'
                      placeholder='Enter City Name'
                      value={addressDetails.city || ''}
                      onChange={(e) => handleOnChange(e.target.value, 'city')}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} className="duumy-box">
                    <TextField
                      fullWidth
                      variant='outlined'
                      margin='dense'
                      className='addressFormInputNumber'
                      required
                      label='Zip - Code'
                      placeholder='Enter Zip - Code'
                      type='number'
                      value={addressDetails.zip_code || ''}
                      onChange={(e) =>
                        e.target.value.length > -1 && e.target.value.length < 7
                          ? handleOnChange(e.target.value, 'zip_code')
                          : ''}
                    />
                  </Grid>
                  <Grid item md={12} xs={12} >
                    <Button
                      onClick={() => handelSaveAddress()}
                      variant='contained'
                      color='primary'
                      size='small'
                      fullWidth
                      style={{ marginTop: '5px' }}
                    >
                      Save
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </SwipeableDrawer>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default AddEditPaymentAddress;
