import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Divider, FormControl, Select, MenuItem, Input, TextField, Button } from '@material-ui/core';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
// import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
// import DeleteIcon from '@material-ui/icons/Delete';

const ApplyCoupon = ({ orderDetails, handleCallback, disabled, handlePaymentData }) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [coupon, setCoupon] = useState('');
  const [loading, setLoading] = useState(false);
  const [modePayment, setModePayment] = useState('');
  const [advanceAmt, setAdvanceAmt] = useState('');

  const [paidAmountType, setPaidAmountType] = useState('');
  const [fullPaymentMode, setfullPaymentMode] = useState('');
  const [partialAmt, setPartialAmt] = useState('');
  const [partialPaymentMode, setPartialPaymentMode] = useState('');


  useEffect(() => {
    if (orderDetails && orderDetails.coupon_apply) {
      setCoupon(orderDetails.coupon_name);
    }
  }, [orderDetails]);

  function addCommas(data) {
    if (data) {
      const amount = JSON.stringify(data);
      const x = amount.split('.');
      let x1 = x[0];
      const x2 = x.length > 1 ? `.${x[1]}` : '';
      const rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        // eslint-disable-next-line no-useless-concat
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    }
    return 0;
  }

  function prizeDetailsInfo(label, amount) {
    return (
      <>
        <Grid item md={6} xs={6}>
          <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
            {label}
          </Typography>
        </Grid>
        <Grid item md={6} xs={6}>
          <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'right' }}>
            {amount}
          </Typography>
        </Grid>
      </>
    );
  }

  function handleRemoveCoupon() {
    setLoading(true);
    axiosInstance
      .get(`${endpoints.salesAol.removeCouponApi}?order_id=${orderDetails.order_id}`)
      .then((res) => {
        setLoading(false);
        if (res && res.data.status_code === 200) {
          setAlert('success', res.data.message);
          handleCallback(
            `${endpoints.salesAol.cartListApi}?order_id=${orderDetails.order_id}`,
            'orderDetails'
          );
        } else {
          setAlert('error', res.data.description);
        }
      })
      .catch((err) => {
        setLoading(false);
        setAlert('error', err.response.data.message);
      });
  }

  function handleApplyCoupon() {
    setLoading(true);
    axiosInstance
      .post(endpoints.salesAol.applyCouponApi, {
        coupon,
        order_id: orderDetails.order_id,
      })
      .then((res) => {
        setLoading(false);
        if (res && res.data.status_code === 200) {
          setAlert('success', res.data.message);
          handleCallback(
            `${endpoints.salesAol.cartListApi}?order_id=${orderDetails.order_id}`,
            'orderDetails'
          );
        } else {
          setAlert('error', res.data.description);
        }
      })
      .catch((err) => {
        setLoading(false);
        setAlert('error', err.response.data.message);
      });
  }

  // const handleModePaymentChange = (event) => {
  //   setModePayment(event.target.value);
  // };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <Grid container spacing={2}>
          <Grid item md={12} xs={12}>
            <Divider style={{ margin: '5px 0px' }} />
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ marginTop: '10px' }}>
                <Typography
                  style={{
                    fontSize: '20px',
                    margin: '0px',
                    fontWeight: 'bold',
                    textAlign: 'center',
                    textDecoration: 'underline',
                  }}
                >
                  Order Summary
                </Typography>
              </Grid>
              <Grid item md={12} xs={12}>
                <Grid container spacing={2} align='middle' justify='flex-end'>
                  <Grid item md={2} xs={8} style={{ margin: '0px' }}>
                    <TextField
                      value={coupon}
                      margin='dense'
                      variant='standard'
                      size='small'
                      fullWidth
                      color='primary'
                      placeholder='Enter Coupon Code'
                      disabled={orderDetails.coupon_apply || disabled}
                      onChange={(e) => setCoupon(e.target.value.trimLeft().toUpperCase())}
                      helperText={
                        orderDetails.coupon_apply ? (
                          <Typography color='primary'>
                            Coupon code applied successfully
                          </Typography>
                        ) : (
                          ''
                        )
                      }
                    />
                  </Grid>
                  {coupon && (
                    <Grid item md={1} xs={4}>
                      {orderDetails && orderDetails.coupon_apply ? (
                        <Button
                          variant='contained'
                          disabled={disabled}
                          color='primary'
                          onClick={() => handleRemoveCoupon()}
                        >
                          Remove
                        </Button>
                      ) : (
                        <Button
                          variant='contained'
                          size='small'
                          color='secondary'
                          disabled={disabled}
                          onClick={() => handleApplyCoupon()}
                        >
                          Apply
                        </Button>
                      )}
                    </Grid>
                  )}
                </Grid>
              </Grid>
              {orderDetails &&
              orderDetails.actual_amount === orderDetails.final_amount ? (
                ''
              ) : (
                <>
                  {prizeDetailsInfo(
                    'Actual Price',
                    `₹ ${orderDetails && addCommas(orderDetails.actual_amount)}`
                  )}
                  {prizeDetailsInfo(
                    'Discount',
                    `${orderDetails && orderDetails.discount_per}%`
                  )}
                  {prizeDetailsInfo(
                    'Total saving amount',
                    `₹ ${orderDetails && addCommas(orderDetails.discount_amount)}`
                  )}
                </>
              )}
              {prizeDetailsInfo(
                'Your pay',
                `₹ ${orderDetails && addCommas(orderDetails.final_amount)}`
              )}
                <>
                  <Grid item md={6} xs={6}>
                    <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
                      {"Paid Amount"}
                    </Typography>
                  </Grid>
                  <Grid item md={6} xs={6}>
                    <FormControl style={{ width: "50%", float: 'right' }}>
                      <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={paidAmountType}
                        onChange={(event)=>{ 
                          setPaidAmountType(event.target.value)
                          setPartialAmt('')
                          handlePaymentData({
                            payment_type: event.target.value,
                            mode_of_payment: (event.target.value === 'FULL') ? fullPaymentMode : partialPaymentMode,
                            partial_amount: (event.target.value === 'PARTIAL') ? '' : null
                          })
                        }}
                      >
                        <MenuItem value={""}>Select Payment Type</MenuItem>
                        <MenuItem value={"FULL"}>FULL</MenuItem>
                        <MenuItem value={"PARTIAL"}>PARTIAL</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  {paidAmountType == "FULL" && 
                    <>
                      <Grid item md={6} xs={6}>
                        <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
                          {"Mode of Payment"}
                        </Typography>
                      </Grid>
                      <Grid item md={6} xs={6}>
                        <FormControl style={{ width: "50%", float: 'right' }}>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={fullPaymentMode}
                            onChange={(event)=>{ 
                              setfullPaymentMode(event.target.value)
                              handlePaymentData({
                                payment_type: paidAmountType,
                                mode_of_payment: event.target.value,
                                partial_amount: null
                              })
                            }}
                          >
                            <MenuItem value={"UPI"}>UPI</MenuItem>
                            <MenuItem value={"BANK TRANSFER"}>BANK TRANSFER</MenuItem>
                            <MenuItem value={"RAZOR PAY"}>RAZOR PAY</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                    </>
                  }
                   {paidAmountType == "PARTIAL" && 
                    <>
                      <Grid item md={6} xs={6}>
                        <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
                          {"Mode of Payment"}
                        </Typography>
                      </Grid>
                      <Grid item md={6} xs={6}>
                        <FormControl style={{ width: "50%", float: 'right' }}>
                          <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={partialPaymentMode}
                            onChange={(event)=>{ 
                              setPartialPaymentMode(event.target.value)
                              setPartialAmt('')
                              handlePaymentData({
                                payment_type: paidAmountType,
                                mode_of_payment: event.target.value,
                                partial_amount: ''
                              })
                            }}
                          >
                            <MenuItem value={"UPI"}>UPI</MenuItem>
                            <MenuItem value={"BANK TRANSFER"}>BANK TRANSFER</MenuItem>
                            <MenuItem value={"RAZOR PAY"}>RAZOR PAY</MenuItem>
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid item md={6} xs={6}>
                        <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
                          {"Amount"}
                        </Typography>
                      </Grid>
                      <Grid item md={6} xs={6}>
                        <FormControl style={{ width: "50%", float: 'right' }}>
                            <TextField value={partialAmt} onChange={(event)=>{ 
                              if(orderDetails.final_amount > event.target.value) {
                                setPartialAmt(event.target.value)
                                handlePaymentData({
                                  payment_type: paidAmountType,
                                  mode_of_payment: partialPaymentMode,
                                  partial_amount: event.target.value,
                                })
                              }
                            }}/>
                        </FormControl>
                      </Grid>
                      {/* <Grid item md={6} xs={6}>
                        <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
                          {"Mode of Payment"}
                        </Typography>
                      </Grid>
                      <Grid item md={6} xs={6}>
                        <FormControl style={{ width: "50%", float: 'right' }}>
                          <TextField value={partialPaymentMode} onChange={(event)=>{ setPartialPaymentMode(event.target.value)}} />
                        </FormControl>
                      </Grid> */}
                    </>
                  }
                </>
            </Grid>
            <Divider />
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default ApplyCoupon;