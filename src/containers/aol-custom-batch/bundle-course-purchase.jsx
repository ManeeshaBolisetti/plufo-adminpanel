import React, { useState, useContext, useEffect } from 'react';
import { Grid, Card, Button, Typography, TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loader from '../../components/loader/loader';
import ShowPriceDetails from './showPriceDetails';
import BundleCourseIndividualCard from './bundle-courses-individual-card';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import moment from 'moment';

const BundleCoursePurchase = ({
  bundleCart,
  handleBack,
  handleNext,
  userId,
  orderIdNo,
}) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [gradeList, setGradeList] = useState([]);
  const [selectedGrade, setSelectedGrade] = useState('');
  const [courseList, setCourseList] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState('');
  const [sectionList, setSectionList] = useState([]);
  const [noOfSessions, setNoOfSessions] = useState('');
  const [batchSizeList, setBatchSizeList] = useState([]);
  const [selectedBatch, setSelectedBatch] = useState('');
  const [priceDetails, setPriceDetails] = useState('');
  const addRecords = {
    daysList: [],
    selectedDays: '',
    timeList: [],
    selectedTime: '',
    startDate: '',
  };

  useEffect(() => {
    if (bundleCart) {
      setGradeList(bundleCart?.gradeList || []);
      setSelectedGrade(bundleCart?.selectedGrade || '');
      setCourseList(bundleCart?.courseList || []);
      setSelectedCourse(bundleCart?.selectedCourse || '');
      setSectionList(bundleCart?.sectionList || []);
      setNoOfSessions(bundleCart?.noOfSessions || '');
      setBatchSizeList(bundleCart?.batchSizeList || []);
      setSelectedBatch(bundleCart?.selectedBatch || '');
      setPriceDetails(bundleCart?.priceDetails || '');
    }
  }, [bundleCart]);
  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200 || data?.status_code === 201) {
        if (type === 'grade') {
          setGradeList(data?.data);
        }
        if (type === 'courses') {
          const DatInfo = JSON.parse(JSON.stringify(data?.result));
          for (let i = 0; i < DatInfo?.length; i += 1) {
            for (let j = 0; j < DatInfo?.[i]?.course?.length; j += 1) {
              DatInfo[i].course[j] = {
                ...DatInfo?.[i].course[j],
                ...addRecords,
              };
            }
          }
          setCourseList(DatInfo);
        }
        if (type === 'sessions') {
          setSectionList(data?.result);
        }
        if (type === 'batch') {
          setBatchSizeList(data?.result);
        }
        if (type === 'price') {
          setPriceDetails(data?.result?.[0]);
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }
  useEffect(() => {
    ApiCall(`${endpoints.communication.grades}?branch_id=1&module_id=14`, 'grade');
  }, []);

  function handleCourseOnChange(value, index, key, info = 'course') {
    setSelectedCourse((prev) => {
      const newData = { ...prev };
      switch (info) {
        case info:
          newData[info][index][key] = value;
          return newData;
        default:
          return newData;
      }
    });
  }

  async function handleSubmit() {
    if (!selectedGrade) {
      setAlert('warning', 'Select grade');
      return;
    }
    if (!selectedCourse) {
      setAlert('warning', 'Select course');
      return;
    }
    if (selectedCourse?.is_fixed) {
      if (!noOfSessions) {
        setAlert('warning', 'Selected No of Sessions');
        return;
      }
    }
    if (!selectedBatch) {
      setAlert('warning', 'Select Batch Size');
      return;
    }
    const courseResponse = [];
    for (let i = 0; i < selectedCourse?.course?.length; i += 1) {
      if (!selectedCourse?.course?.[i]?.selectedDays) {
        setAlert('warning', `You Missed to select Days in course ${i + 1}`);
        return;
      }
      if (!selectedCourse?.course?.[i]?.selectedTime) {
        setAlert('warning', `You missed to select time slot in course ${i + 1}`);
        return;
      }
      if (!selectedCourse?.course?.[i]?.startDate) {
        setAlert('warning', `You missed to enter start date in course ${i + 1}`);
        return;
      }
      let sel = moment(selectedCourse?.course?.[i]?.startDate, "DD/MM/YYYY");
      let newDateString = sel.format("YYYY-MM-DD");
      courseResponse.push({
        id: selectedCourse?.course?.[i]?.timeList?.id,
        time_slot: selectedCourse?.course?.[i]?.selectedTime,
        start_date: selectedCourse?.course?.[i]?.startDate,
        days: selectedCourse?.course?.[i]?.selectedDays,
      });
    }
    setLoading(true);
    const payload = {
      user: userId,
      bundle_id: selectedCourse?.id,
      price: priceDetails?.actual_price,
      final_price: priceDetails?.price,
      order_id: orderIdNo || '',
      course: courseResponse,
    };
    try {
      const { data } = await axiosInstance.post(`${endpoints.salesAol.addBundleToCart}`, {
        ...payload,
      });
      if (data?.status_code === 200) {
        setLoading(false);
        handleNext(
          {
            gradeList,
            selectedGrade,
            courseList,
            selectedCourse,
            sectionList,
            noOfSessions,
            batchSizeList,
            selectedBatch,
            priceDetails,
            ...data?.result,
          },
          data?.result?.order_id
        );
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12}>
        <Card style={{ padding: '20px 10px', margin: '10px 0px' }}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Typography variant='h6' color='primary'>
                Bundle Course
              </Typography>
            </Grid>
            <Grid item md={3} xs={12}>
              <Autocomplete
                style={{ width: '100%' }}
                size='small'
                onChange={(event, value) => {
                  setSelectedGrade(value);
                  setCourseList([]);
                  setSelectedCourse('');
                  setSectionList([]);
                  setNoOfSessions('');
                  setBatchSizeList([]);
                  setSelectedBatch('');
                  if (value) {
                    ApiCall(
                      `${endpoints.academics.bundleCourse}?grade=${value?.grade_id}`,
                      'courses'
                    );
                  }
                }}
                id='grade-id'
                className='dropdownIcon'
                value={selectedGrade}
                options={gradeList}
                getOptionLabel={(option) => option?.grade__grade_name}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='Select Grade'
                    placeholder='Select Grade'
                  />
                )}
              />
            </Grid>
            <Grid item md={selectedCourse ? 3 : 6} xs={12}>
              <Autocomplete
                style={{ width: '100%' }}
                size='small'
                onChange={(event, value) => {
                  setSelectedCourse(JSON.parse(JSON.stringify(value)));
                  setSectionList([]);
                  setNoOfSessions('');
                  setBatchSizeList([]);
                  setSelectedBatch('');
                  if (value) {
                    if (value?.is_fixed) {
                      ApiCall(
                        `${endpoints.salesAol.getSessionList}?bundle_id=${value?.id}`,
                        'sessions'
                      );
                    } else {
                      ApiCall(
                        `${endpoints.salesAol.getBatchSizeList}?bundle_id=${value?.id}`,
                        'batch'
                      );
                    }
                  }
                }}
                id='course-id'
                className='dropdownIcon'
                value={selectedCourse}
                options={courseList}
                getOptionLabel={(option) => option?.name}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='Select Course'
                    placeholder='Select Course'
                  />
                )}
              />
            </Grid>
            {selectedCourse?.is_fixed ? (
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={(event, value) => {
                    setNoOfSessions(value);
                    setBatchSizeList([]);
                    setSelectedBatch('');
                    if (value) {
                      ApiCall(
                        `${endpoints.salesAol.getBatchSizeList}?bundle_id=${selectedCourse?.id}&no_of_session=${value}`,
                        'batch'
                      );
                    }
                  }}
                  id='session-id'
                  className='dropdownIcon'
                  value={noOfSessions}
                  options={sectionList}
                  getOptionLabel={(option) => (option ? JSON.stringify(option) : '')}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select No of Sessions'
                      placeholder='Select No of Sessions'
                    />
                  )}
                />
              </Grid>
            ) : (
              ''
            )}
            <Grid item md={3} xs={12}>
              <Autocomplete
                style={{ width: '100%' }}
                size='small'
                onChange={(event, value) => {
                  setSelectedBatch(value);
                  setPriceDetails('');
                  if (value) {
                    ApiCall(
                      `${endpoints.salesAol.getPriceDetails}?bundle_id=${
                        selectedCourse?.id
                      }&batch_size=${value}${
                        noOfSessions ? `&no_of_session=${noOfSessions}` : ''
                      }`,
                      'price'
                    );
                  }
                }}
                id='session-id'
                className='dropdownIcon'
                value={selectedBatch}
                options={batchSizeList}
                getOptionLabel={(option) =>
                  option ? `1 : ${JSON.stringify(option)}` : ''}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='Select Batch Size'
                    placeholder='Select Batch Size'
                  />
                )}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              {priceDetails ? <ShowPriceDetails selectedPrize={priceDetails} /> : ''}
            </Grid>
            <Grid item md={12} xs={12}>
              {priceDetails ? (
                <Grid container spacing={3}>
                  {selectedCourse?.course?.map((item, index) => (
                    <Grid item md={4} xs={12} key={item.id}>
                      <BundleCourseIndividualCard
                        courseDetails={item}
                        selectedCourse={selectedCourse}
                        noOfSessions={noOfSessions}
                        batchSize={selectedBatch}
                        index={index}
                        handleOnchange={handleCourseOnChange}
                      />
                    </Grid>
                  ))}
                </Grid>
              ) : (
                ''
              )}
            </Grid>
          </Grid>
        </Card>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={6} xs={6} style={{ textAlign: 'left' }}>
              <Button
                onClick={() =>
                  handleBack({
                    gradeList,
                    selectedGrade,
                    courseList,
                    selectedCourse,
                    sectionList,
                    noOfSessions,
                    batchSizeList,
                    selectedBatch,
                    priceDetails,
                  })}
                className='back-btn'
                size='small'
              >
                Back
              </Button>
            </Grid>
            <Grid item md={6} xs={6} style={{ textAlign: 'right' }}>
              <Button
                variant='contained'
                color='primary'
                size='small'
                style={{ marginTop: '10px' }}
                onClick={handleSubmit}
              >
                Next
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Grid>
  );
};

export default BundleCoursePurchase;
