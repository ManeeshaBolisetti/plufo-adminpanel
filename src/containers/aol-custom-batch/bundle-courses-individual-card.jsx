import React, { useState, useContext, useEffect } from 'react';
import { Grid, Card, Typography, TextField } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { Autocomplete } from '@material-ui/lab';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loader from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import { ConverTime } from '../../components/dateTimeConverter';
import { getWeekList } from 'utility-functions';

const BundleCourseIndividualCard = ({
  courseDetails,
  selectedCourse,
  noOfSessions,
  batchSize,
  index,
  handleOnchange,
}) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200) {
        if (type === 'days') {
          handleOnchange(data?.result, index, 'daysList');
        }
        if (type === 'time') {
          handleOnchange(data?.result?.[0], index, 'timeList');
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  const handleDate = (value) => {
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    const date = new Date(value)
    var selectedDay = weekday[date.getDay()];
    var retrievedDays = courseDetails?.selectedDays;
    var ComparedDayslist = retrievedDays?.split("/");
    console.log("Day:",ComparedDayslist.includes(selectedDay));
    if(ComparedDayslist?.includes(selectedDay)){
      handleOnchange(value, index, 'startDate')
    }
    else{
      handleOnchange('', index, 'startDate')
      const message = "Please Select Start Date as per Course Days";
      setAlert('error',message);
    }
  };

  function disableWeekends(value) {
    var weekday = new Array(7);
    weekday[0] = "Sun";
    weekday[1] = "Mon";
    weekday[2] = "Tue";
    weekday[3] = "Wed";
    weekday[4] = "Thu";
    weekday[5] = "Fri";
    weekday[6] = "Sat";
    const date = new Date(value)
    var selectedDay = weekday[date.getDay()];
    var retrievedDays = courseDetails?.selectedDays;
    var ComparedDayslist = retrievedDays.split("/");
    console.log("Day:",ComparedDayslist.includes(selectedDay));
    if(ComparedDayslist.includes(selectedDay)){
      return
    }
    else{
      return value.getDay() === date.getDay();
    }

};

  useEffect(() => {
    if (courseDetails) {
      ApiCall(
        `${endpoints.salesAol.getDaysAndTime}?course_id=${
          courseDetails?.id
        }&batch_size=${batchSize}${noOfSessions ? `&no_of_session=${noOfSessions}` : ''}`,
        'days'
      );
    }
  }, [courseDetails]);

  const getWeeks = (dayIdx) => {
    handleOnchange(getWeekList(dayIdx, selectedCourse), index, 'dateList')
  }

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12}>
        <Card
          style={{ padding: '10px', borderRadius: '10px', border: '1px solid lightgray' }}
        >
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Typography color='primary'>
                Course&nbsp;
                {index + 1}
              </Typography>
              <Typography>
                Course Name: &nbsp;
                {courseDetails?.course_name}
              </Typography>
              <Typography>
                Subject Name: &nbsp;
                {courseDetails?.subject}
              </Typography>
            </Grid>
            <Grid item md={12} xs={12}>
              <Autocomplete
                style={{ width: '100%' }}
                size='small'
                onChange={(event, value) => {
                  handleOnchange(value, index, 'selectedDays');
                  handleOnchange([], index, 'timeList');
                  handleOnchange('', index, 'selectedTime');
                  handleOnchange('', index, 'startDate');
                  let selday = value && value.split('/')[0];
                  getWeeks(weekdays.indexOf(selday))
                  if (value) {
                    ApiCall(
                      `${endpoints.salesAol.getDaysAndTime}?course_id=${
                        courseDetails?.id
                      }&batch_size=${batchSize}${
                        noOfSessions ? `&no_of_session=${noOfSessions}` : ''
                      }&days=${value}`,
                      'time'
                    );
                  }
                }}
                id='days-id'
                className='dropdownIcon'
                value={courseDetails?.selectedDays}
                options={courseDetails?.daysList}
                getOptionLabel={(option) => option || ''}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='Select Days'
                    placeholder='Select Days'
                  />
                )}
              />
            </Grid>
            <Grid item md={12} xs={12}>
              <Grid item md={12} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={(event, value) => {
                    handleOnchange(value, index, 'selectedTime');
                    handleOnchange('', index, 'startDate');
                  }}
                  id='days-id'
                  className='dropdownIcon'
                  value={courseDetails?.selectedTime}
                  options={courseDetails?.timeList?.time_slot_available || []}
                  getOptionLabel={(option) => (option ? ConverTime(option) : '')}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Time Slot'
                      placeholder='Select Time Slot'
                    />
                  )}
                />
              </Grid>
            </Grid>
            <Grid item md={12} xs={12}>
              <MuiPickersUtilsProvider variant='outlined' utils={DateFnsUtils}>
                <KeyboardDatePicker
                  fullWidth
                  autoOk
                  disablePast
                  className='dropdownIcon'
                  variant="inline"
                  inputVariant="outlined"
                  size='small'
                  placeholder='Select Start Date'
                  helperText='Select Start Date'
                  value={courseDetails?.startDate || ''}
                  onChange={(data, value) => handleDate(value)}
                  shouldDisableDate={disableWeekends}
                  onError={console.log}
                  variant='outlined'
                  minDate={new Date('2018-01-01')}
                  format='yyyy-MM-dd'
                />
              </MuiPickersUtilsProvider>
              {/* <Autocomplete
                  size='small'
                  id='daysCombination'
                  className='dropdownIcon'
                  style={{ width: '100%' }}
                  options={courseDetails?.dateList || []}
                  getOptionLabel={(option) => option || ''}
                  filterSelectedOptions
                  value={courseDetails?.startDate || ''}
                  onChange={(event, value) => handleOnchange(value, index, 'startDate')}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Start Date'
                      placeholder='Select Start Date'
                    />
                  )}
                /> */}
            </Grid>
          </Grid>
        </Card>
      </Grid>
      {loading && <Loader />}
    </Grid>
  );
};

export default BundleCourseIndividualCard;
