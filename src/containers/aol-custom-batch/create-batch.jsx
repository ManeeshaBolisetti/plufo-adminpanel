/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from 'react';
import { Grid, Card, TextField, Button, Typography } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import ConfirmDialog from '../../components/confirm-dialog';
import { ConverTime } from '../../components/dateTimeConverter';
import ShowPriceDetails from './showPriceDetails';
import { getWeekList } from 'utility-functions';
import moment from 'moment';

const CreateCustomBatch = ({
  setData,
  userId,
  preFilledData,
  index,
  removeRecord,
  waletAmount,
  orderIdNo,
}) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [gradeList, setGradeList] = useState([]);
  const [selectedGrade, setSelectedGrade] = useState('');
  const [courseList, setCourseList] = useState([]);
  const [selectedCourse, setSelectedCourse] = useState('');
  const [weekList, setWeekList] = useState([]);
  const [selectedWeek, setSelectedWeek] = useState('');
  const [timeList, setTimeList] = useState([]);
  const [selectedTime, setSelectedTime] = useState('');
  const [batchList, setBatchList] = useState([]);
  const [selectedBatch, setSelectedBatch] = useState('');
  const [selectedPrize, setSelectedPrize] = useState('');
  const [selectedSubject, setSelectedSubject] = useState('');
  const [subjectList, setSubjectList] = useState([]);
  const [startDate, setStartDate] = useState('');
  const [sectionList, setSectionList] = useState([]);
  const [noOfSessions, setNoOfSessions] = useState('');
  const [open, setOpen] = useState(false);
  const [dateList, setDateList] = useState([]);
  const weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200 || data?.status_code === 201) {
        if (type === 'grade') {
          setGradeList(data?.data);
        } else if (type === 'subject') {
          setSubjectList(data?.result);
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  function getCoursList(info, infro2) {
    setLoading(true);
    axiosInstance
      .get(`${endpoints.academics.courses}?grade=${info?.grade_id}&subject=${infro2?.id}`)
      .then((result) => {
        setLoading(false);
        if (result.status === 200) {
          setCourseList(result?.data?.result);
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  function getDayWeekBatcPrizeList(api, key) {
    setLoading(true);
    axiosInstance
      .get(api)
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          if (key === 'week') {
            setWeekList(result?.data?.result);
          }
          if (key === 'time') {
            setTimeList(result?.data?.result);
            setSelectedPrize(result?.data?.result?.[0] || '');
          }
          if (key === 'batch') {
            setBatchList(result?.data?.result);
          }
          if (key === 'sessions') {
            setSectionList(result?.data?.result);
          }
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  useEffect(() => {
    if (userId) {
      ApiCall(`${endpoints.communication.subjectList}?branch_id=1`, 'subject');
      ApiCall(`${endpoints.communication.grades}?branch_id=1&module_id=14`, 'grade');
    }
  }, [userId]);

  useEffect(() => {
    if (preFilledData) {
      setGradeList(preFilledData?.gradeList || []);
      setSelectedGrade(preFilledData?.selectedGrade || '');
      setCourseList(preFilledData?.courseList || []);
      setSelectedCourse(preFilledData?.selectedCourse || '');
      setWeekList(preFilledData?.weekList || []);
      setSelectedWeek(preFilledData?.selectedWeek || '');
      setTimeList(preFilledData?.timeList || []);
      setSelectedTime(preFilledData?.selectedTime || '');
      setBatchList(preFilledData?.batchList || []);
      setSelectedBatch(preFilledData?.selectedBatch || '');
      setSelectedPrize(preFilledData?.selectedPrize || '');
      setNoOfSessions(preFilledData?.noOfSessions || '');
      setSectionList(preFilledData?.sectionList || []);
      setSubjectList(preFilledData?.subjectList || []);
      setSelectedSubject(preFilledData?.selectedSubject);
      setStartDate(preFilledData?.startDate || '');
    }
  }, [preFilledData]);

  function handleNextStep() {
    setLoading(true);
    let sel = moment(startDate, "DD/MM/YYYY");
    let newDateString = sel.format("YYYY-MM-DD");
    axiosInstance
      .post(`${endpoints.salesAol.addCartApi}`, {
        user: userId,
        total_paid_amount: selectedPrize?.price,
        wallet_amount_taken:
          (waletAmount &&
            waletAmount.result &&
            waletAmount.result.length !== 0 &&
            waletAmount.result[0].reaming_amount) ||
          '0',
        wallet_agree: 'False',
        batch_days: [selectedWeek],
        time_slot: selectedTime,
        course: [selectedPrize?.id],
        orderId: orderIdNo || null,
        start_date: newDateString,
      })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setData(index, {
            selectedGrade,
            selectedCourse,
            selectedWeek,
            selectedTime,
            selectedBatch,
            selectedPrize,
            gradeList,
            courseList,
            weekList,
            timeList,
            batchList,
            saved: true,
            startDate,
            noOfSessions,
            sectionList,
            subjectList,
            selectedSubject,
            ...result.data.result,
          });
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  function handleDelete() {
    if (preFilledData.saved) {
      setLoading(true);
      axiosInstance
        .get(
          `${endpoints.salesAol.deleteCartApi}?order_id=${preFilledData.order_id}&course_id=${preFilledData.course_id}`
        )
        .then((result) => {
          setLoading(false);
          if (result.data.status_code === 200) {
            removeRecord(index);
            setAlert('success', result.data.message);
          } else {
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    } else {
      removeRecord(index);
    }
  }

  const getWeeks = (dayIdx) => {
    setDateList(getWeekList(dayIdx, selectedCourse))
  }

  return (
    <Grid container spacing={2}>
      {loading ? (
        <Loading message='Loading...' />
      ) : (
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '20px 10px', margin: '10px 0px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12}>
                <Typography variant='h6' color='primary'>
                  Normal Course&nbsp;
                  {index + 1}
                </Typography>
              </Grid>
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  disabled={preFilledData.saved}
                  onChange={(event, value) => {
                    setSelectedGrade(value);
                    setSelectedSubject('');
                    setCourseList([]);
                    setSelectedCourse('');
                    setSectionList([]);
                    setNoOfSessions('');
                    setBatchList([]);
                    setSelectedBatch('');
                    setWeekList([]);
                    setSelectedWeek('');
                    setTimeList([]);
                    setSelectedTime('');
                    setSelectedPrize('');
                    setStartDate('');
                    setDateList([])
                  }}
                  id='grade-id'
                  className='dropdownIcon'
                  value={selectedGrade}
                  options={gradeList}
                  getOptionLabel={(option) => option?.grade__grade_name}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Grade'
                      placeholder='Select Grade'
                    />
                  )}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  disabled={preFilledData.saved}
                  onChange={(event, value) => {
                    setSelectedSubject(value);
                    setCourseList([]);
                    setSelectedCourse('');
                    setSectionList([]);
                    setNoOfSessions('');
                    setBatchList([]);
                    setSelectedBatch('');
                    setWeekList([]);
                    setSelectedWeek('');
                    setTimeList([]);
                    setSelectedTime('');
                    setSelectedPrize('');
                    setStartDate('');
                    setDateList([])
                    if (value) {
                      getCoursList(selectedGrade, value);
                    }
                  }}
                  id='subject-id'
                  className='dropdownIcon'
                  value={selectedSubject}
                  options={subjectList}
                  getOptionLabel={(option) => option?.subject__subject_name || ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Subject'
                      placeholder='Select Subject'
                    />
                  )}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  disabled={preFilledData.saved}
                  onChange={(event, value) => {
                    setSelectedCourse(value);
                    setSectionList([]);
                    setNoOfSessions('');
                    setBatchList([]);
                    setSelectedBatch('');
                    setWeekList([]);
                    setSelectedWeek('');
                    setTimeList([]);
                    setSelectedTime('');
                    setSelectedPrize('');
                    setStartDate('');
                    setDateList([])
                    if (value && value?.is_fixed) {
                      getDayWeekBatcPrizeList(
                        `${endpoints.salesAol.getSeccionsList}?course_id=${value.id}`,
                        'sessions'
                      );
                    } else if (value && !value?.is_fixed) {
                      getDayWeekBatcPrizeList(
                        `${endpoints.salesAol.getBatchSize}?course_id=${value.id}`,
                        'batch'
                      );
                    }
                  }}
                  id='course-id'
                  className='dropdownIcon'
                  value={selectedCourse}
                  options={courseList}
                  getOptionLabel={(option) =>
                    option
                      ? `${option?.course_name} , ${option?.is_fixed
                        ? 'Full Year Course - 40 weeks'
                        : `${'Fixed Course' + ' - '}${option?.no_of_periods} session`
                      }`
                      : ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Course'
                      placeholder='Select Course'
                    />
                  )}
                />
              </Grid>
              {selectedCourse?.is_fixed ? (
                <Grid item md={3} xs={12}>
                  <Autocomplete
                    style={{ width: '100%' }}
                    size='small'
                    disabled={preFilledData.saved}
                    onChange={(event, value) => {
                      setNoOfSessions(value);
                      setBatchList([]);
                      setSelectedBatch('');
                      setWeekList([]);
                      setSelectedWeek('');
                      setTimeList([]);
                      setSelectedTime('');
                      setSelectedPrize('');
                      setStartDate('');
                      setDateList([])
                      if (value) {
                        getDayWeekBatcPrizeList(
                          `${endpoints.salesAol.getBatchSize}?course_id=${selectedCourse?.id}&no_of_session=${value}`,
                          'batch'
                        );
                      }
                    }}
                    id='section-id'
                    className='dropdownIcon'
                    value={noOfSessions}
                    options={sectionList || []}
                    getOptionLabel={(option) => (option ? JSON.stringify(option) : '')}
                    filterSelectedOptions
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant='outlined'
                        label='Select No of sessions'
                        placeholder='Select No of sessions'
                      />
                    )}
                  />
                </Grid>
              ) : (
                ''
              )}
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  disabled={preFilledData.saved}
                  onChange={(event, value) => {
                    setSelectedBatch(value);
                    setWeekList([]);
                    setSelectedWeek('');
                    setTimeList([]);
                    setSelectedTime('');
                    setSelectedPrize('');
                    setStartDate('');
                    setDateList([])
                    if (value) {
                      getDayWeekBatcPrizeList(
                        `${endpoints.salesAol.getBatchSize}?course_id=${selectedCourse.id
                        }&batch_size=${value}${selectedCourse?.is_fixed ? `&no_of_session=${noOfSessions}` : ''
                        }`,
                        'week'
                      );
                    }
                  }}
                  id='batch-id'
                  className='dropdownIcon'
                  value={selectedBatch}
                  options={batchList}
                  getOptionLabel={(option) =>
                    option ? `1 : ${JSON.stringify(option)}` : ''
                  }
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Batch Size'
                      placeholder='Select Batch Size'
                    />
                  )}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  disabled={preFilledData.saved}
                  onChange={(event, value) => {
                    setSelectedWeek(value);
                    setTimeList([]);
                    setSelectedTime('');
                    setSelectedPrize('');
                    setStartDate('');

                    let selday = value && value.split('/')[0];
                    getWeeks(weekdays.indexOf(selday))
                    if (value) {
                      getDayWeekBatcPrizeList(
                        `${endpoints.salesAol.getBatchSize}?course_id=${selectedCourse.id
                        }&batch_size=${selectedBatch}${selectedCourse?.is_fixed ? `&no_of_session=${noOfSessions}` : ''
                        }&days=${value}`,
                        'time'
                      );
                    }
                  }}
                  id='week-id'
                  className='dropdownIcon'
                  value={selectedWeek}
                  options={weekList}
                  getOptionLabel={(option) => option}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Days'
                      placeholder='Select Days'
                    />
                  )}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  disabled={preFilledData.saved}
                  onChange={(event, value) => {
                    setSelectedTime(value);
                  }}
                  id='time-id'
                  className='dropdownIcon'
                  value={selectedTime}
                  options={timeList?.[0]?.time_slot_available || []}
                  getOptionLabel={(option) => (option ? ConverTime(option) : '')}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Time Slot'
                      placeholder='Select Time Slot'
                    />
                  )}
                />
              </Grid>
              <Grid item md={3} xs={12}>
                <Autocomplete
                  size='small'
                  id='daysCombination'
                  className='dropdownIcon'
                  disabled={preFilledData.saved}
                  style={{ width: '100%' }}
                  options={dateList}
                  getOptionLabel={(option) => option || ''}
                  filterSelectedOptions
                  value={startDate || ''}
                  onChange={(event, value) => setStartDate(value)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Start Date'
                      placeholder='Select Start Date'
                    />
                  )}
                />
                {/* <MuiPickersUtilsProvider variant='outlined' utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    fullWidth
                    autoOk
                    disablePast
                    disabled={preFilledData.saved}
                    placeholder='Start Date'
                    helperText='Select Start Date'
                    value={startDate || ''}
                    onChange={(data, value) => setStartDate(value)}
                    onError={console.log}
                    variant='outlined'
                    minDate={new Date('2018-01-01')}
                    format='yyyy-MM-dd'
                  />
                </MuiPickersUtilsProvider> */}
              </Grid>
              <Grid item md={12} xs={12}>
                {selectedTime && startDate && selectedPrize && (
                  <ShowPriceDetails selectedPrize={selectedPrize} />
                )}
              </Grid>
              <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
                <Button
                  disabled={
                    !selectedPrize || !startDate || !selectedTime || preFilledData.saved
                  }
                  variant='contained'
                  color='primary'
                  size='small'
                  onClick={() => handleNextStep()}
                >
                  Save
                </Button>
                &nbsp;
                <Button
                  variant='contained'
                  color='primary'
                  size='small'
                  onClick={() => setOpen(true)}
                >
                  Delete
                </Button>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      )}
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => setOpen(false)}
          confirm={handleDelete}
          title='Are you sure to Delete ?'
        />
      ) : (
        ''
      )}
    </Grid>
  );
};

export default CreateCustomBatch;
