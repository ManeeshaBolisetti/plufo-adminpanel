/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useContext, useEffect, useMemo } from 'react';
import { Grid, Card, Button, Typography, TextField } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { Autocomplete } from '@material-ui/lab';
import Stepper from '@material-ui/core/Stepper';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import CreateCustomBatch from './create-batch';
import PaymentAddress from './payment-address';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import BundleCoursePurchase from './bundle-course-purchase';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import './style.scss';
import displayName from '../../config/displayName';

const defaultData = {
  id: 1,
  selectedGrade: '',
  selectedCourse: '',
  selectedWeek: '',
  selectedTime: '',
  selectedBatch: '',
  selectedPrize: '',
  selectedSubject: '',
  noOfSessions: '',
  startDate: '',
  gradeList: [],
  courseList: [],
  weekList: [],
  timeList: [],
  batchList: [],
  subjectList: [],
  sectionList: [],
  saved: false,
};

const defaultBundleCourse = {
  gradeList: [],
  selectedGrade: '',
  courseList: [],
  selectedCourse: '',
  sectionList: [],
  noOfSessions: '',
  batchSizeList: [],
  selectedBatch: '',
  priceDetails: '',
};

const CreateCustomeBatch = (props) => {
  const history = useHistory();
  const [activeStep, setActiveStep] = useState(0);
  const [waletAmount, setWaletAmount] = useState('');
  const [selectedAddress, setSelectedAddress] = useState('');
  const [cartDetails, setCartDetails] = useState([]);
  const [bundleCart, setBundleCart] = useState('');
  const [orderIdNo, setOrderIdNo] = useState('');
  const [selectCourseType, setSelectCourseType] = useState('');
  const steps = ['Select Course Type', 'Batch Creation', 'Address Verification'];
  const { setAlert } = useContext(AlertNotificationContext);
  const courseTypeList = [
    { id: 1, title: 'Normal Course' },
    { id: 2, title: 'Bundle Course' },
  ];
  const {
    match: { params },
  } = props;

  function getWalletAmount() {
    axiosInstance
      .get(`${endpoints.salesAol.getWaletAmount}?student=${params.user_id}`)
      .then((result) => {
        if (result.status === 200) {
          setWaletAmount(result.data);
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setAlert('error', error.message);
      });
  }

  useEffect(() => {
    getWalletAmount();
  }, []);

  function handleOnChangeData(index, data) {
    if (index === 0) {
      setOrderIdNo(data.order_id);
    }
    const backUpData = [...cartDetails];
    backUpData[index] = data;
    setCartDetails(backUpData);
  }

  function handleAddCourse() {
    const data = { ...defaultData };
    setCartDetails((oldData) => [
      ...oldData,
      {
        ...data,
      },
    ]);
  }
  function handleRemoveCourse(index) {
    setCartDetails((previousData) => {
      previousData.splice(index, 1);
      return [...previousData];
    });
  }

  const createButton = () => {
    return (
      <Button
        disabled={cartDetails.filter((item) => item.saved === false).length}
        color='secondary'
        variant='contained'
        size='small'
        style={{ marginTop: '10px' }}
        onClick={() => handleAddCourse()}
      >
        <AddCircleOutlineIcon />
        &nbsp;Add Course
      </Button>
    );
  };

  const NormalCourse = useMemo(() => {
    return (
      <Grid
        container
        spacing={2}
        style={selectCourseType?.id === 2 ? { display: 'none' } : {}}
      >
        {cartDetails && cartDetails.length === 0 && (
          <>
            <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
              {createButton()}
            </Grid>
            <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
              <Typography color='secondary' variant='h4'>
                Please Add Course
              </Typography>
            </Grid>
          </>
        )}
        <Grid item md={12} xs={12}>
          {cartDetails &&
            cartDetails.length !== 0 &&
            cartDetails.map((item, index) => (
              <CreateCustomBatch
                key={parseInt(item.id, 10) + parseInt(index, 10)}
                index={index}
                setData={handleOnChangeData}
                removeRecord={handleRemoveCourse}
                preFilledData={item}
                userId={params.student_id}
                waletAmount={waletAmount}
                orderIdNo={orderIdNo}
              />
            ))}
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={6} xs={6} style={{ textAlign: 'left' }}>
              <Button onClick={() => setActiveStep(0)} className='back-btn' size='small'>
                Back
              </Button>
            </Grid>
            <Grid item md={6} xs={6} style={{ textAlign: 'right' }}>
              {cartDetails && cartDetails.length !== 0 ? createButton() : ''}
              &nbsp;&nbsp;
              <Button
                disabled={
                  cartDetails.filter((item) => item.saved === false).length ||
                  cartDetails.length === 0
                }
                variant='contained'
                color='primary'
                size='small'
                style={{ marginTop: '10px' }}
                onClick={() => setActiveStep(2)}
              >
                Next
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }, [selectCourseType, cartDetails]);

  const BundleCourse = useMemo(() => {
    return (
      <Grid
        container
        spacing={2}
        style={selectCourseType?.id === 1 ? { display: 'none' } : {}}
      >
        <Grid item md={12} xs={12}>
          <BundleCoursePurchase
            bundleCart={{ ...bundleCart }}
            handleBack={(data) => {
              setActiveStep(0);
              setBundleCart(data);
            }}
            handleNext={(data, orderId = '') => {
              setBundleCart(data);
              setOrderIdNo(orderId);
              setActiveStep(2);
            }}
            userId={params.student_id}
            waletAmount={waletAmount}
            orderIdNo={orderIdNo}
          />
        </Grid>
      </Grid>
    );
  }, [selectCourseType, bundleCart]);

  return (
    <Layout>
      <div style={{ margin: '20px 10px 0px 10px' }}>
        <CommonBreadcrumbs componentName={displayName ? (displayName + ' Sales') : ''} childComponentName='Custom Batch' />
      </div>
      <Grid container spacing={2} style={{ width: '100%', margin: '20px 0px' }}>
        {/* <Grid item md={12} xs={12}>
          <Grid container spacing={2} justify='middle'>
            <Grid item md={12} xs={12} style={{ display: 'flex' }}>
              <CommonBreadcrumbs
                componentName='Plufo Sales'
                childComponentName='Custom Batch'
              />
            </Grid>
          </Grid>
        </Grid> */}
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={1} />
            <Grid item md={12} xs={12}>
              <Card style={{ padding: '10px' }}>
                <Stepper activeStep={activeStep} alternativeLabel>
                  {steps.map((label) => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </Card>
              {activeStep === 0 && (
                <Card style={{ padding: '10px' }}>
                  <Grid container spacing={2}>
                    <Grid item md={3} xs={12}>
                      <Autocomplete
                        style={{ width: '100%' }}
                        size='small'
                        onChange={(event, value) => {
                          setSelectCourseType(value);
                          if (value) {
                            setActiveStep(1);
                            setCartDetails([defaultData]);
                            setBundleCart({ ...defaultBundleCourse });
                          }
                        }}
                        id='course-type-id'
                        className='dropdownIcon'
                        value={selectCourseType}
                        options={courseTypeList}
                        getOptionLabel={(option) => option?.title}
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            variant='outlined'
                            label='Select Course Type'
                            placeholder='Select Course Type'
                          />
                        )}
                      />
                    </Grid>
                    <Grid item md={12} xs={12}>
                      <Grid
                        container
                        spacing={2}
                        justify='space-between'
                        style={{ padding: '5px' }}
                      >
                        <Button
                          onClick={() => history.goBack()}
                          className='back-btn'
                          size='small'
                        >
                          Back
                        </Button>
                        <Button
                          variant='contained'
                          color='primary'
                          size='small'
                          disabled={!selectCourseType}
                          style={{ marginTop: '10px' }}
                          onClick={() => setActiveStep(1)}
                        >
                          Next
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Card>
              )}
              {activeStep === 1 ? (
                <>
                  {NormalCourse}
                  {BundleCourse}
                </>
              ) : (
                ''
              )}
              {activeStep === 2 && (
                <PaymentAddress
                  handleNext={setActiveStep}
                  details={cartDetails}
                  addressSelected={selectedAddress}
                  handleSelectAddress={setSelectedAddress}
                  userId={params.user_id}
                  waletAmount={waletAmount}
                  studentId={params.student_id}
                  orderIdNo={orderIdNo}
                />
              )}
            </Grid>
            <Grid item md={1} />
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default React.memo(CreateCustomeBatch);
