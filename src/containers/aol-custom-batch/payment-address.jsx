/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect, useContext } from 'react';
import { Grid, Card, Button, Typography, TextField } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { useHistory } from 'react-router-dom';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import AddEditPaymentAddress from './add-edit-address';
import Timer from './timer';
import ConfirmDialog from '../../components/confirm-dialog';
import ApplyCoupon from './apply-coupon';
import displayName from '../../config/displayName';

const PaymentAddress = ({
  handleNext,
  // details,
  // userId,
  addressSelected,
  handleSelectAddress,
  // waletAmount,
  studentId,
  orderIdNo,
}) => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [addressList, setAddressList] = useState([]);
  const [selectedAddress, setSelectedAddress] = useState('');
  const [loading, setLoading] = useState(false);
  const [openModel, setOpenModel] = useState(false);
  const [edit, setEdit] = useState(false);
  const [timer, setTimer] = useState(false);
  const [paymentDetails, setPaymentDetails] = useState('');
  const [paymentStatus, setPaymentStatus] = useState('');
  const [orderDetails, setOrderDetails] = useState('');
  const [deleteAddress, setDeleteAddress] = useState(false);
  const [deleteId, setDeleteId] = useState('');
  const [paymentData, setPaymentData] = useState('');

  function getCallApis(url, key) {
    setLoading(true);
    axiosInstance
      .get(url)
      .then((result) => {
        setLoading(false);
        if (result.status === 200) {
          if (key === 'address') {
            setAddressList(result.data);
          } else {
            setOrderDetails(result.data);
          }
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  useEffect(() => {
    getCallApis(`${endpoints.salesAol.getAddressList}?student=${studentId}`, 'address');
    getCallApis(
      `${endpoints.salesAol.cartListApi}?order_id=${orderIdNo}`,
      'orderDetails'
    );
  }, [studentId]);

  useEffect(() => {
    if (addressSelected) {
      setSelectedAddress(addressSelected || '');
    }
  }, [addressSelected]);

  function handleEdit(data) {
    setEdit(true);
    setSelectedAddress(data);
    setOpenModel(true);
  }
  function handleOpenModal() {
    setSelectedAddress('');
    setEdit(false);
    setOpenModel(true);
  }
  function handleClose(data) {
    setOpenModel(false);
    setEdit(false);
    setSelectedAddress('');
    if (data === 'success') {
      getCallApis(`${endpoints.salesAol.getAddressList}?student=${studentId}`, 'address');
    }
  }

  function generetePaymentLink() {
    if (!selectedAddress) {
      setAlert('warning', 'Select Payment Address');
      return;
    }
    const data = {
      // user: parseInt(studentId, 10),
      order_id: orderIdNo,
      address_id: selectedAddress.id,
    };
    setLoading(true);
    axiosInstance
      .post(`${endpoints.salesAol.generetePaymentLinkApi}`, { ...data })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setPaymentDetails(result.data.result);
          setTimer(true);
        } else {
          setAlert('error', result.data.description);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  function resetTime() {
    setTimer(false);
  }

  function copyFunction(id, name) {
    const copyText = document.getElementById(id);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand('copy');
    setAlert('Success', `${name} Link Copied Successfully`);
  }

  function getPaymentStatus() {
    setLoading(true);
    axiosInstance
      .get(
        `${endpoints.salesAol.paymentStatus}?payment_link_id=${paymentDetails && paymentDetails.order_id
        }`
      )
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setPaymentStatus(result.data.result);
          // setAlert('success', result.data.message);
          if (result.data.result && result.data.result.status === 'paid') {
            setTimeout(() => {
              if (displayName === 'My SparkleBox') {
                history.push('/mysparklebox-sales');
              } else if (displayName === 'SparkleBox School') {
                history.push('/sparklebox.school-sales');
              } else {
                history.push('/plufo-sales');
              }
            }, 5000);
          }
        } else {
          setAlert('error', result.data.description);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  function handleDeleteAddress(info) {
    setLoading(true);
    axiosInstance
      .put(`${endpoints.salesAol.deleteAddressApi}`, { address_id: info.id })
      .then((res) => {
        setLoading(false);
        if (res && res.data && res.data.status_code === 200) {
          setAlert('success', res.data.message);
          getCallApis(
            `${endpoints.salesAol.getAddressList}?student=${studentId}`,
            'address'
          );
        } else {
          setAlert('error', res.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  const handleSavePayment = () => {
    console.log(orderDetails)
    setLoading(true);
    const payload = {
      erp_id: studentId,
      amount: paymentData.payment_type === 'FULL' ? orderDetails?.result?.final_amount : paymentData.partial_amount,
      payment_type: paymentData.payment_type === 'FULL' ? 'full_payment' : 'partial_payment',
      mode_of_payment: paymentData.mode_of_payment,
      order_id_list: [orderIdNo],
      address_id: selectedAddress.id,
    }
    axiosInstance
      .post(`${endpoints.salesAol.savePaymentDetails}`, { ...payload })
      .then((result) => {
        setLoading(false);
        // if (result.data.status_code === 200) {

        history.push('/aol-batch-create')
        setAlert('success', 'Saved Successfully')
        // } else {
        //   setAlert('error', result.data.description);
        // }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  function handleDeleteAddressInfo() {
    handleDeleteAddress(deleteId);
    setDeleteAddress(false);
  }

  return (
    <>
      {loading ? (
        <Loading message='Loading...' />
      ) : (
        <Grid container spacing={2}>
          <Grid item md={12} xs={12}>
            <Card style={{ padding: '20px 10px' }}>
              <Grid container spacing={2}>
                <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
                  <Button
                    size='small'
                    variant='contained'
                    style={{
                      border: '1px solid #014B7E',
                      color: '#014B7E',
                      backgroundColor: 'white',
                      borderRadius: '10px',
                    }}
                    onClick={() => handleOpenModal()}
                  >
                    + Add New Address
                  </Button>
                </Grid>
                <Grid item md={12} xs={12}>
                  <Grid container spacing={2}>
                    {addressList && addressList.length !== 0 ? (
                      <Grid item md={12} xs={12}>
                        <Typography variant='h6' style={{ color: '#014B7E' }}>
                          Select Address to Checkout
                        </Typography>
                      </Grid>
                    ) : (
                      ''
                    )}
                    {addressList && addressList.length !== 0 ? (
                      addressList.map((item) => (
                        <Grid item md={3} xs={12} key={item.id}>
                          <Card
                            style={{
                              padding: '10px',
                              borderRadius: '5px',
                              border: '4px solid #FCEEEE',
                              cursor: 'pointer',
                              backgroundColor:
                                item.id === selectedAddress.id ? '#FCEEEE' : '',
                              color:
                                item.id === selectedAddress.id ? '#014B7E' : '#014B7E',
                            }}
                            onClick={() => setSelectedAddress(item)}
                          >
                            <Grid container spacing={2}>
                              <Grid item md={8} xs={8}>
                                {item.name}
                              </Grid>
                              <Grid item md={2} xs={2} style={{ textAlign: 'right' }}>
                                <EditIcon
                                  size='small'
                                  style={{ fontSize: '15px' }}
                                  onClick={() => handleEdit(item)}
                                />
                              </Grid>
                              <Grid item md={2} xs={2} style={{ textAlign: 'right' }}>
                                <DeleteIcon
                                  size='small'
                                  style={{ fontSize: '15px' }}
                                  onClick={() => {
                                    setDeleteId(item);
                                    setDeleteAddress(true);
                                  }}
                                />
                              </Grid>
                              <Grid item md={12} xs={12}>
                                {item.phone_number}
                              </Grid>
                              <Grid item md={12} xs={12}>
                                {item.address1}
                                &nbsp;
                                {item.city}
                                &nbsp;
                                {`(${item.zip_code})`}
                              </Grid>
                            </Grid>
                          </Card>
                        </Grid>
                      ))
                    ) : (
                      <Grid item md={12} xs={12}>
                        <Typography
                          variant='h6'
                          style={{ color: '#014B7E', textAlign: 'center' }}
                        >
                          Please Create New Address to Checkout
                        </Typography>
                      </Grid>
                    )}
                  </Grid>
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid item md={12} xs={12}>
                  <ApplyCoupon
                    orderDetails={orderDetails && orderDetails.result}
                    handleCallback={getCallApis}
                    orderIdNo={orderIdNo}
                    disabled={timer}
                    handlePaymentData={(value) => {
                      setPaymentData(value)
                    }}
                  />
                </Grid>
              </Grid>
              {timer ? (
                <Grid container spacing={2}>
                  <Grid item md={12} xs={12}>
                    <Typography
                      variant='h6'
                      style={{
                        color: '#FF6B6B',
                        fontSize: '20px',
                        fontWeight: 'bold',
                      }}
                    >
                      Payment Link Expires in:-&nbsp;
                      <Timer clock={10800} status={timer} resundOtpEnable={resetTime} />
                    </Typography>
                  </Grid>
                  <Grid item md={2} xs={12} style={{ marginTop: '10px' }}>
                    <span
                      style={{
                        color: '#009CE1',
                        fontSize: '20px',
                        fontFamily: 'Raleway',
                        fontWeight: 'bold',
                        paddingRight: '2px',
                      }}
                    >
                      Payment Link :
                    </span>
                  </Grid>
                  <Grid item md={8} xs={6}>
                    <TextField
                      variant='outlined'
                      fullWidth
                      margin='dense'
                      id='MyLink1'
                      style={{ textAlign: 'center' }}
                      value={(paymentDetails && paymentDetails.short_url) || ''}
                    />
                  </Grid>
                  <Grid item md={2} xs={6} style={{ marginTop: '10px' }}>
                    <Button
                      type='text'
                      toolTip='Copy Link'
                      size='small'
                      color='primary'
                      variant='contained'
                      onClick={() => copyFunction('MyLink1', 'Payment')}
                    >
                      <FileCopyIcon size='small' />
                    </Button>
                  </Grid>
                  <Grid item md={3} xs={12}>
                    <Button
                      size='small'
                      variant='contained'
                      color='primary'
                      onClick={() => getPaymentStatus()}
                    >
                      Get Payment Status
                    </Button>
                  </Grid>
                  <Grid item md={9} xs={12}>
                    <Typography
                      style={{
                        color: '#009F10',
                        fontSize: '20px',
                        fontFamily: 'Poppins',
                        fontWeight: 'bold',
                        paddingRight: '2px',
                      }}
                    >
                      {paymentStatus && paymentStatus.status}
                    </Typography>
                  </Grid>
                </Grid>
              ) : (
                ''
              )}
              <Grid
                container
                spacing={2}
                justify='space-between'
                style={{ padding: '20px 10px' }}
              >
                <Button
                  onClick={() => {
                    handleSelectAddress(selectedAddress);
                    handleNext(1);
                  }}
                  style={{
                    border: '1px solid #FF6B6B',
                    color: '#014B7E',
                    backgroundColor: 'white',
                    borderRadius: '10px',
                  }}
                  size='small'
                >
                  Back
                </Button>
                {paymentData?.payment_type ?
                  <Button
                    variant='contained'
                    size='small'
                    color='primary'
                    disabled={selectedAddress === '' || !paymentData?.payment_type || !paymentData?.mode_of_payment || paymentData?.payment_type === 'PARTIAL' && !paymentData?.partial_amount}
                    onClick={() => handleSavePayment()}
                  >
                    Save
                  </Button>
                  :
                  <Button
                    variant='contained'
                    size='small'
                    color='primary'
                    disabled={selectedAddress === '' || timer}
                    onClick={() => generetePaymentLink()}
                  >
                    Generate Payment Link
                  </Button>
                }
              </Grid>
            </Card>
          </Grid>
          {openModel && (
            <Grid item md={12} xs={12}>
              <AddEditPaymentAddress
                open={openModel}
                edit={edit}
                close={handleClose}
                addressInfo={selectedAddress}
                studentId={studentId}
              />
            </Grid>
          )}
        </Grid>
      )}
      {deleteAddress ? (
        <ConfirmDialog
          open={deleteAddress}
          cancel={() => setDeleteAddress(false)}
          confirm={handleDeleteAddressInfo}
          title='Are you sure you want to delete ?'
        />
      ) : (
        ''
      )}
    </>
  );
};

export default PaymentAddress;
