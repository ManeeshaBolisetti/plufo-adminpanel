import React from 'react';
import { Grid, Typography, Divider } from '@material-ui/core';

const ShowPriceDetails = ({ selectedPrize }) => {
  function prizeDetailsInfo(label, amount) {
    return (
      <>
        <Grid item md={6} xs={6}>
          <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'left' }}>
            {label}
          </Typography>
        </Grid>
        <Grid item md={6} xs={6}>
          <Typography style={{ fontSize: '18px', margin: '0px', textAlign: 'right' }}>
            {amount}
          </Typography>
        </Grid>
      </>
    );
  }

  function addCommas(data) {
    const amount = JSON.stringify(data);
    const x = amount.split('.');
    let x1 = x[0];
    const x2 = x.length > 1 ? `.${x[1]}` : '';
    const rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      // eslint-disable-next-line no-useless-concat
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  }

  return (
    <Grid item md={12} xs={12}>
      {selectedPrize && (
        <Grid container spacing={2}>
          <Grid item md={2} xs={0} />
          <Grid item md={8} xs={12} style={{ padding: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12}>
                <Typography
                  style={{
                    fontSize: '20px',
                    margin: '0px',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}
                >
                  Price Details
                </Typography>
                <Divider style={{ margin: '5px 0px' }} />
              </Grid>
              {selectedPrize && selectedPrize?.actual_price === selectedPrize?.price ? (
                ''
              ) : (
                <>
                  {prizeDetailsInfo(
                    'Actual Batch Price',
                    `₹ ${selectedPrize && addCommas(selectedPrize?.actual_price)}`
                  )}
                  {prizeDetailsInfo(
                    'Discount',
                    `${selectedPrize && selectedPrize.discount_per}%`
                  )}
                  {prizeDetailsInfo(
                    'Total saving amount',
                    `₹ ${selectedPrize && addCommas(selectedPrize?.discount_amount)}`
                  )}
                </>
              )}
              {prizeDetailsInfo(
                'Your pay',
                `₹ ${selectedPrize && addCommas(selectedPrize?.price)}`
              )}
            </Grid>
            <Divider style={{ margin: '5px 0px' }} />
          </Grid>
          <Grid item md={2} xs={0} />
        </Grid>
      )}
    </Grid>
  );
};

export default ShowPriceDetails;
