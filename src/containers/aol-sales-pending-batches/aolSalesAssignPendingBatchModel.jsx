import React, { useEffect, useState, useContext } from 'react';
import { Grid, Card, Typography, TextField, Button } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { Autocomplete } from '@material-ui/lab';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import { ConverTime } from '../../components/dateTimeConverter';
import CustomDialog from '../../components/custom-dialog';
import ShowPriceDetails from '../aol-custom-batch/showPriceDetails';

const AolSalesPendingBatchesModel = ({ open, close, selectedItem }) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [weekList, setWeekList] = useState([]);
  const [timeList, setTimeList] = useState([]);
  const [selectedWeek, setSelectedWeek] = useState('');
  const [selectedTime, setSelectedTime] = useState('');
  const [startDate, setStartDate] = useState('');
  const [selectedPrize, setSelectedPrize] = useState('');

  function getDayWeekBatcPrizeList(api, key) {
    setLoading(true);
    axiosInstance
      .get(api)
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          if (key === 'week') {
            setWeekList(result?.data?.result);
          }
          if (key === 'time') {
            setTimeList(result?.data?.result);
            setSelectedPrize(result?.data?.result?.[0] || '');
          }
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  useEffect(() => {
    if (selectedItem) {
      getDayWeekBatcPrizeList(
        `${endpoints.salesAol.getBatchSize}?course_id=${
          selectedItem?.course_id
        }&batch_size=${selectedItem?.batch_size}${
          selectedItem?.course_type ? `&no_of_session=${selectedItem?.no_of_session}` : ''
        }`,
        'week'
      );
    }
  }, [selectedItem]);

  const batchViewCard = (value) => {
    return (
      <Grid item md={2} xs={12}>
        <Card
          hovable
          style={{
            background: '#FFD9D9',
            padding: '8px',
            borderTop: '5px solid #CECECE',
            borderBottom: '5px solid #CECECE',
            textAlign: 'center',
          }}
        >
          <Typography style={{ fontSize: '13px', fontWeight: 'bold' }}>
            {value}
          </Typography>
        </Card>
      </Grid>
    );
  };

  async function handleSelect() {
    if (!selectedWeek) {
      setAlert('warning', 'Select Days');
      return;
    }
    if (!selectedTime) {
      setAlert('warning', 'Select time slot');
      return;
    }
    if (!startDate) {
      setAlert('warning', 'Select Start Date');
      return;
    }
    setLoading(true);
    const payload = {
      id: selectedItem?.id,
      course_detail_id: selectedPrize?.id,
      time_slot: selectedTime,
      start_date: startDate,
      user: selectedItem?.user_id,
    };
    try {
      const { data } = await axiosInstance.put(
        endpoints.salesAol.updateStudentCourseDetail,
        { ...payload }
      );
      if (data.status_code === 200) {
        setLoading(false);
        setAlert('success', data.message);
        close('success');
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Assign Batch'
        maxWidth='md'
        // fullScreen
        stylesProps={{ zIndex: '1234', marginTop: '100px' }}
      >
        <Grid container spacing={2}>
          <Grid item md={12} xs={6}>
            <Grid container spacing={2}>
              {batchViewCard('Batch Size')}
              {batchViewCard('No of Sessions')}
            </Grid>
          </Grid>
          <Grid item md={12} xs={6}>
            <Grid container spacing={2}>
              {batchViewCard(`1 : ${selectedItem?.batch_size}`)}
              {batchViewCard(selectedItem?.no_of_session || '')}
            </Grid>
          </Grid>
          <Grid item md={12} xs={12}>
            <Grid container spacing={2}>
              <Grid item md={4} xs={12}>
                <Autocomplete
                  style={{ width: '100%', zIndex: '123456789' }}
                  size='small'
                  onChange={(event, value) => {
                    setSelectedWeek(value);
                    setTimeList([]);
                    setSelectedTime('');
                    setStartDate('');
                    if (value) {
                      getDayWeekBatcPrizeList(
                        `${endpoints.salesAol.getBatchSize}?course_id=${
                          selectedItem?.course_id
                        }&batch_size=${selectedItem?.batch_size}${
                          selectedItem?.course_type
                            ? `&no_of_session=${selectedItem?.no_of_session}`
                            : ''
                        }&days=${value}`,
                        'time'
                      );
                    }
                  }}
                  id='week-id'
                  className='dropdownIcon'
                  value={selectedWeek}
                  options={weekList}
                  getOptionLabel={(option) => option}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Days'
                      placeholder='Select Days'
                    />
                  )}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <Autocomplete
                  style={{ width: '100%', zIndex: '123456789' }}
                  size='small'
                  onChange={(event, value) => {
                    setSelectedTime(value);
                  }}
                  id='time-id'
                  className='dropdownIcon'
                  value={selectedTime}
                  options={timeList?.[0]?.time_slot_available || []}
                  getOptionLabel={(option) => (option ? ConverTime(option) : '')}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Time Slot'
                      placeholder='Select Time Slot'
                    />
                  )}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <MuiPickersUtilsProvider variant='outlined' utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    fullWidth
                    autoOk
                    disablePast
                    placeholder='Start Date'
                    helperText='Select Start Date'
                    value={startDate || ''}
                    style={{ width: '100%', zIndex: '123456789' }}
                    onChange={(data, value) => setStartDate(value)}
                    onError={console.log}
                    variant='outlined'
                    minDate={new Date('2018-01-01')}
                    format='yyyy-MM-dd'
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              {startDate && selectedPrize && selectedTime ? (
                <Grid item md={12} xs={12} style={{ display: 'none' }}>
                  <ShowPriceDetails selectedPrize={selectedPrize} />
                </Grid>
              ) : (
                ''
              )}
              <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
                <Button variant='contained' color='primary' onClick={handleSelect}>
                  Submit
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </CustomDialog>
      {loading && <Loader />}
    </>
  );
};

export default AolSalesPendingBatchesModel;
