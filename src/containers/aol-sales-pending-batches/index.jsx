import React, { useState, useContext, useEffect, useRef } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Button,
} from '@material-ui/core';
import debounce from 'lodash.debounce';
import { useHistory } from 'react-router-dom';
import { Pagination } from '@material-ui/lab';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import CustomSearchBar from '../../components/custom-seearch-bar';
import { DateTimeConverter, addCommas } from '../../components/dateTimeConverter';
import AolSalesPendingBatchesModel from './aolSalesAssignPendingBatchModel';
import displayName from '../../config/displayName';


const AolSalesPendingBatches = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');
  const [paymentList, setPaymentList] = useState('');
  const [page, setPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');

  async function ApiCall(url) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200) {
        setPaymentList(data);
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  const debounceFunc = useRef(
    debounce((q) => {
      setPage(1);
      const searchParm = q ? `&term=${q}` : '';
      ApiCall(`${endpoints.salesAol.pendingBatchList}?page=1${searchParm}&page_size=10`);
      if (displayName === 'My SparkleBox') {
        history.push(`/mysparklebox-sales-pending-batches/?page=${page}${searchParm}`);
      } else if (displayName === 'SparkleBox School') {
        history.push(`/sparklebox.school-sales-pending-batches/?page=${page}${searchParm}`);
      } else {
        history.push(`/plufo-sales-pending-batches/?page=${page}${searchParm}`);
      }
    }, 1000)
  ).current;

  useEffect(() => {
    const quiryDetails = new URLSearchParams(window.location.search);
    const pageNo = parseInt(quiryDetails.get('page'), 10) || 1;
    const search = quiryDetails.get('term') || '';
    setPage(pageNo);
    setSearch(search);
    const searchInfo = search ? `&term=${search}` : '';
    ApiCall(
      `${endpoints.salesAol.pendingBatchList}?page=${pageNo}${searchInfo}&page_size=10`
    );
  }, []);

  function handlePagination(event, page) {
    setPage(page);
    const searchInfo = search ? `&term=${search}` : '';
    if (displayName === 'My SparkleBox') {
      history.push(`/mysparklebox-sales-pending-batches/?page=${page}${searchInfo}`);
    } else if (displayName === 'SparkleBox School') {
      history.push(`/sparklebox.school-sales-pending-batches/?page=${page}${searchInfo}`);
    } else {
      history.push(`/plufo-sales-pending-batches/?page=${page}${searchInfo}`);
    }
    ApiCall(
      `${endpoints.salesAol.pendingBatchList}?page=${page}${searchInfo}&page_size=10`
    );
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName={displayName ? (displayName + ' Sales') : ''}
          childComponentName='Student Pending Details'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item md={4} xs={12}>
              <CustomSearchBar
                value={search}
                setValue={(value) => {
                  setSearch(value);
                  debounceFunc(value);
                }}
                onChange={(e) => {
                  setSearch(e.target.value.trimLeft());
                  debounceFunc(e.target.value.trimLeft());
                }}
                label=''
                placeholder='Search for student'
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Student Name</TableCell>
                      <TableCell align='left'>Date of Enrollment</TableCell>
                      <TableCell align='left'>Grade</TableCell>
                      <TableCell align='left'>Subject</TableCell>
                      <TableCell align='left'>Course Type</TableCell>
                      <TableCell align='left'>No. Of Sessions</TableCell>
                      <TableCell align='left'>Amount Paid</TableCell>
                      <TableCell align='left'>Assign</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {paymentList?.result?.length ? (
                      <>
                        {paymentList?.result.map((item, index) => (
                          <TableRow>
                            <TableCell align='left'>{index + 1}</TableCell>
                            <TableCell align='left'>{item?.student_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.date_of_enrollment
                                ? DateTimeConverter(item?.date_of_enrollment)
                                : ''}
                            </TableCell>
                            <TableCell align='left'>{item?.grade_name || ''}</TableCell>
                            <TableCell align='left'>{item?.subject_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.course_type ? 'Full Year' : 'Fixed' || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.no_of_session || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {`${addCommas(item?.total || '')} ₹` || ''}
                            </TableCell>
                            <TableCell align='left'>
                              <Button
                                onClick={() => {
                                  setSelectedItem(item);
                                  setOpen(true);
                                }}
                                variant='contained'
                                color='primary'
                                size='small'
                              >
                                Assign
                              </Button>
                            </TableCell>
                          </TableRow>
                        ))}
                        <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={paymentList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Batches Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {open && (
        <AolSalesPendingBatchesModel
          open={open}
          close={(status) => {
            setOpen(false);
            setSelectedItem('');
            if (status === 'success') {
              setPage(1);
              const searchInfo = search ? `&term=${search}` : '';
              ApiCall(
                `${endpoints.salesAol.pendingBatchList
                }?page=${1}${searchInfo}&page_size=10`
              );
            }
          }}
          selectedItem={selectedItem}
        />
      )}
      {loading && <Loader />}
    </Layout>
  );
};

export default AolSalesPendingBatches;
