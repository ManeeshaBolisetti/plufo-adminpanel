import React, { useState, useContext, useEffect, useRef } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  TextField,
} from '@material-ui/core';
import debounce from 'lodash.debounce';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useHistory } from 'react-router-dom';
import { Pagination } from '@material-ui/lab';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import CustomSearchBar from '../../components/custom-seearch-bar';

const StudentAttendence = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState('');
  const [studentAttendenceList, setStudentAttendenceList] = useState('');
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)
  const [selectDropDown,setSelectDropDown] =useState([])


  async function ApiCall(url) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200) {
        setStudentAttendenceList(data);
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }
  const ref = useRef({
    branchId: branchId
});
ref.current.branchId = branchId;

  const debounceFunc = useRef(
    debounce((q) => {
      // console.log("qqqq",ref.current.branchId)
      const searchParm = q ? `&term=${q}` : '';
      // console.log("fsakkf",branchId);
      ApiCall(
        `${endpoints.aolBatch.getStudentAttendence}?branch=${ref.current.branchId}&page=1${searchParm}&page_size=10`
      );
      history.push(`/aol-student-attendence/?branch=${ref.current.branchId}&page=${1}${searchParm}`);
    }, 1000)
  ).current;

  // useEffect(() => {
  //   debounceFunc.focus();
  // }, [])

  useEffect(() => {
    if(branchId){
      const quiryDetails = new URLSearchParams(window.location.search);
      const pageNo = parseInt(quiryDetails.get('page'), 10) || 1;
      const search = quiryDetails.get('term') || '';
      setPage(pageNo);
      setSearch(search);
      const searchInfo = search ? `&term=${search}` : '';
      const branchDetails = quiryDetails.get('branch') || '';
      const branchInfo = branchId ? `branch=${branchId}` : '';

      ApiCall(
        `${endpoints.aolBatch.getStudentAttendence}?${branchInfo}&page=${pageNo}${searchInfo}&page_size=10`
      );
    }
  }, [branchId]);

  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      setSelectDropDown(res.data.data)

    })
  }, [])

  function handlePagination(event, page) {
    setPage(page);
    if(page===1){
      setIndex(1)
    }else{
      setIndex(1+(page-1)*10)
    }
    const searchInfo = search ? `&term=${search}` : '';
    // const branchInfo = branchId ? `&`
    history.push(`/aol-student-attendence/?page=${page}${searchInfo}`);
    ApiCall(
      `${endpoints.aolBatch.getStudentAttendence}?branch=${branchId}&page=${page}${searchInfo}&page_size=10`
    );
  }
  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Attendance'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
      <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
            onChange={(event,value) =>{
              setBranchId(value?.id)
              setBranch(value)
            }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>
        <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
          {/* <Grid container direction='row' justify='center' alignItems='center'> */}
            <Grid item md={6} xs={12}>
              <CustomSearchBar
                value={search}
                setValue={(value) => {
                  setSearch(value);
                  debounceFunc(value);
                }}
                onChange={(e) => {
                  setSearch(e.target.value.trimLeft());
                  debounceFunc(e.target.value.trimLeft());
                }}
                label=''
                placeholder='Search for student'
              />
            </Grid>
          {/* </Grid> */}
        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Student Name</TableCell>
                      <TableCell align='left'>Personal Attendance</TableCell>
                      <TableCell align='left'>Course Attendance</TableCell>
                      <TableCell align='left'>Batch Name</TableCell>
                      <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Subject Name</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {studentAttendenceList?.results?.length ? (
                      <>
                        {studentAttendenceList?.results.map((item, i) => (
                          <TableRow>
                            <TableCell align='left'>{i + index}</TableCell>
                            <TableCell align='left'>{item?.student_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.personal_attendence || '0'}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.course_attendence || '0'}
                            </TableCell>
                            <TableCell align='left'>{item?.batch_name || ''}</TableCell>
                            <TableCell align='left'>{item?.course_name || ''}</TableCell>
                            <TableCell align='left'>{item?.subject_name || ''}</TableCell>
                          </TableRow>
                        ))}
                        <TableRow>
                          <TableCell colSpan='7'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={studentAttendenceList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='7'>
                          <CustomFiterImage label='Students Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default StudentAttendence;
