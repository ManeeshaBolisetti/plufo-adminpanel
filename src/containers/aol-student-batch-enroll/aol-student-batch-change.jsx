import React, { useState, useContext, useEffect } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Button,
  Typography,
} from '@material-ui/core';
import './style.scss';
import { useHistory, useLocation } from 'react-router-dom';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import ConfirmDialog from '../../components/confirm-dialog';
import StudentOnGoingBatches from './student-on-going-batches-model';
import StudentBatchCreationModel from './aol-student-create-batch-model';
import { ConverTime, DateTimeConverter } from '../../components/dateTimeConverter';
import Filteration from './filterationbatchchange';
import moment from 'moment';
import { Pagination } from '@material-ui/lab';

const StudentBatchChange = () => {
  const location = useLocation();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [userDetails, setUserDetails] = useState([]);
  const [studentBatchDetails, setStudentBatchDetails] = useState({});
  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [batchList, setBatchList] = useState([]);
  const { setAlert } = useContext(AlertNotificationContext);
  const [openCreate, setOpenCreate] = useState(false);
  const [openAssign, setOpenAssign] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  const [selectTime, setSelectedTime] = useState('')
  const [daysList, setDaysList] = useState([]);
  const [index, setIndex] = useState(1);
  const [batchDays, setBatchDays] = useState([])
  const [filterState, setFilterState] = useState({
    gradeList: [],
    selectedGrade: [],
    teacherList:[],
    selectedTeacher: [],
    subjectList: [],
    selectedSubject: [],
    startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
    endDate: moment()?.format()?.split('T')?.[0],
    fullDate: [],
    search: '',
  });

  const [selectDays,setSelectDays] = useState(location?.state?.selected_batch_days);
  async function apiCall(API, KEY) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(API);
      if (data?.status_code === 200) {
        setLoading(false);
        if (KEY === 'batchList') {
          setUserDetails(data);
        }
        if (KEY === 'batches') {
          setBatchList(data);
          setOpen(true);
        }
        if (KEY === 'days') {
          // console.log(data.result, 'checkii');
          setDaysList(data?.result[0] || []);
          setOpenCreate(true);
        }
        if(KEY === 'time') {
          setOpenCreate(true);
          // console.log(data?.result,'chy22========')
          setSelectedTime(data?.data || []);
        }
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  async function getStudentsList(pageNo, searchVal, grade, subject, teacher) {
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = grade?.length ? `&grades=${grade}` : '';
    const selectedSubject = subject ? `&subject_id=${subject}` : '';
    const selectedTeacher = teacher ? `&teacher_id=${teacher}` : '';
    // const selectedCourse = course ? `&course_id=${course}` : '';
    setLoading(true);
    try {
      setStudentBatchDetails(location?.state);
      const { data } = await axiosInstance.get(
        `${
          endpoints.aolBatchAssign.getStudentAllBatchesFilter
        }?page=${pageNo}&page_size=${10}${searchValue}${selectedGrade}${selectedSubject}${selectedTeacher}`
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setUserDetails(data);
      } else {
        setLoading(false);
        setUserDetails('');
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  function handlePagination(event, page) {
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * 10);
    }
    getStudentsList(
      page,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.selectedSubject?.id || '',
      filterState?.selectedTeacher?.tutor_id || '',
      // filterState?.courseList?.id || ''
      // filterState?.startDate || '',
      // filterState?.endDate || ''
    );
  }
  useEffect(() => {
    setStudentBatchDetails(location?.state);
    // apiCall(
    //   `${endpoints.aolBatchAssign.getStudentAllBaches}?batch_id=${location?.state?.current_batch}`,
    //   'batchList'
    // );
    setPage(1);
    getStudentsList(
      1,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.teacherList?.id || '',
      filterState?.courseList?.id || ''
    );
  }, [location?.state]);

  const TitleCards = (label) => {
    return (
      <Card
        style={{
          width: 'fit-content',
          background: '#707070',
          padding: '8px 20px',
          borderTop: '5px solid #CECECE',
          borderBottom: '5px solid #CECECE',
          textAlign: 'center',
        }}
      >
        <Typography style={{ color: 'white' }}>{label}</Typography>
      </Card>
    );
  };

  function handleAssign(data) {
    setSelectedItem(data);
    setOpenAssign(true);
  }

  async function handleSubmitAssign(info) {
    setLoading(true);
    const payload = {
      ...info,
      students: [studentBatchDetails?.erp_id],
      batch: studentBatchDetails?.current_batch,
    };
    try {
      const { data } = await axiosInstance.post(
        endpoints.aolBatchAssign.reAssgnBatchApi,
        {
          ...payload,
        }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setOpenAssign(false);
        setAlert('success', 'Assigned successfully');
        setSelectedItem('');
        history.goBack();
      } else {
        setLoading(false);
        setAlert('error', data?.error_msg);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  const batchViewCard = (value) => {
    return (
      <Grid item md={2} xs={12}>
        <Card
          hovable
          style={{
            background: '#FFD9D9',
            padding: '8px',
            borderTop: '5px solid #CECECE',
            borderBottom: '5px solid #CECECE',
            textAlign: 'center',
          }}
        >
          <Typography style={{ fontSize: '13px', fontWeight: 'bold' }}>
            {value}
          </Typography>
        </Card>
      </Grid>
    );
  };

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Reshuffle Requests'
          childComponentNameNext='Change Batch'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Grid container spacing={2} direction='row'>
                {TitleCards(studentBatchDetails?.student_name || '')}
                &nbsp;&nbsp;&nbsp;
                {TitleCards(
                  !studentBatchDetails?.is_fixed ? 'Full Year Course' : 'Fixed Course'
                )}
                &nbsp;&nbsp;&nbsp;
                {TitleCards(studentBatchDetails?.batch_name)}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={6}>
              <Grid container spacing={2}>
                {batchViewCard('Batch Size')}
                {batchViewCard('No. of Days')}
                {batchViewCard('Start Date')}
                {batchViewCard('End Date')}
                {studentBatchDetails?.selected_time_slot?.[0] && batchViewCard('Time')}
                {studentBatchDetails?.number_of_session &&
                  batchViewCard('No. of sessions')}
              </Grid>
            </Grid>
            <Grid item md={12} xs={6}>
              <Grid container spacing={2}>
                {batchViewCard(`1 : ${studentBatchDetails?.batch_size}`)}
                {batchViewCard(studentBatchDetails?.selected_batch_days?.[0] || '')}
                {batchViewCard(
                  studentBatchDetails?.start_date
                    ? DateTimeConverter(studentBatchDetails?.start_date)
                    : ''
                )}
                {batchViewCard(
                  studentBatchDetails?.end_date
                    ? DateTimeConverter(studentBatchDetails?.end_date)
                    : ''
                )}
                {studentBatchDetails?.selected_time_slot?.[0] &&
                  batchViewCard(
                    studentBatchDetails?.selected_time_slot?.[0] || ''
                    // studentBatchDetails?.time
                    //   ? `${ConverTime(studentBatchDetails?.time.start_time)} - ${ConverTime(
                    //       studentBatchDetails?.time.end_time
                    //     )}`
                    //   : ''
                  )}

                {studentBatchDetails?.number_of_session &&
                  batchViewCard(studentBatchDetails?.number_of_session || '')}
              </Grid>
            </Grid>
            <Grid item md={12} xs={12}>
              <Grid container spacing={2}>
                <Grid item md={3} xs={12} style={{ textAlign: 'center' }}>
                  <Button
                    style={{ borderRadius: '10px' }}
                    variant='contained'
                    className='action-btn'
                    color='primary'
                    onClick={() => {
                      apiCall(
                        `${endpoints.aolBatch.getRunningStudentBatchList}?student=${studentBatchDetails?.user}`,
                        'batches'
                      );
                    }}
                  >
                    Assigned Batches
                  </Button>
                </Grid>
                <Grid item md={3} xs={12} style={{ textAlign: 'center' }}>
                  {/* {console.log(location?.state?.current_batch,'dkh33')} */}
                  <Button
                    style={{ borderRadius: '10px' }}
                    className='action-btn'
                    variant='contained'
                    color='primary'
                    onClick={() => {
                      apiCall(
                        `${endpoints.aolBatchAssign.getAllDays}?batch_change_request_id=${location?.state?.id}`,
                        'days'
                      );
                      apiCall(
                        `${endpoints.aolBatchAssign.getTimeSlot}?aol_batch_id=${location?.state?.current_batch}`,
                        'time'
                      )
                    }}
                  >
                    Create New
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Filteration
              setPage={setPage}
              getStudentsList={getStudentsList}
              filterState={filterState}
              setFilterState={setFilterState}
              setLoading={setLoading}
            />
          </Grid>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S No</TableCell>
                      <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Batch Code</TableCell>
                      <TableCell align='left'>Seats Left</TableCell>
                      <TableCell align='left'>Start Date & Time</TableCell>
                      <TableCell align='left'>Days</TableCell>
                      <TableCell align='left'>No. of Sessions</TableCell>
                      <TableCell align='left'>Teacher Name</TableCell>
                      <TableCell align='left'>Assign</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userDetails?.results?.length !== 0 ? (
                      <>
                        {userDetails?.results?.map((item, i) => (
                          <TableRow key={item.id}>
                            <TableCell align='left'>{i + index}</TableCell>
                            <TableCell align='left'>
                              {item?.course?.course_name || ''}
                            </TableCell>
                            <TableCell align='left'>{item?.batch_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.seat_left === 0 ? 0 : item?.seat_left || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.start_date
                                ? DateTimeConverter(item?.start_date)
                                : ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.batch_days?.[0] || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.no_of_session || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.teacher_name || (
                                <Typography style={{ color: 'red' }}>NA</Typography>
                              )}
                            </TableCell>
                            <TableCell align='left'>
                              <Button
                                variant='contained'
                                size='small'
                                color='primary'
                                disabled={item?.seat_left === 0 && true}
                                onClick={() => handleAssign(item)}
                              >
                                Assign
                              </Button>
                            </TableCell>
                          </TableRow>
                        ))}
                        <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={userDetails?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colspan='6'>
                          <CustomFiterImage label='Batches are not found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item md={12} xs={12}>
          <Button
            onClick={() => history.goBack()}
            variant='contained'
            color='secondary'
            size='small'
          >
            Back
          </Button>
        </Grid>
      </Grid>
      {loading ? <Loader /> : ''}
      {open ? (
        <StudentOnGoingBatches
          open={open}
          close={() => {
            setOpen(false);
            setBatchList([]);
          }}
          data={batchList?.result || []}
        />
      ) : (
        ''
      )}
      {openCreate ? (
        <StudentBatchCreationModel
          open={openCreate}
          close={(info) => {
            setOpenCreate(false);
            if (info === 'success') {
              history.goBack();
            }
          }}
          daysList={daysList}
          selectTime={selectTime} 
          selectedData={studentBatchDetails || {}}
          batchChangeReqId={location?.state?.id}
        />
      ) : (
        ''
      )}
      {openAssign ? (
        <ConfirmDialog
          open={openAssign}
          cancel={() => {
            setOpenAssign(false);
            setSelectedItem('');
          }}
          selectedData={studentBatchDetails || {}}
          confirm={() => {
            handleSubmitAssign({
              new_batch: selectedItem?.id,
              request_batch_change_id: location?.state.id,
            });
          }}
          title='Are you sure to assign?'
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default StudentBatchChange;
