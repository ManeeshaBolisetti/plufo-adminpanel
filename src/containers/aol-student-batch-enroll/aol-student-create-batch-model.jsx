import React, { useState, useContext, useEffect } from 'react';
import { Grid, TextField, Button } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import CustomDialog from '../../components/custom-dialog';
import TimeSlotList from '../aol-batch-reference/timeSlotList';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';

const StudentBatchCreationModel = ({ open, close, selectedData, daysList, selectTime, batchChangeReqId }) => {
  // console.log({ open, close, selectedData, daysList, selectTime, batchChangeReqId }, 'mobilleeee9999.............')
  // console.log({open, close, selectedData, daysList, selectTime},'testing===')
  const [selectedDay, setSelectedDay] = useState('');
  const [startTime, setStartTime] = useState('');
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);

  // useEffect(() => {
  //   console.log(selectedData,'kdkao')
  
  // }, [])
  async function handleSubmit() {
    if (!selectedDay) {
      setAlert('warning', 'Select Days');
      return;
    }
    if (!startTime) {
      setAlert('warning', 'Select Time Slot');
      return;
    }
    setLoading(true);
    const payload = {
      student_id: selectedData?.erp_id,
      batch_id: selectedData?.current_batch,
      days: selectedDay,
      request_batch_change_id: batchChangeReqId,
      // start_time: startTime.slice(6, 8) === 'PM'
      // ? parseInt(startTime.slice(0, 2)) + 12 + ':' + startTime.slice(3, 5) + ':00'
      // : startTime.slice(0, 5) + ':00',
      start_time: startTime,
      
    };
    try {
      const { data } = await axiosInstance.put(
        endpoints.aolBatchAssign.createNewBatchApi,
        {
          ...payload,
        }
      );
      if (data.status_code === 200) {
        setLoading(false);
        close('success');
        setAlert('success', data?.message);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }
  const  allTime = [
    "09:00:00",
    "10:00:00",
    "11:00:00",
    "12:00:00",
    "13:00:00",
    "14:00:00",
    "15:00:00",
    "16:00:00",
    "17:00:00",
    "18:00:00",
    "19:00:00",
    "20:00:00", 

]

const  customeTime = [
  "16:00:00",
  "17:00:00",
  "18:00:00",
  "19:00:00",
  "20:00:00", 

]

const  emptyCustomeTime = [ 

]


  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Create New Batch'
        maxWidth='xs'
        fullScreen={false}
        stylesProps={{ zIndex: '1' }}
      >
        <Grid container spacing={3} style={{ padding: '10px' }}>
          <Grid item md={12} xs={12}>
            <Autocomplete
              size='small'
              id='days'
              className='dropdownIcon'
              style={{ width: '100%' }}
              options={daysList}
              getOptionLabel={(option) => option || ''}
              filterSelectedOptions
              value={selectedDay || ''}
              onChange={(event, value) => setSelectedDay(value)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant='outlined'
                  label='Select Days'
                  placeholder='Select Days'
                />
              )}
            />
          </Grid>
          <Grid item md={12} xs={12}>
            <Autocomplete
              size='small'
              id='daysCombination'
              className='dropdownIcon'
              style={{ width: '100%' }}

              // options={selectedDay.length >11 ? allTime: selectTime}
              options={selectedDay?(selectedDay?.length >=11? allTime : customeTime ):emptyCustomeTime}
              getOptionLabel={(option) => option || ''}
              filterSelectedOptions
              value={startTime || ''}
              onChange={(event, value) => setStartTime(value)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant='outlined'
                  label='Select Time Slots'
                  placeholder='Select Time Slots'
                  // helperText='Slots are only available between 7AM - 9PM'
                />
              )}
            />
          </Grid>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Button variant='contained' color='primary' onClick={() => handleSubmit()}>
              Submit
            </Button>
          </Grid>
        </Grid>
      </CustomDialog>
      {loading && <Loader />}
    </>
  );
};

export default StudentBatchCreationModel;
