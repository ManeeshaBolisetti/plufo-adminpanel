import React, { useState, useContext, useEffect } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Button,
  Typography,
} from '@material-ui/core';
import './style.scss';
import { useHistory } from 'react-router-dom';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { DateTimeConverter } from '../../components/dateTimeConverter';
import ViewRequest from './viewRequest';
import Filteration from './filteration';
import TeacherChange from './teacher-change-model';

const StudentBatchEnroll = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [page, setPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [openTeacherChange, setOpenTeacherChange] = useState(false);
  const [content, setContent] = useState('');
  const [filterState, setFilterState] = useState({
    gradeList: [],
    selectedGrade: [],
    subjectList: [],
    selectedSubject: [],
    startDate: '',
    endDate: '',
    fullDate: [],
    search: '',
  });

  async function getStudentsList(pageNo, searchVal, grade, subject, startDate, endDate) {
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = grade?.length ? `&grades=${grade}` : '';
    const selectedSubject = subject ? `&subject_id=${subject}` : '';
    const start = startDate ? `&start_date=${startDate}` : '';
    const end = endDate ? `&end_date=${endDate}` : '';
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${
          endpoints.aolBatchAssign.batchChangeRequest
        }?is_super_user=true&page=${pageNo}&page_size=${10}${searchValue}${selectedGrade}${selectedSubject}${start}${end}`
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setStudentBatchList(data);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  function handlePagination(event, page) {
    setPage(page);
    getStudentsList(
      page,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.selectedSubject?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }

  function handleUpdateChanges(info) {
    // console.log(info.is_teacher_change)
    // if(info.is_teacher_change && info.currentB) {
    //   setContent(info)
    //   setOpenTeacherChange(true)
    // }
    // else {
    history.push({
      pathname: '/aol-student-enroll-batch-change',
      state: {
        ...info,
      },
    });
    // }
  }

  useEffect(() => {
    setPage(1);
    getStudentsList(
      1,
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }, []);

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Reshuffle Requests'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
          <Filteration
            setPage={setPage}
            getStudentsList={getStudentsList}
            filterState={filterState}
            setFilterState={setFilterState}
            setLoading={setLoading}
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Student Name</TableCell>
                      <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Requested Batch Days</TableCell>
                      <TableCell align='left'>Teacher Change</TableCell>
                      <TableCell align='left'>Requested Message</TableCell>
                      <TableCell align="left">Enrollment Number</TableCell>
                      <TableCell align='left'>Payment Status</TableCell>
                      <TableCell align='left'>Status</TableCell>
                      <TableCell align='left'>Batches</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {studentBatchList?.results?.length ? (
                      <>
                        {studentBatchList?.results.map((item, index) => (
                          <TableRow key={item.id}>
                            <TableCell align='left'>
                              {(page - 1) * 10 + index + 1}
                            </TableCell>
                            <TableCell align='left'>{item?.student_name}</TableCell>
                            <TableCell align='left'>
                              {/* {item?.date_of_enrollment
                                ? DateTimeConverter(item?.date_of_enrollment)
                                : ''} */}
                              {item?.course_name}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.selected_batch_days?.[0]}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.is_teacher_change ? 'Yes' : 'No'}
                            </TableCell>
                            <TableCell align='left'>{item?.description}</TableCell>
                            <TableCell align="left">{item?.enrollment_no || (
                              <Typography style={{color:'red'}}>NA</Typography>
                            )}</TableCell>
                            <TableCell align='left'>
                              {' '}
                              {item?.is_paid ? 'Paid' : 'Not Paid'}
                            </TableCell>
                            <TableCell align='left'>
                              <Typography>{item?.status || ''}</Typography>
                              <Button
                                // style={
                                //   item?.check_request
                                //     ? {
                                //         background: '#FEE6A9',
                                //         borderRadius: '10px',
                                //         border: '1px solid #FBBB17',
                                //       }
                                //     : {}
                                // }
                                variant='contained'
                                color='primary'
                                size='small'
                                disabled={
                                  item?.is_online_class_exists === false ||
                                  item?.status !== 'Requested' ||
                                  item?.is_paid === false
                                }
                                onClick={() => {
                                  setOpen(true);
                                  setContent(item);
                                }}
                              >
                                Action
                              </Button>
                            </TableCell>
                            <TableCell align='left'>
                              <Typography>{item?.batch_name || ''}</Typography>
                              <Button
                                variant='contained'
                                color='primary'
                                size='small'
                                disabled={
                                  item?.is_online_class_exists === false ||
                                  item?.status !== 'Approved'
                                }
                                onClick={() => handleUpdateChanges(item)}
                              >
                                Change
                              </Button>
                            </TableCell>
                          </TableRow>
                        ))}
                        <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={studentBatchList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Batches Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {loading && <Loader />}
      {open && (
        <ViewRequest
          content={content}
          open={open}
          close={(status) => {
            setOpen(false);
            setContent('');
            if (status === 'success') {
              // setPage(1);
              getStudentsList(
                page,
                filterState?.search || '',
                filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                filterState?.subjectList?.id || '',
                filterState?.startDate || '',
                filterState?.endDate || ''
              );
            }
          }}
        />
      )}
      {openTeacherChange && (
        <TeacherChange
          content={content}
          open={openTeacherChange}
          close={(status) => {
            setOpenTeacherChange(false);
            setContent('');
            if (status === 'success') {
              // setPage(1);
              getStudentsList(
                page,
                filterState?.search || '',
                filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                filterState?.subjectList?.id || '',
                filterState?.startDate || '',
                filterState?.endDate || ''
              );
            }
          }}
        />
      )}
    </Layout>
  );
};

export default StudentBatchEnroll;
