import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Button, Divider, TextField } from '@material-ui/core';
import CustomDialog from '../../components/custom-dialog';
import ConfirmDialog from '../../components/confirm-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { Autocomplete } from '@material-ui/lab';

const TeacherChange = ({ content, open, close }) => {

  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [teacherData, setTeacherData] = useState([]);
  const [selectedTeacher, setSelectedTeacher] = useState('')

  useEffect(() => {
    getTeachers()
  }, [])

  const getTeachers = async() => {
    setLoading(false)
    try {
        const { data } = await axiosInstance.get(
          `${endpoints.aolBatchAssign.getTeacherList}`,
        );
        if (data) {
          console.log(data)
          setLoading(false);
          setTeacherData(data)
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
  }

  async function AssignTeacher() {
    setLoading(true);
    const payload = {
      teacher_id: selectedTeacher?.tutor_id,
      batch_id: content?.current_batch,
    };
    try {
      const { data } = await axiosInstance.put(
        endpoints.aolTeachesBatchAssign.assignTeacherToBatch,
        { ...payload }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setAlert('success', data.message);
        close('success')
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }
  return (
    <>
      <CustomDialog
        open={open}
        handleClose={close}
        dialogTitle={`Teacher Change Request from ${content?.student_name || ''}`}
        maxWidth='sm'
        fullScreen={false}
        stylesProps={{ zIndex: '1' }}
      >
        <Grid container spacing={3} style={{ padding: '10px' }} direction='column' alignItems='center' justify='center'>
          <Grid item md={12} xs={12}>
            <Autocomplete
                size='small'
                className='dropdownIcon'
                options={teacherData?.data|| []}
                getOptionLabel={(option) => option?.name || ''}
                filterSelectedOptions
                style={{width: 400}}
                value={selectedTeacher || ''}
                onChange={(event, value) => {
                    setSelectedTeacher(value)
                }}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        size='small'
                        variant='outlined'
                        label='Teacher'
                        placeholder='Select teacher'
                    />
                )}
            />
          </Grid>
          <Grid item md={12} xs={12} style={{ margin: '10px 0px' }}>
            <Grid
              container
              spacing={2}
              direction='row'
              alignItems='center'
              justify='center'
            >
              <Button
                onClick={AssignTeacher}
                disabled={selectedTeacher === ''}
                variant='contained'
                color='primary'
                size='small'
              >
                Assign
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </CustomDialog>
      {loading && <Loader />}
    </>
  );
};

export default TeacherChange;
