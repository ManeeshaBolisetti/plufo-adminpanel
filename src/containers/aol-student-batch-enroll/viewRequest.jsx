import React, { useState, useContext } from 'react';
import { Grid, Typography, Button, Divider } from '@material-ui/core';
import CustomDialog from '../../components/custom-dialog';
import ConfirmDialog from '../../components/confirm-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';

const ViewRequest = ({ content, open, close }) => {
  const [confirm, setConfirn] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [type, setType] = useState('');
  const [loading, setLoading] = useState(false);

  async function handleConfirm(info) {
    setLoading(true);
    const payload = {
      // is_accept: info,
      // user: content?.user,
      // id: content?.check_request?.id,
      status: info,
    };
    try {
      const { data } = await axiosInstance.patch(
        `${endpoints.aolBatchAssign.batchChangeRequest}/${content?.id}/?is_super_user=true`,
        {
          ...payload,
        }
      );
      if (data) {
        setLoading(false);
        setAlert('success', 'Successfully Updated');
        close('success');
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }
  return (
    <>
      <CustomDialog
        open={open}
        handleClose={close}
        dialogTitle={`Batch Change Request from ${content?.student_name || ''}`}
        maxWidth='sm'
        fullScreen={false}
        stylesProps={confirm ? { zIndex: '0' } : {}}
      >
        <Grid container spacing={2}>
          {/* <Grid item md={12} xs={12}>
            <Typography>{content?.check_request?.message || ''}</Typography>
          </Grid>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Divider style={{ margin: '5px 0px' }} />
          </Grid> */}
          <Grid item md={12} xs={12} style={{ margin: '10px 0px' }}>
            <Grid
              container
              spacing={2}
              direction='row'
              alignItems='center'
              justify='center'
            >
              <Button
                onClick={() => {
                  setConfirn(true);
                  setType('Rejected');
                }}
                variant='contained'
                color='primary'
                size='small'
              >
                Reject
              </Button>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <Button
                onClick={() => {
                  setConfirn(true);
                  setType('Approved');
                }}
                style={{ backgroundColor: '#00B792' }}
                variant='contained'
                color='primary'
                size='small'
              >
                Accept
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </CustomDialog>
      {confirm && (
        <ConfirmDialog
          open={confirm}
          cancel={() => {
            setConfirn(false);
            setType('');
          }}
          confirm={() => {
            handleConfirm(type);
          }}
          title={`Are you sure to ${type}`}
        />
      )}
      {loading && <Loader />}
    </>
  );
};

export default ViewRequest;
