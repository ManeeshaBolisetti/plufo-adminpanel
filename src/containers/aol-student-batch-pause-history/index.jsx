import React, { useState, useContext, useEffect } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
  IconButton,
  Typography,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Filteration from './filteration';
import Loader from '../../components/loader/loader';
import {
  DateTimeConverter,
  addCommas,
  ConverTime,
} from '../../components/dateTimeConverter';
import EditIcon from '@material-ui/icons/EditOutlined';
import { Autocomplete } from '@material-ui/lab';
import StudentLeaveModel from './menu-student-desk-model';
// import BatchPauseAssignToExistingBatchModel from './batch-pause-assign-to-existing-batch-model';
import BatchPauseAssignToExistingBatchModel from './batch-pause-assign-to-existing-batch-model';
import CustomDialog from 'components/custom-dialog';

const BatchPauseHistory = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [open, setOpen] = useState(false);
  const [isPreviousActive, setIsPreviousActive] = useState(false)
  const [selectedItem, setSelectedItem] = useState('');
  // const [branch,setBranch] = useState([]);
  // const [selectDropDown,setSelectDropDown] =useState([])
  const [branchId, setBranchId] = useState(null)
  const [filterState, setFilterState] = useState({
    // gradeList: [],
    // selectedGrade: [],
    // subjectList: [],
    // selectedSubject: [],
    // startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
    // endDate: moment()?.format()?.split('T')?.[0],
    // startDate: '',
    // endDate: '',
    // fullDate: [],
    search: '',
    branchList:[],  
    selectedBranch: '',
  });

  async function getStudentsList(pageNo, user, searchVal, grad, sub, startDate, endDate, branch) {
    // console.log({ pageNo, user, searchVal, grad, sub, startDate, endDate, branch }, 'opp999')
    setFilterState({ ...filterState, selectedBranch: branch,search:searchVal })
    const searchUser = user ? `&user=${user}` : '';
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = grad?.length ? `&grades=${grad}` : '';
    const selectedSubject = sub ? `&subject_id=${sub}` : '';
    const start = startDate ? `&start_date=${startDate}` : '';
    const end = endDate ? `&end_date=${endDate}` : '';
    if (branch) {
      setLoading(true);
      try {
        const { data } = await axiosInstance.get(
          `${endpoints.aolBatchAssign.getBatchPauseHistory}?&page=${pageNo ? pageNo : page
          }&page_size=${10}&branch=${branch}${searchUser}${searchValue}${selectedGrade}${selectedSubject}${start}${end}`
        );
        if (data) {
          setLoading(false);
          setStudentBatchList(data);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    }
  }

  function handlePagination(event, page) {
    event.preventDefault();
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * 10);
    }
    getStudentsList(
      page,
      filterState?.search || '',
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || '',
      filterState?.selectedBranch || '',
    );
  }

  useEffect(() => {
    setPage(1);
    getStudentsList(
      1,
      filterState?.search || '',
      filterState?.search || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || '',
      filterState?.selectedBranch || '',
      // filterState?.branchList || '',
    );
  }, []);

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Batch Pause History'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Filteration
            setPage={setPage}
            getStudentsList={getStudentsList}
            filterState={filterState}
            setFilterState={setFilterState}
            setLoading={setLoading}
          />
        </Grid>
      </Grid>
      <div>
        <Divider />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Name</TableCell>
                      <TableCell align='left'>ERP ID</TableCell>
                      {/* <TableCell align='left'>Batch Id</TableCell> */}
                      <TableCell align='left'>Pause Date</TableCell>
                      <TableCell align='left'>Batch Size</TableCell>
                      {/* <TableCell align='left'>Previous Batch Id</TableCell> */}
                      <TableCell align='left'>Previous Batch Name</TableCell>
                      <TableCell align="left">Enrollment Number</TableCell>
                      {/* <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Teacher Name</TableCell>
                      <TableCell align='left'>Batch Name</TableCell>
                      <TableCell align='left'>Batch Start Date & Time</TableCell>
                      <TableCell align='left'>Payment</TableCell> */}
                      <TableCell align='left'>Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {studentBatchList?.result?.length ? (
                      <>
                        {studentBatchList?.result
                          .filter((item) => item.aol_batch === null)
                          .map((item, i) => (
                            <TableRow key={item.id}>
                              <TableCell align='left'>{i + index}</TableCell>
                              <TableCell align='left'>
                                {item?.user_name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align='left'>{item?.user_erp}</TableCell>
                              {/* <TableCell align='left'>
                                {item?.aol_batch || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell> */}
                              <TableCell align='left'>
                                {item?.pause_date || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align='left'>
                                {`1 : ${item?.batch_size}` || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              {/* <TableCell align='left'>
                                {item?.previous_batch_id || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell> */}
                              <TableCell align='left'>
                                {item?.previous_batch_name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align='left'>
                                {item?.enrollment_no || (
                                  <Typography style={{color: 'red', fontFamily:'serif', fontWeight:600}}>NA</Typography>
                                )}
                              </TableCell>
                              {/* <TableCell align='left'>
                              <span style={{ color: '#FF6B6B' }}>
                                {item?.reference_start_date
                                  ? DateTimeConverter(item?.reference_start_date)
                                  : ''}
                              </span>
                              <br />
                              <span style={{ color: '#014B7E' }}>
                                {item?.batch_time_slot
                                  ? `${ConverTime(
                                      item?.batch_time_slot?.split('-')[0]
                                    )} - ${ConverTime(
                                      item?.batch_time_slot?.split('-')[1]
                                    )}`
                                  : '' || ''}
                              </span>
                            </TableCell> */}
                              {/* <TableCell align='left'>
                              {item?.paid_date ? (
                                <span style={{ color: 'green' }}>
                                  {`${addCommas(item?.amount_paid) || ''} ₹` || ''}
                                  <br />
                                  &nbsp;Paid on&nbsp;
                                  {DateTimeConverter(item?.paid_date)}
                                </span>
                              ) : (
                                <span style={{ color: 'red' }}>Fail</span>
                              )}
                            </TableCell> */}
                              <TableCell align='left'>
                                <Button
                                  size='small'
                                  color='primary'
                                  variant='contained'
                                  disabled={item?.aol_batch !== null}
                                  onClick={() => {
                                      setIsPreviousActive(item?.is_previous_active)
                                      setOpen(true);
                                      setSelectedItem(item);
                                  }}
                                  title='Course Extend'
                                >
                                  Assign
                                  {/* <EditIcon disabled = {item?.aol_batch===null} /> */}
                                </Button>
                              </TableCell>
                            </TableRow>
                          ))}
                        <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={studentBatchList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Batches Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {loading && <Loader />}
      {open ? (
        <StudentLeaveModel
          content={selectedItem}
          studentlist={getStudentsList}
          open={open}
          close={() => {
            setOpen(false);
            setSelectedItem('');
          }}
          isPreviousActive={isPreviousActive}
          selectedBranch={filterState?.selectedBranch}
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default BatchPauseHistory;
