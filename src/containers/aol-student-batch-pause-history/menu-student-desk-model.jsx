import React, { useState, useContext } from 'react';
import { Grid, TextField, Button, Divider } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import CustomDialog from '../../components/custom-dialog';
import TimeSlotList from '../aol-batch-reference/timeSlotList';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { useHistory } from 'react-router-dom';
import BatchPauseAssignToExistingBatchModel from './batch-pause-assign-to-existing-batch-model';

const MenuStudentDeskModel = ({ content, studentlist, open, close,isPreviousActive, selectedBranch}) => {
  // console.log({ content, studentlist, open, close,isPreviousActive,selectedBranch}, 'mobi999')
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const dropdownData1 = [
    {
      id: 1,
      value: 'Assign To Last Batch',
    },
    {
      id: 2,
      value: 'Assign To New/Existing Batch',
    },
  ];
  const dropdownData2 = [
    // {
    //   id: 1,
    //   value: 'Assign To Last Batch',
    // },
    {
      id: 2,
      value: 'Assign To New/Existing Batch',
    },
  ];

  const [selectedMenu, setSelectedMenu] = useState('');

  function handleAssign(data,selectedBranch) {
    // console.log(data,'ppp77')
    // console.log({selectedBranch},'shiva777')
    history.push({
      pathname: `/aol-batch-pause-new-batch-assign`,
      state: {
        ...data,selectedBranch
      },
    });
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={12} xs={12}>
          <CustomDialog
            handleClose={close}
            open={open}
            dialogTitle='Update Details'
            maxWidth='md'
            fullScreen={false}
            stylesProps={{ zIndex: '1', marginTop: 50 }}
          >
            <Grid container spacing={3} style={{ padding: '10px' }}>
              <Grid item md={12} xs={12}>
                <Autocomplete
                  size='small'
                  className='dropdownIcon'
                  options={isPreviousActive ? dropdownData1 : dropdownData2 || []}
                  getOptionLabel={(option) => option?.value || ''}
                  filterSelectedOptions
                  style={{ width: 400 }}
                  value={selectedMenu || ''}
                  onChange={(event, value) => {
                    setSelectedMenu(value);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      size='small'
                      variant='outlined'
                      label='Menu'
                      placeholder='Select Item'
                    />
                  )}
                />
              </Grid>
            </Grid>
            {selectedMenu?.id ===1 && (
              <BatchPauseAssignToExistingBatchModel
                getstudentlist={studentlist}
                disable={content?.aol_batch === null}
                content={content}
                open={open}
                close={close}
              />
            )}
            {selectedMenu?.id === 2  && handleAssign(content,selectedBranch)}
          </CustomDialog>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </>
  );
};

export default MenuStudentDeskModel;
