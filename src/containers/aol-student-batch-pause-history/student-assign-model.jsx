import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Button, TextField } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import CustomDialog from '../../components/custom-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import { getWeekList } from 'utility-functions';
import moment from 'moment';
import { Autocomplete } from '@material-ui/lab';

const BatchPauseAssignModel = ({ open, close, selectedData, handleSubmitAssign }) => {
  const [startDate, setStartDate] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);
  const [dateList, setDateList] = useState([]);
  const weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  async function handleSubmit() {
    if (!startDate) {
      setAlert('warning', 'Select Start Date');
      return;
    }
    let sel = moment(startDate, "DD/MM/YYYY");
    let newDateString = sel.format("YYYY-MM-DD");
    const payload = {
      student_id: selectedData?.user_id,
      order_id: selectedData?.id,
      start_date: newDateString,
    };
    handleSubmitAssign(payload);
  }

  useEffect(()=>{
    let selday = selectedData?.days?.[0].split('/')[0];
    getWeeks(weekdays.indexOf(selday))

  },[])

  const getWeeks = (dayIdx) => {
    setDateList(getWeekList(dayIdx, selectedData))
  }

  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Assign'
        maxWidth='xs'
        fullScreen={false}
        stylesProps={{ zIndex: '1' }}
      >
        <Grid container spacing={3} style={{ padding: '10px' }}>
          <Grid item md={12} xs={12}>
            <Typography>{`Days : ${selectedData?.days?.[0]}`}</Typography>
          </Grid>
          {/* <Grid item md={12} xs={12}>
            <MuiPickersUtilsProvider
              variant='outlined'
              utils={DateFnsUtils}
              style={{ zIndex: '9000000000' }}
            >
              <KeyboardDatePicker
                fullWidth
                autoOk
                disablePast
                placeholder='Start Date'
                helperText='Select Start Date'
                value={startDate || ''}
                // onChange={(data, value) => setStartDate(value)}
                onChange={(data, value) => handleDate(value)}
                onError={console.log}
                variant='outlined'
                minDate={new Date('2018-01-01')}
                format='yyyy-MM-dd'
              />
            </MuiPickersUtilsProvider>
          </Grid> */}
          <Grid item md={12} xs={12}>
            <Autocomplete
              size='small'
              id='daysCombination'
              className='dropdownIcon'
              style={{ width: '100%' }}
              options={dateList}
              getOptionLabel={(option) => option || ''}
              filterSelectedOptions
              value={startDate || ''}
              onChange={(event, value) => setStartDate(value)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant='outlined'
                  label='Select Start Date'
                  placeholder='Select Start Date'
                />
              )}
            />
          </Grid>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Button variant='contained' color='primary' onClick={() => handleSubmit()}>
              Submit
            </Button>
          </Grid>
        </Grid>
      </CustomDialog>
    </>
  );
};

export default BatchPauseAssignModel;
