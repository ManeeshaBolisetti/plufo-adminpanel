import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Button, Divider, TextField, Radio, Select, RadioGroup, FormControl, FormLabel } from '@material-ui/core';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { Autocomplete } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
 
  MuiFormControlLabel: {
    minWidth: 100,
  },
  label:{
    marginTop:10,
    marginBottom:10
  },
  button:{
    textAlign:'center',
    marginTop:90
  }

}));


const BatchChangeModel = ({ content, open, close }) => {
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const history = useHistory();
  const [erpId, setErpId] = useState('');
  const [loading, setLoading] = useState(false);
  //   const [open, setOpen] = useState(false);
  const [fileData, setFileData] = useState(content);
  const [modalName, setModalName] = useState('');
  const [selectedWeek, setSelectedWeek] = useState('')
  const [selectedTime, setSelectedTime] = useState('');
  const [teacherChange, setTeacherChange] = useState('no')
  const [reqMessage, setReqMessage] = useState('')
  const [weekList, setWeekList] = useState([])
  const [timeList, setTimeList] = useState([]);
  const [reqLoading, setReqLoading] = useState(false);


  useEffect(() => {
    setLoading(true)
    const bearerToken = localStorage.getItem('token');
    axiosInstance
      // .get(`${endpoints.students.batchDaysListing}?is_batch_days=true&course_id=${content?.course_id}`,)
      .get(`${endpoints.aolBatchAssign.getAllDays}?batch_id=${fileData.aol_batch}&list_all=true`,)
      .then((res) => {
        setLoading(false);
        if (res.data.length !== 0) {
          setWeekList(res.data.result)
        }
      }).catch(() => {
        setLoading(false);
      });

      // axiosInstance
      // .get(`${endpoints.students.batchDaysListing}?is_time_slot=true&course_id=${content?.course_id}&batch_days=${e.target.value}`,)
      // .then((res) => {
      //   console.log('value:', res);
      //   setLoading(false);
      //   if (res.data.length !== 0) {
      //     setTimeList(res.data)
      //   }
      // }).catch(() => {
      //   setLoading(false);
      // });

  }, [])


  function submitRequest() {
    if (!selectedWeek) {
      setAlert('error', 'Select Week')
    }
    // else if (!selectedTime) {
    //   message.error('Select Time Slot')
    // }
    else if (!reqMessage) {
      setAlert('error', 'Please Enter Description')
    }
    else {
      setLoading(true);
      setReqLoading(true);
      const data = {
        current_batch: content?.aol_batch,
        selected_days: selectedWeek,
        batch_time_slot: null,
        status: 'Requested',
        is_teacher_change: teacherChange === 'yes' ? true : false,
        description: reqMessage,
        user_id: content?.student_id,
      }
      const bearerToken = localStorage.getItem('token');
      axiosInstance
        .post(`${endpoints.students.batchChangeRequest}`, data)
        .then((res) => {
          if (res.status === 200) {
            setAlert('success', res.data.Message);
            setLoading(false);
            setReqLoading(false);
            setWeekList([]);
            setTimeList([]);
            setSelectedTime(null);
            setSelectedWeek('');
            setTeacherChange('no');
            setReqMessage('')
            setModalName('')
          } else {
            setAlert('error', res.data.Message)
          }


        }).catch(() => {
          setLoading(false);
          setReqLoading(false);
          setAlert('error', 'Batch Request is Open for this batch')
        });
    }
  }
  return (
    <>
      <Grid container spacing={3} >
        <Grid item md={12} xs={12}>
          <Typography variant="h6" style={{ fontSize: 20, fontWeight: '600', color: '#014B82', marginTop: 20 }}>Batch Change</Typography>
          <Divider />
        </Grid>
        <Grid item md={6}>
          <Autocomplete
            size='small'
            className='dropdownIcon'
            options={weekList || []}
            getOptionLabel={(option) => option || ''}
            filterSelectedOptions
            value={selectedWeek || ''}
            onChange={(event, value) => {
              setSelectedWeek(value);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                size='small'
                variant='outlined'
                label='Select Days'
                placeholder='Select Days'
              />
            )}
          />
        </Grid>

        <Grid item md={6}>
          <Grid container spacing={2} >
            <Grid item md={5}>
              <FormLabel className={classes.label} component="legend" required>Teacher Change</FormLabel>
            </Grid>
            <Grid item md={7}>
              <RadioGroup
                row
                name="position"
                onChange={e => setTeacherChange(e.target.value)}
                value={teacherChange}>
                <FormControlLabel className={classes.MuiFormControlLabel} value="yes" control={<Radio />} label="Yes" />
                <FormControlLabel className={classes.MuiFormControlLabel} value="no" control={<Radio color='primary' />} label="No" />
              </RadioGroup>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={6} >
          <FormLabel className={classes.label} component="legend" required>Description</FormLabel>
          <TextField
            variant='outlined'
            style={{ width: '100%', borderRadius: 5 }}
            multiline
            rows={3}
            value={reqMessage}
            placeholder='Please Provide your Preferred Time'
            onChange={(event) => { setReqMessage(event.target.value) }}
          />
        </Grid>
        <Grid item md={6} className={classes.button}>
          <Button
            key="submit"
            variant='contained'
            color='primary'
            size='small'
            disabled={reqMessage === '' || teacherChange === '' || selectedWeek === ''}
            loading={reqLoading}
            onClick={submitRequest}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </>
  );
};

export default BatchChangeModel;