import React, { useState, useContext, useEffect } from 'react';
import {
  Grid,
  Typography,
  Button,
  Divider,
  TextField,
  Radio,
  Select,
  RadioGroup,
  FormControl,
  FormLabel,
  Input,
} from '@material-ui/core';
import {
  DatePicker,
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { Autocomplete } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles } from '@material-ui/core/styles';
import { Today } from '@material-ui/icons';
import { format } from 'date-fns';

const useStyles = makeStyles((theme) => ({
  MuiFormControlLabel: {
    minWidth: 100,
  },
  label: {
    marginTop: 10,
    marginBottom: 10,
  },
  button: {
    textAlign: 'center',
    marginTop: 10,
    justifyContent:'center',
  },
}));

const BatchPauseModel = ({ content, open, close }) => {
  var date;
  console.log('content', content);
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const history = useHistory();
  const [erpId, setErpId] = useState('');
  const [loading, setLoading] = useState(false);
  const [modalName, setModalName] = useState('');
  const [selectedDate, handleDateChange] = useState(new Date());
  const [reqMessage, setReqMessage] = useState('');
  const [reqLoading, setReqLoading] = useState(false);

  // useEffect(() => {
  //   setLoading(true);
  //   const bearerToken = localStorage.getItem('token');
  //   axiosInstance
  //     .get(
  //       `${endpoints.students.batchDaysListing}?is_batch_days=true&course_id=${content?.course_id}`
  //     )
  //     .then((res) => {
  //       console.log('res.data', res.data);
  //       setLoading(false);
  //       if (res.data.length !== 0) {
  //       }
  //     })
  //     .catch(() => {
  //       setLoading(false);
  //     });
  // }, []);

  function submitRequest() {
    if (!selectedDate) {
      setAlert('error', 'Please Select Batch Pause Date');
    } else {
      setLoading(true);
      setReqLoading(true);
      const bearerToken = localStorage.getItem('token');
      axiosInstance
        .put(
          `${endpoints.students.batchPauseRequest}?erp_id=${
            content?.student_id
          }&batch_id=${content?.aol_batch}&pause_date=${format(
            selectedDate,
            'yyyy-MM-dd'
          )}`
        )
        .then((res) => {
          if (res.data.status_code === 200) {
            setLoading(false);
            setReqLoading(false);
            setReqMessage('');
            setModalName('');
            setAlert('success', res.data.message);
            close();
            history.push('/aol-student-batch-pause-history');
          } else {
            setAlert('error', res.data.message);
          }
        })
        .catch(() => {
          setLoading(false);
          setReqLoading(false);
          setAlert('error', 'Batch Pause Request Not Done');
        });
    }
  }
  return content.aol_batch ? (
    content.aol_batch !== null ? (
      <>
        <Grid container spacing={3}>
          <Grid item md={12} xs={12}>
            <Typography
              variant='h6'
              style={{ fontSize: 20, fontWeight: '600', color: '#014B82', marginTop: 20 }}
            >
              Batch Change
            </Typography>
            <Divider />
          </Grid>
          <Grid item md={6}>
            <FormLabel className={classes.label} component='legend' required>
              {' '}
              Batch Pause Date
            </FormLabel>
          </Grid>
          <Grid item md={6}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                format='MMMM d, yyyy'
                maxDate={new Date()}
                // minDate={content.paid_date}
                // disablePast
                minDate={content.reference_start_date}
                onChange={handleDateChange}
                value={selectedDate}
              >
                <i class='far fa-calendar'></i>
              </KeyboardDatePicker>
            </MuiPickersUtilsProvider>
          </Grid>
        </Grid>

        <Grid container spacing={3}>
          <Grid item md={6}>
            <Grid item md={6} xs={6}>
              <FormLabel className={classes.label} component='legend' required>
                Batch Name
              </FormLabel>
            </Grid>
            <Grid item md={6}>
              <Input row name='position' value={content.batch_name}></Input>
            </Grid>
          </Grid>
          <Grid item md={6}>
            <FormLabel className={classes.label} component='legend' required>
              ERP ID
            </FormLabel>
            <Input row name='position' value={content.erp_id}></Input>
          </Grid>

          <Grid item md={12} className={classes.button}>
            <Button
              key='submit'
              variant='contained'
              color='primary'
              size='small'
              loading={reqLoading}
              onClick={submitRequest}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
        {loading && <Loader />}
      </>
    ) : (
      <Grid item md={12} xs={12}>
        <Typography
          variant='h6'
          style={{ fontSize: 20, fontWeight: '600', color: '#014B82', margin: 50 }}
        >
          Batch Not Started Yet
        </Typography>
        <Divider />
      </Grid>
    )
  ) : (
    <Grid item md={12} xs={12}>
      <Typography
        variant='h6'
        style={{ fontSize: 20, fontWeight: '600', color: '#014B82', margin: 50 }}
      >
        Batch Not Started Yet
      </Typography>
      <Divider />
    </Grid>
  );
};

export default BatchPauseModel;
