import React, { useState, useContext, useEffect, useRef } from 'react';
import {
    Accordion,
    AccordionSummary,
    Table,
    TableHead,
    TableRow,
    TableCell,
    Grid,
    TableBody,
    Card,
    Divider,
    Button,
    TextField,
    IconButton,
    Typography,
    AccordionDetails,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import Layout from '../Layout';
import debounce from 'lodash.debounce';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Multiselect from 'multiselect-react-dropdown';
import axios from 'axios';
import Filteration from './filteration';
import Loader from '../../components/loader/loader';
import {
    DateTimeConverter,
    addCommas,
    ConverTime,
} from '../../components/dateTimeConverter';
import EditIcon from '@material-ui/icons/EditOutlined';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Autocomplete } from '@material-ui/lab';
import StudentLeaveModel from './menu-student-desk-model';
import MenuStudentDeskModel from '../aol-student-desk/menu-student-desk-model';
import FilterIcon from 'components/icon/FilterIcon';
import { values } from 'lodash';
// import CustomSearchBar from '../../components/custom-seearch-bar';
import CustomSearchBar from 'components/custom-seearch-bar';
import { batch } from 'react-redux';

import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Switch from '@mui/material/Switch';
const label = { inputProps: { 'aria-label': 'Switch demo' } };

const ITEM_HEIGHT = 48;
const StudentDesk = () => {
    const history = useHistory();
    const [loading, setLoading] = useState(true);
    const { setAlert } = useContext(AlertNotificationContext);
    const [studentBatchList, setStudentBatchList] = useState([]);
    const [page, setPage] = useState(1);
    const [index, setIndex] = useState(1);
    const [branch, setBranch] = useState([]);
    const [branchId, setBranchId] = useState('');
    const [isActiveValue, setIsActiveValue] = useState([]);
    const [isActiveOptionParams, setIsActiveOptionParams] = useState('')
    const [isTrailValue, setIsTrailValue] = useState([])
    const [isTrailParams, setIsTrailParams] = useState('')
    const [accordianOpen, setAccordianOpen] = useState(false);
    const [selectDropDown, setSelectDropDown] = useState([]);
    const [grade, setGrade] = useState([])
    const [selectedGrade, setSelectedGrade] = useState([]);
    const [selectedGradeId, setSelectedGradeId] = useState('');
    const [selectedSubject, setSelectedSubject] = useState([]);
    const [selectedSubjectId, setSelectedSubjectId] = useState('')
    const [teacher, setTeacher] = useState([]);
    const [selectedTeacher, setSelectedTeacher] = useState([]);
    const [teacherID, setTeacherID] = useState('')
    const [subject, setSubject] = useState([]);
    const [batchList, setBatchList] = useState([]);
    const [selectedBatchList, setSelectedBatchList] = useState([]);
    const [batchId, setBatchId] = useState('');
    const [searchValue, setSearchValue] = useState('');
    const [selectedSearchValue, setSelectedSearchValue] = useState('');
    const [studentList, setStudentList] = useState([])
    const [filterState, setFilterState] = useState({
        // gradeList: [],
        // selectedGrade: [],
        // subjectList: [],
        // selectedSubject: [],
        // startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
        // endDate: moment()?.format()?.split('T')?.[0],
        // startDate: '',
        // endDate: '',
        // fullDate: [],
        search: '',
    });
    const [open, setOpen] = useState(false);
    const [selectedItem, setSelectedItem] = useState('');
    const [rowsPerPage, setRowsPerPage] = useState(25);
    const [snoFilter, setsnoFilter] = useState(true);
    const [studentnameFilter, setStudentFilter] = useState(true);
    const [startdateFilter, setStartdateFilter] = useState(true);
    const [endDateFilter, setEndDateFilter] = useState(true);
    const [erpIdFilter, setErpIdFilter] = useState(true);
    const [usernameFilter, setUsernameFilter] = useState(true);
    const [gradeFilter, setGradeFilter] = useState(true);
    const [subjectFilter, setSubjectFilter] = useState(true);
    const [courseFilter, setCourseFilter] = useState(true);
    const [EnrollFilter, setEnrollFilter] = useState(true);
    const [batchNameFilter, setBatchNameFilter] = useState(true);
    const [teacherFilter, setTeacherFilter] = useState(true);
    const [teacherErpFilter, setTeacherErpFilter] = useState(true);
    const [aolBatchFilter, setAolBatchFilter] = useState(true);
    const [enrollidFilter, setEnrollIdFilter] = useState(true);
     const [batchpauseFilter, setBatchPauseFilter] = useState(true);
    const [batchSizeFilter, setBatchSizeFilter] = useState(true);
    const [batchDaysFilter, setBatchDaysFilter] = useState(true);
    const [remaininSeatsFilter, setRemainingFilter] = useState(true);
   

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [count, setCount] = useState(null);
    // const [page,setPage]=useState(null);
    const openh = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    // console.log('pepsiiiiiiii',studentBatchList)

    const isActiveDropDown = [
        { id: '1', option_value: 'True', params: 'true' },
        { id: '2', option_value: 'False', params: 'false' },
    ]

    const isTrailDropDown = [
        { id: '1', option_value: 'True', params: 'true' },
        { id: '2', option_value: 'False', params: 'false' },
    ]


    async function ApiCall(url, type) {
        // console.log({ url, type }, 'mobileee88888888')
        if (branchId) {

            setLoading(true);
            try {
                const { data } = await axiosInstance.get(url);
                if (data?.status_code === 200 || data?.status_code === 201) {
                    if (type === 'grade') {
                        setSelectedGrade(data?.data)
                        // setSelectedGradeId(data?.grade_id)
                        // setFilterState((prev) => ({ ...prev, gradeList: data?.data }));
                    } else if (type === 'subject') {
                        setSelectedSubject(data?.result)
                    } else if (type === 'teacher') {
                        setSelectedTeacher(data?.data)

                    } else if (type === 'batchList') {
                        setSelectedBatchList(data?.results)

                    }
                    setLoading(false);
                } else {
                    setLoading(false);
                    setAlert('error', data.message);
                }
            } catch (error) {
                setAlert('error', error.message);
                setLoading(false);
            }
        }
    }


    useEffect(() => {
        if (branchId) {
            ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}`, 'grade');
            ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}`, 'subject');
            ApiCall(`${endpoints.aolBatchAssign.getTeacherList}`, 'teacher');
            ApiCall(`${endpoints.studentDesk.batchList}?is_all=true&branch=${branchId}`, 'batchList')
        }
    }, [branchId]);

    //Sample Student desk data

    //   async function getStudentsList(
    //     pageNo,
    //     searchVal,
    //     branch,
    //     grade,
    //     sub,
    //     teacher,
    //     isTrailData,
    //     batchIdValue
    //   ) {
    //     const searchValue = searchVal ? `&term=${searchVal}` : '';
    //     const selectedGrade = selectedGradeId ? `&grades=${selectedGradeId}` : grade ? `&grades=${grade}` : '';
    //     const selectedSubject = selectedSubjectId ? `&subjects=${selectedSubjectId}` : sub ? `&subjects=${sub}` : '';
    //     const selectedTeacher = teacherID ? `&teacher_id=${teacherID}` : teacher ? `&teacher_id=${teacher}` : '';
    //     const bbranch = branchId ? `&branch=${branchId}` : branch ? `&branch=${branch}` : '';
    //     const selectedBatchValue = batchId ? `&aol_batch_id=${batchId}` : batchIdValue ? `&aol_batch_id=${batchIdValue}` : '';
    //     const isTrailParamsAPi = isTrailParams ? `&is_trail=${isTrailParams}` : isTrailData ? `&is_trail=${isTrailData}` : '';

    //     setLoading(true);
    //     try {
    //       const { data } = await axiosInstance.get(
    //         `${endpoints.studentDesk.getAllStudentdata}`
    //         // `${endpoints.aolBatchAssign.getStudentList}?page=${pageNo}&page_size=${rowsPerPage}${searchValue}${selectedGrade}${selectedSubject}${bbranch}${isTrailParamsAPi}${selectedTeacher}${selectedBatchValue}`
    //       );
    //       if (data?.message === 'Success') {
    //         setLoading(false);
    //         setStudentBatchList(data?.result);
    //       } else {
    //         setLoading(false);
    //         setAlert('error', data?.message);
    //       }
    //     } catch (error) {
    //       setLoading(false);
    //       setAlert('error', error?.message);
    //     }
    //   }
    // useEffect(() => {
    //     getStudentsList();
    // }, []);
    async function getStudentsList() {
        // axiosInstance
        //   .get(`${endpoints.academicYearCRUDL.academicYearCRUDL}`)
        try {
            const { data } = await axiosInstance.get(
                `${endpoints.studentDesk.getAllStudentdata}?page=${page}&page_size=${rowsPerPage}${searchValue}${selectedGrade}${selectedSubject}`
                // `${endpoints.aolBatchAssign.getStudentList}?page=${pageNo}&page_size=${rowsPerPage}${searchValue}${selectedGrade}${selectedSubject}${bbranch}${isTrailParamsAPi}${selectedTeacher}${selectedBatchValue}`
            );
            if (data?.message === 'Success') {
                setLoading(false);
                console.log(data?.result)
                setCount(data?.result?.total_pages);
                // setStudentList(data?.result);
                setStudentBatchList(data?.result?.result);
            } else {
                setLoading(false);
                setAlert('error', data?.message);
            }
        } catch (error) {
            setLoading(false);
            setAlert('error', error?.message);
        }
    };


    function handlePagination(event, page) {
        event.preventDefault();
        setPage(page);
        if (page === 1) {
            setIndex(1);
        } else {
            setIndex(1 + (page - 1) * rowsPerPage);
        }
        getStudentsList(
            page,
            filterState?.search || '',
            // filterState?.selectedBranch?.id || '',
            // filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
            // filterState?.subjectList?.id || '',
            // filterState?.startDate || '',
            // filterState?.endDate || ''
        );
    }

    useEffect(() => {
        setPage(1);
        // if (branchId && searchValue) {
        getStudentsList(
            1,
            filterState?.search || '',
            // branchId,
            // filterState?.selectedBranch?.id || '',
            // filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
            // filterState?.subjectList?.id || '',
            // filterState?.startDate || '',
            // filterState?.endDate || ''
        );
        // }
    }, [isTrailValue, selectedGradeId, selectedSubjectId, teacherID, batchId]);

    useEffect(() => {
        if (!branchId) {

            axiosInstance.get(`${endpoints.studentDesk.branchSelect}`).then((res) => {
                setSelectDropDown(res.data.data);
            });
            // setStudentBatchList('')
        }
    }, [branchId]);

    const delayedQuery = useRef(
        debounce((q, state, branch, grade, subject, teacher, isTrailData, batchId) => {
            setPage(1);
            getStudentsList(
                1,
                q,
                branch,
                grade,
                subject,
                teacher,
                isTrailData,
                batchId
                // state?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                // state?.selectedSubject?.id || '',
                // state?.startDate || '',
                // state?.endDate || ''
            );
        }, 1000)
    ).current;

    const handleClearAll = () => {
        setBranchId('')
        setBranch([])
        setGrade([])
        setSubject([])
        setTeacher([])
        setIsTrailValue([])
        setTeacherID('')
        setSelectedSubjectId('')
        setSelectedGradeId('')
        setIsTrailParams('')
        setBatchId('')
        setBatchList([])
        setFilterState({ ...filterState, search: '' });
    };
    function createData(name, calories, fat, carbs, protein, price) {
        return {
            name,
            calories,
            fat,
            carbs,
            protein,
            price,
            history: [
                {
                    date: '2020-01-05',
                    customerId: '11091700',
                    amount: 3,
                },
                {
                    date: '2020-01-02',
                    customerId: 'Anonymous',
                    amount: 1,
                },
            ],
        };
    }

    function Row(props) {
        const { row } = props;
        const [open, setOpen] = React.useState(false);

        return (
            //   <div> {studentBatchList?.map((row)=>(
            <React.Fragment>
                <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>

                    {/* <TableCell>
              
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell> */}
                    {/* <TableCell component="th" scope="row">
                        {row.id}
                    </TableCell> */}{snoFilter ?
                        <TableCell align="right">{row.sno}</TableCell>
                        : ''}
                    {studentnameFilter ? <TableCell align="right">{row.sname}{row.lname}</TableCell> : ''}
                    {erpIdFilter ? <TableCell align="right">{row.erp_id}</TableCell> : ''}
                    {erpIdFilter ? <TableCell align="right">{row.email}</TableCell> : ''}
                    {EnrollFilter ? <TableCell align="right">
                        <IconButton
                            aria-label="expand row"
                            size="small"
                            onClick={() => setOpen(!open)}
                        >
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    </TableCell> : ''}

                    <TableCell align="left">
                        <Button >Action</Button>
                    </TableCell>
                    <TableCell align="left">{''}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                                <Typography variant="h6" gutterBottom component="div">
                                    EnrollDetails
                                </Typography>
                                <Table size="small" aria-label="purchases">
                                    <TableHead>
                                        <TableRow>
                                            {enrollidFilter?<TableCell>EnrollId</TableCell>:''}
                                           {batchNameFilter?<TableCell>BatchName</TableCell>:''} 
                                           {teacherFilter?<TableCell align="right">TeacherName</TableCell>:''} 
                                           {teacherErpFilter? <TableCell align="right">TeacherErpId</TableCell>:''}
                                            {/* <TableCell align="right">CourseName</TableCell> */}
                                            {startdateFilter ? <TableCell align="right">StartDate</TableCell> : ''}
                                            {endDateFilter ? <TableCell align="right">EndDate</TableCell> : ''}

                                            {gradeFilter ? <TableCell align="right">Grade&nbsp;(g)</TableCell> : ''}
                                            {subjectFilter ? <TableCell align="right">Subject&nbsp;(g)</TableCell> : ''}
                                            {courseFilter ? <TableCell align="right">Course</TableCell> : ''}

                                           {batchpauseFilter?<TableCell align="right">BatchPauseDate</TableCell>:''} 
                                           {batchSizeFilter?<TableCell align="right">BatchSize</TableCell>:''} 
                                          {batchDaysFilter?<TableCell align="right">BatchDays</TableCell>:''}  
                                          {remaininSeatsFilter? <TableCell align="right">ReminingSeats</TableCell>:''} 
                                        </TableRow>
                                    </TableHead>
                                    {/* {console.log(row)} */}
                                    <TableBody>
                                        {row.course.map((e) => (
                                            <TableRow key={e?.enrollment_id}>
                                               {enrollidFilter?<TableCell component="th" scope="row">
                                                    {e.enrollment_id}
                                                </TableCell>:''} 
                                                {aolBatchFilter?<TableCell align="right">{e?.aol_batch_name}</TableCell>:""}
                                              {teacherFilter?<TableCell align="right">{e?.teacher_name}</TableCell>:""}  
                                              {teacherErpFilter?<TableCell align="right">{e?.teacher_erp_id}</TableCell>:""}  
                                                {/* <TableCell align="right">{row?.course_name}</TableCell> */}
                                                {startdateFilter ? <TableCell align="right">{row?.start_date ? DateTimeConverter(e?.start_date) : ''}</TableCell> : ''}
                                                {endDateFilter ? <TableCell align="right">{row?.end_date ? DateTimeConverter(e?.end_date) : ''}</TableCell> : ''}

                                                {gradeFilter ? <TableCell align="right">{e.grade}</TableCell> : ''}
                                                {subjectFilter ? <TableCell align="right">{e.subject}</TableCell> : ''}
                                                {courseFilter ? <TableCell align="right">{e.course_name}</TableCell> : ''}

                                               {batchpauseFilter?<TableCell align="right">{e?.batch_pause_date}</TableCell>:""} 
                                               {batchSizeFilter? <TableCell align="right">{e?.batch_size}</TableCell>:""}
                                               {batchDaysFilter?<TableCell align="right">{e?.batch_days}</TableCell>:""} 
                                               {remaininSeatsFilter? <TableCell align="right">{e?.seat_left}</TableCell>:""}
                                                {/* <TableCell align="right">
                                                    {Math.round(historyRow.amount * row.price * 100) / 100}
                                                </TableCell> */}
                                            </TableRow>
                                        ))}
                                    </TableBody>

                                </Table>
                            </Box>
                        </Collapse>
                    </TableCell>
                </TableRow>
            </React.Fragment>
            //   ))}</div> 

        );
    }
    let rows = [];
    let estart_date, eend_date, ebatch_name, batchSize, erp_id, eid, first_name, course, course_name;
    let enrollDetails = [];
    console.log(studentBatchList);
    let student = studentBatchList?.map((n, i) => {
        let id = n.id;
        let sno = (i + index)
        course = n.enrolled_courses;
        // course = n.course_details?.enrollment_details
        // batchSize = n.course_details?.batch_size
        // course_name = n.course_details?.course_name
        // let enrollid = course.map((c) => {
        //     eid = c.enrollment_id;
        //     estart_date = c.start_date;
        //     eend_date = c.end_date;


        // })
        //    let studentData= n.studetnt_info?.map((s)=>{
        //         sname=s.last_name;
        //         erp_id=s.erp_id;
        //         first_name=s.first_name;
        //      email=s.email;
        //     })
        rows.push({
            'id': id,
            'sno': sno,
            'sname': n.studetnt_info?.first_name,
            'lname': n.studetnt_info?.last_name,
            'erp_id': n.studetnt_info?.erp_id,
            'username': n.studetnt_info?.user_name,
            'email': n.studetnt_info?.email,
            'course': course,
            // 'course': n.course_details?.grade_id,
            // 'grade': n.course_details?.grade_name,
            // 'subject': n.course_details?.subject_name,
            // 'enrollIds': eid,
            // 'enrollDetails': course,
            // 'start_date': estart_date,
            // 'end_date': eend_date,
            // 'batch_name': ebatch_name,
            // 'batchSize': batchSize,
            // 'course_name': course_name,
        })
    })
    //   const rows = [
    //     // createData('Frozen yoghurt', 159, 6.0, 24, 4.0, 3.99),
    //     // createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 4.99),
    //     // createData('Eclair', 262, 16.0, 24, 6.0, 3.79),
    //     // createData('Cupcake', 305, 3.7, 67, 4.3, 2.5),
    //     // createData('Gingerbread', 356, 16.0, 49, 3.9, 1.5),
    //   ];
    console.log('data', studentBatchList);
    console.log('Rows', rows);
    return (
        <Layout>
            <div style={{ width: '95%', margin: '20px auto' }}>
                <CommonBreadcrumbs
                    componentName='Master Management'
                    childComponentName='Student Desk'
                />
            </div>

            <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
                <Grid item md={12} xs={12}>
                    <Grid container spacing={2}>
                        <Grid item md={3} xs={12}>
                            <Autocomplete
                                size='small'
                                className='dropdownIcon'
                                options={selectDropDown || []}
                                getOptionLabel={(option) => option?.branch_name || ''}
                                filterSelectedOptions
                                value={branch || ''}
                                onChange={(event, value) => {
                                    setBranchId('')
                                    setBranch([])
                                    setGrade([])
                                    setSubject([])
                                    setTeacher([])
                                    setIsTrailValue([])
                                    setTeacherID('')
                                    setSelectedSubjectId('')
                                    setSelectedGradeId('')
                                    setIsTrailParams('')
                                    setBatchId('')
                                    setBatchList([])
                                    // setStudentBatchList('')
                                    if (value) {
                                        setBranchId(value?.id);
                                        setBranch(value);
                                    }
                                }}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label='Branch'
                                        placeholder='Select Branch'
                                    />
                                )}
                            />
                        </Grid>
                        {branchId ? (
                            <>
                                <Grid md={12} xs={12}>
                                    <Accordion expanded={accordianOpen}>
                                        <AccordionSummary
                                            expandIcon={<ExpandMoreIcon />}
                                            aria-controls='panel1a-content'
                                            id='panel1a-header'
                                            onClick={() => setAccordianOpen(!accordianOpen)}
                                        >
                                            <FilterIcon />
                                            <Typography variant='h6' color='primary'>
                                                Filter
                                            </Typography>
                                        </AccordionSummary>
                                        <AccordionDetails>
                                            <Grid container spacing={3} alignItems='center'>
                                                <Grid item md={3} xs={12}>
                                                    <Autocomplete
                                                        size='small'
                                                        className='dropdownIcon'
                                                        options={selectedGrade || []}
                                                        getOptionLabel={(option) => option?.grade__grade_name}
                                                        value={grade || ''}
                                                        filterSelectedOptions
                                                        onChange={(event, value) => {
                                                            setGrade(value)
                                                            setSelectedGradeId(value?.grade_id)
                                                        }}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                size='small'
                                                                variant='outlined'
                                                                label='Grade'
                                                                placeholder='Select Grade'
                                                            />
                                                        )}
                                                    />
                                                </Grid>
                                                <Grid item md={3} xs={12}>
                                                    <Autocomplete
                                                        size='small'
                                                        className='dropdownIcon'
                                                        options={selectedSubject || []}
                                                        getOptionLabel={(option) => option?.subject__subject_name || ''}
                                                        filterSelectedOptions
                                                        value={subject || ''}
                                                        onChange={(event, value) => {
                                                            setSubject(value)
                                                            setSelectedSubjectId(value?.id)
                                                        }}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                size='small'
                                                                variant='outlined'
                                                                label='Subject'
                                                                placeholder='Select Subject'
                                                            />
                                                        )}
                                                    />
                                                </Grid>
                                                <Grid item md={3} xs={12}>
                                                    <Autocomplete
                                                        size='small'
                                                        className='dropdownIcon'
                                                        options={selectedTeacher || []}
                                                        getOptionLabel={(option) => option?.name || ''}
                                                        filterSelectedOptions
                                                        value={teacher || ''}
                                                        onChange={(event, value) => {
                                                            setTeacher(value);
                                                            setTeacherID(value?.tutor_id);
                                                        }}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                size='small'
                                                                variant='outlined'
                                                                label='Teacher'
                                                                placeholder='Select Teacher'
                                                            />
                                                        )}
                                                    />
                                                </Grid>
                                                <Grid item md={3} xs={12}>
                                                    <Autocomplete
                                                        size="small"
                                                        className='dropdownIcon'
                                                        options={isTrailDropDown || []}
                                                        getOptionLabel={(option) => option?.option_value || ''}
                                                        filterSelectedOptions
                                                        value={isTrailValue || ''}
                                                        onChange={(event, value) => {
                                                            setIsTrailParams(value?.params)
                                                            setIsTrailValue(value)
                                                        }}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                size="small"
                                                                variant="outlined"
                                                                label="Is Trail"
                                                                placeholder='Is Trail'
                                                            />
                                                        )}
                                                    />
                                                </Grid>
                                                <Grid item md={3} xs={12}>
                                                    <Autocomplete
                                                        size="small"
                                                        className='dropdownIcon'
                                                        options={selectedBatchList || []}
                                                        getOptionLabel={(option) => option?.course || ''}
                                                        filterSelectedOptions
                                                        value={batchList || ''}
                                                        onChange={(event, value) => {
                                                            setBatchList(value)
                                                            setBatchId(value?.aol_batch_id)
                                                        }}
                                                        renderInput={(params) => (
                                                            <TextField
                                                                {...params}
                                                                size="small"
                                                                variant="outlined"
                                                                label="Batch Name"
                                                                placeholder='Select Batch Name'
                                                            />
                                                        )}
                                                    />
                                                </Grid>
                                                <Grid item md={6} xs={12}>
                                                    <CustomSearchBar
                                                        value={filterState?.search}
                                                        setValue={(value) => {
                                                            setFilterState({ ...filterState, search: value });
                                                            delayedQuery(value, filterState, branchId, selectedGradeId, selectedSubjectId, teacherID, isTrailParams, batchId);

                                                        }}
                                                        onChange={(e) => {
                                                            setFilterState({ ...filterState, search: e.target.value.trimLeft() });
                                                            delayedQuery(e.target.value.trimLeft(), filterState, branchId, selectedGradeId, selectedSubjectId, teacherID, isTrailParams, batchId);

                                                        }}
                                                        label=''
                                                        placeholder='Student Search'
                                                    />
                                                </Grid>
                                                <Grid item md={3} xs={12}>
                                                    <Button
                                                        variant='contained'
                                                        color='primary'
                                                        className='custom_button_master labelColor'
                                                        size='medium'
                                                        onClick={handleClearAll}
                                                    >
                                                        Clear All
                                                    </Button>
                                                </Grid>

                                            </Grid>
                                        </AccordionDetails>
                                    </Accordion>
                                </Grid>

                            </>
                        ) : ''}

                    </Grid>
                </Grid>
                {/* {branchId ? (
          <Grid item md={12} xs={12}>
            <Filteration
              setPage={setPage}
              getStudentsList={getStudentsList}
              filterState={filterState}
              setFilterState={setFilterState}
              setLoading={setLoading}
            />
          </Grid>

        ) : ''} */}
            </Grid>
            <div>
                <Divider />
            </div>


            <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
                <Grid item md={12} xs={12}>
                    <Card style={{ padding: '10px', borderRadius: '10px' }}>
                        <Grid container spacing={2}>
                            <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>

                                <Table style={{ width: '100%', overflow: 'auto' }}>
                                    <TableContainer component={Paper}>
                                        <Table aria-label="collapsible table">
                                            <TableHead>
                                                <TableRow>

                                                    {/* <TableCell /> */}
                                                    {snoFilter ?
                                                        <TableCell>S No</TableCell>
                                                        : ''}
                                                    {studentnameFilter ? <TableCell align="right">StudentName</TableCell> : ''}

                                                    {erpIdFilter ? <TableCell align="right">ErpId</TableCell> : ''}
                                                    {erpIdFilter ? <TableCell align="right">Email</TableCell> : ''}
                                                    {EnrollFilter ? <TableCell align="right"> </TableCell> : ''}
                                                   



                                                    <TableCell align="right">Action</TableCell>
                                                    <TableCell align="right">
                                                        <div style={{ float: 'right' }}>
                                                            <IconButton
                                                                aria-label="more"
                                                                id="long-button"
                                                                aria-controls={open ? 'long-menu' : undefined}
                                                                aria-expanded={open ? 'true' : undefined}
                                                                aria-haspopup="true"
                                                                onClick={handleClick}
                                                            >
                                                                <MoreVertIcon />
                                                            </IconButton>
                                                            <Menu
                                                                id="long-menu"
                                                                MenuListProps={{
                                                                    'aria-labelledby': 'long-button',
                                                                }}
                                                                anchorEl={anchorEl}
                                                                open={openh}
                                                                onClose={handleClose}
                                                                PaperProps={{
                                                                    style: {
                                                                        maxHeight: 'fit-content',
                                                                        width: '50ch',
                                                                    },
                                                                }}
                                                            >
                                                                <MenuItem > <Switch {...label} checked={snoFilter} color="secondary" onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setsnoFilter(true);
                                                                        setStudentFilter(true);
                                                                        setStartdateFilter(true);
                                                                        setEndDateFilter(true);
                                                                        setUsernameFilter(true);
                                                                        setGradeFilter(true);
                                                                        setSubjectFilter(true);
                                                                        setEnrollFilter(true);
                                                                        setBatchNameFilter(true);
                                                                        setTeacherErpFilter(true);
                                                                        setTeacherFilter(true);
                                                                        setBatchDaysFilter(true);
                                                                        setBatchNameFilter(true);
                                                                        setBatchSizeFilter(true);
                                                                        setRemainingFilter(true);
                                                                        setCourseFilter(true);
                                                                        setBatchPauseFilter(true);

                                                                    }
                                                                    else {
                                                                        setsnoFilter(false);
                                                                        setStudentFilter(false);
                                                                        setStartdateFilter(false);
                                                                        setEndDateFilter(false);
                                                                        setUsernameFilter(false);
                                                                        setGradeFilter(false);
                                                                        setSubjectFilter(false);
                                                                        setEnrollFilter(false);
                                                                        setBatchNameFilter(false);
                                                                        setTeacherErpFilter(false);
                                                                        setTeacherFilter(false);
                                                                        setBatchDaysFilter(false);
                                                                        setBatchNameFilter(false);
                                                                        setBatchSizeFilter(false);
                                                                        setRemainingFilter(false);
                                                                        setCourseFilter(false);
                                                                        setBatchPauseFilter(false);
                                                                    }
                                                                }} />Select All</MenuItem>
                                                                <div>
                                                                    <Divider />
                                                                </div>
                                                                <MenuItem > <Switch {...label} checked={snoFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setsnoFilter(true)
                                                                    }
                                                                    else { setsnoFilter(false); }
                                                                }} />S.No</MenuItem>

                                                                <MenuItem > <Switch {...label} checked={studentnameFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setStudentFilter(true)
                                                                    }
                                                                    else { setStudentFilter(false); }
                                                                }} />Studentname</MenuItem>
                                                                  <MenuItem > <Switch {...label} checked={studentnameFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setErpIdFilter(true)
                                                                    }
                                                                    else {  setErpIdFilter(false); }
                                                                }} />ERP Id</MenuItem>
                                                                <MenuItem > <Switch {...label} checked={startdateFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setStartdateFilter(true)
                                                                    }
                                                                    else { setStartdateFilter(false); }
                                                                }} />StartDate</MenuItem>
                                                                <MenuItem > <Switch {...label} checked={endDateFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setEndDateFilter(true)
                                                                    }
                                                                    else { setEndDateFilter(false); }
                                                                }} />EndDate</MenuItem>
                                                               
                                                                <MenuItem > <Switch {...label} checked={gradeFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setGradeFilter(true)
                                                                    }
                                                                    else { setGradeFilter(false); }
                                                                }} />Grade</MenuItem>
                                                                <MenuItem > <Switch {...label} checked={subjectFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setSubjectFilter(true)
                                                                    }
                                                                    else { setSubjectFilter(false); }
                                                                }} />Subject</MenuItem>
                                                                <MenuItem > <Switch {...label} checked={EnrollFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setEnrollFilter(true)
                                                                    }
                                                                    else { setEnrollFilter(false); }
                                                                }} />EnrollDetails</MenuItem>
                                                               
                                                                <MenuItem > <Switch {...label} checked={courseFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setCourseFilter(true)
                                                                    }
                                                                    else { setCourseFilter(false); }
                                                                }} />CourseName</MenuItem>
                                                                
                                                                 <MenuItem > <Switch {...label} checked={batchNameFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setBatchNameFilter(true)
                                                                    }
                                                                    else { setBatchNameFilter(false); }
                                                                }} />BatchName</MenuItem>
                                                                 <MenuItem > <Switch {...label} checked={teacherFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setTeacherFilter(true)
                                                                    }
                                                                    else { setTeacherFilter(false); }
                                                                }} />TeacherName</MenuItem>
                                                                 <MenuItem > <Switch {...label} checked={teacherErpFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                       setTeacherErpFilter(true)
                                                                    }
                                                                    else { setTeacherErpFilter(false); }
                                                                }} />Teacher Erp</MenuItem>
                                                                 <MenuItem > <Switch {...label} checked={batchpauseFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setBatchPauseFilter(true)
                                                                    }
                                                                    else { setBatchPauseFilter(false); }
                                                                }} />BatchPause</MenuItem>
                                                                 <MenuItem > <Switch {...label} checked={batchSizeFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setBatchSizeFilter(true)
                                                                    }
                                                                    else { setBatchSizeFilter(false); }
                                                                }} />BatchSize</MenuItem>
                                                                <MenuItem > <Switch {...label} checked={batchDaysFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setBatchDaysFilter(true)
                                                                    }
                                                                    else { setBatchDaysFilter(false); }
                                                                }} />BatchDays</MenuItem>
                                                                <MenuItem > <Switch {...label} checked={remaininSeatsFilter} onClick={(e) => {
                                                                    if (e.target.checked === true) {
                                                                        setRemainingFilter(true)
                                                                    }
                                                                    else { setRemainingFilter(false); }
                                                                }} />Remaining Seats</MenuItem>
                                                                {/* {options.map((option) => ( */}
                                                                {/* <MenuItem key={option} selected={option === 'Pyxis'} onClick={handleClose}>
            {option}
          </MenuItem> */}
                                                                {/* ))} */}
                                                            </Menu>
                                                        </div>
                                                    </TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {rows?.map((row) => (
                                                    <Row key={row.name} row={row} />
                                                ))}

                                                <TableRow>
                                                    <TableCell colSpan='12'>
                                                        <Pagination
                                                            style={{ textAlign: 'center', display: 'inline-flex' }}
                                                            onChange={handlePagination}
                                                            // count={studentBatchList?.total_pages}
                                                            count={count}
                                                            color='primary'
                                                            page={page}
                                                        />
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>

                                    </TableContainer>
                                    {/* <TableHead>
                                        <TableRow>
                                            <TableCell align='left'>S.No</TableCell>
                                            <TableCell align='left'>Student Name</TableCell>
                                            <TableCell style={{ width: '10%' }} align='left'>
                                                Start Date
                                            </TableCell>
                                            <TableCell style={{ width: '10%' }} align='left'>
                                                End Date
                                            </TableCell>
                                            <TableCell align='left'>Grade</TableCell>
                                            <TableCell align='left'>Subject</TableCell>
                                            <TableCell align='left'>Course Name</TableCell>
                                            <TableCell align='left'>Teacher Name</TableCell>
                                            <TableCell align="left" >Enrollment Number</TableCell>
                                            <TableCell align='left'>Batch Name</TableCell>
                                            {/* <TableCell align='left'>Batch Start Date & Time</TableCell>
                      <TableCell align='left'>Payment</TableCell> */}
                                    {/* <TableCell align='left'>Actions</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {branch?.branch_name !== undefined && studentBatchList?.result?.length ? (
                                            <>
                                                {studentBatchList?.result
                                                    .filter((item) => item.aol_batch !== null)
                                                    .map((item, i) => (
                                                        <TableRow key={item.id}>
                                                            <TableCell align='left'>{i + index}</TableCell>
                                                            <TableCell align='left'>
                                                                {item?.name || (
                                                                    <Typography style={{ color: 'red' }}>NA</Typography>
                                                                )}
                                                            </TableCell>
                                                            <TableCell align='left'>
                                                                {item?.start_date
                                                                    ? DateTimeConverter(item?.start_date)
                                                                    : ''}
                                                            </TableCell>
                                                            <TableCell align='left'>
                                                                {item?.end_date ? DateTimeConverter(item?.end_date) : ''}
                                                            </TableCell>
                                                            <TableCell align='left'>{item.grade_name}</TableCell>
                                                            <TableCell align='left'>
                                                                {item?.subject_name || (
                                                                    <Typography style={{ color: 'red' }}>NA</Typography>
                                                                )}
                                                            </TableCell>
                                                            <TableCell align='left'>
                                                                {item?.course_name || (
                                                                    <Typography style={{ color: 'red' }}>NA</Typography>
                                                                )}
                                                            </TableCell> */}
                                    {/* <TableCell align='left'>{item.grade_name}</TableCell> */}
                                    {/* <TableCell align='left'>
                              {item?.subject_name || (
                                <Typography style={{ color: 'red' }}>NA</Typography>
                              )}
                            </TableCell> */}
                                    {/* <TableCell align='left'>
                              {item?.course_name || (
                                <Typography style={{ color: 'red' }}>NA</Typography>
                              )}
                            </TableCell> */}
                                    {/* <TableCell align='left'>
                                                                {/* {console.log(item.teacher_name,'kh22')} */}
                                    {/* {item?.teacher_name || (
                                                                    <Typography style={{ color: 'red' }}>NA</Typography>
                                                                )}
                                                            </TableCell>
                                                            <TableCell align="left">
                                                                {item?.enrollment_no || (
                                                                    <Typography style={{ color: 'red' }}>NA</Typography>
                                                                )}
                                                            </TableCell>
                                                            <TableCell align='left'>
                                                                {item?.batch_name || (
                                                                    <Typography style={{ color: 'red' }}>NA</Typography>
                                                                )}
                                                            </TableCell> */}
                                    {/* <TableCell align='left'>
                              <span style={{ color: '#FF6B6B' }}>
                                {item?.reference_start_date
                                  ? DateTimeConverter(item?.reference_start_date)
                                  : ''}
                              </span>
                              <br />
                              <span style={{ color: '#014B7E' }}>
                                {item?.batch_time_slot
                                  ? `${ConverTime(
                                      item?.batch_time_slot?.split('-')[0]
                                    )} - ${ConverTime(
                                      item?.batch_time_slot?.split('-')[1]
                                    )}`
                                  : '' || ''}
                              </span>
                            </TableCell>
                            <TableCell align='left'>
                              {item?.paid_date ? (
                                <span style={{ color: 'green' }}>
                                  {`${addCommas(item?.amount_paid) || ''} ₹` || ''}
                                  <br />
                                  &nbsp;Paid on&nbsp;
                                  {DateTimeConverter(item?.paid_date)}
                                </span>
                              ) : (
                                <span style={{ color: 'red' }}>Fail</span>
                              )}
                            </TableCell> */}
                                    {/* <TableCell align='left'>
                                                                <Button
                                                                    size='small'
                                                                    color='primary'
                                                                    variant='contained'
                                                                    disabled={item?.aol_batch === null}
                                                                    onClick={() => {
                                                                        setOpen(true);
                                                                        setSelectedItem(item);
                                                                    }}
                                                                    title='Course Extend'
                                                                >
                                                                    Update
                                                                    {/* <EditIcon disabled = {item?.aol_batch===null} /> */}
                                    {/* </Button>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))} */}
                                    {/* <TableRow>
                                                    <TableCell colSpan='12'>
                                                        <Pagination
                                                            style={{ textAlign: 'center', display: 'inline-flex' }}
                                                            onChange={handlePagination}
                                                            count={studentBatchList?.total_pages}
                                                            // count='100'
                                                            color='primary'
                                                            page={page}
                                                        />
                                                    </TableCell>
                                                </TableRow>
                                            </>
                                        ) : (
                                            <TableRow>
                                                <TableCell colSpan='9'>
                                                    <CustomFiterImage label='Batches Not Found' />
                                                </TableCell>
                                            </TableRow>
                                        )}
                                    </TableBody>  */}
                                </Table>
                            </Grid>
                        </Grid>
                    </Card>
                </Grid>
            </Grid>
            {loading && <Loader />}
            {branchId ? <MenuStudentDeskModel branchId={branchId} /> : ''}
            {open ? (
                <StudentLeaveModel
                    content={selectedItem}
                    branchId={branchId}
                    open={open}
                    close={() => {
                        setOpen(false);
                        setSelectedItem('');
                    }}
                />
            ) : (
                ''
            )}
        </Layout>
    );
};

export default StudentDesk;
