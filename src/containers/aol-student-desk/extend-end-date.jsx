import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Button, Divider, TextField, Radio, Select, RadioGroup, FormControl, FormLabel } from '@material-ui/core';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { Autocomplete } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { makeStyles } from '@material-ui/core/styles';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
 
  MuiFormControlLabel: {
    minWidth: 100,
  },
  label:{
    marginTop:10,
    marginBottom:10
  },
  button:{
    textAlign:'center',
    marginTop:90
  }

}));


const ExtendEndDate = ({ content, open, close }) => {
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const history = useHistory();
  const [erpId, setErpId] = useState('');
  const [loading, setLoading] = useState(false);
  //   const [open, setOpen] = useState(false);
  const [fileData, setFileData] = useState(content);
  const [modalName, setModalName] = useState('');
  const [selectedWeek, setSelectedWeek] = useState('')
  const [selectedTime, setSelectedTime] = useState('');
  const [teacherChange, setTeacherChange] = useState('no')
  const [reqMessage, setReqMessage] = useState('')
  const [weekList, setWeekList] = useState([])
  const [timeList, setTimeList] = useState([]);
  const [reqLoading, setReqLoading] = useState(false);
  const [fromDate, setFromDate] = useState(content?.end_date);

  const handleFromDate = (value) => {
    setFromDate(moment(value).format('YYYY-MM-DD[T]HH:mm:ss'))
  }


  useEffect(() => {

    console.log('content:',content);
    // fromDate = content?.end_date;
    setLoading(true)
    const bearerToken = localStorage.getItem('token');
    axiosInstance
      .get(`${endpoints.students.batchDaysListing}?is_batch_days=true&course_id=${content?.course_id}`,)
      .then((res) => {
        setLoading(false);
        if (res.data.length !== 0) {
          setWeekList(res.data)
        }
      }).catch(() => {
        setLoading(false);
      });

  }, [])


  function submitRequest() {
    if (!fromDate) {
      setAlert('error', 'Select Date')
    }
    else {
      setLoading(true);
      setReqLoading(true);
      var endDateList = fromDate.split('T');
      var endDate = endDateList[0];
      const bearerToken = localStorage.getItem('token');
      axiosInstance
        .post(`${endpoints.students.extendEndDate}?erp_id=${content?.student_id}&batch_id=${content?.aol_batch}&end_date=${endDate}`)
        .then((res) => {
            console.log('result:',res)
          if (res.data.status_code === 200) {
            setAlert('success', res.data.message);
            setLoading(false);
          } else {
            setAlert('error', res.data.message)
          }


        }).catch((error) => {
          setLoading(false);
          setReqLoading(false);
          setAlert('error','there are no sessions in between old end date and new end date')
        });
    }
  }
  return (
    <>
      <Grid container spacing={3} >
        <Grid item md={12} xs={12}>
          <Typography variant="h6" style={{ fontSize: 20, fontWeight: '600', color: '#014B82', marginTop: 20 }}>Extend End Date</Typography>
          <Divider />
        </Grid>
        <Grid item md={6}>
          <Typography variant="h6"><strong>Student Name:</strong>{content?.name}</Typography>
        </Grid>
        <Grid item md={6}>
        <Typography variant="h6"><strong>Batch Name:</strong>{content?.batch_name}</Typography>
        </Grid>
        <Grid item md={12}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
              margin="dense"
              label='New End Date'
              format="dd/MM/yyyy"
              inputVariant='outlined'
            //   size='small'
              value={fromDate}
              onChange={(value => handleFromDate(value))}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
            </MuiPickersUtilsProvider>
        </Grid>
        <Grid item md={6}>
          <Button
            key="submit"
            variant='contained'
            color='primary'
            size='small'
            disabled={fromDate === ''}
            loading={reqLoading}
            onClick={submitRequest}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </>
  );
};

export default ExtendEndDate;