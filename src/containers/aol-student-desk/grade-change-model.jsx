import React, { useState, useContext, useEffect } from 'react';
import { Grid, Typography, Button, Divider, TextField } from '@material-ui/core';
import CustomDialog from '../../components/custom-dialog';
import ConfirmDialog from '../../components/confirm-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { Autocomplete } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';

const GradePromoteModel = ({ content, open, close }) => {
  // console.log({content, open, close }, 'check22')
  const { setAlert } = useContext(AlertNotificationContext);
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [gradeData, setGradeData] = useState([]);
  const [selectedGrade, setSelectedGrade] = useState('');

  useEffect(() => {
    getGrades();
  }, []);

  const getGrades = async () => {
    setLoading(false);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.communication.grades}?branch_id=1`
      );
      if (data) {
        setLoading(false);
        setGradeData(data);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  };

  function PromoteGrade() {
    if (selectedGrade) {
      const updateGrade = { grade: selectedGrade.grade_id };
      setLoading(true);
      axiosInstance
        .patch(
          `${endpoints.aolBatchAssign.promoteStudentEnrollApi}${content.student_id}/`,
          updateGrade
        )
        .then((response) => {
          setLoading(false);
          if (response.status === 200) {
            setSelectedGrade('');
            setLoading(false);
            setAlert('success', 'Grade Successfully Changed');
            history.push('/aol-student-desk');
            history.go()
            return response;
          } else {
            setAlert('error', response.data.description);
          }
        })
        .catch((err) => {
          setLoading(false);
          setAlert('success', err.response.data.description);
        });
    } else {
      setAlert('warning', message);
      const message = 'Please Select new Grade to Promote';
    }
    // window.location.reload()
    // history.go()

   
  }
  return (
    <>
      <Typography
        variant='h6'
        style={{ fontSize: 20, fontWeight: 'bold', color: '#014B82', marginTop: 20 }}
      >
        Grade Change
      </Typography>
      <Divider />
      <Grid
        container
        spacing={3}
        style={{ padding: '10px' }}
        direction='column'
        alignItems='center'
        justify='center'
      >
        <Grid item md={12} xs={12}>
          <Autocomplete
            size='small'
            className='dropdownIcon'
            options={gradeData?.data || []}
            getOptionLabel={(option) => option?.grade__grade_name || ''}
            filterSelectedOptions
            style={{ width: 400 }}
            value={selectedGrade || ''}
            onChange={(event, value) => {
              setSelectedGrade(value);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                size='small'
                variant='outlined'
                label='Grade'
                placeholder='Select Grade'
              />
            )}
          />
        </Grid>
        <Grid item md={6} xs={6} style={{ margin: '10px 0px' }}>
          <Grid
            container
            spacing={2}
            direction='row'
            alignItems='center'
            justify='center'
          >
            <Button
              onClick={PromoteGrade}
              disabled={selectedGrade === ''}
              variant='contained'
              color='primary'
              size='small'
            >
              Apply
            </Button>
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </>
  );
};

export default GradePromoteModel;
