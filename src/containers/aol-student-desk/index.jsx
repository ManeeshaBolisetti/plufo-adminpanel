import React, { useState, useContext, useEffect, useRef } from 'react';
import {
  Accordion,
  AccordionSummary,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
  IconButton,
  Typography,
  AccordionDetails,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import Layout from '../Layout';
import debounce from 'lodash.debounce';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Filteration from './filteration';
import Loader from '../../components/loader/loader';
import {
  DateTimeConverter,
  addCommas,
  ConverTime,
} from '../../components/dateTimeConverter';
import EditIcon from '@material-ui/icons/EditOutlined';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Autocomplete } from '@material-ui/lab';
import StudentLeaveModel from './menu-student-desk-model';
import MenuStudentDeskModel from '../aol-student-desk/menu-student-desk-model';
import FilterIcon from 'components/icon/FilterIcon';
import { values } from 'lodash';
// import CustomSearchBar from '../../components/custom-seearch-bar';
import CustomSearchBar from 'components/custom-seearch-bar';
import { batch } from 'react-redux';

const StudentDesk = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [branch, setBranch] = useState([]);
  const [branchId, setBranchId] = useState('');
  const [isActiveValue, setIsActiveValue] = useState([]);
  const [isActiveOptionParams, setIsActiveOptionParams] = useState('')
  const [isTrailValue, setIsTrailValue] = useState([])
  const [isTrailParams, setIsTrailParams] = useState('')
  const [accordianOpen, setAccordianOpen] = useState(false);
  const [selectDropDown, setSelectDropDown] = useState([]);
  const [grade, setGrade] = useState([])
  const [selectedGrade, setSelectedGrade] = useState([]);
  const [selectedGradeId, setSelectedGradeId] = useState('');
  const [selectedSubject, setSelectedSubject] = useState([]);
  const [selectedSubjectId, setSelectedSubjectId] = useState('')
  const [teacher, setTeacher] = useState([]);
  const [selectedTeacher, setSelectedTeacher] = useState([]);
  const [teacherID, setTeacherID] = useState('')
  const [subject, setSubject] = useState([]);
  const [batchList, setBatchList] = useState([]);
  const [selectedBatchList, setSelectedBatchList] = useState([]);
  const [batchId, setBatchId] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const [selectedSearchValue, setSelectedSearchValue] = useState('');

  const [filterState, setFilterState] = useState({
    // gradeList: [],
    // selectedGrade: [],
    // subjectList: [],
    // selectedSubject: [],
    // startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
    // endDate: moment()?.format()?.split('T')?.[0],
    // startDate: '',
    // endDate: '',
    // fullDate: [],
    search: '',
  });
  const [open, setOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);

  // console.log('pepsiiiiiiii',studentBatchList)

  const isActiveDropDown = [
    { id: '1', option_value: 'True', params: 'true' },
    { id: '2', option_value: 'False', params: 'false' },
  ]

  const isTrailDropDown = [
    { id: '1', option_value: 'True', params: 'true' },
    { id: '2', option_value: 'False', params: 'false' },
  ]


  async function ApiCall(url, type) {
    // console.log({ url, type }, 'mobileee88888888')
    if (branchId) {

      setLoading(true);
      try {
        const { data } = await axiosInstance.get(url);
        if (data?.status_code === 200 || data?.status_code === 201) {
          if (type === 'grade') {
            setSelectedGrade(data?.data)
            // setSelectedGradeId(data?.grade_id)
            // setFilterState((prev) => ({ ...prev, gradeList: data?.data }));
          } else if (type === 'subject') {
            setSelectedSubject(data?.result)
          } else if (type === 'teacher') {
            setSelectedTeacher(data?.data)

          } else if (type === 'batchList') {
            setSelectedBatchList(data?.results)

          }
          setLoading(false);
        } else {
          setLoading(false);
          setAlert('error', data.message);
        }
      } catch (error) {
        setAlert('error', error.message);
        setLoading(false);
      }
    }
  }


  useEffect(() => {
    if (branchId) {
      ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}`, 'grade');
      ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}`, 'subject');
      ApiCall(`${endpoints.aolBatchAssign.getTeacherList}`, 'teacher');
      ApiCall(`${endpoints.studentDesk.batchList}?is_all=true&branch=${branchId}`, 'batchList')
    }
  }, [branchId]);


  async function getStudentsList(
    pageNo,
    searchVal,
    branch,
    grade,
    sub,
    teacher,
    isTrailData,
    batchIdValue
  ) {
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = selectedGradeId ? `&grades=${selectedGradeId}` : grade ? `&grades=${grade}` : '';
    const selectedSubject = selectedSubjectId ? `&subjects=${selectedSubjectId}` : sub ? `&subjects=${sub}` : '';
    const selectedTeacher = teacherID ? `&teacher_id=${teacherID}` : teacher ? `&teacher_id=${teacher}` : '';
    const bbranch = branchId ? `&branch=${branchId}` : branch ? `&branch=${branch}` : '';
    const selectedBatchValue = batchId ? `&aol_batch_id=${batchId}` : batchIdValue ? `&aol_batch_id=${batchIdValue}` : '';
    const isTrailParamsAPi = isTrailParams ? `&is_trail=${isTrailParams}` : isTrailData ? `&is_trail=${isTrailData}` : '';

    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.aolBatchAssign.getStudentList}?page=${pageNo}&page_size=${rowsPerPage}${searchValue}${selectedGrade}${selectedSubject}${bbranch}${isTrailParamsAPi}${selectedTeacher}${selectedBatchValue}`
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setStudentBatchList(data);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  function handlePagination(event, page) {
    event.preventDefault();
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * rowsPerPage);
    }
    getStudentsList(
      page,
      filterState?.search || '',
      // filterState?.selectedBranch?.id || '',
      // filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      // filterState?.subjectList?.id || '',
      // filterState?.startDate || '',
      // filterState?.endDate || ''
    );
  }

  useEffect(() => {
    setPage(1);
    // if (branchId && searchValue) {
    getStudentsList(
      1,
      filterState?.search || '',
      // branchId,
      // filterState?.selectedBranch?.id || '',
      // filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      // filterState?.subjectList?.id || '',
      // filterState?.startDate || '',
      // filterState?.endDate || ''
    );
    // }
  }, [isTrailValue, selectedGradeId, selectedSubjectId, teacherID, batchId]);

  useEffect(() => {
    if (!branchId) {

      axiosInstance.get(`${endpoints.studentDesk.branchSelect}`).then((res) => {
        setSelectDropDown(res.data.data);
      });
      setStudentBatchList('')
    }
  }, [branchId]);

  const delayedQuery = useRef(
    debounce((q, state, branch, grade, subject, teacher, isTrailData, batchId) => {
      setPage(1);
      getStudentsList(
        1,
        q,
        branch,
        grade,
        subject,
        teacher,
        isTrailData,
        batchId
        // state?.selectedGrade?.map((grade) => grade?.grade_id) || [],
        // state?.selectedSubject?.id || '',
        // state?.startDate || '',
        // state?.endDate || ''
      );
    }, 1000)
  ).current;

  const handleClearAll = () => {
    setBranchId('')
    setBranch([])
    setGrade([])
    setSubject([])
    setTeacher([])
    setIsTrailValue([])
    setTeacherID('')
    setSelectedSubjectId('')
    setSelectedGradeId('')
    setIsTrailParams('')
    setBatchId('')
    setBatchList([])
    setFilterState({ ...filterState, search: '' });
  };

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Student Desk'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={3} xs={12}>
              <Autocomplete
                size='small'
                className='dropdownIcon'
                options={selectDropDown || []}
                getOptionLabel={(option) => option?.branch_name || ''}
                filterSelectedOptions
                value={branch || ''}
                onChange={(event, value) => {
                  setBranchId('')
                  setBranch([])
                  setGrade([])
                  setSubject([])
                  setTeacher([])
                  setIsTrailValue([])
                  setTeacherID('')
                  setSelectedSubjectId('')
                  setSelectedGradeId('')
                  setIsTrailParams('')
                  setBatchId('')
                  setBatchList([])
                  setStudentBatchList('')
                  if (value) {
                    setBranchId(value?.id);
                    setBranch(value);
                  }
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    size='small'
                    variant='outlined'
                    label='Branch'
                    placeholder='Select Branch'
                  />
                )}
              />
            </Grid>
            {branchId ? (
              <>
                <Grid md={12} xs={12}>
                  <Accordion expanded={accordianOpen}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls='panel1a-content'
                      id='panel1a-header'
                      onClick={() => setAccordianOpen(!accordianOpen)}
                    >
                      <FilterIcon />
                      <Typography variant='h6' color='primary'>
                        Filter
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Grid container spacing={3} alignItems='center'>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size='small'
                            className='dropdownIcon'
                            options={selectedGrade || []}
                            getOptionLabel={(option) => option?.grade__grade_name}
                            value={grade || ''}
                            filterSelectedOptions
                            onChange={(event, value) => {
                              setGrade(value)
                              setSelectedGradeId(value?.grade_id)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size='small'
                                variant='outlined'
                                label='Grade'
                                placeholder='Select Grade'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size='small'
                            className='dropdownIcon'
                            options={selectedSubject || []}
                            getOptionLabel={(option) => option?.subject__subject_name || ''}
                            filterSelectedOptions
                            value={subject || ''}
                            onChange={(event, value) => {
                              setSubject(value)
                              setSelectedSubjectId(value?.id)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size='small'
                                variant='outlined'
                                label='Subject'
                                placeholder='Select Subject'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size='small'
                            className='dropdownIcon'
                            options={selectedTeacher || []}
                            getOptionLabel={(option) => option?.name || ''}
                            filterSelectedOptions
                            value={teacher || ''}
                            onChange={(event, value) => {
                              setTeacher(value);
                              setTeacherID(value?.tutor_id);
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size='small'
                                variant='outlined'
                                label='Teacher'
                                placeholder='Select Teacher'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size="small"
                            className='dropdownIcon'
                            options={isTrailDropDown || []}
                            getOptionLabel={(option) => option?.option_value || ''}
                            filterSelectedOptions
                            value={isTrailValue || ''}
                            onChange={(event, value) => {
                              setIsTrailParams(value?.params)
                              setIsTrailValue(value)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size="small"
                                variant="outlined"
                                label="Is Trail"
                                placeholder='Is Trail'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size="small"
                            className='dropdownIcon'
                            options={selectedBatchList || []}
                            getOptionLabel={(option) => option?.course || ''}
                            filterSelectedOptions
                            value={batchList || ''}
                            onChange={(event, value) => {
                              setBatchList(value)
                              setBatchId(value?.aol_batch_id)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size="small"
                                variant="outlined"
                                label="Batch Name"
                                placeholder='Select Batch Name'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={6} xs={12}>
                          <CustomSearchBar
                            value={filterState?.search}
                            setValue={(value) => {
                              setFilterState({ ...filterState, search: value });
                              delayedQuery(value, filterState, branchId, selectedGradeId, selectedSubjectId, teacherID, isTrailParams, batchId);

                            }}
                            onChange={(e) => {
                              setFilterState({ ...filterState, search: e.target.value.trimLeft() });
                              delayedQuery(e.target.value.trimLeft(), filterState, branchId, selectedGradeId, selectedSubjectId, teacherID, isTrailParams, batchId);

                            }}
                            label=''
                            placeholder='Student Search'
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Button
                            variant='contained'
                            color='primary'
                            className='custom_button_master labelColor'
                            size='medium'
                            onClick={handleClearAll}
                          >
                            Clear All
                          </Button>
                        </Grid>

                      </Grid>
                    </AccordionDetails>
                  </Accordion>
                </Grid>

              </>
            ) : ''}

          </Grid>
        </Grid>
        {/* {branchId ? (
          <Grid item md={12} xs={12}>
            <Filteration
              setPage={setPage}
              getStudentsList={getStudentsList}
              filterState={filterState}
              setFilterState={setFilterState}
              setLoading={setLoading}
            />
          </Grid>

        ) : ''} */}
      </Grid>
      <div>
        <Divider />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Student Name</TableCell>
                      <TableCell style={{ width: '10%' }} align='left'>
                        Start Date
                      </TableCell>
                      <TableCell style={{ width: '10%' }} align='left'>
                        End Date
                      </TableCell>
                      <TableCell align='left'>Grade</TableCell>
                      <TableCell align='left'>Subject</TableCell>
                      <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Teacher Name</TableCell>
                      <TableCell align="left" >Enrollment Number</TableCell>
                      <TableCell align='left'>Batch Name</TableCell>
                      {/* <TableCell align='left'>Batch Start Date & Time</TableCell>
                      <TableCell align='left'>Payment</TableCell> */}
                      <TableCell align='left'>Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {branch?.branch_name !== undefined && studentBatchList?.result?.length ? (
                      <>
                        {studentBatchList?.result
                          .filter((item) => item.aol_batch !== null)
                          .map((item, i) => (
                            <TableRow key={item.id}>
                              <TableCell align='left'>{i + index}</TableCell>
                              <TableCell align='left'>
                                {item?.name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align='left'>
                                {item?.start_date
                                  ? DateTimeConverter(item?.start_date)
                                  : ''}
                              </TableCell>
                              <TableCell align='left'>
                                {item?.end_date ? DateTimeConverter(item?.end_date) : ''}
                              </TableCell>
                              <TableCell align='left'>{item.grade_name}</TableCell>
                              <TableCell align='left'>
                                {item?.subject_name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align='left'>
                                {item?.course_name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              {/* <TableCell align='left'>{item.grade_name}</TableCell> */}
                              {/* <TableCell align='left'>
                              {item?.subject_name || (
                                <Typography style={{ color: 'red' }}>NA</Typography>
                              )}
                            </TableCell> */}
                              {/* <TableCell align='left'>
                              {item?.course_name || (
                                <Typography style={{ color: 'red' }}>NA</Typography>
                              )}
                            </TableCell> */}
                              <TableCell align='left'>
                                {/* {console.log(item.teacher_name,'kh22')} */}
                                {item?.teacher_name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align="left">
                                {item?.enrollment_no || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              <TableCell align='left'>
                                {item?.batch_name || (
                                  <Typography style={{ color: 'red' }}>NA</Typography>
                                )}
                              </TableCell>
                              {/* <TableCell align='left'>
                              <span style={{ color: '#FF6B6B' }}>
                                {item?.reference_start_date
                                  ? DateTimeConverter(item?.reference_start_date)
                                  : ''}
                              </span>
                              <br />
                              <span style={{ color: '#014B7E' }}>
                                {item?.batch_time_slot
                                  ? `${ConverTime(
                                      item?.batch_time_slot?.split('-')[0]
                                    )} - ${ConverTime(
                                      item?.batch_time_slot?.split('-')[1]
                                    )}`
                                  : '' || ''}
                              </span>
                            </TableCell>
                            <TableCell align='left'>
                              {item?.paid_date ? (
                                <span style={{ color: 'green' }}>
                                  {`${addCommas(item?.amount_paid) || ''} ₹` || ''}
                                  <br />
                                  &nbsp;Paid on&nbsp;
                                  {DateTimeConverter(item?.paid_date)}
                                </span>
                              ) : (
                                <span style={{ color: 'red' }}>Fail</span>
                              )}
                            </TableCell> */}
                              <TableCell align='left'>
                                <Button
                                  size='small'
                                  color='primary'
                                  variant='contained'
                                  disabled={item?.aol_batch === null}
                                  onClick={() => {
                                    setOpen(true);
                                    setSelectedItem(item);
                                  }}
                                  title='Course Extend'
                                >
                                  Update
                                  {/* <EditIcon disabled = {item?.aol_batch===null} /> */}
                                </Button>
                              </TableCell>
                            </TableRow>
                          ))}
                        <TableRow>
                          <TableCell colSpan='12'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={studentBatchList?.total_pages}
                              // count='100'
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Batches Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {loading && <Loader />}
      {branchId ? <MenuStudentDeskModel branchId={branchId} /> : ''}
      {open ? (
        <StudentLeaveModel
          content={selectedItem}
          branchId={branchId}
          open={open}
          close={() => {
            setOpen(false);
            setSelectedItem('');
          }}
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default StudentDesk;
