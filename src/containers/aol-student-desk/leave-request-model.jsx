import React, { useState, useContext, useEffect } from 'react';
import { Grid, TextField, Button, Divider, Typography } from '@material-ui/core';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Card,
  IconButton,
} from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import TimeSlotList from '../aol-batch-reference/timeSlotList';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';
import {
  DateTimeConverter,
  addCommas,
  ConverTime,
} from '../../components/dateTimeConverter';
import CustomFiterImage from '../../components/custom-filter-image';
import EditIcon from '@material-ui/icons/EditOutlined';
import { Pagination } from '@material-ui/lab';

const LeaveRequest = ({ content, open, close }) => {
  const [loading, setLoading] = useState(false);
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date().setDate(new Date().getDate() + 1));
  const [description, setDescription] = useState('');
  const [leaveData, setLeaveData] = useState([]);
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [selectedItem, setSelectedItem] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);

  const handleFromDate = (value) => {
    setFromDate(value);
  };

  const handleToDate = (value) => {
    setToDate(value);
  };

  const handleAddLeave = async () => {
    setLoading(true);
    const payload = {
      start_date: moment(fromDate).format('YYYY-MM-DD'),
      end_date: moment(toDate).format('YYYY-MM-DD'),
      description: description,
      erp_id: content?.student_id,
      course_id: content?.course_id,
      batch_id: content?.aol_batch,
    };
    try {
      const { data } = await axiosInstance.post(
        `${endpoints.students.addLeaveRequest}?start_date=${moment(fromDate).format(
          'YYYY-MM-DD'
        )}&end_date=${moment(toDate).format(
          'YYYY-MM-DD'
        )}&description=${description}&erp_id=${content?.student_id}&batch_id=${
          content?.aol_batch
        }`
      );
      if (data.status_code == 200) {
        setAlert('success', 'Successfully Applied For Leave');
        setLoading(false);
        getLeaveHistory(page);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  };

  const getLeaveHistory = async (pageNumber) => {
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.students.leaveHistory}?page=${pageNumber}&page_size=${5}`
      );
      if (data) {
        setLoading(false);
        setLeaveData(data);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  };
  function handleCancelLeave(item) {
    let confirmAction = window.confirm('Are you sure want to cancel leave?');
    if (confirmAction) {
      axiosInstance
        .put(
          `${endpoints.students.leaveCancel}?erp_id=${item.user}&leave_id=${item.id}&no_of_days=${item.no_of_sessions_leave}`
        )
        .then((res) => {
          console.log({ res });
          if (res.status == 200) {
            setAlert('success', 'Successfully Cancelled Leave');
            getLeaveHistory(page);
          }
        })
        .catch((error) => {
          setAlert('error', 'error');
        });
    } else {
      setLoading(false);
      setAlert('error', 'Something Went wrong');
    }
  }

  function handlePagination(event, page) {
    event.preventDefault();
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * 5);
    }
    getLeaveHistory(page);
  }

  useEffect(() => {
    getLeaveHistory(page);
  }, []);

  return (
    <>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Grid container spacing={2} style={{ padding: '15px 0' }}>
          <Grid item md={12}>
            <Typography
              variant='h6'
              style={{ fontSize: 20, fontWeight: 'bold', color: '#014B82' }}
            >
              Leave Request
            </Typography>
            <Divider />
          </Grid>
          <Grid item md={3}>
            <KeyboardDatePicker
              margin='dense'
              label='From Date'
              format='dd/MM/yyyy'
              inputVariant='outlined'
              size='small'
              value={fromDate}
              onChange={(value) => handleFromDate(value)}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
          </Grid>
          <Grid item md={3}>
            <KeyboardDatePicker
              margin='dense'
              label='To Date'
              format='dd/MM/yyyy'
              inputVariant='outlined'
              size='small'
              value={toDate}
              onChange={(value) => handleToDate(value)}
              KeyboardButtonProps={{
                'aria-label': 'change date',
              }}
            />
          </Grid>
          <Grid item md={4}>
            <TextField
              label='Description'
              multiline
              rows={2}
              margin='dense'
              value={description}
              size='small'
              onChange={(e) => setDescription(e.target.value)}
              variant='outlined'
            />
          </Grid>
          <Grid item md={2}>
            <Button
              variant='contained'
              color='primary'
              size='small'
              disabled={
                fromDate && toDate && description ? (loading ? true : false) : true
              }
              onClick={handleAddLeave}
              style={{ marginTop: '10px' }}
            >
              Add Leave
            </Button>
          </Grid>
          <Grid item md={12}>
            <Divider />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid alignItems='center' justifyContent='center' item md={12} xs={12}>
            <Table style={{ width: '100%', overflow: 'auto' }}>
              <TableHead>
                <TableRow>
                  <TableCell align='left'>S.No</TableCell>
                  <TableCell align='left'>ERP ID</TableCell>
                  <TableCell align='left'>Start Date</TableCell>
                  <TableCell align='left'>End Date</TableCell>
                  <TableCell align='left'>No.of Sessions for Leave</TableCell>
                  <TableCell align='left'>Description</TableCell>
                  <TableCell align='left'>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {leaveData && leaveData.results ? (
                  <>
                    {leaveData.results.map((item, i) => (
                      <TableRow key={item.id}>
                        <TableCell align='left'>{i + index}</TableCell>
                        <TableCell>{item.user}</TableCell>
                        <TableCell align='left'>
                          {item?.start_date ? DateTimeConverter(item?.start_date) : ''}
                        </TableCell>
                        <TableCell align='left'>
                          {item?.end_date ? DateTimeConverter(item?.end_date) : ''}
                        </TableCell>
                        <TableCell align='left'>
                          {item?.no_of_sessions_leave || ''}
                        </TableCell>
                        <TableCell align='left'>{item?.description || ''}</TableCell>
                        <TableCell align='left'>
                          <Button
                            size='small'
                            variant='contained'
                            color='primary'
                            disabled={item?.is_cancelled === true}
                            onClick={() => {
                              handleCancelLeave(item);
                            }}
                            title='Cancel Leave'
                          >
                            Cancel
                            {/* <EditIcon color='primary' /> */}
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))}
                    <TableRow>
                      <TableCell colSpan='9'>
                        <Pagination
                          style={{ textAlign: 'center', display: 'inline-flex' }}
                          count={Math.ceil(leaveData?.count / 5)}
                          color='primary'
                          page={page}
                          onChange={handlePagination}
                        />
                      </TableCell>
                    </TableRow>
                  </>
                ) : (
                  <TableRow>
                    <TableCell colSpan='9'>
                      <CustomFiterImage label='Batches Not Found' />
                    </TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>
      {loading && <Loader />}
    </>
  );
};

export default LeaveRequest;
