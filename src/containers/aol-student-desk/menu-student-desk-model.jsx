import React, { useState, useContext,useEffect } from 'react';
import { Grid, TextField, Button, Divider } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import CustomDialog from '../../components/custom-dialog';
import TimeSlotList from '../aol-batch-reference/timeSlotList';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import LeaveRequest from './leave-request-model';
import TeacherChangeModel from './teacher-change-model';
import GradePromoteModel from './grade-change-model';
import BatchChangeModel from './batch-change-model';
import ExtendEndDate from './extend-end-date';
import BatchPauseModel from './batch-pause-model';


const MenuStudentDeskModel = ({ content, open, close,branchId }) => {
    const [loading, setLoading] = useState(false)




    // const dropdownData = [
    //     // {
    //     //     id: 1, value: 'Leave Request',
    //     // },
    //     // {
    //     //     id: 2, value: 'Teacher Change',
    //     // },
    //     {
    //         id: 3, value: 'Grade Change',
    //     },
    //     {
    //         id: 4, value: 'Batch Change',
    //     },
    //     {
    //         id: 5, value: 'Extend End Date',
    //     },
    //     {
    //         id: 6, value: 'Batch Pause',
    //     }
    // ]


    const [selectedMenu, setSelectedMenu] = useState('')
    const [dropdownData,setSelectDropDown] = useState([])

    useEffect(() => {
        axiosInstance
        .get(`${endpoints.studentDesk.permissionStudentDesk}?branch=${branchId}`
        )
        .then((res) => {
          console.log(res.data.data,'zpy5555555555')
          setSelectDropDown(res.data.data)
    
        })
      }, [branchId])

    return (
        <>
            <Grid container spacing={2}>
                <Grid item md={12} xs={12}>
                    <CustomDialog
                        handleClose={close}
                        open={open}
                        dialogTitle='Update Details'
                        maxWidth='md'
                        fullScreen={false}
                        stylesProps={{ zIndex: '1', marginTop: 50 }}
                    >
                        <Grid
                            container
                            spacing={3}
                            style={{ padding: '10px' }}
                        >
                            <Grid item md={12} xs={12}>
                                <Autocomplete
                                    size='small'
                                    className='dropdownIcon'
                                    options={dropdownData || []}
                                    getOptionLabel={(option) => option?.value || ''}
                                    filterSelectedOptions
                                    style={{ width: 400 }}
                                    value={selectedMenu || ''}
                                    onChange={(event, value) => {
                                        setSelectedMenu(value)
                                    }}
                                    renderInput={(params) => (
                                        <TextField
                                            {...params}
                                            size='small'
                                            variant='outlined'
                                            label='Menu'
                                            placeholder='Select Item'
                                        />
                                    )}
                                />
                            </Grid>
                        </Grid>
                        {/* {
                            selectedMenu?.id === 1  && <LeaveRequest disable={content?.aol_batch === null} content={content} open={open} close={close} />
                        }
                        {
                            selectedMenu?.id === 2 && <TeacherChangeModel disable={content?.aol_batch === null} content={content} open={open} close={close} />
                        } */}
                        {
                            selectedMenu?.id === 1 && <GradePromoteModel disable={content?.aol_batch === null} content={content} open={open} close={close} />
                        }
                        {
                            selectedMenu?.id === 2  && <BatchChangeModel disable={content?.aol_batch === null} content={content} open={open} close={close} />
                        }
                        {
                            selectedMenu?.id === 3  && <ExtendEndDate disable={content?.aol_batch === null} content={content} open={open} close={close} />
                        } 
                        {   selectedMenu?.id === 4  && <BatchPauseModel disable={content?.aol_batch === null && content?.reference_start_date.setHours(0,0,0,0)>=(new Date()).setHours(0,0,0,0)} content={content} open={open} close={close} />
                        }
                    </CustomDialog>
                </Grid>
            </Grid>
            {loading && <Loader />}
        </>
    );
};

export default MenuStudentDeskModel;
