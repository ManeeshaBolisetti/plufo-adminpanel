import React, { useState, useContext, useEffect, useRef } from 'react';
import {
  Accordion,
  AccordionSummary,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
  IconButton,
  Typography,
  AccordionDetails,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import Layout from '../Layout';
import debounce from 'lodash.debounce';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Filteration from './filteration';
import Loader from '../../components/loader/loader';
import TableContainer from '@mui/material/TableContainer';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import {
  DateTimeConverter,
  addCommas,
  ConverTime,
} from '../../components/dateTimeConverter';
import EditIcon from '@material-ui/icons/EditOutlined';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Autocomplete } from '@material-ui/lab';
import StudentLeaveModel from './menu-student-desk-model';
import MenuStudentDeskModel from '../aol-student-desk/menu-student-desk-model';
import FilterIcon from 'components/icon/FilterIcon';
import { values } from 'lodash';
// import CustomSearchBar from '../../components/custom-seearch-bar';
import CustomSearchBar from 'components/custom-seearch-bar';
import { batch } from 'react-redux';

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
const StudentDesk = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [branch, setBranch] = useState([]);
  const [branchId, setBranchId] = useState('');
  const [isActiveValue, setIsActiveValue] = useState([]);
  const [isActiveOptionParams, setIsActiveOptionParams] = useState('')
  const [isTrailValue, setIsTrailValue] = useState([])
  const [isTrailParams, setIsTrailParams] = useState('')
  const [accordianOpen, setAccordianOpen] = useState(false);
  const [selectDropDown, setSelectDropDown] = useState([]);
  const [grade, setGrade] = useState([])
  const [selectedGrade, setSelectedGrade] = useState([]);
  const [selectedGradeId, setSelectedGradeId] = useState('');
  const [selectedSubject, setSelectedSubject] = useState([]);
  const [selectedSubjectId, setSelectedSubjectId] = useState('')
  const [teacher, setTeacher] = useState([]);
  const [selectedTeacher, setSelectedTeacher] = useState([]);
  const [teacherID, setTeacherID] = useState('')
  const [subject, setSubject] = useState([]);
  const [batchList, setBatchList] = useState([]);
  const [selectedBatchList, setSelectedBatchList] = useState([]);
  const [batchId, setBatchId] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const [selectedSearchValue, setSelectedSearchValue] = useState('');

  const [filterState, setFilterState] = useState({
    // gradeList: [],
    // selectedGrade: [],
    // subjectList: [],
    // selectedSubject: [],
    // startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
    // endDate: moment()?.format()?.split('T')?.[0],
    // startDate: '',
    // endDate: '',
    // fullDate: [],
    search: '',
  });
  const [open, setOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(25);

  // console.log('pepsiiiiiiii',studentBatchList)

  const isActiveDropDown = [
    { id: '1', option_value: 'True', params: 'true' },
    { id: '2', option_value: 'False', params: 'false' },
  ]

  const isTrailDropDown = [
    { id: '1', option_value: 'True', params: 'true' },
    { id: '2', option_value: 'False', params: 'false' },
  ]


  async function ApiCall(url, type) {
    // console.log({ url, type }, 'mobileee88888888')
    if (branchId) {

      setLoading(true);
      try {
        const { data } = await axiosInstance.get(url);
        if (data?.status_code === 200 || data?.status_code === 201) {
          if (type === 'grade') {
            setSelectedGrade(data?.data)
            // setSelectedGradeId(data?.grade_id)
            // setFilterState((prev) => ({ ...prev, gradeList: data?.data }));
          } else if (type === 'subject') {
            setSelectedSubject(data?.result)
          } else if (type === 'teacher') {
            setSelectedTeacher(data?.data)

          } else if (type === 'batchList') {
            setSelectedBatchList(data?.results)

          }
          setLoading(false);
        } else {
          setLoading(false);
          setAlert('error', data.message);
        }
      } catch (error) {
        setAlert('error', error.message);
        setLoading(false);
      }
    }
  }


  useEffect(() => {
    if (branchId) {
      ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}`, 'grade');
      ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}`, 'subject');
      ApiCall(`${endpoints.aolBatchAssign.getTeacherList}`, 'teacher');
      ApiCall(`${endpoints.studentDesk.batchList}?is_all=true&branch=${branchId}`, 'batchList')
    }
  }, [branchId]);


  async function getStudentsList(
    pageNo,
    searchVal,
    branch,
    grade,
    sub,
    teacher,
    isTrailData,
    batchIdValue
  ) {
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedGrade = selectedGradeId ? `&grades=${selectedGradeId}` : grade ? `&grades=${grade}` : '';
    const selectedSubject = selectedSubjectId ? `&subjects=${selectedSubjectId}` : sub ? `&subjects=${sub}` : '';
    const selectedTeacher = teacherID ? `&teacher_id=${teacherID}` : teacher ? `&teacher_id=${teacher}` : '';
    const bbranch = branchId ? `&branch=${branchId}` : branch ? `&branch=${branch}` : '';
    const selectedBatchValue = batchId ? `&aol_batch_id=${batchId}` : batchIdValue ? `&aol_batch_id=${batchIdValue}` : '';
    const isTrailParamsAPi = isTrailParams ? `&is_trail=${isTrailParams}` : isTrailData ? `&is_trail=${isTrailData}` : '';

    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.aolBatchAssign.getStudentList}?page=${pageNo}&page_size=${rowsPerPage}${searchValue}${selectedGrade}${selectedSubject}${bbranch}${isTrailParamsAPi}${selectedTeacher}${selectedBatchValue}`
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setStudentBatchList(data);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  function handlePagination(event, page) {
    event.preventDefault();
    setPage(page);
    if (page === 1) {
      setIndex(1);
    } else {
      setIndex(1 + (page - 1) * rowsPerPage);
    }
    getStudentsList(
      page,
      filterState?.search || '',
      // filterState?.selectedBranch?.id || '',
      // filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      // filterState?.subjectList?.id || '',
      // filterState?.startDate || '',
      // filterState?.endDate || ''
    );
  }

  useEffect(() => {
    setPage(1);
    // if (branchId && searchValue) {
    getStudentsList(
      1,
      filterState?.search || '',
      // branchId,
      // filterState?.selectedBranch?.id || '',
      // filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      // filterState?.subjectList?.id || '',
      // filterState?.startDate || '',
      // filterState?.endDate || ''
    );
    // }
  }, [isTrailValue, selectedGradeId, selectedSubjectId, teacherID, batchId]);

  useEffect(() => {
    if (!branchId) {

      axiosInstance.get(`${endpoints.studentDesk.branchSelect}`).then((res) => {
        setSelectDropDown(res.data.data);
      });
      setStudentBatchList('')
    }
  }, [branchId]);

  const delayedQuery = useRef(
    debounce((q, state, branch, grade, subject, teacher, isTrailData, batchId) => {
      setPage(1);
      getStudentsList(
        1,
        q,
        branch,
        grade,
        subject,
        teacher,
        isTrailData,
        batchId
        // state?.selectedGrade?.map((grade) => grade?.grade_id) || [],
        // state?.selectedSubject?.id || '',
        // state?.startDate || '',
        // state?.endDate || ''
      );
    }, 1000)
  ).current;

  const handleClearAll = () => {
    setBranchId('')
    setBranch([])
    setGrade([])
    setSubject([])
    setTeacher([])
    setIsTrailValue([])
    setTeacherID('')
    setSelectedSubjectId('')
    setSelectedGradeId('')
    setIsTrailParams('')
    setBatchId('')
    setBatchList([])
    setFilterState({ ...filterState, search: '' });
  };
  function createData(name, calories, fat, carbs, protein, price) {
    return {
      name,
      calories,
      fat,
      carbs,
      protein,
      price,
      history: [
        {
          date: '2020-01-05',
          customerId: '11091700',
          amount: 3,
        },
        {
          date: '2020-01-02',
          customerId: 'Anonymous',
          amount: 1,
        },
      ],
    };
  }
  
  function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);
  
    return (
      <React.Fragment>
        <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell align="right">{row.calories}</TableCell>
          <TableCell align="right">{row.fat}</TableCell>
          {/* <TableCell>
              
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell> */}
          <TableCell component="th" scope="row">
            {row.name}
          </TableCell>
          <TableCell align="right">{row.carbs}</TableCell>
          <TableCell align="right">{row.protein}</TableCell>
          <TableCell align="right">{row.carbs}</TableCell>
          <TableCell align="right">
          <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
              {row.calories}</TableCell>
          <TableCell align="right">{row.fat}</TableCell>
          
          <TableCell align="right">{row.protein}</TableCell>
          <TableCell align="right">
         <Button >Action</Button>
             </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  EnrollDetails
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <TableCell>SName</TableCell>
                      <TableCell>Enrollment Number</TableCell>
                      <TableCell align="right">Details</TableCell>
                      <TableCell align="right">Total price ($)</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.history.map((historyRow) => (
                      <TableRow key={historyRow.date}>
                        <TableCell component="th" scope="row">
                          {historyRow.date}
                        </TableCell>
                        <TableCell>{historyRow.customerId}</TableCell>
                        <TableCell align="right">{historyRow.amount}</TableCell>
                        <TableCell align="right">
                          {Math.round(historyRow.amount * row.price * 100) / 100}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  }
  
  
  const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0, 3.99),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3, 4.99),
    createData('Eclair', 262, 16.0, 24, 6.0, 3.79),
    createData('Cupcake', 305, 3.7, 67, 4.3, 2.5),
    createData('Gingerbread', 356, 16.0, 49, 3.9, 1.5),
  ];
  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Student Desk'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={3} xs={12}>
              <Autocomplete
                size='small'
                className='dropdownIcon'
                options={selectDropDown || []}
                getOptionLabel={(option) => option?.branch_name || ''}
                filterSelectedOptions
                value={branch || ''}
                onChange={(event, value) => {
                  setBranchId('')
                  setBranch([])
                  setGrade([])
                  setSubject([])
                  setTeacher([])
                  setIsTrailValue([])
                  setTeacherID('')
                  setSelectedSubjectId('')
                  setSelectedGradeId('')
                  setIsTrailParams('')
                  setBatchId('')
                  setBatchList([])
                  setStudentBatchList('')
                  if (value) {
                    setBranchId(value?.id);
                    setBranch(value);
                  }
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    size='small'
                    variant='outlined'
                    label='Branch'
                    placeholder='Select Branch'
                  />
                )}
              />
            </Grid>
            {branchId ? (
              <>
                <Grid md={12} xs={12}>
                  <Accordion expanded={accordianOpen}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls='panel1a-content'
                      id='panel1a-header'
                      onClick={() => setAccordianOpen(!accordianOpen)}
                    >
                      <FilterIcon />
                      <Typography variant='h6' color='primary'>
                        Filter
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Grid container spacing={3} alignItems='center'>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size='small'
                            className='dropdownIcon'
                            options={selectedGrade || []}
                            getOptionLabel={(option) => option?.grade__grade_name}
                            value={grade || ''}
                            filterSelectedOptions
                            onChange={(event, value) => {
                              setGrade(value)
                              setSelectedGradeId(value?.grade_id)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size='small'
                                variant='outlined'
                                label='Grade'
                                placeholder='Select Grade'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size='small'
                            className='dropdownIcon'
                            options={selectedSubject || []}
                            getOptionLabel={(option) => option?.subject__subject_name || ''}
                            filterSelectedOptions
                            value={subject || ''}
                            onChange={(event, value) => {
                              setSubject(value)
                              setSelectedSubjectId(value?.id)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size='small'
                                variant='outlined'
                                label='Subject'
                                placeholder='Select Subject'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size='small'
                            className='dropdownIcon'
                            options={selectedTeacher || []}
                            getOptionLabel={(option) => option?.name || ''}
                            filterSelectedOptions
                            value={teacher || ''}
                            onChange={(event, value) => {
                              setTeacher(value);
                              setTeacherID(value?.tutor_id);
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size='small'
                                variant='outlined'
                                label='Teacher'
                                placeholder='Select Teacher'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size="small"
                            className='dropdownIcon'
                            options={isTrailDropDown || []}
                            getOptionLabel={(option) => option?.option_value || ''}
                            filterSelectedOptions
                            value={isTrailValue || ''}
                            onChange={(event, value) => {
                              setIsTrailParams(value?.params)
                              setIsTrailValue(value)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size="small"
                                variant="outlined"
                                label="Is Trail"
                                placeholder='Is Trail'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Autocomplete
                            size="small"
                            className='dropdownIcon'
                            options={selectedBatchList || []}
                            getOptionLabel={(option) => option?.course || ''}
                            filterSelectedOptions
                            value={batchList || ''}
                            onChange={(event, value) => {
                              setBatchList(value)
                              setBatchId(value?.aol_batch_id)
                            }}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                size="small"
                                variant="outlined"
                                label="Batch Name"
                                placeholder='Select Batch Name'
                              />
                            )}
                          />
                        </Grid>
                        <Grid item md={6} xs={12}>
                          <CustomSearchBar
                            value={filterState?.search}
                            setValue={(value) => {
                              setFilterState({ ...filterState, search: value });
                              delayedQuery(value, filterState, branchId, selectedGradeId, selectedSubjectId, teacherID, isTrailParams, batchId);

                            }}
                            onChange={(e) => {
                              setFilterState({ ...filterState, search: e.target.value.trimLeft() });
                              delayedQuery(e.target.value.trimLeft(), filterState, branchId, selectedGradeId, selectedSubjectId, teacherID, isTrailParams, batchId);

                            }}
                            label=''
                            placeholder='Student Search'
                          />
                        </Grid>
                        <Grid item md={3} xs={12}>
                          <Button
                            variant='contained'
                            color='primary'
                            className='custom_button_master labelColor'
                            size='medium'
                            onClick={handleClearAll}
                          >
                            Clear All
                          </Button>
                        </Grid>

                      </Grid>
                    </AccordionDetails>
                  </Accordion>
                </Grid>

              </>
            ) : ''}

          </Grid>
        </Grid>
        {/* {branchId ? (
          <Grid item md={12} xs={12}>
            <Filteration
              setPage={setPage}
              getStudentsList={getStudentsList}
              filterState={filterState}
              setFilterState={setFilterState}
              setLoading={setLoading}
            />
          </Grid>

        ) : ''} */}
      </Grid>
      <div>
        <Divider />
      </div>
      <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            {/* <TableCell /> */}
            <TableCell>S No</TableCell>
            <TableCell align="right">Studentname</TableCell>
            <TableCell align="right">Startdate</TableCell>
            <TableCell align="right">EndDate</TableCell>
            <TableCell align="right">Grade&nbsp;(g)</TableCell>
            <TableCell align="right">Subject&nbsp;(g)</TableCell>

            <TableCell align="right">Coursename</TableCell>
            <TableCell align="right">Teachername</TableCell>

            <TableCell align="right">BatchName</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Row key={row.name} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
     
      {loading && <Loader />}
      {branchId ? <MenuStudentDeskModel branchId={branchId} /> : ''}
      {open ? (
        <StudentLeaveModel
          content={selectedItem}
          branchId={branchId}
          open={open}
          close={() => {
            setOpen(false);
            setSelectedItem('');
          }}
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default StudentDesk;
