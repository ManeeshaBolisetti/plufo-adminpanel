import React, { useState, useContext, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Grid, Typography, Button, Divider, TextField } from '@material-ui/core';
import CustomDialog from '../../components/custom-dialog';
import ConfirmDialog from '../../components/confirm-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { Autocomplete } from '@material-ui/lab';

const TeacherChangeModel = ({ content, open, close }) => {

  const location = useLocation();
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [teacherData, setTeacherData] = useState([]);
  const [selectedTeacher, setSelectedTeacher] = useState('')

  useEffect(() => {
    getTeachers()
  }, [])

  const getTeachers = async() => {
    setLoading(false)
    try {
        const { data } = await axiosInstance.get(
          `${endpoints.aolBatchAssign.getTeacherList}`,
        );
        if (data) {
          setLoading(false);
          setTeacherData(data)
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
  }

  async function AssignTeacher() {
    setLoading(true);
    const payload = {
      teacher_id: selectedTeacher?.tutor_id,
      batch_id: content?.aol_batch,
    };
    try {
      const { data } = await axiosInstance.put(
        endpoints.aolTeachesBatchAssign.assignTeacherToBatch,
        { ...payload }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setAlert('success', data.message);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }
  return (
    <>
        <Typography variant="h6" style={{fontSize: 20,fontWeight: 'bold',color: '#014B82', marginTop: 20}}>Teacher Change</Typography>
        <Divider />
        <Grid item  md={12} xs={12}
                    container
                    spacing={2}
                    style={{ padding: '30px' }}
                    direction="row">
          <Grid item md={6} xs={6}>
          <Grid item md={12} xs={12}>
            <Autocomplete
                size='small'
                className='dropdownIcon'
                options={teacherData?.data|| []}
                getOptionLabel={(option) => option?.name || ''}
                filterSelectedOptions
                style={{width: 400}}
                value={selectedTeacher || ''}
                onChange={(event, value) => {
                    setSelectedTeacher(value)
                }}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        size='small'
                        variant='outlined'
                        label='Teacher'
                        placeholder='Select teacher'
                    />
                )}
            />
          </Grid>
          <Grid item md={6} xs={6} style={{ margin: '10px 0px' }}>
            <Grid
              container
              spacing={2}
              direction='row'
              alignItems='center'
              justify='center'
            >
              <Button
                onClick={AssignTeacher}
                disabled={selectedTeacher === ''}
                variant='contained'
                color='primary'
                size='small'
              >
                Assign
              </Button>
            </Grid>
            </Grid>
          </Grid>
          <Grid item md={6} xs={6}>
            <Grid
              container
              spacing={2}
              direction='row'
              alignItems='center'
              justify='center'
            >
              <Typography variant="h6" style={{fontSize: 20, color: '#014B82', marginTop: 20}}>
              <strong> Teacher Name: </strong> {content?.teacher_name || ''}
                </Typography>
            </Grid>
            
          </Grid>
        </Grid>
      {loading && <Loader />}
    </>
  );
};

export default TeacherChangeModel;