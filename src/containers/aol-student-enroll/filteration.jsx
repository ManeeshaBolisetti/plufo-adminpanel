import React, { useRef, useContext, useEffect, useState } from 'react';
import { Grid, TextField, Button } from '@material-ui/core';
import debounce from 'lodash.debounce';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import { LocalizationProvider, DateRangePicker } from '@material-ui/pickers-4.2';
import { Autocomplete } from '@material-ui/lab';
import moment from 'moment';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import './style.scss';
const Filteration = ({ setPage, getStudentsList, filterState, setFilterState,setFilterCLear,setStudentBatchList }) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [hRef, setHRef] = useState([]);
  const [loading, setLoading] = useState(false);
  const bearerToken = JSON.parse(localStorage.getItem('userDetails'));
  const [branch, setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)
  const [selectDropDown, setSelectDropDown] = useState([])
  const [gradeId, setGradeId] = useState([])

  useEffect(() => {
    const searchValue = filterState?.search ? `&term=${filterState?.search}` : '';
    const selectedBranch = filterState?.selectedBranch?.id
      ? `&branch=${filterState?.selectedBranch?.id}` : '';
    const selectedGrade = filterState?.selectedGrade?.map((grade) => grade?.grade_id)
      ?.length
      ? `&grades=${filterState?.selectedGrade?.map((grade) => grade?.grade_id)}`
      : '';
    const selectedSubject = filterState?.selectedSubject?.id
      ? `&subject_id=${filterState?.selectedSubject?.id}`
      : '';
    const start = filterState?.startDate ? `&start_date=${filterState?.startDate}` : '';
    const end = filterState?.endDate ? `&end_date=${filterState?.endDate}` : '';
    if (selectedSubject && selectedGrade && selectedBranch) {
      // setHRef(
      axiosInstance.get(`${endpoints.aolBatchAssign.downloadEnrollmentList}?is_download=True&export_type=csv${searchValue}${selectedBranch}${selectedGrade}${selectedSubject}${start}${end}`)
      // );
    }
  }, [filterState]);

  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200 || data?.status_code === 201) {
        if (type === 'branch') {
          setFilterState((prev) => ({ ...prev, branchList: data?.data }));
        }
        if (type === 'grade') {
          setFilterState((prev) => ({ ...prev, gradeList: data?.data }));
        } else if (type === 'subject') {
          setFilterState((prev) => ({ ...prev, subjectList: data?.result }));
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  useEffect(() => {
    ApiCall(`${endpoints.studentDesk.branchSelect}`, 'branch');
  }, [])

  useEffect(() => {
    if (branchId) {
      // ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}`, 'subject');
      ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}`, 'grade');
    }
  }, [branchId]);

  useEffect(() => {
    if(branchId && gradeId)
    ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}&grades=${gradeId}`, 'subject');
  }, [gradeId])

  const delayedQuery = useRef(
    debounce((q, state) => {
      setPage(1);
      getStudentsList(
        1,
        q,
        state?.selectedBranch?.id || '',
        state?.selectedGrade?.map((grade) => grade?.grade_id) || [],
        state?.selectedSubject?.id || '',
        state?.startDate || '',
        state?.endDate || ''
      );
    }, 1000)
  ).current;

  const handleDownloadCsv = () => {
    const searchValue = filterState?.search ? `&term=${filterState?.search}` : '';
    const selectedBranch = filterState?.selectedBranch?.id ? `&branch=${filterState?.selectedBranch?.id}` : '';
    const selectedGrade = filterState?.selectedGrade?.map((grade) => grade?.grade_id)
      ?.length
      ? `&grades=${filterState?.selectedGrade?.map((grade) => grade?.grade_id)}`
      : '';
    const selectedSubject = filterState?.selectedSubject?.id
      ? `&subject_id=${filterState?.selectedSubject?.id}`
      : '';
    const start = filterState?.startDate ? `&start_date=${filterState?.startDate}` : '';
    const end = filterState?.endDate ? `&end_date=${filterState?.endDate}` : '';
    if (selectedBranch && selectedSubject && selectedGrade) {
      console.log('hi')
      // setHRef([
      //   // {
      //   //   csv: `${endpoints.aolBatchAssign.downloadEnrollmentList}?is_download=True&export_type=csv${searchValue}${selectedBranch}${selectedGrade}${selectedSubject}${start}${end}`,
      //   // },
      //   {
      //     // csv:`https://qa.plufo.letseduvate.com/qbox/aol/GetEnrollmentList/?is_download=True&export_type=csv&branch=1&grades=2&subject_id=2&start_date=2021-12-08&end_date=2021-12-09`
      //   }
      // ]);
      window.open(`${endpoints.aolBatchAssign.downloadEnrollmentList}?is_download=True&export_type=csv${searchValue}${selectedBranch}${selectedGrade}${selectedSubject}${start}${end}`,'_blank')
    } else {
      setAlert('error', 'Please select Branch,Grade & Subject');
    }
  };

  // function handleDate(v1) {
  //   if (v1 && v1.length !== 0) {
  //     setStartDate(moment(new Date(v1[0])).format('YYYY-MM-DD'));
  //     setEndDate(moment(new Date(v1[1])).format('YYYY-MM-DD'));
  //   }
  //   setDateRangeTechPer(v1);
  // }

  return (
    <Grid container spacing={2}>
      <Grid item md={3} xs={12}>
        <Autocomplete
          size='small'
          className='dropdownIcon'
          options={filterState?.branchList || []}
          getOptionLabel={(option) => option?.branch_name || ''}
          filterSelectedOptions
          value={branch || ''}
          onChange={(event, value) => {
            console.log({ event, value }, 'branch check')
            setFilterState({ ...filterState, selectedBranch: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              value?.id || '',
              filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
              filterState?.selectedSubject?.id || '',
              filterState?.startDate || '',
              filterState?.endDate || ''
            );
            setBranchId(value?.id)
            setBranch(value)
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Branch'
              placeholder='Select Branch'
            />
          )}
        />
      </Grid>
      <Grid item md={4} xs={12}>
        <Autocomplete
          size='small'
          id='grades'
          multiple
          className='dropdownIcon'
          options={filterState?.gradeList || []}
          getOptionLabel={(option) => option?.grade__grade_name || ''}
          filterSelectedOptions
          value={filterState?.selectedGrade || ''}
          onChange={(event, value) => {
            setGradeId(value?.map((item) => item?.grade_id))
            setFilterState({ ...filterState, selectedGrade: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              filterState?.selectedBranch?.id || '',
              value?.map((grade) => grade?.grade_id) || [],
              filterState?.selectedSubject?.id || '',
              filterState?.startDate || '',
              filterState?.endDate || ''
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Grade'
              placeholder='Grade'
            />
          )}
        />
      </Grid>
      <Grid item md={4} xs={12}>
        <Autocomplete
          size='small'
          id='subject'
          className='dropdownIcon'
          options={filterState?.subjectList || []}
          getOptionLabel={(option) => option?.subject__subject_name || ''}
          filterSelectedOptions
          value={filterState?.selectedSubject || ''}
          onChange={(event, value) => {
            setFilterState({ ...filterState, selectedSubject: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              filterState?.selectedBranch?.id || '',
              filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
              value?.id || '',
              filterState?.startDate || '',
              filterState?.endDate || '',
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Subject'
              placeholder='Subject'
            />
          )}
        />
      </Grid>
      <Grid item md={4} xs={12}>
        <LocalizationProvider dateAdapter={MomentUtils}>
          <DateRangePicker
            startText='Select-date-range'
            value={filterState?.fullDate}
            onChange={(newValue) => {
              // handleDate(newValue);
              setFilterState((prev) => ({ ...prev, fullDate: newValue }));
              setFilterState((prev) => ({
                ...prev,
                startDate: moment(newValue?.[0])?.format()?.split('T')?.[0],
              }));
              setFilterState((prev) => ({
                ...prev,
                endDate: moment(newValue?.[1])?.format()?.split('T')?.[0],
              }));
              setPage(1);
              getStudentsList(
                1,
                filterState?.search || '',
                filterState?.selectedBranch?.id || '',
                filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                filterState?.selectedSubject?.id || '',
                moment(newValue?.[0])?.format()?.split('T')?.[0] || '',
                moment(newValue?.[1])?.format()?.split('T')?.[0] || ''
              );
            }}
            renderInput={({ inputProps, ...startProps }, endProps, params) => {
              console.log(inputProps, params, 'check');
              return (
                <>
                  <TextField
                    {...startProps}
                    inputProps={{
                      ...inputProps,
                      value: `${inputProps.value} - ${endProps.inputProps.value}`,
                      readOnly: true,
                    }}
                    InputLabelProps={{
                      color: '#014b7e',
                    }}
                    size='small'
                    style={{ minWidth: '100%' }}
                    helperText=''
                  />
                </>
              );
            }}
          />
        </LocalizationProvider>
      </Grid>
      <Grid item md={6} xs={12}>
        <CustomSearchBar
          value={filterState?.search}
          setValue={(value) => {
            setFilterState({ ...filterState, search: value });
            delayedQuery(value, filterState);
          }}
          onChange={(e) => {
            setFilterState({ ...filterState, search: e.target.value.trimLeft() });
            delayedQuery(e.target.value.trimLeft(), filterState);
          }}
          label=''
          placeholder='Student Search'
        />
      </Grid>
      <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
        <Button
          style={{ borderRadius: '10px' }}
          size='small'
          fullWidth
          variant='contained'
          color='primary'
          onClick={() => {
            setBranch([]);
            setPage(1);
            setStudentBatchList([])
            setFilterCLear()
            setFilterState((prev) => ({
              ...prev,
              selectedBranch: [],
              // setBranch: '',
              selectedGrade: [],
              selectedSubject: [],
              startDate: '',
              endDate: '',
              fullDate: [],
              search: '',
            }));
            
          }}
        >
          Clear Filter
        </Button>
      </Grid>
      <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
        <Button
          style={{ borderRadius: '10px' }}
          size='small'
          fullWidth
          variant='contained'
          color='primary'

          // href={hRef && hRef[0] && hRef[0].csv}
          onClick={(e) => {
            handleDownloadCsv(e);
          }}
        >
          Download CSV
        </Button>
      </Grid>
      {loading && <Loader />}
    </Grid>
  );
};

export default Filteration;
