import React, { useState, useContext, useEffect } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Filteration from './filteration';
import Loader from '../../components/loader/loader';
import {
  DateTimeConverter,
  addCommas,
  ConverTime,
} from '../../components/dateTimeConverter';
import { Autocomplete } from '@material-ui/lab';


const StudentBatchEnroll = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [showData, setShowData] = useState(false)
  const [enrollGrade, setEnrollGrade] = useState('');
  const [filterState, setFilterState] = useState({
    gradeList: [],
    selectedBranch:[],
    selectedGrade: [],
    subjectList: [],
    selectedSubject: [],
    startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
    endDate: moment()?.format()?.split('T')?.[0],
    fullDate: [],
    search: '',
  });

  async function getStudentsList(pageNo, searchVal, branch, grad, sub, startDate, endDate) {
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    const selectedBranch = branch ? `&branch=${branch}` : '';
    const selectedGrade = grad?.length ? `&grades=${grad}` : '';
    const selectedSubject = sub ? `&subject_id=${sub}` : '';
    const start = startDate ? `&start_date=${startDate}` : '';
    const end = endDate ? `&end_date=${endDate}` : '';
    if(branch){
      setLoading(true);
      try {
        const { data } = await axiosInstance.get(
          `${
            endpoints.aolBatchAssign.getEnrollmentList
          }?page=${pageNo}&page_size=${10}${searchValue}${selectedBranch}${selectedGrade}${selectedSubject}${start}${end}`
        );
        if (data?.status_code === 200) {
          setLoading(false);
          setStudentBatchList(data);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    }
  }

  const handleCLearFilter =()=> {
    setShowData(true)
  }
  function handlePagination(event, page) {
    event.preventDefault();
    setPage(page);
    if(page===1){
      setIndex(1)
    }else{
      setIndex(1+(page-1)*10)
    }
    getStudentsList(
      page,
      filterState?.search || '',
      filterState?.selectedBranch?.id || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.selectedSubject?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }

  useEffect(() => {
    setPage(1);
    getStudentsList(
      1,
      filterState?.search || '',
      filterState?.selectedBranch?.id || '',
      filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
      filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }, []);

  function handleEdit(item) {
    const data = { ...item };
    if(enrollGrade){
      const updateGrade = { grade: enrollGrade.grade_id }
      setLoading(true);
      axiosInstance
        .patch(`${endpoints.aolBatchAssign.promoteStudentEnrollApi}${data.student_id}/`, updateGrade)
        .then((response) => {
          setLoading(false);
          if (response.status === 200) {
            setEnrollGrade('');
            setLoading(false);
            // setAlert('success', response.data.message);
            setAlert('success', 'Successfully Grade Change')
            history.push('/aol-enroll-student');
            return response;
          } else {
            setAlert('error', response?.data?.description);
          }
        })
        .catch((err) => {
          setAlert('success', err?.response?.data?.description);
          setLoading(false);
        });
    }else{
      const message = 'Please Select new Grade to Promote';
      setAlert('warning',message);
    }
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='All Students'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Filteration
            setPage={setPage}
            getStudentsList={getStudentsList}
            filterState={filterState}
            setFilterState={setFilterState}
            setLoading={setLoading}
            setFilterCLear ={handleCLearFilter}
            setStudentBatchList ={setStudentBatchList}
          />
        </Grid>
      </Grid>
      <div>
        <Divider />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Student Name</TableCell>
                      <TableCell align='left'>Date of Enrollment</TableCell>
                      <TableCell align='left' style={{width:'15%'}}>Grade</TableCell>
                      <TableCell align='left'>Subject</TableCell>
                      <TableCell align='left'>Course Name</TableCell>
                      <TableCell align='left'>Batch Name</TableCell>
                      <TableCell align='left'>Batch Start Date & Time</TableCell>
                      <TableCell align='left'>Payment</TableCell>
                      <TableCell align='left'>Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {studentBatchList?.result?.length ? (
                      <>
                        {studentBatchList?.result.map((item, i) => (
                          <TableRow key={item.id}>
                            <TableCell align='left'>{i + index}</TableCell>
                            <TableCell align='left'>{item?.name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.enrollment_date
                                ? DateTimeConverter(item?.enrollment_date)
                                : ''}
                            </TableCell>
                            <TableCell align='left'>
                              <Autocomplete
                                    size='small'
                                    id='grades'
                                    // multiple
                                    disableClearable
                                    className='dropdownIcon'
                                    options={filterState?.gradeList || []}
                                    getOptionLabel={(option) => option?.grade__grade_name || ''}
                                    filterSelectedOptions
                                    value={item?.grade_name}
                                    onChange={(event, value) => {
                                      item.grade_name = value.grade__grade_name;
                                      setEnrollGrade(value)
                                    
                                    }}
                                    renderInput={(params) => (
                                      <TextField
                                        {...params}
                                        size='small'
                                        variant='outlined'
                                        label={item?.grade_name || ''}
                                        placeholder='Grade'
                                      />
                                    )}
                                  />
                                  </TableCell>
                            <TableCell align='left'>{item?.subject_name || ''}</TableCell>
                            <TableCell align='left'>{item?.course_name || ''}</TableCell>
                            <TableCell align='left'>{item?.batch_name || ''}</TableCell>
                            <TableCell align='left'>
                              <span style={{ color: '#FF6B6B' }}>
                                {item?.reference_start_date
                                  ? DateTimeConverter(item?.reference_start_date)
                                  : ''}
                              </span>
                              <br />
                              <span style={{ color: '#014B7E' }}>
                                {item?.batch_time_slot
                                  ? `${ConverTime(
                                      item?.batch_time_slot?.split('-')[0]
                                    )} - ${ConverTime(
                                      item?.batch_time_slot?.split('-')[1]
                                    )}`
                                  : '' || ''}
                              </span>
                            </TableCell>
                            <TableCell align='left'>
                              {item?.paid_date ? (
                                <span style={{ color: 'green' }}>
                                  {`${addCommas(item?.amount_paid) || ''} ₹` || ''}
                                  <br />
                                  &nbsp;Paid on&nbsp;
                                  {DateTimeConverter(item?.paid_date)}
                                </span>
                              ) : (
                                <span style={{ color: 'red' }}>Fail</span>
                              )}
                            </TableCell>
                            <TableCell align='left'>{ item? 
                            <Button
                             color='primary'
                             size='small'
                             onClick={() => handleEdit(item)}
                             >
                               Promote
                             </Button>
                             :'' || ''}
                             </TableCell>

                          </TableRow>
                        ))}
                        <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={studentBatchList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Batches Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default StudentBatchEnroll;
