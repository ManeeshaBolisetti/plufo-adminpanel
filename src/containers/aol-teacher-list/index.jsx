import React, { useState, useContext, useEffect, useRef } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Button,
  TextField,
} from '@material-ui/core';
import debounce from 'lodash.debounce';
import './style.scss';
import { useHistory } from 'react-router-dom';
import { Pagination, Autocomplete } from '@material-ui/lab';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import CustomSearchBar from '../../components/custom-seearch-bar';

const AolTeacherList = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [teacherList, setTeacherList] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [subjectList, setSubjectList] = useState([]);
  const [selectedSubject, setSelectedSubject] = useState('');

  const ApiCall = async (API, KEY, subId) => {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(API);
      if (data?.status_code === 201 || data?.status_code === 200) {
        setLoading(false);
        if (KEY === 'subject') {
          setSubjectList(data?.result);
          setSelectedSubject(data?.result?.filter((item) => item?.id === subId)?.[0]);
        } else {
          setTeacherList(data);
        }
      } else {
        setAlert('error', data?.message);
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  };
  const debounceApiCall = useRef(
    debounce((api, parms) => {
      ApiCall(api, 'teacher');
      history.push(parms);
    }, 1000)
  ).current;
  useEffect(() => {
    const quiryDetails = new URLSearchParams(window.location.search);
    const SubjetId = parseInt(quiryDetails.get('subject_id'), 10) || '';
    const pageNo = parseInt(quiryDetails.get('page'), 10) || 1;
    const search = quiryDetails.get('term') || '';
    const searchParm = search ? `&term=${search}` : '';
    setSearchValue(search);
    setPage(pageNo);
    if (pageNo && SubjetId) {
      ApiCall(
        `${
          endpoints.aolTeachesBatchAssign.teacherListSubjectBase
        }?subject_id=${SubjetId}&page=${pageNo}&page_size=${10}${searchParm}`,
        'teacher'
      );
    }
    ApiCall(`${endpoints.communication.subjectList}?branch_id=1`, 'subject', SubjetId);
  }, []);

  function handlePagination(event, page) {
    setPage(page);
    const searchVal = searchValue ? `&term=${searchValue}` : '';
    history.push(
      `/aol-teachers/?subject_id=${selectedSubject?.id}&page=${page}${searchVal}`
    );
    ApiCall(
      `${endpoints.aolTeachesBatchAssign.teacherListSubjectBase}?subject_id=${
        selectedSubject?.id
      }&page=${page}&page_size=${10}${searchVal}`,
      'teacher'
    );
  }

  function CallSearhApi(value) {
    setPage(1);
    const searchVal = value ? `&term=${value}` : '';
    debounceApiCall(
      `${endpoints.aolTeachesBatchAssign.teacherListSubjectBase}?subject_id=${
        selectedSubject?.id
      }${searchVal}&page=${1}&page_size=${10}`,
      `/aol-teachers/?subject_id=${selectedSubject?.id}&page=${page}${searchVal}`
    );
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Teacher List'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={4} xs={12}>
              <Autocomplete
                size='small'
                id='sbuject'
                className='dropdownIcon'
                options={subjectList || []}
                getOptionLabel={(option) => option?.subject__subject_name || ''}
                filterSelectedOptions
                value={selectedSubject || ''}
                onChange={(event, value) => {
                  setSelectedSubject(value);
                  if (value) {
                    history.push(`/aol-teachers/?subject_id=${value?.id}&page=${1}`);
                    setPage(1);
                    ApiCall(
                      `${
                        endpoints.aolTeachesBatchAssign.teacherListSubjectBase
                      }?subject_id=${value.id}&page=${1}&page_size=${10}`,
                      'teacher'
                    );
                  } else {
                    history.push(`/aol-teachers`);
                    setTeacherList('');
                  }
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    size='small'
                    variant='outlined'
                    label='Subject'
                    placeholder='Subject'
                  />
                )}
              />
            </Grid>
            {selectedSubject ? (
              <Grid item md={4} xs={12}>
                <CustomSearchBar
                  value={searchValue}
                  setValue={(value) => {
                    setSearchValue(value);
                    CallSearhApi(value);
                  }}
                  onChange={(e) => {
                    setSearchValue(e.target.value.trimLeft());
                    CallSearhApi(e.target.value.trimLeft());
                  }}
                  label=''
                  placeholder='Search for teacher'
                />
              </Grid>
            ) : (
              ''
            )}
          </Grid>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Card style={{ padding: '10px', borderRadius: '10px' }}>
                <Grid container spacing={2}>
                  <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                    <Table style={{ width: '100%', overflow: 'auto' }}>
                      <TableHead>
                        <TableRow>
                          <TableCell align='left'>S.No</TableCell>
                          <TableCell align='left'>Name</TableCell>
                          <TableCell align='left'>ERP ID</TableCell>
                          <TableCell align='left'>Contact</TableCell>
                          <TableCell align='left'>E-Mail</TableCell>
                          <TableCell align='left'>Subject</TableCell>
                          <TableCell align='left'>Batches</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {teacherList?.results?.length ? (
                          <>
                            {teacherList?.results?.map((item, index) => (
                              <TableRow>
                                <TableCell align='left'>{index + 1}</TableCell>
                                <TableCell align='left'>{item?.name || ''}</TableCell>
                                <TableCell align='left'>{item?.erp_id || ''}</TableCell>
                                <TableCell align='left'>
                                  <a
                                    style={{ textDecoration:"none",color: '#105082' }}
                                    href={`tel: +91 ${item?.contact}`}
                                  >
                                    {item?.contact || ''}
                                  </a>
                                </TableCell>
                                <TableCell align='left'>
                                  <a
                                    style={{textDecoration:"none", color: '#105082' }}
                                    href={`mailto: ${item?.email}`}
                                  >
                                    {item?.email || ''}
                                  </a>
                                </TableCell>
                                <TableCell align='left'>
                                  {item?.subjects
                                    ? item?.subjects
                                        ?.map((info) => info?.subject_name)
                                        .join(' , ')
                                    : ''}
                                </TableCell>
                                <TableCell align='left'>
                                  <Button
                                    variant='contained'
                                    color='primary'
                                    size='small'
                                    style={{ fontSize: '13px' }}
                                    onClick={() => {
                                      history.push(`/aol-teachers/${item?.erp_id}`);
                                    }}
                                  >
                                    View All Batches
                                  </Button>
                                </TableCell>
                              </TableRow>
                            ))}
                            <TableRow>
                              <TableCell colSpan='8'>
                                <Pagination
                                  style={{ textAlign: 'center', display: 'inline-flex' }}
                                  onChange={handlePagination}
                                  count={teacherList?.total_pages}
                                  color='primary'
                                  page={page}
                                />
                              </TableCell>
                            </TableRow>
                          </>
                        ) : (
                          <TableRow>
                            <TableCell colSpan='8'>
                              <CustomFiterImage
                                label={
                                  teacherList
                                    ? 'Teahers not Available'
                                    : 'Select Subject to get Teachers List'
                                }
                              />
                            </TableCell>
                          </TableRow>
                        )}
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default AolTeacherList;
