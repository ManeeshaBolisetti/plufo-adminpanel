import React, { useState, useContext, useEffect } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Button,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import './style.scss';
import { useHistory, useParams } from 'react-router-dom';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import CustomSearchBar from '../../components/custom-seearch-bar';
import { DateTimeConverter, ConverTime } from '../../components/dateTimeConverter';

const TeacherAllBatchesList = () => {
  const { id } = useParams();
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [teacherList, setTeacherList] = useState([]);
  const [searchValue, setSearchValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);

  const ApiCall = async (API) => {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(API);
      if (data?.status_code === 200) {
        setLoading(false);
        setTeacherList(data);
      } else {
        setAlert('error', data?.message);
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  };

  useEffect(() => {
    ApiCall(
      `${
        endpoints.aolTeachesBatchAssign.teacherBatchList
      }?erp_id=${id}&page=${1}&page_size=${10}`
    );
  }, [id]);

  function handlePagination(event, page) {
    ApiCall(
      `${
        endpoints.aolTeachesBatchAssign.teacherBatchList
      }?erp_id=${id}&page=${page}&page_size=${10}`
    );
    setPage(page);
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Teacher List'
          childComponentNameNext='All Batch List'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2} direction='row' justify='center'>
            <Grid item md={4} xs={12}>
              <CustomSearchBar
                value={searchValue}
                setValue={setSearchValue}
                onChange={(e) => setSearchValue(e.target.value.trimLeft())}
                label=''
                placeholder='Search for batch'
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Card style={{ padding: '10px', borderRadius: '10px' }}>
                <Grid container spacing={2}>
                  <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                    <Table style={{ width: '100%', overflow: 'auto' }}>
                      <TableHead>
                        <TableRow>
                          <TableCell align='left'>S.No</TableCell>
                          <TableCell align='left'>Batch Name</TableCell>
                          <TableCell align='left'>Batch Size</TableCell>
                          <TableCell align='left'>Days</TableCell>
                          <TableCell align='left'>Start Date</TableCell>
                          <TableCell align='left'>End Date</TableCell>
                          <TableCell align='left'>Time</TableCell>
                          <TableCell align='left'>No of sessions</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {teacherList?.results?.length ? (
                          <>
                            {teacherList?.results?.filter((info) =>
                              info?.batch_name
                                ?.toLowerCase()
                                ?.includes(searchValue?.toLowerCase())
                            ).length ? (
                              teacherList?.results
                                ?.filter((info) =>
                                  info?.batch_name
                                    ?.toLowerCase()
                                    ?.includes(searchValue?.toLowerCase())
                                )
                                ?.map((item, index) => (
                                  <TableRow>
                                    <TableCell align='left'>{index + 1}</TableCell>
                                    <TableCell align='left'>
                                      {item?.batch_name || ''}
                                    </TableCell>
                                    <TableCell align='left'>{`1 : ${item?.batch_size}`}</TableCell>
                                    <TableCell align='left'>
                                      {item?.batch_days?.[0] || ''}
                                    </TableCell>
                                    <TableCell align='left'>
                                      {item?.start_date
                                        ? DateTimeConverter(item?.start_date)
                                        : ''}
                                    </TableCell>
                                    <TableCell align='left'>
                                      {item?.end_date
                                        ? DateTimeConverter(item?.end_date)
                                        : ''}
                                    </TableCell>
                                    <TableCell align='left'>
                                      {item?.batch_time_slot
                                        ? `${ConverTime(
                                            item?.batch_time_slot?.split('-')?.[0]
                                          )} - ${ConverTime(
                                            item?.batch_time_slot?.split('-')?.[1]
                                          )}`
                                        : ''}
                                    </TableCell>
                                    <TableCell align='left'>
                                      {item?.course_details?.no_of_session || ''}
                                    </TableCell>
                                  </TableRow>
                                ))
                            ) : (
                              <TableRow>
                                <TableCell colSpan='8'>
                                  <CustomFiterImage
                                    label={`Batches not found with ${searchValue}`}
                                  />
                                </TableCell>
                              </TableRow>
                            )}
                            <TableRow>
                              <TableCell colSpan='8'>
                                <Pagination
                                  style={{ textAlign: 'center', display: 'inline-flex' }}
                                  onChange={handlePagination}
                                  count={teacherList?.total_pages}
                                  color='primary'
                                  page={page}
                                />
                              </TableCell>
                            </TableRow>
                          </>
                        ) : (
                          <TableRow>
                            <TableCell colSpan='8'>
                              <CustomFiterImage label='Batches are not available' />
                            </TableCell>
                          </TableRow>
                        )}
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Button variant='contained' color='secondary' onClick={() => history.goBack()}>
            Back
          </Button>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default TeacherAllBatchesList;
