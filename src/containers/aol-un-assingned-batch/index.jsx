import React, { useState, useEffect, useContext, useRef } from 'react';
import {
  Grid,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Card,
  TextField,
  Typography
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import debounce from 'lodash.debounce';
import { useHistory } from 'react-router-dom';
import { Pagination } from '@material-ui/lab';
import CustomFiterImage from '../../components/custom-filter-image';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import { DateTimeConverter } from '../../components/dateTimeConverter';
import TeacherAssignModel from './teacher-assignMode';

const AolUnAssignedBatch = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [page, setPage] = useState(1);
  const [open, setOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  const [teacherList, setTeacherList] = useState([]);
  const [unAssignedList, setUnAssignedList] = useState([]);
  const [selectDropDown,setSelectDropDown] =useState([])
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)
  const [aolBranchId,setAolBranchId] = useState([])
  const [showAllTeacher,setShowAllTeacher] =useState(false)

  async function getApiCall(API, KEY) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(API);
      if (data.status_code === 200) {
        setLoading(false);
        if (KEY === 'batchList') {
          setUnAssignedList(data);
        } else {
          setTeacherList(data);
        }
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  useEffect(() => {
    if(branchId){
      const quiryDetails = new URLSearchParams(window.location.search);
      setPage(parseInt(quiryDetails.get('page'), 10) || 1);
      const SearchParm = quiryDetails.get('term')
        ? `&term=${quiryDetails.get('term')}`
        : '';
      setSearchValue(quiryDetails.get('term') || '');
      getApiCall(
        `${endpoints.aolTeachesBatchAssign.getUnAssighedList}?branch=${branchId}&page=${
          parseInt(quiryDetails.get('page'), 10) || 1
        }&page_size=${10}${SearchParm}`,
        'batchList'
      );

    }
    // getApiCall(endpoints.aolTeachesBatchAssign.getAllTeacherList, 'teacherList');
  }, [branchId]);

  useEffect(() => {
    if (selectedItem) {
      const searchValue = !showAllTeacher ? `?aol_batch_id=${aolBranchId}` : '';
      getApiCall(`${endpoints.aolTeachesBatchAssign.getAllTeacherList}${searchValue}`, 'teacherList');
    }
  }, [selectedItem,showAllTeacher]);

  function handlePagination(event, page) {
    setPage(page);
    const searchParm = searchValue ? `&term=${searchValue}` : '';
    history.push(`/aol-un-assigned-batches/?branch=${branchId}&page=${page}${searchParm}`);
    getApiCall(
      `${
        endpoints.aolTeachesBatchAssign.getUnAssighedList
      }?branch=${branchId}&page=${page}&page_size=${10}${searchParm}`,
      'batchList'
    );
  }

  const handleCheckBox=(checkValue) => {
    setShowAllTeacher(checkValue)
  }

  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      setSelectDropDown(res.data.data)
    })
  }, [])

  const ref = useRef({
    branchId:branchId
  });
  ref.current.branchId = branchId;

  const debounceFunc = useRef(
    debounce((q) => {
      setPage(1);
      const searchParm = q ? `&term=${q}` : '';
      history.push(`/aol-un-assigned-batches/?branch=${ref.current.branchId}&page=${1}${searchParm}`);
      getApiCall(
        `${
          endpoints.aolTeachesBatchAssign.getUnAssighedList
        }?branch=${ref.current.branchId}&page=${1}&page_size=${10}${searchParm}`,
        'batchList'
      );
    }, 1000)
  ).current;

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Unassigned Batches'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
      <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
              onChange={(event, value) => {
                // console.log(value,'ml999999')
                setBranchId(value?.id)
                setBranch(value)
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>

        <Grid item md={12} xs={12}>
          {/* <Grid container direction='row' justify='center' alignItems='center'> */}
            <Grid item md={6} xs={12}>
              <CustomSearchBar
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value.trimLeft());
                  debounceFunc(e.target.value.trimLeft());
                }}
                label=''
                placeholder='Search Batch Name'
                setValue={(value) => {
                  setSearchValue(value);
                  debounceFunc(value);
                }}
              />
            </Grid>
          {/* </Grid> */}
        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>S.No</TableCell>
                      <TableCell align='left'>Batch Name</TableCell>
                      <TableCell align='left'>Start Date & Time</TableCell>
                      <TableCell align='left'>End Date & Time</TableCell>
                      <TableCell align='left'>Batch Size</TableCell>
                      <TableCell align='left'>Batch Days</TableCell>
                      <TableCell align='left'>Subject</TableCell>
                      <TableCell align='left'>Course</TableCell>
                      <TableCell align="left">Enrollment Number</TableCell>
                      <TableCell align='left'>Assign</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {unAssignedList?.result?.length ? (
                      <>
                        {unAssignedList?.result.map((item, index) => (
                          <TableRow>
                            <TableCell align='left'>
                              {(page - 1) * 10 + index + 1}
                            </TableCell>
                            <TableCell align='left'>{item?.batch_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.start_date
                                ? DateTimeConverter(item?.start_date)
                                : ''}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.end_date ? DateTimeConverter(item?.end_date) : ''}
                            </TableCell>
                            <TableCell align='left'>
                              {`1 : ${item?.batch_size}` || ''}
                            </TableCell>
                            <TableCell align='left'>
                              {`${item?.days?.[0]}` || ''}
                            </TableCell>
                            <TableCell align='left'>{item?.subject_name || ''}</TableCell>
                            <TableCell align='left'>
                              {item?.is_fixed ? 'Full Year' : 'Fixed'}
                            </TableCell>
                            <TableCell align='left'>
                              {item?.enrollment_no?.map((val)=>{
                                if(val && val?.length>0){
                                  if(!val){
                                    return ""
                                  }else{
                                    return `${val},`
                                  }
                                }else{
                                  return(
                                    <Typography style={{color: 'red', fontFamily:'serif', fontWeight:600}}>NA</Typography>
                                  )
                                }
                              }) || 'NA'}
                            </TableCell>
                            <TableCell align='left'>
                              <Button
                                variant='contained'
                                color='primary'
                                size='small'
                                onClick={() => {
                                  setAolBranchId(item?.id)
                                  setSelectedItem(item);
                                  setOpen(true);
                                }}
                              >
                                Assign
                              </Button>
                            </TableCell>
                          </TableRow>
                        ))}
                        <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={unAssignedList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow>
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Batches Not Avalible' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {open ? (
        <TeacherAssignModel
          open={open}
          close={() => {
            setOpen(false);
            setSelectedItem('');
            setShowAllTeacher(false)
          }}
          selectedData={selectedItem}
          teacherList={teacherList?.data || []}
          handleRefresh={() => {
            setSelectedItem('');
            setOpen(false);
            setPage(1);
            getApiCall(
              `${
                endpoints.aolTeachesBatchAssign.getUnAssighedList
              }?branch=${branchId}&page=${page}&page_size=${10}`,
              'batchList'
            );
          }}
          handleCheckBox={handleCheckBox}
        />
      ) : (
        ''
      )}
      {loading && <Loader />}
    </Layout>
  );
};

export default AolUnAssignedBatch;
