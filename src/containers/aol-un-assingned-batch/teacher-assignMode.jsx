import React, { useContext, useState } from 'react';
import { Grid, Button, TextField, FormControlLabel,Typography,Checkbox } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import CustomDialog from '../../components/custom-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

const TeacherAssignModel = ({
  open,
  close,
  teacherList,
  selectedData,
  handleRefresh,
  handleCheckBox
}) => {
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [selectedTeacher, setSelectedTeacher] = useState('');
  const [flag , setFlag] = useState(false);
  const theme = useTheme();
  const [isActive, setIsActive] = useState(false);
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  async function AsiignTeacher() {
    if (!selectedTeacher) {
      setAlert('warning', 'Select Teacher');
      return;
    }
    if(selectedTeacher.assigned_count >= 6){
      setFlag(true);
    }else{
      handleTeacherAssign();
    }
    
    
  }
  const handleClose = () => {
    setFlag(false);
  };

  const handleChange = (event, checked) => {
    setIsActive(checked);
    handleCheckBox(checked)
    setSelectedTeacher('')
  };
  async function handleTeacherAssign(){
    setLoading(true);
    const payload = {
      teacher_id: selectedTeacher?.tutor_id,
      batch_id: selectedData?.id,
    };
    try {
      const { data } = await axiosInstance.put(
        endpoints.aolTeachesBatchAssign.assignTeacherToBatch,
        { ...payload }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        handleRefresh();
        setAlert('success', data.message);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }
  
let coma=',';
  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Assign Teacher'
        maxWidth='sm'
        fullScreen={false}
        stylesProps={{ zIndex: '1' }}
      >
        <Grid container spacing={2}>
          <Grid item md={12} xs={12}>
            <Autocomplete
              size='small'
              id='daysCombination'
              className='dropdownIcon'
              style={{ width: '100%' }}
              options={teacherList}
              getOptionLabel={(option) =>
                
                option ? `${option?.erp_id} , ${option?.name}, ${option?.num_of_classes},${option?.final_remaing_class?option.final_remaing_class :''}`  : ''}
              filterSelectedOptions
              value={selectedTeacher || ''}
              onChange={(event, value) => setSelectedTeacher(value)}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant='outlined'
                  label='Select Teacher'
                  placeholder='Select Teacher'
                />
              )}
            />
          </Grid>
          <Grid item md={12} xs={12}>
          <FormControlLabel
              control={
                <Checkbox
                  style={{color:'#ff3a67'}}
                  checked={isActive}
                  onChange={handleChange}
                  name='Show All Teachers'
                />
              }
              label={<Typography variant='h9'>Show All Teachers</Typography>}
            />
          </Grid>

          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Button variant='contained' color='primary' onClick={handleTeacherAssign}>
              Assign
            </Button>
          </Grid>
        </Grid>
      </CustomDialog>
      {flag?
      <Dialog
        fullScreen={fullScreen}
        open={flag}
        selectedTeacher={selectedTeacher}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">{"Are you sure to Assign?"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Teacher Already Assigned to {selectedTeacher.assigned_count} Batches
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
           autoFocus
            onClick={handleClose}
             color="primary"
             size='small'
           >
            Cancel
          </Button>
          <Button
           onClick={handleTeacherAssign}
           style={{ backgroundColor: '#00B792' }}
            variant='contained'
            color='primary'
            size='small'
             autoFocus
             >
            Assign
          </Button>
        </DialogActions>
      </Dialog>
      :''}
      {loading && <Loader />}
    </>
  );
};

export default TeacherAssignModel;
