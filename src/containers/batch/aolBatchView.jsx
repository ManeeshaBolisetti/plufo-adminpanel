import React, { useState, useEffect, useContext, useRef } from 'react';
import {
  Grid,
  Button,
  TableHead,
  TableCell,
  Card,
  TableRow,
  Table,
  TableBody,
  IconButton,
  Typography,
  Paper,
  InputBase,
  TextField,
  Popper,
  Box,
  Fade,
  List,
  ListSubheader,
  ListItem,
  Dialog,
  DialogTitle,
  DialogContent,
  ClickAwayListener,
  InputAdornment,
} from '@material-ui/core';
import PopupState, { bindToggle, bindPopper } from 'material-ui-popup-state';

import {
  LocalizationProvider,
  DateRangePicker,
  DateRange,
  DateRangeDelimiter,
} from '@material-ui/pickers-4.2';
import DateRangeIcon from '@material-ui/icons/DateRange';
import moment from 'moment';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import { throttle, debounce } from 'throttle-debounce';
import ListItemText from '@material-ui/core/ListItemText';
import InfoIcon from '@material-ui/icons/Info';
import { useHistory } from 'react-router-dom';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import BlockIcon from '@material-ui/icons/Block';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import useStyles from '../Layout/useStyles';
import Layout from '../Layout';
import './style.scss';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import ConfirmDialog from '../../components/confirm-dialog';
import { Autocomplete, Pagination } from '@material-ui/lab';
// import Filtration from './filtration';
// import moment from 'moment';
import ViewBatchDetails from './viewBatchDetails';
import DuoIcon from '@material-ui/icons/Duo';
// import BlockIcon from '@mui/icons-material/Block';
import { DateTimeConverter } from 'components/dateTimeConverter';
import CreateTwoToneIcon from '@material-ui/icons/CreateTwoTone';
import { CloseIcon, SearchIcon } from '@material-ui/data-grid';
import Filtration from 'containers/zoom/filtration';
import { makeStyles } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/core/styles';
import { useTimer } from 'react-compound-timer/build';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import SubstituteDialog from './substitute_dialog';
import { set } from 'lodash';
import { NoEncryption } from '@material-ui/icons';
// import { ThreeSixtyTwoTone } from '@material-ui/icons';

// const useStyles = makeStyles((theme) => ({
//   // container: {
//   //   display: 'flex',
//   //   flexWrap: 'wrap',
//   // },
//   // textField: {
//   //   marginLeft: theme.spacing(1),
//   //   marginRight: theme.spacing(1),
//   //   width: 200,
//   // },
// }));

const AolViewBatch = () => {
  const history = useHistory();
  const [batchList, setBatchList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [batchStudentRecords, setBatchStudentsRecords] = useState([]);
  const [batchInfo, setBatchInfo] = useState([]);
  const [batchZoomMeetings, setBatchZoomMeetings] = useState([]);
  const [searchedText, setSearchedText] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);
  const [globalSearchError, setGlobalSearchError] = useState(false);
  const [open, setOpen] = useState(false);
  const classes = useStyles();
  const wider = '-10px 0px 20px 8px';
  const widerWidth = '95%';
  const limit = 15;
  const searchInputRef = useRef();
  const [totalPage, setTotalPage] = useState(0);
  const [isApporved, setIsApporved] = useState('');
  const [displayUserDetails, setDisplayUserDetails] = useState(false);
  const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const [courses, setCourseData] = useState('');
  const themeContext = useTheme();
  const [totalCount, setTotalCount] = useState('');
  const [grades, setGrades] = useState('');
  const [gradeId, setGradeId] = useState('');
  const [courseId, setCourseId] = useState('');
  const [isActive, setIsActive] = useState('');
  const [filterDate, setFilterDate] = useState('');
  const [searching, setSearching] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchUserDetails, setSearchUserDetails] = useState([]);
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const [globalSearchResults, setGlobalSearchResults] = useState(false);
  const [userId, setUserId] = useState('');
  const [user, setUser] = useState('');
  const [teacherId, setTeacherId] = useState('');
  const [teacherIdApi, setTeacherIdApi] = useState('');
  const [batchId, setBatchId] = useState('');
  const [batchIdApi, setBatchIdApi] = useState('');
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deleteAlert, setDeleteAlert] = useState(false);
  const [teacherList, setTeacherList] = useState(null);
  const [selectedTeacher, setSelectedTeacher] = useState(null);
  const [teacherID, setTeacherID] = useState(null);
  const [substituteData, setSubstituteData] = useState(null);
  const [substituteList, setSubstituteList] = useState([]);
  const [active, setActive] = useState(false);
   const [anchorEl, setAnchorEl] = useState(null);
  // const [datePopperOpen, setDatePopperOpen] = useState(false);
  // const [dateRange, setDateRange] = useState([
  //   moment().subtract(1, 'weeks').startOf('week'),
  //   moment().subtract(1, 'weeks').endOf('week'),
  // ]);
  const [deleteId, setDeleteId] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [branchs, setBranches] = useState('');
  const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
  const [moduleId, setModuleId] = useState('');
  const [selectedBranch, setSelectedBranch] = useState('');
  const [selectedGrade, setSelectedGrade] = useState([]);
  const [branchId, setBranchId] = useState(null);
  const [selectedCourse, setSelectedCourse] = useState([]);
  const [gradesDisplay, setGradesDisplay] = useState([]);
  const [courseDisplay, setCourseDisplay] = useState([]);
  const [isActiveDisplay, setIsActiveDisplay] = useState([]);
  const [isApprovedDisplay,setIsApproveDisplay] = useState([]);
  const [teacherDisplay, setTeacherDisplay] = useState([]);
  // const [header, setHeader] = useState(" ");
  const id = open ? 'simple-popper' : undefined;
  const [scrollPosition, setScrollPosition] = useState(0);
 
  useEffect(() => {
    if (NavData && NavData.length) {
      NavData.forEach((item) => {
        if (
          item.parent_modules === 'Master Management' &&
          item.child_module &&
          item.child_module.length > 0
        ) {
          item.child_module.forEach((item) => {
            if (item.child_name === 'Batch List') {
              setModuleId(item.child_id);
            }
          });
        }
      });
    }
  }, []);

  const isActiveValue = [
    { id: 0, name: 'False' },
    { id: 1, name: 'True' },
  ];

  const isApporvedValue = [
    { id: 0, name: 'False' },
    { id: 1, name: 'True' },
  ];

  async function getBatchList(pageNo, userId, userType) {
    const user = userType === 'AOL_USER' ? 'student' : 'teacher';
    if (selectedBranch && !gradeId) {
      setLoading(true);
      try {
        const { data } = await axiosInstance.get(
          `${endpoints.batchApis.getBatchApi}?branch=${selectedBranch?.id}&course=${courseId}&${user && userId ? user : ''
          }=${user && userId ? userId : ''
          }&is_active=${isActive}&is_apporved=${isApporved}&meeting_date=${filterDate}&aol_batch_id=${batchIdApi}&page=${pageNo}&page_size=${limit}`
        );
        if (data) {
          setLoading(false);
          setBatchList(data);
          setTeacherId(data?.result);
          setBatchId(data?.result);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    } else if (courseId) {
      setLoading(true);
      try {
        const { data } = await axiosInstance.get(
          `${endpoints.batchApis.getBatchApi}?course=${courseId}&${user && userId ? user : ''
          }=${user && userId ? userId : ''
          }&is_active=${isActive}&is_apporved=${isApporved}&meeting_date=${filterDate}&aol_batch_id=${batchIdApi}&page=${pageNo}&page_size=${limit}`
        );
        if (data) {
          setLoading(false);
          setBatchList(data);
          setTeacherId(data?.result);
          setBatchId(data?.result);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    }
    else if (gradeId && branchs) {
      setLoading(true);
      try {
        const { data } = await axiosInstance.get(
          `${endpoints.batchApis.getBatchApi}?branch=${selectedBranch?.id}&grade_id=${gradeId}&${user && userId ? user : ''
          }=${user && userId ? userId : ''
          }&is_active=${isActive}&is_apporved=${isApporved}&meeting_date=${filterDate}&aol_batch_id=${batchIdApi}&page=${pageNo}&page_size=${limit}`
        );
        if (data) {
          setLoading(false);
          setBatchList(data);
          setTeacherId(data?.result);
          setBatchId(data?.result);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    }
    else {
      setBatchList([]);
      setTeacherId();
      setBatchId();
    }

  }

  const getGlobalUserRecords = async (text) => {
    try {
      const result = await axiosInstance.get(
        `${endpoints.gloabSearch.getUsers}?search=${text}&page=${pageNumber}&page_size=${limit}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        const tempData = [];
        result.data.data.results.map((items) =>
          tempData.push({
            id: items.id,
            name: items.name,
            erpId: items.erp_id,
            contact: items.contact,
            user: items.roles?.role_name,
          })
        );
        setTotalPage(result.data.data.total_pages);
        setSearchUserDetails(tempData);
      } else {
        setAlert('error', result.data.message);
        setGlobalSearchError(false);
      }
    } catch (error) {
      setAlert('error', error.message);
      setGlobalSearchError(false);
    }
  };

  const viewZoomDetails = (id) => {
    history.push(`/aol-view-batch-zoom-details/${id}`);
  };

  const infoBtn = (id) => {
    history.push(`/aol-view-batch-details/${id}`);
  };

  function handlePagination(event, page) {
    event.preventDefault();
    setPageNumber(page);
    getBatchList(page);
  }

  const handleGrade = (event, value) => {
    if (value) {
      setGradesDisplay(value)
      setGradeId(value.grade_id);
    }else{
      setGradesDisplay([])
      setCourseDisplay([])
      setIsActiveDisplay([])
      setIsApproveDisplay([])
      setTeacherDisplay([])
    }
  };

  const handleBatchId = (event, value) => {
    if (value) {
      setBatchIdApi(value?.id);
    } else {
      setBatchIdApi();
      setAlert('warning', 'Please Select the Batch');
    }
  };

  const handleTeacher = (event, value) => {
    if (value) {
      setTeacherDisplay(value);
      setTeacherIdApi(value?.teacher?.teacher_id);
    } else {
      setTeacherIdApi('');
      setAlert('warning', 'Please Select the Teacher');
    }
  };

  const autocompleteSearch = (q, pageId, isDelete) => {
    if (q !== '') {
      setSearching(true);
      setGlobalSearchResults(true);
      getGlobalUserRecords(q);
    }
  };
  const handleCourse = (event, value) => {
    if (value) {
      setCourseDisplay(value)
      setCourseId(value.id);
    }
  };


  const autocompleteSearchDebounced = debounce(500, autocompleteSearch);
  const autocompleteSearchThrottled = throttle(500, autocompleteSearch);

  const changeQuery = (event) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
    setSearchedText(event.target.value);
    if (event.target.value === '') {
      setGlobalSearchResults(false);
      setSearching(false);
      setSearchUserDetails([]);
      setTotalPage(0);
      setCurrentPage(1);
      return;
    }
    const q = event.target.value;
    if (q.length < 5) {
      setCurrentPage(1);
      autocompleteSearchThrottled(event.target.value);
    } else {
      setCurrentPage(1);
      autocompleteSearchDebounced(event.target.value);
    }
  };

  const handleTextSearchClear = (e) => {
    setAnchorEl(anchorEl ? null : e.currentTarget);
    e.preventDefault();
    setTimeout(() => {
      setSearchedText('');
      setGlobalSearchResults(false);
      setSearching(false);
      setSearchUserDetails([]);
      setTotalPage(0);
      setCurrentPage(1);
    }, 500);
  };

  const handleIsActive = (event, value) => {
    if (value) {
      setIsActiveDisplay(value);
      setIsActive(value.name);
    } else {
      setIsActive('');
    }
  };

  const handleDate = (value) => {
    setFilterDate(value.target.value);
  };

  const handleUserType = (userType, userId) => {
    if (userType && userId) {
      // setUser(userType);
      // setUserId(userId);
      getBatchList(1, userId, userType);
    } else {
      setUser('');
      setUserId('');
    }
  };

  const handleIsApporved = (event, value) => {
    if (value) {
      setIsApproveDisplay(value)
      setIsApporved(value.name);
    } else {
      setIsApporved('');
    }
  };

  useEffect(() => {
    if (selectedBranch) {
      axiosInstance
        .get(`${endpoints.academics.grades}?branch_id=${selectedBranch?.id}`)
        .then((result) => {
          if (result.data.status_code === 200) {
            // setTotalCount(result.data.result.count);
            setGrades(result?.data?.data);
            // setActive(result.data.result.results);
          } else {
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setAlert('error', error.message);
        });
    }

  }, [selectedBranch]);

  useEffect(() => {
    if (grades.length !== 0 && gradeId) {
      axiosInstance
        .get(
          `${endpoints.onlineCourses.courseList}?grade=${gradeId}&page=${pageNumber}&page_size=${limit}`
        )
        .then((result) => {
          if (result.data.status_code === 200) {
            setTotalCount(result.data.count);
            setLoading(false);
            setCourseData(result.data.result);
          } else {
            setLoading(false);
            setAlert('error', result.data.description);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
  }, [gradeId]);

 
  // useEffect(() => {
  //   getSubstituteList();
  // }, [isActive, batchIdApi]);

  useEffect(() => {
    getBatchList(1);
  }, [branchId,gradeId, selectedBranch, courseId, isActive, isApporved, filterDate, teacherIdApi]);
  

  const getSubstituteList = () => {
    setLoading(true);
    axiosInstance
      .get(`${endpoints.teacherSubstitution.createSubstitution}`, {})
      .then((res) => {
        setLoading(false);
        setSubstituteList(res.data);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err, 'resultaaaa 11');
      });
    // setDialogOpen(false);
  };

  const handleDelete = (id) => {
    // setDeleteAlert(true);
    axiosInstance
      .delete(`${endpoints.teacherSubstitution.createSubstitution}${id}/`, {})
      .then((res) => {
        console.log('deleted');
        getSubstituteList();
      })
      .catch((err) => {
        console.log(err, 'resultaaaa 11');
      });
    // setDialogOpen(false);
  };

  const handleDeleteInfo = () => {
    handleDelete(deleteId);
    setDeleteAlert(false);
  };

  const handleToggle = (e) => {
    if (e.target.value === 'false') {
      setActive(true);
      getSubstituteList();
    } else {
      setActive(false);
      getBatchList(1);
    }
  };
  // const handleDeactivateMessage = (activate) => {
  //   if (activate) {
  //     setAlert('success', 'Batch cannot be substituted for deactivated batches');
  //   }
  // };

  const handleBranchNew = (event, value) => {
    if (value) {
      setSelectedBranch(value);
      setBranchId(value?.id)
    }
    else {
      setBatchList([])
      setGradesDisplay([])
      setCourseDisplay([])
      setIsActiveDisplay([])
      setIsApproveDisplay([])
      setTeacherDisplay([])
      history.go()
    }

  }

  const getBranchApi = async () => {
    try {
      const result = await axiosInstance.get(`${endpoints.communication.branches}?module_id=${moduleId}`);
      if (result.status === 200) {
        setBranches(result.data.data);
      } else {
        setAlert('error', result.data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
    }
  };
  useEffect(() => {
    getBranchApi()
  }, []);

  function getCourses() {
    axiosInstance.get(
      `${endpoints.teacherViewBatches.courseListApi}?all=True`).then((result) => {
        console.log('result', result);
        if (result.data.status_code === 200) {
          setCourseData(result?.data?.result || []);
        }
      })
  }
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
  };
console.log(window.pageYOffset)
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  useEffect(() => {
    if (!gradeId) {
      getCourses();
    }
  }, []);
  console.log(scrollPosition);
//   const listenScrollEvent = () =>{
//     if(window.scrollY >= 300){
//        setHeader("none");
//        console.log('none')
//     }
//     else{
//      setHeader("flex");
//     }
//  };
//  window.addEventListener('scroll', listenScrollEvent);
  const closeDialog = () => {
    setDialogOpen(false);
  };
  return (
    <>
     <PopupState variant="popper" popupId="demo-popup-popper">
      {(popupState) => (
      <Layout >
        <Grid container spacing={2} className='plufo-zoom-id-generator-body'  >
          <Grid
            item
            container
            md={12}
            xs={12}
            className='plufo-zoom-id-generator-top-head'
          >
            <Grid md={8}>
              <CommonBreadcrumbs
                componentName='Master Management'
                childComponentName='Batch List'
              />
            </Grid>
            <Grid md={4}>
           
              <Paper component='form' className={classes.searchInputContainer}>
                <InputBase
                  aria-describedby={id}
                  value={searchedText}
                  className={classes.searchInput}
                  placeholder='Search..'
                  inputProps={{ 'aria-label': 'search across site' }}
                  inputRef={searchInputRef}
                  onChange={changeQuery}
                  onBlur={handleTextSearchClear}
                />
                {searchedText ? (
                  <IconButton
                    type='submit'
                    className={classes.clearIconButton}
                    aria-label='close'
                    onClick={handleTextSearchClear}
                  >
                    <CloseIcon />
                  </IconButton>
                ) : null}
                <IconButton
                  type='submit'
                  className={classes.searchIconButton}
                  aria-label='search'
                >
                  <SearchIcon />
                </IconButton>
              </Paper>

              <Popper 
              id={id} open={searching} anchorEl={anchorEl}
              //   // open={searching}
              //   // anchorEl={anchorEl}
                className={` ${classes.searchDropdown} ${isMobile ? classes.searchDropdownMobile : 'null'
                  }`}
                placement='bottom'
                style={{
                 
                 
                  display:scrollPosition> 300 ? 'none': 'flex',
                  top: isMobile
                    ? searchInputRef.current &&
                    searchInputRef.current.getBoundingClientRect().top + 44
                    : searchInputRef.current &&
                    searchInputRef.current.getBoundingClientRect().top ,
                  left: 'auto',
                  right: `calc(${isMobile ? '92vw' : '100vw'} - ${searchInputRef.current &&
                    searchInputRef.current.getBoundingClientRect().left +
                    searchInputRef.current.getBoundingClientRect().width -50
                    }px)`,
                  zIndex: 3000,
                }}
                transition
              >
                {({ TransitionProps }) => (
                  <Fade {...TransitionProps} timeout={350}>
                    <Paper>
                      <Grid
                        container
                        className='main_search_container'
                        style={{ flexDirection: 'column' }}
                      >
                        {globalSearchResults && searchUserDetails.length ? (
                          <>
                            <Grid item>
                             
                              <Grid
                                container
                                style={{
                                  
                                  flexDirection: 'row',
                                  paddingBottom: 12,
                                  paddingTop: 12,
                                  paddingLeft: 16,
                                  backgroundColor: 'rgb(224 224 224)',
                                  // backgroundColor: 'transparent',
                                  paddingRight: 16,
                                  minWidth: 374,
                                }}
                               
                              >
                                <Grid
                                  // onScroll={(event) => handleScroll(event)}
                                  style={{
                                    paddingRight: 8,
                                    maxHeight: 385,
                                    height: 300,
                                    overflow: 'auto',
                                  }}
                                  item
                                >
                                  {globalSearchResults && (
                                    <List
                                      style={{ minWidth: 61 }}
                                      subheader={
                                        <ListSubheader
                                          style={{
                                            background: 'rgb(224 224 224)',
                                            width: '100%',
                                            color: '#014B7E',
                                            fontSize: '1rem',
                                            fontWeight: 600,
                                          }}
                                        >
                                          Users
                                        </ListSubheader>
                                      }
                                    >
                                      {globalSearchResults &&
                                        searchUserDetails.length &&
                                        searchUserDetails.map((result, index) => {
                                          return (
                                            <ListItem
                                              style={{ width: 324 }}
                                              className='user_rows_details'
                                              button
                                              onClick={() => {
                                                setSearching(false);
                                                setUser(result.user);
                                                setUserId(result.id);
                                                setDisplayUserDetails(false);
                                                handleUserType(result.user, result.id);
                                              }}
                                            >
                                              <ListItemText
                                                primary={result.name}
                                                secondary={
                                                  <div>
                                                    <span>{result.erpId}</span>
                                                    <span style={{ marginLeft: '10px' }}>
                                                      {result.user === 'AOL_USER'
                                                        ? 'Student'
                                                        : 'Teacher'}
                                                    </span>
                                                    <span style={{ float: 'right' }}>
                                                      Mob: {result.contact}
                                                    </span>
                                                  </div>
                                                }
                                              />
                                              {/* <ListItemSecondaryAction>
                                              <IconButton
                                                aria-label='Delete'
                                                onClick={() =>
                                                  handleUserDelete(result.id, index)
                                                }
                                                className={classes.margin}
                                              >
                                                <DeleteIcon fontSize='small' />
                                              </IconButton>
                                            </ListItemSecondaryAction> */}
                                            </ListItem>
                                          );
                                        })}
                                    </List>
                                  )}
                                </Grid>
                              </Grid>
                            </Grid>
                          </>
                        ) : (
                          <Grid
                            container
                            style={{
                              flexDirection: 'row',
                              backgroundColor: '#eee',
                              minHeight: 324,
                              minWidth: 374,
                              flexGrow: 1,
                            }}
                          >
                            <span
                              style={{
                                padding: 1,
                                textAlign: 'center',
                                margin: 'auto',
                                color: '#014B7E',
                              }}
                            >
                              No data available.
                            </span>
                          </Grid>
                        )}
                      </Grid>
                      <Grid container>
                        {globalSearchError && (
                          <Grid
                            style={{ padding: 8, width: '100%', backgroundColor: '#eee' }}
                            xs={12}
                            item
                          >
                            Something went wrong.
                          </Grid>
                        )}
                      </Grid>
                    </Paper>
                  </Fade>
                )}
              </Popper>
            </Grid>
          </Grid>
          {active === false ? (
            <>
              <Grid item md={3} xs={3} sm={3}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange ={handleBranchNew}
                  id='branch_id'
                  options={branchs}
                  // value={branchsDisplay}
                  getOptionLabel={(option) => option?.branch_name}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Branch'
                      placeholder='Branch'
                    />
                  )}
                />
              </Grid>
              {selectedBranch ? (
                <Grid item md={3} xs={3} sm={3}>
                  <Autocomplete
                    style={{ width: '100%' }}
                    size='small'
                    onChange={handleGrade}
                    id='grade'
                    options={grades}
                    value={gradesDisplay}
                    getOptionLabel={(option) => option?.grade__grade_name}
                    filterSelectedOptions
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant='outlined'
                        label='Grades'
                        placeholder='Grades'
                      />
                    )}
                  />
                </Grid>
              ) : (
                ''
              )}

              <Grid item md={3} xs={12} sm={3}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={handleCourse}
                  id='Course'
                  options={courses || ''}
                  value={courseDisplay}
                  getOptionLabel={(option) => option?.course_name}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Course'
                      placeholder='Course'
                    />
                  )}
                />
              </Grid>


              <Grid item xs={12} sm={3}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={handleIsActive}
                  id='Is Active'
                  options={isActiveValue}
                  value={isActiveDisplay}
                  getOptionLabel={(option) => option?.name}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Is Active'
                      placeholder='Is Active'
                    />
                  )}
                />
              </Grid>

              <Grid item md={3} xs={12} sm={3}>
                <Autocomplete
                  // style={{ width: '100%' }}
                  size='small'
                  onChange={handleIsApporved}
                  id='Is Apporved'
                  options={isApporvedValue}
                  value={isApprovedDisplay}
                  getOptionLabel={(option) => option?.name}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Is Apporved'
                      placeholder='Is Apporved'
                    />
                  )}
                />
              </Grid>
              {/* {console.log(isApporved,'approve')} */}

              {isApporved === 'True' ? (
                <Grid item md={3} xs={12} sm={3}>
                  <Autocomplete
                    style={{ width: '100%' }}
                    size='small'
                    onChange={handleTeacher}
                    id='teacher'
                    options={teacherId || ''}
                    value={teacherDisplay}
                    getOptionLabel={(option) => option?.teacher?.teacher_name}
                    filterSelectedOptions
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant='outlined'
                        label='Teacher'
                        placeholder='Teacher'
                      />
                    )}
                  />
                </Grid>
              ) : (
                ''
              )}
              <Grid item xs={12} md={3}>
                <form noValidate>
                  <TextField
                    id='date'
                    label='Select Date'
                    type='date'
                    value={filterDate}
                    onChange={handleDate}
                    className={classes.textField}
                    style={{ marginTop: '-10px' }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </form>
              </Grid>
            </>
          ) : (
            ''
          )}
          <Grid item md={1} xs={12} sm={2}>
            <FormGroup>
              <FormControlLabel
                control={<Switch />}
                value={active}
                style={{ minWidth: "214px" }}
                onChange={(e) => {
                  handleToggle(e);
                  // getSubstituteList();
                  // console.log(active, 'toggle');
                }}
                label='Substitute Classes'
              />
            </FormGroup>
          </Grid>
          {/* <Grid
      container
      spacing={ 5}
      style={{ width: widerWidth, margin: wider }}
    >

    <form className={classes.root} noValidate autoComplete="off">
      <div>
      <Grid item xs={12} sm={3} >
      <TextField required id="standard-required" label="Required" defaultValue="Hello World" />
      </Grid>

      </div>
      </form>

            
            </Grid> */}
          {/* <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
              <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
                <Filtration
                  setPageNumber={setPageNumber}
                  getZoomIdList={getZoomIdList}
                  filterState={filterState}
                  setFilterState={setFilterState}
                  setLoading={setLoading}
                />
              </Grid>
            </Grid> */}
          {/* <Grid item md={12} xs={12} className='plufo-zoom-id-generator-top-head1'>
              <Button
                variant='contained'
                color='primary'
                size='small'
                onClick={() => getBatchList()}
              >
                <AddCircleOutlineIcon />
                &nbsp;Add Zoom Id
              </Button>
            </Grid> */}

          {active === true ? (
            <Grid item md={12} xs={12} className='plufo-zoom-id-generator-table-div'>
              <Card className='plufo-zoom-id-generator-table-card'>
                {/* { (
                  <Typography
                    variant='h6'
                    color='primary'
                    style={{ textAlign: 'center', margin: '20px 0px' }}
                  >
                    ZoomId's Not Created
                  </Typography>
                )} */}
                {substituteList && substituteList && substituteList.length === 0 ? (
                  <Grid item md={3} xs={12}>
                    <Typography color='primary' variant='h6' style={{ margin: '10px' }}>
                      <strong>No Records Found</strong>
                    </Typography>
                  </Grid>
                ) : (
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell float='left'>S.No</TableCell>
                        <TableCell float='left'>Branch</TableCell>
                        <TableCell float='left'>Batch Name</TableCell>
                        <TableCell float='left'>Batch Days</TableCell>
                        <TableCell float='left'>Batch Time Slot</TableCell>
                        <TableCell float='left'>Seat Left : Batch Size</TableCell>
                        {/* <TableCell float='left'> : </TableCell> */}

                        {/* <TableCell float='left'> Batch Size</TableCell> */}

                        <TableCell float='left'>Teacher Name</TableCell>
                        <TableCell float='left'>Substitution Start Date</TableCell>
                        <TableCell float='left'>Substitution End Date</TableCell>
                        <TableCell float='left'>Is Active</TableCell>

                        <TableCell float='left'>Cancel</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {substituteList &&
                        substituteList &&
                        substituteList.length !== 0 &&
                        substituteList
                          // .filter((item) => item.is_active !== false)
                          .map((item, index) => (
                            <TableRow key={item.id}>
                              <TableCell float='left'>{index + 1}</TableCell>
                              <TableCell float='left'>{selectedBranch?.branch_name}</TableCell>
                              <TableCell float='left'>{item.batch_name}</TableCell>
                              <TableCell float='left'>{item.batch_days}</TableCell>
                              <TableCell float='left'>{item.batch_time_slot}</TableCell>
                              {/* <TableCell float='left'>{`${item?.seat_left}` || ''}</TableCell> */}
                              {/* <TableCell float='left'>:</TableCell> */}

                              <TableCell float='left'>
                                {`${item?.seat_left}:${item?.batch_size}` || ''}
                              </TableCell>
                              <TableCell float='left'>
                                {item?.teacher_name || ''}
                              </TableCell>
                              <TableCell float='left'>
                                {item.substitution_start_date}
                              </TableCell>
                              <TableCell float='left'>
                                {item.substitution_end_date}
                              </TableCell>
                              <TableCell>
                                {item.is_active ? (
                                  <div style={{ color: 'green' }}>Activated</div>
                                ) : (
                                  <div style={{ color: 'red' }}>Deactivated</div>
                                )}
                              </TableCell>
                              <TableCell float='left'>
                                <Button
                                  variant='primary'
                                  size='small'
                                  className='BatchViewfilterButtons'
                                  onClick={() => {
                                    setDeleteId(item.id);
                                    setDeleteAlert(true);
                                    // handleDelete(item.id);
                                  }}
                                >
                                  <BlockIcon />
                                </Button>
                              </TableCell>
                              {/* <TableCell float='left'>{`${item?.seat_left}` || ''}</TableCell> */}
                              {/* <TableCell float='left'>:</TableCell> */}

                              {/* <TableCell float='left'>
                              {`${item?.seat_left}:${item?.batch_size}` || ''}
                            </TableCell>
                            <TableCell float='left'>
                              {item?.teacher?.teacher_name || ''}
                            </TableCell>
                            <TableCell float='left'>
                              {DateTimeConverter(item.start_date)}
                            </TableCell>
                            <TableCell float='left'>
                              {DateTimeConverter(item.end_date)}
                            </TableCell>
                            <TableCell>
                              {item.is_active ? (
                                <div style={{ color: 'green' }}>Activated</div>
                              ) : (
                                <div style={{ color: 'red' }}>Deactivated</div>
                              )}
                            </TableCell>

                            <TableCell float='left'>
                              <Button
                                variant='primary'
                                size='small'
                                className='BatchViewfilterButtons'
                                onClick={() => infoBtn(item.id)}
                              >
                                <InfoIcon />
                              </Button>
                            </TableCell>
                            <TableCell float='left'>
                              <Button
                                variant='primary'
                                size='small'
                                className='BatchViewfilterButtons'
                                onClick={
                                  () => {
                                    setSubstituteData(item.id);
                                    setDialogOpen(true);
                                    setIsActive(item.is_active);
                                  }
                                  // infoBtn(item.id)
                                }
                              >
                                <CreateTwoToneIcon />
                              </Button>
                            </TableCell>

                            <TableCell float='left'>
                              <Button
                                variant='primary'
                                size='small'
                                className='BatchViewfilterButtons'
                                onClick={() => viewZoomDetails(item.id)}
                              >
                                <DuoIcon />
                              </Button>
                            </TableCell> */}
                            </TableRow>
                          ))}
                      <TableRow>
                        <TableCell colSpan='8'>
                          {substituteList &&
                            substituteList &&
                            substituteList.length !== 0 && (
                              // <Paper style={{ textAlign: 'center', padding: '10px' }}>
                              <Pagination
                                style={{
                                  textAlign: 'center',
                                  margin: '0 0 0 260px',
                                  display: 'inline-flex',
                                }}
                                onChange={handlePagination}
                                count={substituteList.total_pages}
                                color='primary'
                                page={pageNumber}
                              />
                              // </Paper>
                            )}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                )}
              </Card>
            </Grid>
          ) : (
            <Grid item md={12} xs={12} className='plufo-zoom-id-generator-table-div'>
              <Card className='plufo-zoom-id-generator-table-card'>
                {/* { (
                  <Typography
                    variant='h6'
                    color='primary'
                    style={{ textAlign: 'center', margin: '20px 0px' }}
                  >
                    ZoomId's Not Created
                  </Typography>
                )} */}
                {batchList && batchList.result && batchList.result.length === 0 ? (
                  <Grid item md={3} xs={12}>
                    <Typography color='primary' variant='h6' style={{ margin: '10px' }}>
                      <strong>No Records Found</strong>
                    </Typography>
                  </Grid>
                ) : (
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell float='left'>S.No</TableCell>
                        <TableCell float='left'>Branch</TableCell>
                        <TableCell float='left'>Batch Name</TableCell>
                        <TableCell float='left'>Batch Days</TableCell>
                        <TableCell float='left'>Batch Time Slot</TableCell>
                        <TableCell float='left'>Seat Left : Batch Size</TableCell>
                        {/* <TableCell float='left'> : </TableCell> */}

                        {/* <TableCell float='left'> Batch Size</TableCell> */}

                        <TableCell float='left'>Teacher Name</TableCell>
                        <TableCell float='left'>Start Date</TableCell>
                        <TableCell float='left'>End Date</TableCell>
                        <TableCell float='left'>Is Active</TableCell>

                        <TableCell float='left'>View More</TableCell>
                        <TableCell float='left'>Substitute Class</TableCell>
                        <TableCell float='left'>Zoom Details</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {batchList &&
                        batchList.result &&
                        batchList.result.length !== 0 &&
                        batchList.result
                          // .filter((item) => item.is_active !== false)
                          .map((item, index) => (
                            <TableRow key={item.id}>
                              <TableCell float='left'>{index + 1}</TableCell>
                              <TableCell float='left'>{selectedBranch?.branch_name}</TableCell>
                              <TableCell float='left'>{item.batch_name}</TableCell>
                              <TableCell float='left'style={{maxWidth: '30px', wordWrap:'break-word', fontWeight:'100px'}} >{item.batch_days}</TableCell>
                              <TableCell float='left'>{item.batch_time_slot}</TableCell>
                              {/* <TableCell float='left'>{`${item?.seat_left}` || ''}</TableCell> */}
                              {/* <TableCell float='left'>:</TableCell> */}

                              <TableCell float='left'>
                                {`${item?.seat_left}:${item?.batch_size}` || ''}
                              </TableCell>
                              <TableCell float='left' style={{ maxWidth: '22px', wordWrap: 'break-word' }}>
                                {item?.teacher?.teacher_name || ''}
                              </TableCell>
                              <TableCell float='left'>
                                {DateTimeConverter(item.start_date)}
                              </TableCell>
                              <TableCell float='left'>
                                {DateTimeConverter(item.end_date)}
                              </TableCell>
                              <TableCell>
                                {item.is_active ? (
                                  <div style={{ color: 'green' }}>Activated</div>
                                ) : (
                                  <div style={{ color: 'red' }}>Deactivated</div>
                                )}
                              </TableCell>

                              <TableCell float='left'>
                                <Button
                                  variant='primary'
                                  size='small'
                                  className='BatchViewfilterButtons'
                                  onClick={() => infoBtn(item.id)}
                                >
                                  <InfoIcon />
                                </Button>
                              </TableCell>
                              <TableCell float='left'>
                                <Button
                                  variant='primary'
                                  size='small'
                                  className='BatchViewfilterButtons'
                                  disabled={item.is_active === false}
                                  onClick={
                                    () => {
                                      setSubstituteData(item.id);
                                      setDialogOpen(true);
                                      setIsActive(item.is_active);
                                    }
                                    // infoBtn(item.id)
                                  }
                                >
                                  <CreateTwoToneIcon />
                                </Button>
                              </TableCell>

                              <TableCell float='left'>
                                <Button
                                  variant='primary'
                                  size='small'
                                  className='BatchViewfilterButtons'
                                  onClick={() => viewZoomDetails(item.id)}
                                >
                                  <DuoIcon />
                                </Button>
                              </TableCell>
                            </TableRow>
                          ))}
                      <TableRow>
                        <TableCell colSpan='8'>
                          {batchList &&
                            batchList.result &&
                            batchList.result.length !== 0 && (
                              // <Paper style={{ textAlign: 'center', padding: '10px' }}>
                              <Pagination
                                style={{
                                  textAlign: 'center',
                                  margin: '0 0 0 260px',
                                  display: 'inline-flex',
                                }}
                                onChange={handlePagination}
                                count={batchList.total_pages}
                                color='primary'
                                page={pageNumber}
                              />
                              // </Paper>
                            )}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                )}
              </Card>
            </Grid>
          )}
        </Grid>
        {loading && <Loading />}
      </Layout>)}
      </PopupState>
      <SubstituteDialog
        closeDialog={closeDialog}
        dialogOpen={dialogOpen}
        setDialogOpen={setDialogOpen}
        isActive={isActive}
        substituteData={substituteData}
      />
      
      {deleteAlert ? (
        <ConfirmDialog
          open={deleteAlert}
          cancel={() => setDeleteAlert(false)}
          confirm={handleDeleteInfo}
          title='Are you sure you want to delete ?'
        />
      ) : (
        ''
      )}
    </>
  );
};

export default AolViewBatch;
