import React, { useState, useEffect, useContext } from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  MenuItem,
  DialogContent,
} from '@material-ui/core';
import moment from 'moment';
import { Autocomplete, Pagination } from '@material-ui/lab';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import { CloseIcon, SearchIcon } from '@material-ui/data-grid';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import './style.scss';
const SubstituteDialog = ({
  // closeDialog,
  dialogOpen,
  isActive,
  setDialogOpen,
  substituteData,
}) => {
  const [teacherList, setTeacherList] = useState(null);
  const [selectedTeacher, setSelectedTeacher] = useState(null);
  const [teacherID, setTeacherID] = useState(null);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const { setAlert } = useContext(AlertNotificationContext);
  const handleModalTeacher = (event, value) => {
    setSelectedTeacher(value);
    setTeacherID(value.tutor_id);
  };

  useEffect(() => {
    if (substituteData) {
      getTeacherList();
    }
  }, [substituteData]);

  const getTeacherList = () => {
    axiosInstance
      .get(`${endpoints.aolTeachesBatchAssign.getAllTeacherList}`, {})
      .then((res) => {
        setTeacherList(res.data.data);
      })
      .catch((err) => {
        console.log(err, 'resultaaaa 11');
      });
    // setDialogOpen(false);
  };

  const confirm = () => {
    // console.log(teacherID, 'hh');
    axiosInstance
      .post(endpoints.teacherSubstitution.createSubstitution, {
        // .post(`https://dev.plufo.letseduvate.com/qbox/course_extend/weekly_report/`, {
        substitution_start_date: startDate,
        substitution_end_date: endDate,
        teacher: teacherID,
        is_active: isActive,
        aol_batch_id: substituteData,
      })
      .then((res) => {
        if (res.data.status_code === 200) {
          setAlert('success', res.data.message);
        }
      })
      .catch((error) => {
        setAlert('error', error.response.data.message);
      });
    setDialogOpen(false);
  };

  const closeDialog = () => {
    setStartDate('');
    setEndDate('');
    setSelectedTeacher(null);
    setDialogOpen(false);
  };

  console.log(startDate === '' ? true : false, endDate, 'ffssss');

  return (
    <div>
      <Dialog
        open={dialogOpen}
        style={{ margin: '35px 0 0 0' }}
        className='create-weekly-report-dialog'
      >
        <DialogTitle className='dialog-title'>
          {' '}
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Typography variant='h6'>Substitute Classes</Typography>
            <Button
              className='substitute-dialog-actions-butons'
              onClick={() => closeDialog()}
            >
              <CloseIcon />
            </Button>
          </div>
        </DialogTitle>
        <DialogContent
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <TextField
            style={{ width: '66%' }}
            required
            label='Start Date'
            variant='outlined'
            margin='dense'
            size='small'
            autoComplete='off'
            name='startDate'
            value={startDate}
            onChange={(e) => setStartDate(moment(e.target.value).format('YYYY-MM-DD'))}
            // onChange={(e) =>
            //   setSessionYear({
            //     ...sessionYear,
            //     startDate: moment(e.target.value).format('YYYY-MM-DD'),
            //   })
            // }
            type='date'
            InputProps={{ inputProps: { min: new Date().toISOString().split('T')[0] } }}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            style={{ width: '66%' }}
            required
            label='End Date'
            variant='outlined'
            margin='dense'
            size='small'
            autoComplete='off'
            name='endDate'
            value={endDate}
            InputProps={{ inputProps: { min: new Date().toISOString().split('T')[0] } }}
            onChange={(e) => setEndDate(moment(e.target.value).format('YYYY-MM-DD'))}
            // onChange={(e) =>
            //   setSessionYear({
            //     ...sessionYear,
            //     endDate: moment(e.target.value).format('YYYY-MM-DD'),
            //   })
            // }
            type='date'
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Grid item md={8} xs={12} sm={8}>
            {/* {console.log(teacherList,)} */}
            <Autocomplete
              fullWidth
              size='small'
              className='filter-student weekly-report-input'
              options={teacherList || []}
              getOptionLabel={(option) => option?.name}
              filterSelectedOptions
              // value={selectedGrade || ''}
              // onChange={(event, value) => {
              //   setGradeID(value.id);
              //   // setSelectedStudent(value);
              // }}
              value={selectedTeacher || ''}
              onChange={handleModalTeacher}
              renderInput={(params) => (
                <TextField
                  {...params}
                  required
                  fullWidth
                  variant='outlined'
                  label='Teacher'
                  // error={helperText.courseListError}
                />
              )}
            />
          </Grid>
          <Button
            style={{ width: 'fit-content', margin: '5px auto' }}
            className='substitute-dialog-actions-butons'
            onClick={() => confirm()}
            disabled={selectedTeacher === null || startDate === '' || endDate === ''}
          >
            Confirm
          </Button>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default SubstituteDialog;
