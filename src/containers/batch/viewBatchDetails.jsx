import React, { useState, useEffect, useContext } from 'react';
import CustomDialog from '../../components/custom-dialog';
import Loader from '../../components/loader/loader';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import PropTypes from 'prop-types';
import Loading from '../../components/loader/loader';
import viewBatchDetailsStyles from './viewBatchDetails.styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  withStyles,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
  IconButton,
  Typography,
  Modal,
  DialogTitle,
  Paper,
  TableContainer,
} from '@material-ui/core';
import { CloseIcon } from '@material-ui/data-grid';
import { makeStyles } from '@material-ui/core/styles';
import styles from './viewBatchDetails.styles';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import { useHistory, useParams } from 'react-router-dom';
import Layout from 'containers/Layout';
import CommonBreadcrumbs from 'components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import InfoIcon from '@material-ui/icons/Info';
import DuoIcon from '@material-ui/icons/Duo';
import { DateTimeConverter } from 'components/dateTimeConverter';
import ViewStudentBatchDetails from './ViewStudentBatchDetails';


const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  studentZoomDetailsBox: {
    padding:'1px',
  },
});

const ViewBatchDetails = ({ match }) => {
  const classes = useStyles();
  const history = useHistory();
  const [batchStudentRecords, setBatchStudentsRecords] = useState([]);
  const [batchInfo, setBatchInfo] = useState([]);
  const [index, setIndex] = useState('');
  const [zoomDetailsOpen, setZoomDetailsOpen] = useState(false);
  const [zoomIndex, setZoomIndex] = useState('');

  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [thumbnail, setThumbnail] = useState('');
  const [open, setOpen] = useState(false);

  const batchId = match.params.id;

  const value = useParams();

  console.log('IDDDD', value);

  async function getBatchInfo(id) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.batchApis.getBatchInfo}${id}/`
      );
      if (data) {
        setLoading(false);
        setBatchInfo(data);
        setThumbnail(data.course.thumbnail[0]);
        console.log('data.course.thumbnail[0]', data?.course);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  async function getBatchStudentsRecords(id) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.batchApis.getBatchStudentsRecords}${id}/`
      );
      if (data) {
        setLoading(false);
        setBatchStudentsRecords(data);
        console.log('data::::::::', data);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  const viewDetails = (index) => {
    setOpen(true);
    setIndex(index);
  };

  const viewStudentZoomDetails = (index) => {
    setZoomDetailsOpen(true);
    setZoomIndex(index);
  };

  const handleClose = () => {
    setOpen(false);
    setZoomDetailsOpen(false);
  };

  useEffect(() => {
    getBatchInfo(batchId);
    getBatchStudentsRecords(batchId);
  }, []);

  return (
    <Layout>
      <div style={{ width: '95%', margin: '30px auto' }}>
        <CommonBreadcrumbs componentName='Batch' childComponentName='Batch Details' />
      </div>

      <Grid item md={12} xs={12} justify='center'>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell float='left'>S.No</TableCell>
              <TableCell float='left'>Student</TableCell>
              <TableCell float='left'>Contact</TableCell>
              <TableCell float='left'>Start Date</TableCell>
              <TableCell float='left'>End Date</TableCell>
              <TableCell float='left'>View Details</TableCell>
              <TableCell float='left'>Zoom Details</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {batchStudentRecords &&
              batchStudentRecords.map((data, index) => (
                <TableRow>
                  <TableCell float='left'>{index + 1}</TableCell>
                  <TableCell float='left'>{data.user.name}</TableCell>
                  <TableCell float='left'>{data.user.contact}</TableCell>
                  <TableCell float='left'>{DateTimeConverter(data.start_date)}</TableCell>
                  <TableCell float='left'>{DateTimeConverter(data.end_date)}</TableCell>
                  <TableCell float='left'>
                    <Button 
                    className='BatchViewfilterButtons'
                    variant='primary'
                    size='small'
                    onClick={() => viewDetails(index)}>
                      <InfoIcon/>
                    </Button>
                  </TableCell>
                  <TableCell float='left'>
                    <Button
                      variant='primary'
                      size='small'
                      className='BatchViewfilterButtons'
                      onClick={() => viewStudentZoomDetails(index)}
                    >
                      <DuoIcon />
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>

        <Card
          style={{
            borderRadius: '10px',
            padding: '20px',
            border: '1px solid lightgray',
            width: '98%',
            marginLeft: '10px',
          }}
        >
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Grid container spacing={2}>
                <Grid item md={6} xs={6} style={{ textAlign: 'left' }}>
                  <Typography variant='h6' color='primary'>
                    <strong>Batch Course Details</strong>
                  </Typography>
                </Grid>
                <Grid item md={12} xs={12}>
                  <Divider />
                </Grid>
                {/* <Grid
                    container
                    spacing={2}
                    style={{
                      padding: '10px',
                      borderRadius: '10px',
                      border: '1px solid #FF6B6B',
                      width: '100%',
                      margin: '5px 0px',
                    }}
                    justify='space-between'
                    alignItems='center'
                    direction='row'
                  >
                    {thumbnail?<img
                    src={`${endpoints.batchApis.imageS3List}${batchInfo?.course?.thumbnail?.[0]}`}
                    alt='crash'
                    height='200px'
                    width='100%'
                    loading='lazy'
                    style={{ imageRendering: 'pixelated', objectFit: 'cover' }}
            />:""}
                  </Grid> */}

                <Grid item md={12}>
                  <Grid item md={12} xs={12}>
                    <Grid container spacing={2}>
                      <Grid item md={6} xs={12}>
                        <Typography>
                          <strong>Course Name :&nbsp;</strong>
                          {batchInfo?.course?.course_name || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={10} xs={12}>
                        <Typography>
                          <strong>Learn:&nbsp;</strong>
                          {batchInfo?.course?.learn || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={10} xs={12}>
                        <Typography>
                          <strong>Over view :&nbsp;</strong>
                          {batchInfo?.course?.overview}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Category:&nbsp;</strong>
                          {batchInfo?.course?.tags?.category[0].tag_name || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Grade:&nbsp;</strong>
                          {batchInfo?.course?.grade?.[0]?.grade_name || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Subject :&nbsp;</strong>
                          {batchInfo?.course?.subject?.subject_name || ''}
                        </Typography>
                      </Grid>
                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Subject Description :&nbsp;</strong>
                          {batchInfo?.course?.subject?.subject_description || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Level :&nbsp;</strong>
                          {batchInfo?.course?.level || ''}
                        </Typography>
                      </Grid>
                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Number of periods :&nbsp;</strong>
                          {batchInfo?.course?.no_of_periods || ''}
                        </Typography>
                      </Grid>
                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Price :&nbsp;</strong>
                          {batchInfo?.course?.prices?.min_price || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Is Active :&nbsp;</strong>
                          {batchInfo?.course?.is_active ? 'True' : 'False'}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Number of week :&nbsp;</strong>
                          {batchInfo?.course?.no_of_week || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={4} xs={12}>
                        <Typography>
                          <strong>Age :&nbsp;</strong>
                          {batchInfo?.course?.age || ''}
                        </Typography>
                      </Grid>

                      <Grid item md={6} xs={12}>
                        <Typography>
                          <strong>Teacher name:&nbsp;</strong>
                          {batchInfo?.teacher_name || ''}
                        </Typography>
                      </Grid>
                      {/* <Grid item md={4} xs={12}>
                
                <Typography>
                  Special Discount :&nbsp;
                'Not Added'
                </Typography>
        </Dialog>
            </Grid> */}
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Card>
      </Grid>

      <Dialog
        fullWidth
        onClose={handleClose}
        aria-labelledby='customized-dialog-title'
        open={open}
      >
        <DialogTitle>
          <Grid
            container
            direction='row'
            justifyContent='space-between'
            alignItems='flex-start'
          >
            <Grid xs={10}>
              <Typography variant='h6' color='primary'>
                <strong>Student Batch Details</strong>
              </Typography>
            </Grid>

            <Grid xs={2}>
              <Button 
              className ='closeHover'
              onClick={handleClose} style={{ float: 'right' }}>
                <CloseIcon fontSize='small' />
              </Button>
            </Grid>
          </Grid>{' '}
        </DialogTitle>
        <DialogContent dividers>
          <Grid item md={12} xs={12}>
            <Typography gutterBottom>
              <strong>Batch Name :&nbsp;</strong>
              {batchStudentRecords[index]?.aol_batch?.batch_name || ''}
            </Typography>
          </Grid>

          <Grid item md={12} xs={12}>
            <Typography gutterBottom>
              <strong>Batch Size :&nbsp;</strong>
              {batchStudentRecords[index]?.aol_batch?.batch_size || ''}
            </Typography>
          </Grid>

          <Grid item md={12} xs={12}>
            <Typography gutterBottom>
              <strong>Is Active :&nbsp;</strong>
              {batchInfo?.course?.is_active ? 'True' : 'False'}
            </Typography>
          </Grid>
        </DialogContent>
      </Dialog>

      <Dialog
        style={{marginTop:'45px'}}
        fullWidth
        onClose={handleClose}
        aria-labelledby='customized-dialog-title'
        open={zoomDetailsOpen}
      >
        <DialogTitle>
          <Grid
            container
            direction='row'
            justifyContent='space-between'
            alignItems='flex-start'
          >
            <Grid xs={10}>
              <Typography variant='h6' color='primary'>
                <strong>Student Zoom Details</strong>
              </Typography>
            </Grid>

            <Grid xs={2}>
              <Button onClick={handleClose} 
              className='closeHover'
              style={{ float: 'right' }}>
                <CloseIcon fontSize='small' />
              </Button>
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent 
        className={classes.studentZoomDetailsBox}
        dividers>
          <Paper className={classes.root}>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    <TableCell float='left'>S.No</TableCell>
                    <TableCell float='left'>Date</TableCell>
                    <TableCell float='left'>Start </TableCell>
                    <TableCell float='left'>End </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {batchStudentRecords &&
                    batchStudentRecords[zoomIndex]?.zoom_date?.map((data, index) => (
                      <TableRow>
                        <TableCell float='left'>{index + 1}</TableCell>
                        <TableCell float='left'>{data.date}</TableCell>
                        <TableCell float='left'>{data.start_time}</TableCell>
                        <TableCell float='left'>{data.end_time}</TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </DialogContent>
      </Dialog>

      {loading && <Loading />}
    </Layout>
  );
};

// export default ViewBatchDetails

ViewBatchDetails.propTypes = {
  classes: PropTypes.instanceOf(Object).isRequired,
};
export default withStyles(viewBatchDetailsStyles)(ViewBatchDetails);
