import React, { useState, useEffect, useContext } from 'react';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import { useHistory } from 'react-router-dom';
import Layout from 'containers/Layout';
import Loading from '../../components/loader/loader';

import CommonBreadcrumbs from 'components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  withStyles,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
  IconButton,
  Typography,
  Modal,
  Paper,
} from '@material-ui/core';
import { TabContext } from '@material-ui/lab';
import { SET_EDIT_DATA } from 'containers/online-class/create-class/create-class-context/create-class-constants';

const ViewBatchZoomDetails = ({ match }) => {
  const batchId = match.params.id;
  const [batchZoomMeetings, setBatchZoomMeetings] = useState([]);
  const [loading, setLoading] = useState(false);
  const [batchZoomLinks, setBatchZoomLinks] = useState([]);
  const { setAlert } = useContext(AlertNotificationContext);
  const [data, setData] = useState([]);

  async function getBatchZoomMeetings(id) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.batchApis.getBatchZoomMeetings}${id}/`
      );
      if (data) {
        setLoading(false);
        setData(data);
        setBatchZoomMeetings(data[0]);
        setBatchZoomLinks(data[0].is_meeting_canceled);
      } else {
        setLoading(false);
        console.log('data?.message', data?.message);
        setAlert('error', data?.message);
      }
    } catch (error) {
      console.log('error', error);
      setLoading(false);
      // setAlert('error', error?.message);
    }
  }

  useEffect(() => {
    getBatchZoomMeetings(batchId);
  }, []);

  return (
    <Layout>
      <Grid container spacing={2} className='plufo-zoom-id-generator-body'>
        <Grid item md={12} xs={12} className='plufo-zoom-id-generator-top-head'>
          <CommonBreadcrumbs
            componentName='Batch'
            childComponentName='Zoom Batch Details'
          />
        </Grid>
        {/* <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
              <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
                <Filtration
                  setPageNumber={setPageNumber}
                  getZoomIdList={getZoomIdList}
                  filterState={filterState}
                  setFilterState={setFilterState}
                  setLoading={setLoading}
                />
              </Grid>
            </Grid> */}
        {/* <Grid item md={12} xs={12} className='plufo-zoom-id-generator-top-head1'>
              <Button
                variant='contained'
                color='primary'
                size='small'
                onClick={() => getBatchList()}
              >
                <AddCircleOutlineIcon />
                &nbsp;Add Zoom Id
              </Button>
            </Grid> */}
        <Grid item md={12} xs={12} className='plufo-zoom-id-generator-table-div'>
          <Card className='plufo-zoom-id-generator-table-card'>
            {data && data.length === 0 ? (
              <Grid item md={3} xs={12}>
                <Typography color='primary' variant='h6' style={{ margin: '10px' }}>
                  <strong>No Records Found</strong>
                </Typography>
              </Grid>
            ) : (
              <>
                <Grid container spacing={2}>
                  <Grid item md={3} xs={12} justify='space-evenly'>
                    <Typography style={{ margin: '10px' }}>
                      <strong>Tutor:&nbsp;</strong>
                      <p>{batchZoomMeetings?.tutor?.tutor_name || ''}</p>
                    </Typography>
                  </Grid>
                  {batchZoomMeetings?.tutor !== null ? (
                    <Grid item md={3} xs={12} style={{ wordBreak: 'break-all' }}>
                      <Typography style={{ margin: '10px' }}>
                        <strong>Join Url :&nbsp;</strong>
                        <p>{batchZoomMeetings?.join_url || ''}</p>
                      </Typography>
                    </Grid>
                  ) : (
                    ''
                  )}
                  <Grid item md={3} xs={12} style={{ wordBreak: 'break-all' }}>
                    <Typography style={{ margin: '10px' }}>
                      <strong>Meeting Id:&nbsp;</strong>
                      <p>{batchZoomMeetings?.meeting_id || ''}</p>
                    </Typography>
                  </Grid>

                  <Grid item md={3} xs={12} className='table-url-box'>
                    <Typography style={{ margin: '10px' }}>
                      <strong>Presenter Url:&nbsp;</strong>
                      <p>{batchZoomMeetings?.presenter_url || ''}</p>
                    </Typography>
                  </Grid>
                </Grid>

                <Grid>
                  <Divider />
                </Grid>

                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell float='left'>S.No</TableCell>
                      <TableCell float='left'>Date</TableCell>
                      <TableCell float='left'>Start time</TableCell>
                      <TableCell float='left'>End time</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {batchZoomLinks &&
                      batchZoomLinks.map((data, index) => (
                        <TableRow>
                          <TableCell float='left'>{index + 1}</TableCell>
                          <TableCell float='left'>{data.date}</TableCell>
                          <TableCell float='left'>{data.start_time}</TableCell>
                          <TableCell float='left'>{data.end_time}</TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </>
            )}
          </Card>
        </Grid>
      </Grid>
      {loading && <Loading />}
    </Layout>
  );
};

export default ViewBatchZoomDetails;
