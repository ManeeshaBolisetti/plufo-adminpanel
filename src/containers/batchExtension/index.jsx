import { DataGrid } from '@material-ui/data-grid';
import React, { useContext, useEffect, useState } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Divider, Grid, TextField, Button, OutlinedInput } from '@material-ui/core';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import FormHelperText from '@material-ui/core/FormHelperText';
import axiosInstance from 'config/axios';
import endpoints from 'config/endpoints';
// import CustomSelectionTable from 'containers/communication/custom-selection-table/custom-selection-table';
import { AlertNotificationContext } from 'context-api/alert-context/alert-state';
import useStyles from './useStyles';
import CommonBreadcrumbs from 'components/common-breadcrumbs/breadcrumbs';
import './style.scss';
import moment from 'moment';
import CustomSelectionTable from 'containers/communication/custom-selection-table/custom-selection-table';

import Layout from '../Layout';
import Loader from '../../components/loader/loader';
import { SearchOutlined } from '@material-ui/icons';
import axios from 'axios';
// import './assign-role.css';

const BatchExtension = (props) => {
    const classes = useStyles();
    const { setAlert } = useContext(AlertNotificationContext);
    const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
    const [selectedRole, setSelectedRole] = useState('');
    const [pageno, setPageno] = useState(1);
    const [assignedRole, setAssigenedRole] = useState(false);
    const [assignedRoleError, setAssignedRoleError] = useState(false);
    const [totalPage, setTotalPage] = useState(0);
    const [usersRow, setUsersRow] = useState([]);
    const [completeData, setCompleteData] = useState([]);
    const [headers, setHeaders] = useState([]);
    const [roles, setRoles] = useState([]);
    const [branchList, setBranchList] = useState([]);
    const [gradeList, setGradeList] = useState([]);
    const [sectionList, setSectionList] = useState([]);
    const [selectedMultipleTeacher, setSelectedMultipleTeacher] = useState([]);
    const [multipleTeacherId, setMultipleTeacherId] = useState([]);
    const [selectedBranch, setSelectedBranch] = useState();
    const [selectedGrades, setSelectedGrades] = useState([]);
    const [selectedSections, setSelectedSections] = useState([]);
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [searchText, setSearchText] = useState('');
    const [roleError, setRoleError] = useState('');
    const [selectectUserError, setSelectectUserError] = useState('');
    const [clearAll, setClearAll] = useState(false);
    const [clearAllActive, setClearAllActive] = useState(false);
    const [filterCheck, setFilterCheck] = useState(false);
    const [selectAllObj, setSelectAllObj] = useState([]);
    const [viewMore, setViewMore] = useState(false);
    const [isSelected, setISselected] = useState(false);
    const [isNewSeach, setIsNewSearch] = useState(true);
    const [branchSelection, setBranchSelection] = useState('');
    const [selectedDate, setSelectedDate] = useState('');
    const [teacherSelect, setSelectedTeacher] = useState([]);
    const [numberOfDays, setNumberOfDays] = useState(null);
    const themeContext = useTheme();
    const [loading, setLoading] = useState(false);
    const isMobile = useMediaQuery(themeContext.breakpoints.down('xs'));
    const [isTeacher, setIsTeacher] = useState(false);
    const [deselectAll, setDeselectAll] = useState()
    const limit = 15;
    const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
    const [moduleId, setModuleId] = useState('');
    const [branchs, setBranches] = useState('');

    useEffect(() => {
        if (NavData && NavData.length) {
            NavData.forEach((item) => {
                if (
                    item.parent_modules === 'Master Management' &&
                    item.child_module &&
                    item.child_module.length > 0
                ) {
                    item.child_module.forEach((item) => {
                        if (item.child_name === 'Batch List') {
                            setModuleId(item.child_id);
                        }
                    });
                }
            });
        }
    }, []);


    const getTeacherApi = async () => {
        if (selectedBranch) {
            try {
                const result = await axiosInstance.get(`${endpoints.batchExtend.getTeacherList}?branch=${selectedBranch?.id}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });
                const resultOptions = [];
                if (result.status === 200) {
                    // console.log(result.data.result, 'besilery===========>')
                    result.data.result.map((items) => resultOptions.push(items.name));
                    setRoles(result.data.result);
                    setSelectedTeacher(result.data.result);
                    // console.log(resultOptions, 'kaltika====>')
                } else {
                    setAlert('error', result.data.message);
                }
            } catch (error) {
                setAlert('error', error.message);
            }
        }

    };

    const getGradeApi = async () => {
        try {
            // console.log(selectedBranch, 'chhhhh');
            const result = await axiosInstance.get(
                `${endpoints.communication.grades}?branch_id=${selectedBranch?.id}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            );
            if (result.status === 200) {
                setGradeList(result.data.data);
            } else {
                setAlert('error', result.data.message);
            }
        } catch (error) {
            setAlert('error', error.message);
        }
    };

    const getSectionApi = async () => {
        try {
            const selectedGradeId = selectedGrades.map((el) => el.grade_id);
            const result = await axiosInstance.get(
                `${endpoints.communication.sections
                }?branch_id=${selectedBranch}&grade_id=${selectedGradeId.toString()}`,
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            );
            if (result.status === 200) {
                setSectionList(result.data.data);
            } else {
                setAlert('error', result.data.message);
            }
        } catch (error) {
            setAlert('error', error.message);
        }
    };

    const displayUsersList = async () => {
        if (selectedBranch) {
            let getHolidayDetails = `${endpoints.batchExtend.holiDayList}?branch=${selectedBranch?.id}&page=${pageno}&page_size=${limit}`;
            if (selectedDate.length) {
                getHolidayDetails += `&holiday_date=${selectedDate}`

            }
            try {
                setLoading(true)
                const result = await axiosInstance.post(getHolidayDetails, {
                    teachers: multipleTeacherId,
                });
                if (result.status === 200) {
                    // console.log(result, 'sta22')
                    setHeaders([
                        { field: 'branchName', headerName: 'Branch', width: 250 },
                        { field: 'fullName', headerName: 'Batch Name', width: 250 },
                        { field: 'email', headerName: 'Batch Days', width: 200 },
                        { field: 'erp_id', headerName: 'Batch Time Slot', width: 250 },
                        { field: 'gender', headerName: 'Teacher Name', width: 250 },
                        { field: 'contact', headerName: 'Start Date', width: 250 },
                        { field: 'end_date', headerName: 'End Date', width: 250 },
                    ]);
                    const rows = [];
                    const selectionRows = [];
                    result.data.result.forEach((items) => {
                        rows.push({
                            id: items.id,
                            branchName: selectedBranch?.branch_name,
                            fullName: `${items.batch_name}`,
                            email: items?.batch_days,
                            erp_id: items?.batch_time_slot,
                            gender: items?.teacher?.teacher_name,
                            contact: items?.start_date,
                            end_date: items?.end_date,
                        });
                        selectionRows.push({
                            id: items.id,
                            data: {
                                id: items.id,
                                fullName: `${items.batch_name}`,
                                email: items?.batch_days,
                                erp_id: items?.batch_time_slot,
                                gender: items?.teacher_name,
                                contact: items?.start_date,
                                end_date: items?.end_date,
                            },
                            selected: selectedUsers.length
                                ? selectedUsers[pageno - 1].selected.includes(items.id)
                                : false,
                        });
                    });

                    setUsersRow(rows);
                    setCompleteData(selectionRows);
                    setTotalPage(result.data.count);
                    if (!selectedUsers.length) {
                        const tempSelectedUser = [];
                        for (let page = 1; page <= result.data.total_pages; page += 1) {
                            tempSelectedUser.push({ pageNo: page, selected: [] });
                        }
                        setSelectedUsers(tempSelectedUser);
                    }

                    if (result.data.total_pages !== selectAllObj.length) {
                        const tempSelectAll = [];
                        for (let page = 1; page <= result.data.total_pages; page += 1) {
                            tempSelectAll.push({ selectAll: false });
                        }
                        setSelectAllObj(tempSelectAll);
                    }
                    setLoading(false)

                } else {
                    // setAlert('error', result.data.message);
                }
            } catch (error) {
                setAlert('error', error.message);
            }
        } else {
            setHeaders([]);
            setUsersRow([]);
            setCompleteData([]);
            setTotalPage(0);
        }
    };

    const handleFilterCheck = () => {
        const selectionArray = [];
        selectedUsers.forEach((item) => {
            item.selected.forEach((ids) => {
                selectionArray.push(ids);
            });
        });
        if (selectedDate === '') {
            setAlert('warning', 'Please Select Date')
        }
        // else if(numberOfDays === null || numberOfDays === ''){
        //     setAlert('warning', 'Please enter Number Of Days')
        // }
        else if (selectionArray.length === 0) {
            setAlert('warning', 'Please Select Batch')
        }
        else {
            axiosInstance.put(`${endpoints.batchExtend.batchDateExtend}`, {
                batch_list: selectionArray,
                no_of_days: parseInt(1),
                holiday_date: selectedDate,
                is_teacher: isTeacher,
                reason: "",
            })
                .then((res) => {
                    // console.log(res)
                    setAlert('success', res.data.message);
                    setPageno(1);
                    setSelectedUsers([])
                    setDeselectAll(!deselectAll)
                    // clearSelectAll();
                    // displayUsersList()
                })
                .catch((err) => {
                    console.log(err, 'resultaaaa 11');
                });
        }


    }

    useEffect(() => {
        if (selectedMultipleTeacher) {

            displayUsersList();
        }

    }, [selectedMultipleTeacher])

    const handleClearAll = () => {
        // if (clearAllActive) {
        setSelectedUsers([]);
        setSelectedDate('')
        setNumberOfDays('')
        setSelectedMultipleTeacher([]);
        setSelectedSections([]);
        setSelectAllObj([]);
        setTotalPage(0);
        setPageno(1);
        setClearAll(true);
        setClearAllActive(false);
        displayUsersList()
        // }
    };

    const handleMultipleTeacher = (event, value) => {
        if (value.length) {
            setIsTeacher(true)
            console.log(value, 'mobile44')
            const ids = value.map((el) => el);
            setSelectedMultipleTeacher(ids);
            const ids2 = ids.map((el) => el?.id)
            setMultipleTeacherId(ids2)
            // console.log(ids2, 'ijijfr88')
        } else {
            setIsTeacher(false)
            setSelectedMultipleTeacher([]);
            setMultipleTeacherId([]);
        }
    };

    const handleBranch = (event, value) => {
        setBranchSelection(value);
        if (value) {
            const ids = value.id;
            setSelectedBranch(ids);
        } else {
            setSelectedBranch();
        }
    };

    const handleSelectAll = (e) => {
        // set select all to true/false

        // turn all the states to true/ False
        const tempSelectObj = selectAllObj.slice();

        // tempSelectObj[pageno - 1].selectAll = !tempSelectObj[pageno - 1].selectAll;
        tempSelectObj[pageno - 1].selectAll = e.target.checked;
        setSelectAllObj(tempSelectObj);
        const testclick = document.querySelectorAll('input[type=checkbox]');
        if (!selectAllObj[pageno - 1].selectAll) {
            for (let i = 2; i < testclick.length; i += 1) {
                // console.log('=== textClick checked: ', testclick[i])
                if (testclick[i].checked) {
                    testclick[i].click();
                }
                testclick[i].removeAttribute('checked');
            }
        } else {
            for (let i = 2; i < testclick.length; i += 1) {
                // console.log('=== textClick unCheck: ', testclick[i])
                if (!testclick[i].checked) {
                    testclick[i].click();
                }
                testclick[i].setAttribute('checked', 'checked');
            }
        }
    };

    const clearSelectAll = () => {
        const tempSelectAll = selectAllObj?.map((obj) => ({ ...obj, selectAll: false }));
        if (tempSelectAll.length) {
            setSelectAllObj(tempSelectAll);
        }
    };

    // const handleTextSearch = (e) => {
    //     setIsNewSearch(true);
    //     setSearchText(e.target.value);
    // };

    useEffect(() => {
        if (isNewSeach) {
            setIsNewSearch(false);
            displayUsersList();
            // setCurrentPage(1);
            // getUsersData();
        }
    }, [isNewSeach]);

    useEffect(() => {
        if (selectedDate) {
            displayUsersList();
        }
        if (selectedMultipleTeacher) {
            displayUsersList()

        }
        if (selectedBranch) {
            displayUsersList()
        }
    }, [selectedBranch, selectedDate, selectedMultipleTeacher])


    useEffect(() => {
        getTeacherApi()
        // getBranchApi();
    }, [selectedBranch]);
    useEffect(() => {
        if (selectedBranch) {
            getGradeApi();
        }
    }, [selectedBranch]);

    useEffect(() => {
        if (
            selectedMultipleTeacher.length ||
            selectedGrades.length ||
            selectedSections.length ||
            searchText
        ) {
            setClearAllActive(true);
        }
    }, [selectedMultipleTeacher, selectedGrades, selectedSections, searchText]);
    useEffect(() => {
        if (selectedGrades.length) {
            getSectionApi();
        }
    }, [selectedGrades]);
    useEffect(() => {
        if (clearAll) {
            // window.alert("hi")
            setMultipleTeacherId([]);
            displayUsersList()
            setClearAll(false);
        }
        if (pageno) {
            displayUsersList();
        }
        // }
    }, [pageno, clearAll, filterCheck]);


    const handleNoOfDays = (event) => {
        if (event) {
            setNumberOfDays(event.target.value);
        } else {
            setNumberOfDays('');
        }
    }

    const checkAll = selectAllObj[pageno - 1]?.selectAll || false;
    // console.log('rerendering ', checkAll, selectAllObj, pageno - 1);

    const getBranchApi = async () => {
        try {
            const result = await axiosInstance.get(`${endpoints.communication.branches}?module_id=${moduleId}`);
            if (result.status === 200) {
                setBranches(result.data.data);
            } else {
                setAlert('error', result.data.message);
            }
        } catch (error) {
            setAlert('error', error.message);
        }
    };
    useEffect(() => {
        getBranchApi()
    }, []);

    return (
        <Layout>
            <div className='assign-role-container'>
                <div className={classes.filtersContainer}>
                    <div className={`bread-crumbs-container ${classes.spacer}`}>
                        <CommonBreadcrumbs
                            componentName='Master Management'
                            childComponentName='Batch Extension'
                        />
                    </div>
                    <Grid container spacing={2} className={classes.spacer}>
                        <Grid item md={3} sm={4} xs={12}>
                            <TextField
                                fullWidth
                                className='meeting-date'
                                label='Meeting Date *'
                                variant='outlined'
                                size='small'
                                autoComplete='off'
                                name='meetingDateFilter'
                                type='date'
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                //   value={filterInput.meetingDateFilter}
                                value={selectedDate}
                                onChange={(e) => {
                                    // setFilterValue(e);
                                    setSelectedDate(moment(e.target.value).format('YYYY-MM-DD'))
                                    setSelectedUsers([])
                                    setDeselectAll(!deselectAll)

                                }}
                            />
                        </Grid>
                        <Grid item md={3} xs={3} sm={3}>
                            <Autocomplete
                                style={{ width: '100%' }}
                                size='small'
                                onChange={(event, value) => {
                                    setSelectedBranch(value);
                                }}
                                id='branch_id'
                                options={branchs}
                                getOptionLabel={(option) => option?.branch_name}
                                filterSelectedOptions
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        variant='outlined'
                                        label='Branch'
                                        placeholder='Branch'
                                    />
                                )}
                            />
                        </Grid>
                        <Grid item xs={12} md={3}>
                            <Autocomplete
                                multiple
                                size='small'
                                onChange={handleMultipleTeacher}
                                value={selectedMultipleTeacher}
                                id='message_log-smsType'
                                options={teacherSelect}
                                getOptionLabel={(option) => option?.name}
                                filterSelectedOptions
                                renderInput={(params) => (
                                    <TextField
                                        className='message_log-textfield'
                                        {...params}
                                        variant='outlined'
                                        label='Teachers'
                                        placeholder='Teachers'
                                    />
                                )}
                            />
                        </Grid>
                        {/* <Grid item md={3} sm={4} xs={12}>
                            <TextField
                                fullWidth
                                className='meeting-name'
                                label='Number of Sessions'
                                variant='outlined'
                                size='small'
                                autoComplete='off'
                                name='noOfDays'
                                value={numberOfDays}
                                //   value={filterInput.meetingNameFilter}
                                onChange={(e) => {
                                    handleNoOfDays(e)
                                    // // setFilterValue(e);
                                    // console.log(e.target, 'dhk4477========')
                                    // setNumberOfDays(e)

                                }}
                            />
                        </Grid> */}
                    </Grid>
                    <Divider className={classes.spacer} />
                    <Grid container spacing={4} className={classes.spacer}>
                        <Grid item md={2} xs={12}>
                            <Button
                                variant='contained'
                                className='disabled-btn'
                                onClick={handleClearAll}
                                fullWidth
                            >
                                CLEAR ALL
                            </Button>
                        </Grid>
                        <Grid item md={2} xs={12}>
                            <Button
                                variant='contained'
                                onClick={handleFilterCheck}
                                color='primary'
                                fullWidth
                            >
                                Extend
                            </Button>
                        </Grid>
                    </Grid>
                </div>
                <div
                    className={`${classes.tableActionsContainer} ${classes.spacer}`}
                    style={{ width: '95%', marginLeft: 'auto', marginRight: 'auto' }}
                >
                    <Grid container spacing={2}>
                        <Grid item md={2} xs={4}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={!!checkAll}
                                        onChange={handleSelectAll}
                                        color='primary'
                                    />
                                }
                                label='Select all'
                            />
                        </Grid>
                        {isMobile && (
                            <Grid item md={4} xs={6}>
                                <p
                                    className={classes.viewMoreBtn}
                                    onClick={() => {
                                        setViewMore((prevState) => !prevState);
                                    }}
                                    style={{ textAlign: 'right' }}
                                >
                                    {viewMore ? 'View Less' : 'View More'}
                                </p>
                            </Grid>
                        )}
                    </Grid>
                </div>
                {assignedRole ? (
                    <div>Please Wait ...</div>
                ) : (
                    <>
                        <span className='create_group_error_span'>{selectectUserError}</span>
                        <CustomSelectionTable
                            header={
                                isMobile
                                    ? headers
                                        .filter((obj) => {
                                            if (viewMore) {
                                                return true;
                                            }
                                            return ['fullName', 'erp_id'].includes(obj.field);
                                        })
                                        .map((header) => ({ ...header, width: 150 }))
                                    : headers
                            }
                            rows={usersRow}
                            checkAll={checkAll}
                            completeData={completeData}
                            totalRows={totalPage}
                            pageno={pageno}
                            selectedUsers={selectedUsers}
                            changePage={setPageno}
                            name='assign_role'
                            setSelectedUsers={setSelectedUsers}
                            pageSize={15}
                            deselectAll={deselectAll}
                        />
                    </>
                )}
            </div>
            {loading && <Loader />}
        </Layout>
    );
};

export default BatchExtension;
