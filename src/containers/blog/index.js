export { default as TeacherBlog } from './Teacher/TeacherBlog';
export { default as ContentView } from './Teacher/ContentView';
export { default as WriteBlog } from './Student/WriteBlog';
export { default as EditBlog } from './Student/EditBlog';

export { default as PreviewBlog } from './Student/PreviewBlog';
export { default as PreviewEditBlog } from './Student/PreviewEditBlog';

export { default as StudentDashboard } from './Student/StudentDashboard';
export { default as BlogView } from './Student/blogView';
export {default as TeacherPublishBlogView} from './Teacher/TeacherPublishBlogView';
export {default as CreateGenre} from './Teacher/CreateGenre'
export {default as EditGenre} from './Teacher/EditGenre'

export { default as ContentViewPublish } from './Teacher/ContentViewPublish';
export { default as AdminBlog } from './Admin/AdminBlog';
export { default as PrincipalBlog } from './Principal/PrincipalBlog';
export { default as ContentViewAdmin } from './Admin/ContentViewAdmin';
export { default as ContentViewPrincipal } from './Principal/ContentViewPrincipal';

export {default as AdminPublishBlogView} from './Admin/AdminPublishBlogView';
export {default as PrincipalPublishBlogView} from './Principal/PrincipalPublishBlogView';
export {default as StudentPublishBlogView} from './Student/StudentPublishBlogView';

export { default as ContentViewPublishStudent } from './Student/ContentViewPublishStudent';
export { default as ContentViewPublishAdmin } from './Admin/ContentViewPublishAdmin';
export { default as ContentViewPublishPrincipal } from './Principal/ContentViewPublishPrincipal';

export {default as CreateWordCountConfig} from './Teacher/CreateWordCountConfig'
export {default as EditWordCountConfig} from './Teacher/EditWordCountConfig'
