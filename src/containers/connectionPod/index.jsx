import Layout from '../Layout';
import React, { useEffect, useState, useContext } from 'react';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { Autocomplete, Pagination } from '@material-ui/lab';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import './style.scss';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
// import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';

// import FilterAltIcon from '@material-ui/icons/FilterAlt';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  MenuItem,
} from '@material-ui/core';
import moment from 'moment';

// import Tablepagination from './tablePagination';
import Tablepagination from '../../components/tablePagination';

const column = [
  'Sl No.',
  'Branch',
  'Meeting Name',
  'Meeting Date',
  'Meeting Time',
  'Student',
  'Teacher',
  'Attended',
  'Actions',
];

const ConnectionPod = (props) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const { is_superuser } = JSON.parse(localStorage.getItem('userDetails')) || {};

  const { erp_user_id } =
    JSON.parse(localStorage.getItem('userDetails')).role_details || {};

  const [loading, setLoading] = useState(false);
  const [meetingData, setMeetingData] = useState(null);
  const [searchText, setSearchText] = useState('');
  const [dialogOpen, setDialogOpen] = useState(false);
  const [studentList, setStudentList] = useState(null);
  const [teacherList, setTeacherList] = useState(null);
  const [meetingName, setMeetingName] = useState('');
  const [studentID, setStudentID] = useState(undefined);
  const [teacherID, setTeacherID] = useState(undefined);
  const [selectedStudent, setSelectedStudent] = useState({});
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [availableSlot, setAvailableSlot] = useState(null);
  const [accordianOpen, setAccordianOpen] = useState(false);
  const [isButtonDisable, setIsButtonDisable] = useState(false);
  const [data, setData] = useState({
    dataPerPage: 10,
    totalData: null,
    totalPages: null,
    currentPage: 1,
  });
  // const [meetingNameFilter, setMeetingNameFilter] = useState('');
  // const [meetingDateFilter, setMeetingDateFilter] = useState('');
  // const [studentNameFilter, setStudentNameFilter] = useState('');
  // const [teacherNameFilter, setTeacherNameFilter] = useState('');

  const [filterInput, setFilterInput] = useState({
    meetingNameFilter: '',
    meetingDateFilter: '',
    studentNameFilter: '',
    teacherNameFilter: '',
    meetingTypeFilter: 'upcoming',
  });
  const [branchs, setBranches] = useState('');
  const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
  const [moduleId, setModuleId] = useState('');
  const [selectedBranch, setSelectedBranch] = useState('');

  useEffect(() => {
    if (NavData && NavData.length) {
      NavData.forEach((item) => {
        if (
          item.parent_modules === 'Master Management' &&
          item.child_module &&
          item.child_module.length > 0
        ) {
          item.child_module.forEach((item) => {
            if (item.child_name === 'Connection Pod') {
              setModuleId(item.child_id);
            }
          });
        }
      });
    }
  }, []);

  useEffect(() => {
    getstudentList(searchText);
    getTeacherList();
    getMeetingData(data.currentPage);
    var date = new Date();
    setDate(moment(date).format('YYYY-MM-DD'));
  }, [selectedBranch]);

  useEffect(() => {
    getAvailableSlot(date, studentID);
  }, [date, studentID]);

  useEffect(() => {
    getstudentList(searchText);
  }, [teacherID, selectedBranch]);
  useEffect(() => {
    getMeetingData(data.currentPage);
  }, [selectedBranch, data.currentPage, data.dataPerPage]);

  const setFilterValue = (event) => {
    if (event.target.name === 'meetingDateFilter') {
      setFilterInput({
        ...filterInput,
        [event.target.name]: moment(event.target.value).format('YYYY-MM-DD'),
      });
    } else
      setFilterInput({
        ...filterInput,
        [event.target.name]: event.target.value,
      });
    console.log(filterInput);
    if (event.target.name === 'meetingNameFilter') {
      getMeetingData(
        1,
        event.target.value,
        filterInput.meetingDateFilter,
        filterInput.studentNameFilter,
        filterInput.teacherNameFilter,
        filterInput.meetingTypeFilter
      );
    }
    if (event.target.name === 'meetingDateFilter') {
      getMeetingData(
        1,
        filterInput.meetingNameFilter,
        event.target.value,
        filterInput.studentNameFilter,
        filterInput.teacherNameFilter,
        filterInput.meetingTypeFilter
      );
    }
    if (event.target.name === 'studentNameFilter') {
      getMeetingData(
        1,
        filterInput.meetingNameFilter,
        filterInput.meetingDateFilter,
        event.target.value,
        filterInput.teacherNameFilter,
        filterInput.meetingTypeFilter
      );
    }
    if (event.target.name === 'teacherNameFilter') {
      getMeetingData(
        1,
        filterInput.meetingNameFilter,
        filterInput.meetingDateFilter,
        filterInput.studentNameFilter,
        event.target.value,
        filterInput.meetingTypeFilter
      );
    }
    if (event.target.name === 'meetingTypeFilter') {
      getMeetingData(
        1,
        filterInput.meetingNameFilter,
        filterInput.meetingDateFilter,
        filterInput.studentNameFilter,
        filterInput.teacherNameFilter,
        event.target.value
      );
    }
  };

  const getMeetingData = async (
    page,
    meetingNameFilter,
    meetingDateFilter,
    studentNameFilter,
    teacherNameFilter,
    meetingTypeFilter
  ) => {
    let params;
    if (is_superuser) {
      params = `teacher_id=${erp_user_id}&page_size=${
        data.dataPerPage
      }&page=${page}&meeting_name=${
        meetingNameFilter !== undefined
          ? meetingNameFilter
          : filterInput.meetingNameFilter
      }&meeting_date=${
        meetingDateFilter !== undefined
          ? meetingDateFilter
          : filterInput.meetingDateFilter
      }&student_name=${
        studentNameFilter !== undefined
          ? studentNameFilter
          : filterInput.studentNameFilter
      }&teacher_name=${
        teacherNameFilter !== undefined
          ? teacherNameFilter
          : filterInput.teacherNameFilter
      }&meeting_type=${
        meetingTypeFilter !== undefined
          ? meetingTypeFilter
          : filterInput.meetingTypeFilter
      }`;
    } else {
      params = `teacher_id=${erp_user_id}&page_size=${data.dataPerPage}&page=${
        page !== undefined ? page : data.currentPage
      }&meeting_name=${
        meetingNameFilter !== undefined
          ? meetingNameFilter
          : filterInput.meetingNameFilter
      }&meeting_date=${
        meetingDateFilter !== undefined
          ? meetingDateFilter
          : filterInput.meetingDateFilter
      }&student_name=${
        studentNameFilter !== undefined
          ? studentNameFilter
          : filterInput.studentNameFilter
      }&meeting_type=${
        meetingTypeFilter !== undefined
          ? meetingTypeFilter
          : filterInput.meetingTypeFilter
      }`;
    }
    if (selectedBranch) {
      const result = await axiosInstance.get(
        `${endpoints.students.getTeacherMeeting}?branch=${selectedBranch?.id}&${params}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        // setLoading(true);
        console.log(result);
        setMeetingData(result.data.result);
        setData({
          ...data,
          totalData: result.data.count,
          totalPages: result.data.total_pages,
          currentPage: result.data.current_page,
        });
        // setLoading(false);
      }
    } else {
      setMeetingData([]);
    }
  };
  const getTeacherList = async () => {
    if (selectedBranch) {
      let page = '0';
      let resultCount = '0';
      const result = await axiosInstance.get(
        `${endpoints.students.getTeacherList}?branch=${selectedBranch?.id}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        console.log(result, 'result');
        setTeacherList(result.data.result);
        resultCount = result.data.count;
        page = result.data.total_pages;
      }
      if (parseInt(page) > 1) {
        getFullTeacherList(resultCount);
      }
    } else {
      setTeacherList([]);
    }
  };

  const getFullTeacherList = async (pagesize) => {
    if (selectedBranch) {
      const result = await axiosInstance.get(
        `${endpoints.students.getTeacherList}?branch=${selectedBranch?.id}&page_size=${pagesize}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        console.log(result, 'result');
        setTeacherList(result.data.result);
        console.log('listmerged');
      }
    } else {
      setTeacherList([]);
    }
  };

  const getstudentList = async (searchText) => {
    if (selectedBranch) {
      let page = '0';
      let resultCount = '0';
      let query = '';
      if (is_superuser) {
        if (teacherID !== undefined) {
          query = `&teacher_id=${teacherID}`;
        }
      } else {
        query = `&teacher_id=${erp_user_id}`;
      }
      const result = await axiosInstance.get(
        `${endpoints.students.connectionPod}?branch=${selectedBranch?.id}&term=${searchText}${query}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        console.log(result, 'result');
        setStudentList(result.data.result);
        resultCount = result.data.count;
        page = result.data.total_pages;
      }
      if (parseInt(page) > 1) {
        getFullStudentList(resultCount);
      }
    } else {
      setStudentList([]);
    }
  };
  const getFullStudentList = async (pagesize) => {
    console.log('getFullStudentList');
    if (selectedBranch) {
      let query = '';
      if (is_superuser) {
        if (teacherID !== undefined) {
          query = `&teacher_id=${teacherID}`;
        }
      } else {
        query = `&teacher_id=${erp_user_id}`;
      }
      const result = await axiosInstance.get(
        `${endpoints.students.connectionPod}?branch=${selectedBranch?.id}&term=${searchText}${query}&page_size=${pagesize}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        console.log(result, 'result');
        setStudentList(result.data.result);
      }
    } else {
      setStudentList([]);
    }
  };

  const filter = () => {
    getMeetingData(1);
  };
  const clearFilter = () => {
    setFilterInput({
      meetingNameFilter: '',
      meetingDateFilter: '',
      studentNameFilter: '',
      teacherNameFilter: '',
      meetingTypeFilter: '',
    });
    getMeetingData(1, '', '', '', '', '');
  };
  const getAvailableSlot = async (date, erpId) => {
    setStudentID(erpId);
    let query;
    if (erpId !== undefined && date.length > 0) {
      console.log('hello');
      query = `${endpoints.students.availableSlot}?date=${date}&erp_id=${erpId}`;
    }
    // else {
    //   query = `${endpoints.students.availableSlot}?date=${date}&erp_id=`;
    // }
    if (query !== undefined) {
      const result = await axiosInstance.get(`${query}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (result.data.status_code === 200) {
        console.log(result);
        setAvailableSlot(result.data.available_slots);
      }
    }
  };

  const confirm = async () => {
    setIsButtonDisable(true);
    let result1 = {
      meetingName: meetingName,
      studentID: studentID,
      date: date,
      time: time,
    };

    let params = `erp_id=${studentID}&meeting_name=${meetingName}&time_slot=${time}&date=${date}`;
    if (is_superuser) {
      params = params + `&teacher_id=${teacherID}`;
    } else {
      params = params + `&teacher_id=${erp_user_id}`;
    }
    // console.log(result1);
    if (
      studentID !== undefined &&
      meetingName.length > 0 &&
      time.length > 0 &&
      date.length > 0
    ) {
      // console.log('hello');
      const result = await axiosInstance.post(
        `${endpoints.students.createMeeting}?${params}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        setAlert('success', result.data.message);
        setDialogOpen(false);
        getMeetingData(1);
        setSelectedStudent({});
        setStudentID(undefined);
        setMeetingName('');
        setDate('');
        setTime('');
        setIsButtonDisable(false);
      }
    }
  };

  const isJoinDisable = (meeting_date, meeting_time,server_time) => {
    // console.log({meeting_date, meeting_time,server_time},'jkhhh999')
    // diff between current date and time and meeting_date,meeting_time 5 min then return false else return true
    var d = new Date();
    var startTime = moment(d);
    var serverTime = moment(new Date(server_time))
    // console.log({startTime,serverTime}, 'DIFF=====>')
    var endTime = moment(meeting_date.slice(0, 11) + meeting_time.slice(0, 5) + ':00');
    var duration = moment.duration(endTime.diff(server_time ? serverTime :  startTime));
    let meetingStartTime = moment(
      meeting_date.slice(0, 11) + meeting_time.slice(0, 5) + ':00'
    );
    let meetingEndTime = moment(
      meeting_date.slice(0, 11) + meeting_time.slice(8) + ':00'
    );
    let meetingDuration = moment.duration(meetingEndTime.diff(meetingStartTime));
    console.log(moment(meeting_time.slice(0, 5) + ':00').format(), 'datediff');
    if (
      parseInt(duration.asMinutes()) < 5 &&
      parseInt(duration.asMinutes()) > -parseInt(meetingDuration.asMinutes())
    ) {
      return false;
    } else return true;
  };

  const getMeetingText = (meeting_date, meeting_time, server_time) => {
    // console.log({meeting_date, meeting_time,server_time},'jkhhh99911')
    var d = new Date();
    var startTime = moment(d);
    var serverTime = moment(new Date(server_time))
    var endTime = moment(meeting_date.slice(0, 11) + meeting_time.slice(0, 5) + ':00');
    var duration = moment.duration(endTime.diff(server_time ? serverTime : startTime));
    let meetingStartTime = moment(
      meeting_date.slice(0, 11) + meeting_time.slice(0, 5) + ':00'
    );
    let meetingEndTime = moment(
      meeting_date.slice(0, 11) + meeting_time.slice(8) + ':00'
    );
    let meetingDuration = moment.duration(meetingEndTime.diff(meetingStartTime));
    console.log(moment(meeting_time.slice(0, 5) + ':00').format(), 'datediff');
    if (parseInt(duration.asMinutes()) < parseInt(meetingDuration.asMinutes()) * -1) {
      return 'Completed';
    }
    if (parseInt(duration.asMinutes()) < 5) {
      return 'Host Meeting';
    }
    if (parseInt(duration.asMinutes()) > 5) {
      return 'Upcoming';
    }
  };

  const closeDialog = () => {
    setDialogOpen(false);
    setSelectedStudent({});
    setStudentID('');
    setMeetingName('');
    setDate('');
    setTime('');
  };

  const getBranchApi = async () => {
    try {
      const result = await axiosInstance.get(
        `${endpoints.communication.branches}?module_id=${moduleId}`
      );
      if (result.status === 200) {
        setBranches(result.data.data);
      } else {
        setAlert('error', result.data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
    }
  };
  useEffect(() => {
    getBranchApi();
  }, []);

  return (
    <>
      <Layout>
        <div className='connection-pod-container'>
          {loading ? (
            <Loader />
          ) : (
            <>
              <div className='connection-pod-breadcrumb-wrapper'>
                <CommonBreadcrumbs
                  componentName='Master Management'
                  childComponentName='Connection Pod'
                />
              </div>
              <div className='filter-container'>
                <Grid container spacing={3} alignItems='center'>
                  <Grid item sm={1} xs={12}>
                    <Tooltip title='Create Meeting' placement='bottom' arrow>
                      <IconButton
                        className='create-meeting-button'
                        // color='primary'
                        onClick={() => {
                          setDialogOpen(true);
                          getAvailableSlot(date);
                        }}
                      >
                        <AddIcon style={{ color: '#ffffff' }} />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                  <Grid item md={3} sm={4} xs={12}>
                    <Autocomplete
                      style={{ width: '100%' }}
                      size='small'
                      onChange={(event, value) => {
                        setSelectedBranch(value);
                      }}
                      id='branch_id'
                      options={branchs}
                      getOptionLabel={(option) => option?.branch_name}
                      filterSelectedOptions
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant='outlined'
                          label='Branch'
                          placeholder='Branch'
                        />
                      )}
                    />
                  </Grid>
                  <Grid item sm={12} xs={12}>
                    <Accordion expanded={accordianOpen}>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls='panel1a-content'
                        id='panel1a-header'
                        onClick={() => setAccordianOpen(!accordianOpen)}
                      >
                        {/* <FilterIcon /> */}
                        <Typography variant='h6' color='primary'>
                          Filter
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <Grid container spacing={3} alignItems='center'>
                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='meeting-name'
                              label='Meeting Name'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='meetingNameFilter'
                              value={filterInput.meetingNameFilter}
                              onChange={(e) => {
                                setFilterValue(e);
                              }}
                            />
                          </Grid>
                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='meeting-date'
                              label='Meeting Date'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='meetingDateFilter'
                              type='date'
                              InputLabelProps={{
                                shrink: true,
                              }}
                              value={filterInput.meetingDateFilter}
                              onChange={(e) => {
                                setFilterValue(e);
                              }}
                            />
                          </Grid>
                          {is_superuser && (
                            <Grid item md={3} sm={4} xs={12}>
                              <TextField
                                fullWidth
                                className='teacher-name'
                                label='Teacher Name'
                                variant='outlined'
                                size='small'
                                autoComplete='off'
                                name='teacherNameFilter'
                                value={filterInput.teacherNameFilter}
                                onChange={(e) => {
                                  setFilterValue(e);
                                }}
                              />
                            </Grid>
                          )}

                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='student-name'
                              label='Student Name'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='studentNameFilter'
                              value={filterInput.studentNameFilter}
                              onChange={(e) => {
                                setFilterValue(e);
                              }}
                            />
                          </Grid>
                          <Grid item md={3} sm={4} xs={12}>
                            <FormControl fullWidth margin='dense' variant='outlined'>
                              <InputLabel>Meeting Type</InputLabel>
                              <Select
                                value={filterInput.meetingTypeFilter || ''}
                                label='Meeting Type'
                                name='meetingTypeFilter'
                                onChange={(e) => {
                                  setFilterValue(e);
                                }}
                              >
                                <MenuItem value={''}>All</MenuItem>
                                <MenuItem value={'upcoming'}>Upcoming</MenuItem>
                                <MenuItem value={'past'}>Past</MenuItem>
                              </Select>
                            </FormControl>
                          </Grid>
                          {/* <Grid item sm={2} xs={3}>
                            <Button className='filter-button' onClick={() => filter()}>
                              Filter
                            </Button>
                          </Grid> */}
                          <Grid item sm={2} xs={3}>
                            <Button
                              className='filter-button'
                              onClick={() => clearFilter()}
                            >
                              Clear Filter
                            </Button>
                          </Grid>
                        </Grid>
                      </AccordionDetails>
                    </Accordion>
                  </Grid>
                </Grid>
              </div>
              <div className='table-container'>
                <TableContainer component={Paper}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        {column &&
                          column.map((eachColumn, index) => {
                            return <TableCell key={index}>{eachColumn}</TableCell>;
                          })}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {meetingData &&
                        meetingData.map((eachData, index) => {
                          return (
                            <TableRow key={eachData.id}>
                              <TableCell>
                                {data.currentPage == 1
                                  ? index + data.currentPage
                                  : (data.currentPage - 1) * data.dataPerPage +
                                    (index + 1)}
                              </TableCell>
                              <TableCell>{selectedBranch?.branch_name}</TableCell>
                              <TableCell>{eachData.meeeting_name}</TableCell>
                              <TableCell>
                                {`${moment(eachData.meeting_date).format('DD-MM-YYYY')}`}
                                {/* {eachData.meeting_date} */}
                              </TableCell>
                              <TableCell>{eachData.meeting_time}</TableCell>
                              <TableCell>{eachData.student.student_name}</TableCell>
                              <TableCell>{eachData.teacher.teacher_name}</TableCell>
                              <TableCell>{eachData.is_attended ? 'Yes' : 'No'}</TableCell>
                              <TableCell>
                                <Button
                                  disabled={isJoinDisable(
                                    eachData.meeting_date,
                                    eachData.meeting_time,
                                    eachData.server_time
                                  )}
                                  size='small'
                                  className='host-meeting-button'
                                  onClick={() => {
                                    console.log(eachData.host);
                                    window.open(eachData.host, '_blank');
                                  }}
                                >
                                  {getMeetingText(
                                    eachData.meeting_date,
                                    eachData.meeting_time,
                                    eachData.server_time
                                  )}
                                </Button>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                    </TableBody>
                  </Table>
                </TableContainer>
              </div>
              <Tablepagination data={data} setData={setData} />
            </>
          )}
        </div>
      </Layout>
      <Dialog open={dialogOpen} className='create-meetinng-dialog'>
        <DialogTitle className='dialog-title'>Create Meeting</DialogTitle>
        <div className='meeting-form'>
          {is_superuser && (
            <Autocomplete
              fullWidth
              size='small'
              className='filter-teacher meeting-form-input'
              options={(teacherList && teacherList) || []}
              getOptionLabel={(option) => option.name || ''}
              filterSelectedOptions
              onChange={(event, value) => {
                setTeacherID(value?.id);
                // setSelectedStudent(value);
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  required
                  fullWidth
                  variant='outlined'
                  label='Teacher'
                />
              )}
              renderOption={(option, { selected }) => (
                <React.Fragment>{option.name}</React.Fragment>
              )}
            />
          )}
          <Autocomplete
            fullWidth
            size='small'
            className='filter-student meeting-form-input'
            options={(studentList && studentList) || []}
            getOptionLabel={(option) => option.name || ''}
            filterSelectedOptions
            // value={selectedStudent || ''}
            onChange={(event, value) => {
              setStudentID(value.student_id);
              // setSelectedStudent(value);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                required
                fullWidth
                variant='outlined'
                label='Student'
              />
            )}
            renderOption={(option, { selected }) => (
              <React.Fragment>{option.name}</React.Fragment>
            )}
          />
          <TextField
            required
            fullWidth
            className='filter-student meeting-form-input'
            label='Meeting Name'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='searchText'
            value={meetingName}
            onChange={(e) => setMeetingName(e.target.value)}
          />
          <TextField
            required
            fullWidth
            className='filter-student meeting-form-input'
            label='Meeting Date'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='searchText'
            value={date}
            onChange={(e) => setDate(moment(e.target.value).format('YYYY-MM-DD'))}
            type='date'
            // min={new Date().toISOString().split('T')[0]}
            InputProps={{ inputProps: { min: new Date().toISOString().split('T')[0] } }}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Autocomplete
            fullWidth
            size='small'
            className='filter-student meeting-form-input'
            options={(availableSlot && availableSlot) || []}
            getOptionLabel={(option) => option || ''}
            filterSelectedOptions
            value={time || ''}
            onChange={(event, value) => {
              setTime(value);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                required
                fullWidth
                variant='outlined'
                label='Meeting Time'
              />
            )}
          />
          <div className='meeting-form-actions'>
            <Button
              disabled={isButtonDisable === true}
              className='meeting-form-actions-butons'
              onClick={() => confirm()}
            >
              Confirm
            </Button>
            <Button className='meeting-form-actions-butons' onClick={() => closeDialog()}>
              Cancel
            </Button>
          </div>
        </div>
      </Dialog>
    </>
  );
};

export default ConnectionPod;
