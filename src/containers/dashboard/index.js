import React from 'react';
import { useHistory } from 'react-router';
import Dashboard from './main-dashboard';
import Layout from '../Layout';
import { DashboardContextProvider } from './dashboard-context';
import displayName from '../../config/displayName';
// import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';

const DashboardWrapper = () => {
  const history = useHistory();
  return (
    <Layout>
      <DashboardContextProvider>
        <div style={{ margin: '10px 25px' }} className='page_title'>
          {' '}
          <span style={{ cursor: 'pointer' }} onClick={() => history.push('/dashboard')}>
            {displayName} Dashboard
          </span>
        </div>
        <Dashboard />
      </DashboardContextProvider>
      {/* Dashboard */}
    </Layout>
  );
};

export default DashboardWrapper;
