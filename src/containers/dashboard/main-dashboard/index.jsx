import React, { useState, useEffect } from 'react';
// import Layout from '../../Layout';
import { Grid } from '@material-ui/core';
import { ReportStatsWidget } from '../widgets';
import SpellcheckIcon from '@material-ui/icons/Spellcheck';
import OndemandVideoIcon from '@material-ui/icons/OndemandVideo';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import WebAsset from '@material-ui/icons/WebAsset';
import ForumIcon from '@material-ui/icons/Forum';
import { reportTypeConstants } from '../dashboard-constants';
import { useDashboardContext } from '../dashboard-context';
import endpoints from 'config/endpoints';
import axiosInstance from '../../../config/axios';
import axios from 'axios';
import reactHtmlParser from 'react-html-parser';
import Loader from '../../../components/loader/loader';

const Dashboard = () => {
  const { attendance, classwork, homework } = reportTypeConstants;
  const { branchIds = [], getReport = () => {} } = useDashboardContext();

  const [reports, setReports] = useState({
    attendanceReport: [],
    classworkReport: [],
    homeworkReport: [],
    studentReport: [],
  });
  const [allReports, setAllReports] = useState([]);
  const [loading, setLoading] = useState(false);
  const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};

  //   const getAttendanceReport = (params) => {
  //     getReport(attendance, params)
  //       .then((response) => {
  //         const attendanceReport = response.map(
  //           ({ grade = '', subject = '', present = 0, absent = 0 }) => ({
  //             detail: `${grade} - ${subject}`,
  //             positive: present,
  //             negative: absent,
  //           })
  //         );
  //         setReports((prev) => ({ ...prev, attendanceReport }));
  //       })
  //       .catch(() => {});
  //   };

  //   const getAttendanceReport = (params) => {
  //     axiosInstance
  //       .get(`${endpoints.dashboard.teacher.listAttendanceReport}`)
  //       .then((response) => {
  //         console.log(response);
  //       })
  //       .catch(() => {
  //         console.log('response');
  //       });
  //   };

  const getAttendanceReport = () => {
    setLoading(true);
    axiosInstance
      .get(
        `${endpoints.dashboard.listAttendanceReport}`
        // `https://dev.plufo.letseduvate.com/qbox/aol/dashboard-report/`,
        // {
        //   headers: {
        //     Authorization: `Bearer ${token}`,
        //   },
        // }

        //   `${
        //     endpoints.ibook.teacherView
        //   }?domain_name=${getDomainName()}&page_number=${pageNo}&page_size=${limit}`
        // `https://dev.plufo.letseduvate.com/qbox/course_extend/weekly_report_list/?page_size=${data.dataPerPage}&page=${data.currentPage}`
      )
      .then((result) => {
        // const ar = result.data.map((res) => res);
        setLoading(false);
        setAllReports(result.data);
        result.data.map((res) => {
          if (res.section_name === 'Teacher Attendance Report') {
            const attendanceReport = res;
            setReports((prev) => ({ ...prev, attendanceReport }));
            // console.log(res, 'check');
          }
          if (res.section_name === 'Classwork Report') {
            const classworkReport = res;
            setReports((prev) => ({ ...prev, classworkReport }));
            // console.log(res, 'check');
          }
          if (res.section_name === 'HomeWork Report') {
            const homeworkReport = res;
            setReports((prev) => ({ ...prev, homeworkReport }));
            // console.log(res, 'check44');
          }
          if (res.section_name === 'Student Attendance Report') {
            const studentReport = res;
            setReports((prev) => ({ ...prev, studentReport }));
            // console.log(res, 'check22');
          }
        });

        console.log(result.data, 'jjjj');
      })
      .catch((error) => {
        setLoading(false);
        //   setAlert('error', error.message);
        console.log('err', error);
      });
  };

  useEffect(() => {
    // const params = { branch_ids: branchIds.join(',') };
    // if (branchIds.length > 0) {
    getAttendanceReport();
    //   getClassworkReport(params);
    //   getHomeworkReport(params);
    // }
  }, []);
  const { attendanceReport = [], classworkReport = [], homeworkReport = [] , studentReport = [] } =
    reports || {};

  console.log(
    reports,
    reports.attendanceReport.icon
      ? Symbol(reports.attendanceReport.icon)
      : SpellcheckIcon,
    'hhhh'
  );

  return (
    <>
      {/*<Layout>*/}

      <Grid style={{ margin: '10px 20px', width:"96%"}} container spacing={2}>
        {/* {console.log(item, 'aaa')} */}
        {allReports.map((item) => (
          <Grid item xs={12} md={4}>
            {console.log(item.icon, 'aaa')}
            <ReportStatsWidget
              title={item.section_name}
              data={
                item.section_name === 'Teacher Attendance Report'
                  ? attendanceReport
                  : item.section_name === 'Classwork Report'
                  ? classworkReport
                  : item.section_name === 'HomeWork Report'
                  ? homeworkReport
                  : item.section_name === 'Student Attendance Report'
                  ? studentReport
                  : ''
              }
              avatar={
                item.section_name === 'Attendance Report'
                  ? SpellcheckIcon
                  : item.section_name === 'Classwork Report'
                  ? OndemandVideoIcon
                  : item.section_name === 'HomeWork Report'
                  ? MenuBookIcon
                  : item.section_name === 'Blog Report'
                  ? WebAsset
                  : item.section_name === 'Discussion Report'
                  ? ForumIcon
                  : SpellcheckIcon
              }
              setReports={setReports}
              totalReportData={reports}
            />
          </Grid>
        ))}
      </Grid>
      {loading && <Loader />}

      {/* </Layout> */}
    </>
  );
};

export default Dashboard;
