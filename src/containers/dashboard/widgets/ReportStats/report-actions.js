import React, { useContext, useState } from 'react';
import { IconButton, Menu, Fade, Typography, Tooltip } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVert from '@material-ui/icons/MoreVert';
import GetAppIcon from '@material-ui/icons/GetApp';
import { AlertNotificationContext } from '../../../../context-api/alert-context/alert-state';
import { useDashboardContext } from '../../dashboard-context';
import SyncIcon from '@material-ui/icons/Sync';
import axiosInstance from 'config/axios';
import { _setUnhandledRejectionFn } from 'tinymce/themes/silver/theme';
// import { Tooltip } from 'highcharts';

const reportTypes = [
  { type: 'Daily Report', days: '1' },
  { type: 'Weekly Report', days: '7' },
  { type: 'Monthly Report', days: '30' },
];

const ReportAction = ({ title, data, setReports, totalReportData, syncActive }) => {
  const { branchIds = [], downloadReport = () => {} } = useDashboardContext();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const { setAlert } = useContext(AlertNotificationContext);
  const [href, setHref] = useState(null);

  const handleClick = (event) => {
    // console.log(data?.options?.map((ele) => ele.report_name) === title, title, 'actions');

    setAnchorEl(event.currentTarget);
  };

  const handleGetData = (event) => {
    // console.log(data?.options?.map((ele) => ele.report_name) === title, title, 'actions');
    // console.log(event.currentTarget, 'data', data.refresh_url, data, data.section_name);
    console.log(totalReportData, data, 'shdjshjdhs');

    const refreshURL = data.refresh_url;
    axiosInstance
      .get(`${refreshURL}`)
      .then((result) => {
        console.log(result.data.data, 'fff');
        if (data.section_name === 'Teacher Attendance Report') {
          setReports({
            ...totalReportData,
            attendanceReport: {
              ...totalReportData.attendanceReport,
              data: result.data.data,
            },
          });
        }
        if (data.section_name === 'Student Attendance Report') {
          setReports({
            ...totalReportData,
            studentReport: {
              ...totalReportData.studentReport,
              data: result.data.data,
            },
          });
        }
        if (data.section_name === 'HomeWork Report') {
          setReports({
            ...totalReportData,
            homeworkReport: {
              ...totalReportData.homeworkReport,
              data: result.data.data,
            },
          });
        }
      })
      .catch((error) => {
        console.log('err', error);
      });
  };

  const handleClose = (event) => {
    setAnchorEl(null);
  };

  const downloadExcelFile = (excelData) => {
    const blob = window.URL.createObjectURL(
      new Blob([excelData], {
        type: 'application/vnd.ms-excel',
      })
    );
    let link = document.createElement('a');
    link.style.display = 'none';
    link.href = blob;
    link.setAttribute('download', 'report.xls');
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleDownload = (type, days) => {
    // const params = { days, branch_ids: branchIds.join(',') };
    // const decisonParam = title.toLowerCase().split(' ')[0];
    // downloadReport(decisonParam, params)
    //   .then((response) => {
    //     const { headers = {}, message = 'Downloadable report not available', data = '' } =
    //       response || {};
    //     const contentType = headers['content-type'] || 'application/json';
    //     if (contentType === 'application/json') {
    //       setAlert('info', message);
    //     } else {
    //       downloadExcelFile(data);
    //     }
    //   })
    //   .catch(() => {});
    const reportData = data?.options?.map((ele) => {
      // console.log(ele.report_name, 'hiii333')
      if (ele.report_name === type) {

        if (ele.url !== null) {
          // setHref(
          //   axiosInstance.get(`${ele.url}`).then((response) => {
          //     console.log(response,'mmkkk3333')
          //     const {
          //       headers = {},
          //       message = 'Downloadable report not available',
          //       data = '',
          //     } = response || {};
          //     const contentType = headers['content-type'] || 'application/json';
          //     if (contentType === 'application/json') {
          //       setAlert('info', message);
          //     } else {
          //       // downloadExcelFile(data);
          //       window.open(ele.url, '_blank');
          //     }
          //   })
          // );
          // console.log(ele,'oopp8888')
          window.open(ele.url, '_blank');
        } else {
          window.open('/', '_blank');
        }
      }
    });
    // if (data?.options?.map((ele) => ele.report_name) === type) {
    // }
    // console.log(type, 'actions 2');
    handleClose();
  };

  return (
    <div>
      <Tooltip title='Click to get recent data' arrow>
        <IconButton
          aria-label='download'
          ref={anchorEl}
          aria-controls={open ? 'menu-list-grow' : undefined}
          aria-haspopup='true'
          onClick={(e) => handleGetData(e)}
          disabled={syncActive === false}
        >
          <SyncIcon />
        </IconButton>
      </Tooltip>
      <IconButton
        aria-label='download'
        ref={anchorEl}
        aria-controls={open ? 'menu-list-grow' : undefined}
        aria-haspopup='true'
        onClick={handleClick}
        title='Download Reports'
      >
        <MoreVert />
      </IconButton>
      <Menu
        id='fade-menu'
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {reportTypes.map(({ type, days }) => (
          <MenuItem
            dense={true}
            key={`${type - days}`}
            onClick={() => handleDownload(type, days)}
            component='button'
            // href={href}
          >
            <ListItemIcon>
              <GetAppIcon fontSize='small' />
            </ListItemIcon>
            <Typography variant='inherit'>{type}</Typography>
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};

export default ReportAction;
