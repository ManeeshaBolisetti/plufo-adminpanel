import React, { useEffect, useState } from 'react';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import {
  CardHeader,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Chip,
  Avatar,
  Box,
  IconButton,
} from '@material-ui/core';
import '../style.scss';
import ReportAction from './report-actions';
import { useStyles } from './useStyles';
import axiosInstance from 'config/axios';
import endpoints from 'config/endpoints';
import SyncIcon from '@material-ui/icons/Sync';

const ReportStatsWidget = ({
  avatar,
  title,
  data,
  ptitle,
  ntitle,
  setReports,
  totalReportData,
}) => {
  const { role_details } = JSON.parse(localStorage.getItem('userDetails')) || {};
  console.log(role_details, 'rrr');
  const [isNoRecords, setIsNoRecords] = useState(false);
  const [syncActive, setSyncActive] = useState(false);
  const classes = useStyles();
  // const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
  // console.log(totalReportData?.studentReport?.section_name,'ufiefief999999')
  // useEffect(() => {

  // }, [totalReportData])

  const handleGetData = (event) => {
    // console.log(data?.options?.map((ele) => ele.report_name) === title, title, 'actions');
    // console.log(event.currentTarget, 'data', data.refresh_url, data, data.section_name);
    console.log(totalReportData, data, 'shdjshjdhs');
    const refreshURL = data.refresh_url;
    // setIsNoRecords(true);
    setSyncActive(false);
    axiosInstance
      .get(`${refreshURL}`)
      .then((result) => { 
        
        console.log(result, 'pppppp222');
        if (data.section_name === 'Teacher Attendance Report') {
          setReports({
            ...totalReportData,
            attendanceReport: {
              ...totalReportData.attendanceReport,
              data: result.data.data,
            },
          });
        }
        if (data.section_name === 'Student Attendance Report') {
          setReports({
            ...totalReportData,
            studentReport: {
              ...totalReportData.studentReport,
              data: result.data.data,
            },
          });
        }
        if (data.section_name === 'HomeWork Report') {
          setReports({
            ...totalReportData,
            homeworkReport: {
              ...totalReportData.homeworkReport,
              data: result.data.data,
            },
          });
        }
        setIsNoRecords(true);
        setSyncActive(true);
      })
      .catch((error) => {
        setIsNoRecords(true);
        setSyncActive(true);
        console.log('err', error);
      });
  };

  const renderReportData = () => {
    // console.log(data?.data?.length, 'kkkkk');
    if (role_details.role_id === 2) {
      if (data?.data?.length) {
        return (
          <List>
            {data.data.map((item) => (
              <ListItem>
                {console.log(item, 'check')}
                <ListItemText primary={item?.name} className={classes.cardText} />
                <ListItemSecondaryAction>
                  {item?.count_green >= 0 && item?.count_green !== null && (
                    <Chip
                      className={classes.positive_count}
                      size='small'
                      label={item?.count_green !== null ? item?.count_green : ''}
                      title={ptitle || ''}
                    />
                  )}
                  {item?.count_red >= 0 && item?.count_red !== null && (
                    <Chip
                      className={classes.negative_count}
                      size='small'
                      label={item?.count_red}
                      title={ntitle || ''}
                    />
                  )}
                  {item?.count_yellow >= 0 && item?.count_yellow !== null && (
                    <Chip
                      className={classes.yellow_count}
                      size='small'
                      label={item?.count_blue}
                      title={ntitle || ''}
                    />
                  )}
                  {item?.count_default >= 0 && item?.count_default !== null && (
                    <Chip
                      className={classes.default_count}
                      size='small'
                      label={item?.count_default}
                      title={ntitle || ''}
                    />
                  )}
                  {item?.info >= 0 && (
                    <Chip
                      className={classes.info_count}
                      size='small'
                      label={item?.info}
                      title={ntitle || ''}
                    />
                  )}
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
        );
      } else {
        return (
          <Box className={classes.noDataTag}>
            {isNoRecords ? (
              <>
                <Box style={{ fontSize: '1.2rem' }}>☹️</Box>
                <Box>No Records</Box>
              </>
            ) : (
              <>
                <Box>
                  Click on{' '}
                  <IconButton
                    onClick={(e) => handleGetData(e)}
                    disableFocusRipple
                    style={{ padding: 0 }}
                  >
                    <SyncIcon />{' '}
                  </IconButton>{' '}
                  to get data{' '}
                </Box>
              </>
            )}
          </Box>
        );
      }
    } else {
      if (data?.data?.length) {
        return (
          <List>
            {data.data.map((item) => (
              <ListItem>
                {console.log(item, 'check')}
                <ListItemText primary={item?.name} className={classes.cardText} />
                <ListItemSecondaryAction>
                  {item?.count_green >= 0 && item?.count_green !== null && (
                    <Chip
                      className={classes.positive_count}
                      size='small'
                      label={item?.count_green !== null ? item?.count_green : ''}
                      title={ptitle || ''}
                    />
                  )}
                  {item?.count_red >= 0 && item?.count_red !== null && (
                    <Chip
                      className={classes.negative_count}
                      size='small'
                      label={item?.count_red}
                      title={ntitle || ''}
                    />
                  )}
                  {item?.count_blue >= 0 && item?.count_blue !== null && (
                    <Chip
                      className={classes.info_count}
                      size='small'
                      label={item?.count_blue}
                      title={ntitle || ''}
                    />
                  )}
                  {item?.count_default >= 0 && item?.count_default !== null && (
                    <Chip
                      className={classes.default_count}
                      size='small'
                      label={item?.count_default}
                      title={ntitle || ''}
                    />
                  )}
                  {item?.info >= 0 && (
                    <Chip
                      className={classes.info_count}
                      size='small'
                      label={item?.info}
                      title={ntitle || ''}
                    />
                  )}
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
        );
      } else {
        return (
          <Box className={classes.noDataTag}>
            {/* {console.log(totalReportData?.studentReport?.section_name === 'Teacher Attendance Report', 'mobile4444')} */}
            {isNoRecords ? (
              <>
                <Box style={{ fontSize: '1.2rem' }}>☹️</Box>
                <Box>No Records</Box>
              </>
            ) : data.section_name === 'Teacher Attendance Report' ? (
              <>
                <Box>
                  Click on{' '}
                  <IconButton
                    onClick={(e) => handleGetData(e)}
                    disableFocusRipple
                    style={{ padding: 0 }}
                  >
                    <SyncIcon />{' '}
                  </IconButton>{' '}
                  to get data{' '}
                </Box>
              </>
            ) : " "}
          </Box>
        );
      }
    }
  };

  

  return (
    <Card className={`dashboardWidget ${classes.root}`} variant='outlined'>
      <CardHeader
        className={classes.cardHeader}
        titleTypographyProps={{
          className: classes.title,
          variant: 'h6',
          color: 'secondary',
        }}
        title={title}
        action={
          <ReportAction
            setReports={setReports}
            totalReportData={totalReportData}
            title={title}
            data={data}
            syncActive={syncActive}
          />
        }
        avatar={
          <Avatar
            aria-label='report-title'
            style={{ backgroundColor: '#ffffff', color: '#FF6B6B' }}
          >
            {console.log(avatar.map, 'dd')}
            {avatar ? React.createElement(avatar, { fontSize: 'small' }) : null}
          </Avatar>
        }
      />
      <CardContent className={classes.cardBody}>{renderReportData()}</CardContent>
    </Card>
  );
};

export default ReportStatsWidget;
