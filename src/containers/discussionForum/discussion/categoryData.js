const categoryData = [
  {
    id: 1,
    status: 'active',
    academicYear: '2018-2019',
    category: 'Sport',
    subCategory: 'Football',
    subSubCategory: 'LALIGA',
    backgroundColor: '#EFFFB2',
  },
  {
    id: 2,
    status: 'inactive',
    academicYear: '2017-2018',
    category: 'Sport1',
    subCategory: 'Football',
    subSubCategory: 'LALIGA',
    backgroundColor: '#D5FAFF',
  },
  {
    id: 3,
    status: 'active',
    academicYear: '2019-2020',
    category: 'Sport2',
    subCategory: 'Crecket',
    subSubCategory: 'LALIGA',
    backgroundColor: '#CEFFCF',
  },
  {
    id: 4,
    status: 'active',
    academicYear: '2018-2019',
    category: 'Subject',
    subCategory: 'English',
    subSubCategory: 'LALIGA',
    backgroundColor: '#FFC4BB',
  },
  {
    id: 6,
    status: 'active',
    academicYear: '2020-2021',
    category: 'Subject',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
  {
    id: 7,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Earth',
    subCategory: 'Environmental topics',
    subSubCategory: 'LALIGA',
    backgroundColor: '#CCF0FF',
  },
  {
    id: 8,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Earth',
    subCategory: 'Environmental topics',
    subSubCategory: 'LALIGA',
    backgroundColor: '#FFCEF9',
  },
  {
    id: 9,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Category4',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#EFFFB2',
  },
  {
    id: 10,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Category4',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#D5FAFF',
  },
  {
    id: 11,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Category4',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#CEFFCF',
  },
  {
    id: 12,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Category1',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#FFC4BB',
  },
  {
    id: 13,
    status: 'active',
    academicYear: '2020-2021',
    category: 'Category2',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
  {
    id: 14,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Category4',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#CEFFCF',
  },
  {
    id: 15,
    status: 'active',
    academicYear: '2020-2021',
    category: 'Category3',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
  {
    id: 16,
    status: 'active',
    academicYear: '2020-2021',
    category: 'Category4',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
  {
    id: 17,
    status: 'inactive',
    academicYear: '2020-2021',
    category: 'Category5',
    subCategory: 'Hindi',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
  {
    id: 18,
    status: 'active',
    academicYear: '2020-2021',
    category: 'Category6',
    subCategory: 'Football',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
  {
    id: 19,
    status: 'active',
    academicYear: '2019-2020',
    category: 'Category7',
    subCategory: 'maths',
    subSubCategory: 'LALIGA',
    backgroundColor: '#E8CDFF',
  },
];

export default categoryData;
