export const discussionData = [
  {
    category_id: 1,
    category: 'Sports',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Who is responsible for spreading a rumour Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam last line overflow hidden test',
    likes: 3,
    ans: 2,
    awards: 3,
    attachments: 2,
    firstname: 'Rahul',
    lastname: 'Khanna',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 2,
        likes: 3,
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
            likes: 3,
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 3,
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_count: 5,
        likes: 2,
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
            likes: 7,
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 3,
          },
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
            likes: 3,
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 3,
          },
        ],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 1,
        likes: 3,
        commnet_reply: [],
      },
      {
        comment_id: 4,
        commnet: 'Parent comments',
        firstname: 'Rutuja',
        lastname: 'Mehta',
        commnet_count: 2,
        likes: 2,
        commnet_reply: [
          {
            comment_id: 21,
            commnet: 'Child comments',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 3,
            commnet_reply: [
              {
                comment_id: 3,
                commnet: 'child child comments',
                firstname: 'Nitesh',
                lastname: 'Mehta',
                commnet_count: 1,
                likes: 3,
              },
            ],
          },
        ],
      },
    ],
  },
  {
    category_id: 2,
    category: 'Subject',
    sub_category: 'History',
    sub_sub_category: 'War',
    title: 'The best resolved issues of 2020 as a result of pandemic',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 0,
    ans: 0,
    awards: 0,
    attachments: 0,
    firstname: 'Smriti',
    lastname: 'Khanna',
    username: 'rahulkhanna',
    userid: 2013,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 2,
        likes: 1,
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
            commnet_count: 2,
            likes: 4,
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 2,
          },
        ],
      },
    ],
  },
  {
    category_id: 3,
    category: 'Sports',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title:
      'The best resolved issues of 2020 as a result of pandemic responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 8,
    ans: 21,
    awards: 3,
    attachments: 2,
    firstname: 'Smriti',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 2,
        likes: 3,
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
            likes: 3,
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 3,
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_count: 2,
        likes: 3,
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 2,
        likes: 3,
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
            likes: 3,
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 2,
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 4,
    category: 'Sports',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 30,
    ans: 2,
    awards: 3,
    attachments: 2,
    firstname: 'Rahul',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [],
  },
  {
    category_id: 11,
    category: 'Sports',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title: 'The best resolved issues of 2020 as a result of pandemic',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 13,
    ans: 3,
    awards: 3,
    attachments: 2,
    firstname: 'Rahul',
    lastname: 'Khanna',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 2,
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_count: 0,
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 1,
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_count: 0,
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 23,
    category: 'Sports',
    sub_category: 'Crecket',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 1,
    ans: 4,
    awards: 1,
    attachments: 0,
    firstname: 'Kahul',
    lastname: 'Khanna',
    username: 'rahulkhanna',
    userid: 2016,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 25,
    category: 'Subjects',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 34,
    ans: 232,
    awards: 32,
    attachments: 2,
    firstname: 'Mehta',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 26,
    category: 'Subjects',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 34,
    ans: 232,
    awards: 32,
    attachments: 2,
    firstname: 'Mehta',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 27,
    category: 'Subjects',
    sub_category: 'Football',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 34,
    ans: 232,
    awards: 32,
    attachments: 2,
    firstname: 'Mehta',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 28,
    category: 'Subjects',
    sub_category: 'English',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 34,
    ans: 232,
    awards: 32,
    attachments: 2,
    firstname: 'Mehta',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 29,
    category: 'Subjects',
    sub_category: 'Maths',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 34,
    ans: 232,
    awards: 32,
    attachments: 2,
    firstname: 'Mehta',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
  {
    category_id: 30,
    category: 'Environmental topics',
    sub_category: 'Earth',
    sub_sub_category: 'LALIGA',
    title: 'Who is responsible for spreading a rumour ?',
    paragraph:
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam volup Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam',
    likes: 34,
    ans: 232,
    awards: 32,
    attachments: 2,
    firstname: 'Mehta',
    lastname: 'Mehta',
    username: 'rahulkhanna',
    userid: 2010,
    date: '25.05.2020',
    time: '16:34',
    replies: [
      {
        comment_id: 1,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 12,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Rujuta',
            lastname: 'Kelkar',
          },
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 2,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Rujuta',
        lastname: 'Kelkar',
        commnet_reply: [],
      },
      {
        comment_id: 3,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [
          {
            comment_id: 21,
            commnet:
              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
            firstname: 'Smriti',
            lastname: 'Kelkar',
          },
        ],
      },
      {
        comment_id: 4,
        commnet:
          'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod',
        firstname: 'Smriti',
        lastname: 'Mehta',
        commnet_reply: [],
      },
    ],
  },
];
