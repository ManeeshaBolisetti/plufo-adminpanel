import React, { useEffect, useState, useContext } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {
  SvgIcon,
  Button,
  Grid,
  FormControl,
  TextField,
  Divider,
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Layout from '../Layout';
import Filter from '../../assets/images/Filter.svg';
import Activity from '../../assets/images/activity.svg';
import Ask from '../../assets/images/Ask.svg';
import Clearall from '../../assets/images/🔍-Product-Icons.svg'
import Filtericon from '../../assets/images/Filter Icon.svg'
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Enviroment from './enviromentCard';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Viewmore from './viewMore';


import Pagination from '@material-ui/lab/Pagination';
import './discussionForum.scss';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { withRouter } from 'react-router-dom';

const Discussionforum = (props) => {
  const { match } = props;
  const [categoryListRes, setcategoryListRes] = useState([]);
  const [branch, setBranchRes] = useState([]);
  const [gradeRes, setGradeRes] = useState([]);
  const [postListRes, setPostListRes] = useState([]);
  const [categoryValue, setCategoryValue] = useState('');
  const [branchValue, setBranchValue] = useState(null);
  const [gradeValue, setGradeValue] = useState('');
  const [isViewmoreView, setisViewmoreView] = useState(false);
  const [viewMoreList, setViewMoreList] = useState(null);
  const [PostListResPagenation, setPostListResPagenation] = useState(null);
  const [page, setPage] = React.useState(1);
  const [anstrue, setanstrue] = React.useState(false);
  const { setAlert } = useContext(AlertNotificationContext);

  useEffect(() => {
    const getCategoryList = () => {
      axiosInstance
        .get(endpoints.discussionForum.categoryList)
        .then((res) => {
          setcategoryListRes(res.data.result);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    getCategoryList();
    getBranch();
  }, []);

  const getBranch = () => {
    axiosInstance
      .get(endpoints.discussionForum.branch)
      .then((res) => {
        if (res.data.data) {
          setBranchRes(res.data.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChangeBranch = (value) => {
    if (value) {
      setBranchValue(value);
      axiosInstance
        .get(`${endpoints.discussionForum.grade}?branch_id=${value.id}&module_id=8`)
        .then((res) => {
          if (res.data.data) {
            setGradeRes(res.data.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      setBranchValue(null);
    }
  };

  const handleGradeChange = (value) => {
    setGradeValue(value);
    if (value) {
      setGradeValue(value);
    } else {
      setGradeValue(null);
    }
  };

  const handleCategoryChange = (value) => {
    if (value) {
      setCategoryValue(value);
    } else {
      setCategoryValue(null);
    }
  };

  const callFilter = () => {
    const body = {
      categoryValue: categoryValue ? categoryValue : '',
      gradeValue: gradeValue ? gradeValue : '',
    };
    filterValid(body);
  };

  const filterValid = (body) => {
    // console.log(body.gradeValue.grade_id, "body")
    if (body && body.gradeValue !== null && body.categoryValue.id === undefined) {
      axiosInstance
        .get(
          `${endpoints.discussionForum.filterCategory}?grade=${gradeValue.grade_id}&page=${page}`
        )
        .then((res) => {
          if (res.data.status_code === 200) {
            setPostListRes(res.data.data.results);
            setPostListResPagenation(res.data.data.results);
          } else {
            setAlert('error', res.data.message);
          }
        })
        .catch((err) => {
          setAlert('error', err.message);

          console.log(err);
        });
    } else if (
      body &&
      body.categoryValue.id !== undefined &&
      body.gradeValue.grade_id === undefined
    ) {
      axiosInstance
        .get(
          `${endpoints.discussionForum.filterCategory}?category=${categoryValue.id}&page=${page}`
        )
        .then((res) => {
          if (res.data.status_code === 200) {
            setPostListRes(res.data.data.results);
            setPostListResPagenation(res.data.data.results);
          }
          // else {
          //     setAlert('error', res.data.message)
          // }
        })
        .catch((err) => {
          setAlert('error', err.message);
          console.log(err);
        });
    } else if (body.categoryValue.id && body.gradeValue.grade_id) {
      axiosInstance
        .get(
          `${endpoints.discussionForum.filterCategory}?category=${categoryValue.id}&grade=${gradeValue.grade_id}&page=${page}`
        )
        .then((res) => {
          if (res.data.status_code === 200) {
            setPostListRes(res.data.data.results);
            setPostListResPagenation(res.data.data.results);
          } else {
            setAlert('error', res.data.message);
          }
        })
        .catch((err) => {
          setAlert('error', err.message);
          console.log(err);
        });
    }
  };

  useEffect(() => {
    getPostList();
  }, [...postListRes]);

  const getPostList = () => {
    axiosInstance
      .get(`${endpoints.discussionForum.filterCategory}?&page=${page}`)
      .then((res) => {
        if (res.data.status_code === 200) {
          setPostListRes(res.data.data.results);
          setPostListResPagenation(res.data.data);
        } else {
          setAlert('error', res.data.message);
        }
      })
      .catch((err) => {
        setAlert('error', err.message);
        console.log(err);
      });
  };

  const clearAll = () => {
    setBranchValue(null);
    setCategoryValue(null);
    setGradeValue(null);
    getPostList();
  };
  const handleViewmore = (list, ansTrue) => {
    setViewMoreList(list);
    setisViewmoreView(true);
    setanstrue(ansTrue);
  };
  const handlePageChange = (event, value) => {
    setPage(value);
  };

  const deletPost = (id, index) => {
    axiosInstance
      .delete(`${endpoints.discussionForum.deletePost}${id}/update-post/`)
      .then((res) => {
        if (res.data.status_code === 200) {
          setAlert('success', res.data.message);
          const postList = postListRes;
          postList.splice(index, 1);
          setPostListRes(postList);
        }
      })
      .catch((err) => {
        setAlert('error', err.message);
        console.log(err);
      });
  };
  const back = () => {
    setisViewmoreView(false);
  };

  const navigateToCreatePage = () => {
    props.history.push(`${match.url}/create`);
  };

  const validate = (formData) => {
    let input = formData;
    let errors = {};
    let isValid = true;
    if (!input['categoryValue']) {
      isValid = false;
      errors['categoryValue'] = 'Please enter your event category.';
    }

    if (!input['gradeValue']) {
      isValid = false;
      errors['gradeValue'] = 'Please enter your event grade.';
    }

    let errorPayload = {
      errors,
      isValid,
    };

    return errorPayload;
  };
  return (
      <Layout>
          <>
              <div className="bread-crumbs-container ds-forum" onClick={back}>
                  <CommonBreadcrumbs
                      componentName='Discussion Forum'
                      childComponentName={isViewmoreView ? "Post" : ''}
          />
        </div>
              {  !isViewmoreView && (
<div className="df-container" >
                    <Grid container spacing={2}>
                        <Grid item xs={10} style={{ display: 'flex', marginTop: 30, marginLeft: 20, borderBottom: '1px solid #E2E2E2', }}>
                            <div className="branch-dropdown">
                                <Grid item xs={4} sm={2} style={{ marginBottom: 20 }}>
                                    <FormControl className={`select-form`}>
                                        <Autocomplete
                                            // {...defaultProps}
                                            style={{ width: 350 }}
                                            // multiple
                                            value={branchValue}
                                            id="tags-outlined"
                                            options={branch}
                                            getOptionLabel={(option) => option.branch_name}
                                            filterSelectedOptions
                                            size="small"
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="outlined"
                                                    label="Branch"

                                                />
                                            )}
                                            onChange={(e, value) => {
                                                handleChangeBranch(value);
                                            }}
                                            getOptionSelected={(option, value) => value && option.id == value.id}
                                        />
                                        {/* <FormHelperText style={{marginLeft: '20px', color: 'red'}}>{error && error.errorMessage && error.errorMessage.branchError}</FormHelperText> */}

                                    </FormControl>

                                </Grid>
                            </div>
                            <div className="grade-dropdown">
                                <Grid item xs={4} sm={2}>
                                    <FormControl className={`subject-form`}>
                                        <Autocomplete
                                            // {...defaultProps}
                                            style={{ width: 350, marginLeft: 20 }}
                                            // multiple
                                            required={true}
                                            value={gradeValue}
                                            id="tags-outlined"
                                            options={gradeRes}
                                            getOptionLabel={(option) => option.grade__grade_name}
                                            filterSelectedOptions
                                            size="small"
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="outlined"
                                                    label="Grade"

                                                />
                                            )}
                                            onChange={(e, value) => {
                                                handleGradeChange(value);
                                            }}
                                            getOptionSelected={(option, value) => value && option.id == value.id}
                                        />
                                        {/* <FormHelperText style={{marginLeft: '20px', color: 'red'}}>{error && error.errorMessage && error.errorMessage.erp_gradeError}</FormHelperText> */}
                                    </FormControl>
                                </Grid>
                            </div>

                            <div className="category-dropdown">
                                <Grid item xs={4} sm={2}>
                                    <FormControl className={`select-form`}>
                                        <Autocomplete
                                            style={{ width: 350 }}
                                            // multiple
                                            value={categoryValue}
                                            id="tags-outlined"
                                            options={categoryListRes}
                                            getOptionLabel={(option) => option.category_name}
                                            filterSelectedOptions
                                            size="small"
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    variant="outlined"
                                                    label="Category"

                                                />
                                            )}
                                            onChange={(e, value) => {
                                                handleCategoryChange(value);
                                            }}
                                            getOptionSelected={(option, value) => value && option.id == value.id}
                                        />
                                        {/* <FormHelperText style={{marginLeft: '20px', color: 'red'}}>{error && error.errorMessage && error.errorMessage.branchError}</FormHelperText> */}

                                    </FormControl>
                                    {/*  */}
                                </Grid>
                            </div>





                        </Grid>
                        <Grid item xs={10} style={{ display: 'flex', marginLeft: '20px' }} divider className="df">
                            <div className="df-btn-container">
                                <div className="df-clear-all">
                                    <Button variant="contained" className="df-clear" onClick={clearAll}>
                                        <SvgIcon
                                            component={() => (
                                                <img
                                                    style={{ width: '12px', marginRight: '5px' }}
                                                    src={Clearall}
                                                    alt='given'
                                                />
                                            )}
                                        />
                                    Clear All</Button>
                                </div>
                                <div className="df-filter">
                                    <Button variant="contained" color="secondary" onClick={callFilter}>
                                        <SvgIcon
                                            component={() => (
                                                <img
                                                    style={{ width: '12px', marginRight: '5px' }}
                                                    src={Filter}
                                                    alt='given'
                                                />
                                            )}
                                        />

                                Filter
                            </Button>
                                </div>
                            </div>

                        </Grid>


                    </Grid>
                </div>
)
        )}
              {   !isViewmoreView && (
<div className="env-container" >
                    <Grid item xs={11} className="catname-df-forum">
                        <div className="env-name" style={{ display: 'flex', justifyContent: 'space-between', borderBottom: '1px solid #E2E2E2' }}>

                            <div className="cat-name">
                                {
                                    postListRes && postListRes.map((catName, index) => {
                                        return (
                                            <Grid item xs={2} className="catname-df-forum">
                                                <span style={{ fontSize: '16px', color: index === 0 ? '#FF6B6B' : '#014B7E', marginLeft: index !== 0 ? 15 : null }}>{catName.categories.category_name.charAt(0).toUpperCase() + catName.categories.category_name.slice(1)}</span>
                                                {index === 0 && <mark><span className="tab-names" style={{ border: '4px solid red', display: 'block', width: '50px' }}></span></mark>}
                                            </Grid>
                                        )
                                    })
                                }
                            </div>
                            <Divider variant="middle" />

                            <div style={{ display: 'flex' }}>
                                <span style={{ color: '#014B7E', fontSize: '18px', paddingTop: 10, marginLeft: 238, fontWeight: 600 }}>Number of discussion: {postListRes && postListRes.length}</span>
                                <div className="df-btn-question-container" style={{ display: 'flex' }} >
                                    <div className="df-ask">
                                        <Button variant="contained"
                                            style={{ color: 'red' }}
                                            onClick={navigateToCreatePage}
                                            //   href={`${match.url}/create`}
                                            color='primary'>
                                            <SvgIcon
                                                component={() => (
                                                    <img
                                                        style={{ width: '9px', marginRight: '5px' }}
                                                        src={Ask}
                                                        alt='given'
                                                    />
                                                )}
                                            />



                                                  Ask</Button>
                                    </div>
                                    <div className="df-activity">
                                        <Button variant="contained" color="secondary">
                                            <SvgIcon
                                                component={() => (
                                                    <img
                                                        style={{ width: '12px', marginRight: '5px' }}
                                                        src={Activity}
                                                        alt='given'
                                                    />
                                                )}
                                            />

                                Activity
                            </Button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </Grid>

                    <div className="env-card-container" >
                        {
                          postListRes && postListRes.length > 0 ?  postListRes && postListRes.map((list, index) =>{
                            return <Grid container  >
                            {/* <Grid item xs={11} className="ev-view-card"> */}
                                {/* <Grid item xs={2} className="ev-view-card"> */}
                                    <Enviroment list={list} index={index} handleViewmore={handleViewmore}
                                        deletPost={deletPost}
                                    />
                                {/* </Grid> */}
                            {/* </Grid> */}
                        </Grid>
                          })
                       : <span style={{color: '#042955', marginTop: '50px'}}>No Results Found</span> }

                    </div>
                </div>
)
        )}

              <div className="view-more-container">
                  {
                        isViewmoreView && <Viewmore viewMoreList={viewMoreList} anstrue={anstrue} />
                    }
        </div>
              <div className="pagination-cont">
                  { !isViewmoreView && (
<Pagination
                        onChange={handlePageChange}
                        count={Math.ceil(PostListResPagenation && PostListResPagenation.total_pages )} color="secondary" 
                        />
)}
                </div>
          )}
        </div>
      </>
    </Layout>
  );
};

export default withRouter(Discussionforum);
