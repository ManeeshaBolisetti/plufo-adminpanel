import React from 'react';

const Hoverlist = () => {
  return (
    <div className='hide'>
      <div className='cards'>
        <img
          style={{ width: 50, height: 50 }}
          src='https://i.pinimg.com/originals/96/65/de/9665dec76344cadf64d6f2cf62ed24e8.jpg'
          alt='Avatar'
        />
        <div className='containers'>
          <h4>
            <b>John Doe</b>
          </h4>
          <p>Architect & Engineer</p>
        </div>
      </div>
    </div>
  );
};

export default Hoverlist;
