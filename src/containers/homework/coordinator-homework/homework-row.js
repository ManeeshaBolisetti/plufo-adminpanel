import React from 'react';
import { useHistory } from 'react-router-dom';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import HomeworkCol from './homework-col';
import moment from 'moment';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  dayicon : {
    width: '40px',
    height: '40px',
    borderRadius: '25px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '1rem',
    fontWeight: 600,
    border: `1px solid  rgba(251,57,101,255) !important`,
    '@media screen and(max-width:768px)': {
      width: '30px',
      height: '30px',
      borderRadius: '25px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginRight: '1rem',
      fontWeight: 600,
      color: 'rgba(251,57,101,255)',
      fontSize: '12px',
      border: `1px solid rgba(251,57,101,255)`,
    }
  },
}))

const HomeworkRow = ({ data, cols, selectedCol, sectionId, setSelectedCol, handleViewHomework, coord_selected_teacher_id,selectedSectionMapping}) => {
  const history = useHistory();
  const classes = useStyles()
  const navigateToAddScreen = ({ date, sessionYear, branch, grade, subject, subjectId }) => {
    history.push(`/homework/cadd/${sessionYear}/${branch}/${grade}/${date}/${subject}/${subjectId}/${coord_selected_teacher_id}/${selectedSectionMapping}`);
  };
  return (
    <TableRow>
      {cols.map((col) => {
        const isSelected =
          selectedCol.date === data.date && selectedCol.subject === col.subject_name;
        return typeof col === 'object' ? (
          <HomeworkCol
            key={col.id}
            data={data[col.subject_name]}
            isSelected={isSelected}
            canUpload={data.canUpload}
            handleClick={(view) => {
              setSelectedCol({
                date: data.date,
                subject: col.subject_name,
                subjectId: col.subject_id,
                sectionId: sectionId,
                homeworkId: data[col.subject_name].hw_id,
                view,
                coord_selected_teacher_id
              });
            }}
            handleNavigationToAddScreen={() => {
              navigateToAddScreen({
                date: data.date,
                //section: sectionId,
                sessionYear: data.sessionYear,
                branch: data.branch,
                grade: data.grade,
                subject: col.subject_name,
                subjectId: col.subject_id,
                coord_selected_teacher_id
              });
            }}
            handleViewHomework={() => {
              handleViewHomework({
                date: data.date,
                subject: col.subject_name,
                subjectId: col.subject_id,
                homeworkId: data[col.subject_name].hw_id,
                coord_selected_teacher_id
              });
            }}
          />
        ) : (
          <TableCell className='no-wrap-col' style={{ minWidth: '188px'}}>
            <div>
              <div className={classes.dayicon}>
                {moment(data.date).format('dddd').split('')[0]}
              </div>
              {data.date}
            </div>
          </TableCell>
        );
      })}
    </TableRow>
  );
};

export default HomeworkRow;
