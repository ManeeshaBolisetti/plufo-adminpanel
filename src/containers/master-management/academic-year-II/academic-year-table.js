/* eslint-disable react/jsx-no-duplicate-props */
import React, { useContext, useEffect, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import IconButton from '@material-ui/core/IconButton';
import {
  Grid,
  TextField,
  Button,
  Tooltip,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Switch,
  Typography,
  FormControl,
  Select,
  MenuItem,
} from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import TablePagination from '@material-ui/core/TablePagination';
import Layout from '../../Layout';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../../components/common-breadcrumbs/breadcrumbs';
import endpoints from '../../../config/endpoints';
import axiosInstance from '../../../config/axios';
// import CreateAcademicYear from './create-academic-year';
// import EditAcademicYear from './edit-academic-year';
import '../master-management.css';
import './style.scss';
import Loading from '../../../components/loader/loader';
import AcademicYearCard from './academic-year-card';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Autocomplete, Pagination } from '@material-ui/lab';
import FilterIcon from '../../../assets/images/filter.png';
import AddEditDialog from './add-edit-dialog.jsx';
import axios from 'axios';
import clsx from 'clsx';
import Testpagination from '../../../components/tablePagination';
import Filter from './filter.jsx';
import displayName from '../../../config/displayName';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    boxShadow: 'none',
    margin: '5px 0 0 0',
  },
  container: {
    maxHeight: '70vh',
  },
  buttonContainer: {
    background: theme.palette.background.secondary,
    paddingBottom: theme.spacing(2),
  },
  centerInMobile: {
    width: '100%',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center',
    },
  },
  columnHeader: {
    color: `${theme.palette.secondary.main} !important`,
    fontWeight: 600,
    fontSize: '1rem',
    backgroundColor: `#ffffff !important`,
  },
  tableCell: {
    color: theme.palette.secondary.main,
    padding: 0,
  },
  filterContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    padding: '0 45px',
    margin: '-45px 0 0 0',
    '@media (max-width: 900px)': {
      margin: '-15px 0 0 0',
    },
  },
  filterAccordion: {
    width: '50%',
    backgroundColor: 'transparent',
    '@media (max-width: 600px)': {
      width: '100%',
    },
    '@media (min-width: 601px) and (max-width: 900px)': {
      width: '65%',
    },
    '@media (min-width: 901px) and (max-width: 1350px)': {
      width: '80%',
    },
  },
  createMeetingButton: {
    margin: '0 0 0 10px',
  },
  filterContent: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    flexGrow: 1,
    '@media (max-width: 900px)': {
      display: 'grid',
      gap: '20px',
      margin: '0 0 0 50px',
    },
  },
  filterAutocomplete: {
    width: '70%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  filterButton: {
    width: '20%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
    // margin: '5px 0 0 0',
  },
  isSearchFilter: {
    width: '25%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
  },
  isActiveFilter: {
    width: '20%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
  },
  isOrderingFilter: {
    width: '30%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
  },
  tableHeaderCell: {
    textTransform: 'capitalize',
  },
  hideTableHeaderCells: {
    display: 'none',
  },
  container: {
    overflow: 'none !important',
  },
}));

// const columns = [
//   { id: 'session_year', label: 'Session Year', minWidth: 100 },
//   {
//     id: 'actions',
//     label: 'Actions',
//     minWidth: 170,
//     align: 'right',
//     labelAlign: 'center',
//   },
// ];

const AcademicYearTableII = () => {
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const [page, setPage] = React.useState(1);
  const [academicYear, setAcademicYear] = useState([]);
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const [yearId, setYearId] = useState();
  const [sessionYear, setSessionYear] = useState('');
  const [addFlag, setAddFlag] = useState(false);
  const [editFlag, setEditFlag] = useState(false);
  const [tableFlag, setTableFlag] = useState(true);
  const [delFlag, setDelFlag] = useState(false);
  const [loading, setLoading] = useState(false);
  const [totalCount, setTotalCount] = useState(0);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [accordianOpen, setAccordianOpen] = useState(false);
  const [modalOpenFor, setModalOpenFor] = useState('Add');
  const [sessionYearEdit, setSessionYearEdit] = useState(null);
  const [tableHeader, setTableHeader] = useState(null);
  const [isSearch, setIsSearch] = useState('');
  const [isActive, setIsActive] = useState('');
  const [isOrdering, setIsOrdering] = useState('');
  const [isActiveSelectedValue, setIsActiveSelectedValue] = useState(null);
  const [isOrderingSelectedValue, setIsOrderingSelectedValue] = useState(null);

  const isActiveValue = [
    { id: 0, name: 'All', label: 'All' },
    { id: 1, name: 'false', label: 'False' },
    { id: 2, name: 'true', label: 'True' },
  ];
  const isOrderingValue = [
    { id: 0, name: 'session_year', label: 'Session Year(Asc)' },
    { id: 1, name: '-session_year', label: 'Session Year(Desc)' },
    { id: 2, name: 'is_active', label: 'Is Active(Asc)' },
    { id: 3, name: '-is_active', label: 'Is Active(Desc)' },
  ];
  const limit = 10;
  const [goBackFlag, setGoBackFlag] = useState(false);
  const [data, setData] = useState({
    dataPerPage: 10,
    totalData: null,
    totalPages: null,
    currentPage: 1,
  });
  const themeContext = useTheme();
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const wider = isMobile ? '-10px 0px' : '-10px 0px 20px 8px';
  const widerWidth = isMobile ? '98%' : '95%';

  let url = '';
    if (displayName === 'My SparkleBox') {
     url = 'https://dev.erp.mysparklebox.com/qbox/academic-year';
    } else if (displayName === 'SparkleBox School') {
      url = 'https://qa.plufo.letseduvate.com/qbox/academic-year';
    } else {
      url = 'https://qa.plufo.letseduvate.com/qbox/academic-year';

    }

  const handleChangePage = (event, newPage) => {
    setPage(newPage + 1);
  };

  const handleAddYear = () => {
    setDialogOpen(true);
    setModalOpenFor('Add');
    setSessionYearEdit('');
    // setTableFlag(false);
    // setAddFlag(true);
    // setEditFlag(false);
  };

  const handleEditYear = (year) => {
    // setTableFlag(false);
    // setAddFlag(false);
    // setEditFlag(true);
    // console.log(id, year, startDate, endDate, 'hiiiii');
    setModalOpenFor('Edit');
    // setYearId(id);
    setSessionYear(year);
    setSessionYearEdit(year);
    setDialogOpen(true);
  };

  const handleGoBack = () => {
    setPage(1);
    setTableFlag(true);
    setAddFlag(false);
    setEditFlag(false);
    setGoBackFlag(!goBackFlag);
  };

  const handleDeleteYear = (e) => {
    e.preventDefault();
    setLoading(true);
    axiosInstance
      .put(endpoints.masterManagement.updateAcademicYear, {
        is_delete: true,
        academic_year_id: yearId,
      })
      .then((result) => {
        if (result.data.status_code === 200) {
          setDelFlag(!delFlag);
          setLoading(false);
          setAlert('success', result.data.message);
        } else {
          setLoading(false);
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
    setOpenDeleteModal(false);
  };

  const handleDelete = (year) => {
    setSessionYear(year.session_year);
    handleOpenDeleteModal(year.id);
  };

  const handleOpenDeleteModal = (id) => {
    setYearId(id);
    setOpenDeleteModal(true);
  };
  const handleCloseCreateModal = () => {
    setDialogOpen(false);
  };
  const handleCloseDeleteModal = () => {
    setOpenDeleteModal(false);
    setDialogOpen(false);
  };

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 450);
  }, [page, delFlag, goBackFlag]);

  // useEffect(() => {
  //   axios
  //     .get(`https://qa.plufo.letseduvate.com/qbox/academic-year/`)
  //     .then((result) => {
  //       // if (result.data.status_code === 200) {
  //       //   {
  //       //     setTotalCount(result.data.result.count);
  //       //     setAcademicYear(result.data.result.results);
  //       //   }
  //       console.log(result.data.result);
  //     })
  //     .catch((error) => {
  //       // setAlert('error', error.message);
  //       console.log(error);
  //     });
  // }, []);
  const createHeaders = (data) => {
    let headers = Object.keys(data).map((eachData) => eachData.replace('_', ' '));
    console.log(headers);

    setTableHeader(headers);
  };
  const handleAcademicYear = (page) => {
    // axiosInstance
    //   .get(
    //     `${endpoints.masterManagementNew.academicYear1}?page=${page}&page_size=${limit}`
    //   )
    
    axios
      .get(
        `${url}/?search=${isSearch}&is_active=${isActive}&ordering=${isOrdering}&page=${
          page !== undefined ? page : data.currentPage
        }&page_size=${data.dataPerPage}`
      )
      .then((result) => {
        if (result.status === 200) {
          {
            console.log(result.data.count);
            console.log(result);
            createHeaders(result.data.result[0]);
            setTotalCount(result.data.count);
            setAcademicYear(result.data.result);
            setData({
              ...data,
              totalData: result.data.count,
              totalPages: result.data.total_pages,
              currentPage: result.data.current_page,
            });
          }
        } else {
          setAlert('error', result.data.error_message);
        }
      })
      .catch((error) => {
        setAlert('error', error.message);
      });
  };

  const handleIsActiveFilter = (event, value) => {
    if (value) {
      // console.log(value.props.children, 'val');
      setIsActive(value.name);
    } else {
      setIsActive('');
    }
  };
  const handleIsOrderingFilter = (event, value) => {
    if (value) {
      setIsOrdering(value.name);
    } else {
      setIsOrdering('');
    }
  };

  useEffect(() => {
    // axiosInstance
    //   .get(`${endpoints.masterManagement.academicYear}?page=${page}&page_size=${limit}`)
    //   .then((result) => {
    //     if (result.data.status_code === 200) {
    //       {
    //         setTotalCount(result.data.result.count);
    //         setAcademicYear(result.data.result.results);
    //       }
    //     } else {
    //       setAlert('error', result.data.error_message);
    //     }
    //   })
    //   .catch((error) => {
    //     setAlert('error', error.message);
    //   });
    // console.log(data.currentPage, 'paginate 11111');
    handleAcademicYear(1);
    console.log(isActive, isActiveSelectedValue, 'ccccccc');
  }, [
    delFlag,
    goBackFlag,
    isActive,
    isOrdering,
    isSearch,
    isActiveSelectedValue,
    isOrderingSelectedValue,
    // page,
  ]);

  useEffect(() => {
    handleAcademicYear(data.currentPage);
  }, [data.currentPage, data.dataPerPage]);

  const handleIsActive = (e, year) => {
    console.log(year);
    console.log(e.target.checked);
    let requestObj = {
      session_year: year.session_year,
      is_active: !year.is_active,
      start_date: year.start_date,
      end_date: year.end_date,
    };
    axios
      .put(`${url}/${year.id}/`, requestObj)
      .then((result) => {
        if (result.status === 200) {
          {
            handleAcademicYear();
            setAlert('success', result.data.message);
          }
        } else {
          setAlert('error', result.data.error_message);
        }
      })
      .catch((error) => {
        setAlert('error', error.message);
      });
  };

  const handleClearFilter = () => {
    setIsActive('');
    setIsOrdering('');
    setIsSearch('');
    setIsActiveSelectedValue(null);
    setIsOrderingSelectedValue(null);
    // setIsActiveValue([]);
    console.log(isActiveValue, 'cleariiiii');
  };

  return (
    <>
      {loading ? <Loading message='Loading...' /> : null}
      <Layout>
        <div>
          <div style={{ width: '95%', margin: '10px auto' }}>
            <CommonBreadcrumbs
              componentName='Master Management'
              childComponentName='Academic Year'
              childComponentNameNext={
                addFlag && !tableFlag
                  ? 'Add Academic Year'
                  : editFlag && !tableFlag
                  ? 'Edit Academic Year'
                  : null
              }
            />
          </div>
          {tableFlag && !addFlag && !editFlag && (
            <div className={classes.filterContainer}>
              {/* <PageSize /> */}
              {/* <Testpagination data={data} setData={setData} /> */}
              {/* <Filter
                isActiveValue={isActiveValue}
                handleIsActiveFilter={handleIsActiveFilter}
                accordianOpen={accordianOpen}
                setAccordianOpen={setAccordianOpen}
              /> */}
              <Accordion
                // style={{ backgroundColor: 'aqua' }}
                elevation={0}
                expanded={accordianOpen}
                className={classes.filterAccordion}
              >
                <AccordionSummary
                  expandIcon={
                    <img
                      src={FilterIcon}
                      alt='filter'
                      width='20px'
                      height='20px'
                      style={{}}
                    />
                  }
                  style={{ backgroundColor: 'transparent' }}
                  aria-controls='panel1a-content'
                  id='panel1a-header'
                  onClick={() => setAccordianOpen(!accordianOpen)}
                ></AccordionSummary>
                <AccordionDetails style={{ backgroundColor: '#ffffff' }} elevation={4}>
                  <div className={classes.filterContent}>
                    <TextField
                      className={classes.isSearchFilter}
                      required
                      label='Session Year'
                      variant='outlined'
                      size='small'
                      autoComplete='off'
                      name='searchText'
                      value={isSearch}
                      onChange={(e) => setIsSearch(e.target.value)}
                    />
                    <Autocomplete
                      value={isActiveSelectedValue}
                      className={classes.isActiveFilter}
                      size='small'
                      // onChange={handleIsActiveFilter}
                      onChange={(event, option) => {
                        setIsActive(option.name);
                        setIsActiveSelectedValue(option);
                      }}
                      id='Is Active'
                      options={isActiveValue}
                      getOptionLabel={(option) => option?.label}
                      filterSelectedOptions
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant='outlined'
                          label='Is Active'
                          placeholder='Is Active'
                        />
                      )}
                    />
                    <Autocomplete
                      value={isOrderingSelectedValue}
                      className={classes.isOrderingFilter}
                      size='small'
                      // onChange={handleIsOrderingFilter}
                      onChange={(event, option) => {
                        setIsOrdering(option.name);
                        setIsOrderingSelectedValue(option);
                      }}
                      id='Is Ordering'
                      options={isOrderingValue}
                      getOptionLabel={(option) => option?.label}
                      filterSelectedOptions
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant='outlined'
                          label='Sort By'
                          placeholder='Sort By'
                        />
                      )}
                    />
                    {/* <FormControl
                      margin='dense'
                      variant='outlined'
                      className='data-count-formcontrol'
                    >
                      <Select
                        value={isActive || ''}
                        name='data-per-page'
                        onChange={handleIsActiveFilter}
                        className='data-count-select'
                      >
                        <MenuItem value={0}>All</MenuItem>
                        <MenuItem value={1}>True</MenuItem>
                        <MenuItem value={2}>False</MenuItem>
                      </Select>
                    </FormControl> */}

                    <Button
                      onClick={() => handleClearFilter()}
                      className={classes.filterButton}
                    >
                      Clear
                    </Button>
                  </div>
                </AccordionDetails>
              </Accordion>

              <Tooltip title='Add Academic Year' placement='bottom' arrow>
                <IconButton
                  className={classes.createMeetingButton}
                  // color='primary'
                  onClick={handleAddYear}
                  style={{ backgroundColor: '#ef676a' }}
                >
                  <AddIcon style={{ color: '#ffffff' }} />
                </IconButton>
              </Tooltip>
              {/* </Grid>
              </Grid> */}
            </div>
          )}
        </div>

        <AddEditDialog
          id={yearId}
          year={sessionYearEdit}
          dialogFor={modalOpenFor}
          dialogOpen={dialogOpen}
          handleCloseCreateModal={handleCloseCreateModal}
          handleGoBack={handleGoBack}
          setLoading={setLoading}
          handleAcademicYear={handleAcademicYear}
        />

        {console.log(tableHeader, 'table')}
        {!isMobile && tableFlag && !addFlag && !editFlag && (
          <Paper className={`${classes.root} common-table`}>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label='sticky table'>
                <TableHead className={classes.tableHeaderRow}>
                  <TableRow>
                    {tableHeader &&
                      tableHeader.map((eachColumn, index) => {
                        return (
                          <TableCell
                            className={clsx([classes.tableHeaderCell], {
                              [classes.hideTableHeaderCells]: eachColumn === 'is active',
                            })}
                            key={index}
                          >
                            {eachColumn === 'id' ? 'Sl No.' : eachColumn}
                          </TableCell>
                        );
                      })}
                    <TableCell>Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {academicYear.map((year, index) => {
                    return (
                      <TableRow hover academicyear='checkbox' tabIndex={-1} key={index}>
                        <TableCell className={classes.tableCell}>
                          {data.currentPage == 1
                            ? index + data.currentPage
                            : (data.currentPage - 1) * data.dataPerPage + (index + 1)}

                          {/* Hello */}
                        </TableCell>
                        <TableCell className={classes.tableCell}>
                          {year.session_year}
                        </TableCell>
                        <TableCell className={classes.tableCell}>
                          {year.start_date}
                        </TableCell>
                        <TableCell className={classes.tableCell}>
                          {year.end_date}
                        </TableCell>
                        <TableCell className={classes.tableCell}>
                          {/* <IconButton
                            onClick={(e) => {
                              handleDelete(year);
                            }}
                            title='Delete Academic Year'
                          >
                            <DeleteOutlinedIcon style={{ color: '#fe6b6b' }} />
                          </IconButton> */}
                          <Switch
                            checked={year.is_active}
                            onChange={(e) => {
                              handleIsActive(e, year);
                            }}
                            inputProps={{ 'aria-label': 'controlled' }}
                          />

                          <IconButton
                            onClick={(e) => handleEditYear(year)}
                            title='Edit Academic Year'
                          >
                            <EditOutlinedIcon style={{ color: '#fe6b6b' }} />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <div className='paginateData'>
              <Testpagination data={data} setData={setData} />{' '}
              {/* <TablePagination
                component='div'
                count={totalCount}
                rowsPerPage={limit}
                page={page - 1}
                onChangePage={handleChangePage}
                rowsPerPageOptions={false}
              /> */}
            </div>
          </Paper>
        )}
        {isMobile && !addFlag && !editFlag && (
          <>
            {academicYear.map((year) => (
              <AcademicYearCard
                year={year}
                handleIsActive={handleIsActive}
                // handleDelete={handleDelete}
                handleEditYear={handleEditYear}
              />
            ))}
            <div className='paginateData paginateMobileMargin'>
              <TablePagination
                component='div'
                count={totalCount}
                rowsPerPage={limit}
                page={page - 1}
                onChangePage={handleChangePage}
                rowsPerPageOptions={false}
              />
            </div>
          </>
        )}
        <Dialog
          open={openDeleteModal}
          onClose={handleCloseDeleteModal}
          aria-labelledby='draggable-dialog-title'
        >
          <DialogTitle
            style={{ cursor: 'move', color: '#014b7e' }}
            id='draggable-dialog-title'
          >
            Toggle Academic Year
          </DialogTitle>
          <DialogContent>
            <DialogContentText>{`Confirm Delete Academic Year ${sessionYear}`}</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDeleteModal} className='labelColor cancelButton'>
              Cancel
            </Button>
            <Button color='primary' onClick={handleDeleteYear}>
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      </Layout>
    </>
  );
};

export default AcademicYearTableII;
