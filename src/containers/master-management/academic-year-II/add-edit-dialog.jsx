import { Dialog, DialogTitle, TextField, Button } from '@material-ui/core';
import React, { useState, useContext, useEffect } from 'react';
import { LocalizationProvider, DateRangePicker } from '@material-ui/pickers-4.2';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';

import endpoints from '../../../config/endpoints';
import axiosInstance from '../../../config/axios';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import moment from 'moment';
import axios from 'axios';
const AddEditDialog = (props) => {
  const {
    id,
    year,
    dialogFor,
    setDialogOpen,
    dialogOpen,
    handleCloseCreateModal,
    setLoading,
    handleAcademicYear,
  } = props;

  const [sessionYear, setSessionYear] = useState({
    id: null,
    value: '',
    helperText: '',
    error: false,
    startDate: '',
    endDate: '',
  });
  const [dateRangeTechPer, setDateRangeTechPer] = useState([
    moment().subtract(6, 'days'),
    moment(),
  ]);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);

  // useEffect(() => {
  //   var date = new Date();
  //   setStartDate(moment(date).format('YYYY-MM-DD'));
  //   setEndDate(moment(date).format('YYYY-MM-DD'));
  // }, []);

  useEffect(() => {
    console.log(props, 'hiiiiii');
    if (dialogFor === 'Add') {
      setSessionYear({ ...sessionYear, value: '', startDate: '', endDate: '' });
    } else {
      setSessionYear({
        ...sessionYear,
        id: year.id,
        value: year.session_year,
        startDate: year.start_date,
        endDate: year.end_date,
      });
      // setStartDate({...startDate})
      // setEndDate({...endDate})
    }
  }, [dialogFor, year]);

  const handleConfirm = () => {
    // let sessionYearPattern = RegExp('^[0-9]{4}-[0-9]{2}');
    // let nameRegex = RegExp('^[A-Z]{1}[a-z]{2,}$');
    //     return nameRegex.test(userName);
    let sessionYearPattern = '^[0-9]{4}-[0-9]{2}';

    if (dialogFor === 'Add') {
      console.log(sessionYear.value, startDate, 'hiiii');
      if (sessionYear.value.match(sessionYearPattern)) {
        axios
          .post(`https://qa.plufo.letseduvate.com/qbox/academic-year/`, {
            session_year: sessionYear.value,
            start_date: sessionYear.startDate,
            end_date: sessionYear.endDate,
          })
          .then((result) => {
            console.log(result, 'adddd');
            if (result.status === 201) {
              {
                handleAcademicYear();
                handleCloseCreateModal();
                setSessionYear({ ...sessionYear, value: '', startDate: '', endDate: '' });
                // setStartDate('');
                // setEndDate('');
                setAlert('success', result.data.message);
              }
            } else {
              setAlert('error', 'This Academic year already exists');
            }
          })
          .catch((error) => {
            setAlert('error', error.message);
          });
      } else {
        setSessionYear({
          ...sessionYear,
          error: true,
          helperText: 'Provide proper pattern',
        });
      }
    } else {
      let request = {
        academic_year_id: sessionYear.id,
        session_year: sessionYear.value,
        start_date: sessionYear.startDate,
        end_date: sessionYear.endDate,
      };
      console.log(request, id, startDate, 'eddddd');
      // request['academic_year_id'] = id;
      // if (sessionYear !== '' && sessionYear !== year) {
      // request['session_year'] = sessionYear;
      if (sessionYear.value.match(sessionYearPattern)) {
        axios
          .put(
            `https://qa.plufo.letseduvate.com/qbox/academic-year/${sessionYear.id}/`,
            request
          )
          .then((result) => {
            if (result.status === 200) {
              handleAcademicYear();
              handleCloseCreateModal();
              setSessionYear({ ...sessionYear, value: '', startDate: '', endDate: '' });
              setAlert('success', result.data.message);
            } else {
              setAlert('error', result.data.message);
            }
          })
          .catch((error) => {
            setAlert('error', error.message);
          });
      } else {
        setSessionYear({
          ...sessionYear,
          error: true,
          helperText: 'Provide proper pattern',
        });
      }
      // }
      // else {
      //   setLoading(false);
      //   setAlert('error', 'No Fields to Update');
      // }
    }
  };
  // function handleDate(v1) {
  //   console.log(v1, 'v1');
  //   if (v1 && v1.length !== 0) {
  //     setStartDate(moment(new Date(v1[0])).format('YYYY-MM-DD'));
  //     setEndDate(moment(new Date(v1[1])).format('YYYY-MM-DD'));
  //   }
  //   setDateRangeTechPer(v1);
  // }
  // inputProps={{maxLength:7,pattern:'^[0-9]{4}-[0-9]{2}'}}
  return (
    <div>
      <Dialog open={dialogOpen}>
        <DialogTitle>{dialogFor} Academic Year</DialogTitle>
        <div className='dialog-form' style={{ padding: '0 24px 24px', display: 'grid' }}>
          <TextField
            // fullWidth
            variant='outlined'
            margin='dense'
            value={sessionYear.value}
            onChange={(e) => setSessionYear({ ...sessionYear, value: e.target.value })}
            label='Academic Year'
            error={sessionYear.error}
            helperText={sessionYear.helperText}
          />
          {/* <LocalizationProvider dateAdapter={MomentUtils}>
            <DateRangePicker
              startText='Select-date-range'
              value={dateRangeTechPer}
              onChange={(newValue) => {
                handleDate(newValue);
              }}
              renderInput={({ inputProps, ...startProps }, endProps) => {
                return (
                  <>
                    <TextField
                      {...startProps}
                      inputProps={{
                        ...inputProps,
                        value: `${inputProps.value} - ${endProps.inputProps.value}`,
                        readOnly: true,
                      }}
                      size='small'
                      style={{ minWidth: '100%' }}
                    />
                  </>
                );
              }}
            />
          </LocalizationProvider> */}
          {console.log(startDate, 'eddddd')}
          <TextField
            required
            label='Start Date'
            variant='outlined'
            margin='dense'
            size='small'
            autoComplete='off'
            name='startDate'
            value={sessionYear.startDate}
            // onChange={(e) => setStartDate(moment(e.target.value).format('YYYY-MM-DD'))}
            onChange={(e) =>
              setSessionYear({
                ...sessionYear,
                startDate: moment(e.target.value).format('YYYY-MM-DD'),
              })
            }
            type='date'
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            required
            label='End Date'
            variant='outlined'
            margin='dense'
            size='small'
            autoComplete='off'
            name='endDate'
            value={sessionYear.endDate}
            // onChange={(e) => setEndDate(moment(e.target.value).format('YYYY-MM-DD'))}
            onChange={(e) =>
              setSessionYear({
                ...sessionYear,
                endDate: moment(e.target.value).format('YYYY-MM-DD'),
              })
            }
            type='date'
            InputLabelProps={{
              shrink: true,
            }}
          />
          <div
            className='actions'
            style={{
              marginTop: '24px',
              width: '75%',
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <Button onClick={() => handleConfirm()}>Confirm</Button>
            <Button onClick={() => handleCloseCreateModal()}>Close</Button>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default AddEditDialog;
