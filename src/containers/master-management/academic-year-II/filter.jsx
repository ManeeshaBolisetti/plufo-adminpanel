import React from 'react';
import {
  Grid,
  TextField,
  Button,
  Tooltip,
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Switch,
  Typography,
} from '@material-ui/core';
import { Autocomplete, Pagination } from '@material-ui/lab';
import FilterIcon from '../../../assets/images/filter.png';
import { makeStyles, useTheme } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  //   filterAccordion: {
  //     width: '20%',
  //   },
  filterContent: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    flexGrow: 1,
  },
  filterAutocomplete: {
    width: '70%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  filterButton: {
    width: '20%',
  },
}));
const Filter = (props) => {
  const classes = useStyles();
  const { accordianOpen, setAccordianOpen, handleIsActiveFilter, isActiveValue } = props;

  return (
    <div>
      <Accordion
        // style={{ backgroundColor: 'aqua' }}
        elevation={0}
        expanded={accordianOpen}
        className={classes.filterAccordion}
      >
        <AccordionSummary
          expandIcon={
            <img src={FilterIcon} alt='filter' width='20px' height='20px' style={{}} />
          }
          style={{ backgroundColor: '#FAFAFA' }}
          aria-controls='panel1a-content'
          id='panel1a-header'
          onClick={() => setAccordianOpen(!accordianOpen)}
        ></AccordionSummary>
        <AccordionDetails style={{ backgroundColor: '#FFFFFF' }} elevation={4}>
          <div className={classes.filterContent}>
            <TextField
              required
              label='Session Year'
              variant='outlined'
              size='small'
              autoComplete='off'
              name='searchText'
              value='2021-22'
              onChange={(e) => console.log(e.target.value)}
            />
            <Autocomplete
              size='small'
              onChange={handleIsActiveFilter}
              id='Is Active'
              options={isActiveValue}
              getOptionLabel={(option) => option?.name}
              filterSelectedOptions
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant='outlined'
                  label='Is Active'
                  placeholder='Is Active'
                />
              )}
            />
            <Button className={classes.filterButton}>Clear</Button>
          </div>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default Filter;
