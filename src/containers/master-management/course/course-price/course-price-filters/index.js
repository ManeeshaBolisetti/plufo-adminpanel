import React, { useState, useContext, useEffect } from 'react';
import { TextField, Grid, useTheme } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import axiosInstance from '../../../../../config/axios';
import endpoints from '../../../../../config/endpoints';
import Loading from '../../../../../components/loader/loader';
import { AlertNotificationContext } from '../../../../../context-api/alert-context/alert-state';
import './course-price-filters.css';

const CoursePriceFilters = (props) => {
  const {
    setCourseId,
    selectedCourse,
    setSelectedCourse,
    courseKey,
    gradeKey,
    setCollectData,
    selectedSubjectId
  } = props;
  const { setAlert } = useContext(AlertNotificationContext);
  const themeContext = useTheme();
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const wider = isMobile ? '-10px 0px' : '-10px 0px 20px 8px';
  const widerWidth = isMobile ? '98%' : '95%';
  const [loading, setLoading] = useState(false);
  const [gradeList, setGradeList] = useState([]);
  const [courseList, setCourseList] = useState([]);
  const [subjectList, setSubjectList] = useState([]);
  const [selectedSubject, setSelectedSubject] = useState('');
  const [selectedGrade, setSelectedGrade] = useState([]);
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)
  const [selectDropDown,setSelectDropDown] =useState([])
  const [gardeId, setGradeId] =useState ([])

  const aolHostURL = window.location.host;

  const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
  const [moduleId, setModuleId] = useState('');


  // console.log(selectedGrade.grade_id,'grade999999')
  // console.log(gardeId,'ipe87788')

  useEffect(() => {
    if (NavData && NavData.length) {
      NavData.forEach((item) => {
        if (
          item.parent_modules === 'Master Management' &&
          item.child_module &&
          item.child_module.length > 0
        ) {
          item.child_module.forEach((item) => {
            if (item.child_name === 'Course Price') {
              setModuleId(item.child_id);
            }
          });
        }
      });
    }
  }, []);

 async function ApiCall(url, type) {
   setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if(data?.status_code === 200 || data?.status_code === 201) {
        if(type === 'grade') {
          setGradeList(data?.data);
          if(gradeKey){
            let gradeIndex = data?.data.findIndex((item)=> item.grade_id == gradeKey);
            if(gradeIndex >= 0){
              setSelectedGrade([data?.data[gradeIndex]]);
              getCourseList([data?.data[gradeIndex]].map(item => item.grade_id));
            }
          }
        } else if(type === 'subject'){
          setSubjectList(data?.result);
          if(selectedSubjectId){
            let findIndex = data?.result.findIndex((item)=> item.id == selectedSubjectId);
            findIndex >=0 && (setSelectedSubject(data?.result[findIndex]));
          }
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error',error.message);
      setLoading(false);
    }
 }

  useEffect(() => {
    if(moduleId && branchId) {
      ApiCall(`${endpoints.communication.subjectList}?branch_id=${branchId}&module_id=${moduleId}`, 'subject');
      ApiCall(`${endpoints.communication.grades}?branch_id=${branchId}&module_id=${moduleId}`, 'grade');
    }
  }, [moduleId,branchId]);

  const handleGrade = (event, value = []) => {
    setSelectedGrade([]);
    setCourseList([]);
    setSelectedCourse('');
    setCollectData();
    setCourseId('');
    if (value && value?.length) {
      // console.log(value?.id,'pppppp')
      setSelectedGrade(value);
      setGradeId(value.map(item => item.grade_id))

      // getCourseList(value.map(item => item.grade_id));
    }
  };

  const handleCourse = (event, value = '') => {
    setSelectedCourse('');
    setCollectData();
    setCourseId('');
    if (value) {
      setSelectedCourse(value);
      setCourseId(value?.id);
      getCourseList()
    }
  };

  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      setSelectDropDown(res.data.data)

    })
  }, [branch]);

  useEffect(() => {
    getCourseList()
    
  }, [selectedSubjectId, selectedSubject?.id])
  const getCourseList = () => {
    if(selectedSubjectId || selectedSubject?.id){
      setLoading(true);
      axiosInstance
        .get(`${endpoints.academics.courses}?branch=${branchId}&grade=${gardeId}&subject=${selectedSubject.id || selectedSubjectId}&module_id=${moduleId}`)
        .then((result) => {
          setLoading(false);
          if (result.data.status_code === 200) {
            setCourseList(result.data?.result);
            if (gradeKey && courseKey) {
              const courseObj = result.data?.result?.find(
                ({ id }) => id === Number(courseKey)
              );
              setSelectedCourse(courseObj);
            }
          } else {
            setCourseList([]);
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setCourseList([]);
          setLoading(false);
          setAlert('error', error.message);
        });

    }
  };

  return (
    <>
    <Grid
      container
      spacing={isMobile ? 3 : 5}
      style={{ width: widerWidth, margin: wider }}
    >
      <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
              onChange={(event, value) => {
                if(value){
                  setBranchId(value?.id)
                  setBranch(value)
                } else {
                  // setSelectedCourse([]);
                  setSelectedSubject('');
                  setSelectedGrade([]);
                  setCourseList([]);
                  setSelectedCourse('');
                  setCollectData();
                  setCourseId('');
                }
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>
      {branchId ? (
      <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
        <Autocomplete
          size='small'
          id='grades'
          multiple
          className='dropdownIcon'
          options={gradeList || []}
          style={{ width: '100%' }}
          getOptionLabel={(option) => option?.grade__grade_name || ''}
          filterSelectedOptions
          value={selectedGrade || ''}
          onChange={handleGrade}
          // disabled={gradeKey && courseKey}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Grade'
              placeholder='Grade'
            />
          )}
        />
      </Grid>
      ): ''}
      {selectedGrade?.length  ? (
      <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
        <Autocomplete
          size='small'
          id='sbuject'
          className='dropdownIcon'
          options={subjectList || []}
          getOptionLabel={(option) => option?.subject__subject_name || ''}
          filterSelectedOptions
          style={{ width: '100%' }}
          value={selectedSubject || ''}
          onChange={(event, value)=> { 
            setSelectedSubject(value);
            // handleGrade();
            handleCourse();
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Subject'
              placeholder='Subject'
            />
          )}
        />
      </Grid>
      ) : ''}
      {/* {selectedSubject ? ( */}
      {/* <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
        <Autocomplete
          size='small'
          id='grades'
          multiple
          className='dropdownIcon'
          options={gradeList || []}
          style={{ width: '100%' }}
          getOptionLabel={(option) => option?.grade__grade_name || ''}
          filterSelectedOptions
          value={selectedGrade || ''}
          onChange={handleGrade}
          // disabled={gradeKey && courseKey}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Grade'
              placeholder='Grade'
            />
          )}
        />
      </Grid> */}
      {/* // ) : ''} */}
       {selectedSubject && selectedGrade?.length ? (
         <Grid item xs={12} sm={6} className={isMobile ? '' : 'filterPadding'}>
          <Autocomplete
            size='small'
            id='courseName'
            className='dropdownIcon'
            options={courseList || []}
            style={{ width: '100%' }}
            getOptionLabel={(option) => option ? `${option?.course_name} , ${option?.is_fixed ? 'Full Year Course - 40 weeks' : 'Fixed Course' + ' - ' + option?.no_of_periods + ' session'}` : ''}
            filterSelectedOptions
            value={selectedCourse || ''}
            onChange={handleCourse}
            renderInput={(params) => (
              <TextField
                {...params}
                size='small'
                variant='outlined'
                label='Course'
                placeholder='Course'
              />
            )}
          />
        </Grid>
       ) : ''} 
    </Grid>
    {loading && <Loading /> }
    </>
  );
};

export default CoursePriceFilters;
