import React, { useState, useContext } from 'react';
import './style.scss';
import { Autocomplete } from '@material-ui/lab';
import { TextField, Grid, Button, Card, IconButton, SvgIcon } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RupeeIcon from '../../../../../assets/images/rupee-indian.svg';
import DeleteIcon from '@material-ui/icons/Delete';
import ConfirmDialog from '../../../../../components/confirm-dialog';
import { AlertNotificationContext } from '../../../../../context-api/alert-context/alert-state';

const CoursePriceWeek = (props) => {
  const [indexNo, setIndexNo] = useState('');
  const [open, setOpen] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const { collectData, setCollectData, selectedLimit, courseDetails } = props;
  const [otherDaysList] = useState([
    { id: 1, day: 'Monday', send: 'Mon' },
    { id: 2, day: 'Tuesday', send: 'Tue' },
    { id: 3, day: 'Wednesday', send: 'Wed' },
    { id: 4, day: 'Thursday', send: 'Thu' },
    { id: 5, day: 'Friday', send: 'Fri' },
    { id: 6, day: 'Saturday', send: 'Sat' },
    { id: 7, day: 'Sunday', send: 'Sun' },
  ]);
  const daysList = [1, 2, 3, 4, 5, 6, 7];

  function handleAddPrice(key) {
    setCollectData((previousData) => {
      previousData[selectedLimit][key].push({
        week: collectData[selectedLimit].daysInWeek === 7 ? [...otherDaysList] : [],
        final_price: 0,
        actual_price: 0,
        no_of_session:
          collectData[selectedLimit].daysInWeek * collectData[selectedLimit].no_of_week,
      });
      return [...previousData];
    });
  }

  function handleOnChange(value, key, index, key2) {
    setCollectData((data) => {
      const newData = [...data];
      switch (key) {
        case 'daysInWeek':
          newData[selectedLimit]['daysInWeek'] = value;
          return newData;
        case 'no_of_week':
          newData[selectedLimit]['no_of_week'] = value;
          return newData;
        case 'dayPriceList':
          newData[selectedLimit]['dayPriceList'][index][key2] = value;
          return newData;
        default:
          return null;
      }
    });
  }

  function handleDelete(index) {
    setOpen(false);
    setIndexNo('');
    setCollectData((previousData) => {
      previousData[selectedLimit].dayPriceList.splice(index, 1);
      return [...previousData];
    });
  }

  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={12} xs={12} style={{ marginBottom: '15px' }}>
          <div className='daysTag'>Create Price</div>
        </Grid>
        <Grid item md={12} xs={12} className='priceLimitContainer'>
          <Grid container spacing={2}>
            <Grid item md={6} xs={12} style={{ marginTop: '8px' }}>
              <Autocomplete
                size='small'
                id='daysCombination'
                className='dropdownIcon'
                options={daysList}
                getOptionLabel={(option) => (option ? JSON.stringify(option) : '')}
                filterSelectedOptions
                value={collectData[selectedLimit].daysInWeek}
                onChange={(event, value) => {
                  handleOnChange(value, 'daysInWeek');
                  console.log(props);
                  // if(courseDetails.is_fixed){
                  //   handleOnChange(40, 'no_of_week');
                  // }
                  // else{
                  //   handleOnChange(Math.ceil(courseDetails?.no_of_periods / collectData[selectedLimit].daysInWeek) || 40, 'no_of_week');
                  // }
                  if (!courseDetails.is_fixed) {
                    handleOnChange(
                      Math.ceil(
                        courseDetails?.no_of_periods /
                          collectData[selectedLimit].daysInWeek
                      ) || 40,
                      'no_of_week'
                    );
                  }
                }}
                disabled={collectData[selectedLimit].dayPriceList?.length}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant='outlined'
                    label='No. of day in week'
                    placeholder='No. of day in week'
                  />
                )}
              />
            </Grid>
            {courseDetails && collectData[selectedLimit].daysInWeek ? (
              <Grid item md={3} xs={6}>
                <TextField
                  disabled={!courseDetails.is_fixed}
                  value={collectData[selectedLimit].no_of_week}
                  variant='outlined'
                  margin='dense'
                  label='No. of weeks'
                  onChange={(event) => {
                    handleOnChange(event.target.value, 'no_of_week');
                  }}
                />
              </Grid>
            ) : (
              ''
            )}
            {collectData[selectedLimit].daysInWeek &&
            collectData[selectedLimit].no_of_week ? (
              <Grid item md={3} xs={6} style={{ textAlign: 'right', marginTop: '8px' }}>
                <Button
                  onClick={() => handleAddPrice('dayPriceList')}
                  size='small'
                  variant='contained'
                  color='primary'
                >
                  <AddIcon />
                  &nbsp; Add Price List
                </Button>
              </Grid>
            ) : (
              ''
            )}
          </Grid>
          <Grid container spacing={2}>
            {collectData?.length !== 0
              ? collectData?.[selectedLimit]?.dayPriceList.map((item, index) => (
                  <Grid item md={12} xs={12} key={index}>
                    <Card
                      hovable
                      style={{
                        margin: '5px 0px',
                        padding: '10px',
                        border: '1px solid lightgray',
                        borderRadius: '5px',
                      }}
                    >
                      <Grid
                        container
                        spacing={2}
                        direction='row'
                        justify='center'
                        alignItems='center'
                      >
                        <Grid item md={5} xs={12} style={{ marginTop: '3px' }}>
                          <Autocomplete
                            size='small'
                            multiple
                            id='daysCombination'
                            className='dropdownIcon'
                            options={otherDaysList}
                            getOptionLabel={(option) => option?.day || ''}
                            value={item.week}
                            onChange={(event, value) => {
                              if (
                                value.length <= collectData[selectedLimit].daysInWeek ||
                                value.length === 0
                              ) {
                                handleOnChange(value, 'dayPriceList', index, 'week');
                              } else {
                                setAlert(
                                  'warning',
                                  `You can select only ${collectData[selectedLimit].daysInWeek} days in a week`
                                );
                              }
                            }}
                            disabled={collectData[selectedLimit].daysInWeek === 7}
                            filterSelectedOptions
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                variant='outlined'
                                label='Week Days'
                                placeholder='Week Days'
                              />
                            )}
                          />
                        </Grid>
                        {courseDetails?.is_fixed ? (
                          <Grid item md={2} xs={12}>
                            <TextField
                              disabled
                              className='inputFiled'
                              type='number'
                              variant='outlined'
                              label='No. of session'
                              placeholder='No. of session'
                              fullWidth
                              margin='dense'
                              value={item?.no_of_session || ''}
                              onChange={(e) =>
                                e.target.value > -1 &&
                                e.target.value?.length < 10 &&
                                handleOnChange(
                                  e.target.value.trimLeft(),
                                  'dayPriceList',
                                  index,
                                  'no_of_session'
                                )
                              }
                            />
                          </Grid>
                        ) : (
                          ''
                        )}
                        <Grid item md={courseDetails?.is_fixed ? 2 : 3} xs={12}>
                          <TextField
                            className='inputFiled'
                            type='number'
                            InputProps={{
                              inputProps: { autoComplete: 'off' },
                              startAdornment: (
                                <div>
                                  <SvgIcon
                                    component={() => (
                                      <img
                                        style={{
                                          height: '20px',
                                          width: '20px',
                                          marginTop: '5px',
                                          marginRight: '5px',
                                        }}
                                        src={RupeeIcon}
                                      />
                                    )}
                                  />
                                </div>
                              ),
                            }}
                            variant='outlined'
                            label='Actual Price'
                            placeholder='Actual Price'
                            fullWidth
                            margin='dense'
                            value={item?.actual_price || ''}
                            onChange={(e) =>
                              e.target.value > -1 &&
                              e.target.value?.length < 10 &&
                              handleOnChange(
                                e.target.value.trimLeft(),
                                'dayPriceList',
                                index,
                                'actual_price'
                              )
                            }
                          />
                        </Grid>
                        <Grid item md={courseDetails?.is_fixed ? 2 : 3} xs={12}>
                          <TextField
                            className='inputFiled'
                            type='number'
                            InputProps={{
                              inputProps: { autoComplete: 'off' },
                              startAdornment: (
                                <div>
                                  <SvgIcon
                                    component={() => (
                                      <img
                                        style={{
                                          height: '20px',
                                          width: '20px',
                                          marginTop: '5px',
                                          marginRight: '5px',
                                        }}
                                        src={RupeeIcon}
                                      />
                                    )}
                                  />
                                </div>
                              ),
                            }}
                            variant='outlined'
                            label='Final Price'
                            placeholder='Final Price'
                            fullWidth
                            margin='dense'
                            value={item?.final_price || ''}
                            onChange={(e) => {
                              if (parseInt(item?.actual_price, 10) >= e.target.value) {
                                e.target.value > -1 &&
                                  e.target.value?.length < 10 &&
                                  handleOnChange(
                                    e.target.value.trimLeft(),
                                    'dayPriceList',
                                    index,
                                    'final_price'
                                  );
                              } else {
                                setAlert(
                                  'warning',
                                  'Final Price Price should be less than Actual Price'
                                );
                              }
                            }}
                          />
                        </Grid>
                        <Grid item md={1} variant={12} style={{ textAlign: 'right' }}>
                          <IconButton
                            onClick={() => {
                              setOpen(true);
                              setIndexNo(index);
                            }}
                            size='small'
                            variant='contained'
                            color='primary'
                          >
                            <DeleteIcon />
                          </IconButton>
                        </Grid>
                      </Grid>
                    </Card>
                  </Grid>
                ))
              : ''}
          </Grid>
        </Grid>
      </Grid>
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => {
            setOpen(false);
            setIndexNo('');
          }}
          confirm={() => handleDelete(indexNo)}
          title='Are You Sure To Delete ?'
        />
      ) : (
        ''
      )}
    </>
  );
};

export default CoursePriceWeek;
