import React, { useState, useEffect, useContext, useRef } from 'react';
import './style.scss';
import {
  Grid,
  Switch,
  TextField,
  Table,
  TableBody,
  TableRow,
  TableCell,
  TableHead,
  SvgIcon,
} from '@material-ui/core';
import debounce from 'lodash.debounce';
import RupeeIcon from '../../../../../assets/images/rupee-indian.svg';
import { AlertNotificationContext } from '../../../../../context-api/alert-context/alert-state';
import endpoints from '../../../../../config/endpoints';
import axiosInstance from '../../../../../config/axios';
import Loader from '../../../../../components/loader/loader';
import ConfirmDialog from '../../../../../components/confirm-dialog';

const ViewEditCoursePrice = ({ allDataRecords, refresh, courseDetails }) => {
  const [dataRecords, setDataRecords] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [selectedData, setSelectedData] = useState('');

  useEffect(() => {
    if (allDataRecords?.length) {
      setDataRecords(JSON.parse(JSON.stringify(allDataRecords)));
    }
  }, [allDataRecords]);

  async function handleUpdate(payload) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.put(
        endpoints.coursePrice.updateCourseDetailsApi,
        {
          ...payload,
        }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setOpen(false);
        setSelectedData('');
        setAlert('success', data.message);
        refresh();
      } else {
        setLoading(false);
        setAlert('error', data.description);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.description);
    }
  }

  function handleOnChange(value, key, index) {
    setDataRecords((data) => {
      const newData = [...data];
      switch (key) {
        case key:
          newData[index][key] = value;
          return newData;
        default:
          return null;
      }
    });
  }

  const delayedQuery = useRef(debounce((q) => handleUpdate(q), 1000)).current;

  return (
    <Grid container spacing={2}>
      <Grid item md={12} xs={12} style={{ marginBottom: '15px' }}>
        <div className='daysTag'>View All Price List</div>
      </Grid>
      <Grid
        item
        md={12}
        xs={12}
        className='priceLimitContainer'
        style={{ marginBottom: '20px' }}
      >
        <Grid container spacing={2}>
          <Table style={{ padding: '0px', margin: '0px' }}>
            <TableHead style={{ padding: '0px', margin: '0px' }}>
              <TableRow style={{ padding: '0px', margin: '0px' }}>
                <TableCell style={{ padding: '0px', margin: '0px' }}>Days</TableCell>
                <TableCell style={{ padding: '0px', margin: '0px' }}>
                  No. of Weeks
                </TableCell>
                {courseDetails?.is_fixed ? (
                  <TableCell style={{ padding: '0px', margin: '0px' }}>
                    No. of Sessions
                  </TableCell>
                ) : (
                  ''
                )}
                <TableCell style={{ padding: '0px', margin: '0px' }}>
                  Actual Price
                </TableCell>
                <TableCell style={{ padding: '0px', margin: '0px' }}>
                  Final Price
                </TableCell>
                <TableCell style={{ padding: '0px', margin: '0px' }}>Status</TableCell>
              </TableRow>
            </TableHead>
            <TableBody style={{ padding: '0px', margin: '0px' }}>
              {dataRecords?.length &&
                dataRecords.map((item, index) => (
                  <TableRow key={item.id}>
                    <TableCell style={{ padding: '0px', margin: '0px' }}>
                      {item?.days?.[0]}
                    </TableCell>
                    <TableCell style={{ padding: '0px', margin: '0px' }}>
                      {`${item?.no_of_week} Weeks`}
                    </TableCell>
                    {courseDetails?.is_fixed ? (
                      <TableCell style={{ padding: '0px', margin: '0px' }}>
                        {item?.no_of_session || ''}
                      </TableCell>
                    ) : (
                      ''
                    )}
                    <TableCell style={{ padding: '0px', margin: '0px' }}>
                      <TextField
                        className='inputFiled'
                        type='number'
                        InputProps={{
                          inputProps: { autoComplete: 'off' },
                          startAdornment: (
                            <div>
                              <SvgIcon
                                component={() => (
                                  <img
                                    style={{
                                      height: '20px',
                                      width: '20px',
                                      marginTop: '5px',
                                      marginRight: '5px',
                                      imageRendering: 'pixelated',
                                    }}
                                    alt='crash'
                                    src={RupeeIcon}
                                  />
                                )}
                              />
                            </div>
                          ),
                        }}
                        variant='standard'
                        value={item?.price || ''}
                        margin='dense'
                        onChange={(e) => {
                          if (e.target.value > -1 && e.target.value?.length < 10) {
                            handleOnChange(
                              e.target.value.trimLeft(),
                              'price',
                              index,
                              item.id
                            );
                            delayedQuery({
                              actual_price: parseInt(e.target.value.trimLeft(), 10),
                              course_detail_id: item.id,
                            });
                          }
                        }}
                      />
                    </TableCell>
                    <TableCell style={{ padding: '0px', margin: '0px' }}>
                      <TextField
                        className='inputFiled'
                        type='number'
                        InputProps={{
                          inputProps: { autoComplete: 'off' },
                          startAdornment: (
                            <div>
                              <SvgIcon
                                component={() => (
                                  <img
                                    style={{
                                      height: '20px',
                                      width: '20px',
                                      marginTop: '5px',
                                      marginRight: '5px',
                                      imageRendering: 'pixelated',
                                    }}
                                    alt='crash'
                                    src={RupeeIcon}
                                  />
                                )}
                              />
                            </div>
                          ),
                        }}
                        variant='standard'
                        value={item?.final_price || ''}
                        margin='dense'
                        onChange={(e) => {
                          if (parseInt(item?.price, 10) >= e.target.value) {
                            if (e.target.value > -1 && e.target.value?.length < 10) {
                              handleOnChange(
                                e.target.value.trimLeft(),
                                'final_price',
                                index,
                                item.id
                              );
                              delayedQuery({
                                final_price: parseInt(e.target.value.trimLeft(), 10),
                                course_detail_id: item.id,
                              });
                            }
                          } else {
                            setAlert(
                              'warning',
                              'Final Price should be less than Actual Price'
                            );
                          }
                        }}
                      />
                    </TableCell>
                    <TableCell style={{ padding: '0px', margin: '0px' }}>
                      <Switch
                        color='primary'
                        checked={item?.is_delete}
                        onChange={(e) => {
                          setSelectedData({
                            is_delete: e.target.checked,
                            course_detail_id: item.id,
                          });
                          setOpen(true);
                        }}
                      />
                      &nbsp;
                      {!item?.is_delete ? (
                        <span
                          style={{ color: 'green', fontSize: '15px', fontWeight: 'bold' }}
                        >
                          Active
                        </span>
                      ) : (
                        <span
                          style={{ color: 'red', fontSize: '15px', fontWeight: 'bold' }}
                        >
                          In Active
                        </span>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
      {loading && <Loader />}
      {open ? (
        <ConfirmDialog
          open={open}
          cancel={() => {
            setOpen(false);
            setSelectedData('');
          }}
          confirm={() => handleUpdate(selectedData)}
          title='Are you sure to change the status'
        />
      ) : (
        ''
      )}
    </Grid>
  );
};

export default ViewEditCoursePrice;
