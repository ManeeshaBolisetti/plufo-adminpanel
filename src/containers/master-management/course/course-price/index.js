import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Grid, useTheme, Paper, Divider, Button } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles } from '@material-ui/core/styles';
import CoursePriceFilters from './course-price-filters';
import JoinLimitContainer from './join-limit-container';
import Layout from '../../../Layout';
import CommonBreadcrumbs from '../../../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../../../context-api/alert-context/alert-state';
import endpoints from '../../../../config/endpoints';
import axiosInstance from '../../../../config/axios';
import CoursePriceWeek from './course-price-week';
import ViewEditCoursePrice from './course-price-week/view-edit-course-price';
import Loader from '../../../../components/loader/loader';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    margin: '0 auto',
    boxShadow: 'none',
    marginTop: '1.5%',
  },
  container: {
    maxHeight: '70vh',
    width: '100%',
  },
  tableCell: {
    color: theme.palette.secondary.main,
  },
}));

const CoursePrice = (props) => {
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const { courseKey, gradeKey } = useParams();
  const classes = useStyles();
  const themeContext = useTheme();
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const wider = isMobile ? '-10px 0px' : '-10px 0px 20px 8px';
  const widerWidth = isMobile ? '98%' : '95%';
  const [courseId, setCourseId] = useState('' || courseKey);
  const [selectedCourse, setSelectedCourse] = useState('');
  const [selectedLimit, setSelectedLimit] = useState(0);
  const [isEdit, setIsEdit] = useState(false);
  const [editCourseData, setEditCourseData] = useState([]);

  const [collectData, setCollectData] = useState([
    { limit: '1:1', batch_size: '1', daysInWeek: '', dayPriceList: [], no_of_week: '' },
    { limit: '1:5', batch_size: '5', daysInWeek: '', dayPriceList: [], no_of_week: '' },
    { limit: '1:10', batch_size: '10', daysInWeek: '', dayPriceList: [], no_of_week: '' },
    { limit: '1:20', batch_size: '20', daysInWeek: '', dayPriceList: [], no_of_week: '' },
    { limit: '1:30', batch_size: '30', daysInWeek: '', dayPriceList: [], no_of_week: '' },
  ]);

  async function getDetailsApi(id) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.coursePrice.getCoursePriceApi}?course=${id}`
      );
      if (data?.status_code === 200) {
        setLoading(false);
        const Batch1 = [];
        const Batch5 = [];
        const Batch10 = [];
        const Batch20 = [];
        const Batch30 = [];
        if (data?.result?.length !== 0) {
          for (let i = 0; i < data?.result.length; i++) {
            const item = data?.result[i];
            if (item.batch_size === 1) {
              Batch1.push(item);
            } else if (item.batch_size === 5) {
              Batch5.push(item);
            } else if (item.batch_size === 10) {
              Batch10.push(item);
            } else if (item.batch_size === 20) {
              Batch20.push(item);
            } else if (item.batch_size === 30) {
              Batch30.push(item);
            }
          }
        }
        setEditCourseData([
          [...Batch1],
          [...Batch5],
          [...Batch10],
          [...Batch20],
          [...Batch30],
        ]);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  useEffect(() => {
    if (courseId) {
      getDetailsApi(courseId);
    }
  }, [courseId]);

  function handleFilter(data) {
    return data
      ?.sort((a, b) => a.id - b.id)
      ?.map((item) => item.send)
      ?.join('/');
  }

  async function handleSubmit() {
    const payloadData = JSON.parse(JSON.stringify([...collectData]));
    if (payloadData?.filter((item) => item.daysInWeek === '')?.length === 5) {
      setAlert('warning', 'price Lists are not created');
      return;
    }
    for (let i = 0; i < payloadData?.length; i += 1) {
      if (payloadData[i]?.daysInWeek && payloadData[i].dayPriceList?.length === 0) {
        setAlert('warning', `Price List is empty in this ${payloadData[i]?.limit} batch`);
        return;
      }
      for (let j = 0; j < payloadData[i]?.dayPriceList?.length; j += 1) {
        if (
          typeof payloadData[i]?.dayPriceList[j]?.week === 'object' &&
          payloadData[i]?.dayPriceList[j]?.week?.length === 0
        ) {
          setAlert(
            'warning',
            `No of Days in Week is not selected in ${payloadData[i]?.limit} batch -> ${
              j + 1
            } record`
          );
          return;
        }
        if (
          typeof payloadData[i]?.dayPriceList[j]?.week === 'object' &&
          payloadData[i]?.dayPriceList[j]?.week?.length !== payloadData[i]?.daysInWeek
        ) {
          setAlert(
            'warning',
            `No of Days in Week & Week in days are not matching in ${
              payloadData[i]?.limit
            } batch-> ${j + 1} record`
          );
          return;
        }
        if (selectedCourse?.is_fixed && !payloadData[i]?.dayPriceList[j]?.no_of_session) {
          setAlert(
            'warning',
            `You missing to Enter no of session in Batch ${payloadData[i]?.limit} -> ${
              j + 1
            } record`
          );
          return;
        }
        if (!payloadData[i]?.dayPriceList[j]?.actual_price) {
          setAlert(
            'warning',
            `You missing to Enter price in Batch ${payloadData[i]?.limit} -> ${
              j + 1
            } record`
          );
          return;
        }
        payloadData[i].dayPriceList[j].week =
          typeof payloadData[i].dayPriceList[j].week === 'object'
            ? handleFilter(payloadData[i].dayPriceList[j].week)
            : payloadData[i].dayPriceList[j].week;
      }
    }
    setLoading(true);
    try {
      const { data } = await axiosInstance.post(
        `${endpoints.coursePrice.getCoursePriceApi}`,
        {
          batch: payloadData,
          course: courseId,
        }
      );
      if (data?.status_code === 200) {
        setLoading(false);
        setAlert('success', data?.message);
        getDetailsApi(courseId);
        setCollectData([
          {
            limit: '1:1',
            batch_size: '1',
            daysInWeek: '',
            dayPriceList: [],
            no_of_week: '',
          },
          {
            limit: '1:5',
            batch_size: '5',
            daysInWeek: '',
            dayPriceList: [],
            no_of_week: '',
          },
          {
            limit: '1:10',
            batch_size: '10',
            daysInWeek: '',
            dayPriceList: [],
            no_of_week: '',
          },
          {
            limit: '1:20',
            batch_size: '20',
            daysInWeek: '',
            dayPriceList: [],
            no_of_week: '',
          },
          {
            limit: '1:30',
            batch_size: '30',
            daysInWeek: '',
            dayPriceList: [],
            no_of_week: '',
          },
        ]);
      } else {
        setLoading(false);
        setAlert('error', data?.description);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Course Price'
        />
      </div>
      <Grid container spacing={2} style={{ width: '100%', overflow: 'hidden' }}>
        <Grid item md={12} xs={12}>
          <CoursePriceFilters
            setCourseId={setCourseId}
            setCollectData={() =>
              setCollectData([
                {
                  limit: '1:1',
                  batch_size: '1',
                  daysInWeek: '',
                  dayPriceList: [],
                  no_of_week: '',
                },
                {
                  limit: '1:5',
                  batch_size: '5',
                  daysInWeek: '',
                  dayPriceList: [],
                  no_of_week: '',
                },
                {
                  limit: '1:10',
                  batch_size: '10',
                  daysInWeek: '',
                  dayPriceList: [],
                  no_of_week: '',
                },
                {
                  limit: '1:20',
                  batch_size: '20',
                  daysInWeek: '',
                  dayPriceList: [],
                  no_of_week: '',
                },
                {
                  limit: '1:30',
                  batch_size: '30',
                  daysInWeek: '',
                  dayPriceList: [],
                  no_of_week: '',
                },
              ])}
            selectedSubjectId = {props?.location?.params?.selectedSubjectId}
            selectedCourse={selectedCourse}
            setSelectedCourse={setSelectedCourse}
            courseKey={courseKey}
            gradeKey={gradeKey}
            isEdit={isEdit}
          />
        </Grid>
      </Grid>
      <div>
        {' '}
        <Divider />{' '}
      </div>
      <Paper className={classes.root} style={{ display: selectedCourse ? '' : 'none' }}>
        <Grid
          container
          spacing={isMobile ? 3 : 5}
          style={{ width: widerWidth, margin: wider }}
        >
          <Grid item xs={12} sm={2}>
            <JoinLimitContainer setSelectedLimit={setSelectedLimit} />
          </Grid>
          <Grid item xs={12} sm={10}>
            {editCourseData[selectedLimit]?.length ? (
              <ViewEditCoursePrice
                allDataRecords={editCourseData[selectedLimit]}
                refresh={() => getDetailsApi(courseId)}
                courseDetails={selectedCourse}
              />
            ) : (
              ''
            )}
            <CoursePriceWeek
              courseDetails={selectedCourse}
              collectData={collectData}
              setCollectData={setCollectData}
              selectedLimit={selectedLimit}
            />
          </Grid>
          <Grid item xs={12} sm={12} style={{ textAlign: 'right' }}>
            <Button onClick={() => handleSubmit()} variant='contained' color='primary'>
              Submit
            </Button>
          </Grid>
        </Grid>
      </Paper>
      {loading && <Loader />}
    </Layout>
  );
};

export default CoursePrice;
