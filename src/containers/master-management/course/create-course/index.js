import React, { useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import {
  Grid,
  TextField,
  Button,
  useTheme,
  SvgIcon,
  Switch,
  Typography,
} from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Autocomplete from '@material-ui/lab/Autocomplete';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import MyTinyMcEditor from '../../../../components/tinyMc-editor';
// import { filter } from 'lodash';
import Loading from '../../../../components/loader/loader';
import CommonBreadcrumbs from '../../../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../../../context-api/alert-context/alert-state';
import Layout from '../../../Layout';
import endpoints from '../../../../config/endpoints';
import axiosInstance from '../../../../config/axios';
import CourseCard from '../course-card';
import './style.css';
import deleteIcon from '../../../../assets/images/delete.svg';
import attachmenticon from '../../../../assets/images/attachmenticon.svg';
import { Context } from '../view-course/context/ViewStore';
import LinearProgressBar from '../../../../components/progress-bar';
import displayName from '../../../../config/displayName';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    margin: '0 auto',
    boxShadow: 'none',
  },
  container: {
    maxHeight: '70vh',
    width: '100%',
  },
  columnHeader: {
    color: `${theme.palette.secondary.main} !important`,
    fontWeight: 600,
    fontSize: '1rem',
    backgroundColor: `#ffffff !important`,
  },
  tableCell: {
    color: theme.palette.secondary.main,
  },
  buttonContainer: {
    width: '95%',
    margin: '0 auto',
    background: theme.palette.background.secondary,
    paddingBottom: theme.spacing(2),
  },
}));

const CreateCourse = () => {
  const classes = useStyles();
  const history = useHistory();
  const aolHostURL = window.location.host;
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const themeContext = useTheme();
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const wider = isMobile ? '-10px 0px' : '-10px 0px 20px 8px';
  const widerWidth = isMobile ? '98%' : '95%';
  const { courseKey, gradeKey } = useParams();
  // context
  const [branchDropdown, setBranchDropdown] = useState([]);
  const [gradeDropdown, setGradeDropdown] = useState([]);
  const [categoryDropdown, setCategoryDropdown] = useState([]);
  const [subjectDropdown, setSubjectDropdown] = useState([]);
  const [noOfPeriods, setNoPeriods] = useState('');
  const [title, setTitle] = useState('');
  const [metaTitle, setMetaTitle] = useState('');
  const [courseMetaTitle, setCourseMetaTitle] = useState('');
  const [courseDescription, setCourseDescription] = useState('');
  const [courseFooterContent, setCourseFooterContent] = useState('');
  const [isShow, setIsShow] = useState(false);
  const [editData, setEditData] = useState({});
  const [editFlag, setEditFlag] = useState(false);
  const [coursePre, setCoursePre] = useState('');
  const [learn, setLearn] = useState('');
  const [overview, setOverview] = useState('');
  const [filePath, setFilePath] = useState([]);
  const [selectedFileName, setSelectedFileName] = useState('');
  const [selectedThumbnail, setSelectedThumbnail] = useState('');
  const [nextToggle, setNextToggle] = useState(false);
  const [thumbnailImage, setThumbnailImage] = useState('');
  const [data, setData] = useState([]);
  const [selectDropDown,setSelectDropDown] =useState([])
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)
  const [saveBranch,setSaveBranch] = useState('')
  // const branchDrop = [{ branch_name: 'AOL' }];
  const [filterData, setFilterData] = useState({
    branch:'',
    grade: [],
    courseLevel: '',
    category: [],
    subject: '',
    course_type: '',
  });

  const [courseLevelDrop, setCourseLevelDrop] = useState([
    { value: 'Beginner', level: 'Low' },
    { value: 'Intermediate', level: 'Mid' },
    { value: 'Advance', level: 'High' },
  ]);
  const courseTypeList = [
    { id: 1, name: 'Full Year Course' },
    { id: 2, name: 'Fixed Course' },
  ];
  const [progress, setProgress] = React.useState(10);
  const [isLodding, setIsLodding] = React.useState(0);

  const handleCourseLevel = (event, value) => {
    setFilterData({ ...filterData, courseLevel: '' });
    if (value) {
      setFilterData({ ...filterData, courseLevel: value });
    }
  };

  const handleAddPeriod = () => {
    const list = [...data];
    setNoPeriods((prev) => Number(prev) + 1);
    list.push({ title: '', description: '',session_date:'', files: [] });
    setData(list);
  };

  const goBackHandler = () => {
    const isCreate = Number(sessionStorage.getItem('createCourse')) || '';
    const isPeriod = Number(sessionStorage.getItem('periodDetails')) || '';
    if (window.location.host === endpoints.aolConfirmURL) {
      const isAolValue = Number(sessionStorage.getItem('isAol')) || '';
      if (isAolValue === 1) {
        history.push(`/online-class/view-class`);
      } else if (isAolValue === 2) {
        history.push('/online-class/attend-class');
      } else if (isAolValue === 3) {
        history.push('/online-class/teacher-view-class');
      } else {
        const gKey = Number(sessionStorage.getItem('gradeKey')) || '';
        if (isCreate !== 1 || isPeriod === 1) {
          history.push(`/course-list/${gKey}`);
        }
        sessionStorage.removeItem('gradeKey');
      }
    } else {
      const isErpValue = Number(sessionStorage.getItem('isErpClass')) || '';
      if (isErpValue === 1) {
        history.push(`/online-class/view-class`);
      } else if (isErpValue === 2) {
        history.push('/erp-online-class-student-view');
      } else if (isErpValue === 3) {
        history.push('/erp-online-class-teacher-view');
      } else {
        const gKey = Number(sessionStorage.getItem('gradeKey')) || '';
        if (isCreate !== 1 || isPeriod === 1) {
          history.push(`/course-list/${gKey}`);
        }
        sessionStorage.removeItem('gradeKey');
      }
    }
  };

  const handleBack = () => {
    const isNext = Number(sessionStorage.getItem('nextFlag')) || '';
    if (isNext !== 1) {
      if (Number(gradeKey)) {
        goBackHandler();
      } else {
        const isCreate = Number(sessionStorage.getItem('createCourse')) || '';
        const periodView = Number(sessionStorage.getItem('periodDetails')) || '';
        const isGrade = Number(sessionStorage.getItem('gradeKey')) || '';
        if (isCreate === 1 || periodView === 1 || Number(isGrade) > 0)
          setNextToggle((prev) => !prev);
      }
    } else {
      setNextToggle((prev) => !prev);
      sessionStorage.removeItem('nextFlag');
    }
  };

  const handleBackToCourseList = () => {
    history.push(`/course-list/`);
  };

  useEffect(() => {
    if (Number(courseKey)) {
      axiosInstance
        .get(`${endpoints.onlineCourses.fetchCourseDetails}?course_id=${courseKey}`)
        .then((result) => {
          // debugger
          if (result.data?.result?.length > 0) {
            if (result.data?.status_code === 200) {
              handleCategory();
              handleGrade();
              handleBranch();
              const {
                course_period,
                no_of_periods,
                learn: learn_text,
                pre_requirement,
                overview: overview_text,
                course_name,
                meta_title,
                course_meta_title,
                meta_description,
                is_show,
                course_footer_content,
                files: doc_file,
                thumbnail: thumbnail_file,
                level: level_name,
                is_fixed,
                tags: {
                  category: category_data,
                  branch: branch_data,
                  grade: grade_data,
                  subjects: subject_data,
                },
              } = result.data?.result[0]?.course_id;
              console.log(branch_data.map((item) => item.branch_name).toString(),'avshek999')
              setSaveBranch(branch_data.map((item) => item.branch_name))
              setData(course_period.reverse());
              setNoPeriods(is_fixed ? 40 : no_of_periods);
              setLearn(learn_text);
              setCoursePre(pre_requirement);
              setOverview(overview_text);
              setTitle(course_name);
              setMetaTitle(meta_title);
              setCourseMetaTitle(course_meta_title);
              setCourseDescription(meta_description);
              setCourseFooterContent(course_footer_content);
              setIsShow(is_show);
              setFilePath(doc_file);
              setThumbnailImage(thumbnail_file.includes('') ? '' : thumbnail_file[0]);
              setSelectedFileName(doc_file[0]?.split('_')[1]);
              setSelectedThumbnail(thumbnail_file[0]?.split('_')[1]);
              setEditFlag(true);
              if (Number(gradeKey)) setNextToggle((prev) => !prev);
              else if (window.location.host === endpoints.aolConfirmURL) {
                const isAolValue = Number(sessionStorage.getItem('isAol')) || '';
                if (isAolValue === 1) {
                  history.push(`/online-class/view-class`);
                } else if (isAolValue === 2) {
                  history.push('/online-class/attend-class');
                } else if (isAolValue === 3) {
                  history.push('/online-class/teacher-view-class');
                }
              } else {
                const isErpValue = Number(sessionStorage.getItem('isErpClass')) || '';
                if (isErpValue === 1) {
                  history.push(`/online-class/view-class`);
                } else if (isErpValue === 2) {
                  history.push('/erp-online-class-student-view');
                } else if (isErpValue === 3) {
                  history.push('/erp-online-class-teacher-view');
                }
              }
              setFilterData({
                branch: {
                  branch_name : (branch_data.map((item) => item?.branch_name)).toString(),
                },
                courseLevel: courseLevelDrop?.find((obj) => obj?.level === level_name),
                category: category_data,
                subject: {
                  id: subject_data?.id,
                  subjectName: subject_data?.subject_name,
                },
                grade: grade_data,
                course_type: is_fixed ? courseTypeList[0] : courseTypeList[1],
              });
            } else {
              setEditFlag(false);
            }
          } else {
            setEditFlag(false);
            goBackHandler();
          }
        })
        .catch((error) => {
          console.error(error);
          setEditFlag(false);
        });
    } else {
      goBackHandler();
    }
  }, []);

  const handleNext = () => {
    if (!thumbnailImage) {
      setAlert('warning', 'Thumbnail Image is compulsory!');
      return;
    }
    if (filePath?.length !== 1) {
      setAlert('warning', 'Document is compulsory!');
      return;
    }
    if (!title) {
      setAlert('warning', 'Title is compulsory!');
      return;
    }
    if (noOfPeriods <= 0) {
      setAlert('warning', 'No. of periods should be more than 0!');
      return;
    }
    if (!filterData.subject.id) {
      setAlert('warning', 'Subject is compulsory!');
      return;
    }
    if(filterData.branch === ''){
      setAlert('warning', 'Branch is compulsory!');
      return;
    }
    if (coursePre === '') {
      setAlert('warning', 'Course prerequisiteites is compulsory!');
      return;
    }
    if (learn === '') {
      setAlert('warning', 'What will you learn from this course is compulsory!');
      return;
    }
    if (overview === '') {
      setAlert('warning', 'Course Overview is compulsory!');
      return;
    }
    if (filterData.category === []) {
      setAlert('warning', 'Category is compulsory!');
      return;
    }
    if (!filterData.courseLevel.level) {
      setAlert('warning', 'Level is compulsory!');
      return;
    }
    if (!metaTitle) {
      setAlert('warning', 'Course URL Title is compulsory!');
      return;
    }
    if (!courseMetaTitle) {
      setAlert('warning', 'Course Meta Title is compulsory!');
      return;
    }
    if (filterData.grade === []) {
      setAlert('warning', 'Grade is compulsory!');
      return;
    }
    if (!filterData.course_type) {
      setAlert('warning', 'Course Type is compulsory!');
      return;
    }
    // if (filterData.course_type.id === 1) {
    //   if (editFlag) {
    //     handleEdit();
    //   } else {
    //     handleSubmit();
    //   }
    // } else 
    if (noOfPeriods > 0) {
      if (data.length === 0) {
        const list = [...data];
        for (let i = 0; i < noOfPeriods; i += 1) {
          list.push({ title: '', description: '',session_date:'', files: [] });
        }
        setData(list);
      }
      sessionStorage.setItem('nextFlag', 1);
      setNextToggle((prev) => !prev);
    } else {
      setAlert('warning', 'Periods should be more than or equal to 1');
    }
  };

  const handleNoOfPeriods = (event) => {
    const val = event.target.value;
    if (val <= 1000) setNoPeriods(val);
    else setAlert('warning', "No. of periods can't be more than 1000");
  };

  const handleCategory = (event, value) => {
    setFilterData({ ...filterData, category: [], branch: '', grade: [], subject: '' });
    // setSelectDropDown([]);
    setGradeDropdown([]);
    setSubjectDropdown([]);
    if (value) {
      setFilterData({ ...filterData, category: value, branch:'', grade: [], subject: '' });
      axiosInstance
        .get(
          `${endpoints.onlineCourses.categoryList}?tag_type=2&parent_id=${
            value?.length !== 0 ? value.map((item) => item.id) : ''
          }`
        )
        .then((result) => {
          if (result.data?.status_code === 201) {
            const list1 = [];
            const list2 = [];
            const resp = result.data?.result;
            resp.forEach((obj) => {
              if (obj?.tag_type === '1') {
                list1.push({
                  id: obj?.id,
                  subjectName: obj?.subject__subject_name,
                  subject_id: obj?.subject_id,
                });
              }
          else {
                list2.push({
                  id: obj.id,
                  grade_name: obj?.grade__grade_name,
                  grade_id: obj?.grade_id,
                });
              }
            });
            setSubjectDropdown(list1);
            setGradeDropdown(list2);
          }
        });
    }
  };

  const handleSubject = (event, value) => {
    setFilterData({ ...filterData, subject: value });
    if (value) {
      setFilterData({ ...filterData, subject: value });
    }
  };

  useEffect(() => {
    if(branchId){
      setGradeDropdown([]);
      let url = `${endpoints.communication.grades}`;
      if (aolHostURL !== endpoints.aolConfirmURL) url += `?branch_id=${branchId}`;
      else url += `?branch_id=${branchId}`;
      axiosInstance
        .get(url)
        .then((result) => {
          if (result.data.status_code === 200) {
            const list = [];
            result.data.data.forEach((obj) => {
              list.push({
                id: obj.id,
                grade_name: obj?.grade__grade_name,
                gradeId: obj?.grade_id,
              });
            });
            setGradeDropdown(list);
          }
        })
        .catch((error) => {
          setGradeDropdown([]);
          setAlert('error', error.message);
        });
    }
    // if (aolHostURL !== endpoints.aolConfirmURL) {
    // }
  }, [branchId]);

  const handleGrade = (event, value) => {
    setFilterData({ ...filterData, grade: [] });
    if (value) {
      setFilterData({
        ...filterData,
        grade: value,
      });
    }
  };

  const handleBranch = (event, value) =>{
    setBranchId(value?.id)
    setBranch(value)
    setFilterData({...filterData, branch:''});
    if(value) {
      setFilterData({
        ...filterData,
        branch: value
      })
    }
  }

  const removeFileHandler = (i, fileType) => {
    if (fileType === 'thumbnail') {
      setThumbnailImage('');
      setSelectedThumbnail('');
      setIsLodding(0);
    } else if (fileType === 'doc') {
      setIsLodding(0);
      filePath.splice(i, 1);
      setSelectedFileName('');
    }
    setAlert('success', 'File deleted successfully');
  };

  const handleImageChange = (event) => {
    setIsLodding(1);
    if (filePath.length < 10) {
      // const data = event.target.files[0];
      const fd = new FormData();
      fd.append('file', event.target.files[0]);
      axiosInstance.post(`${endpoints.onlineCourses.fileUpload}`, fd).then((result) => {
        if (result.data.status_code === 200) {
          console.log(
            result.data,
            ' === ',
            result.data?.result?.get_file_path?.split('_')[1]
          );
          const fileList = [...filePath];
          fileList.push(result.data?.result?.get_file_path);
          setFilePath(fileList);
          setSelectedFileName(result.data?.result?.get_file_path?.split('_')[1]);

          const timer = setInterval(() => {
            setProgress((prevProgress) =>
              prevProgress >= 100 ? 100 : prevProgress + 10
            );
          }, 700);
          setAlert('success', result.data.message);
          return () => {
            setIsLodding(0);
            clearInterval(timer);
          };
        }
        setAlert('error', result.data?.message);
      });
    } else {
      setAlert('warning', 'Limit Exceeded for file upload!');
    }
  };

  const handleThumbnail = (event) => {
    setIsLodding(2);
    const fd = new FormData();
    fd.append('file', event.target.files[0]);
    const fileName = event.target.files[0]?.name;
    if (
      fileName.indexOf('.jpg') > 0 ||
      fileName.indexOf('.jpeg') > 0 ||
      fileName.indexOf('.png') > 0
    ) {
      axiosInstance.post(`${endpoints.onlineCourses.fileUpload}`, fd).then((result) => {
        if (result.data.status_code === 200) {
          setThumbnailImage(result.data?.result?.get_file_path);
          setSelectedThumbnail(result.data?.result?.get_file_path?.split('_')[1]);
          // setAlert('success', result.data.message);
          // setProgress(100);
          const timer = setInterval(() => {
            setProgress((prevProgress) =>
              prevProgress >= 100 ? 100 : prevProgress + 10
            );
          }, 700);
          setAlert('success', result.data.message);
          return () => {
            setIsLodding(0);
            // setAlert('success', result.data.message);
            clearInterval(timer);
          };
        }
        setAlert('error', result.data.message);
        setIsLodding(0);
      });
    } else {
      setAlert('error', 'Only .jpg, .jpeg & .png files are acceptable!');
    }
  };

  const handleSubmit = () => {
    // debugger
    const list = [...data];
    const result = list.filter(item => item.title === '' && ((item.description !== '' && item.description !== null) || item.files.length > 0))
    if(result.length === 0) {
    setLoading(true);
    const isAol = aolHostURL !== endpoints.aolConfirmURL;
    axiosInstance
      .post(`${endpoints.onlineCourses.createCourse}`, {
        course_name: title,
        meta_title: metaTitle,
        course_meta_title: courseMetaTitle,
        meta_description: courseDescription,
        course_footer_content: courseFooterContent,
        is_show: isShow,
        pre_requirement: coursePre,
        overview,
        learn,
        course_category:
          filterData?.category?.map((item) => item.course_category_id) || [],
        branch:filterData.branch?.id,
        grade: filterData?.grade?.map((item) => item.gradeId) || [],
        // branch:filterData?.branch?.map((item) => item.id) || [],
        branch: filterData?.branch?.id,
        is_fixed: filterData.course_type.id === 1,
        no_of_week: filterData.course_type.id === 1 ? 40 : 0,
        level: filterData.courseLevel.level,
        no_of_periods: parseInt(data?.length, 10),
        files: filePath,
        thumbnail: [thumbnailImage],
        period_data: data,
        subject: filterData?.subject?.subject_id,
        tag_id: [
          // ...filterData?.branch?.map((item)=> item.id),
          // ...filterData?.branch?.id,
          ...filterData?.category?.map((item) => item.id),
          ...filterData?.grade?.map((item) => item.id),
          filterData?.subject?.id,
        ],
      })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setFilePath([]);
          setThumbnailImage('');
          setData([]);
          setNoPeriods('');
          setTitle('');
          setMetaTitle('');
          setCoursePre('');
          setOverview('');
          setLearn('');
          setEditData();
          setEditFlag(false);
          setFilterData({
            branch:'',
            grade: [],
            courseLevel: '',
            category: [],
            subject: '',
            course_type: '',
          });
          setAlert('success', result?.data?.message);
          setNextToggle(false);
          history.push(`/course-list`);
        } else {
          setAlert('error', result?.data?.description);
          setGradeDropdown([]);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert(
          'error',
          error.response?.data?.message ||
            error.response?.data?.msg ||
            error.response?.data?.description
        );
        setGradeDropdown([]);
      });
    }
    else {
      const index = list.indexOf(result[0])
      setAlert('error', `Please Enter Title in Period ${index + 1}`)
    }
  };

  const handleEdit = () => {
    const list = [...data];
    const result = list.filter(item => item.title === '' && ((item.description !== '' && item.description !== null) || item.files.length > 0))
    if(result.length === 0) {
    setLoading(true);
    const isAol = aolHostURL !== endpoints.aolConfirmURL;
    axiosInstance
      .put(`${endpoints.onlineCourses.updateCourse}${courseKey}/update-course/`, {
        course_name: title,
        meta_title: metaTitle,
        course_meta_title: courseMetaTitle,
        meta_description: courseDescription,
        course_footer_content: courseFooterContent,
        is_show: isShow,
        pre_requirement: coursePre,
        overview,
        learn,
        course_category:
          filterData?.category?.map((item) => item.course_category_id) || [],
        branch: filterData?.branch?.id,
        grade: filterData?.grade?.map((item) => item.grade_id) || [],
        is_fixed: filterData.course_type.id === 1,
        no_of_week: filterData.course_type.id === 1 ? 40 : 0,
        level: filterData.courseLevel.level,
        no_of_periods: parseInt(data?.length, 10),
        files: filePath,
        thumbnail: [thumbnailImage],
        period_data: data,
        subject: filterData?.subject?.subject_id,
        tag_id: [
          ...filterData?.category?.map((item) => item.id),
          ...filterData?.grade?.map((item) => item.id),
          filterData?.subject?.id,
        ],
      })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setFilePath([]);
          setThumbnailImage('');
          setData([]);
          setNoPeriods('');
          setTitle('');
          setMetaTitle('');
          setCoursePre('');
          setOverview('');
          setLearn('');
          setEditData();
          setEditFlag(false);
          setFilterData({
            branch: '',
            grade: [],
            courseLevel: '',
            category: [],
            subject: '',
            course_type: '',
          });
          setAlert('success', result.data.message);
          setNextToggle((prev) => !prev);
          history.push(`/course-list/${sessionStorage.getItem('gradeKey')}`);
          sessionStorage.removeItem('gradeKey');
        } else {
          setAlert('success', result.data.description);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert(
          'error',
          error.response?.data?.message ||
            error.response?.data?.msg ||
            error.response?.data?.description
        );
      });
    }
    else {
      const index = list.indexOf(result[0])
      setAlert('error', `Please Enter Title in Period ${index + 1}`)
    }
  };

  const FileRow = (props) => {
    const { name, file, onClose, index } = props;
    return (
      <div className='file_row_image_course'>
        <div className='file_name_container_course'>{name}</div>
        <Divider orientation='vertical' className='divider_color' flexItem />
        <div className='file_close_course'>
          <span onClick={onClose}>
            <SvgIcon
              component={() => (
                <img
                  style={{
                    width: '15px',
                    height: '15px',
                    cursor: 'pointer',
                  }}
                  src={deleteIcon}
                  alt='given'
                />
              )}
            />
          </span>
        </div>
      </div>
    );
  };

  useEffect(() => {
    // if (aolHostURL !== endpoints.aolConfirmURL) {
    axiosInstance
      .get(`${endpoints.communication.branches}`)
      .then((result) => {
        if (result.data.status_code === 200) {
          setBranchDropdown(result.data.data);
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setBranchDropdown([]);
        setAlert('error', error.message);
      });

    axiosInstance
      .get(`${endpoints.onlineCourses.categoryList}?tag_type=1`)
      .then((result) => {
        if (result.data.status_code === 201) {
          setCategoryDropdown(result.data.result);
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setCategoryDropdown([]);
        setAlert('error', error.message);
      });
    // }
  }, []);

  useEffect(() => {
    if (data?.length < 1) setNextToggle(false);
  }, [data?.length]);

  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      const list = [];
            res.data.data.forEach((obj) => {
              list.push({
                id: obj.id,
                branch_name: obj?.branch_name,
              });
            })
      setSelectDropDown(list)

    })
  }, [])

  function handleTinyMce() {
    return (
      <MyTinyMcEditor
        id='Course-Footer-Content'
        initialValue={courseFooterContent || ''}
        value={courseFooterContent || ''}
        heightInchs='250'
        onChange={(content) => setCourseFooterContent(content)}
      />
    );
  }

  return (
    <>
      {loading ? <Loading message='Loading...' /> : null}
      <Layout>
        <div>
          <div style={{ width: '95%', margin: '20px auto' }}>
            <CommonBreadcrumbs
              componentName='Master Management'
              childComponentName={
                gradeKey ? 'Period Details' : courseKey ? 'Edit Course' : 'Create Course'
              }
              childComponentNameNext={!gradeKey && nextToggle && 'Periods'}
            />
          </div>
        </div>
        {!nextToggle ? (
          !gradeKey && (
            <Grid
              container
              spacing={isMobile ? 3 : 5}
              style={{ width: widerWidth, margin: wider }}
            >
              <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={handleCourseLevel}
                  id='academic-year'
                  className='dropdownIcon'
                  disabled ={editFlag}
                  value={filterData?.courseLevel || ''}
                  options={courseLevelDrop || []}
                  getOptionLabel={(option) => option?.value || ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Course Level'
                      placeholder='Course Level'
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={handleCategory}
                  multiple
                  id='volume'
                  className='dropdownIcon'
                  disabled={editFlag}
                  value={filterData?.category || []}
                  options={categoryDropdown || []}
                  getOptionLabel={(option) => option?.tag_name || ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Category'
                      placeholder='Category'
                    />
                  )}
                />
              </Grid>
              <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              // value={branch || ''}
              disabled={editFlag}
              value={filterData?.branch || []}
              onChange={handleBranch}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>
              
              <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  multiple
                  onChange={handleGrade}
                  id='volume'
                  disabled={editFlag}
                  className='dropdownIcon'
                  value={filterData?.grade || []}
                  options={gradeDropdown || []}
                  getOptionLabel={(option) => option?.grade_name || ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Grade'
                      placeholder='Grade'
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={handleSubject}
                  id='volume'
                  disabled={editFlag}
                  className='dropdownIcon'
                  value={filterData?.subject || ''}
                  options={subjectDropdown || []}
                  getOptionLabel={(option) => option?.subjectName || ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Subject'
                      placeholder='Subject'
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={(event, value) => {
                    setFilterData({ ...filterData, course_type: value });
                    if (value?.id === 1) {
                      setNoPeriods(40);
                    } else {
                      setNoPeriods('');
                    }
                  }}
                  disabled={editFlag}
                  id='course_type'
                  className='dropdownIcon'
                  value={filterData?.course_type || ''}
                  options={courseTypeList || []}
                  getOptionLabel={(option) => option?.name || ''}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Course Type'
                      placeholder='Course Type'
                    />
                  )}
                />
              </Grid>
              {filterData?.course_type ? (
                <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
                  <TextField
                    id='noofperiods'
                    type='number'
                    className='dropdownIcon'
                    style={{ width: '100%' }}
                    label={
                      filterData?.course_type?.id === 1 ? 'No of weeks' : 'No of sessions'
                    }
                    placeholder={
                      filterData?.course_type?.id === 1 ? 'No of weeks' : 'No of sessions'
                    }
                    variant='outlined'
                    size='small'
                    disabled={filterData?.course_type?.id === 1}
                    value={noOfPeriods}
                    inputProps={{
                      min: 0,
                      max: 1000,
                      maxLength: 4,
                      readOnly: data.length > 0,
                    }}
                    name='noofperiods'
                    onChange={(e) => handleNoOfPeriods(e)}
                    required
                  />
                </Grid>
              ) : (
                ''
              )}
              <Grid item xs={12}>
                <Divider />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static1'
                  label='Course Title'
                  placeholder='Course Title'
                  multiline
                  rows='1'
                  color='secondary'
                  style={{ width: '100%' }}
                  value={title}
                  variant='outlined'
                  onChange={(e) => setTitle(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static2'
                  label='Course Prerequisites'
                  placeholder='Course Prerequisites'
                  multiline
                  rows='6'
                  color='secondary'
                  style={{ width: '100%' }}
                  value={coursePre}
                  variant='outlined'
                  onChange={(e) => setCoursePre(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static3'
                  label='What Will You Learn From This Course'
                  placeholder='What Will You Learn From This Course'
                  multiline
                  rows='6'
                  color='secondary'
                  style={{ width: '100%' }}
                  value={learn}
                  variant='outlined'
                  onChange={(e) => setLearn(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static4'
                  label='Course Overview'
                  placeholder='Course Overview'
                  multiline
                  rows='6'
                  color='secondary'
                  style={{ width: '100%' }}
                  value={overview}
                  variant='outlined'
                  onChange={(e) => setOverview(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static5'
                  label='Course URL Title'
                  placeholder='Course URL Title'
                  multiline
                  rows='1'
                  required
                  color='secondary'
                  style={{ width: '100%' }}
                  value={metaTitle}
                  variant='outlined'
                  onChange={(e) => setMetaTitle(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static5'
                  label='Course Meta Title'
                  placeholder='Course Meta Title'
                  multiline
                  rows='1'
                  required
                  color='secondary'
                  style={{ width: '100%' }}
                  value={courseMetaTitle}
                  variant='outlined'
                  onChange={(e) => setCourseMetaTitle(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  className='multiRowTextfield'
                  id='outlined-multiline-static4'
                  label='Course Meta Description'
                  placeholder='Course Description'
                  multiline
                  rows='6'
                  color='secondary'
                  style={{ width: '100%' }}
                  value={courseDescription}
                  variant='outlined'
                  onChange={(e) => setCourseDescription(e.target.value)}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography>Course footer content</Typography>
                {editFlag ? handleTinyMce() : ''}
                {!editFlag ? handleTinyMce() : ''}
              </Grid>
              <Grid item xs={12}>
                <Typography>Show course in {displayName?displayName:''} Dashboard</Typography>
                No&nbsp;
                <Switch
                  checked={isShow}
                  onChange={(e) => setIsShow(e.target.checked)}
                  name='checkedA'
                  inputProps={{ 'aria-label': 'secondary checkbox' }}
                />
                &nbsp;Yes
              </Grid>
              <div className='attachmentContainer'>
                <div style={{ display: 'flex' }}>
                  {filePath?.length > 0
                    ? filePath?.map((file, i) => (
                        <FileRow
                        name={selectedFileName}
                        key={`homework_student_question_attachment_${i}`}
                        file={file}
                        index={i}
                        onClose={() => removeFileHandler(i, 'doc')}
                      />
                      ))
                    : null}
                </div>

                {filePath?.length < 1 && (
                  <div className='attachmentButtonContainer'>
                    <div>
                      <Button
                        startIcon={
                          <SvgIcon
                            component={() => (
                              <img
                                style={{ height: '20px', width: '20px' }}
                                src={attachmenticon}
                              />
                            )}
                          />
                        }
                        className='attachment_button_doc'
                        title='Attach Supporting File'
                        variant='contained'
                        size='small'
                        disableRipple
                        disableElevation
                        disableFocusRipple
                        disableTouchRipple
                        component='label'
                        style={{ textTransform: 'none' }}
                      >
                        <input
                          type='file'
                          style={{ display: 'none' }}
                          id='raised-button-file'
                          // accept='image/*'
                          onChange={handleImageChange}
                        />
                        Add Document
                      </Button>
                    </div>
                    {isLodding === 1 && (
                      <div style={{ width: '200px', margin: '10px' }}>
                        <LinearProgressBar value={progress} color='secondary' />
                      </div>
                    )}
                  </div>
                )}

                {thumbnailImage !== '' && (
                  <FileRow
                    name={selectedThumbnail}
                    key='Thumbnail'
                    file={thumbnailImage}
                    onClose={() => removeFileHandler(0, 'thumbnail')}
                  />
                )}

                {thumbnailImage === '' && (
                  <div className='attachmentButtonContainer'>
                    <div>
                      <Button
                        startIcon={
                          <SvgIcon
                            component={() => (
                              <img
                                style={{ height: '20px', width: '20px' }}
                                src={attachmenticon}
                              />
                            )}
                          />
                        }
                        className='attachment_button_doc'
                        title='Attach Supporting File'
                        variant='contained'
                        size='small'
                        disableRipple
                        disableElevation
                        disableFocusRipple
                        disableTouchRipple
                        component='label'
                        style={{ textTransform: 'none' }}
                      >
                        <input
                          type='file'
                          style={{ display: 'none' }}
                          id='raised-button-file'
                          accept='image/*'
                          onChange={handleThumbnail}
                        />
                        Add Thumbnail
                      </Button>
                    </div>
                    {isLodding === 2 && (
                      <div style={{ width: '200px', margin: '10px' }}>
                        <LinearProgressBar value={progress} color='secondary' />
                      </div>
                    )}
                  </div>
                )}
              </div>

              <Grid item xs={12} sm={12}>
                <Divider />
              </Grid>
              <Grid item xs={12} sm={12} className={isMobile ? '' : 'filterPadding'}>
                <Button onClick={handleBackToCourseList} className='periodBackButton1'>
                  Back
                </Button>
                <Button
                  className='nextPageButton'
                  onClick={handleNext}
                  style={{ float: 'right' }}
                >
                  NEXT
                  {/* {filterData?.course_type?.id === 1 ? 'SUBMIT' : 'NEXT'} */}
                </Button>
              </Grid>
            </Grid>
          )
        ) : (
          <>
            <Paper className={classes.root}>
              <Grid container className='periodCardsContainer' spacing={isMobile ? 3 : 5}>
                {data?.map((_, i) => (
                  <Grid item xs={12} sm={4}>
                    <CourseCard
                      gradeKey={gradeKey}
                      setNoPeriods={setNoPeriods}
                      key={i}
                      index={i}
                      cData={data}
                      setData={setData}
                    />
                  </Grid>
                ))}
                {!gradeKey && (
                  <Grid item xs={12} sm={4}>
                    {data.length < 200 && (
                      <Button onClick={handleAddPeriod} className='periodAddButton'>
                        <AddOutlinedIcon style={{ fontSize: '100px' }} />
                      </Button>
                    )}
                  </Grid>
                )}
              </Grid>
            </Paper>
            <div className='submitContainer'>
              <Grid item xs={12} sm={12}>
                <div className='buttonContainer'>
                  <Button onClick={handleBack} className='periodBackButton'>
                    Back
                  </Button>
                  {!gradeKey && (
                    <Button
                      onClick={editFlag ? handleEdit : handleSubmit}
                      className='periodSubmitButton'
                    >
                      Submit
                    </Button>
                  )}
                </div>
              </Grid>
            </div>
          </>
        )}
      </Layout>
    </>
  );
};

export default CreateCourse;
