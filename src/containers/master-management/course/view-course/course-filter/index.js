import React, { useState, useEffect, useContext } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Grid, TextField, Button, useTheme } from '@material-ui/core';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Autocomplete from '@material-ui/lab/Autocomplete';
import GetAppIcon from '@material-ui/icons/GetApp';
import Box from '@material-ui/core/Box';
// import useStyles from './useStyles';
import Divider from '@material-ui/core/Divider';
import endpoints from '../../../../../config/endpoints';
import axiosInstance from '../../../../../config/axios';
import { AlertNotificationContext } from '../../../../../context-api/alert-context/alert-state';
import '../../create-course/style.css';

const CourseFilter = ({
  handleCourseList,
  setCourseData,
  setPageFlag,
  handleClearFilter,
  tabValue,
  page,
  setPage,
}) => {
  const themeContext = useTheme();
  const { gradeKey } = useParams();
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const aolHostURL = window.location.host;
  const wider = isMobile ? '-10px 0px' : '-10px 0px 20px 8px';
  const widerWidth = isMobile ? '98%' : '95%';
  const [gradeDropdown, setGradeDropdown] = useState([]);
  const [gradeIds, setGradeIds] = useState([]);

  const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
  const [moduleId, setModuleId] = useState('');
  const [hRef, setHRef] = useState([]);
  const [selectDropDown,setSelectDropDown] =useState([])
  const [branch,setBranch] = useState([]);
  const [branchId, setBranchId] = useState(null)

  useEffect(() => {
    if (NavData && NavData.length) {
      NavData.forEach((item) => {
        if (
          item.parent_modules === 'Master Management' &&
          item.child_module &&
          item.child_module.length > 0
        ) {
          item.child_module.forEach((item) => {
            if (item.child_name === 'Course') {
              setModuleId(item.child_id);
            }
          });
        }
      });
    }
    setHRef([
      {
        csv: `${endpoints.onlineCourses.downloadAllCoursesApi}?export_type=csv`,
      },
    ]);
  }, []);

  const [filterData, setFilterData] = useState({
    // branch: '',
    grade: [],
  });

  // const branchDrop = [{ branch_name: 'AOL' }];

  const handleClear = () => {
    setFilterData({
      grade: [],
      // branch: '',
    });
    setGradeIds([]);
    // setBranchId([]);
    setBranch([])
    // setCourseData([]);
    setPageFlag(false);
    handleClearFilter();
  };

  const handleFilter = () => {
    handleCourseList(gradeIds, tabValue,branchId);
  };

  useEffect(() => {
    if (moduleId && branchId) {
      let url = `${endpoints.communication.grades}`;
      if (aolHostURL === endpoints.aolConfirmURL)
        url += `?branch_id=${branchId}&module_id=${moduleId}`;
      else url += `?branch_id=${branchId}&module_id=${moduleId}`;

      axiosInstance
        .get(url)
        .then((result) => {
          if (result.data.status_code === 200) {
            setGradeDropdown(result?.data?.data);
          } else {
            setAlert('error', result?.data?.message);
            setGradeDropdown([]);
          }
        })
        .catch((error) => {
          setAlert('error', error.message);
          setGradeDropdown([]);
        });
    }
  }, [moduleId, branchId]);

  useEffect(() => {
    axiosInstance
    .get(`${endpoints.studentDesk.branchSelect}`
    )
    .then((res) => {
      setSelectDropDown(res.data.data)

    })
  }, [])

  // const handleBranch = (event, value) => {
  //   setFilterData({ ...filterData, branch: '' });
  //   if (value) {
  //     setFilterData({
  //       ...filterData,
  //       branch: value,
  //     });
  //     axiosInstance
  //       .get(`${endpoints.communication.grades}?branch_id=${5}&module_id=8`)
  //       .then((result) => {
  //         if (result.data.status_code === 200) {
  //           setGradeDropdown(result?.data?.data);
  //         } else {
  //           setAlert('error', result?.data?.message);
  //           setGradeDropdown([]);
  //         }
  //       })
  //       .catch((error) => {
  //         setAlert('error', error.message);
  //         setGradeDropdown([]);
  //       });
  //   } else {
  //     setGradeDropdown([]);
  //   }
  // };

  useEffect(() => {
    if (gradeKey && moduleId && branchId) {
      let url = `${endpoints.communication.grades}`;
      if (aolHostURL === endpoints.aolConfirmURL)
        url += `?branch_id=${branchId}&module_id=${moduleId}`;
      else url += `?branch_id=${branchId}&module_id=${moduleId}`;

      axiosInstance
        .get(url)
        .then((result) => {
          if (result.data.status_code === 200) {
            setGradeDropdown(result?.data?.data);
            const gradeObj = result.data?.data?.find(
              ({ grade_id }) => grade_id === Number(gradeKey)
            );
            if (gradeKey > 0) {
              setFilterData({
                grade: gradeObj,
              });
              setGradeIds(gradeKey);
              handleCourseList(gradeKey);
            } else history.push('/course-list');
          } else {
            setAlert('error', result?.data?.message);
            setGradeDropdown([]);
          }
        })
        .catch((error) => {
          setAlert('error', error.message);
          setGradeDropdown([]);
        });
    }
  }, [gradeKey, moduleId]);

  const handleGrade = (event, value) => {
    setFilterData({ ...filterData, grade: '' });
    setPage(1);
    if (value) {
      setGradeIds(value.grade_id);
      setFilterData({
        ...filterData,
        grade: value,
      });
    }
  };

  return (
    <>
      <Grid
        container
        spacing={isMobile ? 3 : 5}
        style={{ width: widerWidth, margin: wider }}
      >
        {/* <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={handleBranch}
            id='grade'
            className='dropdownIcon'
            value={filterData?.branch}
            options={branchDrop}
            getOptionLabel={(option) => option?.branch_name}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Branch'
                placeholder='Branch'
              />
            )}
          />
        </Grid> */}
          {/* <Grid item md={12} xs={12}> */}
          <Grid item md={3} xs={12}>
            <Autocomplete
              size='small'
              className='dropdownIcon'
              options={selectDropDown || []}
              getOptionLabel={(option) => option?.branch_name || ''}
              filterSelectedOptions
              value={branch || ''}
              onChange={(event, value) => {
                // console.log(value,'ml999999')
                setBranchId(value?.id)
                setBranch(value)
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  size='small'
                  variant='outlined'
                  label='Branch'
                  placeholder='Select Branch'
                />
              )}
            />
          </Grid>
        {/* </Grid> */}
        <Grid item xs={12} sm={3} className={isMobile ? '' : 'filterPadding'}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={handleGrade}
            id='volume'
            className='dropdownIcon'
            value={filterData?.grade}
            options={gradeDropdown}
            getOptionLabel={(option) => option?.grade__grade_name}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Grade'
                placeholder='Grade'
              />
            )}
          />
        </Grid>
      </Grid>
      <Grid
        container
        spacing={isMobile ? 3 : 5}
        style={{ width: widerWidth, margin: wider }}
      >
        <Grid item xs={6} sm={2} className={isMobile ? '' : 'addButtonPadding'}>
          <Button
            variant='contained'
            className='labelColor buttonModifiedDesign'
            size='medium'
            onClick={handleClear}
          >
            CLEAR ALL
          </Button>
        </Grid>
        <Grid item xs={6} sm={2} className={isMobile ? '' : 'addButtonPadding'}>
          <Button
            variant='contained'
            style={{ color: 'white' }}
            color='primary'
            className='buttonModifiedDesign'
            size='medium'
            onClick={handleFilter}
          >
            FILTER
          </Button>
        </Grid>
        <div>
          <Divider
            orientation='vertical'
            style={{
              backgroundColor: '#014e7b',
              height: '40px',
              marginTop: '1rem',
              marginLeft: '2rem',
              marginRight: '1.25rem',
            }}
          />
        </div>
        <Grid
          item
          xs={6}
          sm={2}
          className={isMobile ? 'createButton' : 'createButton addButtonPadding'}
        >
          <Button
            startIcon={<AddOutlinedIcon style={{ fontSize: '30px' }} />}
            variant='contained'
            style={{ color: 'white' }}
            color='primary'
            className='buttonModifiedDesign'
            onClick={() => {
              sessionStorage.removeItem('isAol');
              sessionStorage.removeItem('gradeKey');
              sessionStorage.setItem('createCourse', 1);
              sessionStorage.removeItem('periodDetails');
              sessionStorage.removeItem('isErpClass');
              history.push('/create/course');
            }}
            size='medium'
          >
            CREATE
          </Button>
        </Grid>
        <Grid
          item
          md={3}
          xs={6}
          sm={2}
          className={isMobile ? 'createButton' : 'createButton addButtonPadding'}
        >
          <Button
            startIcon={<GetAppIcon style={{ fontSize: '30px' }} />}
            variant='contained'
            style={{ color: 'white' }}
            color='primary'
            className='buttonModifiedDesign'
            size='medium'
            href={hRef && hRef[0] && hRef[0].csv}
          >
            all course download
          </Button>
        </Grid>
        <Grid
          item
          // md={4}
          xs={6}
          sm={2}
          className={isMobile ? 'createButton' : 'createButton addButtonPadding'}
        >
          <Button
            startIcon={<AddOutlinedIcon style={{ fontSize: '30px' }} />}
            variant='contained'
            style={{ color: 'white' }}
            color='primary'
            className='buttonModifiedDesign'
            size='medium'
            onClick={() => {
              history.push('/import/course/');
            }}
          >
            Course Import
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default CourseFilter;
