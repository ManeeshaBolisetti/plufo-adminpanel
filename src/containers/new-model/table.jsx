import React, { useContext, useState, useEffect } from 'react';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import axios from 'axios';
import clsx from 'clsx';
import Layout from '../Layout';
import { SentimentDissatisfiedTwoTone, SettingsApplicationsTwoTone } from '@material-ui/icons';
import Loader from '../../components/loader/loader';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import { Autocomplete, Pagination } from '@material-ui/lab';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  MenuItem,
  Switch,
} from '@material-ui/core';
import moment from 'moment';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Checkbox from '@mui/material/Checkbox';
import FilterIcon from '../../components/icon/FilterIcon';
import ArrowUpwardRoundedIcon from '@material-ui/icons/ArrowUpwardRounded';
import ArrowDownwardRoundedIcon from '@material-ui/icons/ArrowDownwardRounded';
import CustomSearchBar from '../../components/custom-seearch-bar';
import displayName from '../../config/displayName';




const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    boxShadow: 'none',
    margin: '5px 0 0 0',
  },
  container: {
    maxHeight: '70vh',
  },
  buttonContainer: {
    background: theme.palette.background.secondary,
    paddingBottom: theme.spacing(2),
  },
  centerInMobile: {
    width: '100%',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center',
    },
  },
  columnHeader: {
    color: `${theme.palette.secondary.main} !important`,
    fontWeight: 600,
    fontSize: '1rem',
    backgroundColor: `#ef676a; !important`,
  },
  tableCell: {
    color: theme.palette.secondary.main,
    padding: 20,
  },
  filterContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    padding: '0 45px',
    margin: '-45px 0 0 0',
    '@media (max-width: 900px)': {
      margin: '-15px 0 0 0',
    },
  },
  filterAccordion: {
    width: '50%',
    backgroundColor: 'transparent',
    '@media (max-width: 600px)': {
      width: '100%',
    },
    '@media (min-width: 601px) and (max-width: 900px)': {
      width: '65%',
    },
    '@media (min-width: 901px) and (max-width: 1350px)': {
      width: '80%',
    },
  },
  createMeetingButton: {
    margin: '0 0 0 10px',
  },
  filterContent: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    flexGrow: 1,
    '@media (max-width: 900px)': {
      display: 'grid',
      gap: '20px',
      margin: '0 0 0 50px',
    },
  },
  filterAutocomplete: {
    width: '70%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  filterButton: {
    width: '20%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
    // margin: '5px 0 0 0',
  },
  isSearchFilter: {
    width: '25%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
  },
  isActiveFilter: {
    width: '20%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
  },
  isOrderingFilter: {
    width: '30%',
    '@media (max-width: 900px)': {
      width: '100%',
    },
  },
  tableHeaderCell: {
    textTransform: 'capitalize',
    background: 'primary',
  },
  hideTableHeaderCells: {
    display: 'none',
  },
  container: {
    overflow: 'none !important',
  },
}));

const TabularField = () => {
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const { is_superuser } = JSON.parse(localStorage.getItem('userDetails')) || {};

  const { erp_user_id } =
    JSON.parse(localStorage.getItem('userDetails')).role_details || {};
  const [academicYear, setAcademicYear] = useState([]);
  const [totalCount, setTotalCount] = useState(0);
  const [tableHeader, setTableHeader] = useState(null);
  const [tableHeaderPost, setTableHeaderPost] = useState(null);
  const [meetingData, setMeetingData] = useState(null);
  const [searchText, setSearchText] = useState('');
  const [dialogOpen, setDialogOpen] = useState(false);
  const [studentList, setStudentList] = useState(null);
  const [teacherList, setTeacherList] = useState(null);
  const [meetingName, setMeetingName] = useState('');
  const [studentID, setStudentID] = useState(undefined);
  const [teacherID, setTeacherID] = useState(undefined);
  const [selectedStudent, setSelectedStudent] = useState({});
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [availableSlot, setAvailableSlot] = useState(null);
  const [accordianOpen, setAccordianOpen] = useState(false);
  const [isButtonDisable, setIsButtonDisable] = useState(false);
  const [data, setData] = useState({
    dataPerPage: 10,
    totalData: null,
    totalPages: null,
    currentPage: 1,
  });
  const [actionsData, setActionsData] = useState([]);
  const [element, setElement] = useState([]);
  const [loading, setLoading] = useState(false);
  const [filterInput, setFilterInput] = useState({
    meetingNameFilter: '',
    meetingDateFilter: '',
    studentNameFilter: '',
    teacherNameFilter: '',
    meetingTypeFilter: 'upcoming',
  });
  const [listInfo, setListInfo] = useState([]);
  const [checked, setChecked] = useState(false);
  const [values, setValue] = useState({});
  const [keyStore, setKeyStore] = useState([]);
  const [edit, setEdit] = useState(false);
  const [editId, setEditId] = useState(null);
  const [search, setSearch] = useState('');

  let url = '';
    if (displayName === 'My SparkleBox') {
     url = 'https://dev.erp.mysparklebox.com/qbox/academic-year/';
    } else if (displayName === 'SparkleBox School') {
      url = 'https://dev.plufo.letseduvate.com/qbox/academic-year/';
    } else {
      url = 'https://dev.plufo.letseduvate.com/qbox/academic-year/';

    }

  const createHeaders = (data) => {
    let headers = Object.keys(data).map((eachData) => eachData.replace('_', ' '));
    setTableHeader(headers);
  };

  function createHeadersPost(data) {
    setKeyStore(Object.keys(data));
    let headersPost = Object.keys(data).map((item) => actionsData[item]);
    setTableHeaderPost(headersPost);
  }

  function Options(data) {
    // axiosInstance
    //   .options(`${endpoints.academicYearCRUDL.academicYearCRUDL}`)
    axios.options(`${url}`)
      .then((result) => {
        if (result.status === 200) {
          {
            setListInfo(result.data);
            setActionsData(result.data?.actions?.POST);
            setElement(data);
          }
        } else {
          setAlert('error', result.data.error_message);
        }
      })
      .catch((error) => {
        setAlert('error', error.message);
      });

  }

  useEffect(() => {
    handleAcademicYear();
  }, []);

  useEffect(() => {
    createHeadersPost(element);
  }, [element])


  const handleAcademicYear = () => {
    // axiosInstance
    //   .get(`${endpoints.academicYearCRUDL.academicYearCRUDL}`)
    axios.get(`${url}`)
      .then((result) => {
        if (result.status === 200) {
          {
            createHeaders(result.data.result[0]);
            Options(result.data.result[0]);
            setTotalCount(result.data.count);
            setAcademicYear(result.data.result);
            setData({
              ...data,
              totalData: result.data.count,
              totalPages: result.data.total_pages,
              currentPage: result.data.current_page,
            });
          }
        } else {
          setAlert('error', result.data.error_message);
        }
      })
      .catch((error) => {
        setAlert('error', error.message);
      });
  };



  const confirm = async () => {
    setIsButtonDisable(true);
    // let result1 = {
    //   session_year: values.session_year,
    //   studentID: studentID,
    //   date: date,
    //   time: time,
    // };

    // let params = `erp_id=${studentID}&meeting_name=${meetingName}&time_slot=${time}&date=${date}`;
    // if (is_superuser) {
    //   params = params + `&teacher_id=${teacherID}`;
    // } else {
    //   params = params + `&teacher_id=${erp_user_id}`;
    // }
    // console.log(result1);
    // if (
    //   studentID !== undefined &&
    //   meetingName.length > 0 &&
    //   time.length > 0 &&
    //   date.length > 0
    // ) {
    // console.log('hello');
    // const result = await axiosInstance.post(
    //   `${endpoints.academicYearCRUDL.academicYearCRUDL}`, values,
    //   {
    //     headers: {
    //       Authorization: `Bearer ${token}`,
    //     },
    //   }
    // );
    if (edit) {
      console.log('check-put');
      const result = await axios.put(`${url}${editId}/`, values);
      console.log('result-put:', result);
      if (result.status === 200) {
        // setAlert('success', result.data.message);
        setAlert('success', 'successfully updated');
        setDialogOpen(false);
        setIsButtonDisable(false);
        setEditId(null);
        handleAcademicYear();
      } else {
        setAlert('error', 'something went wrong');
      }
    } else {
      console.log('check-post');
      const result = await axios.post(`${url}`, values);
      console.log('result-post:', result);
      if (result.status === 201) {
        // setAlert('success', result.data.message);
        setAlert('success', 'successfully added');
        setDialogOpen(false);
        setIsButtonDisable(false);
        handleAcademicYear();
      } else {
        // setAlert('error',result.data.message);
        setAlert('error', 'something went wrong');

      }
    }

    // }
  };


  const closeDialog = () => {
    setDialogOpen(false);
    setSelectedStudent({});
    setValue({});
    setStudentID('');
    setMeetingName('');
    setDate('');
    setTime('');
    setEdit(false);
    handleAcademicYear();
  };

  // Create Dynamic change Functions for

  const handleChange = (event, Id) => {
    setValue(currentValues => {
      currentValues[Id] = event;
      return currentValues;
    });
  };


  function handleEditYear(data) {
    setValue({});
    // axiosInstance.get(`${endpoints.academicYearCRUDL.academicYearCRUDL}${data.id}/`).then((response)=>{
    //   console.log('response: ', response);
    // })
    axios.get(`${url}${data.id}/`).then((response) => {
      if (response.status === 200) {
        keyStore.map((item) => {
          setValue(currentValues => {
            currentValues[item] = response.data[item];
            return currentValues;
          });
        });
        setEdit(true);
        setEditId(data.id);
        setDialogOpen(true);
      }
    })
  }

  function handleDelete(data) {
    // axiosInstance.delete(`${endpoints.academicYearCRUDL.academicYearCRUDL}${data.id}/`).then((response)=>{
    //   console.log('response: ', response);
    // })
    axios.delete(`${url}${data.id}/`).then((response) => {
      console.log('response-delete: ', response);
      if (response.status === 204) {
        // setAlert('success', response.data.message);
        setAlert('success', 'Successfully Deleted');
        handleAcademicYear();
      } else {
        // setAlert('error', response.data.message);
        setAlert('error', 'something went wrong');
      }
    })
  }

  function handleIsActive(checked, data) {
    keyStore.map((item) => {
      setValue(currentValues => {
        if (item === 'is_active') {
          currentValues[item] = checked;
        } else {
          currentValues[item] = data[item];
        }
        return currentValues;
      });
    });
    axios.put(`${url}${data.id}/`, values).then((response) => {
      console.log('result-put:', response);
      if (response.status === 200) {
        // setAlert('success', result.data.message);
        setAlert('success', 'successfully updated');
        handleAcademicYear();
      } else {
        setAlert('error', 'something went wrong');
      }
    })
  }

  function clearFilter() {
    console.log('clearFilter')
  }

  function handleFilter(item, e) {
    console.log("item:", item, e.target.value);
  }

  function handleOrdering(eachColumn) {
    console.log("ordering:", eachColumn)
    let ordering = eachColumn.replace(' ', '_');
    axios.get(`${url}filters/?ordering=${ordering}`).then((response) => {
      console.log("response-ordering:", response)
      if (response.status === 200) {
        createHeaders(response.data[0]);
        Options(response.data[0]);
        setTotalCount(response.data.count);
        setAcademicYear(response.data);
        setData({
          ...data,
          totalData: response.data.count,
          totalPages: response.data.total_pages,
          currentPage: response.data.current_page,
        });
      }
    })
  }

  function handleSearch(e) {
    setSearch(e.target.value);
    axios.get(`${url}filters/?search=${e.target.value}`).then((response) => {
      console.log("response-search:", response)
      if (response?.status === 200 && response.data.length > 0) {
        createHeaders(response?.data[0]);
        Options(response?.data[0]);
        setTotalCount(response?.data.count);
        setAcademicYear(response?.data);
        setData({
          ...data,
          totalData: response?.data.count,
          totalPages: response?.data.total_pages,
          currentPage: response?.data.current_page,
        });
      }else{
        setAlert('error','Not Data Found')
      }
    })
  }

  return (
    <>
      <Layout>
        <div className='connection-pod-container'>
          {loading ? (
            <Loader />
          ) : (
            <>
              <div className='connection-pod-breadcrumb-wrapper'>
                <CommonBreadcrumbs
                  componentName='Master Management'
                  childComponentName={listInfo.name}
                />
              </div>
              <div className='filter-container'>
                <Grid container spacing={3} alignItems='center'>
                  <Grid item sm={1} xs={12}>
                    <Tooltip title='Create' placement='bottom' arrow>
                      <IconButton
                        className='create-meeting-button'
                        color='primary'
                        onClick={() => {
                          setValue({});
                          setEdit(false);
                          setDialogOpen(true);
                        }}
                      >
                        <AddIcon style={{ color: '#ffffff' }} />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                  <Grid item sm={11} xs={12}>
                    <Accordion expanded={accordianOpen}>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls='panel1a-content'
                        id='panel1a-header'
                        onClick={() => setAccordianOpen(!accordianOpen)}
                      >
                        {/* <FilterIcon /> */}
                        <Typography variant='h6' color='primary'>
                          Filter
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <Grid container spacing={3} alignItems='center'>
                          <Grid container spacing={3} alignItems='center' style={{ display: 'flex' }}>
                            {tableHeaderPost && tableHeaderPost?.map((item, i) =>
                              item.read_only === false ?
                                item.type === 'string' ?
                                  (
                                    <Grid item md={3} sm={4} xs={12}>
                                      <TextField
                                        label={item.label}
                                        fullWidth
                                        className='student-name'
                                        variant='outlined'
                                        size='small'
                                        name='searchText'
                                        value={item[i]}
                                        onChange={(e) => {
                                          handleFilter(keyStore[i], e);
                                        }}
                                        disabled={item.read_only}
                                        inputProps={{ maxLength: `${item.max_length}` }}
                                      />
                                    </Grid>
                                  )
                                  :
                                  item.type === 'date' ?
                                    (
                                      <Grid item md={3} sm={4} xs={12}>
                                        <TextField
                                          type={item.type}
                                          disabled={item.read_only}
                                          label={item.label}
                                          fullWidth
                                          className='student-name'
                                          variant='outlined'
                                          size='small'
                                          name='searchText'
                                          value={item[i]}
                                          onChange={(e) => {
                                            handleFilter(keyStore[i], e);
                                          }}
                                          InputProps={{ inputProps: { min: new Date().toISOString().split('T')[0] } }}
                                          InputLabelProps={{
                                            shrink: true,
                                            maxLength: `${item.max_length}`
                                          }}
                                        />
                                      </Grid>
                                    )
                                    :
                                    item.type === 'boolean' ?
                                      (
                                        <Grid item md={3} sm={4} xs={12}>
                                          <FormControl fullWidth margin='dense' variant='outlined'>
                                            <InputLabel>{item.label}</InputLabel>
                                            <Select
                                              value={item[i]}
                                              label={item.label}
                                              onChange={(e) => {
                                                handleFilter(keyStore[i], e);
                                              }}
                                            >
                                              <MenuItem value={''}></MenuItem>
                                              <MenuItem value={'true'}>True</MenuItem>
                                              <MenuItem value={'false'}>False</MenuItem>
                                            </Select>
                                          </FormControl>
                                        </Grid>
                                      )
                                      :
                                      (
                                        <Grid item md={3} sm={4} xs={12}>
                                          <TextField
                                            required={item.required}
                                            helperText={item.help_text}
                                            type={item.type}
                                            readOnly={item.read_only}
                                            label={item.label}
                                            fullWidth
                                            className='teacher-name'
                                            variant='outlined'
                                            size='small'
                                            autoComplete='off'
                                            name='searchText'
                                            value={item[i]}
                                            onChange={(e) => {
                                              handleFilter(keyStore[i], e);
                                            }}
                                            disabled={item.read_only}
                                            inputProps={{ maxLength: `${item.max_length}` }}
                                          />
                                        </Grid>
                                      )
                                :
                                ''
                            )}
                          </Grid>
                          {is_superuser && (
                            <Grid item md={3} sm={4} xs={12}>
                              <TextField
                                fullWidth
                                className='teacher-name'
                                label='Teacher Name'
                                variant='outlined'
                                size='small'
                                autoComplete='off'
                                name='teacherNameFilter'
                                value={filterInput.teacherNameFilter}
                              // onChange={(e) => {
                              //   setFilterValue(e);
                              // }}
                              />
                            </Grid>
                          )}

                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='student-name'
                              label='Student Name'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='studentNameFilter'
                              value={filterInput.studentNameFilter}
                            // onChange={(e) => {
                            //   setFilterValue(e);
                            // }}
                            />
                          </Grid>
                          <Grid item md={3} sm={4} xs={12}>
                            <CustomSearchBar
                              value={search}
                              onChange={(e) => handleSearch(e)}
                              label={listInfo.name}
                              placeholder='Search'
                            />
                          </Grid>
                          <Grid item sm={2} xs={3}>
                            <Button
                              className='filter-button'
                              onClick={() => clearFilter()}
                            >
                              Clear Filter
                            </Button>
                          </Grid>
                        </Grid>
                      </AccordionDetails>
                    </Accordion>
                  </Grid>
                </Grid>
              </div>
              <Paper className={`${classes.root} common-table`}>
                <TableContainer className={classes.container}>
                  <Table stickyHeader aria-label='sticky table'>
                    <TableHead className={classes.tableHeaderRow}>
                      <TableRow>
                        {tableHeader &&
                          tableHeader.map((eachColumn, index) => {
                            return (
                              <TableCell
                                className={clsx([classes.tableHeaderCell], {
                                  [classes.hideTableHeaderCells]: eachColumn === 'is active',
                                })}
                                key={index}
                              >
                                {eachColumn === 'id' ? 'Sl No.' : eachColumn}
                                &nbsp;&nbsp;&nbsp;
                                {eachColumn.includes('date') || eachColumn.includes('id') ?
                                  (
                                    ''
                                  )
                                  :
                                  (
                                    <>
                                      <ArrowUpwardRoundedIcon
                                        onClick={(e) => handleOrdering(eachColumn)}
                                        style={{ fontSize: 'initial', cursor: 'pointer' }}
                                      />
                                      <ArrowDownwardRoundedIcon
                                        onClick={(e) => handleOrdering(`-` + eachColumn)}
                                        style={{ fontSize: 'initial', cursor: 'pointer' }}
                                      />
                                    </>
                                  )
                                }
                              </TableCell>
                            );
                          })}
                        <TableCell>Actions</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {academicYear.map((year, index) => {
                        return (
                          <TableRow hover academicyear='checkbox' tabIndex={-1} key={index}>
                            {/* <TableCell className={classes.tableCell}>
                              {data.currentPage == 1
                                ? index + data.currentPage
                                : (data.currentPage - 1) * data.dataPerPage + (index + 1)}

                            </TableCell> */}
                            {keyStore.map((item) =>
                              item === "is_active" ?
                                (
                                  // <TableCell className={classes.tableCell}>
                                  //   <Checkbox
                                  //     checked={year[item]}
                                  //     InputProps={{
                                  //       'aria-label': 'controlled'
                                  //     }}
                                  //   />
                                  // </TableCell>
                                  ''
                                )
                                :
                                (
                                  <TableCell className={classes.tableCell}>
                                    {year[item]}
                                  </TableCell>
                                )

                            )
                            }
                            <TableCell className={classes.tableCell}>
                              <IconButton
                                onClick={(e) => {
                                  handleDelete(year);
                                }}
                                title='Delete Academic Year'
                              >
                                <DeleteOutlinedIcon style={{ color: '#fe6b6b' }} />
                              </IconButton>
                              <Switch
                                checked={year.is_active}
                                onChange={(e) => {
                                  handleIsActive(!(year.is_active), year);
                                }}
                                inputProps={{ 'aria-label': 'controlled' }}
                              />

                              <IconButton
                                onClick={(e) => handleEditYear(year)}
                                title='Edit Academic Year'
                              >
                                <EditOutlinedIcon style={{ color: '#fe6b6b' }} />
                              </IconButton>
                            </TableCell>
                          </TableRow>
                        );
                      }).reverse()
                      }
                    </TableBody>
                  </Table>
                </TableContainer>
              </Paper>
            </>
          )}
        </div>
      </Layout>
      <Dialog open={dialogOpen} className='create-meetinng-dialog'>
        <DialogTitle className='dialog-title'>Create</DialogTitle>
        <div className='meeting-form'>
          {/* {is_superuser && (
            
          )} */}
          {tableHeaderPost && tableHeaderPost?.map((item, i) =>
            item.read_only === false ?
              item.type === 'string' ?
                (
                  <TextField
                    required={item.required}
                    helperText={item.help_text}
                    type={item.type}
                    label={item.label}
                    fullWidth
                    className='filter-student meeting-form-input'
                    variant='outlined'
                    size='small'
                    value={values[keyStore[i]]}
                    onChange={(e) => {
                      values[keyStore[i]] = e.target.value;
                      handleChange(values[keyStore[i]], keyStore[i])
                    }
                    }
                    disabled={item.read_only}
                    inputProps={{ maxLength: `${item.max_length}`, shrink: true, 'aria-label': 'controlled' }}
                  />
                )
                :
                item.type === 'date' ?
                  (
                    <TextField
                      required={item.required}
                      helperText={item.help_text}
                      type={item.type}
                      disabled={item.read_only}
                      label={item.label}
                      fullWidth
                      className='filter-student meeting-form-input'
                      variant='outlined'
                      size='small'
                      value={values[keyStore[i]]}
                      onChange={(e) => {
                        values[keyStore[i]] = e.target.value;
                        handleChange(values[keyStore[i]], keyStore[i])
                      }
                      }
                      // onChange={(e) => setDate(moment(e.target.value).format('YYYY-MM-DD'))}
                      // min={new Date().toISOString().split('T')[0]}
                      InputProps={{ inputProps: { min: new Date().toISOString().split('T')[0] } }}
                      InputLabelProps={{
                        shrink: true,
                        maxLength: `${item.max_length}`,
                        'aria-label': 'controlled'
                      }}
                    />
                  )
                  :
                  item.type === 'boolean' ?
                    (
                      <Checkbox
                        onChange={(e) => {
                          setChecked(!checked);
                          handleChange(!checked, keyStore[i])
                        }
                        }
                        required={item.required}
                        helperText={item.help_text}
                        label={item.label}
                        disabled={item.read_only}
                        value={values[keyStore[i]]}
                        checked={values[keyStore[i]] ? values[keyStore[i]] : checked}
                        InputProps={{
                          'aria-label': 'controlled'
                        }}
                      />
                    )
                    :
                    (
                      <TextField
                        required={item.required}
                        helperText={item.help_text}
                        type={item.type}
                        readOnly={item.read_only}
                        label={item.label}
                        fullWidth
                        className='filter-student meeting-form-input'
                        variant='outlined'
                        size='small'
                        value={values[keyStore[i]]}
                        onChange={(e) => {
                          values[keyStore[i]] = e.target.value;
                          handleChange(e.target.value, keyStore[i])
                        }
                        }
                        disabled={item.read_only}
                        inputProps={{ maxLength: `${item.max_length}`, shrink: true, 'aria-label': 'controlled' }}
                      />
                    )
              :
              ''
          )}

          <div className='meeting-form-actions'>
            <Button
              disabled={isButtonDisable === true}
              className='meeting-form-actions-butons'
              onClick={() => confirm()}
            >
              Confirm
            </Button>
            <Button className='meeting-form-actions-butons' onClick={() => closeDialog()}>
              Cancel
            </Button>
          </div>
        </div>
      </Dialog>

    </>
  );
};

export default TabularField;
