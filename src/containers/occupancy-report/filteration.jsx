import React, { useRef, useContext, useEffect, useState } from 'react';
import { Grid, TextField, Button } from '@material-ui/core';
import debounce from 'lodash.debounce';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import { LocalizationProvider, DateRangePicker,DatePicker } from '@material-ui/pickers-4.2';
import { Autocomplete } from '@material-ui/lab';
import moment from 'moment';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import './style.scss';



const Filteration = ({ setPage, getStudentsList, filterState, setFilterState }) => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [hRef, setHRef] = useState([]);
  const [loading, setLoading] = useState(false);
  const bearerToken = JSON.parse(localStorage.getItem('userDetails'));

  const occupancyFilterList = [
    {
      id:1,
      name:'Grade Wise Report'
    },
    {
      id:2,
      name:'Batch Wise Report'
    },
    {
      id:3,
      name:'Subject Wise Report'
    },
    {
      id:4,
      name:'Teacher Wise Report'
    },
  ]

  useEffect(() => {
    const searchValue = filterState?.search ? `&term=${filterState?.search}` : '';
    const selectedGrade = filterState?.selectedGrade?.map((grade) => grade?.grade_id)
      ?.length
      ? `&grades=${filterState?.selectedGrade?.map((grade) => grade?.grade_id)}`
      : '';
    const selectedSubject = filterState?.selectedSubject?.id
      ? `&subject_id=${filterState?.selectedSubject?.id}`
      : '';
    const start = filterState?.startDate ? `&start_date=${filterState?.startDate}` : '';
    const end = filterState?.endDate ? `&end_date=${filterState?.endDate}` : '';
    if (selectedGrade) {
      // setHRef(
        axiosInstance.get(`${endpoints.aolBatchAssign.downloadEnrollmentList}?is_download=True&export_type=csv${searchValue}${selectedGrade}${selectedSubject}${start}${end}`)
        // );
    }
  }, [filterState]);

  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200 || data?.status_code === 201) {
        if (type === 'grade') {
          setFilterState((prev) => ({ ...prev, gradeList: data?.data }));
        } else if (type === 'subject') {
          setFilterState((prev) => ({ ...prev, subjectList: data?.result }));
        }
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  useEffect(() => {
    ApiCall(`${endpoints.communication.subjectList}?branch_id=1`, 'subject');
    ApiCall(`${endpoints.communication.grades}?branch_id=1`, 'grade');
  }, []);

  const delayedQuery = useRef(
    debounce((q, state) => {
      setPage(1);
      getStudentsList(
        1,
        q,
        state?.selectedGrade?.map((grade) => grade?.grade_id) || [],
        state?.selectedSubject?.id || '',
        state?.startDate || '',
        state?.endDate || ''
      );
    }, 1000)
  ).current;

  const handleDownloadCsv = () => {
    const searchValue = filterState?.search ? `&term=${filterState?.search}` : '';
    const selectedGrade = filterState?.selectedGrade?.map((grade) => grade?.grade_id)
      ?.length
      ? `&grades=${filterState?.selectedGrade?.map((grade) => grade?.grade_id)}`
      : '';
    const selectedSubject = filterState?.selectedSubject?.id
      ? `&subject_id=${filterState?.selectedSubject?.id}`
      : '';
    const start = filterState?.startDate ? `&start_date=${filterState?.startDate}` : '';
    const end = filterState?.endDate ? `&end_date=${filterState?.endDate}` : '';
    if (selectedSubject && selectedGrade) {
      setHRef((`${endpoints.aolBatchAssign.downloadEnrollmentList}?is_download=True&export_type=csv${searchValue}${selectedGrade}${selectedSubject}${start}${end}`)

          );
    } else {
      setAlert('error', 'Please select Grade and Subject');
    }
  };

  // function handleDate(v1) {
  //   if (v1 && v1.length !== 0) {
  //     setStartDate(moment(new Date(v1[0])).format('YYYY-MM-DD'));
  //     setEndDate(moment(new Date(v1[1])).format('YYYY-MM-DD'));
  //   }
  //   setDateRangeTechPer(v1);
  // }

  return (
    <Grid container spacing={2}>
      <Grid item md={4} xs={12}>
        <Autocomplete
          size='small'
          id='grades'
          multiple
          className='dropdownIcon'
          options={occupancyFilterList || []}
          getOptionLabel={(option) => option?.name || ''}
          filterSelectedOptions
          value={filterState?.selectedGrade || ''}
          onChange={(event, value) => {
            setFilterState({ ...filterState, selectedGrade: value });
            setPage(1);
            getStudentsList(
              1,
              filterState?.search || '',
              value,
              filterState?.startDate || '',
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              size='small'
              variant='outlined'
              label='Occupancy Report Filter'
              placeholder='Occupancy Report Filter'
            />
          )}
        />
      </Grid>
      <Grid item md={4} xs={12}>
        <LocalizationProvider dateAdapter={MomentUtils}>
          <DatePicker
            startText='Select-date'
            value={filterState?.fullDate}
            onChange={(newValue) => {
              // handleDate(newValue);
              setFilterState((prev) => ({ ...prev, fullDate: newValue }));
              setFilterState((prev) => ({
                ...prev,
                startDate: moment(newValue?.[0])?.format()?.split('T')?.[0],
              }));
              setPage(1);
              getStudentsList(
                1,
                filterState?.search || '',
                filterState?.selectedGrade?.map((grade) => grade?.grade_id) || [],
                filterState?.selectedSubject?.id || '',
                moment(newValue?.[0])?.format()?.split('T')?.[0] || '',
                moment(newValue?.[1])?.format()?.split('T')?.[0] || ''
              );
            }}
            renderInput={({ inputProps, ...startProps }, endProps, params) => {
              console.log(inputProps, params, 'check');
              return (
                <>
                  <TextField
                    {...startProps}
                    inputProps={{
                      ...inputProps,
                      value: `${inputProps.value} - ${endProps.inputProps.value}`,
                      readOnly: true,
                    }}
                    InputLabelProps={{
                      color: '#014b7e',
                    }}
                    size='small'
                    style={{ minWidth: '100%' }}
                    helperText=''
                  />
                </>
              );
            }}
          />
        </LocalizationProvider>
      </Grid>
      <Grid item md={6} xs={12}>
        <CustomSearchBar
          value={filterState?.search}
          setValue={(value) => {
            setFilterState({ ...filterState, search: value });
            delayedQuery(value, filterState);
          }}
          onChange={(e) => {
            setFilterState({ ...filterState, search: e.target.value.trimLeft() });
            delayedQuery(e.target.value.trimLeft(), filterState);
          }}
          label=''
          placeholder='Occupancy Report Search'
        />
      </Grid>
      <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
        <Button
          style={{ borderRadius: '10px' }}
          size='small'
          fullWidth
          variant='contained'
          color='primary'
          onClick={() => {
            setPage(1);
            getStudentsList(1);
            setFilterState((prev) => ({
              ...prev,
              selectedGrade: [],
              selectedSubject: [],
              startDate: '',
              endDate: '',
              fullDate: [],
              search: '',
            }));
          }}
        >
          Clear Filter
        </Button>
      </Grid>
      <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
        <Button
          style={{ borderRadius: '10px' }}
          size='small'
          fullWidth
          variant='contained'
          color='primary'
          href={hRef}
          onClick={(e) => {
            handleDownloadCsv(e);
          }}
        >
          Download CSV
        </Button>
      </Grid>
      {loading && <Loader />}
    </Grid>
  );
};

export default Filteration;
