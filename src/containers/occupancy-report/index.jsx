import React, { useState, useContext, useEffect } from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  Grid,
  TableBody,
  Card,
  Divider,
  Button,
  TextField,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import moment from 'moment';
import { useHistory } from 'react-router-dom';
import Layout from '../Layout';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomFiterImage from '../../components/custom-filter-image';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Filteration from './filteration';
import Loader from '../../components/loader/loader';
import {
  DateTimeConverter,
  addCommas,
  ConverTime,
} from '../../components/dateTimeConverter';
import { Autocomplete } from '@material-ui/lab';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns'
import CustomSearchBar from '../../components/custom-seearch-bar';





const OccupancyReport = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [page, setPage] = useState(1);
  const [index, setIndex] = useState(1);
  const [selectedOccupancy, setSelectedOccupancy] = useState(null);
  const [selectedDate, handleDateChange] = useState(new Date());
  const [search, setSearch] = useState('');
  const [hRef, setHRef] = useState([]);
  const [labelName, setLabelName] = useState('name');
  const [list, setList] = useState([]);



  const occupancyFilterList = [
    {
      id: 1,
      name: 'Grade Wise Report'
    },
    {
      id: 2,
      name: 'Batch Wise Report'
    },
    {
      id: 3,
      name: 'Subject Wise Report'
    },
    {
      id: 4,
      name: 'Teacher Wise Report'
    },
  ]


  function handleClear() {
    setSelectedOccupancy(null);
    setList([]);
    handleDateChange(new Date());
  }

  const handleDownloadCsv = () => {
    let year = selectedDate.getFullYear();
    let month = selectedDate.getMonth() + 1;
    let day = selectedDate.getDate();
    let date = year + '-' + month + '-' + day;
    if (selectedOccupancy?.id === 1) {
      setHRef((`${endpoints.occupancyReportCsv.gradeOccupancyCsv}?is_download=True&export_type=csv&date=${date}`)
      );
    }
    else if (selectedOccupancy?.id === 2) {
      setHRef((`${endpoints.occupancyReportCsv.batchOccupancyCsv}?date=${date}`)
      );
    }
    else if (selectedOccupancy?.id === 3) {
      setHRef((`${endpoints.occupancyReportCsv.subjectOccupancyCsv}?date=${date}`)
      );
    }
    else if (selectedOccupancy?.id === 4) {
      setHRef((`${endpoints.occupancyReportCsv.teacherOccupancyCsv}?date=${date}`)
      );
    }
    else {
      setAlert('error', 'Please select Occupancy Filter');
    }
  };

  function getOccupancyList(id) {
    let year = selectedDate.getFullYear();
    let month = selectedDate.getMonth() + 1;
    let day = selectedDate.getDate();
    let date = year + '-' + month + '-' + day;
    console.log("date", date)
    if (id === 1) {
      setLabelName('Grade Name');
      axiosInstance.get(`${endpoints.occupancyReport.gradeOccupancyList}?date=${date}`).then((response) => {
        console.log('gradeOccupancyList:', response);
        if (response.data.status_code === 200) {
          setList(response.data.response);
          setAlert('success', response.data.message);
        } else {
          setAlert('error', response.data.message);
        }
      })
    } else if (id === 2) {
      setLabelName('Batch Name');
      axiosInstance.get(`${endpoints.occupancyReport.batchOccupancyList}?date=${date}`).then((response) => {
        console.log('batchOccupancyList:', response);
        if (response.data.status_code === 200) {
          setList(response.data.response);
          setAlert('success', response.data.message);
        } else {
          setAlert('error', response.data.message);
        }
      })
    } else if (id === 3) {
      setLabelName('Subject Name');
      axiosInstance.get(`${endpoints.occupancyReport.subjectOccupancyList}?date=${date}`).then((response) => {
        console.log('subjectOccupancyList:', response);
        if (response.data.status_code === 200) {
          setList(response.data.response);
          setAlert('success', response.data.message);
        } else {
          setAlert('error', response.data.message);
        }
      })
    } else if (id === 4) {
      setLabelName('Teacher Name');
      axiosInstance.get(`${endpoints.occupancyReport.teacherOccupancyList}?date=${date}`).then((response) => {
        console.log('teacherOccupancyList:', response);
        if (response.data.status_code === 200) {
          setList(response.data.response);
          setAlert('success', response.data.message);
        } else {
          setAlert('error', response.data.message);
        }
      })
    } else {
      setLabelName('Name');
      // setAlert('error', 'Please select Occupancy Filter');
    }
  }

  useEffect(() => {
    getOccupancyList(selectedOccupancy?.id);
  }, [selectedOccupancy?.id, selectedDate]);


  useEffect(() => {
    if (selectedOccupancy === null) {
      setList([]);
    }
  }, [selectedOccupancy]);



  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Occupancy Report'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={4} xs={12}>
              <Autocomplete
                size='small'
                id='grades'
                className='dropdownIcon'
                options={occupancyFilterList || []}
                getOptionLabel={(option) => option?.name || ''}
                value={selectedOccupancy}
                onChange={(event, newValue) => setSelectedOccupancy(newValue)}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    size='small'
                    variant='outlined'
                    label='Occupancy Report Filter'
                    placeholder='Occupancy Report Filter'
                  />
                )}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  placeholder="select date"
                  variant="inline"
                  inputVariant="outlined"
                  label="Select date"
                  size='small'
                  value={selectedDate}
                  onChange={date => handleDateChange(date)}
                  format="yyyy/MM/dd"
                />
              </MuiPickersUtilsProvider>
            </Grid>
            <Grid item md={6} xs={12}>
              <CustomSearchBar
                value={search}
                onChange={(e) => setSearch(e.target.value)}
                label=''
                placeholder='Occupancy Report Search'
              />
            </Grid>
            <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
              <Button
                style={{ borderRadius: '10px' }}
                size='small'
                fullWidth
                variant='contained'
                color='primary'
                onClick={handleClear}
              >
                Clear Filter
              </Button>
            </Grid>
            <Grid item md={2} xs={12} style={{ textAlign: 'left' }}>
              <Button
                style={{ borderRadius: '10px' }}
                size='small'
                fullWidth
                variant='contained'
                color='primary'
                href={hRef}
                onClick={(e) => {
                  handleDownloadCsv(e);
                }}
              >
                Download Excel
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <div>
        <Divider />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      {selectedOccupancy?.id === 2 ?
                        <>
                          <TableCell align='left'>S.No</TableCell>
                          <TableCell align='left'>Batch Size</TableCell>
                          <TableCell align='left'>Percentage</TableCell>
                        </>
                        :
                        <>
                          <TableCell align='left'>S.No</TableCell>
                          <TableCell align='left'>{labelName}</TableCell>
                          <TableCell align='left'>1:10</TableCell>
                          <TableCell align='left'>1:5</TableCell>
                        </>
                      }
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {list ? (
                      <>
                        {list.map((item, i) => (
                          <TableRow key={item.id}>
                            {selectedOccupancy?.id === 2 ?
                              <>
                                <TableCell align='left'>{i + index}</TableCell>
                                <TableCell align='left'>{item?.batch_size}</TableCell>
                                <TableCell align='left'>{item?.percentage}</TableCell>
                              </>
                              :
                              <>
                                <TableCell align='left'>{i + index}</TableCell>
                                {selectedOccupancy?.id === 1 ?
                                  <TableCell align='left'>{item?.grade || ''}</TableCell>
                                  : selectedOccupancy?.id === 2 ?
                                    <TableCell align='left'>{item?.batch || ''}</TableCell>
                                    : selectedOccupancy?.id === 3 ?
                                      <TableCell align='left'>{item?.subject || ''}</TableCell>
                                      : selectedOccupancy?.id === 4 ?
                                        <TableCell align='left'>{item?.teacher_name || ''}</TableCell>
                                        :
                                        ''
                                }
                                <TableCell align='left'>{item?.batch_size_5}</TableCell>
                                <TableCell align='left'>{item?.batch_size_10}</TableCell>
                              </>
                            }
                          </TableRow>
                        ))}
                        {/* <TableRow>
                          <TableCell colSpan='9'>
                            <Pagination
                              style={{ textAlign: 'center', display: 'inline-flex' }}
                              onChange={handlePagination}
                              count={studentBatchList?.total_pages}
                              color='primary'
                              page={page}
                            />
                          </TableCell>
                        </TableRow> */}
                      </>
                    ) : (
                      <TableRow>
                        <TableCell colSpan='9'>
                          <CustomFiterImage label='Reports Not Found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default OccupancyReport;
