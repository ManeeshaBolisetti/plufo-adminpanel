import React, { useContext } from 'react';
import { IconButton, Collapse, Typography, Box, makeStyles, Button, withStyles, Grid } from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import moment from 'moment';
import clsx from 'clsx';
import { useHistory, useLocation } from 'react-router-dom';
import MuiPaper from '@material-ui/core/Paper';
import StudentListModal from './StudentListModal';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import axiosInstance from '../../../config/axios';
import endpoints from '../../../config/endpoints';

const useStyles = makeStyles((theme) => ({
    card: {
        padding: '8px',
        border: '1px solid #F9D474',
        borderRadius: '10px',
        backgroundColor: '#FFFADF',
        cursor: 'pointer',
        minHeight: '200px',
    },
    activeCard: {
        padding: '8px',
        border: '1px solid #F9D474',
        borderRadius: '10px',
        backgroundColor: '#F9D474',
        height: 'auto',
        minHeight: '200px',
    },
    classTitle: {
        display: 'inline-block',
        color: '#001495',
        fontSize: '18px',
        fontFamily: 'Poppins',
        lineHeight: '27px',
    },
    classTime: {
        display: 'inline-block',
        color: '#001495',
        fontSize: '16px',
        fontFamily: 'Poppins',
        lineHeight: '25px',
        float: 'right',
    },
    classSchedule: {
        color: '#014B7E',
        fontSize: '16px',
        fontFamily: 'Poppins',
        lineHeight: '25px',
    },
    classScheduleCourseName : {
        color: '#001495',
        fontSize: '18px',
        fontFamily: 'Poppins',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        display: '-webkit-box',
        maxWidth: '100%',
        '-webkit-line-clamp': '1',
        '-webkit-box-orient': 'vertical',
        cursor: 'pointer',
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: 'none',
    },
    paper: {
        width: "80%",
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
}))

const StyledButton = withStyles({
    root: {
        height: '26px',
        width: '112px',
        fontSize: '15px',
        fontFamily: 'Open Sans',
        color: '#FFFFFF',
        backgroundColor: '#344ADE',
        borderRadius: '5px',
        float: 'right',
        display:'none'
    }
})(Button);

const StyledStudentListButton = withStyles({
    root: {
        height: '26px',
        fontSize: '15px',
        fontFamily: 'Open Sans',
        color: '#FFFFFF',
        backgroundColor: '#ff6b6b',
        borderRadius: '5px',
        letterSpacing: 0,
        //cursor: 'pointer'
    }
})(Button);

const StyledPaper = withStyles({
    root: {
      margin: '10px',
      padding: '10px',
      //border: '1px solid #E2E2E2',
      border: '1px solid #F9D474',
      borderRadius: '10px',
      boxShadow: '0px 0px 4px #00000029',
    },
  })(MuiPaper);

export default function ClassCardComponent(props) {
    const history = useHistory();
    const classes = useStyles({});
    const location = useLocation();
    const [enableEdit, setEnabelEdit] = React.useState(false)
    const classData = props.classData.zoom_meeting ? props.classData.zoom_meeting : props.classData;
    const [studentList, setStudentList] = React.useState([]);
    const [openStudentListModal, setOpenStudentListModal] = React.useState(false);
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const handleStudentList = () => {
        setOpenStudentListModal(true);
    }

    const handleOpen = () => { setEnabelEdit(true) }
    const handleClose = () => { setEnabelEdit(false) }
    const updateClasses = () => {
        if (props.updateClasses) {
            props.updateClasses()
        }
    }

    React.useEffect(() => {
        setStudentList([]);
        axiosInstance.get(`${endpoints.onlineCourses.studentList}?batch_id=${props.toggle ? classData?.id: classData?.online_class?.aol_batch_id}&is_aol=1`)
            .then((result) => {
                setStudentList(result?.data?.data);
            })
    }, [classData]);

    return (
        <StyledPaper style={{backgroundColor: props.selectedId === classData.id ? '#F9D474' : '#FFFADF'}}>
            {(props && props.toggle) ?
                ( <div>
                         <Typography className={classes.classSchedule}>
                            Course Name:{classData && classData?.course_name}
                        </Typography>
                        <Typography className={classes.classSchedule}>
                            Batch Name:{classData && classData.batch_name}
                        </Typography>
                        <Typography className={classes.classSchedule}>
                            Batch Size: {classData && `1 : ${classData.batch_size}`}
                        </Typography>
                        <Typography className={classes.classSchedule}>
                            Batch days: {classData && `${classData?.batch_days?.map((item)=>item)?.join(' , ')}`}
                        </Typography>
                        <Typography className={classes.classSchedule}>
                            No. Seat Left : {classData.seat_left ? classData.seat_left : 0}
                        </Typography>

                        {(classData.batch_size === 1 || classData.batch_size === 5) && studentList.length > 0 && (
                            <div style={{marginTop: '10px'}}>
                                <Typography className={classes.classTitle}>Student List</Typography>
                                <span style={{ float: 'right'}}>
                                    <IconButton
                                        className={clsx(classes.expand, {[classes.expandOpen]: expanded})}
                                        onClick={handleExpandClick}
                                        aria-expanded={expanded}
                                        aria-label="show more"
                                        size='small'
                                    >
                                        <ExpandMoreIcon style={{color: '#001495'}}/>
                                    </IconButton>
                                </span>
                            </div>
                        )}
                        <Collapse in={expanded} timeout="auto" unmountOnExit>
                            <div style={{ marginLeft: '10px'}}>
                                {studentList.length > 0 && studentList.map((student) => <Typography className={classes.classSchedule}>{student.first_name}</Typography>)}
                            </div>
                        </Collapse>

                        <div style={{ marginTop: '15px', width: '100%', height: '30px'}}>
                            {props.selectedId !== props.classData.id && (
                                <StyledButton
                                    variant="contained"
                                    color="secondary"
                                    onClick={(e) => props.handleSelctedClass(classData)}
                                >
                                    VIEW
                                </StyledButton>
                            )}
                        </div>
                </div>
                ) : (
                    <div>
                        <div>
                        <Typography className={classes.classScheduleCourseName} title={classData.online_class ? classData?.online_class?.cource_name : ''}>
                               Course Name: {classData.online_class ? classData?.online_class?.cource_name : ''}
                            </Typography>
                            <Typography className={classes.classTitle}>
                               Batch Name: {classData.online_class ? classData?.online_class?.title : ''}
                            </Typography>
                            {/* <IconButton
                                onClick={handleEditClass}
                                title='Edit Teacher'
                                style={{ float: 'right', verticalAlign: 'top', display: 'inline-block', padding: '7px' }}
                            >
                                <EditOutlinedIcon style={{ color: '#fe6b6b', fontSize: '22px' }} />
                            </IconButton>
                            {editClassJsx} */}

                        </div>
                        <div>
                            <Typography className={classes.classTitle}>
                                {classData.online_class ? `No. Seat Left : ${classData.online_class && classData.online_class.seat_left}` : ''}
                            </Typography>
                        </div>

                        <Typography className={classes.classSchedule}>
                            Start Date: {classData.online_class ? moment(classData.online_class.start_time).format('DD-MM-YYYY') : ''}
                        </Typography>

                        <Typography className={classes.classSchedule}>
                            End Date: {classData.online_class ? moment(classData.online_class.end_time).format('DD-MM-YYYY') : ''}
                        </Typography>
                        <Typography className={classes.classSchedule}>
                            Assigned To: {classData.online_class && classData.online_class.teacher.name}
                        </Typography>
                        {(props.batchSize === 1 || props.batchSize === 5) && studentList.length > 0 && (
                            <div style={{marginTop: '10px'}}>
                                <Typography className={classes.classTitle}>Student List</Typography>
                                <span style={{ float: 'right'}}>
                                    <IconButton
                                        className={clsx(classes.expand, {[classes.expandOpen]: expanded})}
                                        onClick={handleExpandClick}
                                        aria-expanded={expanded}
                                        aria-label="show more"
                                        size='small'
                                    >
                                        <ExpandMoreIcon style={{color: '#001495'}}/>
                                    </IconButton>
                                </span>
                            </div>
                        )}
                        <Collapse in={expanded} timeout="auto" unmountOnExit>
                            <div style={{ marginLeft: '10px'}}>
                                {studentList.length > 0 && studentList.map((student) => <Typography className={classes.classSchedule}>{student.first_name}</Typography>)}
                            </div>
                        </Collapse>

                        <div style={{ marginTop: '10px', width: '100%', height: '30px' }}>
                            {(props.batchSize > 5) && studentList.length > 0 && (
                                <StyledStudentListButton onClick={handleStudentList} color="primary">
                                    Students List
                                </StyledStudentListButton>
                            )}
                            {props.selectedId !== props.classData.id && (
                                <StyledButton
                                    variant="contained"
                                    color="secondary"
                                    onClick={(e) => props.handleSelctedClass(classData)}
                                >
                                    VIEW
                                </StyledButton>
                            )}
                        </div>
                    </div>
                )}
            <StudentListModal
                openStudentListModal={openStudentListModal}
                setOpenStudentListModal={setOpenStudentListModal}
                studentList={studentList}
            />
        </StyledPaper>
    )
}

export const ClassCard = React.memo(ClassCardComponent);