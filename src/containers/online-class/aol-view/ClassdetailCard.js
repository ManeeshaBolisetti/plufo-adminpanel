import React, { useState, useEffect } from 'react';
import { Divider, makeStyles, withStyles, Typography, Button } from '@material-ui/core';
import moment from 'moment';
import JoinClass from './JoinClass';
import { useHistory } from 'react-router-dom';
import axiosInstance from '../../../config/axios';
import endpoints from '../../../config/endpoints';
import AssignModal from './assign-modal';
import CloseIcon from '@material-ui/icons/Close';
import { Pointer } from 'highcharts';
import ReassignModal from './re-assign-modal';
import StudentListModal from './StudentListModal';

const useStyles = makeStyles({
    classDetailsBox: {
        backgroundColor: '#FFFFFF',
        border: '1px solid #F9D474',
        borderRadius: '10px',
        boxShadow: '0px 0px 4px #00000029',
    },
    classHeader: {
        minHeight: '64px',
        padding: '8px 8px 15px 15px',
        backgroundColor: '#F9D474',
        borderRadius: '10px 10px 0px 0px',
    },
    classHeaderText: {
        display: 'inline-block',
        color: '#014B7E',
        fontSize: '16px',
        fontWeight: 300,
        fontFamily: 'Poppins',
        lineHeight: '25px',
        marginTop: '5px',
    },
    classHeaderTime: {
        display: 'inline-block',
        color: '#014B7E',
        fontSize: '16px',
        fontFamily: 'Poppins',
        lineHeight: '25px',
        float: 'right',
        marginTop: '5px',
    },
    classHeaderSub: {
        display: 'inline-block',
        color: '#014B7E',
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'Poppins',
        lineHeight: '25px',
        width: '180px',
        overflowWrap: 'break-word',
    },
    subPeriods: {
        display: 'inline-block',
        color: '#014B7E',
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'Poppins',
        lineHeight: '25px',
        float: 'right',
    },
    classDetails: {
        padding: '8px 15px',
        backgroundColor: '#FFFFFF',
        borderRadius: '0px 0px 10px 10px',
    },
    classDetailsTitle: {
        marginTop: '10px',
        color: '#014B7E',
        fontSize: '16px',
        fontWeight: 'bold',
        fontFamily: 'Poppins',
        lineHeight: '25px',
    },
    classDetailsDivider: {
        color: '#014B7E',
        marginBottom: '10px',
    },
    joinClassDiv: {
        height: '300px',
        overflowY: 'scroll',
        '&::-webkit-scrollbar': {
            width: '3px',
        },
        /* Track */
        '&::-webkit-scrollbar-track': {
            background: '#f1f1f1',
        },
        
        /* Handle */
        '&::-webkit-scrollbar-thumb': {
            background: '#888', 
        },
        
        /* Handle on hover */
        '&::-webkit-scrollbar-thumb:hover': {
            background: '#555',
        },
    },
    studentIndex: {
        color: '#014B7E',
        fontSize: '16px',
        fontFamily: 'Poppins',
    },
    studentsList: {
        display: 'inline-block',
        fontSize: '16px',
        fontFamily: 'Poppins',
        marginLeft: '10px',
    },
    classDetailsDescription: {
        display: 'inline-block',
        height: '50px',
        color: '#014B7E',
        fontSize: '16px',
        fontFamily: 'Poppins',
        lineHeight: '25px',
        overflow: 'hidden',
    },
    closeDetailCard: {
        float: 'right',
        fontSize: '30px',
        color: '#014B7E',
    },
})

const StyledButton = withStyles({
    root: {
        marginTop: '10px',
        height: '31px',
        fontSize: '18px',
        fontFamily: 'Poppins',
        fontWeight: '',
        lineHeight: '27px',
        textTransform: 'capitalize',
        backgroundColor: '#FFAF71',
        borderRadius: '10px',
        '& :hover': {
        },
    }
})(Button);


const StyledAcceptButton = withStyles({
    root: {
        height: '30px',
        padding: '10px',
        fontSize: '18px',
        fontFamily: 'Open Sans',
        color: '#FFFFFF',
        backgroundColor: '#ff6b6b',
        borderRadius: '5px',
        letterSpacing: 0,
        cursor: 'pointer'
    }
})(Button);

export default function ClassdetailsCardComponent(props) {
    const classes = useStyles({});
    // <<<<<<<<<<<>>>>>>>>>>>>MODAL >>>>><<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    const [openAssignModal, setOpenAssignModal] = useState(false);
    const [teacherDropdown, setTeacherDropdown] = useState([])
    const [cancelFlag, setCancelFlag] = useState(false)
    // <<<<<<<<<<<<<<<<<<<<<<<Reassign Modal>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    const [openReassignModal,setOpenReassignModal] = useState(false);
    const [openStudentListModal, setOpenStudentListModal] = useState(false);
    const [ studentList, setStudentList] = useState([]);

    const assignData = props
    const [periodsData, setPeriodsData] = React.useState([]);
    //Periods date start
    const history = useHistory();


    // let isTeacher = props.classData && props.classData.hasOwnProperty('is_canceled');
    useEffect(() => {
        if (props.classData) {
            axiosInstance.get(`erp_user/${props.toggle ? props?.classData?.id : props?.classData?.online_class?.aol_batch_id}/online-class-details/`)
                .then((res) => {
                    setPeriodsData(res.data.data);
                })
        }
    }, [props.classData, cancelFlag]);

    const handleAttendance = () => {
        history.push(`/aol-attendance-list/${props.toggle ? props?.classData?.id : props?.classData?.online_class?.aol_batch_id}`);
    }


    const handleAssign = () => {
        setOpenAssignModal(true);
    }

    const handleReassign = () => {
        setOpenReassignModal(true);
    }

    const handleStudentList = () => {
        setOpenStudentListModal(true);
    }

    const handleReshuffle = () => {
        if (props.toggle) {
            history.push(`/aol-reshuffle/${props.toggle ? props?.classData?.id : props?.classData?.online_class?.aol_batch_id}`)
        } else {
            history.push(`/aol-reshuffle/${props?.classData?.online_class?.aol_batch_id}`)

        }
    }
    const handleCoursePlan = () => {
        history.push(`/create/course/${props.filterData && props?.filterData?.course?.id}/${props.filterData && props?.filterData?.grade?.id}`)
        sessionStorage.setItem('isAol',1);
    }

    useEffect(() => {
        // axiosInstance.get(`${endpoints.aol.teacherList}?branch_id=${props?.filterData?.branch?.id}&grade_id=${props?.filterData?.grade?.grade_id}&is_aol=1`)
        //     .then(result => {
        //         if (result.data.status_code === 200) {
        //             setTeacherDropdown(result?.data?.data)
        //         }
        //     })
        console.log(props && props?.batchWiseGradeId,props,'AAAAAAAAAAAAAAAAA')

            if(props.batchWiseGradeId){
                axiosInstance.get(`${endpoints.aol.teacherList}?branch_id=${props?.filterData?.branch?.id}&grade_id=${props?.batchWiseGradeId}&is_aol=1`)
                .then(result => {
                    if (result.data.status_code === 200) {
                        setTeacherDropdown(result?.data?.data)
                    }
                })
            }
    }, [props.batchWiseGradeId])

    useEffect(() => {
        setStudentList([]);
        axiosInstance.get(`${endpoints.onlineCourses.studentList}?batch_id=${props.toggle ? props?.classData?.id : props?.classData?.online_class?.aol_batch_id}&is_aol=1`)
            .then((result) => {
                setStudentList(result?.data?.data);
            })
    }, [props.classData]);

    return (
        <>
            <div className={classes.classDetailsBox}>
                <div className={classes.classHeader}>
                    <div>
                        <CloseIcon  onClick={(e) => props.hendleCloseDetails()} className={classes.closeDetailCard} />
                    </div>
                    {props.classData.online_class && (
                        <>

                            <div>
                                <Typography className={classes.classHeaderText}>
                                    {props.classData && props.classData.online_class && props.classData.online_class.title}
                                </Typography>
                                <Typography className={classes.classHeaderTime}>
                                    {props.classData && props.classData.online_class && moment(props.classData.online_class.start_time).format('h:mm:ss')}
                                </Typography>
                            </div>
                        </>
                    )}
                    <div>
                        {/* {props.classData.online_class && (
                            <Typography className={classes.classHeaderSub}>
                                {props.classData && props.classData.online_class.subject[0] && props.classData.online_class.subject[0].subject_name}
                            </Typography>
                        )} */}

                        <Typography className={classes.classHeaderSub}>
                            {props.toggle ? props.classData.batch_name : ''}
                        </Typography>
                        {props.toggle ?
                            <StyledAcceptButton onClick={handleAssign} color="primary">ASSIGN</StyledAcceptButton> :
                            <div>
                                <StyledAcceptButton onClick={handleReassign} color="primary">RE-ASSIGN</StyledAcceptButton>
                                {/* <StyledAcceptButton onClick={handleStudentList} style={{margin: '10px'}}>Students</StyledAcceptButton> */}
                            </div>
                        }
                    </div>
                </div>
                <div className={classes.classDetails}>
                    {props?.toggle ? '' :
                        <Typography className={classes.classDetailsTitle}>
                            Description
                        </Typography>
                    }

                    <Divider className={classes.classDetailsDivider} />
                    <div className={classes.joinClassDiv}>
                        {/* {props.toggle ? '' : periodsData.length > 0 && periodsData.map((data, id) => ( */}
                        {props.toggle ? (
                            <>
                                {studentList && studentList.length > 0 && studentList.map((student, id) => 
                                    <div>
                                        <span className={classes.studentIndex}>{id + 1}</span>
                                        <Typography className={classes.studentsList}>
                                            {student.first_name}
                                        </Typography>
                                    </div>
                                )}
                                {studentList.length === 0 && (
                                    <Typography>No students</Typography>
                                )}
                            </>
                        ) :(
                            <>
                                {
                                    periodsData.length > 0 && periodsData.map((data, id) => (
                                        <JoinClass
                                            // key={id}
                                            // data={props.classData}
                                            data={data}
                                            joinUrl={props.classData?.presenter_url}
                                            cancelFlag={cancelFlag}
                                            setCancelFlag={setCancelFlag}
                                        // isTeacher={isTeacher}
                                        />
                                    ))
                                }
                            </>
                        )}
                    </div>
                    <Divider className={classes.classDetailsDivider} />

                    {props.toggle === false ? (
                        <>
                            <StyledButton
                                onClick={handleAttendance}
                                color="primary"
                                fullWidth
                            >
                                Attendance
                            </StyledButton>
                            <StyledButton
                                onClick={handleReshuffle}
                                color="primary"
                                fullWidth
                            >
                                Reshuffle
                            </StyledButton>
                        </>
                    ) : (
                            <>
                                <StyledButton
                                    onClick={handleReshuffle}
                                    color="primary"
                                    fullWidth
                                >
                                    Reshuffle
                            </StyledButton>
                                {/* <StyledButton color="primary">Resources</StyledButton> */}
                            </>
                        )}
                    <StyledButton
                        color="primary"
                        onClick={handleCoursePlan}
                        fullWidth
                    >
                        View Course Plan
                    </StyledButton>
                </div>
            </div>
            <AssignModal
                openAssignModal={openAssignModal}
                setOpenAssignModal={setOpenAssignModal}
                teacherDropdown={teacherDropdown}
                assignData={assignData}
                reload={props.reload}
                setReload={props.setReload}
                hendleCloseDetails={props.hendleCloseDetails}
                batchDays={props?.classData?.batch_days}
            />
            <ReassignModal
                openReassignModal={openReassignModal}
                setOpenReassignModal={setOpenReassignModal}
                teacherDropdown={teacherDropdown}
                selectedTeacher={props.toggle ? props?.classData?.teacher : props?.classData?.online_class?.teacher}
                allData={props?.classData}
                getClasses={props.getClasses}
            />
            {/* <StudentListModal
                openStudentListModal={openStudentListModal}
                setOpenStudentListModal={setOpenStudentListModal}
                studentList={studentList}
            /> */}
        </>
    )
}

export const ClassdetailsCard = React.memo(ClassdetailsCardComponent);
