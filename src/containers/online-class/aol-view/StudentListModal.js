import React, { useEffect, useState, useContext } from 'react';
import Dialog from '@material-ui/core/Dialog';
import { makeStyles, useTheme } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state'
import axiosInstance from '../../../config/axios'
import endpoints from '../../../config/endpoints';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import './style.css'

const useStyles = makeStyles(theme => ({
    dialogWrapper: {
        padding: theme.spacing(2),
        position: 'absolute',
        top: theme.spacing(5),
        width: '95%'
    },
    dialogTitle: {
        paddingRight: '0px'
    },
    table: {
        minWidth: 650,
    },
}))

const StudentListModal = ({ openStudentListModal, setOpenStudentListModal, studentList}) => {
    const classes = useStyles({});
    //const [ studentList, setStudentList] = useState([]);

    // useEffect(() => {
    //     setStudentList([]);
    //     axiosInstance.get(`${endpoints.onlineCourses.studentList}?batch_id=${props?.classData?.id}&is_aol=1`)
    //         .then((result) => {
    //             setStudentList(result.data.data);
    //         })
    // }, [props.classData]);

    return (
        <Dialog open={openStudentListModal} className='reAssignModal' onClose={() => setOpenStudentListModal(false)} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title" className='reshuffle-header'>Student List</DialogTitle>
            <DialogContent>
                <DialogContentText style={{ marginTop: '1.25rem' }} className='reAssignDisplay'>
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>Sr. No</TableCell>
                                    <TableCell align="right">ERP Id</TableCell>
                                    <TableCell align="right">Student Name</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                            {studentList && studentList.length > 0 && studentList.map((student, id) => (
                                <TableRow key={id}>
                                    <TableCell component="th" scope="row">
                                        {id + 1}
                                    </TableCell>
                                    <TableCell align="right">{student.username}</TableCell>
                                    <TableCell align="right">{student.first_name}</TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>`
                </DialogContentText>
            </DialogContent>
            <DialogActions>
            </DialogActions>
        </Dialog>

    )
}

export default StudentListModal;




