import React, { useState, useEffect, useContext, useRef } from 'react';
import ClassCard from './ClassCard';
import debounce from 'lodash.debounce';
import SearchIcon from '@material-ui/icons/Search';
import {
  Divider,
  Grid,
  makeStyles,
  useTheme,
  withStyles,
  Button,
  TextField,
  Switch,
  FormControlLabel,
  Typography,
  InputAdornment,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import useMediaQuery from '@material-ui/core/useMediaQuery';
// import Pagination from './Pagination';
import { Pagination } from '@material-ui/lab';
// import MomentUtils from '@date-io/moment';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import {
  LocalizationProvider,
  DateRangePicker,
  KeyboardDate,
} from '@material-ui/pickers-4.2';
import moment from 'moment';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import CommonBreadcrumbs from '../../../components/common-breadcrumbs/breadcrumbs';
import Loader from '../../../components/loader/loader';
import { useLocation } from 'react-router-dom';
import axiosInstance from '../../../config/axios';
import endpoints from '../../../config/endpoints';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import filterImage from '../../../assets/images/unfiltered.svg';
import './style.css';
import { gridColumnLookupSelector } from '@material-ui/data-grid';
import ClassdetailsCard from './ClassdetailCard';
import displayName from '../../../config/displayName';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: '55px auto 20px auto',
    width: '90%',
    border: '1px solid #D8D8D8',
    borderRadius: '5px',
    [theme.breakpoints.down('xs')]: {
      margin: '55px auto 20px auto',
    },
    [theme.breakpoints.down('sm')]: {
      margin: '55px auto 20px auto',
    },
  },
  topFilter: {
    width: '90%',
    margin: '30px auto 0px auto',
    [theme.breakpoints.down('xs')]: {
      margin: '55px auto 20px auto',
    },
  },
  classDetailsBox: {
    backgroundColor: '#FFFFFF',
    border: '1px solid #F9D474',
    borderRadius: '10px',
  },
  classHeader: {
    padding: '8px 21px',
    backgroundColor: '#F9D474',
    borderRadius: '10px 10px 0px 0px',
  },
  classHeaderText: {
    display: 'inline-block',
    color: '#014B7E',
    fontSize: '16px',
    fontWeight: 300,
    fontFamily: 'Poppins',
    lineHeight: '25px',
  },
  classHeaderTime: {
    display: 'inline-block',
    color: '#014B7E',
    fontSize: '16px',
    fontFamily: 'Poppins',
    lineHeight: '25px',
    float: 'right',
  },
  classHeaderSub: {
    display: 'inline-block',
    color: '#014B7E',
    fontSize: '16px',
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    lineHeight: '25px',
  },
  subPeriods: {
    display: 'inline-block',
    color: '#014B7E',
    fontSize: '16px',
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    lineHeight: '25px',
    float: 'right',
  },
  classDetails: {
    padding: '8px 21px',
    backgroundColor: '#FFFFFF',
    borderRadius: '0px 0px 10px 10px',
  },
  classDetailsTitle: {
    color: '#014B7E',
    fontSize: '16px',
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    lineHeight: '25px',
  },
  classDetailsDivider: {
    color: '#014B7E',
  },
  classDetailsDescription: {
    height: '50px',
    color: '#014B7E',
    fontSize: '16px',
    fontFamily: 'Poppins',
    lineHeight: '25px',
    overflow: 'hidden',
    marginBottom: '12px',
  },
  cardHover: {
    border: '1px solid #004087',
    borderRadius: '5px',
  },
}));

const StyledButton = withStyles({
  root: {
    marginTop: '25px',
    height: '31px',
    width: '100%',
    fontSize: '18px',
    fontFamily: 'Poppins',
    textTransform: 'capitalize',
    backgroundColor: '#FFAF71',
    borderRadius: '10px',
  },
})(Button);

const UpcomingClasses = () => {
  const { setAlert } = useContext(AlertNotificationContext);
  const location = useLocation();
  const classes = useStyles({});
  const [classesData, setClassesdata] = React.useState([]);
  const [classData, setClassData] = React.useState();
  const [apiCall, setApiCall] = React.useState(false);
  const [itemSize, setItemSize] = React.useState(4);
  const [size, setSize] = React.useState(12);
  const [selected, setSelected] = React.useState();
  const [searchValue, setSearchValue] = useState('');
  const [classTypeList, setClassTypeList] = React.useState([
    { id: 0, type: 'Compulsory Class' },
    { id: 1, type: 'Special Class' },
    { id: 2, type: 'Parent Class' },
    { id: 3, type: 'Optional Class' },
  ]);

  const [classType, setClassType] = React.useState('');
  const [startDate, setStartDate] = React.useState(null);
  const [endDate, setEndDate] = React.useState(null);
  const [isLoding, setIsLoding] = useState(false);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [gradeDropdown, setGradeDropdown] = useState([]);
  const [courseDropdown, setCourseDropdown] = useState([]);
  const [batch, setBatch] = useState([]);
  const [toggle, setToggle] = useState(true);
  const [toggledData, setToggledData] = useState([]);
  const NavData = JSON.parse(localStorage.getItem('navigationData')) || {};
  const [moduleId, setModuleId] = useState();

  const [reload, setReload] = useState(false);
  const limit = 15;
  const limits = 12;

  const [dateRangeTechPer, setDateRangeTechPer] = useState([
    moment().subtract(6, 'days'),
    moment(),
  ]);
  const [startDateTechPer, setStartDateTechPer] = useState(moment().format('YYYY-MM-DD'));
  const [endDateTechPer, setEndDateTechPer] = useState(getDaysAfter(moment(), 7));

  const themeContext = useTheme();
  const isTabDivice = useMediaQuery(themeContext.breakpoints.down('sm'));

  // batches view <<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  const branchDrop = [{ id: 1, branch_name: displayName ? displayName : '' }];
  const [batchWiseGradeId, setBatchWiseGradeId] = useState('');

  const delayedQuery = useRef(debounce((q, filInfo, dot, mId, tog) => handleFilterData(q, filInfo, dot, mId, tog), 1000)).current;
  function handleSearch(e) {
    setSearchValue(e.target.value);
    delayedQuery(e.target.value, filterData, dateRangeTechPer, moduleId, toggle);
  }

  function handleFilterData(searchVal, filInfo, dot, mId, tog) {
    getClasses(searchVal, filInfo, dot, mId, tog);
  }

  useEffect(() => {
    if (NavData && NavData.length) {
      NavData.forEach((item) => {
        if (
          item.parent_modules === 'Online Class' &&
          item.child_module &&
          item.child_module.length > 0
        ) {
          item.child_module.forEach((item) => {
            if (item.child_name === 'View Class') {
              setModuleId(item.child_id);
            }
          });
        }
      });
    }
  }, []);

  // Filter data for batchev view<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  const [filterData, setFilterData] = useState({
    branch: '',
    grade: '',
    course: '',
    batch: '',
  });

  function getDaysAfter(date, amount) {
    return date ? date.add(amount, 'days').format('YYYY-MM-DD') : undefined;
  }
  function getDaysBefore(date, amount) {
    return date ? date.subtract(amount, 'days').format('YYYY-MM-DD') : undefined;
  }
  const handlePagination = (event, page) => {
    setPage(page);
  };

  const handleBranch = (event, value) => {
    setFilterData({ ...filterData, branch: '' });
    if (value) {
      setIsLoding(true);
      setFilterData({ ...filterData, branch: value });
      axiosInstance
        .get(
          `${endpoints.communication.grades}?branch_id=${value.id}&module_id=${moduleId}`
        )
        .then((result) => {
          setIsLoding(false);
          if (result.data.status_code === 200) {
            setGradeDropdown(result.data.data);
          } else {
            setGradeDropdown([]);
          }
        }).catch((error) => {
          setIsLoding(false);
          console.log(error);
        });
    }
  };

  const handleGrade = (event, value) => {
    setFilterData({ ...filterData, garde: '' });
    if (value) {
      setIsLoding(true);
      setFilterData({ ...filterData, grade: value });
      axiosInstance
        .get(`${endpoints.aol.courseList}?grade=${value.grade_id}&module_id=${moduleId}`)
        .then((result) => {
          setIsLoding(false);
          if (result.data.status_code === 200) {
            setCourseDropdown(result.data.result);
          } else {
            setCourseDropdown([]);
          }
        }).catch((error) => {
          setIsLoding(false);
          console.log(error);
        });
    }
  };
  const handleCourse = (event, value) => {
    setFilterData({ ...filterData, course: '' });
    setBatch([]);
    if (value) {
      setIsLoding(true);
      setFilterData({ ...filterData, course: value });
      axiosInstance
        .get(`${endpoints.aol.batchLimitList}?course_id=${value.id}`)
        .then((result) => {
          setIsLoding(false);
          if (result.data.status_code === 200) {
            setBatch(result.data.result);
          } else {
            setBatch([]);
          }
        })
        .catch((error) => {
          console.log(error, error.description);
          setIsLoding(false);
        });
    }
  };

  const handleBatch = (event, value) => {
    setFilterData({ ...filterData, batch: '' });
    if (value) {
      setFilterData({ ...filterData, batch: value });
    }
  };

  // api call
  const getClasses = (searchVal = searchValue, filInfo = filterData, dot = dateRangeTechPer, mId = moduleId, tog = toggle) => {
    setPage(1);
    const [startDateTechPer, endDateTechPer] = dot;
    if (tog) {
      setIsLoding(true);
      const courseId = filInfo.course.id ? `&course_id=${filInfo.course.id}` : '';
      const gradeId = filInfo.grade.id ? `&grade_id=${filInfo.grade.grade_id}` : '';
      const batchSize = filInfo.batch.batch_size ? `&batch_size=${filInfo.batch.batch_size}` : '';
      const module_Id = mId ? `&module_id=${mId}` : '';
      const startDate = startDateTechPer ? `&start_date=${startDateTechPer.format('YYYY-MM-DD')}` : '';
      const endDate = endDateTechPer ? `&end_date=${endDateTechPer.format('YYYY-MM-DD')}` : '';
      axiosInstance
        .get(`${endpoints.aol.draftBatch}?page_number=${1}${courseId}${module_Id}${batchSize}${gradeId}${endDate}${startDate}&is_aol=1&page_size=${limit}${searchVal ? `&term=${searchVal}` : ''}`)
        .then((result) => {
          setIsLoding(false);
          setTotalCount(result?.data?.total_pages);
          setToggledData(result.data.result);
          setClassesdata([]);
        }).catch((error) => {
          setIsLoding(false);
          console.log(error);
        });
    } else {
      if (!filInfo.grade && !searchVal) {
        setAlert('warning', 'Select Grade');
        return;
      }
      if (!filInfo.course && !searchVal) {
        setAlert('warning', 'Select Course');
        return;
      }
      if (!filInfo.batch && !searchVal) {
        setAlert('warning', 'Select Batch Limit');
        return;
      }
      if (!startDateTechPer && !searchVal) {
        setAlert('warning', 'Select Start Date');
        return;
      }
      const courseId = filInfo.course.id ? `&course_id=${filInfo.course.id}` : '';
      const gradeId = filInfo.grade.id ? `&grade_id=${filInfo.grade.grade_id}` : '';
      const batchSize = filInfo.batch.batch_size ? `&batch_limit=${filInfo.batch.batch_size}` : '';
      const module_Id = mId ? `&module_id=${mId}` : '';
      const startDate = startDateTechPer ? `&start_date=${startDateTechPer.format('YYYY-MM-DD')}` : '';
      const endDate = endDateTechPer ? `&end_date=${endDateTechPer.format('YYYY-MM-DD')}` : '';
      setIsLoding(true);
      axiosInstance
        .get(
          `${endpoints.aol.classes
          }?page_number=${1}&page_size=${limit}&class_type=1&is_aol=1${module_Id}${courseId}${gradeId}${batchSize}${startDate}${endDate}${searchVal ? `&term=${searchVal}` : ''}`
        )
        .then((result) => {
          setIsLoding(false);
          setTotalCount(result?.data?.total_pages);
          setClassesdata(result.data.data);
          setToggledData([]);
        }).catch((error) => {
          setIsLoding(false);
          console.log(error);
        });
    }
  };

  const handleSelctedClass = (data) => {
    console.log(data, '+++++++');
    setItemSize(6);
    setSize(8);
    setClassData(data);
    if (!toggle) {
      // setToggledData([]);
    }
    // setToggledData(data);
    setSelected(data.id);
    if (isTabDivice) {
      // console.log('**** TAb *****');
    }
    setIsLoding(true);
    axiosInstance
      .get(`${endpoints.aol.batchWiseGrade}?id=${toggle ? data?.id : data?.online_class?.aol_batch_id}`)
      .then((result) => {
        setIsLoding(false);
        setBatchWiseGradeId(result?.data?.result[0].id);
      })
      .catch((error) => {
        console.log(error);
        setIsLoding(false);
      });
  };

  useEffect(() => {
    const [startDateTechPer, endDateTechPer] = dateRangeTechPer;
    if ((!filterData.grade?.grade_id || filterData.grade === '') && toggle) {
      const courseId = filterData.course.id ? `&course_id=${filterData.course.id}` : '';
      const gradeId = filterData.grade.id ? `&grade_id=${filterData.grade.grade_id}` : '';
      const batchSize = filterData.batch.batch_size ? `&batch_size=${filterData.batch.batch_size}` : '';
      const module_Id = moduleId ? `&module_id=${moduleId}` : '';
      const startDate = startDateTechPer ? `&start_date=${startDateTechPer.format('YYYY-MM-DD')}` : '';
      const endDate = endDateTechPer ? `&end_date=${endDateTechPer.format('YYYY-MM-DD')}` : '';
      setItemSize(4);
      setSize(12);
      setIsLoding(true);
      axiosInstance
        .get(`${endpoints.aol.draftBatch}?page=${page}${courseId}${gradeId}${batchSize}${module_Id}${startDate}${endDate}&page_size=12&is_aol=1${searchValue ? `&term=${searchValue}` : ''}`)
        .then((result) => {
          setToggledData(result.data.result);
          setTotalCount(result?.data?.total_pages);
          setClassesdata([]);
          setIsLoding(false);
        })
        .catch((error) => {
          console.log(error);
          setToggledData([]);
          setIsLoding(false);
        });
    } else if (!toggle && filterData.grade && filterData.course && filterData.batch && dateRangeTechPer) {
      setIsLoding(true);
      setToggledData([]);
      setItemSize(4);
      setSize(12);
      const courseId = filterData.course.id ? `&course_id=${filterData.course.id}` : '';
      const gradeId = filterData.grade.id ? `&grade_id=${filterData.grade.grade_id}` : '';
      const batchSize = filterData.batch.batch_size ? `&batch_limit=${filterData.batch.batch_size}` : '';
      const module_Id = moduleId ? `&module_id=${moduleId}` : '';
      const startDate = startDateTechPer ? `&start_date=${startDateTechPer.format('YYYY-MM-DD')}` : '';
      const endDate = endDateTechPer ? `&end_date=${endDateTechPer.format('YYYY-MM-DD')}` : '';
      axiosInstance
        .get(
          `${endpoints.aol.classes}?page_number=${page}${courseId}${gradeId}${batchSize}${module_Id}${startDate}${endDate}&page_size=${limit}&class_type=1&is_aol=1${searchValue ? `&term=${searchValue}` : ''}`
        )
        .then((result) => {
          setIsLoding(false);
          setTotalCount(result?.data?.total_pages);
          setClassesdata(result.data.data);
          setToggledData([]);
        })
        .catch((error) => {
          console.log(error);
          setIsLoding(false);
        });
    }
  }, [page, toggle]);

  const handleFilter = () => {
    getClasses();
    hendleCloseDetails();
  };

  const clearAll = () => {
    setStartDate([]);
    setEndDate([]);
    setClassType([]);
    setFilterData([]);
    setClassesdata([]);
    setToggledData([]);
    setClassData('');
    setItemSize(4);
    setSize(12);
    setSearchValue('');
  };

  const handleToggle = () => {
    setToggle(!toggle);
    setClassesdata([]);
    setToggledData([]);
    if (!toggle) {
      setToggledData([]);
    }
    setClassData(null);
    setSelected();
  };

  const hendleCloseDetails = () => {
    setItemSize(4);
    setSize(12);
    setSelected(0);
    setClassData('');
    // setFilter(false);
  };

  if (reload) {
    getClasses();
    setReload(!reload);
  }

  return (
    <>
      <div className='breadcrumb-container-create' style={{ marginLeft: '15px' }}>
        <CommonBreadcrumbs componentName='Online Class' childComponentName='View Class' />
      </div>
      <Grid container spacing={2} className={classes.topFilter}>
        <Grid item xs={12} sm={4}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={handleBranch}
            id='grade'
            className='dropdownIcon'
            value={filterData?.branch}
            options={branchDrop}
            getOptionLabel={(option) => option?.branch_name}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Branch'
                placeholder='Branch'
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={handleGrade}
            id='volume'
            className='dropdownIcon'
            value={filterData?.grade}
            options={gradeDropdown}
            getOptionLabel={(option) => option?.grade__grade_name}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Grade'
                placeholder='Grade'
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={handleCourse}
            id='volume'
            className='dropdownIcon'
            value={filterData?.course}
            options={courseDropdown}
            getOptionLabel={(option) => option?.course_name}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Course'
                placeholder='Course'
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Autocomplete
            style={{ width: '100%' }}
            size='small'
            onChange={handleBatch}
            id='batch'
            className='dropdownIcon'
            value={filterData?.batch}
            options={batch}
            getOptionLabel={(option) =>
              option ? `1 : ${JSON.stringify(option.batch_size)}` : ''}
            filterSelectedOptions
            renderInput={(params) => (
              <TextField
                {...params}
                variant='outlined'
                label='Batch Limit'
                placeholder='Batch Limit'
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <LocalizationProvider dateAdapter={MomentUtils}>
            <DateRangePicker
              startText='Select-date-range'
              value={dateRangeTechPer}
              onChange={(newValue) => {
                setDateRangeTechPer(newValue);
              }}
              renderInput={({ inputProps, ...startProps }, endProps) => {
                return (
                  <>
                    <TextField
                      {...startProps}
                      inputProps={{
                        ...inputProps,
                        value: `${inputProps.value} - ${endProps.inputProps.value}`,
                        readOnly: true,
                      }}
                      size='small'
                      style={{ minWidth: '100%' }}
                    />
                  </>
                );
              }}
            />
          </LocalizationProvider>
        </Grid>
        <Grid item xs={12} sm={4}>
          <FormControlLabel
            className='switchLabel'
            control={(
              <Switch
                checked={toggle}
                onChange={handleToggle}
                name='optional'
                color='primary'
              />
            )}
            label={toggle ? 'Yet To Start' : 'Started'}
          />
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={3}>
            <Grid item md={2} xs={12}>
              <StyledButton variant='contained' color='primary' onClick={clearAll}>
                Clear All
              </StyledButton>
            </Grid>
            <Grid item md={2} xs={12}>
              <StyledButton variant='contained' color='primary' onClick={handleFilter}>
                Get Classes
              </StyledButton>
            </Grid>
            <Grid item md={8} xs={12}>
              <TextField
                style={{ borderRadius: '10px' }}
                variant='standard'
                color='primary'
                label="Search User"
                margin="dense"
                fullWidth
                onChange={handleSearch}
                value={searchValue}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <SearchIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Divider />
      <Grid container spacing={3} className={classes.root}>
        <Grid item sm={size} xs={12}>
          <Grid container spacing={3}>
            {classesData.length === 0 && toggledData.length === 0 && (
              <Grid item xs={12} style={{ textAlign: 'center', marginTop: '10px' }}>
                <img src={filterImage} alt='crash' height='250px' width='250px' />
                <Typography>Please select the filter to dislpay classes</Typography>
              </Grid>
            )}
            {classesData.length > 0 &&
              classesData.map((data, id) => {
                return (
                  <Grid item sm={itemSize} xs={12} key={id}>
                    <ClassCard
                      classData={data}
                      selectedId={selected}
                      handleSelctedClass={handleSelctedClass}
                      toggle={toggle}
                      batchSize={filterData.batch.batch_size}
                    />
                  </Grid>
                );
              })}
            {toggledData.length > 0 &&
              toggledData.map((data, id) => {
                return (
                  <Grid item sm={itemSize} xs={12} key={id}>
                    <ClassCard
                      classData={data}
                      selectedId={selected}
                      handleSelctedClass={handleSelctedClass}
                      toggle={toggle}
                    />
                  </Grid>
                );
              })}
          </Grid>
        </Grid>

        {classData && (
          <Grid
            item
            sm={4}
            xs={12}
            className='viewMoreContainer'
            style={{ padding: '20px' }}
          >
            <ClassdetailsCard
              classData={classData}
              filterData={filterData}
              toggle={toggle}
              reload={reload}
              setReload={setReload}
              hendleCloseDetails={hendleCloseDetails}
              getClasses={getClasses}
              batchWiseGradeId={batchWiseGradeId}
            />
          </Grid>
        )}
        {/* toggledData.length > 0 && (
                    <Grid item sm={3} xs={12}>
                        <ClassdetailsCard
                            toggledData={toggledData}
                            filterData={filterData}
                            toggle={toggle}
                            
                        />
                    </Grid>
                ) */}
      </Grid>
      {classesData?.length > 0 && (
        <div style={{ justifyContent: 'center', display: 'flex' }}>
          <Pagination
            onChange={handlePagination}
            style={{ marginTop: 25 }}
            count={totalCount}
            color='primary'
            page={page}
          />
        </div>
      )}
      {toggledData?.length > 0 && (
        <div style={{ justifyContent: 'center', display: 'flex' }}>
          <Pagination
            onChange={handlePagination}
            style={{ marginTop: 25 }}
            count={totalCount}
            color='primary'
            page={page}
          />
        </div>
      )}
      {isLoding && <Loader />}
    </>
  );
};

export default UpcomingClasses;
