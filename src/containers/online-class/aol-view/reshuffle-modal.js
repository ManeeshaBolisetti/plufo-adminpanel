/* eslint-disable react-hooks/exhaustive-deps */
import Dialog from '@material-ui/core/Dialog';
import { Grid, makeStyles, Button, TextField, Typography, Divider } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import { KeyboardTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import React, { useEffect, useState, useContext } from 'react';
import axiosInstance from '../../../config/axios';
import endpoints from '../../../config/endpoints';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import Loader from '../../../components/loader/loader';
import './style.css';

const useStyles = makeStyles((theme) => ({
  dialogWrapper: {
    padding: theme.spacing(2),
    position: 'absolute',
    top: theme.spacing(5),
    bottom: '10px',
  },
  dialogTitle: {
    paddingRight: '0px',
  },
  closeButton: {
    position: 'absolute',
    right: '24px',
    top: '24px',
    color: 'white',
  },
}));

const ReshuffleModal = ({
  openReshuffleModal,
  setOpenReshuffleModal,
  studentName,
  modalData,
  id,
  reshuffleFlag,
  setReshuffleFlag,
}) => {
  const classes = useStyles();
  const { setAlert } = useContext(AlertNotificationContext);
  const [batchList, setBatchList] = useState([]);
  const [dayList, setDayList] = useState([]);
  const [timeList, setTimeList] = useState([]);
  const [weekList] = useState([1, 2, 3]);
  const [loading, setLoading] = useState(false);

  const [filterData, setFilterData] = useState({
    selectedDay: '',
    selectedTime: '',
    batch: '',
    selectedWeek: '',
  });

  const handleBatch = (event, value) => {
    setFilterData({ ...filterData, batch: value });
  };

  const getApiData = (api, key) => {
    setLoading(true);
    axiosInstance
      .get(api)
      .then((result) => {
        setLoading(false);
        if (key === 'batchList') {
          setBatchList(result?.data?.result || []);
        }
        if (key === 'dayList') {
          setDayList(result.data.result);
        }
        if (key === 'timeList') {
          setTimeList(result.data.result?.[0]?.time_slot || []);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error?.response?.data?.message);
      });
  };

  useEffect(() => {
    setBatchList([]);
    getApiData(`${endpoints.aol.getBatchDaysList}?batch_id=${id}`, 'dayList');
    if(modalData?.is_started === false) {
      getApiData(`${endpoints.aol.reshuffleBatchList}?batch_id=${id}`, 'batchList');
    }
    setFilterData({
      selectedDay: '',
      selectedTime: '',
      batch: '',
      selectedWeek: '',
    })
  }, [modalData]);

  const handleReshuffle = () => {
    if (!filterData.batch) {
      return setAlert('warning', 'Select Batch');
    }
    setLoading(true);
    axiosInstance
      .post(`${endpoints.aol.studentReshuffle}`, {
        batch: parseInt(id, 10),
        new_batch: filterData?.batch?.id,
        students: [modalData?.user_id],
        aol: '1',
      })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setAlert('success', result.data.message);
          setOpenReshuffleModal(false);
          setReshuffleFlag(!reshuffleFlag);
        } else {
          setAlert('warning', result.data.error_msg);
        }
      }).catch((err) => {
        setLoading(false);
        setAlert('error', err?.response?.data?.error_msg);
      })
  };

  const handleCreateBatch = () => {
    setLoading(true);
    axiosInstance
      .post(`${endpoints.aol.createNewReShuffleBatch}`, {
        batch: parseInt(id, 10),
        days: filterData?.selectedDay,
        time_slot: filterData?.selectedTime,
        students: [modalData?.user_id],
        aol: '1',
      })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setAlert('success', result.data.message);
          setOpenReshuffleModal(false);
          setReshuffleFlag(!reshuffleFlag);
        } else {
          setAlert('warning', result.data.error_msg);
        }
      }).catch((error)=> {
        setLoading(false);
        setAlert('error', error?.response?.data?.error_msg);
      })
  };

  const DialogTitle = (props) => {
    const { children, onClose, ...other } = props;
    return (
      <MuiDialogTitle
        disableTypography
        className={classes.root}
        {...other}
        style={{
          padding: '16px 24px',
          background: '#ff6b6b',
          color: 'white',
          borderRadius: '5px',
        }}
      >
        <Typography variant='h6'>{children}</Typography>
        {onClose ? (
          <IconButton
            aria-label='close'
            className={classes.closeButton}
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  };

  const labelData = (name, data) => {
    return(
      <>
      <Grid item md={4} xs={6}>
        <Typography>{name}</Typography>
      </Grid>
      <Grid item md={8} sm={6}>
        <Typography>:&nbsp; {data}</Typography>
      </Grid>
    </>
    )
  }

  function formatDate(date) {
    const d = new Date(date);
    let month = `${d.getMonth() + 1}`;
    let day = `${d.getDate()}`;
    const year = d.getFullYear();
    if (month.length < 2) month = `0${month}`;
    if (day.length < 2) day = `0${day}`;
    return [year, month, day].join('-');
  }

  const converTime = (info) => {
    let hour = (info.split(':'))[0];
    let min = (info.split(':'))[1];
    const part = hour >= 12 ? 'PM' : 'AM';
    min = (`${min}`).length === 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = (`${hour}`).length === 1 ? `0${hour}` : hour;
    return (`${hour}:${min} ${part}`);
  };


  return (
    <div>
      <Dialog
        open={openReshuffleModal}
        aria-labelledby='form-dialog-title'
        classes={{ paper: classes.dialogWrapper }}
        maxWidth='md'
      >
        <DialogTitle id='form-dialog-title' onClose={() => setOpenReshuffleModal(false)}>
          Batch Reshuffle
        </DialogTitle>
        <DialogContent>
          <DialogContentText style={{ marginTop: '1.25rem' }}>
            <Grid container spacing={4}>
              <Grid item md={6} xs={12} sm={12}>
                <TextField
                  style={{ width: '100%' }}
                  id='standard-read-only-input'
                  label='Course Name'
                  defaultValue={modalData?.course_name}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </Grid>
              <Grid item md={6} xs={12} sm={12}>
                <TextField
                  style={{ width: '100%' }}
                  id='standard-read-only-input'
                  label='Batch Name'
                  defaultValue={modalData?.title}
                  InputProps={{
                    readOnly: true,
                  }}
                />
              </Grid>
            </Grid>
            <Grid container spacing={4}>
              {modalData?.is_started ? (
                <>
                  <Grid item md={12} xs={12} sm={12}>
                    <Autocomplete
                      style={{ width: '100%' }}
                      size='small'
                      onChange={(event, value) => {
                        setFilterData({
                          ...filterData,
                          selectedWeek: value,
                          batch: '',
                        });
                        if(modalData?.is_started && value) {
                          getApiData(`${endpoints.aol.reshuffleBatchList}?batch_id=${id}&started=true&week_count=${value}`, 'batchList');
                        }
                      }}
                      id='week'
                      className='dropdownIcon'
                      value={filterData?.selectedWeek || ''}
                      options={weekList}
                      getOptionLabel={(option) => option && JSON.stringify(option)}
                      filterSelectedOptions
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant='outlined'
                          label='Start In Week'
                          placeholder='Start In Week'
                        />
                      )}
                    />
                  </Grid>
                </>
              ) : (
                ''
              )}
              {batchList.length !== 0 ? (
                <Grid item md={12} xs={12} sm={12}>
                  <Autocomplete
                    style={{ width: '100%' }}
                    size='small'
                    onChange={handleBatch}
                    id='batch'
                    className='dropdownIcon'
                    value={filterData?.batch}
                    options={batchList}
                    getOptionLabel={(option) => option ? `${option?.course__is_common ? option?.course__course_name + ',' : ''}  ${option?.start_date ? formatDate(option?.start_date) + ' ' + converTime(option?.start_date?.split('T')?.[1]) + ',' :'' }  ${option?.batch_days?.[0] ? option?.batch_days?.[0] : ''} , ${option?.batch_time_slot} ${option?.teacher__name ? ',' + option?.teacher__name : ''}` : ''}
                    filterSelectedOptions
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant='outlined'
                        label='Reshuffle Batch Name'
                        placeholder='Reshuffle Batch Name'
                      />
                    )}
                  />
                </Grid>
              ) :  batchList.length === 0 ? (
                <Grid item md={12} xs={12} sm={12} style={{ textAlign: 'center' }}>
                    <Typography color='primary'>
                    {!loading && modalData?.is_started && filterData?.selectedWeek
                      ? 'Batches Not Found Please Contact to Admin'
                      : ''}
                    {!loading && !modalData?.is_started && filterData?.selectedTime
                      ? 'Batches Not Found Please Create New Batch'
                      : ''}
                  </Typography>
                  </Grid>
              ) : (
                ''
              )}
              {!modalData?.is_started && batchList.length === 0 ? (
              <Grid item md={12} xs={12} sm={12}>
                <Autocomplete
                  style={{ width: '100%' }}
                  size='small'
                  onChange={(event, value) => {
                    setFilterData({
                      ...filterData,
                      selectedDay: value,
                      selectedTime: '',
                      batch: '',
                      selectedWeek: ''
                    });
                    if (!modalData?.is_started && value) {
                      getApiData(
                        `${endpoints.aol.getBatchTimeSlotList}?course_id=${modalData?.course_id}&days=${value}`,
                        'timeList'
                      );
                    }
                  }}
                  id='day'
                  className='dropdownIcon'
                  value={filterData?.selectedDay}
                  options={dayList}
                  getOptionLabel={(option) => option}
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant='outlined'
                      label='Select Days'
                      placeholder='Select Days'
                    />
                  )}
                />
              </Grid>
              ) : (
                ''
              )}
              {!modalData?.is_started && filterData?.selectedDay && batchList.length === 0 ? (
                <Grid item md={12} xs={12} sm={12}>
                  <Autocomplete
                    style={{ width: '100%' }}
                    size='small'
                    onChange={(event, value) => {
                      setFilterData({
                        ...filterData,
                        selectedTime: value,
                        batch: '',
                        selectedWeek: '',
                      });
                    }}
                    id='time'
                    className='dropdownIcon'
                    value={filterData?.selectedTime}
                    options={timeList}
                    getOptionLabel={(option) => option}
                    filterSelectedOptions
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant='outlined'
                        label='Select Time'
                        placeholder='Select Time'
                      />
                    )}
                  />
                </Grid>
              ) : (
                ''
             )}
            </Grid>

            {filterData?.batch && batchList?.length !== 0 ? (
              <Grid container spacing={4}>
               <Grid item md={2} xs={1} />
               <Grid item md={8} xs={10}>
                <Grid container spacing={2} style={{ margin: '10px 0px', border: '1px solid #184b86', borderRadius: '5px' }}>
                  <Grid item xs={12} md={12}>
                    <Typography variant='h5' style={{ textAlign: 'center' }}>Selected Batch Details</Typography>
                    <Divider className='divider'/>
                  </Grid>
                  {labelData('Course Name', filterData?.batch?.course__course_name)}
                  {labelData('Start date', filterData?.batch.start_date ? formatDate(filterData?.batch.start_date) : 'Batch Not Started')}
                  {labelData('Start time', filterData?.batch.start_date ? converTime(filterData?.batch.start_date?.split('T')?.[1]) : 'Batch Not Started')}
                  {labelData('Days', filterData?.batch?.batch_days?.[0])}
                  {labelData('Time slot', filterData?.batch?.batch_time_slot)}
                  {labelData('Teacher name', filterData?.batch?.teacher__name || 'Teacher Not Assigned')}
                  {labelData('Batch size', `1 : ${filterData?.batch?.batch_size}`)}
                </Grid>
               </Grid>
               <Grid item md={2} xs={1} />
              </Grid>
            ) : (
              <div style={{ marginTop: '2rem' }} />
            )}
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} sm={12} style={{ textAlign: 'center' }}>
                <Button variant="contained" onClick={() => setOpenReshuffleModal(false)} color='primary' style={{ marginTop: '10px' }}>
                  Cancel
                </Button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                {batchList?.length !== 0 ? (
                <Button fullwidth variant="contained" color='primary' disabled={loading} onClick={handleReshuffle} style={{ marginTop: '10px' }}>
                    Reshuffle
                  </Button>): ''}
                {filterData?.selectedDay &&
                  filterData?.selectedTime &&
                  batchList?.length === 0 &&
                  !modalData?.is_started ? (
                  <Button  variant="contained" fullwidth color='primary' disabled={loading} onClick={handleCreateBatch} style={{ marginTop: '10px' }}>
                  Create New Batch
                </Button>
                ):''}
              </Grid>
              {/* {batchList?.length !== 0 ? (
                <Grid item md={6} xs={12} sm={6} style={{ textAlign: 'left' }}>
                  <Button fullwidth color='primary' onClick={handleReshuffle}>
                    Reshuffle
                  </Button>
                </Grid>
              ) : (
                ''
              )} */}
              {/* // {filterData?.selectedDay &&
              // filterData?.selectedTime &&
              // batchList?.length === 0 &&
              // !modalData?.is_started ? (
              //   <Grid item md={6} xs={12} sm={6} style={{ textAlign: 'left' }}>
              //     <Button fullwidth color='primary' onClick={handleCreateBatch}>
              //       Create New Batch
              //     </Button>
              //   </Grid>
              // ) : (
              //   ''
              // )} */}
            </Grid>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {/* <Button onClick={() => setOpenReshuffleModal(false)} color="primary">
                            Cancel
                    </Button>
                    <Button color="primary"
                        // onClick={() => setOpenReshuffleModal(false)}
                        onClick={handleReshuffle}
                    >
                        Reshuffle
                    </Button> */}
        </DialogActions>
        {loading && <Loader style={{zIndex: 100000 }} />}
      </Dialog>
    </div>
  );
};

export default ReshuffleModal;
