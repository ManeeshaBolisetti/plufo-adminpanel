/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState, useEffect } from 'react';
import { Grid, Card, Divider, Button, Popover, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import Timer from 'react-compound-timer';
import axiosInstance from '../../../../config/axios';
import endpoints from '../../../../config/endpoints';
import Loader from '../../../../components/loader/loader';
import { AlertNotificationContext } from '../../../../context-api/alert-context/alert-state';
import ResourceDialog from '../../../online-class/online-class-resources/resourceDialog';
import CountdownTimer from './CountdownTimer';
import './index.css';
import UploadDialogBox from './UploadDialogBox';

const JoinClass = (props) => {
  const { index, cardIndex, getClassName, historicalData } = props;
  const fullData = props.fullData;
  const handleClose = props.handleClose;
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const [joinAnchor, setJoinAnchor] = useState(null);
  const [classWorkDialog, setDialogClassWorkBox] = useState(false);

  const [isAccept, setIsAccept] = useState(props.data ? props.data.is_accepted : false);
  const [isRejected, setIsRejected] = useState(
    props.data ? props.data.is_restricted : false
  );
  const history = useHistory();

  // const classStartTime = moment(props && props?.data && props?.data?.date).format(
  //   'DD-MM-YYYY'
  // );
  const currDate = moment(new Date()).format('DD-MM-YYYY');

  const startTime = props && props?.data?.start_time;
  const currTime = moment(new Date()).format('x');
  const servTime = new Date(`${props.data.server_time}`).getTime();
  const classTimeMilli = new Date(`${props.data.date}T${startTime}`).getTime();
  const diffTime = classTimeMilli - 5 * 60 * 1000;
  const serverD = new Date(`${props.data.server_time}`)
  const servYear = serverD.getFullYear();
  const servMonth = serverD.getMonth() + 1;
  const servDay = serverD.getDate();
  const serverDate = servDay + '-' + servMonth + '-' + servYear;
  const endTime = new Date(`${props.data.date}T${props?.data?.end_time}`).getTime();

  const classStartTimes = new Date(props && props?.data && props?.data?.date)
  const classStartYear = parseInt(classStartTimes.getFullYear());
  const classStartMonth = parseInt(classStartTimes.getMonth() + 1);
  const classStartDay = parseInt(classStartTimes.getDate());
  const classStartTime = classStartTimes.getDate() + "-" + parseInt(classStartTimes.getMonth() + 1) + "-" + classStartTimes.getFullYear()
  const serverStartTimes = new Date(props && props?.data && props?.data?.server_time);
  const serverStartYear = parseInt(serverStartTimes.getFullYear());
  const serverStartMonth = parseInt(serverStartTimes.getMonth() + 1);
  const serverStartDay = parseInt(serverStartTimes.getDate());
  const serverStartTime = serverStartTimes.getDate() + "-" + parseInt(serverStartTimes.getMonth() + 1) + "-" + serverStartTimes.getFullYear()

  var offset = new Date();
  function convertTZ(date, tzString) {
    return new Date(
      (typeof date === 'string' ? new Date(date) : date).toLocaleString('en-US', {
        timeZone: tzString,
      })
    );
  }
  const date = convertTZ(offset, 'Asia/Kolkata');
  const currDates = new Date(date);
  const currDateDay = currDates.getDate();
  const currDateMonth = parseInt(currDates.getMonth() + 1);
  const currDateYear = currDates.getFullYear();
  const diffAuditTime = classTimeMilli + 2 * 60 * 1000;



  const handleCloseData = () => {
    setAnchorEl(null);
    // setJoinAnchor(null)
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    // setJoinAnchor(event.currentTarget);
  };
  const isClassStartted = () => {
    let disableFlag = false;
    if ((servTime ? servTime : new Date().getTime()) >= startTime) {
      disableFlag = false;
    } else {
      disableFlag = true;
    }
    return disableFlag;
  };
  const handleCloseAccept = () => {
    setJoinAnchor(null);
  };

  const handleClickAccept = (event) => {
    if (diffTime > parseInt(servTime ? servTime : currTime)) {
      setJoinAnchor(event.currentTarget);
    } else if (parseInt(servTime ? servTime : currTime) > diffTime || parseInt(servTime ? servTime : currTime) === diffTime) {
      handleIsAccept();
    }
  };

  const handleIsAccept = () => {
    const params = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: props.data && props.data.date,
      is_accepted: true,
    };
    axiosInstance
      .put(endpoints.studentViewBatchesApi.rejetBatchApi, params)
      .then((res) => {
        setLoading(false);
        setIsAccept(true);
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  };
  function handleCancel() {
    setLoading(true);
    const params1 = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: props.data && props.data.date,
    };

    const params2 = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: props.data && props.data.date,
      is_restricted: true,
    };

    if (window.location.pathname === '/erp-online-class-student-view') {
      //url = endpoints.studentViewBatchesApi.rejetBatchApi;
      axiosInstance
        .put(endpoints.studentViewBatchesApi.rejetBatchApi, params2)
        .then((res) => {
          setLoading(false);
          setAlert('success', 'Class Rejected');
          handleClose('success');
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    } else {
      //url = endpoints.teacherViewBatches.cancelBatchApi;
      axiosInstance
        .put(endpoints.teacherViewBatches.cancelBatchApi, params1)
        .then((res) => {
          setLoading(false);
          setAlert('success', res.data.message);
          handleClose('success');
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
  }
  const handleTakeQuiz = (fullData) => {
    if (fullData && fullData.online_class && fullData.online_class.question_paper_id) {
      history.push({
        pathname: `/erp-online-class/${fullData.online_class.id}/${fullData.online_class.question_paper_id}/pre-quiz`,
        state: { data: fullData.online_class.id },
      });
    } else {
      setAlert('error', 'This onlineclass does not have quiz associated with it.');
      return;
    }
  };
  const handleOpenClassWorkDialogBox = (value) => {
    setDialogClassWorkBox(value);
  };

  const handleAudit = () => {
    // console.log(fullData?.join_url, 'audit testing');
    window.open(fullData?.join_url)
    // axiosInstance
    //   .get(`${endpoints.teacherViewBatches.hostApi}?id=${fullData.id}`)
    //   .then((res) => {
    //     console.log(res);
    //     window.open(res.data.url, '_blank');
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  };

  const isClassAudited = () => {
    let disableFlag = false;
    var today = new Date();
    var time = today.getTime();
    if ((serverStartDay ? serverStartDay : currDateDay) === classStartDay && (serverStartMonth ? serverStartMonth : currDateMonth) === classStartMonth && (serverStartYear ? serverStartYear : currDateYear) === classStartYear) {
      if ((servTime ? servTime : time) >= diffAuditTime) {
        disableFlag = false;
      } else {
        disableFlag = true;
      }
    } else {
      disableFlag = true;
    }
    return disableFlag;
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const openJoin = Boolean(joinAnchor);
  const ids = open ? 'accept-popover' : undefined;
  return (
    <Grid container spacing={2} direction='row' alignItems='center'>
      <Grid item md={6} xs={12}>
        <span className='TeacherFullViewdescreption1'>
          {moment(props.data ? props.data.date : '').format('DD-MM-YYYY')}
        </span>
      </Grid>
      {window.location.pathname === '/erp-online-class-student-view' ? (
        <>
          <Grid item xs={4}>
            <Button
              size='small'
              color='secondary'
              fullWidth
              variant='contained'
              onClick={() => handleTakeQuiz(fullData)}
              disabled={
                props?.data?.class_status?.toLowerCase() === 'cancelled' ||
                fullData?.online_class?.question_paper_id === 0
              }
              // className='takeQuizButton'
              className={`teacherFullViewSmallButtons1 ${getClassName()[1]}`}
            >
              Take Quiz
            </Button>
          </Grid>

          <Grid item xs={4}>
            <Button
              size='small'
              fullWidth
              variant='contained'
              onClick={() => {
                setDialogClassWorkBox(true);
              }}
              disabled={
                isClassStartted() ||
                props?.data?.class_status?.toLowerCase() === 'cancelled'
              }
              className={`teacherFullViewSmallButtons1 ${getClassName()[1]}`}
            >
              Class Work
            </Button>
            {classWorkDialog && (
              <UploadDialogBox
                historicalData={historicalData}
                periodData={props?.data}
                setLoading={setLoading}
                fullData={fullData}
                classWorkDialog={classWorkDialog}
                OpenDialogBox={handleOpenClassWorkDialogBox}
              />
            )}
          </Grid>
        </>
      ) : (
        ''
      )}
      {window.location.pathname === '/online-class/teacher-view-class' ? (
        <>
          <Grid item xs={4}>
            <Button
              size='small'
              color='#344ADE'
              fullWidth
              variant='contained'
              onClick={() => {
                const { id = '', online_class = {} } = fullData || {};
                const { id: onlineClassId = '', start_time = '' } = online_class || {};
                history.push({
                  pathname: `/erp-online-class/class-work/${onlineClassId}/${id}/${props.data.date}`,
                  state: { historicalData: historicalData },
                });
              }}
              disabled={isClassStartted() || props?.data?.is_cancelled}
              className={`teacherFullViewSmallButtons1 ${getClassName()[1]}`}
            >
              Class Work
            </Button>
          </Grid>
        </>
      ) : (
        ''
      )}

      {isAccept ? (
        <Grid item xs={6}>
          <Button
            size='small'
            color='secondary'
            fullWidth
            variant='contained'
            onClick={() => window.open(fullData && fullData.join_url)}
            className='teacherFullViewSmallButtons'
          >
            Join
          </Button>
        </Grid>
      ) : (
        <>
          {isRejected ? (
            <Grid item xs={6}>
              <Typography style={{ color: '#ff6b6b' }}>Rejected</Typography>
            </Grid>
          ) : (
            <>
              <Grid item md={3} xs={6}>
                {window.location.pathname === '/erp-online-class' ? (
                  <Button
                    size='small'
                    color='secondary'
                    fullWidth
                    variant='contained'
                    disabled={isClassAudited()}
                    onClick={() =>
                      // window.open(fullData && fullData.presenter_url, '_blank')
                      // setAlert('success', 'audit testing')
                      // console.log(fullData, 'audit testing')
                      handleAudit()
                    }
                    className='teacherFullViewSmallButtons'
                  >
                    Audit
                  </Button>
                ) : (
                  ''
                )}
                {window.location.pathname === '/erp-online-class-teacher-view' ? (
                  <Button
                    size='small'
                    color='secondary'
                    fullWidth
                    variant='contained'
                    onClick={() =>
                      window.open(fullData && fullData.presenter_url, '_blank')
                    }
                    className='teacherFullViewSmallButtons'
                  >
                    Host
                  </Button>
                ) : (
                  ''
                )}
                {window.location.pathname === '/erp-online-class-student-view' ? (
                  <>
                    <Popover
                      id={ids}
                      open={openJoin}
                      joinAnchor={joinAnchor}
                      onClose={handleClose}
                      style={{ overflow: 'hidden', margin: '19% 0 0 30%' }}
                      className='xyz'
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                    >
                      <Grid
                        container
                        spacing={2}
                        style={{ textAlign: 'center', padding: '10px' }}
                      >
                        <Grid item xs={12} style={{ textAlign: 'right' }}>
                          <CloseIcon
                            style={{ color: '#014B7E' }}
                            onClick={() => handleCloseAccept()}
                          />
                        </Grid>
                        <Grid item md={12} xs={12}>
                          <Typography>
                            You Can Join 5mins Before :{' '}
                            {moment(`${props?.data?.date}T${startTime}`).format(
                              'hh:mm:ss A'
                            )}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Popover>
                    <Button
                      size='small'
                      color='secondary'
                      fullWidth
                      variant='contained'
                      // onClick={handleIsAccept}
                      onClick={(e) => handleClickAccept(e)}
                      disabled={classStartTime === (serverDate ? serverDate : currDate) ? false : true}
                      className='teacherFullViewSmallButtons'
                    >
                      Accept
                    </Button>
                  </>
                ) : (
                  ''
                )}
              </Grid>
              <Grid item md={3} xs={6}>
                <Popover
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  style={{ overflow: 'hidden' }}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                  }}
                >
                  <Grid
                    container
                    spacing={2}
                    style={{ textAlign: 'center', padding: '10px' }}
                  >
                    <Grid item md={12} xs={12}>
                      <Typography>Are you sure to Cancel ?</Typography>
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Button
                        variant='contained'
                        size='small'
                        style={{ fontSize: '11px' }}
                        onClick={() => handleCloseData()}
                      >
                        Cancel
                      </Button>
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Button
                        variant='contained'
                        color='primary'
                        style={{ fontSize: '11px' }}
                        size='small'
                        onClick={() => handleCancel()}
                      >
                        Confirm
                      </Button>
                    </Grid>
                  </Grid>
                </Popover>
                {endTime >= (servTime ? servTime : currTime) ? (
                  <Button
                    size='small'
                    fullWidth
                    variant='contained'
                    className='teacherFullViewSmallButtons1'
                    onClick={(e) => handleClick(e)}

                  >
                    {window.location.pathname === '/erp-online-class-student-view'
                      ? 'Reject'
                      : 'Cancel'}
                  </Button>) : ('')}
              </Grid>
            </>
          )}
        </>
      )}
    </Grid>
  );
};

const DetailCardView = ({
  fullData,
  handleClose,
  viewMoreRef,
  selectedClassType,
  selectedGrade,
}) => {
  const [noOfPeriods, setNoOfPeriods] = useState([]);
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const { role_details } = JSON.parse(localStorage.getItem('userDetails'));
  /*
  useEffect(() => {
    if (fullData) {
      axiosInstance
        .get(
          `erp_user/${
            fullData  && fullData.id
          }/online-class-details/`
        )
        .then((res) => {
          setNoOfPeriods(res.data.data);
        })
        .catch((error) => setAlert('error', error.message));
    }
  }, [fullData]);
  */
  console.log(selectedClassType, '[[[[[[[[[[[[[[[[');
  useEffect(() => {
    setLoading(true)
    let detailsURL =
      window.location.pathname === '/erp-online-class-student-view'
        ? `erp_user/${fullData && fullData.id}/student-oc-details/`
        : `erp_user/${fullData && fullData.id}/online-class-details/`;
    if (fullData) {
      axiosInstance
        .get(detailsURL)
        .then((res) => {
          setLoading(false)
          console.log(res.data);
          setNoOfPeriods(res.data.data);
        })
        .catch((error) => {
          setLoading(false)
          setAlert('error', error.message)
        });
    }
  }, [fullData, handleClose]);

  const handleCloseData = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const converTime = (info) => {
    let hour = info.split(':')[0];
    let min = info.split(':')[1];
    const part = hour >= 12 ? 'PM' : 'AM';
    min = `${min}`.length === 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = `${hour}`.length === 1 ? `0${hour}` : hour;
    return `${hour}:${min} ${part}`;
  };

  function handleCancel() {
    setLoading(true);
    const params = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: fullData && fullData?.online_class?.start_time.split('T')[0],
    };
    let url = '';
    if (window.location.pathname === '/erp-online-class-student-view') {
      url = endpoints.studentViewBatchesApi.rejetBatchApi;
    } else {
      url = endpoints.teacherViewBatches.cancelBatchApi;
    }
    axiosInstance
      .put(url, params)
      .then((res) => {
        setLoading(false);
        setAlert('success', res.data.message);
        handleClose('success');
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  console.log(fullData.online_class.cource_id, selectedGrade, '=====================');
  const handleAttendance = () => {
    history.push(`/aol-attendance-list/${fullData.online_class && fullData.id}`);
  };
  const handleCoursePlan = () => {
    if (window.location.pathname === '/erp-online-class-student-view') {
      sessionStorage.setItem('isErpClass', 2);
      history.push(
        `/create/course/${fullData.online_class && fullData.online_class.course_id}/1`
      );
    } else {
      sessionStorage.setItem('isErpClass', 3);
      history.push(
        `/create/course/${fullData.online_class && fullData.online_class.cource_id}/${selectedGrade.id
        }`
      );
    }
  };

  const [openPopup, setOpenPopup] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState('');

  const handleClickOpen = () => {
    setOpenPopup(true);
  };

  const handleClosePopup = (value) => {
    setOpenPopup(false);
    setSelectedValue(value);
  };

  return (
    <>
      <Grid container spacing={2} ref={viewMoreRef}>
        <Grid item md={12} xs={12} className='teacherBatchFullViewMainDiv'>
          <Card className='teacherBatchFullViewMainCard'>
            <Grid container spacing={2} className='teacherBatchFullViewHeader'>
              <Grid item xs={12} style={{ textAlign: 'right' }}>
                <CloseIcon style={{ color: '#014B7E', cursor: 'pointer' }} onClick={() => handleClose()} />
              </Grid>
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item md={8} xs={8}>
                    <h4 className='teacherBatchFullCardLable'>
                      {(fullData &&
                        fullData.online_class &&
                        fullData.online_class.title) ||
                        ''}
                    </h4>
                    <h4 className='teacherBatchFullCardLable'>
                      {(fullData &&
                        fullData.online_class &&
                        fullData.online_class.subject &&
                        fullData.online_class.subject.length !== 0 &&
                        fullData.online_class.subject.map((item) => (
                          <span>
                            {item.subject_name || ''}
                            &nbsp;
                          </span>
                        ))) ||
                        ''}
                    </h4>
                  </Grid>
                  <Grid item md={4} xs={4}>
                    <h4 className='teacherBatchFullCardLable'>
                      {(fullData &&
                        fullData.join_time &&
                        converTime(fullData.join_time.split(' ')[1])) ||
                        ''}
                    </h4>
                    <h4 className='teacherBatchFullCardLable'>
                      {noOfPeriods && noOfPeriods.length}
                      &nbsp;Periods
                    </h4>
                  </Grid>
                </Grid>
              </Grid>
              {/* 
                    <IconButton
                      size='small'
                      className='teacherBatchFullViewClose'
                      onClick={() => handleClose()}
                    >
                      <CloseIcon className='teacherBatchFullViewCloseIcon' />
                    </IconButton>
                  </Grid> */}
            </Grid>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12}>
                <span className='TeacherFullViewdescreption'>Periods Date</span>
              </Grid>
              <Grid
                item
                md={12}
                xs={12}
                style={{ maxHeight: '40vh', overflowY: 'scroll' }}
              >
                <Divider className='fullViewDivider' />
                {!fullData && noOfPeriods && noOfPeriods.length === 0 && (
                  <Typography style={{ color: '#ff6b6b', margin: '10px' }}>
                    No Record found
                  </Typography>
                )}
                {noOfPeriods &&
                  noOfPeriods.length > 0 &&
                  noOfPeriods.map((data) => (
                    <JoinClass
                      data={data}
                      fullData={fullData}
                      handleClose={handleClose}
                    />
                  ))}

                <Divider className='fullViewDivider' />
              </Grid>
              <Grid item md={12} xs={12}>
                {window.location.pathname !== '/erp-online-class-student-view' ? (
                  <Button
                    fullWidth
                    size='small'
                    variant='contained'
                    className='teacherFullViewFullButtons'
                    onClick={handleAttendance}
                  >
                    Attendance
                  </Button>
                ) : (
                  <Button
                    fullWidth
                    size='small'
                    variant='contained'
                    className='teacherFullViewFullButtons'
                    onClick={handleClickOpen}
                  >
                    Resources
                  </Button>
                )}
                {selectedClassType && selectedClassType.id !== 0 && (
                  <Button
                    fullWidth
                    size='small'
                    variant='contained'
                    className='teacherFullViewFullButtons'
                    onClick={handleCoursePlan}
                  >
                    View Course Plan
                  </Button>
                )}
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      <ResourceDialog
        selectedValue={selectedValue}
        open={openPopup}
        onClose={handleClosePopup}
        title={fullData.online_class.title}
        resourceId={fullData.id}
        onlineClassId={fullData.online_class.id}
        startDate={fullData.online_class.start_time}
        endDate={fullData.online_class.end_time}
      />
      {loading && <Loader />}
    </>
  );
};

export default DetailCardView;
