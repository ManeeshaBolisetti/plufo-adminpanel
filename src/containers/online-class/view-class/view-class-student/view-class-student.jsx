import React, { useContext, useEffect, useState } from 'react';
import { Grid, Typography, Button, Modal } from '@material-ui/core';
import moment from 'moment';

import Countdown from '../../../../components/countdown/countdown';
import './view-class-student.scss';
import { OnlineclassViewContext } from '../../online-class-context/online-class-state';
import ResourceModal from '../../online-class-resources/resourceModal';
import OnlineClassFeedback from '../../Feedback/OnlineClassFeedback';
import axiosInstance from '../../../../config/axios';
import endpoints from '../../../../config/endpoints';
import { AlertNotificationContext } from '../../../../context-api/alert-context/alert-state';

const ViewClassStudent = (props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isFeedbackOpen, setIsFeedbackOpen] = useState(false);
  const [onlineClassId, setOnlineClassId] = useState(null);
  const {
    data: {
      resource_available: isResourceAvailable,
      is_accepted: isAccepted,
      join_time: joinTime,
      id: meetingId,
      zoom_meeting: {
        is_canceled:canceledTag,
        id: zoomId,
        online_class: {
          id: olClassId,
          start_time: startTime,
          end_time: endTime,
          title = '',
          subject: subjectName,
          join_limit: joinLimit,
        },
      } = {},
    },
  } = props || {};

  const {
    handleAccept,
    dispatch,
    handleJoin,
    studentView: { currentServerTime },
  } = useContext(OnlineclassViewContext);

  const [hasClassStarted, setHasClassStarted] = useState(false);
  const [hasClassEnded, setHasClassEnded] = useState(false);
  const [isJoinTime, setIsJoinTime] = useState(false);

  const { setAlert } = useContext(AlertNotificationContext);

  const { role_details: roleDetails } =
    JSON.parse(localStorage.getItem('userDetails')) || {};

  useEffect(() => {
    const now = new Date(moment(currentServerTime).format('llll'));
    // const now = new Date(currentServerTime);
    if (startTime) {
      const difference = new Date(moment(startTime).format('llll')) - now;
      if (difference < 0) {
        setHasClassStarted(true);
      } else {
        setTimeout(() => {
          setHasClassStarted(true);
        }, difference);
      }
    }
    if (joinTime) {
      // const difference = new Date(moment(joinTime).format('llll')) - now;
      const difference = moment(joinTime).diff(moment.now());
      const duration = moment.duration(difference, 'milliseconds');
      const days = moment.duration(duration).days();
      const hours = moment.duration(duration).hours();
      const minutes = moment.duration(duration).minutes();
      const seconds = moment.duration(duration).seconds();
      if(minutes <= 5){
        setTimeout(() => {
          setIsJoinTime(true);
        }, difference);

      }
    }
    if (endTime) {
      const difference = new Date(moment(endTime).format('llll')) - now;
      if (difference < 0) {
        setHasClassStarted(false);
        setHasClassEnded(true);
        setIsJoinTime(false);
      } else {
        setTimeout(() => {
          setHasClassStarted(false);
          setHasClassEnded(true);
          setIsJoinTime(false);
        }, difference);
      }
    }
  }, []);

  const getClassOngoingStatus = () => {
    if (hasClassEnded) {
      return <>Class has ended</>;
    }
    if (hasClassStarted) {
      return <>Class is ongoing</>;
    }
    return (
      <>
        <Countdown startTime={startTime} />
      </>
    );
  };

  const handleClassAccept = () => {
    dispatch(handleAccept(meetingId));
  };

  const handleClassJoin = async () => {
    try {
      const response = await axiosInstance.get(
        `${endpoints.onlineClass.feedback}?erp_user_id=${roleDetails.erp_user_id}`
      );
      const {
        is_attended: isAttended,
        feedback_submitted: feedbackSubmitted,
      } = response.data;
      if (isAttended && feedbackSubmitted === false) {
        setIsFeedbackOpen(true);
      } else {
        dispatch(handleJoin(meetingId));
      }
    } catch (error) {
      setAlert('error', 'Failed to get attendance details');
    }
  };

  const closeModalHandler = () => {
    setIsModalOpen(false);
  };

  const handleFeedbackClick = async (rating) => {
    try {
      const formData = new FormData();
      formData.append('online_class_id', olClassId);
      formData.append('zoom_meeting_id', zoomId);
      formData.append('erp_user_id', roleDetails.erp_user_id);
      formData.append('rating', rating);
      await axiosInstance.post(`${endpoints.onlineClass.feedback}`, formData);
      setIsFeedbackOpen(false);
      dispatch(handleJoin(meetingId));
    } catch (error) {
      setAlert('error', 'Failed to submit feedback');
    }
  };

  return (
    <div className='viewclass__student-container'>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          {/*  */}
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography variant='h5' gutterBottom color='primary'>
                {title}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography
                variant='h6'
                gutterBottom
                color='secondary'
                className='responsive__align'
              >
                {getClassOngoingStatus()}
              </Typography>
            </Grid>
          </Grid>
          {/*  */}
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography variant='h6' gutterBottom color='secondary'>
                {subjectName[0]?.subject_name.substring(subjectName[0]?.subject_name.lastIndexOf("_")+1)}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography
                variant='h6'
                gutterBottom
                color='secondary'
                className='responsive__align'
              >
                Join limit: {joinLimit}
              </Typography>
            </Grid>
          </Grid>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <Typography variant='h6' gutterBottom color='secondary'>
                {moment(startTime).format('MMMM Do YYYY, h:mm:ss a')}
              </Typography>
            </Grid>
          </Grid>
          {/*  */}
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={3}>
            {!canceledTag?
            <Grid item xs={12} sm={6}>
              {!isAccepted ? (
                <Button
                  className='viewclass__student-btn'
                  variant='contained'
                  color='primary'
                  disabled={isAccepted || !!hasClassEnded}
                  onClick={handleClassAccept}
                >
                  Accept Class
                </Button>
              ) : (
                <Button
                  className='viewclass__student-btn'
                  variant='contained'
                  color='primary'
                  disabled={!isJoinTime || hasClassEnded}
                  onClick={handleClassJoin}
                >
                  Join Class
                </Button>
              )}
            </Grid>
            :
            <Grid item xs={12} sm={6}>
                <Button
                  className='viewclass__student-btn'
                  variant='contained'
                  color='primary'
                  disabled={canceledTag}
                  onClick={handleClassAccept}
                >
                  Canceled Class
                </Button>
            </Grid>}
            {isResourceAvailable ? (
              <Grid item xs={12} sm={6}>
                <Button
                  className='viewclass__student-btn'
                  variant='contained'
                  color='primary'
                  onClick={() => {
                    setIsModalOpen(true);
                    setOnlineClassId(olClassId);
                  }}
                >
                  Resources
                </Button>
              </Grid>
            ) : (
              ''
            )}
          </Grid>
        </Grid>
      </Grid>
      {onlineClassId && (
        <ResourceModal
          alert={setAlert}
          isOpen={isModalOpen}
          onClick={closeModalHandler}
          id={olClassId}
          type='resource'
          key={`resource_modal${olClassId}`}
        />
      )}
      <Modal
        open={isFeedbackOpen}
        onClose={() => {
          setIsFeedbackOpen(false);
        }}
        aria-labelledby='simple-modal-title'
        aria-describedby='simple-modal-description'
      >
        <div className='onlineclass__feedback--modal'>
          <div className='onlineclass__feedback--topbar'>
            <p className='onlineclass__feedback--topbartitle'>
              How was your last online class ?
            </p>
          </div>
          <div className='onlineclass__emoji--container'>
            <OnlineClassFeedback
              handleFeedBack={(rating) => {
                handleFeedbackClick(rating);
              }}
              feedbackType='numeric'
            />
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default ViewClassStudent;
