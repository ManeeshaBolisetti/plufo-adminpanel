import React, { useState, useContext } from 'react';
import { Grid, TextField, Divider, Button, Card } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import filterImage from '../../assets/images/unfiltered.svg';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';

const ExistingStudent = () => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [phoneNumber, setPhoneNumber] = useState('');
  const [loading, setLoading] = useState(false);
  const [studentDetails, setStudentDetails] = useState('');
  const history = useHistory();

  function getStudentDetails() {
    if (!phoneNumber) {
      setAlert('warning', 'Enter Student Name or Phone Number');
      return;
    }
    setLoading(true);
    axiosInstance
      .get(`${endpoints.salesAol.searchUserApi}?search=${phoneNumber}`)
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setStudentDetails(result.data.data);
        } else {
          setAlert('error', result.data.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  return (
    <>
      {loading ? (
        <Loading message='Loading...' />
      ) : (
        <Grid Container spacing={2}>
          <Grid item md={12} xs={12}>
            <Grid container spacing={2}>
              <Grid item md={6} xs={12}>
                <TextField
                  value={phoneNumber}
                  variant='outlined'
                  margin='dense'
                  label='Enter Student Name / Phone Number'
                  fullWidth
                  onKeyPress={(e) => (e.keyCode === 13 ? getStudentDetails() : '')}
                  placeholder='Enter Student Name / Phone Number'
                  onChange={(e) => setPhoneNumber(e.target.value.trimLeft())}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <Button
                  color='primary'
                  variant='contained'
                  style={{ marginTop: '8px' }}
                  onClick={() => getStudentDetails()}
                >
                  Get Student Details
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Divider />
          <Grid item md={12} xs={12}>
            {phoneNumber &&
              studentDetails &&
              studentDetails.results &&
              studentDetails.results.length !== 0 && (
                <Grid container spacing={2}>
                  {studentDetails &&
                    studentDetails.results &&
                    studentDetails.results.map((item) => (
                      <Grid
                        key={item.id}
                        item
                        md={4}
                        xs={12}
                        style={{ margin: '10px 0px' }}
                      >
                        <Card
                          style={{
                            borderRadius: '5px',
                            backgroundColor: 'snow',
                            color: '#014B7E',
                            padding: '10px',
                          }}
                        >
                          <Grid container spacing={2}>
                            <Grid item md={12} xs={12}>
                              <Typography style={{ fontSize: '15px', margin: '0px' }}>
                                Name: &nbsp;
                                {item.name || ''}
                              </Typography>
                            </Grid>
                            <Grid item md={12} xs={12}>
                              <Typography style={{ fontSize: '15px', margin: '0px' }}>
                                Phone Number : &nbsp;
                                {item.contact || ''}
                              </Typography>
                            </Grid>
                            <Grid item md={12} xs={12}>
                              <Typography style={{ fontSize: '15px', margin: '0px' }}>
                                ERP ID : &nbsp;
                                {item.erp_id || ''}
                              </Typography>
                            </Grid>
                            <Grid
                              item
                              md={12}
                              xs={12}
                              style={{ margin: '10px 0px', textAlign: 'right' }}
                            >
                              <Button
                                size='small'
                                color='primary'
                                variant='contained'
                                onClick={() =>
                                  history.push(
                                    `/aol-custom-batch/${item.user}/${item.id}`
                                  )}
                              >
                                Proceed
                              </Button>
                            </Grid>
                          </Grid>
                        </Card>
                      </Grid>
                    ))}
                </Grid>
              )}
            {phoneNumber &&
              studentDetails &&
              studentDetails.results &&
              studentDetails.results.length === 0 && (
                <Typography
                  variant='h4'
                  style={{ color: '#014B7E', textAlign: 'center' }}
                >
                  Records Not Found
                </Typography>
              )}
            {(!studentDetails || !phoneNumber) && (
              <Grid container spacing={2}>
                <Grid
                  item
                  md={12}
                  xs={12}
                  style={{ textAlign: 'center', marginTop: '10px' }}
                >
                  <img src={filterImage} alt='crash' height='250px' width='250px' />
                  <Typography>
                    Please Enter Student Name or PhoneNumber to get Details
                  </Typography>
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default ExistingStudent;
