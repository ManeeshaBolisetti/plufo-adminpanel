import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useHistory } from 'react-router-dom';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Grid, Card } from '@material-ui/core';
import './style.scss';
import NewStudent from './new-student';
import ExistingStudent from './existing-student';
import Layout from '../Layout';
import displayName from '../../config/displayName';


const SalesModule = () => {
  const history = useHistory();
  const [step, setStep] = useState(1);
  const handleChange = (event, newValue) => {
    setStep(newValue);
  };
  return (
    <Layout>
      <Grid container spacing={2} style={{ width: '100%', margin: '20px 0px' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2} justify='middle'>
            <Grid item md={12} xs={12} style={{ display: 'flex' }}>
              <button
                type='button'
                className='studentIdCardNavigationLinks'
                onClick={() => history.push('/dashboard')}
              >
                Dashboard
              </button>
              <ArrowForwardIosIcon className='studentIdCardNavArrow' />
              <span className='studentIdCardNavigationLinks'>{displayName ? (displayName + ' Sales') : ''}</span>
              <ArrowForwardIosIcon className='studentIdCardNavArrow' />
              <span className='studentIdCardNavigationLinks'>
                {step === 0 ? 'New Student' : 'Existing Student'}
              </span>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid Container spacing={2}>
            <Grid item md={12} xs={12}>
              <AppBar position='static' color='default'>
                <Tabs
                  value={step}
                  onChange={handleChange}
                  indicatorColor='primary'
                  textColor='primary'
                  variant='fullWidth'
                  centered
                  aria-label='full width tabs example'
                >
                  <Tab label='New Student' />
                  <Tab label='Existing Student' />
                </Tabs>
              </AppBar>
            </Grid>
            <Grid item md={12} xs={12}>
              {step === 0 ? (
                <Card style={{ width: '100%', padding: '10px' }}>
                  <NewStudent />
                </Card>
              ) : (
                <Card style={{ width: '100%', padding: '10px' }}>
                  <ExistingStudent />
                </Card>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default SalesModule;
