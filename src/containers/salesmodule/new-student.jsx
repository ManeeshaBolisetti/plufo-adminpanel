import React, { useState, useEffect, useContext } from 'react';
import {
  Grid,
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Button,
} from '@material-ui/core';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import Loading from '../../components/loader/loader';

const NewStudent = () => {
  const { setAlert } = useContext(AlertNotificationContext);
  const [gradeList, setGradeList] = useState([]);
  const [childName, setChildName] = useState('');
  const [parentName, setParentName] = useState('');
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [selectedGender, setSelectedGender] = useState('');
  const [selectedGrade, setSelectedGrade] = useState('');
  const [school, setSchool] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNo, setPhoneNo] = useState('');
  const [loading, setLoading] = useState(false);

  const filterEmailId = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
  const genderList = [
    { id: '1', value: 'male', name: 'Male' },
    { id: '2', value: 'female', name: 'Female' },
    { id: '3', value: 'other', name: 'Other' },
  ];

  useEffect(() => {
    setLoading(true);
    axiosInstance
      .get(`${endpoints.masterManagement.grades}?page=1&page_size=1000`)
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setGradeList(result.data.result.results);
        } else {
          setAlert('error', result.data.message);
          setGradeList([]);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
        setGradeList([]);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handleSubmitForm() {
    if (!childName) {
      setAlert('warning', 'Enter Child Name');
      return;
    }
    if (!parentName) {
      setAlert('warning', 'Enter Parent Name');
      return;
    }
    if (!school) {
      setAlert('warning', 'Enter School Name');
      return;
    }
    if (!email) {
      setAlert('warning', 'Enter Email Address');
      return;
    }
    if (email && email.match(filterEmailId) === null) {
      setAlert('warning', 'Enter Valid Email Address');
      return;
    }
    if (!phoneNo) {
      setAlert('warning', 'Enter phone number');
      return;
    }
    if (phoneNo && phoneNo.length !== 10) {
      setAlert('warning', 'Enter Valid Phone Number');
      return;
    }
    if (!selectedGrade) {
      setAlert('warning', 'Select Grade');
      return;
    }
    if (!selectedGender) {
      setAlert('warning', 'Select Gender');
      return;
    }
    if (!dateOfBirth) {
      setAlert('warning', 'Enter Student Date of Birth');
      return;
    }
    const formData = new FormData();
    formData.set('url', `${endpoints.salesAol.newStudentApi}`);
    formData.set('child_name', childName);
    formData.set('aol_parent', parentName);
    formData.set('date_of_birth', dateOfBirth);
    formData.set('grade', selectedGrade);
    formData.set('gender', selectedGender);
    formData.set('contact', phoneNo);
    formData.set('school_name', school);
    formData.set('email', email);
    formData.set('confirm_password', phoneNo);
    setLoading(true);
    axiosInstance
      .post(endpoints.salesAol.newStudentApi, formData, {
        headers: {
          'content-type': 'multipart/form-data',
        },
      })
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setAlert('success', 'New Student Created successfully');
          setChildName('');
          setParentName('');
          setDateOfBirth('');
          setSelectedGender('');
          setSelectedGrade('');
          setPhoneNo('');
          setSchool('');
          setEmail('');
        } else {
          setAlert('error', result.data.description);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.description);
      });
  }

  return (
    <>
      {loading ? (
        <Loading message='Loading...' />
      ) : (
        <>
          <Grid container spacing={2}>
            <Grid item md={4} xs={12}>
              <TextField
                label='Child Name'
                value={childName}
                margin='dense'
                variant='outlined'
                fullWidth
                placeholder='Enter Child Name'
                onChange={(e) => setChildName(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                label='Parent Name'
                value={parentName}
                margin='dense'
                variant='outlined'
                fullWidth
                placeholder='Enter Parent Name'
                onChange={(e) => setParentName(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                label='School / City'
                value={school}
                margin='dense'
                variant='outlined'
                fullWidth
                placeholder='Enter School/City Name'
                onChange={(e) => setSchool(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                label='Email Address'
                value={email}
                margin='dense'
                variant='outlined'
                fullWidth
                placeholder='Enter Email address'
                helperText={
                  email && email.match(filterEmailId) === null
                    ? 'Enter Valid Email Address'
                    : ''
                }
                onChange={(e) => setEmail(e.target.value.trimLeft())}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                label='Phone Number'
                value={phoneNo}
                margin='dense'
                variant='outlined'
                fullWidth
                className='new-student-register-phoneNo-input'
                type='number'
                placeholder='Enter Phone number'
                helperText={
                  phoneNo && phoneNo.length !== 10 && 'Enter valid Phone Number'
                }
                onChange={(e) =>
                  e.target.value.length > -1 && e.target.value.length < 11
                    ? setPhoneNo(e.target.value.trimLeft())
                    : ''}
              />
            </Grid>
            <Grid item md={4} xs={12}>
              <FormControl fullWidth>
                <InputLabel id='demo-controlled-open-select-label'>
                  Select Grade
                </InputLabel>
                <Select
                  labelId='demo-controlled-open-select-label'
                  id='demo-controlled-open-select'
                  value={selectedGrade}
                  onChange={(e) => setSelectedGrade(e.target.value)}
                  name='child_gender'
                  margin='dense'
                  fullWidth
                >
                  <MenuItem value={1} key={1} disabled>
                    Select Grade
                  </MenuItem>
                  {gradeList &&
                    gradeList.length !== 0 &&
                    gradeList.map((dropItem) => (
                      <MenuItem value={dropItem.id} key={dropItem.id}>
                        {dropItem.grade_name}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item md={4} xs={12}>
              <FormControl fullWidth>
                <InputLabel id='demo-controlled-open-select-label'>
                  Select Gender
                </InputLabel>
                <Select
                  labelId='demo-controlled-open-select-label'
                  id='demo-controlled-open-select'
                  value={selectedGender}
                  onChange={(e) => setSelectedGender(e.target.value)}
                  name='child_gender'
                  margin='dense'
                  fullWidth
                >
                  <MenuItem value={1} key={1} disabled>
                    Select Gender
                  </MenuItem>
                  {genderList &&
                    genderList.length !== 0 &&
                    genderList.map((dropItem) => (
                      <MenuItem value={dropItem.id} key={dropItem.id}>
                        {dropItem.name}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item md={4} xs={12}>
              <TextField
                type='date'
                value={dateOfBirth}
                margin='dense'
                variant='outlined'
                fullWidth
                helperText='Enter Date of birth'
                onChange={(e) => setDateOfBirth(e.target.value)}
              />
            </Grid>
            <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
              <Button color='primary' onClick={() => handleSubmitForm()}>
                Submit
              </Button>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
};

export default NewStudent;
