/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState, useEffect, useRef } from 'react';
import { Grid, Paper, Button } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { useHistory } from 'react-router-dom';
import debounce from 'lodash.debounce';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import AolViewAllBatches from './aol-view-all-batches';

const AolCreateBatch = () => {
  const history = useHistory();
  const { setAlert } = useContext(AlertNotificationContext);
  const [studentList, setStudentList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [page, setPage] = useState(1);
  const [currentFilterType, setCurrentFilterType] = useState('BTSUa');
  async function getStudentList(pageNo, searchVal) {
    setLoading(true);
    const searchParm = searchVal ? `&term=${searchVal}` : '';
    console.log(searchVal)
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.aolBatch.getUnAssighedList
        }?page=${pageNo}&page_size=${10}${searchParm}`
      );
      if (data?.status_code === 200) {
        setStudentList(data);
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  async function getStudentListFilter(pageNo, searchVal, type) {
    setLoading(true);
    console.log(type)
    const searchParm = searchVal ? `&term=${searchVal}` : '';
    let filterParam = '';
    if (type === "BTS") {
      filterParam = `&is_timeslot_selected=${true}`
    } else if (type === "BTNS") {
      filterParam = `&is_timeslot_selected=${false}`
    }
    console.log(filterParam)
    try {
      const { data } = await axiosInstance.get(
        `${endpoints.aolBatch.getStudentTimeSlotStatus}?page=${pageNo}&page_size=${10}${filterParam}${searchParm}`
      );
      if (data?.status_code === 200) {
        console.log(data)
        setStudentList(data);
        setLoading(false);
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }


  async function filterbatch(page, searchValue, type) {
    // setCurrentFilterType(type);
    // if(type === "BTSUa"){
    //   getStudentList();
    //   return;
    // }
    // let filterParam = '';
    // setLoading(true);
    // if(type === "BTS"){
    //   filterParam = `&is_timeslot_selected=${true}`
    // }else if(type === "BTNS"){
    //   filterParam = `&is_timeslot_selected=${false}`
    // }
    // const searchParm = searchValue ? `&term=${searchValue}` : '';
    // console.log(searchValue)
    // try {
    //   const { data } = await axiosInstance.get(
    //     `${endpoints.aolBatch.getStudentTimeSlotStatus}?page=${page}&page_size=${10}${filterParam}${searchParm}`
    //   );
    //   if (data?.status_code === 200) {
    //     console.log(data)
    //     setStudentList(data);
    //     setLoading(false);
    //   } else {
    //     setLoading(false);
    //     setAlert('error', data?.message);
    //   }
    // } catch (error) {
    //   setLoading(false);
    //   setAlert('error', error?.message);
    // }
    setCurrentFilterType(type);
    if (type === "BTSUa") {
      const searchParm = searchValue ? `&term=${searchValue}` : '';
      window.location = `/student-batch-timeslot/?page=${page}${searchParm}`;
    }
    else {
      const searchParm = searchValue ? `&term=${searchValue}` : '';
      let filterParam = '';
      if (type === "BTS") {
        filterParam = `&is_timeslot_selected=${true}`
      } else if (type === "BTNS") {
        filterParam = `&is_timeslot_selected=${false}`
      }
      window.location = `/student-batch-timeslot/?page=${page}${searchParm}${filterParam}`;
    }
  }

  useEffect(() => {
    const quiryDetails = new URLSearchParams(window.location.search);
    setPage(parseInt(quiryDetails.get('page'), 10) || 1);
    const searchVal = quiryDetails.get('term') || '';
    setSearchValue(searchVal);
    const filterParam = quiryDetails.get('is_timeslot_selected') || null;
    setCurrentFilterType(filterParam !== null? filterParam === 'true' ? 'BTS' : 'BTNS' : 'BTSUa')
    if (filterParam !== null) {
      getStudentListFilter(parseInt(quiryDetails.get('page'), 10) || 1, searchVal, filterParam === true ? 'BTS' : 'BTNS');
    }
    else {
      getStudentList(parseInt(quiryDetails.get('page'), 10) || 1, searchVal);
    }
  }, []);

  const handlePagination = (event, page) => {
    setPage(page);
    const searchParm = searchValue ? `&term=${searchValue}` : '';
    if(currentFilterType === 'BTSUa') {
      window.location = `/student-batch-timeslot/?page=${page}${searchParm}`
    }
    else {
      let filterParam = '';
      if(currentFilterType === 'BTS') {
        filterParam = `&is_timeslot_selected=${true}`
      }
      else {
        filterParam = `&is_timeslot_selected=${false}`
      }
      window.location = `/student-batch-timeslot/?page=${page}${searchParm}${filterParam}`
    }
  };

  function handleAssign(data) {
    history.push({
      pathname: `/aol-batch-assign`,
      state: {
        ...data,
      },
    });
  }

  const debounceFunc = useRef(
    debounce((q, ftype) => {
      if (ftype === "BTSUa") {
        setPage(1);
        const searchParm = q ? `&term=${q}` : '';
        history.push(`/student-batch-timeslot/?page=${1}${searchParm}`);
        getStudentList(1, q);
      }
      else {
        console.log(ftype)
        setPage(1);
        const searchParm = q ? `&term=${q}` : '';
        let filterParam = '';
        if (ftype === "BTS") {
          filterParam = `&is_timeslot_selected=${true}`
        } else if (ftype === "BTNS") {
          filterParam = `&is_timeslot_selected=${false}`
        }
        history.push(`/student-batch-timeslot/?page=${1}${searchParm}${filterParam}`);
        getStudentListFilter(1, q, ftype);
      }
    }, 1000)
  ).current;

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Student Batch Timeslot'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item md={4} xs={12}>
              <CustomSearchBar
                value={searchValue}
                onChange={(e) => {
                  setSearchValue(e.target.value.trimLeft());
                  debounceFunc(e.target.value.trimLeft(), currentFilterType);
                }}
                label=''
                placeholder='Search Student Name'
                setValue={(value) => {
                  setSearchValue(value);
                  // debounceFunc(value, currentFilterType);
                }}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          {/* <Button variant='contained' color='primary' onClick={()=>{ filterbatch("BTS") }}> Batch Time Selected</Button>{' '} */}
          <Button variant='contained' color='primary' onClick={() => { filterbatch(1, searchValue, "BTNS") }}> Batch Time Not Selected</Button>{' '}
          <Button variant='contained' color='primary' onClick={() => { filterbatch(1, searchValue, "BTSUa") }}> Batch Time Selected and UnAssigned</Button>{' '}
        </Grid>
        <Grid item md={12} xs={12}>
          <AolViewAllBatches
            studentData={studentList?.result || studentList?.results || []}
            currentFilterType={currentFilterType}
            assign={handleAssign}
            page={page}
          />
          {studentList?.result?.length || studentList?.results?.length ? (
            <Paper style={{ textAlign: 'center', padding: '10px' }}>
              <Pagination
                style={{ textAlign: 'center', display: 'inline-flex' }}
                onChange={handlePagination}
                count={studentList?.total_pages}
                color='primary'
                page={page}
              />
            </Paper>
          ) : (
            ''
          )}
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default AolCreateBatch;
