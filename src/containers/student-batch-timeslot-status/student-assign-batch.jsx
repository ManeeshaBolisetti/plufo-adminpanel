import React, { useContext, useState, useEffect } from 'react';
import {
  Card,
  Grid,
  Typography,
  Button,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import CustomFiterImage from '../../components/custom-filter-image';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
// import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import Loader from '../../components/loader/loader';
import StudentOnGoingBatches from './student-ongoing-batches';
import CreateStudentBatchModel from './create-student-batch-model';
import ConfirmDialog from '../../components/confirm-dialog';
import StudentAssignModel from './student-assign-model';
import { DateTimeConverter, ConverTime } from '../../components/dateTimeConverter';
import './style.scss';

const StudentAssignBatch = () => {
  const location = useLocation();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [userDetails, setUserDetails] = useState([]);
  const [studentBatchDetails, setStudentBatchDetails] = useState({});
  const [open, setOpen] = useState(false);
  const [batchList, setBatchList] = useState([]);
  const { setAlert } = useContext(AlertNotificationContext);
  const [openCreate, setOpenCreate] = useState(false);
  const [openAssign, setOpenAssign] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  const batchViewCard = (value) => {
    return (
      <Grid item md={3} xs={12}>
        <Card
          hovable
          style={{
            background: '#FFD9D9',
            padding: '8px',
            borderTop: '5px solid #CECECE',
            borderBottom: '5px solid #CECECE',
            textAlign: 'center',
          }}
        >
          <Typography>{value}</Typography>
        </Card>
      </Grid>
    );
  };

  async function apiCall(API, KEY) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(API);
      if (data?.status_code === 200) {
        setLoading(false);
        if (KEY === 'batchList') {
          setUserDetails(data);
        }
        if (KEY === 'batches') {
          setBatchList(data);
          setOpen(true);
        }
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error.message);
    }
  }

  useEffect(() => {
    setStudentBatchDetails(location.state);
    apiCall(
      `${endpoints.aolBatch.getRunningBatchList}?course=${location?.state?.course_id}&course_detail_id=${location?.state?.course_details_id}`,
      'batchList'
    );
  }, [location?.state]);

  const TitleCards = (label) => {
    return (
      <Card
        style={{
          width: 'fit-content',
          background: '#707070',
          padding: '8px 20px',
          borderTop: '5px solid #CECECE',
          borderBottom: '5px solid #CECECE',
          textAlign: 'center',
        }}
      >
        <Typography style={{ color: 'white' }}>{label}</Typography>
      </Card>
    );
  };

  function handleAssign(data) {
    setOpenAssign(true);
    setSelectedItem(data);
  }

  async function handleSubmitAssign(info) {
    setLoading(true);
    const payload = {
      ...info,
      batch_id: selectedItem?.id,
    };
    try {
      const { data } = await axiosInstance.put(endpoints.aolBatch.assignBatch, {
        ...payload,
      });
      if (data?.status_code === 200) {
        setLoading(false);
        setOpenAssign(false);
        setSelectedItem('');
        history.goBack();
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
  }

  return (
    <Layout>
      <div style={{ width: '95%', margin: '20px auto' }}>
        <CommonBreadcrumbs
          componentName='Master Management'
          childComponentName='Unassigned Students'
          childComponentNameNext='Assign Batch'
        />
      </div>
      <Grid container spacing={2} style={{ width: '95%', margin: '20px auto' }}>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={12}>
              <Grid container spacing={2} direction='row'>
                {TitleCards(studentBatchDetails?.name || '')}
                &nbsp;&nbsp;&nbsp;
                {TitleCards(
                  studentBatchDetails?.is_fixed ? 'Yearly Course' : 'Fixed Course' || ''
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={12} xs={6}>
              <Grid container spacing={2}>
                {batchViewCard('Batch Size')}
                {batchViewCard('No of Days')}
                {batchViewCard('Time Slot')}
                {batchViewCard('Start Date')}
              </Grid>
            </Grid>
            <Grid item md={12} xs={6}>
              <Grid container spacing={2}>
                {batchViewCard(`1 : ${studentBatchDetails?.batch_size}`)}
                {batchViewCard(studentBatchDetails?.days?.[0] || '')}
                {batchViewCard(
                  studentBatchDetails?.batch_time_slot
                    ? `${ConverTime(
                        studentBatchDetails?.batch_time_slot?.split('-')[0]
                      )} - ${ConverTime(
                        studentBatchDetails?.batch_time_slot?.split('-')[1]
                      )}`
                    : '' || ''
                )}
                {batchViewCard(
                  studentBatchDetails?.reference_start_date
                    ? new Date(studentBatchDetails?.reference_start_date)
                        .toString()
                        .split('G')[0]
                        .substring(0, 16)
                    : ''
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item md={3} xs={12} style={{ textAlign: 'center' }}>
              <Button
                style={{ borderRadius: '10px' }}
                variant='contained'
                className='action-btn'
                color='primary'
                onClick={() => {
                  apiCall(
                    `${endpoints.aolBatch.getRunningStudentBatchList}?student=${studentBatchDetails?.student_id}`,
                    'batches'
                  );
                }}
              >
                Assigned Batches
              </Button>
            </Grid>
            <Grid item md={3} xs={12} style={{ textAlign: 'center' }}>
              <Button
                style={{ borderRadius: '10px' }}
                className='action-btn'
                variant='contained'
                color='primary'
                onClick={() => setOpenCreate(true)}
              >
                Create New
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px', borderRadius: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12} style={{ width: '100%', overflow: 'auto' }}>
                <Table style={{ width: '100%', overflow: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell align='left'>Batch Code</TableCell>
                      <TableCell align='left'>Days</TableCell>
                      <TableCell align='left'>Seats Left</TableCell>
                      <TableCell align='left'>Date Started</TableCell>
                      <TableCell align='left'>Teacher Name</TableCell>
                      <TableCell align='left'>Assign</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {userDetails?.result?.length !== 0 ? (
                      userDetails?.result?.map((item) => (
                        <TableRow key={item.id}>
                          <TableCell align='left'>{item?.batch_name || ''}</TableCell>
                          <TableCell align='left'>
                            {item?.batch_days?.[0] || ''}
                          </TableCell>
                          <TableCell align='left'>{item?.seat_left || ''}</TableCell>
                          <TableCell align='left'>
                            {item?.start_date ? DateTimeConverter(item?.start_date) : ''}
                          </TableCell>
                          <TableCell align='left'>{item?.teacher__name || ''}</TableCell>
                          <TableCell align='left'>
                            <Button
                              variant='contained'
                              size='small'
                              color='primary'
                              onClick={() => handleAssign(item)}
                            >
                              Assign
                            </Button>
                          </TableCell>
                        </TableRow>
                      ))
                    ) : (
                      <TableRow>
                        <TableCell colspan='5'>
                          <CustomFiterImage label='Batches are not found' />
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item md={12} xs={12}>
          <Button
            onClick={() => history.goBack()}
            variant='contained'
            color='secondary'
            size='small'
          >
            Back
          </Button>
        </Grid>
      </Grid>
      {loading ? <Loader /> : ''}
      {open ? (
        <StudentOnGoingBatches
          open={open}
          close={() => {
            setOpen(false);
            setBatchList([]);
          }}
          data={batchList?.result || []}
        />
      ) : (
        ''
      )}
      {openCreate ? (
        <CreateStudentBatchModel
          open={openCreate}
          close={(info) => {
            setOpenCreate(false);
            if (info === 'success') {
              history.goBack();
            }
          }}
          selectedData={studentBatchDetails || {}}
        />
      ) : (
        ''
      )}
      {openAssign && location?.state?.is_fixed ? (
        <StudentAssignModel
          close={() => {
            setOpenAssign(false);
            setSelectedItem('');
          }}
          open={openAssign}
          handleSubmitAssign={handleSubmitAssign}
          selectedData={studentBatchDetails || {}}
        />
      ) : (
        ''
      )}
      {openAssign && location?.state?.is_fixed === false ? (
        <ConfirmDialog
          open={openAssign}
          cancel={() => {
            setOpenAssign(false);
            setSelectedItem('');
          }}
          selectedData={studentBatchDetails || {}}
          confirm={() => {
            handleSubmitAssign({
              student_id: studentBatchDetails?.student_id,
              order_id: studentBatchDetails?.id,
              start_date: null,
            });
          }}
          title='Are you sure to assign?'
        />
      ) : (
        ''
      )}
    </Layout>
  );
};

export default StudentAssignBatch;
