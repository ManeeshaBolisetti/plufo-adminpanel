import React, { useState, useContext } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import CustomDialog from '../../components/custom-dialog';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';

const StudentAssignModel = ({ open, close, selectedData, handleSubmitAssign }) => {
  const [startDate, setStartDate] = useState('');
  const { setAlert } = useContext(AlertNotificationContext);

  async function handleSubmit() {
    if (!startDate) {
      setAlert('warning', 'Select Start Date');
      return;
    }
    const payload = {
      student_id: selectedData?.student_id,
      order_id: selectedData?.id,
      start_date: startDate,
    };
    handleSubmitAssign(payload);
  }

  return (
    <>
      <CustomDialog
        handleClose={close}
        open={open}
        dialogTitle='Assign'
        maxWidth='xs'
        fullScreen={false}
        stylesProps={{ zIndex: '1' }}
      >
        <Grid container spacing={3} style={{ padding: '10px' }}>
          <Grid item md={12} xs={12}>
            <Typography>{`Days : ${selectedData?.days?.[0]}`}</Typography>
          </Grid>
          <Grid item md={12} xs={12}>
            <MuiPickersUtilsProvider
              variant='outlined'
              utils={DateFnsUtils}
              style={{ zIndex: '9000000000' }}
            >
              <KeyboardDatePicker
                fullWidth
                autoOk
                disablePast
                placeholder='Start Date'
                helperText='Select Start Date'
                value={startDate || ''}
                onChange={(data, value) => setStartDate(value)}
                onError={console.log}
                variant='outlined'
                minDate={new Date('2018-01-01')}
                format='yyyy-MM-dd'
              />
            </MuiPickersUtilsProvider>
          </Grid>
          <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
            <Button variant='contained' color='primary' onClick={() => handleSubmit()}>
              Submit
            </Button>
          </Grid>
        </Grid>
      </CustomDialog>
    </>
  );
};

export default StudentAssignModel;
