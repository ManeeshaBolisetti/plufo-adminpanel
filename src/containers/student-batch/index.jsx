/* eslint-disable react/no-array-index-key */
import React, { useState, useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Grid, Card, Typography, Divider, IconButton } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import EditIcon from '@material-ui/icons/Edit';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Loader from '../../components/loader/loader';
import Layout from '../Layout';
import filterImage from '../../assets/images/unfiltered.svg';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import ClassdetailsCard from '../online-class/aol-view/ClassdetailCard';
import ClassCard from '../online-class/aol-view/ClassCard';
import LockOpenIcon from '@material-ui/icons/LockOpen';

const StudentBatchView = (props) => {
  const history = useHistory();
  const { match } = props;
  const { setAlert } = useContext(AlertNotificationContext);
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState(1);
  const [toggle, setToggle] = useState(true);
  const [selected, setSelected] = React.useState();
  const [classData, setClassData] = React.useState();
  const [size, setSize] = React.useState(12);
  const [batchWiseGradeId, setBatchWiseGradeId] = useState('');
  const [userDetails, setUserDetails] = useState('');
  const [classesData, setClassesData] = useState([]);
  const [toggledData, setToggledData] = useState([]);
  const [itemSize, setItemSize] = React.useState(4);
  const [reload, setReload] = useState(false);
  const [filterData] = useState({
    branch: '',
    grade: '',
    course: '',
    batch: '',
  });
  const bearerToken = JSON.parse(localStorage.getItem('userDetails'));

  async function getApiCall(url, key) {
    try {
      const { data } = await axiosInstance.get(url);
      if (data.status_code === 200) {
        if (key === 'user') {
          setUserDetails(data.data);
        }
      } else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
    }
  }

  useEffect(() => {
    if (match.params.userId) {
      getApiCall(
        `${endpoints.gloabSearch.singleUser}${match.params.userId}/global-search-user/`,
        'user'
      );
    }
  }, [match.params.userId]);

  const hendleCloseDetails = () => {
    setItemSize(4);
    setSelected(0);
    setSize(12);
    setClassData('');
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
    setToggle(!toggle);
    hendleCloseDetails('');
  };

  const handleSelctedClass = (data) => {
    setItemSize(6);
    setSize(8);
    setClassData(data);
    setSelected(data.id);
    setLoading(true);
    axiosInstance
      .get(
        `${endpoints.aol.batchWiseGrade}?id=${
          toggle ? data?.id : data?.online_class?.aol_batch_id
        }`
      )
      .then((result) => {
        setLoading(false);
        setBatchWiseGradeId(result?.data?.result[0].id);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  };

  const getClasses = () => {
    if (toggle) {
      setClassesData([]);
      setLoading(true);
      axiosInstance
        .get(
          `${endpoints.aol.studentBatches}?user_id=${match.params.userId}&page=1&page_size=100`
        )
        .then((result) => {
          setLoading(false);
          setToggledData(result.data.result);
        })
        .catch((error) => {
          setLoading(false);
          console.log(error);
        });
    } else {
      setLoading(true);
      setToggledData([]);
      axiosInstance
        .get(
          `${endpoints.aol.studentBatches}?user_id=${match?.params?.userId}&started=True&page=1&page_size=100`
        )
        .then((result) => {
          setLoading(false);
          setClassesData(result?.data?.data);
        })
        .catch((error) => {
          setLoading(false);
          console.log(error);
        });
    }
  };

  useEffect(() => {
    getClasses();
  }, [toggle, match.params.userId, reload]);

  function handleUpdate() {
    history.push(`/user-management/edit-user/${match.params.userId}`);
    if (history.location.pathname.includes('/edit-user/')) {
      window.location.reload();
    }
  }

  const handleResetPassword = () => {
    let requestData = {
      erp_id: userDetails?.erp_id,
    };
    axiosInstance
      .post(`${endpoints.aol.resetPassword}`, requestData)
      .then((result) => {
        setLoading(false);
        if (result.data.status_code === 200) {
          setAlert('success', result.data.Message);
        } else setAlert('error', result.data.Message);
      })
      .catch((error) => {
        setAlert('error', error.Message);
      });
  };

  return (
    <Layout>
      <Grid
        container
        spacing={2}
        style={{ padding: '10px', width: '100%', overflow: 'hidden' }}
      >
        <Grid item md={12} xs={12}>
          <CommonBreadcrumbs componentName='Student Batch View' />
        </Grid>
        <Grid item md={12} xs={12}>
          <Card style={{ padding: '10px' }}>
            <Grid container spacing={2}>
              <Grid item md={11} xs={12}>
                <Grid container spacing={2}>
                  <Grid item md={4} xs={12}>
                    <Typography color='secondary'>
                      Name: &nbsp;
                      {userDetails?.name}
                    </Typography>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <Typography color='secondary'>
                      Phone Number: &nbsp;
                      {userDetails?.contact}
                    </Typography>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <Typography color='secondary'>
                      ERP ID: &nbsp;
                      {userDetails?.erp_id}
                    </Typography>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <Typography color='secondary'>
                      Email ID: &nbsp;
                      {userDetails?.user?.email}
                    </Typography>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <Typography color='secondary'>
                      Address: &nbsp;
                      {userDetails?.address}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item md={1} xs={1} style={{ textAlign: 'right' }}>
                <IconButton
                  size='small'
                  color='primary'
                  className='edit_icon_global_user_details'
                  onClick={() => handleUpdate()}
                >
                  <EditIcon />
                </IconButton>
                <br />
                <IconButton
                  style={{ fontSize: '20px' }}
                  size='small'
                  color='primary'
                  className='edit_icon_global_user_details'
                  onClick={() => handleResetPassword()}
                  title='Reset Password To Default'
                >
                  {/* Reset Password */}

                  <LockOpenIcon />
                  {/* <EditIcon /> */}
                </IconButton>
              </Grid>
              {/* <Grid item md={12} xs={1} style={{ textAlign: 'right' }}>
                <IconButton
                  size='small'
                  color='primary'
                  className='edit_icon_global_user_details'
                  onClick={() => handleUpdate()}
                >
                  Reset Password
                  <LockOpenIcon />
                  <EditIcon />
                </IconButton>
              </Grid> */}
            </Grid>
          </Card>
        </Grid>
        <Grid item md={12} xs={12}>
          <Divider />
        </Grid>
        <Grid item md={12} xs={12}>
          <AppBar position='static' color='default'>
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor='primary'
              textColor='primary'
              variant='scrollable'
              scrollButtons='auto'
              aria-label='scrollable auto tabs example'
            >
              <Tab label='Yet to Start' value={1} />
              <Tab label='Started' value={2} />
            </Tabs>
          </AppBar>
        </Grid>
        <Grid item md={12} xs={12}>
          <Grid container spacing={2}>
            <Grid item sm={size} xs={12}>
              <Grid container spacing={3}>
                {classesData?.length === 0 && toggledData?.length === 0 && (
                  <Grid item xs={12} style={{ textAlign: 'center', marginTop: '10px' }}>
                    <img src={filterImage} alt='crash' height='250px' width='250px' />
                    <Typography>Records Not Found</Typography>
                  </Grid>
                )}
                {classesData?.length > 0 &&
                  classesData.map((data, id) => {
                    return (
                      <Grid item sm={itemSize} xs={12} key={id}>
                        <ClassCard
                          classData={data}
                          selectedId={selected}
                          handleSelctedClass={handleSelctedClass}
                          toggle={toggle}
                          batchSize={parseInt(data.batch_size, 10)}
                        />
                      </Grid>
                    );
                  })}
                {toggledData?.length > 0 &&
                  toggledData.map((data, id) => {
                    return (
                      <Grid item sm={itemSize} xs={12} key={id}>
                        <ClassCard
                          classData={data}
                          selectedId={selected}
                          handleSelctedClass={handleSelctedClass}
                          toggle={toggle}
                        />
                      </Grid>
                    );
                  })}
              </Grid>
            </Grid>
            {classData && (
              <Grid
                item
                sm={4}
                xs={12}
                className='viewMoreContainer'
                style={{ padding: '20px' }}
              >
                <ClassdetailsCard
                  classData={classData}
                  filterData={filterData}
                  toggle={toggle}
                  reload={reload}
                  setReload={setReload}
                  hendleCloseDetails={hendleCloseDetails}
                  getClasses={getClasses}
                  batchWiseGradeId={batchWiseGradeId}
                />
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
      {loading && <Loader />}
    </Layout>
  );
};

export default StudentBatchView;
