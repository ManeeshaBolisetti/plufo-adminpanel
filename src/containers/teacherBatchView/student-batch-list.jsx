import React, { useState, useContext, useEffect } from 'react';
import { Grid, TextField, Button, Divider, Typography } from '@material-ui/core';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Card,
  IconButton,
} from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import CustomDialog from '../../components/custom-dialog';
import CustomFiterImage from '../../components/custom-filter-image';





export default function StudentBatchList({ content, open, close }){

  return(
    <Grid container spacing={2}>
      <Grid alignItems='center' justifyContent='center' item md={12} xs={12}>
        <CustomDialog
          handleClose={close}
          open={open}
          dialogTitle='Student List'
          maxWidth='sm'
          fullScreen={false}
          stylesProps={{ zIndex: '1', marginTop: 50 }}
        >
          <Table style={{ width: '100%', overflow: 'auto' }}>
            <TableHead>
              <TableRow>
                <TableCell align='left'>S.No</TableCell>
                <TableCell align='left'>ERP ID</TableCell>
                <TableCell align='left'>Student Name</TableCell>
                <TableCell align='left'>Enrollment Number</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {content && content.length != 0 ? (
                <>
                  {content?.map((item, i) => (
                    <TableRow key={item.id}>
                      <TableCell align='left'>{i + 1}</TableCell>
                      <TableCell>{item?.user__erp_id}</TableCell>
                      <TableCell>{item?.user__name}</TableCell>
                      <TableCell>{item?.enroll.map((val) => {
                        if (val && val.length > 0) {
                          if (!val) {
                            return ""
                          } else {
                            return `${val} `
                          }
                        }
                        else {
                          return (
                            <Typography style={{ color: 'red', fontFamily: 'serif', fontWeight: 600 }}>NA</Typography>
                          )

                        }
                      }) || 'NA'} </TableCell>
                    </TableRow>
                  ))}
                </>
              ) : (
                <TableRow>
                  <TableCell colSpan='9'>
                    <CustomFiterImage label='List Not Found' />
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </CustomDialog>
      </Grid>
    </Grid>

  );
}