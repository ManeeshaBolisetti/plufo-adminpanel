// /* eslint-disable react-hooks/exhaustive-deps */
// import React, { useContext, useState, useEffect } from 'react';
// import { Grid, Card, Divider, Button, Popover, Typography } from '@material-ui/core';
// import { useHistory } from 'react-router-dom'
// import CloseIcon from '@material-ui/icons/Close';
// import moment from 'moment';
// import axiosInstance from '../../config/axios';
// import endpoints from '../../config/endpoints';
// import Loader from '../../components/loader/loader';
// import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
// import ResourceDialog from '../online-class/online-class-resources/resourceDialog';
// import './styles.css'

// const JoinClass = (props) => {
//   const fullData = props.fullData;
//   const handleClose = props.handleClose;
//   const [loading, setLoading] = useState(false);
//   const { setAlert } = useContext(AlertNotificationContext);
//   const [anchorEl, setAnchorEl] = useState(null);
//   const [joinAnchor, setJoinAnchor] = useState(null);
//   const [isAccept, setIsAccept] = useState(props.data ? props.data.is_accepted : false);
//   const [isRejected, setIsRejected] = useState(props.data ? props.data.is_restricted : false);
//   const history = useHistory()

//   const classStartTime = moment(props && props?.data && props?.data?.date).format(
//     'DD-MM-YYYY'
//   );
//   const currDate = moment(new Date()).format('DD-MM-YYYY');

//   const startTime = props && props?.data?.start_time;
//   const currTime = moment(new Date()).format('x');
//   const classTimeMilli = new Date(`${props.data.date}T${startTime}`).getTime();
//   const diffTime = classTimeMilli - 5 * 60 * 1000;

//   const handleCloseData = () => {
//     setAnchorEl(null);
//   };

//   const handleClick = (event) => {
//     setAnchorEl(event.currentTarget);
//   };
//   const handleCloseAccept = () => {
//     setJoinAnchor(null);
//   };
//   const handleClickAccept = (event) => {
//     if (diffTime > parseInt(currTime)) {
//       setJoinAnchor(event.currentTarget);
//     } else if (parseInt(currTime) > diffTime || parseInt(currTime) === diffTime) {
//       handleIsAccept();
//     }
//   };

//   const handleIsAccept = () => {
//     const params = {
//       zoom_meeting_id: fullData && fullData.id,
//       class_date: props.data && props.data.date,
//       is_accepted: true
//     };
//     axiosInstance
//       .put(endpoints.studentViewBatchesApi.rejetBatchApi, params)
//       .then((res) => {
//         setLoading(false);
//         setIsAccept(true);
//       })
//       .catch((error) => {
//         setLoading(false);
//         setAlert('error', error.message);
//       });
//   }

//   const handleJoin = async () => {
//     const params = {
//       zoom_meeting_id: fullData && fullData.id,
//       class_date: props.data && props.data.date,
//       is_attended: true
//     };
//     await axiosInstance
//       .put(endpoints.studentViewBatchesApi.rejetBatchApi, params)
//       .then((res) => {
//         setLoading(false);
//         setIsAccept(true);
//       })
//       .catch((error) => {
//         setLoading(false);
//         setAlert('error', error.message);
//       });
//     window.open(fullData && fullData.join_url)

//   }

//   function handleCancel() {
//     setLoading(true);
//     const params1 = {
//       zoom_meeting_id: fullData && fullData.id,
//       class_date: props.data && props.data.date
//     };

//     const params2 = {
//       zoom_meeting_id: fullData && fullData.id,
//       class_date: props.data && props.data.date,
//       is_restricted: true
//     };

//     if (window.location.pathname === '/online-class/attend-class') {
//       //url = endpoints.studentViewBatchesApi.rejetBatchApi;
//       axiosInstance
//         .put(endpoints.studentViewBatchesApi.rejetBatchApi, params2)
//         .then((res) => {
//           setLoading(false);
//           setAlert('success', 'Class Rejected');
//           handleClose('success');
//         })
//         .catch((error) => {
//           setLoading(false);
//           setAlert('error', error.message);
//         });
//     } else {
//       //url = endpoints.teacherViewBatches.cancelBatchApi;
//       axiosInstance
//         .put(endpoints.teacherViewBatches.cancelBatchApi, params1)
//         .then((res) => {
//           setLoading(false);
//           setAlert('success', res.data.message);
//           handleClose('success');
//         })
//         .catch((error) => {
//           setLoading(false);
//           setAlert('error', error.message);
//         });
//     }
//   }

//   const open = Boolean(anchorEl);
//   const id = open ? 'simple-popover' : undefined;
//   const openJoin = Boolean(joinAnchor);
//   const ids = open ? 'accept-popover' : undefined;

//   return (
//     <Grid container spacing={2} direction='row' alignItems='center'>
//       <Grid item md={6} xs={12}>
//         <span className='TeacherFullViewdescreption1'>
//           {moment(props.data ? props.data.date : '').format('DD-MM-YYYY')}
//         </span>
//       </Grid>
//       {isAccept ? (
//         <Grid item xs={6}>
//           <Button
//             size='small'
//             color='secondary'
//             fullWidth
//             variant='contained'
//             onClick={handleJoin}
//             className='teacherFullViewSmallButtons'
//           >
//             Join
//           </Button>
//         </Grid>
//       ) : (
//         <>
//           {isRejected ? (
//             <Grid item xs={6}>
//               <Typography style={{ color: '#ff6b6b' }}>Rejected</Typography>
//             </Grid>
//           ) : (
//             <>
//               <Grid item md={3} xs={6}>
//                 {window.location.pathname === '/online-class/attend-class' ? (
//                   <>
//                     <Popover
//                       id={ids}
//                       open={openJoin}
//                       joinAnchor={joinAnchor}
//                       onClose={handleClose}
//                       style={{ overflow: 'hidden', margin: '19% 0 0 30%' }}
//                       className='xyz'
//                       anchorOrigin={{
//                         vertical: 'top',
//                         horizontal: 'center',
//                       }}
//                       transformOrigin={{
//                         vertical: 'top',
//                         horizontal: 'center',
//                       }}
//                     >
//                       <Grid
//                         container
//                         spacing={2}
//                         style={{ textAlign: 'center', padding: '10px' }}
//                       >
//                         <Grid item xs={12} style={{ textAlign: 'right' }}>
//                           <CloseIcon
//                             style={{ color: '#014B7E' }}
//                             onClick={() => handleCloseAccept()}
//                           />
//                         </Grid>
//                         <Grid item md={12} xs={12}>
//                           <Typography>
//                             You Can Join 5mins Before :
//                             {moment(`${props?.data?.date}T${startTime}`).format(
//                             'hh:mm:ss A'
//                           )}
//                           </Typography>
//                         </Grid>
//                       </Grid>
//                     </Popover>
//                     <Button
//                       size='small'
//                       color='secondary'
//                       fullWidth
//                       variant='contained'
//                       onClick={(e) => handleClickAccept(e)}
//                       disabled={classStartTime === currDate ? false : true}
//                       className='teacherFullViewSmallButtons'
//                     >
//                       Accept
//                     </Button>
//                   </>
//                   // <Button
//                   //   size='small'
//                   //   color='secondary'
//                   //   fullWidth
//                   //   variant='contained'
//                   //   onClick={handleIsAccept}
//                   //   className='teacherFullViewSmallButtons'
//                   // >
//                   //   Accept
//                   // </Button>
//                 ) : (
//                   <Button
//                     size='small'
//                     color='secondary'
//                     fullWidth
//                     variant='contained'
//                     onClick={() =>
//                       window.open(fullData && fullData.presenter_url, '_blank'
//                       )}
//                     className='teacherFullViewSmallButtons'
//                   >
//                     Host
//                   </Button>
//                 )}
//               </Grid>
//               <Grid item md={3} xs={6} className='xxx'>
//                 <Popover
//                   className='deletePopover'
//                   id={id}
//                   open={open}
//                   anchorEl={anchorEl}
//                   onClose={handleClose}
//                   style={{ overflow: 'hidden' }}
//                   anchorOrigin={{
//                     vertical: 'top',
//                     horizontal: 'center',
//                   }}
//                   transformOrigin={{
//                     vertical: 'top',
//                     horizontal: 'center',
//                   }}
//                 >
//                   <Grid
//                     container
//                     spacing={2}
//                     style={{ textAlign: 'center', padding: '10px' }}
//                   >
//                     <Grid item md={12} xs={12}>
//                       <Typography>Are you sure you want to cancel?</Typography>
//                     </Grid>
//                     <Grid item md={6} xs={12}>
//                       <Button
//                         variant='contained'
//                         size='small'
//                         style={{ fontSize: '11px' }}
//                         onClick={() => handleCloseData()}
//                       >
//                         Cancel
//                     </Button>
//                     </Grid>
//                     <Grid item md={6} xs={12}>
//                       <Button
//                         variant='contained'
//                         color='primary'
//                         style={{ fontSize: '11px' }}
//                         size='small'
//                         onClick={() => handleCancel()}
//                       >
//                         Confirm
//                     </Button>
//                     </Grid>
//                   </Grid>
//                 </Popover>
//                 <Button
//                   size='small'
//                   fullWidth
//                   variant='contained'
//                   onClick={(e) => handleClick(e)}
//                   className='teacherFullViewSmallButtons1'
//                 >
//                   {window.location.pathname === '/online-class/attend-class'
//                     ? 'Reject'
//                     : 'Cancel'}
//                 </Button>
//               </Grid>
//             </>
//           )}

//         </>
//       )}
//     </Grid>
//   )
// }

// const TeacherBatchFullView = ({ fullData, handleClose, selectedGrade }) => {
//   const [noOfPeriods, setNoOfPeriods] = useState([]);
//   const [loading, setLoading] = useState(false);
//   const { setAlert } = useContext(AlertNotificationContext);
//   const [anchorEl, setAnchorEl] = useState(null);
//   const history = useHistory()
//   const { role_details } = JSON.parse(localStorage.getItem('userDetails'))
//   /*
//   useEffect(() => {
//     if (fullData) {
//       axiosInstance
//         .get(
//           `erp_user/${
//             fullData  && fullData.id
//           }/online-class-details/`
//         )
//         .then((res) => {
//           setNoOfPeriods(res.data.data);
//         })
//         .catch((error) => setAlert('error', error.message));
//     }
//   }, [fullData]);
//   */

//   useEffect(() => {
//     let detailsURL = window.location.pathname === '/online-class/attend-class'
//       ? `erp_user/${fullData && fullData?.id}/student-oc-details/`
//       : `erp_user/${fullData && fullData?.id}/online-class-details/`;

//     if (fullData) {
//       axiosInstance
//         .get(detailsURL)
//         .then((res) => {
//           console.log(res.data);
//           setNoOfPeriods(res.data.data);
//         })
//         .catch((error) => setAlert('error', error.message));
//     }
//   }, [fullData, handleClose]);

//   const handleCloseData = () => {
//     setAnchorEl(null);
//   };

//   const handleClick = (event) => {
//     setAnchorEl(event.currentTarget);
//   };

//   const open = Boolean(anchorEl);
//   const id = open ? 'simple-popover' : undefined;

//   const converTime = (info) => {
//     let hour = info.split(':')[0];
//     let min = info.split(':')[1];
//     const part = hour > 12 ? 'PM' : 'AM';
//     min = `${min}`.length === 1 ? `0${min}` : min;
//     hour = hour > 12 ? hour - 12 : hour;
//     hour = `${hour}`.length === 1 ? `0${hour}` : hour;
//     return `${hour}:${min} ${part}`;
//   };

//   function handleCancel() {
//     setLoading(true);
//     const params = {
//       zoom_meeting_id: fullData && fullData?.id,
//       class_date: fullData && fullData?.online_class?.start_time.split('T')[0],
//     };
//     let url = '';
//     if (window.location.pathname === '/online-class/attend-class') {
//       url = endpoints.studentViewBatchesApi.rejetBatchApi;
//     } else {
//       url = endpoints.teacherViewBatches.cancelBatchApi;
//     }
//     axiosInstance
//       .put(url, params)
//       .then((res) => {
//         setLoading(false);
//         setAlert('success', res.data.message);
//         handleClose('success');
//       })
//       .catch((error) => {
//         setLoading(false);
//         setAlert('error', error.message);
//       });
//   }

//   const handleAttendance = () => {
//     history.push(`/aol-attendance-list/${fullData?.online_class && fullData?.id}`)
//   }
//   // console.log(selectedGrade,'FFFFFFFFFFFFFFFFFFFFFFF')
//   // history.push(`/create/course/${props.filterData && props?.filterData?.course?.id}/${props.filterData && props?.filterData?.grade?.id}`)
//   // sessionStorage.setItem('isAol','isAol');
//   const handleCoursePlan = () => {
//     if (window.location.pathname === '/online-class/attend-class') {
//       history.push(`/create/course/${fullData.online_class && fullData?.online_class?.course_id}/${1}`)
//       sessionStorage.setItem('isAol', 2);
//     } else {
//       history.push(`/create/course/${fullData.online_class && fullData?.online_class?.cource_id}/${1}`)
//       sessionStorage.setItem('isAol', 3);
//     }
//   }

//   const [openPopup, setOpenPopup] = React.useState(false);
//   const [selectedValue, setSelectedValue] = React.useState("");

//   const handleClickOpen = () => {
//     setOpenPopup(true);
//   };

//   const handleClosePopup = (value) => {
//     setOpenPopup(false);
//     setSelectedValue(value);
//   };

//   return (
//     <>
//       <Grid container spacing={2}>
//         <Grid item md={12} xs={12} className='teacherBatchFullViewMainDiv'>
//           <Card className='teacherBatchFullViewMainCard'>
//             <Grid container spacing={2} className='teacherBatchFullViewHeader'>
//               <Grid item xs={12} style={{ textAlign: 'right' }}>
//                 <CloseIcon
//                   style={{ color: '#014B7E' }}
//                   onClick={() => handleClose()}
//                 />
//               </Grid>
//               <Grid item xs={12}>
//                 <Grid container spacing={2}>
//                   <Grid item md={8} xs={8}>
//                     <h4 className='teacherBatchFullCardLable'>
//                       {(fullData &&
//                         fullData.online_class &&
//                         fullData?.online_class?.title) ||
//                         ''}
//                     </h4>
//                     <h4 className='teacherBatchFullCardLable'>
//                       {(fullData &&
//                         fullData.online_class &&
//                         fullData.online_class.subject &&
//                         fullData.online_class.subject.length !== 0 &&
//                         fullData?.online_class?.subject.map((item) => (
//                           <span>
//                             {item?.subject_name || ''}
//                             &nbsp;
//                           </span>
//                         ))) ||
//                         ''}
//                     </h4>
//                   </Grid>
//                   <Grid item md={4} xs={4}>
//                     <h4 className='teacherBatchFullCardLable'>
//                       {(fullData &&
//                         fullData?.join_time &&
//                         converTime(fullData?.join_time.split(' ')[1])) ||
//                         ''}
//                     </h4>
//                     <h4 className='teacherBatchFullCardLable'>
//                       {noOfPeriods && noOfPeriods?.length}
//                       &nbsp;Periods
//                     </h4>
//                   </Grid>
//                 </Grid>
//               </Grid>
//               {/*
//                     <IconButton
//                       size='small'
//                       className='teacherBatchFullViewClose'
//                       onClick={() => handleClose()}
//                     >
//                       <CloseIcon className='teacherBatchFullViewCloseIcon' />
//                     </IconButton>
//                   </Grid> */}

//             </Grid>
//             <Grid container spacing={2}>
//               <Grid item md={12} xs={12}>
//                 <span className='TeacherFullViewdescreption'>Periods Date</span>
//               </Grid>
//               <Grid item md={12} xs={12}>
//                 <Divider className='fullViewDivider' />
//                 {noOfPeriods && noOfPeriods.length === 0 && (
//                   <Typography style={{ color: '#ff6b6b', margin: '10px' }}>
//                     No Record found
//                   </Typography>
//                 )}
//                 {noOfPeriods && noOfPeriods.length > 0 && noOfPeriods.map((data) => <JoinClass data={data} fullData={fullData} handleClose={handleClose} />)}

//                 <Divider className='fullViewDivider' />
//               </Grid>
//               <Grid item md={12} xs={12}>
//                 {window.location.pathname !== '/online-class/attend-class' ? (
//                   <Button fullWidth size='small' className='teacherFullViewFullButtons' onClick={handleAttendance}>
//                     Attendance
//                   </Button>
//                 ) : (
//                   <Button fullWidth size='small' className='teacherFullViewFullButtons' onClick={handleClickOpen}>
//                     Resources
//                   </Button>
//                 )}
//                 <Button fullWidth size='small' className='teacherFullViewFullButtons' onClick={handleCoursePlan}>
//                   View Course Plan
//                 </Button>
//               </Grid>
//             </Grid>
//           </Card>
//         </Grid>
//       </Grid>
//       <ResourceDialog
//         selectedValue={selectedValue}
//         open={openPopup}
//         onClose={handleClosePopup}
//         title={fullData.online_class.title}
//         resourceId={fullData?.id}
//         onlineClassId={fullData.online_class?.id}
//         startDate={fullData.online_class.start_time}
//         endDate={fullData.online_class.end_time}
//       />
//       {loading && <Loader />}
//     </>
//   );
// };

// export default TeacherBatchFullView;

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState, useEffect } from 'react';
import { Grid, Card, Divider, Button, Popover, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loader from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import ResourceDialog from '../online-class/online-class-resources/resourceDialog';
import './styles.css';
import UploadDialogBox from '../online-class/erp-view-class/admin/UploadDialogBox';

const JoinClass = (props) => {
  // const { index, cardIndex, historicalData } = props;
  // console.log('Props:',historicalData)
  const fullData = props.fullData;
  const handleClose = props.handleClose;
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const [joinAnchor, setJoinAnchor] = useState(null);
  const [isAccept, setIsAccept] = useState(props.data ? props.data.is_accepted : false);
  const { token, user_id } = JSON.parse(localStorage.getItem('userDetails')) || {};
  // const { user_id } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const [isRejected, setIsRejected] = useState(
    props.data ? props.data.is_restricted : false
  );
  const history = useHistory();
  const [classWorkDialog, setDialogClassWorkBox] = useState(false);
  const historicalData = true;

  const classStartTimes = new Date(props && props?.data && props?.data?.date)
  const classStartYear = parseInt(classStartTimes.getFullYear());
  const classStartMonth = parseInt(classStartTimes.getMonth() + 1);
  const classStartDay = parseInt(classStartTimes.getDate());
  const classStartTime = classStartTimes.getDate() + "-" + parseInt(classStartTimes.getMonth() + 1) + "-" + classStartTimes.getFullYear()
  const serverStartTimes = new Date(props && props?.data && props?.data?.server_time);
  const serverStartYear = parseInt(serverStartTimes.getFullYear());
  const serverStartMonth = parseInt(serverStartTimes.getMonth() + 1);
  const serverStartDay = parseInt(serverStartTimes.getDate());
  const serverStartTime = serverStartTimes.getDate() + "-" + parseInt(serverStartTimes.getMonth() + 1) + "-" + serverStartTimes.getFullYear()

  var offset = new Date();
  function convertTZ(date, tzString) {
    return new Date(
      (typeof date === 'string' ? new Date(date) : date).toLocaleString('en-US', {
        timeZone: tzString,
      })
    );
  }
  const date = convertTZ(offset, 'Asia/Kolkata');
  const currDate = new Date(date);
  const currDateDay = parseInt(currDate.getDay());
  const currDateMonth = parseInt(currDate.getMonth() + 1);
  const currDateYear = parseInt(currDate.getFullYear());

  const startTime = props && props?.data?.start_time;
  const currTime = moment(new Date()).format('x');
  const classTimeMilli = new Date(`${props.data.date}T${startTime}`).getTime();
  const diffTime = classTimeMilli - 5 * 60 * 1000;
  const diffTenTime = classTimeMilli - 10 * 60 * 1000;
  // const endTime = props && props?.data?.end_time
  const endTime = new Date(`${props.data.date}T${props?.data?.end_time}`).getTime();
  const servTime = new Date(`${props?.data?.server_time}`).getTime();
  const serverTime = new Date(`${props?.data?.server_time}`)
  //console.log(currTime, endTime, '+++++++++++++++++++++++++++')
  const severJHT = props && props?.data?.server_time;
  const serverJHTime = new Date(`${props.data.date}T${severJHT}`).getTime();

  const check = new Date(
    `${props.data.date}T${props?.data?.end_time}`
  ).toLocaleString('en-US', { timeZone: 'Asia/Kolkata' });
  const handleCloseData = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseAccept = () => {
    setJoinAnchor(null);
  };
  const handleClickAccept = (event) => {
    if (diffTime > parseInt(servTime ? servTime : currTime)) {
      setJoinAnchor(event.currentTarget);
    } else if (parseInt(servTime ? servTime : currTime) > diffTime || parseInt(servTime ? servTime : currTime) === diffTime) {
      handleIsAccept();
    }
  };
  const handleClickJoin = (event) => {
    if (diffTime > parseInt(servTime ? servTime : currTime)) {
      setJoinAnchor(event.currentTarget);
    } else if (parseInt(servTime ? servTime : currTime) > diffTime || parseInt(servTime ? servTime : currTime) === diffTime) {
      handleJoin();
    }
  };
  const isClassStartted = () => {
    let disableFlag = false;
    var today = new Date();
    var time = today.getTime();
    if ((serverStartDay ? serverStartDay : currDateDay) === classStartDay && (serverStartMonth ? serverStartMonth : currDateMonth) === classStartMonth && (serverStartYear ? serverStartYear : currDateYear) === classStartYear) {
      if ((servTime ? servTime : time) >= diffTime) {
        disableFlag = false;
      } else {
        disableFlag = true;
      }
    } else {
      disableFlag = true;
    }
    return disableFlag;
  };

  const isClassHosted = () => {
    let disableFlag = false;
    var today = new Date();
    var time = today.getTime();
    if ((serverStartDay ? serverStartDay : currDateDay) === classStartDay && (serverStartMonth ? serverStartMonth : currDateMonth) === classStartMonth && (serverStartYear ? serverStartYear : currDateYear) === classStartYear) {
      if ((servTime ? servTime : time) >= diffTenTime) {
        disableFlag = false;
      } else {
        disableFlag = true;
      }
    } else {
      disableFlag = true;
    }
    return disableFlag;
  };
  const isTeacherClassStartted = () => {
    let disableFlag = false;
    var today = new Date();
    var time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    var sTime = serverTime.getHours() + ':' + serverTime.getMinutes() + ':' + serverTime.getSeconds();
    if ((serverStartDay ? serverStartDay : currDateDay) === classStartDay && (serverStartMonth ? serverStartMonth : currDateMonth) === classStartMonth && (serverStartYear ? serverStartYear : currDateYear) === classStartYear) {
      if ((sTime ? sTime : time) >= startTime) {
        disableFlag = false;
      }
    } else if ((serverStartYear ? serverStartYear : currDateYear) >= classStartYear) {
      if ((serverStartMonth ? serverStartMonth : currDateMonth) >= classStartMonth) {
        if (((serverStartDay ? serverStartDay : currDateDay) < classStartDay) && (serverStartMonth ? serverStartMonth : currDateMonth) <= classStartMonth) {
          disableFlag = true;
        } else {
          disableFlag = false;
        }
      } else {
        disableFlag = true;
      }
    } else {
      disableFlag = true;
    }

    return disableFlag;
  };

  const handleIsAccept = () => {
    const params = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: props.data && props.data.date,
      is_accepted: true,
      student_id: user_id
    };
    axiosInstance
      .put(endpoints.studentViewBatchesApi.rejetBatchApi, params)
      .then((res) => {
        setLoading(false);
        setIsAccept(true);
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  };

  // function handleStudentAttendance(data) {
  //   console.log('data::::', data);
  //   axiosInstance.put(`${endpoints.communication.teacherAttendance}?id=${data?.online_class?.id}`).then((response) => {
  //     console.log('response', response);
  //     if (response.data.status_code === 200) {
  //       setAlert('success', response.data.message);
  //     } else {
  //       setAlert('error', response.data.message);
  //     }
  //   })
  // }


  const handleJoin = () => {
    // handleStudentAttendance(fullData);
    let message = "you're redirecting to Zoom...";
    setAlert('warning', message);
    axiosInstance
      .get(
        `${endpoints.studentViewBatchesApi.studentZoomApi}?id=${fullData.online_class.id}`
      )
      .then((res) => {
        if (res.data && res.data.url) {
          window.open(res.data.url);
        }
        setLoading(false);
        setIsAccept(true);
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  };

  function handleCancel() {
    setLoading(true);
    const params1 = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: props.data && props.data.date,
    };

    const params2 = {
      zoom_meeting_id: fullData && fullData.id,
      class_date: props.data && props.data.date,
      is_restricted: true,
    };

    if (window.location.pathname === '/online-class/attend-class') {
      //url = endpoints.studentViewBatchesApi.rejetBatchApi;
      axiosInstance
        .put(endpoints.studentViewBatchesApi.rejetBatchApi, params2)
        .then((res) => {
          setLoading(false);
          setAlert('success', 'Class Rejected');
          handleClose('success');
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    } else {
      //url = endpoints.teacherViewBatches.cancelBatchApi;
      axiosInstance
        .put(endpoints.teacherViewBatches.cancelBatchApi, params1)
        .then((res) => {
          setLoading(false);
          setAlert('success', res.data.message);
          handleClose('success');
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
  }

  const getClassName = () => {
    let classIndex = `${fullData.class_type}`;

    return [
      `teacherBatchFullViewMainCard${classIndex}`,
      `teacherBatchFullViewHeader${classIndex}`,
      `addTextColor${classIndex}`,
      `darkButtonBackground${classIndex}`,
    ];
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const openJoin = Boolean(joinAnchor);
  const ids = open ? 'accept-popover' : undefined;


  function handleTeacherAttendance(data) {
    axiosInstance.put(`${endpoints.communication.teacherAttendance}?id=${data?.online_class?.id}`).then((response) => {
      if (response.data.status_code === 200) {
        setAlert('success', response.data.message);
      } else {
        setAlert('error', response.data.message);
      }
    })
  }

  function handleHost(data) {
    setLoading(true);
    handleTeacherAttendance(data);
    let message = 'Redirecting to Zoom ...';
    setAlert('warning', message);
    axiosInstance
      .get(`${endpoints.teacherViewBatches.hostApi}?id=${data.id}`)
      .then((res) => {
        setLoading(false);
        if (res?.data?.url) {
          window.open(res?.data?.url, '_blank');
        } else {
          setAlert('error', res?.data?.message);
        }
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  const handleTakeQuiz = (fullData) => {
    if (fullData && fullData.online_class && fullData.online_class.question_paper_id) {
      history.push({
        pathname: `/erp-online-class/${fullData.online_class.id}/${fullData.online_class.question_paper_id}/pre-quiz`,
        state: { data: fullData.online_class.id },
      });
    } else {
      setAlert('error', 'This onlineclass does not have quiz associated with it.');
      return;
    }
  };
  const handleOpenClassWorkDialogBox = (value) => {
    setDialogClassWorkBox(value);
  };

  return (
    <Grid container spacing={2} direction='row' alignItems='center'>
      <Grid item md={5} xs={12} style={{maxWidth:'30%'}}>
        <span className='TeacherFullViewdescreption1'>
          {moment(props.data ? props.data.date : '').format('DD-MM-YYYY')}
        </span>
      </Grid>
      {window.location.pathname === '/online-class/attend-class' ? (
        <>
          {/* <Grid item xs={4}>
            <Button
              size='small'
              color='secondary'
              fullWidth
              variant='contained'
              onClick={() => handleTakeQuiz(fullData)}
              disabled={props?.data?.class_status?.toLowerCase() === 'cancelled' || fullData?.online_class?.question_paper_id===0}
              // className='takeQuizButton'
              className={`teacherFullViewSmallButtons1 ${getClassName()[1]}`}
            >
              Take Quiz
            </Button>
          </Grid> */}

          <Grid item md={4} xs={4}>
            <Button
              size='small'
              fullWidth
              variant='contained'
              onClick={() => {
                setDialogClassWorkBox(true);
              }}
              disabled={
                isClassStartted() ||
                props?.data?.class_status?.toLowerCase() === 'cancelled' ||
                props?.data?.erp_online_class_id === null ||
                props?.data?.erp_zoom_id === null ||
                props?.data?.erp_online_class_id === undefined ||
                props?.data?.erp_zoom_id === undefined
              }
              className={`teacherFullViewSmallButtons ${getClassName()[1]}`}
            >
              Class Work
            </Button>
            {classWorkDialog && (
              <UploadDialogBox
                historicalData={historicalData}
                periodData={props?.data}
                setLoading={setLoading}
                fullData={fullData}
                classWorkDialog={classWorkDialog}
                OpenDialogBox={handleOpenClassWorkDialogBox}
              />
            )}
          </Grid>
        </>
      ) : (
        ''
      )}
      {window.location.pathname === '/online-class/teacher-view-class' ? (
        <>
          <Grid item md={4} xs={4}>
            <Button
            style={{maxWidth:'25%'}}
              size='small'
              color='#344ADE'
              fullWidth
              variant='contained'
              onClick={() => {
                const { id = '', online_class = {} } = fullData || {};
                const { id: onlineClassId = '', start_time = '' } = online_class || {};

                history.push({
                  pathname: `/erp-online-class/class-work/${props.data?.erp_online_class_id}/${props.data?.erp_zoom_id}/${props.data?.date}`,
                  state: { historicalData: historicalData },
                });
              }}
              disabled={
                isTeacherClassStartted() ||
                props?.data?.is_cancelled ||
                props?.data?.erp_online_class_id === null ||
                props?.data?.erp_zoom_id === null ||
                props?.data?.erp_online_class_id === undefined ||
                props?.data?.erp_zoom_id === undefined

              }
              className={`teacherFullViewSmallButtons ${getClassName()[1]}`}
            >
              Class Work
            </Button>
          </Grid>
        </>
      ) : (
        ''
      )}
      {isAccept ? (
        <Grid item md={3} xs={6}>
          <>
            <Popover
              id={ids}
              open={openJoin}
              joinAnchor={joinAnchor}
              onClose={handleClose}
              style={{ overflow: 'hidden', margin: '19% 0 0 30%' }}
              className='xyz'
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <Grid
                container
                spacing={2}
                style={{ textAlign: 'center', padding: '10px' }}
              >
                <Grid item xs={12} style={{ textAlign: 'right' }}>
                  <CloseIcon
                    style={{ color: '#014B7E' }}
                    onClick={() => handleCloseAccept()}
                  />
                </Grid>
                <Grid item md={12} xs={12}>
                  <Typography>
                    You Can Join Before 5 mins from the meeting time :
                    {moment(`${props?.data?.date}T${startTime}`).format('hh:mm:ss A')}
                  </Typography>
                </Grid>
              </Grid>
            </Popover>
            <Button
              size='small'
              color='secondary'
              fullWidth
              variant='contained'
              onClick={(e) => handleClickJoin(e)}
              // disabled={classStartTime === (serverStartTime ? serverStartTime : currDate) ? false : true}
              disabled={isClassStartted()}
              // if ((serverStartDay ? serverStartDay : currDateDay) === classStartDay && (serverStartMonth ? serverStartMonth : currDateMonth) === classStartMonth && (serverStartYear ? serverStartYear : currDateYear) === classStartYear) {
              className='teacherFullViewSmallButtons'
            >
              Join
            </Button>
          </>
        </Grid>
      ) : (
        <>
          {endTime < (servTime ? servTime : currTime) ? (
            <Button
              size='small'
              color='secondary'
              variant='contained'
              disabled='true'
              className='teacherFullViewSmallButtons'
            >
              Class Over
            </Button>
          ) : isRejected ? (
            <>
              <Grid item xs={6}>
                <Typography style={{ color: '#ff6b6b' }}>Rejected</Typography>
              </Grid>
            </>
          ) : (
            <>
              <Grid item md={3} xs={6}>
                {window.location.pathname === '/online-class/attend-class' ? (
                  <>
                    <Popover
                      id={ids}
                      open={openJoin}
                      joinAnchor={joinAnchor}
                      onClose={handleClose}
                      style={{ overflow: 'hidden', margin: '19% 0 0 30%' }}
                      className='xyz'
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                    >
                      <Grid
                        container
                        spacing={2}
                        style={{ textAlign: 'center', padding: '10px' }}
                      >
                        <Grid item xs={12} style={{ textAlign: 'right' }}>
                          <CloseIcon
                            style={{ color: '#014B7E' }}
                            onClick={() => handleCloseAccept()}
                          />
                        </Grid>
                        <Grid item md={12} xs={12}>
                          <Typography>
                            You can start before 5 min from the meeting time :
                            {moment(`${props?.data?.date}T${startTime}`).format(
                              'hh:mm:ss A'
                            )}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Popover>
                    <Button
                      size='small'
                      color='secondary'
                      fullWidth
                      variant='contained'
                      onClick={(e) => handleClickAccept(e)}
                      disabled={((serverStartDay ? serverStartDay : currDateDay) === classStartDay && (serverStartMonth ? serverStartMonth : currDateMonth) === classStartMonth && (serverStartYear ? serverStartYear : currDateYear) === classStartYear) ? false : true}
                      className='teacherFullViewSmallButtons'
                    >
                      Accept
                    </Button>
                    {/* endTime < currTime ?
                       <Button
                       size='small'
                       color='secondary'
                       fullWidth
                       variant='contained'
                       disabled='true'
                       className='teacherFullViewSmallButtons'
                     >
                       Class Over
                     </Button>:
                      <Button
                      size='small'
                      color='secondary'
                      fullWidth
                      variant='contained'
                      onClick={(e) => handleClickAccept(e)}
                      disabled={classStartTime === currDate ? false : true}
                      className='teacherFullViewSmallButtons'
                    >
                      Accept
                    </Button> */}
                  </>
                ) : (
                  <Button
                    size='small'
                    color='secondary'
                    fullWidth
                    variant='contained'
                    disabled={isClassHosted()}
                    onClick={() => handleHost(fullData)}
                    className='teacherFullViewSmallButtons'
                  >
                    Host
                  </Button>
                )}
              </Grid>
              <Grid item md={3} xs={6} className='xxx'>
                <Popover
                  className='deletePopover'
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  style={{ overflow: 'hidden' }}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                  }}
                >
                  <Grid
                    container
                    spacing={2}
                    style={{ textAlign: 'center', padding: '10px' }}
                  >
                    <Grid item md={12} xs={12}>
                      <Typography>Are you sure you want to cancel?</Typography>
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Button
                        variant='contained'
                        size='small'
                        style={{ fontSize: '11px' }}
                        onClick={() => handleCloseData()}
                      >
                        Cancel
                      </Button>
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Button
                        variant='contained'
                        color='primary'
                        style={{ fontSize: '11px' }}
                        size='small'
                        onClick={() => handleCancel()}
                      >
                        Confirm
                      </Button>
                    </Grid>
                  </Grid>
                </Popover>
                {window.location.pathname === '/online-class/teacher-view-class' ? (
                  <Button
                    size='small'
                    fullWidth
                    variant='contained'
                    onClick={(e) => handleClick(e)}
                    // disabled={endTime<currTime? false}
                    className='teacherFullViewSmallButtons1'
                  >
                    Cancel
                  </Button>
                ) : (
                  ''
                )}
              </Grid>
            </>
          )}
        </>
      )}
    </Grid>
  );
};

const TeacherBatchFullView = ({ fullData, handleClose, selectedGrade }) => {
  const [noOfPeriods, setNoOfPeriods] = useState([]);
  const [loading, setLoading] = useState(false);
  const { setAlert } = useContext(AlertNotificationContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory();
  const { role_details } = JSON.parse(localStorage.getItem('userDetails'));
  /*
  useEffect(() => {
    if (fullData) {
      axiosInstance
        .get(
          `erp_user/${
            fullData  && fullData.id
          }/online-class-details/`
        )
        .then((res) => {
          setNoOfPeriods(res.data.data);
        })
        .catch((error) => setAlert('error', error.message));
    }
  }, [fullData]);
  */

  useEffect(() => {
    let detailsURL =
      window.location.pathname === '/online-class/attend-class'
        ? `erp_user/${fullData && fullData?.id}/student-oc-details/`
        : `erp_user/${fullData && fullData?.id}/online-class-details/?is_substitute=${fullData.online_class.is_substitute || ''
        }&substituted_teacher_id=${fullData.online_class.is_substitute === true
          ? fullData.online_class.substituted_teacher_id
          : ''
        }&start_date=${fullData.online_class.substitute_start_date || ''}&end_date=${fullData.online_class.substitute_end_date || ''
        }`;

    if (fullData) {
      axiosInstance
        .get(detailsURL)
        .then((res) => {
          setNoOfPeriods(res.data.data);
        })
        .catch((error) => setAlert('error', error.message));
    }
  }, [fullData, handleClose]);

  const handleCloseData = () => {
    setAnchorEl(null);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const converTime = (info) => {
    let hour = info.split(':')[0];
    let min = info.split(':')[1];
    const part = hour >= 12 ? 'PM' : 'AM';
    min = `${min}`.length === 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = `${hour}`.length === 1 ? `0${hour}` : hour;
    return `${hour}:${min} ${part}`;
  };

  function handleCancel() {
    setLoading(true);
    const params = {
      zoom_meeting_id: fullData && fullData?.id,
      class_date: fullData && fullData?.online_class?.start_time.split('T')[0],
    };
    let url = '';
    if (window.location.pathname === '/online-class/attend-class') {
      url = endpoints.studentViewBatchesApi.rejetBatchApi;
    } else {
      url = endpoints.teacherViewBatches.cancelBatchApi;
    }
    axiosInstance
      .put(url, params)
      .then((res) => {
        setLoading(false);
        setAlert('success', res.data.message);
        handleClose('success');
      })
      .catch((error) => {
        setLoading(false);
        setAlert('error', error.message);
      });
  }

  const handleAttendance = () => {
    history.push(`/aol-attendance-list/${fullData?.online_class && fullData?.id}`);
  };
  // console.log(selectedGrade,'FFFFFFFFFFFFFFFFFFFFFFF')
  // history.push(`/create/course/${props.filterData && props?.filterData?.course?.id}/${props.filterData && props?.filterData?.grade?.id}`)
  // sessionStorage.setItem('isAol','isAol');
  const handleCoursePlan = () => {
    if (window.location.pathname === '/online-class/attend-class') {
      history.push(
        `/create/course/${fullData.online_class && fullData?.online_class?.course_id
        }/${1}`
      );
      sessionStorage.setItem('isAol', 2);
    } else {
      history.push(
        `/create/course/${fullData.online_class && fullData?.online_class?.cource_id
        }/${1}`
      );
      sessionStorage.setItem('isAol', 3);
    }
  };

  const [openPopup, setOpenPopup] = React.useState(false);
  const [selectedValue, setSelectedValue] = React.useState('');

  const handleClickOpen = () => {
    setOpenPopup(true);
  };

  const handleClosePopup = (value) => {
    setOpenPopup(false);
    setSelectedValue(value);
  };

  return (
    <>
      <Grid container spacing={2}>
        <Grid item md={12} xs={12} className='teacherBatchFullViewMainDiv'>
          <Card className='teacherBatchFullViewMainCard'>
            <Grid container spacing={2} className='teacherBatchFullViewHeader'>
              <Grid item xs={12} style={{ textAlign: 'right' }}>
                <CloseIcon style={{ color: '#014B7E', cursor: 'pointer' }} onClick={() => handleClose()} />
              </Grid>
              <Grid item xs={12}>
                <Grid container spacing={2}>
                  <Grid item md={8} xs={8}>
                    <h4 className='teacherBatchFullCardLable'>
                      {(fullData &&
                        fullData.online_class &&
                        fullData?.online_class?.title) ||
                        ''}
                    </h4>
                    <h4 className='teacherBatchFullCardLable'>
                      {(fullData &&
                        fullData.online_class &&
                        fullData.online_class.subject &&
                        fullData.online_class.subject.length !== 0 &&
                        fullData?.online_class?.subject.map((item) => (
                          <span>
                            {item?.subject_name || ''}
                            &nbsp;
                          </span>
                        ))) ||
                        ''}
                    </h4>
                  </Grid>
                  <Grid item md={4} xs={4}>
                    <h4 className='teacherBatchFullCardLable'>
                      {(fullData &&
                        fullData?.join_time &&
                        converTime(fullData?.join_time.split(' ')[1])) ||
                        ''}
                    </h4>
                    <h4 className='teacherBatchFullCardLable'>
                      {noOfPeriods && noOfPeriods?.length}
                      &nbsp;Periods
                    </h4>
                  </Grid>
                </Grid>
              </Grid>
              {/* 
                    <IconButton
                      size='small'
                      className='teacherBatchFullViewClose'
                      onClick={() => handleClose()}
                    >
                      <CloseIcon className='teacherBatchFullViewCloseIcon' />
                    </IconButton>
                  </Grid> */}
            </Grid>
            <Grid container spacing={2}>
              <Grid item md={12} xs={12}>
                <span className='TeacherFullViewdescreption'>Periods Date</span>
              </Grid>
              <Grid item md={12} xs={12} style={{ height: '400px', overflowY: 'scroll' }}>
                <Divider className='fullViewDivider' />
                {!fullData && noOfPeriods && noOfPeriods.length === 0 && (
                  <Typography style={{ color: '#ff6b6b', margin: '10px' }}>
                    No Record found
                  </Typography>
                )}
                {noOfPeriods &&
                  noOfPeriods.length > 0 &&
                  noOfPeriods.map((data) => (
                    <JoinClass
                      data={data}
                      fullData={fullData}
                      handleClose={handleClose}
                    />
                  ))}

                <Divider className='fullViewDivider' />
              </Grid>
              <Grid item md={12} xs={12}>
                {window.location.pathname !== '/online-class/attend-class' ? (
                  <Button
                    fullWidth
                    size='small'
                    variant='contained'
                    className='teacherFullViewFullButtons'
                    onClick={handleAttendance}
                  >
                    Attendance
                  </Button>
                ) : (
                  <Button
                    fullWidth
                    size='small'
                    variant='contained'
                    className='teacherFullViewFullButtons'
                    onClick={handleClickOpen}
                  >
                    Resources
                  </Button>
                )}
                <Button
                  fullWidth
                  size='small'
                  variant='contained'
                  className='teacherFullViewFullButtons'
                  onClick={handleCoursePlan}
                >
                  View Course Plan
                </Button>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>
      <ResourceDialog
        selectedValue={selectedValue}
        open={openPopup}
        onClose={handleClosePopup}
        title={fullData.online_class.title}
        resourceId={fullData?.id}
        onlineClassId={fullData.online_class?.id}
        startDate={fullData.online_class.start_time}
        endDate={fullData.online_class.end_time}
      />
      {loading && <Loader />}
    </>
  );
};

export default TeacherBatchFullView;
