import React, { useState } from 'react';
import moment from 'moment';
import './style.scss';
import { Grid, Card, Button, Typography } from '@material-ui/core';
import StudentBatchList from './student-batch-list';

const TeacherBatchViewCard = ({ fullData, selectedBranch, handleViewMore, selectedViewMore }) => {
  const [open, setOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');
  console.log('fullData', fullData, fullData.online_class.is_substitute);
  return (
    <>
      {fullData.online_class.is_substitute === true ? (
        <Grid container spacing={2} className='teacherbatchsSubstituteCardMain'>
          <Grid item md={12} xs={12}>
            <Card
              className={
                (fullData && fullData.id) === (selectedViewMore && selectedViewMore.id)
                  ? 'teacherBatchSubstituteCardActive'
                  : 'teacherBatchSubstituteCardInActive'
              }
            >
              <Grid container spacing={2} style={{ height: '300px' }}>
                <Grid item md={9} xs={9} style={{ padding: '5px' }}>
                  {fullData &&
                    fullData.online_class &&
                    fullData.online_class.cource_name && (
                      <span className='teacherBatchSubstituteCardLable'>
                        {fullData.online_class.cource_name}
                      </span>
                    )}
                </Grid>
                <Grid item md={9} xs={9} style={{ padding: '5px' }}>
                  {fullData &&
                    fullData.online_class &&
                    fullData.online_class.course_name && (
                      <span className='teacherBatchSubstituteCardLable'>
                        {fullData.online_class.course_name}
                      </span>
                    )}
                  {fullData &&
                    fullData.online_class &&
                    fullData.online_class.cource_name && (
                      <span className='teacherBatchSubstituteCardLable'>
                        {fullData.online_class.cource_name}
                      </span>
                    )}
                  {fullData && fullData.online_class && fullData.online_class.title && (
                    <span className='teacherBatchSubstituteCardLable'>
                      {fullData.online_class.title}
                    </span>
                  )}
                </Grid>
                <Grid item xs={3} style={{ padding: '5px' }}>
                  <Typography>
                    {moment(
                      fullData.online_class ? fullData.online_class.start_time : ''
                    ).format('hh:mm A')}
                  </Typography>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchSubstituteCardLable'>
                    {(fullData &&
                      fullData.online_class &&
                      fullData.online_class.subject &&
                      fullData.online_class.subject.length !== 0 &&
                      fullData.online_class.subject.map((item) => (
                        <span>
                          {item.subject_name || ''}
                          &nbsp;
                        </span>
                      ))) ||
                      ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchSubstituteCardLable1'>
                    Batch Days:&nbsp;
                    {fullData.online_class ? fullData.online_class.week_day : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchSubstituteCardLable1'>
                    Start Date:&nbsp;
                    {/*(fullData &&
                    fullData.online_class &&
                    fullData.online_class.start_time &&
                    new Date(fullData.online_class.start_time)
                      .toString()
                      .split('G')[0]
                      .substring(0, 16)) ||
                  ''*/}
                    {fullData.online_class
                      ? moment(fullData.online_class.start_time).format('DD-MM-YYYY')
                      : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchSubstituteCardLable1'>
                    End Date:&nbsp;
                    {/*(fullData &&
                    fullData.online_class &&
                    fullData.online_class.end_time &&
                    new Date(fullData.online_class.end_time)
                      .toString()
                      .split('G')[0]
                      .substring(0, 16)) ||
                  '' */}
                    {fullData.online_class
                      ? moment(fullData.online_class.end_time).format('DD-MM-YYYY')
                      : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchSubstituteCardLable1'>
                    Students Count:&nbsp;
                    {fullData.online_class ? fullData.online_class.no_of_students : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchSubstituteCardLable1'>
                    Branch:&nbsp;
                    {selectedBranch?.branch_name}
                  </span>
                </Grid>
                <Grid item md={6} xs={12} style={{ textAlign: 'left' }}>
                  <Button
                    variant='contained'
                    color='secondary'
                    className='TeacherBatchSubstituteCardViewMoreButton'
                    disabled={
                      fullData?.online_class?.students === null ||
                      fullData?.online_class?.students === undefined
                    }
                    style={{
                      display:
                        (fullData && fullData?.id) ===
                          (selectedViewMore && selectedViewMore?.id)
                          ? 'none'
                          : '',
                    }}
                    onClick={() => {
                      setOpen(true);
                      setSelectedItem(
                        fullData.online_class ? fullData.online_class?.students : ''
                      );
                    }}
                  >
                    Student List
                  </Button>
                </Grid>
                <Grid item md={6} xs={12} style={{ textAlign: 'right' }}>
                  <Button
                    variant='contained'
                    color='secondary'
                    className='TeacherBatchSubstituteCardViewMoreButton'
                    style={{
                      display:
                        (fullData && fullData?.id) ===
                          (selectedViewMore && selectedViewMore?.id)
                          ? 'none'
                          : '',
                    }}
                    onClick={() => handleViewMore(fullData)}
                  >
                    View More
                  </Button>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      ) : (
        <Grid container spacing={2} className='teacherbatchsCardMain'>
          <Grid item md={12} xs={12}>
            <Card
              className={
                (fullData && fullData.id) === (selectedViewMore && selectedViewMore.id)
                  ? 'teacherBatchCardActive'
                  : 'teacherBatchCardInActive'
              }
            >
              <Grid container spacing={2} style={{ height: '300px' }}>
                {/* <Grid item md={9} xs={9} style={{ padding: '5px', height:'50px', border:'1px solid red' }}>
                  {fullData &&
                    fullData.online_class &&
                    fullData.online_class.cource_name && (
                      <span className='teacherBatchCardLable'>
                        {fullData.online_class.cource_name}
                      </span>
                    )}
                </Grid> */}
                <Grid item md={9} xs={9} style={{ padding: '5px' }}>
                  {fullData &&
                    fullData.online_class &&
                    fullData.online_class.course_name && (
                      <span className='teacherBatchCardLable'>
                        {fullData.online_class.course_name}
                      </span>
                    )}
                  {fullData &&
                    fullData.online_class &&
                    fullData.online_class.cource_name && (
                      <span className='teacherBatchCardLable'>
                        {fullData.online_class.cource_name}
                      </span>
                    )}
                  {fullData && fullData.online_class && fullData.online_class.title && (
                    <span className='teacherBatchCardLable'>
                      {fullData.online_class.title}
                    </span>
                  )}
                </Grid>
                <Grid item xs={3} style={{ padding: '5px' }}>
                  <Typography>
                    {moment(
                      fullData.online_class ? fullData.online_class.start_time : ''
                    ).format('hh:mm A')}
                  </Typography>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchCardLable'>
                    {(fullData &&
                      fullData.online_class &&
                      fullData.online_class.subject &&
                      fullData.online_class.subject.length !== 0 &&
                      fullData.online_class.subject.map((item) => (
                        <span>
                          {item.subject_name || ''}
                          &nbsp;
                        </span>
                      ))) ||
                      ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchCardLable1'>
                    Batch Days:&nbsp;
                    {/* {fullData.online_class ? fullData.online_class.week_day : ''} */}
                    {fullData.online_class ? fullData.online_class?.week_day: 'NA'}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchCardLable1'>
                    Start Date:&nbsp;
                    {/*(fullData &&
                 fullData.online_class &&
                 fullData.online_class.start_time &&
                 new Date(fullData.online_class.start_time)
                   .toString()
                   .split('G')[0]
                   .substring(0, 16)) ||
               ''*/}
                    {fullData.online_class
                      ? moment(fullData.online_class.start_time).format('DD-MM-YYYY')
                      : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchCardLable1'>
                    End Date:&nbsp;
                    {/*(fullData &&
                 fullData.online_class &&
                 fullData.online_class.end_time &&
                 new Date(fullData.online_class.end_time)
                   .toString()
                   .split('G')[0]
                   .substring(0, 16)) ||
               '' */}
                    {fullData.online_class
                      ? moment(fullData.online_class.end_time).format('DD-MM-YYYY')
                      : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchCardLable1'>
                    Students Count:&nbsp;
                    {fullData.online_class ? fullData.online_class.no_of_students : ''}
                  </span>
                </Grid>
                <Grid item md={12} xs={12} style={{ padding: '5px' }}>
                  <span className='teacherBatchCardLable1'>
                    Branch:&nbsp;
                    {/* {selectedBranch?.branch_name} */}
                    {fullData.online_class ? fullData.online_class.branch: 'NA'}
                  </span>
                </Grid>
                <Grid item md={6} xs={12} style={{ textAlign: 'left' }}>
                  <Button
                    variant='contained'
                    color='secondary'
                    className='TeacherBatchCardViewMoreButton'
                    disabled={
                      fullData?.online_class?.students === null ||
                      fullData?.online_class?.students === undefined
                    }
                    style={{
                      display:
                        (fullData && fullData?.id) ===
                          (selectedViewMore && selectedViewMore?.id)
                          ? 'none'
                          : '',
                    }}
                    onClick={() => {
                      setOpen(true);
                      setSelectedItem(
                        fullData.online_class ? fullData.online_class?.students : ''
                      );
                    }}
                  >
                    Student List
                  </Button>
                </Grid>
                <Grid item md={6} xs={12} style={{ textAlign: 'right' }}>
                  <Button
                    variant='contained'
                    color='secondary'
                    className='TeacherBatchCardViewMoreButton'
                    style={{
                      display:
                        (fullData && fullData?.id) ===
                          (selectedViewMore && selectedViewMore?.id)
                          ? 'none'
                          : '',
                    }}
                    onClick={() => handleViewMore(fullData)}
                  >
                    View More
                  </Button>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      )}
      {open ? (
        <StudentBatchList
          content={selectedItem}
          open={open}
          close={() => {
            setOpen(false);
            setSelectedItem('');
          }}
        />
      ) : (
        ''
      )}
    </>
  );
};

export default TeacherBatchViewCard;
