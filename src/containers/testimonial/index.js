import React, { useState, useEffect, useContext } from 'react';
import {
  Grid,
  Button,
  TableHead,
  TableCell,
  Card,
  TableRow,
  Table,
  TableBody,
  IconButton,
  TablePagination,
  Typography,
  useTheme,
  Paper,
  SvgIcon,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import BlockIcon from '@material-ui/icons/Block';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
import './style.scss';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import TabPanel from './testimonial-tab';
import unfiltered from '../../assets/images/unfiltered.svg';
import selectfilter from '../../assets/images/selectfilter.svg';
import VideoCard from './video-card';
import { Pagination } from '@material-ui/lab';

const Testimonial = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [tabValue, setTabValue] = useState(0);
  const themeContext = useTheme();
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const { setAlert } = useContext(AlertNotificationContext);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const limit = 6;
  const [pageFlag, setPageFlag] = useState(false);
  const [tabVal, setTabVal] = useState('');
  const [videoData, setVideoData] = useState()
  const [VideoDetail, setVideoDetail] = useState()
  const [deleteFlag, setDeleteFlag] = useState(false);



  const [question, setQuestions] = useState([
    {
      id: "ckoil19kv0001306dzylmuu5g",
      question: '',
      attachments: [],
      is_attachment_enable: false,
      max_attachment: 5,
      penTool: false,
    },
  ]);


  const handleVideoList = (tabMenuval) => {

    setTabVal(tabMenuval);
    setLoading(true);

    if (tabMenuval === 0 || tabMenuval === undefined || tabMenuval === null || tabMenuval === '') {
      axiosInstance
        .get(`${endpoints.testimonial.TestimonialView}?page=${page}&page_size=${limit}`)
        .then((result) => {
          if (result.data.status_code === 200) {
            setVideoData(result.data.data.results);
            setTotalCount(result.data.data.count);
            setVideoDetail(result.data.data);
            setLoading(false);
          } else {
            setLoading(false);
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
    if (tabMenuval === 1) {
      axiosInstance
        .get(`${endpoints.testimonial.TestimonialView}?page=${page}&page_size=${limit}&is_active=True`)
        .then((result) => {
          if (result.data.status_code === 200) {
            setVideoData(result.data.data.results);
            setTotalCount(result.data.data.count);
            setVideoDetail(result.data.data);
            setLoading(false);
          } else {
            setLoading(false);
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
    if (tabMenuval === 2) {
      axiosInstance
        .get(`${endpoints.testimonial.TestimonialView}?page=${page}&page_size=${limit}&is_active=False`)
        .then((result) => {
          if (result.data.status_code === 200) {
            setVideoData(result.data.data.results);
            setTotalCount(result.data.data.count);
            setVideoDetail(result.data.data);
            setLoading(false);
          } else {
            setLoading(false);
            setAlert('error', result.data.message);
          }
        })
        .catch((error) => {
          setLoading(false);
          setAlert('error', error.message);
        });
    }
    // else{
    //   setAlert('warning', 'something went wrong');
    // }

  }

  const handlePagination = (event, page) => {
    setPage(page);
    setPageFlag(true);
  };

  useEffect(() => {
    handleVideoList()
  }, []);
  useEffect(() => {
    setPage(1);
  }, [tabVal])

  useEffect(() => {
    handleVideoList(tabVal);
  }, [deleteFlag]);

  useEffect(() => {
    if (pageFlag === true && page) {
      handleVideoList(tabVal);
    }
  }, [page]);

  return (
    <Layout>
      <Grid container spacing={2} className='aol-coupon-code-generator-body'>
        <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head'>
          <CommonBreadcrumbs componentName='Testimonial' />
        </Grid>
        <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head'>
          <Card>
            <Grid
              item
              xs={6}
              sm={2}
              className={isMobile ? 'createButton' : 'createButton addButtonPadding'}
            >
              <Button
                startIcon={<AddOutlinedIcon style={{ fontSize: '30px' }} />}
                variant='contained'
                style={{ color: 'white' }}
                color='primary'
                className='buttonModifiedDesign'
                onClick={() => {
                  history.push('/testimonial-upload');
                }}
                size='medium'
              >
                CREATE
          </Button>
            </Grid>
          </Card>
        </Grid>

        <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head'>
          <div>
            <TabPanel
              handleVideoList={handleVideoList}
              setTabValue={setTabValue}
              tabValue={tabValue}
            />
          </div>
        </Grid>
        <Grid item md={12} xs={12}>
          <Paper >
            {videoData && videoData.length > 0 ? (
              <Grid
                container
                style={
                  isMobile
                    ? { width: '95%', margin: '10px auto' }
                    : { width: '100%', margin: '10px auto' }
                }
                spacing={5}
              >
                <Grid item xs={12} sm={12}>
                  <Grid container spacing={3}>
                    {videoData.map((video, i) => (
                      <Grid
                        item
                        xs={12}
                        sm={4}
                      >
                        <VideoCard
                          index={i}
                          videoSrc={video.file}
                          video={video.id}
                          active={video.is_active}
                          tabVal={tabVal}
                          deleteFlag={deleteFlag}
                          setDeleteFlag={setDeleteFlag}
                          handleVideoList={handleVideoList}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </Grid>

              </Grid>
            ) : (
              <div className='periodDataUnavailable'>
                <SvgIcon
                  component={() => (
                    <img
                      style={
                        isMobile
                          ? { height: '100px', width: '200px' }
                          : { height: '160px', width: '290px' }
                      }
                      src={unfiltered}
                    />
                  )}
                />

              </div>
            )}
            {videoData?.length > 0 && (
              <div className='paginateData paginateMobileMargin'>
                <Pagination
                  onChange={handlePagination}
                  style={{ marginTop: 25 }}
                  count={Math.ceil(totalCount / limit)}
                  color='primary'
                  page={page}
                />
              </div>
            )}
          </Paper>
        </Grid>
      </Grid>
      {loading && <Loading />}
    </Layout>
  );
};

export default Testimonial;
