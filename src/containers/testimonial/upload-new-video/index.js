import React, { useState, useEffect, useContext, useRef } from 'react';
import {
    Grid,
    Button,
    TableHead,
    TableCell,
    Card,
    TableRow,
    Table,
    TableBody,
    IconButton,
    TablePagination,
    Typography,
    CardContent,
    FormLabel,
    FormControl,
    Input,
    Switch,
    Box,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import BlockIcon from '@material-ui/icons/Block';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import CommonBreadcrumbs from '../../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../../Layout';
import './style.scss';
import axiosInstance from '../../../config/axios';
import Loading from '../../../components/loader/loader';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import './style.scss';
import endpoints from '../../../config/endpoints';
import displayName from '../../../config/displayName';

const Testimonial = () => {
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [tabValue, setTabValue] = useState(0);
    const [fileData, setFileData] = useState();
    const [showIn, setShowIn] = useState(false)
    const { setAlert } = useContext(AlertNotificationContext);
    const fileRef = useRef()


    const handelfile = (file) => {
        setFileData(file)
    }

    const showVideoOnChange = (index, value) => {
        setShowIn(value)
    }

    const handleUpload = () => {
        if (fileData == undefined || fileData == '') {
            setAlert('error', "Please select video");
        } else {
            setLoading(true)
            const formData = new FormData();
            formData.append('file', fileData);
            formData.append('is_active', showIn);
            axiosInstance.post(`${endpoints.testimonial.TestimonialView}`, formData,)
                .then(result => {
                    if (result.data.status_code === 200) {
                        setAlert('success', result.data.message);
                        fileRef.current.value = null;
                        setLoading(false)
                        history.push('/testimonial');
                    } else {
                        setAlert('error', result.data.message);
                        setLoading(false)
                    }

                }).catch((err) => {
                    setAlert('error', err.message);
                    setLoading(false)
                })
        }
    }

    return (
        <Layout>
            <Grid container spacing={2} className='aol-coupon-code-generator-body'>
                <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head'>
                    <CommonBreadcrumbs componentName='Testimonial' />
                </Grid>
                <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head'>
                    <Grid item xs={12}>
                        <Card className='question-card'>
                            <CardContent>
                                <Grid container>
                                    <Grid item container xs={12} md={6}>
                                        <Grid item xs={10} style={{ marginBottom: '10px' }}>
                                            <FormLabel style={{ color: '#014b7e' }} >upload video testimonials</FormLabel>
                                        </Grid>
                                        <Grid item xs={10}>
                                            <FormControl fullWidth >
                                                <Input
                                                    type='file'
                                                    inputRef={fileRef}
                                                    inputProps={{ accept: "video/mp4,video/x-m4v,video/*" }}
                                                    onChange={(e) => {
                                                        handelfile(e.target.files[0])
                                                    }}
                                                />
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Grid item xs={10} style={{ marginBottom: '10px' }}>
                                            <FormLabel style={{ color: '#014b7e' }} >show video in {displayName ?displayName : ''}.com websites</FormLabel>
                                        </Grid>
                                        <Box>
                                            <Grid component="label" container alignItems="center" spacing={1}>
                                                <Grid item>No</Grid>
                                                <Grid item>
                                                    <Switch
                                                        onChange={(e) => {
                                                            showVideoOnChange('is_attachment_enable', e.target.checked);
                                                        }}
                                                        name='checkedA'
                                                        color='primary'
                                                    />
                                                </Grid>
                                                <Grid item>Yes</Grid>
                                            </Grid>
                                        </Box>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
                <Grid item md={12} xs={12} className='aol-coupon-code-generator-top-head2'>
                    <Card className='question-card'>
                        <CardContent>
                            <Grid
                                container
                                direction="row"
                                justify="space-between"
                                alignItems="flex-start"
                            >

                                <Grid item xs={6} sm={2} >
                                    <Button
                                        variant='contained'
                                        className='labelColor buttonModifiedDesign'
                                        size='medium'
                                        onClick={() => {
                                            history.push('/testimonial');
                                        }}
                                    >
                                        Back
                                    </Button>
                                </Grid>
                                <Grid item xs={6} sm={2} className='form-field'>
                                    <div className='finish-btn-container'>
                                        <Button
                                            className='buttonModifiedDesign'
                                            color='primary'
                                            onClick={handleUpload}
                                        >
                                            Upload
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>

            </Grid>
            {loading && <Loading />}
        </Layout>
    );
};

export default Testimonial;
