import React, { useContext, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Button, useTheme, IconButton, Popover, withStyles } from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import Box from '@material-ui/core/Box';
import useStyles from './useStyles';
import endpoints from '../../../config/endpoints';
import axiosInstance from '../../../config/axios';
import { AlertNotificationContext } from '../../../context-api/alert-context/alert-state';
import { Grade } from '@material-ui/icons';
import Loading from '../../../components/loader/loader';

const StyledButton = withStyles({
  root: {
    color: '#FFFFFF',
    backgroundColor: '#FF6B6B',
    '&:hover': {
      backgroundColor: '#FF6B6B',
    },
  }
})(Button);

const CancelButton = withStyles({
  root: {
    color: '#8C8C8C',
    backgroundColor: '#e0e0e0',
    '&:hover': {
      backgroundColor: '#e0e0e0',
    },
  }
})(Button);

const VideoCard = ({
  index,
  videoSrc,
  video,
  active,
  tabVal,
  deleteFlag,
  setDeleteFlag,
  handleVideoList
}) => {
  const themeContext = useTheme();
  const { setAlert } = useContext(AlertNotificationContext);
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const classes = useStyles();
  const [showMenu, setShowMenu] = useState(false);
  const [showPeriodIndex, setShowPeriodIndex] = useState();
  const [loading, setLoading] = useState(false);

  const handlePeriodMenuOpen = (index, id) => {
    setShowMenu(true);
    setShowPeriodIndex(index);
  };

  const handlePeriodMenuClose = (index) => {
    setShowMenu(false);
    setShowPeriodIndex();
  };


  const handleDelete = (e, index) => {
    setLoading(true)
    axiosInstance
      .delete(`${endpoints.testimonial.aol}${e}/update-deleteupload_video/`)
      .then((result) => {
        if (result.data.status_code === 200) {
          setAlert('success', 'Video successfully Deleted');
          setDeleteFlag(!deleteFlag);
          setLoading(false)
        } else {
          setAlert('error', 'ERROR!');
          setLoading(false)
        }
        handleClose();
      })
      .catch((error) =>
        setAlert('error', error.response?.data?.message || error.response?.data?.msg),
        setLoading(false)
      );
  };

  const handleStatus = (e, index) => {
    if (tabVal === 1) {
      setLoading(true)
      axiosInstance.put(`${endpoints.testimonial.aol}${e}/update-deleteupload_video/`, {
        "is_active": "False"
      }).then(result => {
        if (result.data.status_code === 200) {
          setAlert('success', 'Video successfully Inactivated')
          handleVideoList(tabVal);
          setLoading(false);
        }
        else {
          setAlert('error', 'Not Updated, Try After Few Mins.');
          setLoading(false);
        }
      }).catch((error) =>
        setAlert('error', error.message),
        setLoading(false)
      );
    }

    if (tabVal === 2) {
      setLoading(true)
      axiosInstance.put(`${endpoints.testimonial.aol}${e}/update-deleteupload_video/`, {
        "is_active": "True"
      }).then(result => {
        if (result.data.status_code === 200) {
          setAlert('success', 'Video successfully Activated')
          handleVideoList(tabVal);
          setLoading(false)
        }
        else {
          setAlert('error', 'Not Updated, Try After Few Mins.')
          setLoading(false)
        }
      }).catch((error) =>
        setAlert('error', error.message),
        setLoading(false)
      );
    }
  }

  // Confirm Popover 
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  return (
    <>
      <Paper elevation={3} style={{ padding: '5px 10px' }}>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <Typography
              className={classes.title}
              variant='p'
              component='p'
              color='primary'
            >
              Testimonial - {video}
            </Typography>
          </Grid>
          <Grid item xs={4} className={classes.textRight}>
            <Box>
              <span
                className='period_card_menu'
                onClick={() => handlePeriodMenuOpen(index)}
                onMouseLeave={handlePeriodMenuClose}
              >
                <IconButton className='moreHorizIcon' color='primary'>
                  <MoreHorizIcon />
                </IconButton>
                {showPeriodIndex === index && showMenu ? (
                  <div
                    className='tooltip'
                    style={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <span className='tooltiptext'>
                      <div className='tooltip' onClick={(e) => handleClick(e)}>
                        Delete
                                    </div>
                      <div
                        className='tooltip'
                        onClick={() => handleStatus(video)}
                        style={{ marginTop: '5px' }}
                      >

                        {tabVal == 1 ? 'Inactive' : ''}
                        {tabVal == 2 ? 'Active' : ''}
                      </div>
                    </span>
                    <Popover
                      id={id}
                      open={open}
                      anchorEl={anchorEl}
                      onClose={handleClose}
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                      }}
                      transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                      }}
                    >
                      <div style={{ padding: '20px 30px' }}>
                        <Typography style={{ fontSize: '20px', marginBottom: '15px' }}>Are you sure you want to delete?</Typography>
                        <div>
                          <CancelButton
                            onClick={(e) => handleClose()}
                          >
                            Cancel
                          </CancelButton>
                          <StyledButton
                            onClick={() => handleDelete(video)}
                            style={{ float: 'right' }}
                          >
                            Confirm
                          </StyledButton>
                        </div>
                      </div>
                    </Popover>
                  </div>
                ) : null}
              </span>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <video
              video='attachment-iframe'
              style={{
                width: '100%',
                objectFit: 'contain',
              }}
              controls
              // autoPlay
              controlsList='nodownload'
              key={videoSrc}
            >
              <source src={videoSrc} type="video/mp4" />
                 {/* {src.endsWith('.mp4') ? (
                
                      <source src={src} type="video/mp4" />
                  ) : (
                      <source src={src} type='audio/mp3' />
                  )} */}
                 Your browser does not support HTML5 video.
              </video>
          </Grid>
        </Grid>
      </Paper>
      {loading && <Loading />}
    </>
  )
}

export default VideoCard