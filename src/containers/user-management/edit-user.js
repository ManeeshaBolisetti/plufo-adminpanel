/* eslint-disable react/no-did-update-set-state */
/* eslint-disable camelcase */
/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { styles } from './useStyles';
import UserDetailsForm from './user-details-form';
import SchoolDetailsForm from './school-details-form';
import GuardianDetailsForm from './guardian-details-form';
import { fetchUser, editUser } from '../../redux/actions';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import { getSteps, jsonToFormData } from './utils';
import CustomStepperConnector from '../../components/custom-stepper-connector';
import CustomStepperIcon from '../../components/custom-stepper-icon';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';

class EditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeStep: 0,
      showTeacherForm: false,
      showParentForm: false,
      showGuardianForm: false,
      user: null,
    };
  }

  componentDidMount() {
    this.fetchUserDetails();
  }

  componentDidUpdate(prevProps) {
    const { selectedUser } = this.props;
    if (prevProps.selectedUser !== selectedUser && selectedUser) {
      this.setState({ user: selectedUser });
    }
  }

  toggleParentForm = (e) => {
    this.setState({ showParentForm: e.target.checked });
  };

  toggleGuardianForm = (e) => {
    this.setState({ showGuardianForm: e.target.checked });
  };
  toggleTeacherForm = (e) => {
    this.setState({ showTeacherForm: e.target.checked });
  };

  handleReset = () => {
    this.setState({ activeStep: 0 });
  };

  handleNext = () => {
    this.setState((prevState) => ({ activeStep: prevState.activeStep + 1 }));
  };

  handleBack = () => {
    this.setState((prevState) => ({ activeStep: prevState.activeStep - 1 }));
  };

  onSubmitSchoolDetails = (details) => {
    const { setAlert } = this.context;
    if (!details.academic_year) {
      setAlert('error', 'Please Select Academic Year');
    } else if (!details.branch) {
      setAlert('error', 'Please Select Branch');
    } else if (details.grade && details.grade.length === 0) {
      setAlert('error', 'Please Select Grade');
    } else if (details.section && details.section.length === 0) {
      setAlert('error', 'Please Select Section');
    } else if (details.subjects && details.subjects.length === 0) {
      setAlert('error', 'Please Select Subject');
    } else {
      const { selectedUser } = this.props;
      if (selectedUser.parent.father_first_name) {
        this.setState({ showParentForm: true });
      }
      if (selectedUser.parent.guardian_first_name) {
        this.setState({ showGuardianForm: true });
      }
      if (selectedUser.weekly_off.length) {
        this.setState({ showTeacherForm: true });
      }
      this.setState((prevState) => ({ user: { ...prevState.user, ...details } }));
      this.handleNext();
    }
  };

  onSubmitUserDetails = (details) => {
    const { showParentForm, showGuardianForm, showTeacherForm } = this.state;
    this.setState((prevState) => ({ user: { ...prevState.user, ...details } }));
    if (showParentForm || showGuardianForm || showTeacherForm) {
      this.handleNext();
    } else {
      this.onEditUser(false);
    }
  };

  onSubmitGuardianDetails = (details) => {
    var endTime = details.working_hours_end_time.split('T');
    var startTime = details.working_hours_start_time.split('T');

    var end_time = endTime[1];
    var start_time = startTime[1];
    this.setState(
      (prevState) => ({
        user: {
          ...prevState.user,
          weekly_off: details.weekly_off,
          working_hours_end_time: end_time,
          working_hours_start_time: start_time,
          parent: { ...prevState.user.parent, ...details },
        },
      }),
      () => {
        this.onEditUser(true);
      }
    );
  };

  onEditUser = (requestWithParentorGuradianDetails) => {
    const { user } = this.state;
    const { editUser, history, selectedUser } = this.props;
    let requestObj = user;
    const {
      academic_year,
      branch,
      grade,
      section,
      subjects,
      first_name,
      middle_name,
      last_name,
      gender,
      date_of_birth,
      address,
      contact,
      email,
      profile,
      parent,
      erp_user,
      working_hours_start_time,
      working_hours_end_time,
      weekly_off,
    } = requestObj;
    const {
      father_first_name,
      father_middle_name,
      father_last_name,
      father_email,
      father_mobile,
      father_photo,
      address: parent_address,
      mother_first_name,
      mother_middle_name,
      mother_last_name,
      mother_email,
      mother_mobile,
      mother_photo,
      guardian_first_name,
      guardian_middle_name,
      guardian_last_name,
      guardian_email,
      guardian_mobile,
    } = parent;
    requestObj = {
      erp_id: selectedUser.erp_id,
      academic_year: academic_year.id,
      branch: branch.id,
      grade: grade.map((grade) => grade.id).join(),
      section: section.map((section) => section.id).join(),
      subjects: subjects.map((sub) => sub.id).join(),
      first_name,
      middle_name,
      last_name,
      gender,
      date_of_birth,
      contact,
      email,
      erp_user,
      address,
      working_hours_start_time,
      working_hours_end_time,
      weekly_off: weekly_off.map((wee) => wee.dayVal).join('/'),
      profile,
      father_photo,
      mother_photo,
      parent: {
        id: selectedUser.parent.id,
        father_first_name,
        father_middle_name,
        father_last_name,
        father_email,
        father_mobile,
        address: parent_address,
        mother_first_name,
        mother_middle_name,
        mother_last_name,
        mother_email,
        mother_mobile,
        guardian_first_name,
        guardian_middle_name,
        guardian_last_name,
        guardian_email,
        guardian_mobile,
      },
    };

    if (!requestWithParentorGuradianDetails) {
      delete requestObj.parent;
      delete requestObj.father_photo;
      delete requestObj.mother_photo;
      delete requestObj.working_hours_start_time;
      delete requestObj.working_hours_end_time;
      delete requestObj.weekly_off;
    }
    const { showParentForm, showGuardianForm, showTeacherForm } = this.state;
    console.log(showTeacherForm, 'chhhhhhhh');
    if (!showTeacherForm) {
      requestObj.working_hours_start_time = null;
      requestObj.working_hours_end_time = null;
      requestObj.weekly_off = [];
    }
    const { setAlert } = this.context;
    const requestObjFormData = jsonToFormData(requestObj);
    console.log(requestObjFormData, 'rrrrrrrr');

    editUser(requestObjFormData)
      .then(() => {
        history.push('/user-management/view-users');
        setAlert('success', 'User updated');
      })
      .catch(() => {
        setAlert('error', 'User update failed');
      });
  };

  onSubmitForm = (details) => {
    this.onSubmitGuardianDetails(details);
  };

  fetchUserDetails() {
    const { fetchUser, match } = this.props;

    fetchUser(match.params.id);
  }

  render() {
    const {
      activeStep,
      user,
      showParentForm,
      showGuardianForm,
      showTeacherForm,
    } = this.state;
    const showParentOrGuardianForm =
      showParentForm || showGuardianForm || showTeacherForm;
    const steps = getSteps(showParentOrGuardianForm);
    const { classes, creatingUser, fetchingUserDetails, selectedUser } = this.props;
    return (
      <Layout>
        <div className='edit-user-container'>
          <div className='bread-crumbs-container'>
            <CommonBreadcrumbs
              componentName='User Management'
              childComponentName='Edit User'
            />
          </div>
          {user ? (
            <>
              {/* <Stepper activeStep={activeStep} alternativeLabel className={classes.stepper}>
              {steps.map((label) => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper> */}
              <Stepper
                activeStep={activeStep}
                alternativeLabel
                className={`${classes.stepper} stepper`}
                connector={<CustomStepperConnector />}
              >
                {steps.map((label) => (
                  <Step key={label}>
                    <StepLabel
                      StepIconComponent={CustomStepperIcon}
                      classes={{
                        alternativeLabel: classes.stepLabel,
                      }}
                    >
                      {label}
                    </StepLabel>
                  </Step>
                ))}
              </Stepper>
              <div className={classes.formContainer}>
                {activeStep === 0 && (
                  <SchoolDetailsForm
                    onSubmit={this.onSubmitSchoolDetails}
                    details={user}
                    isEdit={true}
                  />
                )}
                {activeStep === 1 && (
                  <UserDetailsForm
                    onSubmit={this.onSubmitUserDetails}
                    details={user}
                    handleBack={this.handleBack}
                    toggleParentForm={this.toggleParentForm}
                    toggleGuardianForm={this.toggleGuardianForm}
                    toggleTeacherForm={this.toggleTeacherForm}
                    showParentForm={showParentForm}
                    showGuardianForm={showGuardianForm}
                    showTeacherForm={showTeacherForm}
                    isSubmitting={creatingUser}
                  />
                )}
                {activeStep === 2 && selectedUser && (
                  <GuardianDetailsForm
                    onSubmit={this.onSubmitGuardianDetails}
                    details={user}
                    handleBack={this.handleBack}
                    showParentForm={showParentForm}
                    showGuardianForm={showGuardianForm}
                    showTeacherForm={showTeacherForm}
                    isSubmitting={creatingUser}
                  />
                )}
              </div>
            </>
          ) : fetchingUserDetails ? (
            'Loading'
          ) : (
            'Loading'
          )}

          {/* <div>
              <div>
                <Button
                  disabled={activeStep === 0}
                  onClick={this.handleBack}
                  className={classes.backButton}
                >
                  Back
                </Button>
                <Button variant='contained' color='primary' onClick={this.handleNext}>
                  {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                </Button>
              </div>
            </div> */}
        </div>
      </Layout>
    );
  }
}

EditUser.contextType = AlertNotificationContext;

const mapStateToProps = (state) => ({
  creatingUser: state.userManagement.creatingUser,
  fetchingUserDetails: state.userManagement.fetchingUserDetails,
  selectedUser: state.userManagement.selectedUser,
});

const mapDispatchToProps = (dispatch) => ({
  fetchUser: (params) => {
    return dispatch(fetchUser(params));
  },
  editUser: (params) => {
    return dispatch(editUser(params));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(EditUser));
