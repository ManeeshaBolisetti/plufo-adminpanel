/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useRef, useContext, useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { useFormik } from 'formik';
import FormHelperText from '@material-ui/core/FormHelperText';
import Box from '@material-ui/core/Box';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { useStyles } from './useStyles';
import resolveValidationSchema from './schemas/guardian-details';
import ImageUpload from '../../components/image-upload';
import { ContainerContext } from '../../containers/Layout';
import './styles.scss';
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
import {
  DatePicker,
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  KeyboardTimePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/moment';
import moment from 'moment';


const WeeklyOffDays = [
  {
    id:1,
    dayVal:'Mon',
    day:'Monday',
  },
  {
    id:2,
    dayVal:'Tue',
    day:'Tuesday',
  },
  {
    id:3,
    dayVal:'Wed',
    day:'Wednesday',
  },
  {
    id:4,
    dayVal:'Thu',
    day:'Thursday',
  },
  {
    id:5,
    dayVal:'Fri',
    day:'Friday',
  },
  {
    id:6,
    dayVal:'Sat',
    day:'Saturday',
  },
]


const GuardianDetailsForm = ({
  details,
  onSubmit,
  handleBack,
  showParentForm,
  showGuardianForm,
  showTeacherForm,
  isSubmitting,
}) => {
  const { containerRef } = useContext(ContainerContext);
  const themeContext = useTheme();
  const isMobile = useMediaQuery(themeContext.breakpoints.down('sm'));
  const classes = useStyles();
  const validationSchema = resolveValidationSchema(showParentForm, showGuardianForm, showTeacherForm);
  const formik = useFormik({
    initialValues: {
      father_first_name: details?.parent?.father_first_name,
      father_middle_name: details?.parent?.father_middle_name,
      father_last_name: details?.parent?.father_last_name,
      mother_first_name: details?.parent?.mother_first_name,
      mother_middle_name: details?.parent?.mother_middle_name,
      mother_last_name: details?.parent?.mother_last_name,
      father_email: details?.parent?.father_email,
      mother_email: details?.parent?.mother_email,
      father_mobile: details?.parent?.father_mobile,
      mother_mobile: details?.parent?.mother_mobile,
      mother_photo: details?.parent?.mother_photo,
      father_photo: details?.parent?.father_photo,
      address: details?.parent?.address,
      guardian_first_name: details?.parent?.guardian_first_name,
      guardian_middle_name: details?.parent?.guardian_middle_name,
      guardian_last_name: details?.parent?.guardian_last_name,
      guardian_email: details?.parent?.guardian_email,
      guardian_mobile: details?.parent?.guardian_mobile,
      guardian_photo: details?.parent?.guardian_photo,
      working_hours_start_time: details?.working_hours_start_time,
      working_hours_end_time: details?.working_hours_end_time,
      weekly_off: details?.weekly_off,
    },
    validationSchema,
    onSubmit: (values) => {
      console.log("values: ",values)
      onSubmit(values);
    },
    validateOnBlur: false,
    validateOnChange: false,
  });
  const [weekoff, setWeekoff] = useState([]);
  const [weeks , setWeeks] = useState([]);

  useEffect(() => {
    // pageTop.current.scrollIntoView();
    containerRef.current.scrollTop = 0;
    console.log('scrollTop ', containerRef.current.scrollTop);
    setWeekoff(WeeklyOffDays);

  }, []);
  useEffect(() => {
  if(WeeklyOffDays){
    const array1 = formik.values.weekly_off;
    const array2 = WeeklyOffDays;
    for (var i = 0, len = array1.length; i < len; i++){
      for(var j=0, len = array2.length; j < len; j++){
        if (array1[i] === array2[j].dayVal){
          weeks.push(array2[j])
        }
      }
  }
  }
  formik.values.weekly_off=weeks;
  }, []);
  return (
    <>
      {showParentForm && (
        <>
          <div className='details-container parent-form-container'>
            <Typography variant='h5' gutterBottom color='primary'>
              Father's Details
            </Typography>
            <Grid container spacing={4} className='form-grid'>
              <Grid item md={12} xs={12} className='profile-img-container'>
                <ImageUpload
                  id='father-image'
                  value={formik.values.father_photo}
                  onChange={(value) => {
                    formik.setFieldValue('father_photo', value);
                  }}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth color='secondary' size='small'>
                  <InputLabel htmlFor='component-outlined'>First name</InputLabel>
                  <OutlinedInput
                    id='father_first_name'
                    name='father_first_name'
                    onChange={formik.handleChange}
                    inputProps={{ maxLength: 20 }}
                    value={formik.values.father_first_name}
                    label='First name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.father_first_name
                      ? formik.errors.father_first_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Middle name</InputLabel>
                  <OutlinedInput
                    id='father_middle_name'
                    name='father_middle_name'
                    onChange={formik.handleChange}
                    inputProps={{ maxLength: 20 }}
                    value={formik.values.father_middle_name}
                    label='Middle name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.father_middle_name
                      ? formik.errors.father_middle_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Last name</InputLabel>
                  <OutlinedInput
                    id='father_last_name'
                    name='father_last_name'
                    onChange={formik.handleChange}
                    inputProps={{ maxLength: 20 }}
                    value={formik.values.father_last_name}
                    label='Last name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.father_last_name ? formik.errors.father_last_name : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Email ID</InputLabel>
                  <OutlinedInput
                    id='father_email'
                    name='father_email'
                    onChange={formik.handleChange}
                    inputProps={{ maxLength: 40 }}
                    value={formik.values.father_email}
                    label='Email ID'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.father_email ? formik.errors.father_email : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Mobile no.</InputLabel>
                  <OutlinedInput
                    id='father_mobile'
                    name='father_mobile'
                    onChange={formik.handleChange}
                    inputProps={{maxLength:10,pattern:'^[0-9]{10}$'}}
                    placeholder='Ex: 995656xxxx'
                    value={formik.values.father_mobile}
                    label='Mobile no.'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.father_mobile ? formik.errors.father_mobile : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              {/* <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>
                    Alternate mobile no.
                  </InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid> */}
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Address</InputLabel>
                  <OutlinedInput
                    id='address'
                    name='address'
                    inputProps={{ maxLength: 150 }}
                    multiline
                    rows={4}
                    rowsMax={6}
                    onChange={formik.handleChange}
                    value={formik.values.address}
                    label='Address'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.address ? formik.errors.address : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              {/* <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>Address line 2</InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl> 
                    </Grid> */}
            </Grid>
          </div>
          <Divider className={classes.divider} />
          <div className='details-container parent-form-container'>
            <Typography variant='h5' gutterBottom color='primary'>
              Mother's Details
            </Typography>
            <Grid
              container
              spacing={4}
              direction={isMobile ? 'column' : 'row'}
              className='form-grid'
            >
              <Grid item md={12} xs={12} className='profile-img-container'>
                <ImageUpload
                  id='mother-image'
                  value={formik.values.mother_photo}
                  onChange={(value) => {
                    formik.setFieldValue('mother_photo', value);
                  }}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>First name</InputLabel>
                  <OutlinedInput
                    id='mother_first_name'
                    name='mother_first_name'
                    onChange={formik.handleChange}
                    value={formik.values.mother_first_name}
                    label='First name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.mother_first_name
                      ? formik.errors.mother_first_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Middle name</InputLabel>
                  <OutlinedInput
                    id='mother_middle_name'
                    name='mother_middle_name'
                    onChange={formik.handleChange}
                    value={formik.values.mother_middle_name}
                    label='Middle name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.mother_middle_name
                      ? formik.errors.mother_middle_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Last name</InputLabel>
                  <OutlinedInput
                    id='mother_last_name'
                    name='mother_last_name'
                    onChange={formik.handleChange}
                    value={formik.values.mother_last_name}
                    label='Last name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.mother_last_name ? formik.errors.mother_last_name : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Email ID</InputLabel>
                  <OutlinedInput
                    id='mother_email'
                    name='mother_email'
                    onChange={formik.handleChange}
                    value={formik.values.mother_email}
                    label='Email ID'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.mother_email ? formik.errors.mother_email : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Mobile no.</InputLabel>
                  <OutlinedInput
                    id='mother_mobile'
                    name='mother_mobile'
                    inputProps={{maxLength:10,pattern:'^[0-9]{10}$'}}
                    placeholder='Ex: 995656xxxx'
                    onChange={formik.handleChange}
                    value={formik.values.mother_mobile}
                    label='Mobile no.'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.mother_mobile ? formik.errors.mother_mobile : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              {/* <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>
                    Alternate mobile no.
                  </InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid> */}
              {/* <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>Address line 1</InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid>
              <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>Address line 2</InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid> */}
            </Grid>
          </div>
        </>
      )}

      {showGuardianForm && (
        <>
          <Divider className={classes.divider} />
          <div className='details-container guardian-form-container'>
            <Typography variant='h5' gutterBottom color='primary'>
              Guardian Details
            </Typography>
            <Grid container spacing={4}>
              <Grid item md={12} xs={12} className='profile-img-container'>
                <ImageUpload
                  id='mother-image'
                  value={formik.values.guardian_photo}
                  onChange={(value) => {
                    formik.setFieldValue('guardian_photo', value);
                  }}
                />
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>First name</InputLabel>
                  <OutlinedInput
                    id='guardian_first_name'
                    name='guardian_first_name'
                    onChange={formik.handleChange}
                    value={formik.values.guardian_first_name}
                    label='First name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.guardian_first_name
                      ? formik.errors.guardian_first_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Middle name</InputLabel>
                  <OutlinedInput
                    id='guardian_middle_name'
                    name='guardian_middle_name'
                    onChange={formik.handleChange}
                    value={formik.values.guardian_middle_name}
                    label='Middle name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.guardian_middle_name
                      ? formik.errors.guardian_middle_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Last name</InputLabel>
                  <OutlinedInput
                    id='guardian_last_name'
                    name='guardian_last_name'
                    onChange={formik.handleChange}
                    value={formik.values.guardian_last_name}
                    label='Last name'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.guardian_last_name
                      ? formik.errors.guardian_last_name
                      : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Email ID</InputLabel>
                  <OutlinedInput
                    id='guardian_email'
                    name='guardian_email'
                    onChange={formik.handleChange}
                    value={formik.values.guardian_email}
                    label='Email ID'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.guardian_email ? formik.errors.guardian_email : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item md={4} xs={12}>
                <FormControl variant='outlined' fullWidth size='small'>
                  <InputLabel htmlFor='component-outlined'>Mobile no.</InputLabel>
                  <OutlinedInput
                    id='guardian_mobile'
                    name='guardian_mobile'
                    onChange={formik.handleChange}
                    value={formik.values.guardian_mobile}
                    inputProps={{maxLength:10,pattern:'^[0-9]{10}$'}}
                    placeholder='Ex: 995656xxxx'
                    label='Mobile no.'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.guardian_mobile ? formik.errors.guardian_mobile : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              {/* <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>
                    Alternate mobile no.
                  </InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid> */}
              {/* <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>Address line 1</InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid>
              <Grid item md={4}>
                <FormControl variant='outlined' fullWidth disabled>
                  <InputLabel htmlFor='component-outlined'>Address line 2</InputLabel>
                  <OutlinedInput
                    id='component-outlined'
                    value=''
                    onChange={() => {}}
                    label='Name'
                  />
                </FormControl>
              </Grid> */}

              {/* <Grid item md={4}>
                <Button startIcon={<AttachFileIcon />}>Attach Image</Button>
              </Grid> */}
            </Grid>
          </div>
        </>
      )}

    {showTeacherForm && (
        <>
          <Divider className={classes.divider} />
          <div className='details-container teacher-form-container'>
            <Typography variant='h5' gutterBottom color='primary'>
              Teacher's Details
            </Typography>
            <Grid container spacing={4} className='form-grid'>
            <Grid item md={4} xs={12}>
              <FormControl
                  color='secondary'
                  fullWidth
                  className={classes.margin}
                  variant='outlined'
                >
                  <Autocomplete
                    id='weekly_off'
                    name='weekly_off'
                    onChange={(e, value) => {
                      let arrayWeeks = value.findIndex(item => item.id === 0) !== -1? weekoff.filter(item => item.id !== 0) : value
                      formik.setFieldValue('weekly_off', arrayWeeks);
                    }}
                    value={(formik.values.weekly_off)?formik.values.weekly_off:[]||''}
                    limitTags={2}
                    multiple
                    options={weekoff || []}
                    getOptionLabel={(option) => option.day || ''}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant='outlined'
                        label='Weekly off Days'
                        placeholder='Weekly off Days'
                      />
                    )}
                    getOptionSelected={(option, value) => option.id == value.id}
                    size='small'
                  />
                  <FormHelperText style={{ color: 'red' }}>
                    {formik.errors.weekly_off ? formik.errors.weekly_off : ''}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <Typography>Working Hours</Typography>
                   <Divider />
              </Grid>     
                  <Grid item md={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardTimePicker
                      views={["hours", "minutes", "seconds"]}
                        value={formik.values.working_hours_start_time || null}
                        onChange={(value) => {
                          formik.setFieldValue('working_hours_start_time', moment(value).format('YYYY-MM-DD[T]HH:mm:ss'));
                        }}
                        // format='YYYY-MM-DD'
                        // maxDate={new Date()}
                        size='small'
                        inputVariant='outlined'
                        fullWidth
                        label='Start Time'
                        KeyboardButtonProps={{
                          'aria-label': 'change time',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                    <FormHelperText style={{ color: 'red' }}>
                      {formik.errors.working_hours_start_time ? formik.errors.working_hours_start_time : ''}
                    </FormHelperText>
                  </Grid>
                  <Grid item md={4} xs={12}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardTimePicker
                      views={["hours", "minutes", "seconds"]}
                        value={formik.values.working_hours_end_time || null}
                        onChange={(value) => {
                          formik.setFieldValue('working_hours_end_time', moment(value).format('YYYY-MM-DD[T]HH:mm:ss'));
                        }}
                        // format='YYYY-MM-DD'
                        // maxDate={new Date()}
                        size='small'
                        inputVariant='outlined'
                        fullWidth
                        label='End Time'
                        KeyboardButtonProps={{
                          'aria-label': 'change time',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                    <FormHelperText style={{ color: 'red' }}>
                      {formik.errors.working_hours_end_time ? formik.errors.working_hours_end_time : ''}
                    </FormHelperText>
                  </Grid>
             
            </Grid>
          </div>
        </>
      )}

      <Grid
        container
        item
        xs={12}
        style={{ marginTop: '20px' }}
        direction={isMobile ? 'column-reverse' : 'row'}
        spacing={3}
        className='form-action-btn-container'
      >
        <Grid item md='1'>
          <Box display='flex' justifyContent={isMobile ? 'center' : ''}>
            <Button
              className={`${classes.formActionButton} disabled-btn`}
              variant='contained'
              color='primary'
              onClick={handleBack}
            >
              Back
            </Button>
          </Box>
        </Grid>
        <Grid item md='1'>
          <Box display='flex' justifyContent={isMobile ? 'center' : ''}>
            <Button
              className={classes.formActionButton}
              variant='contained'
              color='primary'
              onClick={() => {
                formik.handleSubmit();
              }}
              disabled={isSubmitting}
            >
              Submit
            </Button>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};

export default GuardianDetailsForm;
