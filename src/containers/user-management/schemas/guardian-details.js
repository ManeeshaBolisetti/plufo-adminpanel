import * as Yup from 'yup';

const phoneRegExp = /^\+?1?\d{10}$/;

const validationSchema = (validateParent, validateGuardian, validateTeacher) => {
  const parentValidationObj = Yup.object({
    father_first_name: Yup.string().required('Required'),
    father_last_name: Yup.string().required('Required'),
    mother_first_name: Yup.string().required('Required'),
    mother_last_name: Yup.string().required('Required'),
    father_email: Yup.string().email('Provide a valid email').required('Required'),
    mother_email: Yup.string().email('Provide a valid email').required('Required'),
    father_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    mother_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    address: Yup.string().required('Required'),
  });

  const guardianValidationObj = Yup.object({
    guardian_first_name: Yup.string().required('Required'),
    guardian_last_name: Yup.string().required('Required'),
    guardian_email: Yup.string().email('Provide a valid email').required('Required'),
    guardian_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
  });
  const teacherValidationObj = Yup.object({
    working_hours_start_time: Yup.string().required('Required'),
    working_hours_end_time: Yup.string().required('Required'),
  });
  const parentTeacherValidationObj = Yup.object({
    father_first_name: Yup.string().required('Required'),
    father_last_name: Yup.string().required('Required'),
    mother_first_name: Yup.string().required('Required'),
    mother_last_name: Yup.string().required('Required'),
    father_email: Yup.string().email('Provide a valid email').required('Required'),
    mother_email: Yup.string().email('Provide a valid email').required('Required'),
    father_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    mother_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    address: Yup.string().required('Required'),
    working_hours_start_time: Yup.string().required('Required'),
    working_hours_end_time: Yup.string().required('Required'),
  });
  const guardianTeacherValidationObj = Yup.object({
    guardian_first_name: Yup.string().required('Required'),
    guardian_last_name: Yup.string().required('Required'),
    guardian_email: Yup.string().email('Provide a valid email').required('Required'),
    guardian_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    working_hours_start_time: Yup.string().required('Required'),
    working_hours_end_time: Yup.string().required('Required'),
  });


  const parentGuardianValidationObj = Yup.object({
    father_first_name: Yup.string().required('Required'),
    father_last_name: Yup.string().required('Required'),
    mother_first_name: Yup.string().required('Required'),
    mother_last_name: Yup.string().required('Required'),
    father_email: Yup.string().email('Provide a valid email').required('Required'),
    mother_email: Yup.string().email('Provide a valid email').required('Required'),
    father_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    mother_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    address: Yup.string().required('Required'),
    guardian_first_name: Yup.string().required('Required'),
    guardian_last_name: Yup.string().required('Required'),
    guardian_email: Yup.string().email('Provide a valid email').required('Required'),
    guardian_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
  });

  const parentGuardianTeacherValidationObj = Yup.object({
    father_first_name: Yup.string().required('Required'),
    father_last_name: Yup.string().required('Required'),
    mother_first_name: Yup.string().required('Required'),
    mother_last_name: Yup.string().required('Required'),
    father_email: Yup.string().email('Provide a valid email').required('Required'),
    mother_email: Yup.string().email('Provide a valid email').required('Required'),
    father_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    mother_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    address: Yup.string().required('Required'),
    guardian_first_name: Yup.string().required('Required'),
    guardian_last_name: Yup.string().required('Required'),
    guardian_email: Yup.string().email('Provide a valid email').required('Required'),
    guardian_mobile: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Required'),
    working_hours_start_time: Yup.string().required('Required'),
    working_hours_end_time: Yup.string().required('Required'),
  });

  if (validateParent && validateGuardian & validateTeacher) {
    return parentGuardianTeacherValidationObj;
  }
  if (validateParent && validateGuardian) {
    return parentGuardianValidationObj;
  }
  if (validateTeacher && validateGuardian) {
    return guardianTeacherValidationObj;
  }
  if (validateTeacher && validateParent) {
    return parentTeacherValidationObj;
  }
  if(validateParent){
    return parentValidationObj;
  }
  if(validateTeacher){
    return teacherValidationObj;
  }
  return guardianValidationObj;
};

export default validationSchema;
