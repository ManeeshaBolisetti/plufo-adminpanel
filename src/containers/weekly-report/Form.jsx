import React, { useState, useEffect, useContext } from 'react';
import { Autocomplete, Pagination } from '@material-ui/lab';
import './style.scss';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  MenuItem,
  Radio,
  RadioGroup,
  FormControlLabel,
  FormLabel,
  ClickAwayListener,
  InputAdornment,
} from '@material-ui/core';
import {
  LocalizationProvider,
  DateRangePicker,
  DateRange,
  DateRangeDelimiter,
} from '@material-ui/pickers-4.2';
import DateRangeIcon from '@material-ui/icons/DateRange';
import moment from 'moment';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import './style.scss';

const Form = (props) => {
  const { setDialogOpen, step, setStep, dialogDetail, editData, getWeeklyReportTable } =
    props || {};
  const [datePopperOpen, setDatePopperOpen] = useState(false);
  const [dateRange, setDateRange] = useState([
    moment().subtract(1, 'weeks').startOf('week'),
    moment().subtract(1, 'weeks').endOf('week'),
  ]);
  const [feedback, setFeedback] = useState({
    topic: '',
    achievement: '',
    learningGoals: '',
    skillRemark: '',
    friendlyCourteous: '',
    attentive: '',
    punctual: '',
    personalityRemark: '',
    studentEmailValidation: '',
    teacherEmailValidation: '',
  });
  //   const [modalOpenFor, setModalOpenFor] = useState('Create');

  const { setAlert } = useContext(AlertNotificationContext);
  const { token } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const { is_superuser } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const { erp_user_id } =
    JSON.parse(localStorage.getItem('userDetails')).role_details || {};

  const [studentList, setStudentList] = useState(null);
  const [studentDates, setStudentDates] = useState(null);
  const [teacherList, setTeacherList] = useState(null);
  const [studentID, setStudentID] = useState(undefined);
  const [teacherID, setTeacherID] = useState(undefined);
  const [selectedTeacher, setSelectedTeacher] = useState(null);
  const [selectedStudent, setSelectedStudent] = useState(null);
  const [availableSlot, setAvailableSlot] = useState(null);
  const [searchText, setSearchText] = useState('');
  const [date, setDate] = useState('');
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [gradeList, setGradeList] = useState([]);
  const [gradeID, setGradeID] = useState(undefined);
  const [subjectID, setSubjectID] = useState(undefined);
  const [selectedGrade, setSelectedGrade] = useState(null);
  const [subjectList, setSubjectList] = useState([]);
  const [selectedSubject, setSelectedSubject] = useState(null);
  const [finalAchievement, setFinalAchievement] = useState('');
  const [helperText, setHelperText] = useState({
    teacher: '',
    teacherError: false,
    student: '',
    studentError: false,
    teacherListError: false,
    studentListError: false,
    dateRangeError: false,
    gradeListError: false,
    courseListError: false,
  });

  useEffect(() => {
    // getstudentList(searchText);
    getTeacherList();
    getFullDates();
    // getMeetingData(data.currentPage);

    var date = new Date();
    setDate(moment(date).format('YYYY-MM-DD'));
  }, []);

  useEffect(() => {
    if (selectedStudent) {
      getGradeList();
      getSubjectList();
    }
  }, [selectedStudent])

  useEffect(() => {
    getstudentList();
  }, [teacherID, startDate, endDate]);

  const getTeacherList = async () => {
    let page = '0';
    let resultCount = '0';
    const result = await axiosInstance.get(`${endpoints.students.getTeacherList}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (result.data.status_code === 200) {
      console.log(result, 'result');
      setTeacherList(result.data.result);
      resultCount = result.data.count;
      page = result.data.total_pages;
    }
    if (parseInt(page) > 1) {
      getFullTeacherList(resultCount);
    }
  };

  const getFullTeacherList = async (pagesize) => {
    const result = await axiosInstance.get(
      `${endpoints.students.getTeacherList}?page_size=${pagesize}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (result.data.status_code === 200) {
      console.log(result, 'result');
      setTeacherList(result.data.result);
      console.log('listmerged');
    }
  };

  const getGradeList = async () => {
    const result = await axiosInstance.get(
      `${endpoints.mappingStudentGrade.grade}?branch_id=1`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (result.data.status_code === 200) {
      console.log('result111', result.data.data);
      console.log('selectedStudent:', selectedStudent)
      let data = selectedStudent?.grade_ids?.map((item) => {
        let data1 = result?.data?.data.map((gradeI) => {
          if (item === gradeI?.grade_id) {
            // setGradeList(gradeI);
            gradeList.push(gradeI)
          }
        })
      })
    }
  };

  const getSubjectList = async () => {
    const result = await axiosInstance.get(
      `${endpoints.communication.subjectList}?branch_id=1`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (result.data.status_code === 201) {
      console.log(result.data.result, 'result567');
      let id = selectedStudent?.subject_id;
      let data = result.data.result.map((n) => {
        if (n?.id === id) {
          // setSubjectList(result.data.result);
          subjectList.push(n);
        }
      })
      console.log('listmerged');
    }
  };

  const getstudentList = async (searchText) => {
    let page = '0';
    let resultCount = '0';
    let query = '';
    let startDates = '';
    let endDates = '';
    if (is_superuser) {
      if (teacherID !== undefined) {
        query = `&teacher_id=${teacherID}`;
      }
    } else {
      query = `&teacher_id=${erp_user_id}`;
    }
    if (startDate) {
      startDates = `&start_date=${startDate}`;
    }
    if (endDate) {
      endDates = `&end_date=${endDate}`;
    }
    if (query.length > 0) {
      const result = await axiosInstance.get(
        `${endpoints.students.connectionPod}?term=${searchText !== undefined ? searchText : ''
        }${query}${startDates}${endDates}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (result.data.status_code === 200) {
        console.log(result, 'result');
        setStudentList(result.data.result);
        resultCount = result.data.count;
        page = result.data.total_pages;
      }
      if (parseInt(page) > 1) {
        getFullStudentList(resultCount);
      }
    }
  };
  const getFullStudentList = async (pagesize) => {
    console.log('getFullStudentList');
    let query = '';
    if (is_superuser) {
      if (teacherID !== undefined) {
        query = `&teacher_id=${teacherID}`;
      }
    } else {
      query = `&teacher_id=${erp_user_id}`;
    }
    const result = await axiosInstance.get(
      `${endpoints.students.connectionPod}?term=${searchText}${query}&page_size=${pagesize}&start_date=${startDate}&end_date=${endDate}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    if (result.data.status_code === 200) {
      console.log(result, 'result');
      setStudentList(result.data.result);
    }
  };
  const getFullDates = async (pagesize) => {
    console.log('getFullDates');
    // let query = '';
    // if (is_superuser) {
    //   if (teacherID !== undefined) {
    //     query = `&teacher_id=${teacherID}`;
    //   }
    // } else {
    //   query = `&teacher_id=${erp_user_id}`;
    // }
    const result = await axiosInstance.get(
      `${endpoints.weeklyReport.gettingDates}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    console.log('result', result)
    if (result.status === 200) {
      console.log('result', result.data);
      setStudentDates(result.data);
    }
  };

  const handleNext = (e) => {
    e.preventDefault();
    if (selectedTeacher !== null) {
      setHelperText({
        ...helperText,
        teacherListError: false,
      });
    } else {
      setHelperText({
        ...helperText,
        teacherListError: true,
      });
    }
    if (selectedStudent === null) {
      setHelperText({
        ...helperText,
        studentListError: true,
      });
    } else {
      setHelperText({
        ...helperText,
        studentListError: false,
      });
    }
    if (dateRange === null) {
      setHelperText({
        ...helperText,
        dateRangeError: true,
      });
    } else {
      setHelperText({
        ...helperText,
        dateRangeError: false,
      });
    }
    if (selectedGrade === null) {
      setHelperText({
        ...helperText,
        gradeListError: true,
      });
    } else {
      setHelperText({
        ...helperText,
        gradeListError: false,
      });
    }
    if (selectedSubject === null) {
      setHelperText({
        ...helperText,
        courseListError: true,
      });
    } else {
      setHelperText({
        ...helperText,
        courseListError: false,
      });
    }
    if (step === 1) {
      console.log(
        selectedTeacher,
        selectedStudent,
        selectedGrade,
        selectedSubject,
        'inside step'
      );
      if (
        selectedTeacher !== null &&
        selectedStudent !== null &&
        selectedGrade !== null &&
        selectedSubject !== null
      ) {
        console.log('inside if', step);
        setStep(step + 1);
      }
    }
    if (step === 2) {
      console.log(
        feedback.topic.length,
        feedback.achievement.length,
        feedback.learningGoals.length,
        feedback.skillRemark.length,
        'inside step'
      );
      if (
        feedback.topic.length &&
        feedback.achievement.length &&
        feedback.learningGoals.length &&
        feedback.skillRemark.length
      ) {
        setStep(step + 1);
      }
    }
  };

  const nextButtonDisable = () => {
    if (step === 1) {
      if (
        selectedTeacher === null ||
        selectedStudent === null ||
        selectedGrade === null ||
        selectedSubject === null
      ) {
        return true;
      } else {
        return false;
      }
    }
  };

  const handleBack = (e) => {
    e.preventDefault();
    setStep(step - 1);
  };
  const handleOnChange = (event) => {
    setFeedback({
      ...feedback,
      [event.target.name]: event.target.value.trimLeft(),
    });
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (event.target.name === 'studentEmailValidation') {
      if (!emailRegex.test(event.target.value.trimLeft())) {
        setHelperText({
          ...helperText,
          student: 'Invalid email',
          studentError: true,
        });
      } else {
        setHelperText({
          ...helperText,
          student: '',
          studentError: false,
        });
      }
    }
    if (event.target.name === 'teacherEmailValidation') {
      if (!emailRegex.test(event.target.value.trimLeft())) {
        setHelperText({
          ...helperText,
          teacher: 'Invalid email',
          teacherError: true,
        });
      } else {
        setHelperText({
          ...helperText,
          teacher: '',
          teacherError: false,
        });
      }
    }
  };

  const handleGrade = (event, value) => {
    setSelectedGrade(value);
    setGradeID(value.id);
  };

  const confirm = () => {
    if (dialogDetail === 'Create') {
      axiosInstance
        .post(endpoints.weeklyReport.create, {
          // .post(`https://dev.plufo.letseduvate.com/qbox/course_extend/weekly_report/`, {
          student: studentID,
          student_name: selectedStudent.name,
          teacher: selectedTeacher.name,
          teacher_id: teacherID,
          grade: selectedGrade.grade__grade_name,
          course: selectedSubject.subject__subject_name,
          topic: feedback.topic,
          achievement: feedback.achievement,
          learning_goals: feedback.learningGoals,
          remarks: feedback.skillRemark,
          friendly_courteous: feedback.friendlyCourteous,
          attentive: feedback.attentive,
          punctual: feedback.punctual,
          remark: feedback.personalityRemark,
          final_achievement: finalAchievement,
          student_mail: feedback.studentEmailValidation,
          teacher_mail: feedback.teacherEmailValidation,
          subject: `Weekly Report ${moment(dateRange[0])
            .format('DD MMM')
            .toUpperCase()} - ${moment(dateRange[1]).format('DD MMM').toUpperCase()}`,
        })
        .then((res) => {
          getWeeklyReportTable();
          setAlert('success', res.data.message);
          setStep(1);
        })
        .catch((err) => {
          console.log(err, 'resultaaaa 11');
        });
      setDialogOpen(false);
    }
    if (dialogDetail === 'Edit') {
      let editObj = {
        report_id: editData.id,
        teacher: selectedTeacher.name,
        grade: selectedGrade.grade__grade_name,
        course: selectedSubject.subject__subject_name,
        topic: feedback.topic,
        achievement: feedback.achievement,
        learning_goals: feedback.learningGoals,
        remarks: feedback.skillRemark,
        friendly_courteous: feedback.friendlyCourteous,
        attentive: feedback.attentive,
        punctual: feedback.punctual,
        remark: feedback.personalityRemark,
        final_achievement: finalAchievement,
        student_mail: feedback.studentEmailValidation,
        teacher_mail: feedback.teacherEmailValidation,
      };
      console.log(editObj, 'edittttttttt');
      axiosInstance
        .put(
          endpoints.weeklyReport.list,
          // .put(
          //   `https://dev.plufo.letseduvate.com/qbox/course_extend/weekly_report_list/`,

          editObj
        )
        .then((res) => {
          getWeeklyReportTable();
          setAlert('success', res.data.message);
          setStep(1);
        })
        .catch((err) => {
          console.log(err, 'resultaaaa 11');
        });
      setDialogOpen(false);
    }
  };

  const closeDialog = () => {
    setDialogOpen(false);
    setStep(1);
  };

  const handleTeacher = (event, value) => {
    console.log(value, 'nnnnn');
    setSelectedTeacher(value);
    setTeacherID(value?.id);
    setStartDate(null);
    setEndDate(null);
    setSelectedStudent(null);
    setGradeList([]);
    setSelectedGrade(null);
    setSubjectList([]);
    setSelectedSubject(null);
    // getstudentList();
    // if (value && value?.length) {
    //   setSelectedTeacher(value);
    //   setTeacherID(value.id);
    // }
  };
  const handleDates = (event, value) => {
    console.log(value.start_date, 'nnnnn');
    setStartDate(value?.start_date);
    setEndDate(value?.end_date);
    // setSelectedTeacher(value);
    // setTeacherID(value?.id);
    // getstudentList();
    // if (value && value?.length) {
    //   setSelectedTeacher(value);
    //   setTeacherID(value.id);
    // }
  };
  const handleStudent = (event, value) => {
    setSelectedStudent(value);
    console.log('student:', value)
    setStudentID(value?.student_id);
    setGradeList([]);
    setSelectedGrade(null);
    setSubjectList([]);
    setSelectedSubject(null);
    // if (value && value?.length) {
    //   setSelectedStudent(value);
    //   setStudentID(value.student_id);
    // }
  };

  const handleSubject = (event, value) => {
    setSelectedSubject(value);
    setSubjectID(value.id);

    // if (value && value?.length) {
    //   setSelectedSubject(value);
    //   //   getCourseList(value.map(item => item.grade_id));
    // }
  };

  useEffect(() => {
    if (dialogDetail === 'Edit') {
      console.log(editData, 'edit');

      setFeedback({
        ...feedback,
        topic: editData.topics_covered,
        achievement: editData.achievements,
        learningGoals: editData.learning_goals,
        skillRemark: editData.remarks,
        friendlyCourteous: editData.freindly_courteous,
        attentive: editData.attentive,
        punctual: editData.puntual,
        personalityRemark: editData.remark,
        studentEmailValidation: editData.student_mail,
        teacherEmailValidation: editData.teacher_mail,
      });
      setFinalAchievement(editData.final_achievement);
    }
  }, [dialogDetail]);

  useEffect(() => {
    if (dialogDetail === 'Edit') {
      let currentTeacher =
        teacherList &&
        teacherList.filter((eachList) => eachList.name === editData.teacher_name);

      teacherList && setSelectedTeacher(currentTeacher[0]);
      teacherList && setTeacherID(currentTeacher[0]?.id);
      currentTeacher && getstudentList();
      console.log(currentTeacher, 'teaaaaaaa');
    }
  }, [teacherList]);

  useEffect(() => {
    if (dialogDetail === 'Edit') {
      //   console.log(
      //     editData.student_name,
      //     studentList &&
      //       studentList.filter((eachList) => eachList.name === editData.student_name),
      //     'nnnnn'
      //   );
      let currentStudent =
        studentList &&
        studentList.filter((eachList) => eachList.name === editData.student_name);
      console.log(
        studentList && studentList.filter((eachList) => eachList.id === 17510),
        studentList,
        editData.student_name,
        'Cccccc'
      );

      studentList && setSelectedStudent(currentStudent[0]);
      studentList && setStudentID(currentStudent[0]?.student_id);
    }
  }, [studentList]);

  useEffect(() => {
    if (dialogDetail === 'Edit') {
      let currentGrade =
        gradeList &&
        gradeList.filter((eachList) => eachList.grade__grade_name === editData.grade);

      gradeList && setSelectedGrade(currentGrade[0]);
      gradeList && setGradeID(currentGrade[0].id);
      console.log(currentGrade, gradeList, editData.grade, 'teaaaaaaa');
    }
  }, [gradeList]);

  useEffect(() => {
    if (dialogDetail === 'Edit') {
      let currentCourse =
        subjectList &&
        subjectList.filter(
          (eachList) => eachList.subject__subject_name === editData.course
        );

      subjectList && setSelectedSubject(currentCourse[0]);
      subjectList && setSubjectID(currentCourse[0].id);
      console.log(currentCourse, subjectList, editData.course, 'teaaaaaaa');
    }
  }, [subjectList]);

  // const handleNextFirstStep = () =>{
  //   if(selectedTeacher===null || selectedStudent===null || dateRange===null || selectedGrade===null || selectedSubject===null){
  //     return true
  //   }
  //   return false
  // }
  console.log('Dates', studentDates);
  return (
    <div>
      {step === 1 ? (
        <div className='weekly-report'>
          {/* {is_superuser && ( */}
          <Autocomplete
            fullWidth
            size='small'
            className='filter-teacher weekly-report-input'
            options={(teacherList && teacherList) || []}
            getOptionLabel={(option) => option.name || ''}
            filterSelectedOptions
            onChange={handleTeacher}
            value={selectedTeacher || ''}
            disabled={dialogDetail === 'Edit'}
            // onChange={
            //   ((e) =>
            //     e.target.value.length > -1 && e.target.value.length < 21
            //       ? handleOnChange(e.target.value, 'student_name')
            //       : '',
            //   setTeacherID(e.target.value.id))
            // }
            renderInput={(params) => (
              <TextField
                {...params}
                required
                fullWidth
                variant='outlined'
                label='Teacher'
                error={helperText.teacherListError}
              />
            )}
            renderOption={(option, { selected }) => (
              <React.Fragment>{option.name}</React.Fragment>
            )}
          />
          <Autocomplete
            fullWidth
            size='small'
            className='filter-teacher weekly-report-input'
            options={(studentDates) || []}
            getOptionLabel={(option) => option?.start_date + ' - ' + option?.end_date || ''}
            filterSelectedOptions
            onChange={handleDates}
            // value={selectedTeacher || ''}
            disabled={dialogDetail === 'Edit'}
            // onChange={
            //   ((e) =>
            //     e.target.value.length > -1 && e.target.value.length < 21
            //       ? handleOnChange(e.target.value, 'student_name')
            //       : '',
            //   setTeacherID(e.target.value.id))
            // }
            renderInput={(params) => (
              <TextField
                {...params}
                required
                fullWidth
                variant='outlined'
                label='Select Dates'
                error={helperText.teacherListError}
              />
            )}
            renderOption={(option, { selected }) => (
              <React.Fragment>{option.start_date} - {option.end_date}</React.Fragment>
            )}
          />

          <Autocomplete
            fullWidth
            size='small'
            className='filter-student weekly-report-input'
            options={(studentList && studentList) || []}
            getOptionLabel={(option) => option.name || ''}
            filterSelectedOptions
            value={selectedStudent || ''}
            onChange={handleStudent}
            disabled={dialogDetail === 'Edit'}
            // onChange={
            //   ((e) =>
            //     e.target.value.length > -1 && e.target.value.length < 21
            //       ? handleOnChange(e.target.value, 'student_name')
            //       : '',
            //   setStudentID(e.target.value.student_id))
            // }
            renderInput={(params) => (
              <TextField
                {...params}
                required
                fullWidth
                variant='outlined'
                label='Student'
                error={helperText.studentListError}

              />
            )}
            renderOption={(option, { selected }) => (
              <React.Fragment>
                {option?.is_shifted === true ?
                  (
                    <div style={{ color: 'red' }}>
                      {option.name}
                    </div>
                  )
                  :
                  (
                    <div>
                      {option.name}
                    </div>
                  )
                }
              </React.Fragment>
            )}
          />
          <ClickAwayListener
            onClickAway={(e) => {
              setDatePopperOpen(false);
            }}
          >
            <LocalizationProvider
              dateAdapter={MomentUtils}
              style={{ backgroundColor: '#F9F9F9' }}
            >
              {dialogDetail === 'Create' && (
                <DateRangePicker
                  id='date-range-picker-date'
                  disableCloseOnSelect={false}
                  startText='Select-dates'
                  PopperProps={{ open: datePopperOpen }}
                  // endText='End-date'
                  value={dateRange}
                  disableFuture
                  minDate={moment().subtract(2, 'weeks').startOf('week')}
                  maxDate={new Date()}
                  onChange={(newValue) => {
                    const [startDate, endDate] = newValue;
                    const sevenDaysAfter = moment(startDate).add(6, 'days');
                    setDateRange([startDate, sevenDaysAfter]);
                    setDatePopperOpen(false);
                  }}
                  renderInput={({ inputProps, ...startProps }, endProps) => {
                    return (
                      <>
                        {/* <TextField
                // {...params}
                required
                size='small'
                variant='outlined'
                label='Grade'
                placeholder='Grade'
                error={helperText.gradeListError}
              /> */}
                        {/* <TextField
                          {...startProps}
                          InputProps={{
                            ...inputProps,
                            value: `${moment(inputProps.value).format(
                              'MM-DD-YYYY'
                            )} - ${moment(endProps.inputProps.value).format(
                              'MM-DD-YYYY'
                            )}`,
                            readOnly: true,
                            endAdornment: (
                              <InputAdornment position='start'>
                                <DateRangeIcon
                                  style={{ width: '35px' }}
                                  color='primary'
                                />
                              </InputAdornment>
                            ),
                          }}
                          size='small'
                          style={{ width: '100%' }}
                          onClick={() => {
                            setDatePopperOpen(true);
                          }}
                          error={helperText.dateRangeError}
                        />
                        {/* <TextField {...startProps} size='small' /> */}
                        {/* <DateRangeDelimiter> to </DateRangeDelimiter> */}
                        {/* <TextField {...endProps} size='small' /> */}
                      </>
                    );
                  }}
                />
              )}
            </LocalizationProvider>
          </ClickAwayListener>
          <Autocomplete
            fullWidth
            size='small'
            className='filter-student weekly-report-input'
            options={gradeList || []}
            getOptionLabel={(option) => option?.grade__grade_name || ''}
            filterSelectedOptions
            // value={selectedGrade || ''}
            // onChange={(event, value) => {
            //   setGradeID(value.id);
            //   // setSelectedStudent(value);
            // }}
            value={selectedGrade || ''}
            onChange={handleGrade}
            renderInput={(params) => (
              <TextField
                {...params}
                required
                size='small'
                variant='outlined'
                label='Grade'
                placeholder='Grade'
                error={helperText.gradeListError}
              />
            )}
          />
          <Autocomplete
            fullWidth
            size='small'
            className='filter-student weekly-report-input'
            options={subjectList || []}
            getOptionLabel={(option) => option?.subject__subject_name || ''}
            filterSelectedOptions
            // value={selectedGrade || ''}
            // onChange={(event, value) => {
            //   setGradeID(value.id);
            //   // setSelectedStudent(value);
            // }}
            value={selectedSubject || ''}
            onChange={handleSubject}
            renderInput={(params) => (
              <TextField
                {...params}
                required
                fullWidth
                variant='outlined'
                label='Course'
                error={helperText.courseListError}
              />
            )}
          />
          <div className='weekly-report-actions'>
            {step === 1 ? (
              <Button
                className='weekly-dialog-actions-butons'
                onClick={(e) => handleNext(e)}
                disabled={
                  selectedTeacher === null ||
                  selectedStudent === null ||
                  selectedGrade === null ||
                  selectedSubject === null
                }
              >
                Next
              </Button>
            ) : (
              <Button className='weekly-dialog-actions-butons' onClick={() => confirm()}>
                Confirm
              </Button>
            )}
            {step !== 1 ? (
              <Button
                className='weekly-dialog-actions-butons'
                onClick={(e) => handleBack(e)}
              >
                Previous
              </Button>
            ) : (
              ''
            )}
            <Button
              className='weekly-dialog-actions-butons'
              onClick={() => closeDialog()}
            >
              Cancel
            </Button>
          </div>
        </div>
      ) : step === 2 ? (
        <div className='weekly-report'>
          {/* {is_superuser && ( */}
          {/* <DialogTitle className='dialog-title'>SKILLS AND ABILITIES</DialogTitle> */}

          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Topic'
            variant='outlined'
            size='small'
            autoComplete='off'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.topic}
            name='topic'
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 301
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.topic.length > 0
              ? `${feedback.topic !== undefined ? 300 - feedback.topic.length : ''
              } characters
            remaining`
              : ''}
          </p>
          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Achievement'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='achievement'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.achievement}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 301
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.achievement.length > 0
              ? `${feedback.achievement !== undefined
                ? 300 - feedback.achievement.length
                : ''
              } characters
            remaining`
              : ''}
            {/* {feedback.achievement !== undefined ? 300 - feedback.achievement.length : ''}{' '}
            characters remaining */}
          </p>
          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Learning Goals'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='learningGoals'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.learningGoals}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 301
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.learningGoals.length > 0
              ? `${feedback.learningGoals !== undefined
                ? 300 - feedback.learningGoals.length
                : ''
              } characters
            remaining`
              : ''}
            {/* {feedback.learningGoals !== undefined
              ? 300 - feedback.learningGoals.length
              : ''}{' '}
            characters remaining */}
          </p>

          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Remark'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='skillRemark'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.skillRemark}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 401
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.skillRemark.length > 0
              ? `${feedback.skillRemark !== undefined
                ? 300 - feedback.skillRemark.length
                : ''
              } characters
            remaining`
              : ''}

            {/* {feedback.skillRemark !== undefined ? 400 - feedback.skillRemark.length : ''}{' '}
            characters remaining */}
          </p>

          <div className='weekly-report-actions'>
            {step === 2 ? (
              <Button
                className='weekly-dialog-actions-butons'
                onClick={(e) => handleNext(e)}
                disabled={
                  feedback.topic.length < 1 ||
                  feedback.achievement.length < 1 ||
                  feedback.learningGoals.length < 1 ||
                  feedback.skillRemark.length < 1
                }
              >
                Next
              </Button>
            ) : (
              <Button className='weekly-dialog-actions-butons' onClick={() => confirm()}>
                Confirm
              </Button>
            )}
            {step === 2 ? (
              <Button
                className='weekly-dialog-actions-butons'
                onClick={(e) => handleBack(e)}
              >
                Previous
              </Button>
            ) : (
              ''
            )}
            <Button
              className='weekly-dialog-actions-butons'
              onClick={() => closeDialog()}
            >
              Cancel
            </Button>
          </div>
        </div>
      ) : (
        <div className='weekly-report'>
          {/* {is_superuser && ( */}
          {/* <DialogTitle className='dialog-title'>PERSONALITY AND CHARACTER</DialogTitle> */}

          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Friendly and Courteous'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='friendlyCourteous'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.friendlyCourteous}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 151
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.friendlyCourteous.length > 0
              ? `${feedback.friendlyCourteous !== undefined
                ? 300 - feedback.friendlyCourteous.length
                : ''
              } characters
            remaining`
              : ''}
            {/* {feedback.friendlyCourteous !== undefined
              ? 150 - feedback.friendlyCourteous.length
              : ''}{' '}
            characters remaining */}
          </p>

          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Attentive'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='attentive'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.attentive}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 151
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.attentive.length > 0
              ? `${feedback.attentive !== undefined ? 300 - feedback.attentive.length : ''
              } characters
            remaining`
              : ''}
            {/* {feedback.attentive !== undefined ? 150 - feedback.attentive.length : ''}{' '}
            characters remaining */}
          </p>

          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Punctual'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='punctual'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.punctual}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 151
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.punctual.length > 0
              ? `${feedback.punctual !== undefined ? 300 - feedback.punctual.length : ''
              } characters
            remaining`
              : ''}

            {/* {feedback.punctual !== undefined ? 150 - feedback.punctual.length : ''}{' '}
            characters remaining */}
          </p>

          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Remark'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='personalityRemark'
            multiline
            rows={2}
            rowsMax={Infinity}
            value={feedback.personalityRemark}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 401
                ? handleOnChange(e)
                : ''
            }
          />
          <p>
            {feedback.personalityRemark.length > 0
              ? `${feedback.personalityRemark !== undefined
                ? 300 - feedback.personalityRemark.length
                : ''
              } characters
            remaining`
              : ''}
            {/* {feedback.personalityRemark !== undefined
              ? 400 - feedback.personalityRemark.length
              : ''}{' '}
            characters remaining */}
          </p>

          <FormControl component='fieldset'>
            <FormLabel component='legend'>Final Achievement</FormLabel>
            <RadioGroup
              style={{ flexWrap: 'nowrap' }}
              row
              aria-label='performance-grading'
              name='row-radio-buttons-group'
              value={finalAchievement}
              onChange={(e) => setFinalAchievement(e.target.value)}
            >
              <FormControlLabel
                style={{ minWidth: '120px' }}
                value='Emerging'
                control={<Radio />}
                label='Emerging'
              />
              <FormControlLabel
                style={{ minWidth: '120px' }}
                value='Proficient'
                control={<Radio />}
                label='Proficient'
              />
              <FormControlLabel
                style={{ minWidth: '120px' }}
                value='Accomplished'
                control={<Radio />}
                label='Accomplished'
              />
              <FormControlLabel
                style={{ minWidth: '120px' }}
                value='Exemplary'
                control={<Radio />}
                label='Exemplary'
              />
            </RadioGroup>
          </FormControl>
          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Student Email'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='studentEmailValidation'
            value={feedback.studentEmailValidation}
            helperText={helperText.student}
            error={helperText.studentError}
            // value={meetingName}
            // onChange={(e) => setMeetingName(e.target.value)}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 40
                ? handleOnChange(e)
                : ''
            }
          />
          <TextField
            required
            fullWidth
            className='filter-student weekly-report-input'
            label='Teacher Email'
            variant='outlined'
            size='small'
            autoComplete='off'
            name='teacherEmailValidation'
            value={feedback.teacherEmailValidation}
            helperText={helperText.teacher}
            error={helperText.teacherError}
            onChange={(e) =>
              e.target.value.length > -1 && e.target.value.length < 40
                ? handleOnChange(e)
                : ''
            }
          // value={meetingName}
          // onChange={(e) => setMeetingName(e.target.value)}
          />
          <div className='weekly-report-actions'>
            {step > 2 ? (
              <Button
                className='weekly-dialog-actions-butons'
                onClick={() => confirm()}
                disabled={
                  feedback.friendlyCourteous.length < 1 ||
                  feedback.attentive.length < 1 ||
                  feedback.punctual.length < 1 ||
                  feedback.personalityRemark.length < 1 ||
                  finalAchievement.length < 1 ||
                  feedback.studentEmailValidation.length < 1 ||
                  feedback.teacherEmailValidation.length < 1
                }
              >
                Confirm
              </Button>
            ) : (
              ''
            )}
            {step !== 1 ? (
              <Button
                className='weekly-dialog-actions-butons'
                onClick={(e) => handleBack(e)}
              >
                Previous
              </Button>
            ) : (
              ''
            )}
            <Button
              className='weekly-dialog-actions-butons'
              onClick={() => closeDialog()}
            >
              Cancel
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Form;
