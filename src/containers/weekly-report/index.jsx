import React, { useState, useContext, useEffect } from 'react';
import Layout from '../Layout';
import Loader from '../../components/loader/loader';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import { Autocomplete, Pagination } from '@material-ui/lab';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  MenuItem,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import './style.scss';
import Form from './Form';
import List from './weekly_report_list';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Filteration from './flteration';

const WeeklyReport = (props) => {
  const [loading, setLoading] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);
  const { is_superuser } = JSON.parse(localStorage.getItem('userDetails')) || {};
  const [step, setStep] = useState(1);
  const { setAlert } = useContext(AlertNotificationContext);
  const [dialogDetail, setDialogDetail] = useState('Create');
  const [editData, setEditData] = useState(null);
  const [data, setData] = useState({
    dataPerPage: 10,
    totalData: null,
    totalPages: null,
    currentPage: 1,
  });
  const [tableData, setTableData] = useState(null);
  const [studentBatchList, setStudentBatchList] = useState('');
  const [filterState, setFilterState] = useState({
    gradeList: [],
    selectedGrade: [],
    subjectList: [],
    selectedSubject: [],
    startDate: '',
    endDate: '',
    fullDate: [],
    search: '',
  });
  const [page, setPage] = useState(1);
  const [teacherID,setTeacherId] = useState('');
  const [searchDataValue, setSearchDataValue] = useState('')
  const [startDateValue, setStartDateValue] = useState('')
  const [ endDateValue, setEndDateValue] = useState('') 



  async function getWeeklyReportTable(pageNo, searchVal, teacher, startDate, endDate) {
    console.log({pageNo, searchVal, teacher,startDate, endDate}, 'mobile')
    const searchValue = searchVal ? `&term=${searchVal}` : '';
    setSearchDataValue(searchValue)
    const selectedTeacher = teacher?.length ? `&teacher_id=${teacher}` : '';
    setTeacherId(selectedTeacher);
    // const selectedSubject = subject ? `&subject_id=${subject}` : '';
    const start = startDate ? `&start_date=${startDate}` : '';
    setStartDateValue(start)
    const end = endDate ? `&end_date=${endDate}` : '';
    setEndDateValue(end);
    setLoading(true);
    try {
      const  result  = await axiosInstance.get(
        `${endpoints.weeklyReport.list}?page_size=${data.dataPerPage}&page=${data.currentPage}${searchValue}${selectedTeacher}${start}${end}`
      );
      if ( result?.status === 200) {
        setLoading(false);
        setTableData(result.data.result)
        setData({
          ...data,
          totalData: result.data.count,
          totalPages: result.data.total_pages,
          currentPage: result.data.current_page,
        });
      } else {
        setLoading(false);
        setAlert('error', data?.message);
      }
    } catch (error) {
      setLoading(false);
      setAlert('error', error?.message);
    }
}

  // const getWeeklyReportTable = () => {
  //   axiosInstance
  //     .get(
  //       `${endpoints.weeklyReport.list}?page_size=${data.dataPerPage}&page=${data.currentPage}`
  //       // `https://dev.plufo.letseduvate.com/qbox/course_extend/weekly_report_list/?page_size=${data.dataPerPage}&page=${data.currentPage}`
  //     )
  //     .then((result) => {
  //       if (result.data.status_code === 200) {
  //         console.log(result, 'pagination');
  //         setTableData(result.data.result);
          // setData({
          //   ...data,
          //   totalData: result.data.count,
          //   totalPages: result.data.total_pages,
          //   currentPage: result.data.current_page,
          // });
  //       }
  //     })
  //     .catch((error) => {
  //       //   setLoading(false);
  //       //   setAlert('error', error.message);
  //       console.log('err', error);
  //     });
  // };

  useEffect(() => {
    setPage(1);
    getWeeklyReportTable(
      1,
      filterState?.search || '',
      filterState?.selectedGrade?.map((teacher) => teacher?.tutor_id) || [],
      // filterState?.subjectList?.id || '',
      filterState?.startDate || '',
      filterState?.endDate || ''
    );
  }, []);

  return (
    <>
      <Layout>
        <div className='weekly-report-container'>
          {loading ? (
            <Loader />
          ) : (
            <>
              <div className='weekly-report-breadcrumb-wrapper'>
                <CommonBreadcrumbs
                  componentName='Reports'
                  childComponentName='Weekly Report'
                />
              </div>
            </>
          )}
        </div>
        <div style={{ width: '95%', margin: '0 auto' }} className='filter-container'>
          <Grid container spacing={3} alignItems='center'>
            <Grid item sm={1} xs={12}>
              <Tooltip title='Create Report' placement='bottom' arrow>
                <IconButton
                  className='create-report-button'
                  // color='primary'
                  style={{ backgroundColor: '#ef676a' }}
                  onClick={() => {
                    setDialogOpen(true);
                    // getAvailableSlot(date);
                    setDialogDetail('Create');
                  }}
                >
                  <AddIcon style={{ color: '#ffffff' }} />
                </IconButton>
              </Tooltip>
            </Grid>
            <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
                <Filteration
                  setPage={setPage}
                  // getStudentsList={getStudentsList}
                  getWeeklyReportTable={getWeeklyReportTable}
                  gradeList
                  filterState={filterState}
                  setFilterState={setFilterState}
                  setLoading={setLoading}
                />
            </Grid>
            {/* <Grid item sm={11} xs={12}>
                    <Accordion expanded={accordianOpen}>
                      <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls='panel1a-content'
                        id='panel1a-header'
                        onClick={() => setAccordianOpen(!accordianOpen)}
                      >
                        <Typography variant='h6' color='primary'>
                          Filter
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <Grid container spacing={3} alignItems='center'>
                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='meeting-name'
                              label='Meeting Name'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='meetingNameFilter'
                              value={filterInput.meetingNameFilter}
                              onChange={(e) => {
                                setFilterValue(e);
                              }}
                            />
                          </Grid>
                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='meeting-date'
                              label='Meeting Date'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='meetingDateFilter'
                              type='date'
                              InputLabelProps={{
                                shrink: true,
                              }}
                              value={filterInput.meetingDateFilter}
                              onChange={(e) => {
                                setFilterValue(e);
                              }}
                            />
                          </Grid>
                          {is_superuser && (
                            <Grid item md={3} sm={4} xs={12}>
                              <TextField
                                fullWidth
                                className='teacher-name'
                                label='Teacher Name'
                                variant='outlined'
                                size='small'
                                autoComplete='off'
                                name='teacherNameFilter'
                                value={filterInput.teacherNameFilter}
                                onChange={(e) => {
                                  setFilterValue(e);
                                }}
                              />
                            </Grid>
                          )}

                          <Grid item md={3} sm={4} xs={12}>
                            <TextField
                              fullWidth
                              className='student-name'
                              label='Student Name'
                              variant='outlined'
                              size='small'
                              autoComplete='off'
                              name='studentNameFilter'
                              value={filterInput.studentNameFilter}
                              onChange={(e) => {
                                setFilterValue(e);
                              }}
                            />
                          </Grid>
                          <Grid item md={3} sm={4} xs={12}>
                            <FormControl fullWidth margin='dense' variant='outlined'>
                              <InputLabel>Meeting Type</InputLabel>
                              <Select
                                value={filterInput.meetingTypeFilter || ''}
                                label='Meeting Type'
                                name='meetingTypeFilter'
                                onChange={(e) => {
                                  setFilterValue(e);
                                }}
                              >
                                <MenuItem value={''}>All</MenuItem>
                                <MenuItem value={'upcoming'}>Upcoming</MenuItem>
                                <MenuItem value={'past'}>Past</MenuItem>
                              </Select>
                            </FormControl>
                          </Grid>
                          
                          <Grid item sm={2} xs={3}>
                            <Button
                              className='filter-button'
                              onClick={() => clearFilter()}
                            >
                              Clear Filter
                            </Button>
                          </Grid>
                        </Grid>
                      </AccordionDetails>
                    </Accordion>
                  </Grid> */}
            <Grid xs={12}>
              {' '}
              <List
                setDialogOpen={setDialogOpen}
                setDialogDetail={setDialogDetail}
                setEditData={setEditData}
                getWeeklyReportTable={getWeeklyReportTable}
                tableData={tableData}
                pageData={data}
                dialogOpen={dialogOpen}
                teacherID={teacherID}
                searchDataValue={searchDataValue}
                startDateValue={startDateValue}
                endDateValue={endDateValue}
              />
            </Grid>
          </Grid>
        </div>
        {/* <div>Hello</div> */}
      </Layout>
      <Dialog
        open={dialogOpen}
        style={{ margin: '35px 0 0 0' }}
        className='create-weekly-report-dialog'
      >
        <DialogTitle className='dialog-title'>{dialogDetail} Weekly Report</DialogTitle>
        <Form
          setDialogOpen={setDialogOpen}
          dialogDetail={dialogDetail}
          editData={editData}
          step={step}
          setStep={setStep}
          getWeeklyReportTable={getWeeklyReportTable}
        />
      </Dialog>
    </>
  );
};

export default WeeklyReport;
