import React, { useState, useEffect, useContext } from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  MenuItem,
  DialogContent,
} from '@material-ui/core';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import axiosInstance from '../../config/axios';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import './style.scss';
import Tablepagination from '../../components/tablePagination';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
// import FileDownloadIcon from '@material-ui/icons/FileDownload';
import endpoints from '../../config/endpoints';

const List = ({
  setDialogOpen,
  setDialogDetail,
  setEditData,
  getWeeklyReportTable,
  pageData,
  tableData,
  dialogOpen,
  teacherID,
  searchDataValue,
  startDateValue,
  endDateValue,

}) => {
  const [weeklyData, setWeeklyData] = useState(null);
  const { setAlert } = useContext(AlertNotificationContext);
  const [dialogOpens, setDialogOpens] = useState(false);
  const [viewData, setViewData] = useState(null);
  const column = [
    'Sl No.',
    'ERP ID',
    'Student Name',
    'Teacher Name',
    'Grade',
    'Course',
    // 'Topics',
    // 'Achievements',
    // 'Learning Goals',
    // 'Remarks',
    // 'Friendly & Courteous',
    // 'Attentive',
    // 'Punctual',
    // 'Remark',
    // 'Final Achievement',
    'Actions',
  ];

  const [data, setData] = useState({
    dataPerPage: 10,
    totalData: null,
    totalPages: null,
    currentPage: 1,
  });

  useEffect(() => {
    getWeeklyReportList();
    // getWeeklyReportTable();
  }, [data.currentPage, data.dataPerPage, data.totalPages]);

  const getWeeklyReportList = () => {
    axiosInstance
      .get(
        `${endpoints.weeklyReport.list}?page_size=${data.dataPerPage}&page=${data.currentPage}${teacherID}${searchDataValue}${startDateValue}${endDateValue}`
        //   `${
        //     endpoints.ibook.teacherView
        //   }?domain_name=${getDomainName()}&page_number=${pageNo}&page_size=${limit}`
        // `https://dev.plufo.letseduvate.com/qbox/course_extend/weekly_report_list/?page_size=${data.dataPerPage}&page=${data.currentPage}`
      )
      .then((result) => {
        if (result.data.status_code === 200) {
          setWeeklyData(result.data.result);
          setData({
            ...data,
            totalData: result.data.count,
            totalPages: result.data.total_pages,
            currentPage: result.data.current_page,
          });
        }
      })
      .catch((error) => {
          // setLoading(false);
          setAlert('error', error.message);
        // console.log('err', error);
      });
  };

  const handleGeneratePDF = (id) => {
    // console.log(event);
    axiosInstance
      .post(`${endpoints.weeklyReport.generateReport}?report_id=${id}`)
      //   .post(
      //     `https://dev.plufo.letseduvate.com/qbox/course_extend/generate_report/?report_id=${id}`,
      //     {}
      //   )
      .then((res) => {
        if (res.data.status_code === 200) {
          console.log(res.data.message);
          setAlert('success', res.data.message);
          // getWeeklyReportTable();
          getWeeklyReportList();
        }
      })
      .catch((err) => {
        console.log(err, 'resultaaaa 11');
      });
    // setDialogOpen(false);
  };

  useEffect(() => {
    setData(pageData);
    setWeeklyData(tableData);
  }, [tableData]);

  const closeDialog = () => {
    setDialogOpens(false);
  };

  //   const handleGeneratePDF = (e) => {
  //     console.log(e);
  //   };

  //   console.log(weeklyData, 'weekly');

  return (
    <div>
      <div className='table-container'>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                {column &&
                  column.map((eachColumn, index) => {
                    return <TableCell key={index}>{eachColumn}</TableCell>;
                  })}
              </TableRow>
            </TableHead>
            <TableBody>
              {weeklyData &&
                weeklyData.map((eachData, index) => {
                  return (
                    <TableRow key={eachData.id}>
                      <TableCell>
                        {data.currentPage == 1
                          ? index + data.currentPage
                          : (data.currentPage - 1) * data.dataPerPage + (index + 1)}
                      </TableCell>
                      <TableCell>{eachData.erp_id}</TableCell>
                      <TableCell>{eachData.student_name}</TableCell>
                      <TableCell>{eachData.teacher_name}</TableCell>
                      <TableCell>{eachData.grade}</TableCell>
                      <TableCell>{eachData.course}</TableCell>
                      {/* <TableCell>{eachData.topics_covered}</TableCell>

                      <TableCell>{eachData.achievements}</TableCell>
                      <TableCell>{eachData.learning_goals}</TableCell>
                      <TableCell>{eachData.remarks}</TableCell>
                      <TableCell>{eachData.freindly_courteous}</TableCell>
                      <TableCell>{eachData.attentive}</TableCell>
                      <TableCell>{eachData.puntual}</TableCell>
                      <TableCell>{eachData.remark}</TableCell>
                      <TableCell>{eachData.final_achievement}</TableCell> */}

                      <TableCell style={{ margin: '0 auto' }} className='action-buttons'>
                        <Button
                          disabled={eachData.is_pdf_generated}
                          size='small'
                          className='host-meeting-button'
                          //   onClick={() => {
                          //     console.log(eachData.host);
                          //     window.open(eachData.host, '_blank');

                          //   }}
                          onClick={(e) => {
                            // console.log(e.id, 'generating');
                            handleGeneratePDF(eachData.id);
                          }}
                        >
                          {/* {getMeetingText(
                                    eachData.meeting_date,
                                    eachData.meeting_time
                                  )} */}
                          Generate
                        </Button>
                        <Button
                          size='small'
                          className='host-meeting-button'
                          disabled={eachData?.is_pdf_generated}
                          style={{ margin: '0 15px' }}
                          onClick={(e) => {
                            setDialogOpen(true);
                            setDialogDetail('Edit');
                            setEditData(eachData);
                          }}
                        >
                          <EditOutlinedIcon />
                        </Button>
                        <Button
                          size='small'
                          className='host-meeting-button'
                          style={{ margin: '0 15px' }}
                          //   disabled={eachData.is_pdf_generated}
                          onClick={(e) => {
                            setDialogOpens(true);
                            //   setDialogDetail('Edit');
                            setViewData(eachData);
                          }}
                        >
                          <InfoIcon />
                        </Button>
                        <Button
                          size='small'
                          className='host-meeting-button'
                          disabled={eachData.is_pdf_generated === false}
                          onClick={(e) => {
                            window.open(eachData.pdf_url, '_blank');
                          }}
                        >
                          {/* <FileDownloadIcon /> */}
                          Download
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
      <Dialog
        open={dialogOpens}
        style={{ margin: '35px 0 0 0' }}
        className='create-weekly-report-dialog'
      >
        <DialogTitle className='dialog-title'>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Typography variant='h6'>View Weekly Report</Typography>
            <Button
              className='close-button'
              onClick={() => closeDialog()}
            >
              <CloseIcon />
            </Button>
          </div>
        </DialogTitle>
        <DialogContent>
          {viewData !== null && (
            <div style={{ paddingBottom: '10px' }}>
              <Typography variant='body1'>
                <b>ERP ID -</b> {viewData.erp_id}
              </Typography>
              <Typography variant='body1'>
                <b>Student Name -</b> {viewData.student_name}
              </Typography>
              <Typography variant='body1'>
                <b>Teacher Name -</b> {viewData.teacher_name}
              </Typography>
              <Typography variant='body1'>
                <b>Grade -</b> {viewData.grade}
              </Typography>
              <Typography variant='body1'>
                <b>Course -</b> {viewData.course}
              </Typography>
              <Typography variant='body1'>
                <b>Weekly Date -</b> {viewData.week_data}
              </Typography>
              <Typography variant='body1'>
                <b>Topic -</b> {viewData.topics_covered}
              </Typography>
              <Typography variant='body1'>
                <b>Achievement -</b> {viewData.achievements}
              </Typography>
              <Typography variant='body1'>
                <b>Learning Goals -</b> {viewData.learning_goals}
              </Typography>
              <Typography variant='body1'>
                <b>Remark -</b> {viewData.remarks}
              </Typography>
              <Typography variant='body1'>
                <b>Friendly and Courteous -</b> {viewData.freindly_courteous}
              </Typography>
              <Typography variant='body1'>
                <b>Attentive -</b> {viewData.attentive}
              </Typography>
              <Typography variant='body1'>
                <b>Punctual -</b> {viewData.puntual}
              </Typography>
              <Typography variant='body1'>
                <b>Remark -</b> {viewData.remark}
              </Typography>
              <Typography variant='body1'>
                <b>Final Achievement -</b> {viewData.final_achievement}
              </Typography>
              <Typography variant='body1'>
                <b>Student Mail ID -</b> {viewData.student_mail}
              </Typography>
              <Typography variant='body1'>
                <b>Teacher Mail ID -</b> {viewData.teacher_mail}
              </Typography>
            </div>
          )}
        </DialogContent>
      </Dialog>
      <Tablepagination
      data={pageData} 
      setData={setData}
      // getWeeklyReportTable={getWeeklyReportTable}
      />
    </div>
  );
};

export default List;
