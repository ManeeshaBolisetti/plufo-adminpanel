/* eslint-disable no-nested-ternary */
import React, { useState, useContext, useEffect } from 'react';
import {
  Grid,
  Card,
  TextField,
  Typography,
  Radio,
  RadioGroup,
  FormControlLabel,
  Switch,
  Button,
} from '@material-ui/core';
import { useHistory, useLocation } from 'react-router-dom';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import './style.scss';
import Loader from '../../components/loader/loader';
import Layout from '../Layout';

const AddEditZoom = ()=>{
    const location = useLocation();
    const history = useHistory();
    const { setAlert } = useContext(AlertNotificationContext);
    const [edit, setEdit] = useState(false);
    const [loading, setLoading] = useState(false);
    const [zoomDetails, setZoomDetails] = useState({
        email:'',
        username:'',
        zoom_user_id:'',
        is_active: true,
    });

    useEffect(() => {
        if (location.state && location.state.edit) {
          setEdit(true);
          setZoomDetails(location.state);
        }
    }, [location.state]);

      function handleSubmit() {
        if (!zoomDetails.email) {
          setAlert('warning', 'Enter Zoom Email');
          return;
        }
        if (!zoomDetails.username) {
          setAlert('warning', 'Enter Zoom User Name');
          return;
        }
        if (!zoomDetails.zoom_user_id) {
          setAlert('warning', 'Enter Zoom User ID');
          return;
        }

        const data = { ...zoomDetails };
        if (edit) {
          data.id = location.state.id;
          setLoading(true);
          axiosInstance
            .put(`${endpoints.zoomIdApis.addUpdateZoomIdApi}`, { ...data })
            .then((response) => {
              setLoading(false);
              if (response.data.status_code === 200) {
                history.push('/aol-view-zoom');
                setAlert('success', response.data.message);
              } else {
                setAlert('error', response.data.description);
              }
            })
            .catch((err) => {
              setAlert('success', err.response.data.description);
              setLoading(false);
            });
        } else {
          setLoading(true);
          axiosInstance
            .post(endpoints.zoomIdApis.addUpdateZoomIdApi, { ...data })
            .then((response) => {
              setLoading(false);
              if (response.data.status_code === 200) {
                history.push('/aol-view-zoom');
                setAlert('success', response.data.message);
              } else {
                setAlert('error', response.data.description);
              }
            })
            .catch((err) => {
              setAlert('success', err.response.data.description);
              setLoading(false);
            });
        }
      }
      function handleOnchange(value, key) {
        setZoomDetails((prev) => {
          const data = { ...prev };
          switch (key) {
            case key:
              data[key] = value;
              return data;
            default:
              return data;
          }
        });
      }
    
      
    
      function handleTextField(key, name, type) {
        return (
          <TextField
            className='add-edit-text-field'
            value={zoomDetails[key] || ''}
            disabled={edit && key === 'discount'}
            onChange={(e) => {
              if (type === 'number' && e.target.value > -1) {
                handleOnchange(e.target.value.trimLeft(), key);
              }else {
                handleOnchange(e.target.value.trimLeft(), key);
              }
            }}
            margin='dense'
            variant='outlined'
            label={type === 'date' ? '' : name}
            helperText={type === 'date' ? name : ''}
            fullWidth
            type={type}
            placeholder={`Enter ${name}`}
          />
        );
      }
    

      return (
        <>
          {loading ? (
            <Loader />
          ) : (
            <Layout>
              <Grid container spacing={2} className='add-edit-zoom-body'>
                <Grid item md={12} xs={12} className='add-edit-zoom-top-head'>
                  <CommonBreadcrumbs
                    componentName='Zoom'
                    childComponentName={edit ? 'Update Zoom' : 'Add Zoom'}
                  />
                </Grid>
                <Grid item md={12} xs={12} className='add-edit-zoom-form-div'>
                  <Card className='add-edit-zoom-form-card'>
                    <Grid container spacing={2}>
                      <Grid item md={4} xs={12}>
                        {handleTextField('username', 'User Name', 'text')}
                      </Grid>
                      <Grid item md={4} xs={12}>
                        {handleTextField('email', 'Zoom Email', 'text')}
                      </Grid>
                      <Grid item md={4} xs={12}>
                        {handleTextField('zoom_user_id', 'Zoom User Id', 'text')}
                      </Grid>
                      
                      
                    </Grid>
                    <Grid item md={12} xs={12} style={{ textAlign: 'right' }}>
                      <Grid
                        container
                        spacing={2}
                        justify='space-between'
                        style={{ padding: '20px 10px' }}
                      >
                        <Button
                          size='small'
                          variant='contained'
                          color='primary'
                          onClick={handleSubmit}
                        >
                          Submit
                        </Button>
                      </Grid>
                    </Grid>
                  </Card>
                </Grid>
              </Grid>
            </Layout>
          )}
        </>
      );
};

export default AddEditZoom;