import React, { useRef, useContext, useEffect } from 'react';
import { Grid, TextField, Button } from '@material-ui/core';
import debounce from 'lodash.debounce';
import MomentUtils from '@material-ui/pickers-4.2/adapter/moment';
import { LocalizationProvider, DateRangePicker } from '@material-ui/pickers-4.2';
import { Autocomplete } from '@material-ui/lab';
import moment from 'moment';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import CustomSearchBar from '../../components/custom-seearch-bar';
import endpoints from '../../config/endpoints';
import axiosInstance from '../../config/axios';

const Filtration = ({
  setPageNumber,
  getZoomIdList,
  filterState,
  setFilterState,
  setLoading,
}) => {
  const { setAlert } = useContext(AlertNotificationContext);

  async function ApiCall(url, type) {
    setLoading(true);
    try {
      const { data } = await axiosInstance.get(url);
      if (data?.status_code === 200 || data?.status_code === 201) {
        if (type === 'zoomDrop') {
          setFilterState((prev) => ({ ...prev, zoomIdList: data?.data }));
        }
        setLoading(false);
      }
       else {
        setLoading(false);
        setAlert('error', data.message);
      }
    } catch (error) {
      setAlert('error', error.message);
      setLoading(false);
    }
  }

  useEffect(() => {
    ApiCall(`${endpoints.zoomIdApis.getZoomIdApi}`);
  }, []);

  const delayedQuery = useRef(
    debounce((q, state) => {
      setPageNumber(1);
      getZoomIdList(
        1,
        q,
        state?.selectedZoomIds?.id || '',
        state?.startDate || '',
        state?.endDate || ''
      );
    }, 1000)
  ).current;

  return (
    <Grid container spacing={2}>
      <Grid item md={3} xs={12}>
        <Autocomplete
          // size='small'
          id='zoomids'
          multiple
          className='dropdownIcon'
          options={filterState?.zoomIdList || []}
          getOptionLabel={(option) => option?.zoom__zoom_email || ''}
          filterSelectedOptions
          value={filterState?.selectedZoomIds || ''}
          onChange={(event, value) => {
              event.preventDefault();
            setFilterState({ ...filterState, selectedZoomIds: value });
            setPageNumber(1);
            getZoomIdList(
              1,
              filterState?.search || '',
              value?.id || '',
              filterState?.startDate || '',
              filterState?.endDate || ''
            );
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              // size='small'
              variant='outlined'
              label='ZoomId'
              placeholder='ZoomId'
            />
          )}
        />
      </Grid>
      <Grid item md={3} xs={12}>
        <LocalizationProvider dateAdapter={MomentUtils}>
          <DateRangePicker
            startText='Select-date-range'
            value={filterState?.fullDate}
            onChange={(newValue) => {
              setFilterState((prev) => ({ ...prev, fullDate: newValue }));
              setFilterState((prev) => ({
                ...prev,
                startDate: moment(newValue?.[0])?.format()?.split('T')?.[0],
              }));
              setFilterState((prev) => ({
                ...prev,
                endDate: moment(newValue?.[1])?.format()?.split('T')?.[0],
              }));
              setPageNumber(1);
              getZoomIdList(
                1,
                filterState?.search || '',
                filterState?.selectedZoomIds?.map((zoom) => zoom?.zoom_id) || [],
                moment(newValue?.[0])?.format()?.split('T')?.[0] || '',
                moment(newValue?.[1])?.format()?.split('T')?.[0] || ''
              );
            }}
            renderInput={({ inputProps, ...startProps }, endProps) => {
              return (
                <>
                  <TextField
                    {...startProps}
                    inputProps={{
                      ...inputProps,
                      value: `${inputProps.value} - ${endProps.inputProps.value}`,
                      readOnly: true,
                    }}
                    // size='small'
                    style={{ minWidth: '100%' }}
                    helperText=''
                  />
                </>
              );
            }}
          />
        </LocalizationProvider>
      </Grid>
      <Grid item md={3} xs={12}>
        <CustomSearchBar
          value={filterState?.search}
          setValue={(value) => {
            setFilterState({ ...filterState, search: value });
            delayedQuery(value, filterState);
          }}
          onChange={(e) => {
            setFilterState({ ...filterState, search: e.target.value.trimLeft() });
            delayedQuery(e.target.value.trimLeft(), filterState);
          }}
          label=''
          placeholder='Zoom Id Search'
        />
      </Grid>
      <Grid item md={3} xs={12} style={{ textAlign: 'left' }}>
        <Button
          style={{ borderRadius: '10px' }}
          // size='small'
          fullWidth
          variant='contained'
          color='primary'
          onClick={() => {
            setPageNumber(1);
            getZoomIdList(1);
            setFilterState((prev) => ({
              ...prev,
              selectedZoomIds: [],
              startDate: '',
              endDate: '',
              fullDate: [],
              search: '',
            }));
          }}
        >
          filter
        </Button>
      </Grid>
    </Grid>
  );
};

export default Filtration;
