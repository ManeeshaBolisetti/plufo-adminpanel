import React, { useState, useEffect, useContext } from 'react';
import {
  Grid,
  Button,
  TableHead,
  TableCell,
  Card,
  TableRow,
  Table,
  TableBody,
  IconButton,
  Typography,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import BlockIcon from '@material-ui/icons/Block';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import CommonBreadcrumbs from '../../components/common-breadcrumbs/breadcrumbs';
import Layout from '../Layout';
import './style.scss';
import axiosInstance from '../../config/axios';
import endpoints from '../../config/endpoints';
import Loading from '../../components/loader/loader';
import { AlertNotificationContext } from '../../context-api/alert-context/alert-state';
import ConfirmDialog from '../../components/confirm-dialog';
import { Pagination } from '@material-ui/lab';
import Filtration from './filtration';
import moment from 'moment';

const selectedZoomId = {
    username: '',
    email: '',
    zoom_user_id:'',
    is_active:true,
};

const ZoomIdList = () => {
    const history = useHistory();
    const { setAlert } = useContext(AlertNotificationContext);
    const [zoomList, setZoomList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [pageNumber, setPageNumber] = useState(1);
    const [open, setOpen] = useState(false);
    const [content, setContent] = useState('');
    // const [search, setSearch] = useState('');
    // const [zoomEmail, setZoomEmail] = useState('');
    // const [zoomUserName, setZoomUserName] = useState('');
    // const [zoomUserId, setZoomUserId] = useState('');

    const [filterState, setFilterState] = useState({
      zoomIdList: [],
      selectedZoomIds: [],
      startDate: moment()?.subtract(1, 'days')?.format()?.split('T')?.[0],
      endDate: moment()?.format()?.split('T')?.[0],
      fullDate: [],
      search: '',
    });

    async function getZoomIdList(pageNo, searchVal, zoomDrop, startDate, endDate) {
      const searchValue = searchVal ? `&term=${searchVal}` : '';
      const selectedZoom = zoomDrop ? `&zoom_drop=${zoomDrop}` : '';
      const start = startDate ? `&start_date=${startDate}` : '';
      const end = endDate ? `&end_date=${endDate}` : '';
      setLoading(true);
      try {
        const { data } = await axiosInstance.get(
          `${
            endpoints.zoomIdApis.getZoomIdApi
          }?page=${pageNo}&page_size=${10}${searchValue}${selectedZoom}${start}${end}`
        );
        if (data?.status_code === 200) {
          setLoading(false);
          setZoomList(data);
        } else {
          setLoading(false);
          setAlert('error', data?.message);
        }
      } catch (error) {
        setLoading(false);
        setAlert('error', error?.message);
      }
    }
  
      function handleUpdate() {
        setLoading(true);
        axiosInstance
          .put(`${endpoints.zoomIdApis.addUpdateZoomIdApi}`, { ...content })
          .then((response) => {
            setLoading(false);
            if (response.data.status_code === 200) {
              setOpen(false);
              setAlert('success', `ZoomId Successfully ${content.key}`);
              getZoomIdList(
                1,
                filterState?.search || '',
                filterState?.selectedZoomIds?.id|| '',
                filterState?.startDate || '',
                filterState?.endDate || ''
              );
              setContent('');
            } else {
              setAlert('success', response.data.description);
            }
          })
          .catch((err) => {
            setAlert('success', err.response.data.description);
            setLoading(false);
          });
      }
    
      function addZoomId() {
        history.push({
          pathname: '/aol-add-edit-zoom-id',
          state: {
            ...selectedZoomId,
          },
        });
      }
      function handleEdit(item) {
        const data = { ...item };
        data.edit = true;
        history.push({
          pathname: '/aol-add-edit-zoom-id',
          state: {
            ...data,
          },
        });
      }

      function handlePagination(event, page) {
        event.preventDefault();
        setPageNumber(page);
        getZoomIdList(
          page,
          filterState?.search || '',
          filterState?.selectedZoomIds?.id || '',
          filterState?.startDate || '',
          filterState?.endDate || ''
        );
      }

      useEffect(() => {
        setPageNumber(1);
        getZoomIdList(
          1,
          filterState?.search || '',
          filterState?.selectedZoomIds?.id || '',
          filterState?.startDate || '',
          filterState?.endDate || ''
        );
      }, []);
    
      return (
        <Layout>
          <Grid container spacing={2} className='plufo-zoom-id-generator-body'>
            <Grid item md={12} xs={12} className='plufo-zoom-id-generator-top-head'>
              <CommonBreadcrumbs componentName='Zoom' childComponentName='Zoom Ids' />
            </Grid>
            <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
              <Grid item md={12} xs={12} style={{ textAlign: 'center' }}>
                <Filtration
                  setPageNumber={setPageNumber}
                  getZoomIdList={getZoomIdList}
                  filterState={filterState}
                  setFilterState={setFilterState}
                  setLoading={setLoading}
                />
              </Grid>
            </Grid>
            <Grid item md={12} xs={12} className='plufo-zoom-id-generator-top-head1'>
              <Button
                variant='contained'
                color='primary'
                size='small'
                onClick={() => addZoomId()}
              >
                <AddCircleOutlineIcon />
                &nbsp;Add Zoom Id
              </Button>
            </Grid>
            <Grid item md={12} xs={12} className='plufo-zoom-id-generator-table-div'>
              <Card className='plufo-zoom-id-generator-table-card'>
                {zoomList && zoomList.result && zoomList.result.length === 0 && (
                  <Typography
                    variant='h6'
                    color='primary'
                    style={{ textAlign: 'center', margin: '20px 0px' }}
                  >
                    ZoomId's Not Created
                  </Typography>
                )}
                {zoomList && zoomList.result && zoomList.result.length !== 0 && (
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell float='left'>S.No</TableCell>
                        <TableCell float='left'>Zoom Email</TableCell>
                        <TableCell float='left'>Status</TableCell>
                        <TableCell float='left'>Actions</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {zoomList &&
                        zoomList.result &&
                        zoomList.result.length !== 0 &&
                        zoomList.result.map((item, index) => (
                          <TableRow key={item.id}>
                            <TableCell float='left'>{index + 1}</TableCell>
                            <TableCell float='left'>{item.email}</TableCell>
                            
                            <TableCell float='left'>
                              {item.is_active ? (
                                <span style={{ color: 'green', fontSize: '15px' }}>
                                  Active
                                </span>
                              ) : (
                                <span style={{ color: 'red', fontSize: '15px' }}>
                                  In Active
                                </span>
                              )}
                            </TableCell>
                            <TableCell float='left'>
                              <IconButton
                                size='small'
                                title={item.is_active ? 'DeActivate' : 'Activate'}
                                onClick={
                                  () => {
                                    setOpen(true);
                                    setContent({
                                      is_active: !item.is_active,
                                      id: item.id,
                                      key: item.is_active ? 'DeActivate' : 'Activate',
                                    });
                                  }
                                  // handleUpdate({ is_active: !item.is_active, id: item.id })
                                }
                              >
                                {item.is_active ? (
                                  <BlockIcon color='secondary' />
                                ) : (
                                  <RemoveCircleIcon color='primary' />
                                )}
                              </IconButton>
                              <IconButton
                                size='small'
                                disabled
                                onClick={
                                  () => {
                                    setOpen(true);
                                    setContent({
                                      is_delete: !item.is_delete,
                                      id: item.id,
                                      key: 'Deleted',
                                    });
                                  }
                                  // handleUpdate({ is_delete: !item.is_delete, id: item.id })
                                }
                              >
                                <DeleteIcon />
                              </IconButton>
                              <IconButton size='small' onClick={() => handleEdit(item)}>
                                <EditIcon color='secondary' />
                              </IconButton>
                            </TableCell>
                          </TableRow>
                        ))}
                    <TableRow>
                      <TableCell colSpan='8'>
                        {zoomList && zoomList.result && zoomList.result.length !== 0 && (
                          <Pagination
                          style={{ textAlign: 'center', display: 'inline-flex' }}
                          onChange={handlePagination}
                          count={zoomList.total_pages}
                          color='primary'
                          page={pageNumber}
                          />
                        )}
                      </TableCell>
                    </TableRow>
                    </TableBody>
                  </Table>
                )}
              </Card>
            </Grid>
          </Grid>
          {loading && <Loading />}
          {open ? (
            <ConfirmDialog
              open={open}
              cancel={() => setOpen(false)}
              confirm={handleUpdate}
              title={`Are you sure to ${content && content.key}`}
            />
          ) : (
            ''
          )}
        </Layout>
      );
};

export default ZoomIdList;