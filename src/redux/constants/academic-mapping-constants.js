const mappingConstants = {
  SUBJECT_REQUEST: 'SUBJECT_REQUEST',
  SUBJECT_SUCCESS: 'SUBJECT_SUCCESS',
  SUBJECT_FAILURE: 'SUBJECT_FAILURE',
};

export default mappingConstants;
